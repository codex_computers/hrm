{*******************************************************}
{                                                       }
{     ��������� :  ������ ��������                     }
{                                                       }
{     ����� :  12.03.2010                               }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

unit RabotnoMesto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMemo,
  cxGroupBox, cxLabel, cxDBLabel, cxRadioGroup, cxBlobEdit, cxRichEdit,
  cxDBRichEdit, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxSpinEdit, cxSplitter, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxCustomHint, dxPSdxDBOCLnk,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinOffice2013White, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxPScxSchedulerLnk;

type
  TfrmRabotnoMesto = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ZVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA_PLATA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_GRUPA_PLATA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn;
    cxGrid1DBTableView1OBRAZOVANIE_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVNO: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_OBRAZOVANIE: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    txtZvanje: TcxDBTextEdit;
    cxGroupBox2: TcxGroupBox;
    txtUslovi: TcxDBMemo;
    Label5: TLabel;
    txtVidVrabotuvanje: TcxDBTextEdit;
    cbVidVrabotuvanje: TcxDBLookupComboBox;
    Label6: TLabel;
    txtGrupa: TcxDBTextEdit;
    cbGrupa: TcxDBLookupComboBox;
    Label7: TLabel;
    txtObrazovanie: TcxDBTextEdit;
    cbObrazovanie: TcxDBLookupComboBox;
    Label8: TLabel;
    cxGrid1DBTableView1OPIS_AKTIVNO: TcxGridDBColumn;
    cxDBRadioGroup1: TcxDBRadioGroup;
    Label9: TLabel;
    txtOpis: TcxDBMemo;
    Label10: TLabel;
    Label11: TLabel;
    txtOdgovaraPred: TcxDBTextEdit;
    txtNaziv: TcxDBTextEdit;
    Label2: TLabel;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ODGOVARA_PRED: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    dxBarLargeButton10: TdxBarLargeButton;
    aStatusPM: TAction;
    aUrediTextOpis: TAction;
    cxButton2: TcxButton;
    aUrediTextUslovi: TAction;
    txtRabIsk: TcxDBTextEdit;
    Label12: TLabel;
    cxGrid1DBTableView1RABOTNO_ISKUSTVO: TcxGridDBColumn;
    Label14: TLabel;
    txtSZObrazovanie: TcxDBTextEdit;
    cbSZObrazovanie: TcxDBLookupComboBox;
    cxGrid1DBTableView1LOKACIJA_RM: TcxGridDBColumn;
    cxGrid1DBTableView1ID_SZ_OBRAZOVANIE: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1ID_TIP_RABOTNO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_SZ_OBRAZOVANIE: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_RABOTNO_VREME: TcxGridDBColumn;
    Label15: TLabel;
    txtRabotnoVreme: TcxDBSpinEdit;
    Label16: TLabel;
    txtTipRV: TcxDBTextEdit;
    cbTipRV: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID_RE: TcxGridDBColumn;
    Label17: TLabel;
    Rakovodenje: TcxDBTextEdit;
    Label18: TLabel;
    DenoviGO: TcxDBTextEdit;
    DenUsloviGO: TcxDBTextEdit;
    Label19: TLabel;
    UsloviRabota: TcxDBTextEdit;
    Label20: TLabel;
    Label22: TLabel;
    SplitterdPanel: TcxSplitter;
    Label21: TLabel;
    txtStSlozenost: TcxDBTextEdit;
    cxGrid1DBTableView1STEPEN_SLOZENOST: TcxGridDBColumn;
    Label4: TLabel;
    txtGrupaPlata: TcxDBTextEdit;
    cbGrupaPlata: TcxDBLookupComboBox;
    txtObrazovanieOpis: TcxDBMemo;
    Label13: TLabel;
    txtLokacija: TcxDBTextEdit;
    Label23: TLabel;
    KATEGORIJA_SIFRA: TcxDBTextEdit;
    KATEGORIJA: TcxDBLookupComboBox;
    cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VID_VRABOTUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_GRUPA1: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1SZO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJA: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJARM_NAZIV: TcxGridDBColumn;
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aStatusPMExecute(Sender: TObject);
    procedure aUrediTextOpisExecute(Sender: TObject);
    procedure aUrediTextUsloviExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure txtObrazovanieOpisDblClick(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRabotnoMesto: TfrmRabotnoMesto;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,DaNe,
Notepad, dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmRabotnoMesto.aAzurirajExecute(Sender: TObject);
begin
  SplitterdPanel.OpenSplitter;
  inherited;

end;

procedure TfrmRabotnoMesto.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(5);
end;

procedure TfrmRabotnoMesto.aNovExecute(Sender: TObject);
begin
  SplitterdPanel.OpenSplitter;
  inherited;
  dmSis.tblRabotnoMestoID_RE_FIRMA.Value:=firma;

end;

procedure TfrmRabotnoMesto.aStatusPMExecute(Sender: TObject);
begin
  SplitterdPanel.OpenSplitter;
  inherited;
  if not dmSis.tblRabotnoMesto.IsEmpty then
  begin
    if dmSis.tblRabotnoMesto.State in [dsEdit,dsInsert] then dmSis.tblRabotnoMesto.Cancel;
    if dmSis.tblRabotnoMestoAKTIVNO.Value=1 then
    begin
         frmDaNe := TfrmDaNe.Create(self, '������� �� ������', '��������� ����� � �������.'+#10#13+'���� ��������� ������ �� �� ��������� �������� ���������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
           Abort
       else
       begin
          //�� �� update-��� ��������
           dmSis.tblRabotnoMesto.Edit;
           dmsis.tblRabotnoMestoAKTIVNO.Value:=0;
           dmSis.tblRabotnoMesto.Post;
       end;
    end
    else
     if dmSis.tblRabotnoMestoAKTIVNO.Value=0 then
     begin
         frmDaNe := TfrmDaNe.Create(self, '������� �� ������', '��������� ����� � ���������.'+#10#13+'���� ��������� ������ �� �� ��������� �������� �������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
            Abort
       else
       begin
          //�� �� update-��� ��������
           dmSis.tblRabotnoMesto.Edit;
           dmsis.tblRabotnoMestoAKTIVNO.Value:=1;
           dmSis.tblRabotnoMesto.Post;
        end;
     end;
  end;
end;

procedure TfrmRabotnoMesto.aUrediTextOpisExecute(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoMestoOPIS.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRabotnoMestoOPIS.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmRabotnoMesto.aUrediTextUsloviExecute(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoMestoUSLOVI.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
   	dmSis.tblRabotnoMestoUSLOVI.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmRabotnoMesto.cxDBTextEditAllExit(Sender: TObject);
begin
 // if Sender=cxDBLookupComboBox(cbSZObrazovanie) then
  if (Sender as TWinControl)= cbSZObrazovanie then
  begin
    dmSis.tblRabotnoMestoID_SZ_OBRAZOVANIE.Value:=dmSis.tblSZObrazovanieID.Value;
  end;

  inherited;

end;

procedure TfrmRabotnoMesto.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
//  case key of
//    VK_RETURN:
//     begin
//         ShowMessage(Copy(txtOpis.Text,Length(txtOpis.Text)-1,1));
//
//         if not ((Copy(txtOpis.Text,Length(txtOpis.Text)-1,1))='.') then
//            ShowMessage(Copy(txtOpis.Text,Length(txtOpis.Text),1));
//          //   PostMessage(Handle,WM_NextDlgCtl,0,0);
//     end;
//  end;
end;

procedure TfrmRabotnoMesto.FormCreate(Sender: TObject);
begin
  inherited;
  dmsis.tblReVoFirma.close;
  dmSis.tblReVoFirma.ParamByName('re').AsInteger:=firma;
  dmsis.tblReVoFirma.open;

  Position:=poDesktopCenter;
  WindowState:=wsMaximized;
end;

procedure TfrmRabotnoMesto.txtObrazovanieOpisDblClick(Sender: TObject);
begin
  inherited;
   frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoMestoOBRAZOVANIE_OPIS.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRabotnoMestoOBRAZOVANIE_OPIS.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

end.
