unit Deca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit, cxCalendar,
  cxGroupBox, cxRadioGroup, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxCustomHint, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TfrmDeca = class(TfrmMaster)
    cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1DATA_RAGJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1POL: TcxGridDBColumn;
    cxGrid1DBTableView1RODITELNAZIV: TcxGridDBColumn;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    MB_VRABOTEN: TcxDBTextEdit;
    Label11: TLabel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1POL1: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    MB: TcxDBTextEdit;
    Label4: TLabel;
    Label2: TLabel;
    Prezime: TcxDBTextEdit;
    tatkovo_ime: TcxDBTextEdit;
    Label5: TLabel;
    Label6: TLabel;
    ime: TcxDBTextEdit;
    vozrast: TcxDBTextEdit;
    Label3: TLabel;
    DATUM_RADJANJE: TcxDBDateEdit;
    Label7: TLabel;
    Pol: TcxDBRadioGroup;
    cxGrid1DBTableView1PRZIME: TcxGridDBColumn;
    cxGrid1DBTableView1MAT_BR: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1OSIGURENIK: TcxGridDBColumn;
    cxGrid1DBTableView1OsigurenikDaNe: TcxGridDBColumn;
    cxGrid1DBTableView1RODITEL: TcxGridDBColumn;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MBPropertiesEditValueChanged(Sender: TObject);
    function proverkaModul11(broj: Int64): boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDeca: TfrmDeca;

implementation

uses dmUnit, Utils;

{$R *.dfm}

procedure TfrmDeca.aAzurirajExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if tag = 1  then
       MB.SetFocus
    else
       prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmDeca.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if tag = 1 then
      begin
        dm.tblDecaRODITEL.Value:=dm.tblLicaVraboteniMB.Value;
        MB.SetFocus;
      end
    else
         prva.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmDeca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if tag = 1 then
     begin
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.FullRefresh;
     end;
end;

procedure TfrmDeca.FormShow(Sender: TObject);
begin
  inherited;
   if tag = 1 then
     begin
       dm.tblDeca.close;
       dm.tblDeca.ParamByName('roditelMB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblDeca.Open;

       dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblLica.FullRefresh;
     end
  else
     begin
       dm.tblDeca.close;
       dm.tblDeca.ParamByName('roditelMB').Value:='%';
       dm.tblDeca.Open;

       dm.tblLica.Close;
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.Open;
     end;
end;

function TfrmDeca.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;


procedure TfrmDeca.MBPropertiesEditValueChanged(Sender: TObject);
var pom, date:string;
begin
  if (mb.Text <> '')  and ((cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse)) then
      begin
        if proverkaModul11(StrToInt64(MB.Text))then
          begin
            pom:=dm.tblDecaMAT_BR.Value;
            date:= pom[1] + pom[2] + '.' + pom[3] + pom[4] + '.';
            if StrToInt(pom[5]) = 9 then
              date:=date + '1'
            else if StrToInt(pom[5]) in [0,1]then
              date:=date + '2';
            date:=date + pom[5] + pom[6]+ pom[7];
            dm.tblDecaDATA_RAGJANJE.Value:=StrToDate(date);
            if strtoint(pom[10]) in [0,1,2,3,4] then
              dm.tblDecaPOL.Value:=1
            else if strtoint(pom[10]) in [5,6,7,8,9] then
              dm.tblDecaPOL.Value:=2
          end
        else
          begin
            ShowMessage('��������� ������� ��� �� � ������� !!!');
          end;
      end;
end;

end.
