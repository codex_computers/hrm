object dmSis: TdmSis
  OldCreateOrder = False
  Height = 645
  Width = 751
  object tblVidVrabotuvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_VID_VRABOTUVANJE'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_VID_VRABOTUVANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_VID_VRABOTUVANJE('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select '
      '    vv.id,'
      '    vv.naziv,'
      '    vv.ts_ins,'
      '    vv.ts_upd,'
      '    vv.usr_ins,'
      '    vv.usr_upd'
      'from hr_vid_vrabotuvanje vv'
      ''
      ' WHERE '
      '        VV.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select '
      '    vv.id,'
      '    vv.naziv,'
      '    vv.ts_ins,'
      '    vv.ts_upd,'
      '    vv.usr_ins,'
      '    vv.usr_upd'
      'from hr_vid_vrabotuvanje vv')
    AutoUpdateOptions.UpdateTableName = 'HR_VID_VRABOTUVANJE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_VID_VRABOTUVANJE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 16
    oRefreshDeletedRecord = True
    object tblVidVrabotuvanjeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblVidVrabotuvanjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidVrabotuvanjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblVidVrabotuvanjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblVidVrabotuvanjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidVrabotuvanjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblGrupaRM: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_GRUPA_RM'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OZNAKA = :OZNAKA,'
      '    STEPEN_SLOZENOST = :STEPEN_SLOZENOST,'
      '    STEPEN_NA_SLOZENOST_2 = :STEPEN_NA_SLOZENOST_2,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_GRUPA_RM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_GRUPA_RM('
      '    ID,'
      '    NAZIV,'
      '    OZNAKA,'
      '    STEPEN_SLOZENOST,'
      '    STEPEN_NA_SLOZENOST_2,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OZNAKA,'
      '    :STEPEN_SLOZENOST,'
      '    :STEPEN_NA_SLOZENOST_2,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    grm.id,'
      '    grm.naziv,'
      '    grm.oznaka,'
      '    grm.stepen_slozenost,'
      '    grm.stepen_na_slozenost_2,'
      '    grm.ts_ins,'
      '    grm.ts_upd,'
      '    grm.usr_ins,'
      '    grm.usr_upd'
      'from hr_grupa_rm grm'
      ''
      ' WHERE '
      '        GRM.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    grm.id,'
      '    grm.naziv,'
      '    grm.oznaka,'
      '    grm.stepen_slozenost,'
      '    grm.stepen_na_slozenost_2,'
      '    grm.ts_ins,'
      '    grm.ts_upd,'
      '    grm.usr_ins,'
      '    grm.usr_upd'
      'from hr_grupa_rm grm')
    AutoUpdateOptions.UpdateTableName = 'HR_GRUPA_RM'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_GRUPA_RM_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 72
    oRefreshDeletedRecord = True
    object tblGrupaRMID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblGrupaRMNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaRMTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblGrupaRMTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblGrupaRMUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaRMUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaRMOZNAKA: TFIBStringField
      DisplayLabel = #1054#1079#1085#1072#1082#1072
      FieldName = 'OZNAKA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaRMSTEPEN_SLOZENOST: TFIBBCDField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' ('#1055#1088#1074' '#1076#1077#1083' '#1086#1076' '#1080#1085#1090#1077#1088#1074#1072#1083#1086#1090')'
      FieldName = 'STEPEN_SLOZENOST'
      Size = 8
    end
    object tblGrupaRMSTEPEN_NA_SLOZENOST_2: TFIBBCDField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' ('#1042#1090#1086#1088' '#1076#1077#1083' '#1086#1076' '#1080#1085#1090#1077#1088#1074#1072#1083#1086#1090')'
      FieldName = 'STEPEN_NA_SLOZENOST_2'
      Size = 8
    end
  end
  object tblRabotnoMesto: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RABOTNO_MESTO'
      'SET '
      '    NAZIV = :NAZIV,'
      '    ZVANJE = :ZVANJE,'
      '    OPIS = :OPIS,'
      '    GRUPA_PLATA = :GRUPA_PLATA,'
      '    VID_VRABOTUVANJE = :VID_VRABOTUVANJE,'
      '    GRUPA = :GRUPA,'
      '    OBRAZOVANIE = :OBRAZOVANIE,'
      '    OBRAZOVANIE_OPIS = :OBRAZOVANIE_OPIS,'
      '    RABOTNO_ISKUSTVO = :RABOTNO_ISKUSTVO,'
      '    LOKACIJA_RM = :LOKACIJA_RM,'
      '    ID_SZ_OBRAZOVANIE = :ID_SZ_OBRAZOVANIE,'
      '    USLOVI = :USLOVI,'
      '    RABOTNO_VREME = :RABOTNO_VREME,'
      '    ID_TIP_RABOTNO_VREME = :ID_TIP_RABOTNO_VREME,'
      '    AKTIVNO = :AKTIVNO,'
      '    ODGOVARA_PRED = :ODGOVARA_PRED,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DEN_USLOVI_RABOTA = :DEN_USLOVI_RABOTA,'
      '    DENOVI_ODMOR = :DENOVI_ODMOR,'
      '    RAKOVODENJE = :RAKOVODENJE,'
      '    USLOVI_RABOTA = :USLOVI_RABOTA,'
      '    STEPEN_SLOZENOST = :STEPEN_SLOZENOST,'
      '    kategorija = :kategorija'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_RABOTNO_MESTO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_RABOTNO_MESTO('
      '    ID,'
      '    NAZIV,'
      '    ZVANJE,'
      '    OPIS,'
      '    GRUPA_PLATA,'
      '    VID_VRABOTUVANJE,'
      '    GRUPA,'
      '    OBRAZOVANIE,'
      '    OBRAZOVANIE_OPIS,'
      '    RABOTNO_ISKUSTVO,'
      '    LOKACIJA_RM,'
      '    ID_SZ_OBRAZOVANIE,'
      '    USLOVI,'
      '    RABOTNO_VREME,'
      '    ID_TIP_RABOTNO_VREME,'
      '    AKTIVNO,'
      '    ODGOVARA_PRED,'
      '    ID_RE_FIRMA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    DEN_USLOVI_RABOTA,'
      '    DENOVI_ODMOR,'
      '    RAKOVODENJE,'
      '    USLOVI_RABOTA,'
      '    STEPEN_SLOZENOST,'
      '    kategorija'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :ZVANJE,'
      '    :OPIS,'
      '    :GRUPA_PLATA,'
      '    :VID_VRABOTUVANJE,'
      '    :GRUPA,'
      '    :OBRAZOVANIE,'
      '    :OBRAZOVANIE_OPIS,'
      '    :RABOTNO_ISKUSTVO,'
      '    :LOKACIJA_RM,'
      '    :ID_SZ_OBRAZOVANIE,'
      '    :USLOVI,'
      '    :RABOTNO_VREME,'
      '    :ID_TIP_RABOTNO_VREME,'
      '    :AKTIVNO,'
      '    :ODGOVARA_PRED,'
      '    :ID_RE_FIRMA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :DEN_USLOVI_RABOTA,'
      '    :DENOVI_ODMOR,'
      '    :RAKOVODENJE,'
      '    :USLOVI_RABOTA,'
      '    :STEPEN_SLOZENOST,'
      '    :kategorija'
      ')')
    SelectSQL.Strings = (
      'select'
      '    rm.id,'
      '    rm.naziv,'
      '    rm.zvanje,'
      '    rm.opis,'
      '    rm.grupa_plata,'
      '    gp.naziv naziv_grupa_plata,'
      '    rm.vid_vrabotuvanje,'
      '    vv.naziv naziv_vid_vrabotuvanje,'
      '    rm.grupa,'
      '    grm.naziv naziv_grupa,'
      '    grm.oznaka as grupaOznaka,'
      '    rm.obrazovanie,'
      '    o.opis naziv_obrazovanie,'
      '    rm.obrazovanie_opis,'
      '    rm.rabotno_iskustvo,'
      '    rm.lokacija_rm,'
      '    rm.id_sz_obrazovanie,'
      '    --hso.naziv naziv_sz_obrazovanie,'
      '    hso.opis opis_sz_obrazovanie,'
      '    rm.uslovi,'
      '    rm.rabotno_vreme,'
      '    rm.id_tip_rabotno_vreme,'
      '    htrm.naziv tip_rabotno_vreme,'
      '    rm.aktivno,'
      
        '    (case when rm.aktivno=1 then '#39#1040#1082#1090#1080#1074#1085#1086' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086#39' else '#39#1053 +
        #1077' '#1072#1082#1090#1080#1074#1085#1086' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086#39' end) opis_aktivno,'
      '    rm.odgovara_pred,'
      '    rm.id_re_firma, '
      '   -- mr.naziv  rabotna_edinica,'
      '    rm.ts_ins,'
      '    rm.ts_upd,'
      '    rm.usr_ins,'
      '    rm.usr_upd,'
      '    rm.den_uslovi_rabota,'
      '    rm.denovi_odmor,'
      '    rm.rakovodenje,'
      '    rm.uslovi_rabota,'
      '    rm.stepen_slozenost,'
      '    hso.naziv szo_naziv,'
      '    rm.kategorija,'
      '    k.kategorija as kategorijaRM_naziv'
      'from hr_rabotno_mesto rm'
      'left outer join hr_grupa_rm grm on grm.id=rm.grupa'
      
        'left outer join hr_vid_vrabotuvanje vv on vv.id=rm.vid_vrabotuva' +
        'nje'
      'left outer join plt_obrazovanie o on o.id=rm.obrazovanie'
      'left outer join hr_grupa_plata gp on gp.id=rm.grupa_plata'
      
        'left outer join hr_sz_obrazovanie hso on hso.id=rm.id_sz_obrazov' +
        'anie'
      
        'left outer join hr_tip_rabotno_vreme htrm on htrm.id=rm.id_tip_r' +
        'abotno_vreme'
      'left outer join hr_kategorija_rm k on k.id = rm.kategorija'
      'inner join mat_re mr on mr.id=rm.id_re_firma'
      '--where mr.poteklo like :firma||'#39',%'#39
      
        'where (((mr.poteklo like mr.koren ||'#39','#39'||:firma||'#39','#39')and (not mr' +
        '.poteklo like :firma||'#39','#39')) or'
      '     (mr.poteklo like :firma||'#39',%'#39'))'
      'order by rm.id'
      ''
      ''
      '')
    AutoUpdateOptions.UpdateTableName = 'HR_RABOTNO_MESTO'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RABOTNO_MESTO_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 131
    oRefreshDeletedRecord = True
    object tblRabotnoMestoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRabotnoMestoNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoZVANJE: TFIBStringField
      DisplayLabel = #1047#1074#1072#1114#1077
      FieldName = 'ZVANJE'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoGRUPA_PLATA: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' '#1085#1072' '#1087#1083#1072#1090#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'GRUPA_PLATA'
    end
    object tblRabotnoMestoNAZIV_GRUPA_PLATA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' '#1085#1072' '#1087#1083#1072#1090#1072
      FieldName = 'NAZIV_GRUPA_PLATA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoVID_VRABOTUVANJE: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'VID_VRABOTUVANJE'
    end
    object tblRabotnoMestoNAZIV_VID_VRABOTUVANJE: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      FieldName = 'NAZIV_VID_VRABOTUVANJE'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoGRUPA: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'GRUPA'
    end
    object tblRabotnoMestoNAZIV_GRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'NAZIV_GRUPA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoOBRAZOVANIE: TFIBSmallIntField
      DisplayLabel = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'OBRAZOVANIE'
    end
    object tblRabotnoMestoOBRAZOVANIE_OPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'OBRAZOVANIE_OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoUSLOVI: TFIBStringField
      DisplayLabel = #1059#1089#1083#1086#1074#1080
      FieldName = 'USLOVI'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoAKTIVNO: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1085#1086
      FieldName = 'AKTIVNO'
    end
    object tblRabotnoMestoNAZIV_OBRAZOVANIE: TFIBStringField
      DisplayLabel = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'NAZIV_OBRAZOVANIE'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoOPIS_AKTIVNO: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1072#1082#1090#1080#1074#1085#1086#1089#1090
      FieldName = 'OPIS_AKTIVNO'
      Size = 24
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblRabotnoMestoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblRabotnoMestoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoODGOVARA_PRED: TFIBStringField
      DisplayLabel = #1054#1076#1075#1086#1074#1086#1088#1077#1085' '#1087#1088#1077#1076
      FieldName = 'ODGOVARA_PRED'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoRABOTNO_ISKUSTVO: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086
      FieldName = 'RABOTNO_ISKUSTVO'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoLOKACIJA_RM: TFIBStringField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1056#1052
      FieldName = 'LOKACIJA_RM'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoID_SZ_OBRAZOVANIE: TFIBIntegerField
      DisplayLabel = #1057#1090#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_SZ_OBRAZOVANIE'
    end
    object tblRabotnoMestoID_TIP_RABOTNO_VREME: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_TIP_RABOTNO_VREME'
    end
    object tblRabotnoMestoOPIS_SZ_OBRAZOVANIE: TFIBStringField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'OPIS_SZ_OBRAZOVANIE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoTIP_RABOTNO_VREME: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077
      FieldName = 'TIP_RABOTNO_VREME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoID_RE_FIRMA: TFIBIntegerField
      DisplayLabel = #1060#1080#1088#1084#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_RE_FIRMA'
    end
    object tblRabotnoMestoDEN_USLOVI_RABOTA: TFIBIntegerField
      DisplayLabel = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054' '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072' '
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblRabotnoMestoDENOVI_ODMOR: TFIBIntegerField
      DisplayLabel = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054
      FieldName = 'DENOVI_ODMOR'
    end
    object tblRabotnoMestoRAKOVODENJE: TFIBBCDField
      DisplayLabel = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1088#1080#1084#1072#1114#1072
      FieldName = 'RAKOVODENJE'
      Size = 8
    end
    object tblRabotnoMestoUSLOVI_RABOTA: TFIBBCDField
      DisplayLabel = #1059#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072
      FieldName = 'USLOVI_RABOTA'
      Size = 8
    end
    object tblRabotnoMestoGRUPAOZNAKA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' ('#1086#1079#1085#1072#1082#1072')'
      FieldName = 'GRUPAOZNAKA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoRABOTNO_VREME: TFIBBCDField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077
      FieldName = 'RABOTNO_VREME'
      Size = 2
    end
    object tblRabotnoMestoSTEPEN_SLOZENOST: TFIBBCDField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
      FieldName = 'STEPEN_SLOZENOST'
      Size = 8
    end
    object tblRabotnoMestoSZO_NAZIV: TFIBStringField
      FieldName = 'SZO_NAZIV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoMestoKATEGORIJA: TFIBIntegerField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'KATEGORIJA'
    end
    object tblRabotnoMestoKATEGORIJARM_NAZIV: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'KATEGORIJARM_NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblGrupaPlata: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_GRUPA_PLATA'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    VALUTA = :VALUTA,'
      '    MINIMALNA = :MINIMALNA,'
      '    MAKSIMALNA = :MAKSIMALNA,'
      '    STAPKA = :STAPKA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_GRUPA_PLATA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_GRUPA_PLATA('
      '    ID,'
      '    NAZIV,'
      '    VALUTA,'
      '    MINIMALNA,'
      '    MAKSIMALNA,'
      '    STAPKA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :VALUTA,'
      '    :MINIMALNA,'
      '    :MAKSIMALNA,'
      '    :STAPKA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    gp.id,'
      '    gp.naziv,'
      '    gp.valuta,'
      '    gp.minimalna,'
      '    gp.maksimalna,'
      '    gp.stapka,'
      '    gp.ts_ins,'
      '    gp.ts_upd,'
      '    gp.usr_ins,'
      '    gp.usr_upd'
      'from hr_grupa_plata gp'
      ' WHERE '
      '        GP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    gp.id,'
      '    gp.naziv,'
      '    gp.valuta,'
      '    gp.minimalna,'
      '    gp.maksimalna,'
      '    gp.stapka,'
      '    gp.ts_ins,'
      '    gp.ts_upd,'
      '    gp.usr_ins,'
      '    gp.usr_upd'
      'from hr_grupa_plata gp')
    AutoUpdateOptions.UpdateTableName = 'HR_GRUPA_PLATA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_GRUPA_PLATA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 192
    oRefreshDeletedRecord = True
    object tblGrupaPlataID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblGrupaPlataNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaPlataVALUTA: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaPlataMINIMALNA: TFIBBCDField
      DisplayLabel = #1052#1080#1085#1080#1084#1072#1083#1085#1072
      FieldName = 'MINIMALNA'
      Size = 2
    end
    object tblGrupaPlataMAKSIMALNA: TFIBBCDField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1072
      FieldName = 'MAKSIMALNA'
      Size = 2
    end
    object tblGrupaPlataSTAPKA: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1087#1082#1072
      FieldName = 'STAPKA'
      Size = 2
    end
    object tblGrupaPlataTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblGrupaPlataTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblGrupaPlataUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaPlataUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsVidVrabotuvanje: TDataSource
    DataSet = tblVidVrabotuvanje
    Left = 115
    Top = 16
  end
  object dsGrupaRM: TDataSource
    DataSet = tblGrupaRM
    Left = 115
    Top = 71
  end
  object dsRabotnoMesto: TDataSource
    DataSet = tblRabotnoMesto
    Left = 115
    Top = 131
  end
  object dsGrupaPlata: TDataSource
    DataSet = tblGrupaPlata
    Left = 115
    Top = 192
  end
  object tblObrazovanie: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PLT_OBRAZOVANIE'
      'SET '
      '    OPIS = :OPIS,'
      '    ID_GRUPA_RM = :ID_GRUPA_RM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PLT_OBRAZOVANIE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PLT_OBRAZOVANIE('
      '    ID,'
      '    OPIS,'
      '    ID_GRUPA_RM'
      ')'
      'VALUES('
      '    :ID,'
      '    :OPIS,'
      '    :ID_GRUPA_RM'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    po.ID,'
      '    po.OPIS,'
      '    po.id_grupa_rm,'
      '    hgr.oznaka oznaka_grupa,'
      '    hgr.naziv naziv_grupa'
      'FROM'
      '    PLT_OBRAZOVANIE po '
      '    left outer join hr_grupa_rm hgr on hgr.id=po.id_grupa_rm'
      ''
      ' WHERE '
      '        PO.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    po.ID,'
      '    po.OPIS,'
      '    po.id_grupa_rm,'
      '    hgr.oznaka oznaka_grupa,'
      '    hgr.naziv naziv_grupa'
      'FROM'
      '    PLT_OBRAZOVANIE po '
      '    left outer join hr_grupa_rm hgr on hgr.id=po.id_grupa_rm'
      'order by po.OPIS')
    BeforeDelete = AllTableBeforeDelete
    OnNewRecord = tblObrazovanieNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 256
    oRefreshDeletedRecord = True
    object tblObrazovanieID: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblObrazovanieOPIS: TFIBStringField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
      FieldName = 'OPIS'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieID_GRUPA_RM: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_GRUPA_RM'
    end
    object tblObrazovanieOZNAKA_GRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
      FieldName = 'OZNAKA_GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieNAZIV_GRUPA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1075#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
      FieldName = 'NAZIV_GRUPA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsObrazovanie: TDataSource
    DataSet = tblObrazovanie
    Left = 115
    Top = 256
  end
  object tblSistematizacija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_SISTEMATIZACIJA'
      'SET '
      '    ID = :ID,'
      '    OPIS = :OPIS,'
      '    OD_DATUM = :OD_DATUM,'
      '    DO_DATUM = :DO_DATUM,'
      '    DOKUMENT = :DOKUMENT,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_SISTEMATIZACIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_SISTEMATIZACIJA('
      '    ID,'
      '    OPIS,'
      '    OD_DATUM,'
      '    DO_DATUM,'
      '    DOKUMENT,'
      '    ID_RE_FIRMA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OPIS,'
      '    :OD_DATUM,'
      '    :DO_DATUM,'
      '    :DOKUMENT,'
      '    :ID_RE_FIRMA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.id_re_firma,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd'
      'from hr_sistematizacija hs'
      'where(  hs.id_re_firma=:firma'
      '     ) and (     HS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.id_re_firma,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd'
      'from hr_sistematizacija hs'
      'where hs.id_re_firma=:firma')
    AutoUpdateOptions.UpdateTableName = 'HR_SISTEMATIZACIJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_SISTEMATIZACIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 299
    oRefreshDeletedRecord = True
    object tblSistematizacijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSistematizacijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaOD_DATUM: TFIBDateField
      DisplayLabel = #1054#1076' '#1076#1072#1090#1091#1084
      FieldName = 'OD_DATUM'
    end
    object tblSistematizacijaDO_DATUM: TFIBDateField
      DisplayLabel = #1044#1086' '#1076#1072#1090#1091#1084
      FieldName = 'DO_DATUM'
    end
    object tblSistematizacijaDOKUMENT: TFIBStringField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'DOKUMENT'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblSistematizacijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblSistematizacijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaID_RE_FIRMA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1092#1086#1088#1084#1072
      FieldName = 'ID_RE_FIRMA'
    end
  end
  object dsSitematizacija: TDataSource
    DataSet = tblSistematizacija
    Left = 115
    Top = 298
  end
  object tblRMRE: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RM_RE'
      'SET '
      '    BROJ = :BROJ,'
      '    ID_RE = :ID_RE,'
      '    ID_SISTEMATIZACIJA = :ID_SISTEMATIZACIJA,'
      '    ID_RM = :ID_RM,'
      '    BROJ_IZVRSITELI = :BROJ_IZVRSITELI,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    RAKOVODENJE = :RAKOVODENJE,'
      '    DENOVI_ODMOR = :DENOVI_ODMOR,'
      '    DEN_USLOVI_RABOTA = :DEN_USLOVI_RABOTA,'
      '    USLOVI_RABOTA = :USLOVI_RABOTA,'
      '    ODGOVARA_PRED = :ODGOVARA_PRED,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    BENEFICIRAN_STAZ = :BENEFICIRAN_STAZ'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_RM_RE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_RM_RE('
      '    BROJ,'
      '    ID,'
      '    ID_RE,'
      '    ID_SISTEMATIZACIJA,'
      '    ID_RM,'
      '    BROJ_IZVRSITELI,'
      '    ID_RE_FIRMA,'
      '    RAKOVODENJE,'
      '    DENOVI_ODMOR,'
      '    DEN_USLOVI_RABOTA,'
      '    USLOVI_RABOTA,'
      '    ODGOVARA_PRED,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    BENEFICIRAN_STAZ'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :ID,'
      '    :ID_RE,'
      '    :ID_SISTEMATIZACIJA,'
      '    :ID_RM,'
      '    :BROJ_IZVRSITELI,'
      '    :ID_RE_FIRMA,'
      '    :RAKOVODENJE,'
      '    :DENOVI_ODMOR,'
      '    :DEN_USLOVI_RABOTA,'
      '    :USLOVI_RABOTA,'
      '    :ODGOVARA_PRED,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :BENEFICIRAN_STAZ'
      ')')
    RefreshSQL.Strings = (
      'select mr.koren,'
      '    hrr.broj,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.id_re_firma,'
      '    hrr.rakovodenje,'
      '    hrr.denovi_odmor,'
      '    hrr.den_uslovi_rabota,'
      '    hrr.uslovi_rabota,'
      '    hrr.odgovara_pred,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hrk.naziv as uslovi_rabota_naziv,'
      '    hrk.procent as uslovi_rabota_procent,'
      '    hrr.beneficiran_staz'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hrr.uslo' +
        'vi_rabota'
      
        'where(  (((mr.poteklo||'#39'%'#39' like  '#39'%,'#39'||:firma ||'#39',%'#39')and (not mr' +
        '.poteklo like :firma||'#39','#39')) or'
      '      (mr.poteklo like :firma||'#39',%'#39')) and'
      
        '      (((:param = 1) and (current_date between hs.od_datum and c' +
        'oalesce(hs.do_datum, current_date)))'
      '       or (:param = 0))'
      '      --mr.poteklo like :firma||'#39',%'#39' and'
      
        '      --(((:param = 1) and (current_date between hs.od_datum and' +
        ' coalesce(hs.do_datum, current_date)))'
      '       --or (:param = 0))'
      '     ) and (     HRR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.broj,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.id_re_firma,'
      '    hrr.rakovodenje,'
      '    hrr.denovi_odmor,'
      '    hrr.den_uslovi_rabota,'
      '    hrr.uslovi_rabota,'
      '    hrr.odgovara_pred,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hrk.naziv as uslovi_rabota_naziv,'
      '    hrk.procent as uslovi_rabota_procent,'
      '    hrr.beneficiran_staz'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hrr.uslo' +
        'vi_rabota'
      
        'where (((mr.poteklo||'#39'%'#39' like  '#39'%,'#39'||:firma ||'#39',%'#39')and (not mr.p' +
        'oteklo like :firma||'#39','#39')) or'
      '      (mr.poteklo like :firma||'#39',%'#39')) and'
      
        '      (((:param = 1) and (current_date between hs.od_datum and c' +
        'oalesce(hs.do_datum, current_date)))'
      '       or (:param = 0))'
      '      --mr.poteklo like :firma||'#39',%'#39' and'
      
        '      --(((:param = 1) and (current_date between hs.od_datum and' +
        ' coalesce(hs.do_datum, current_date)))'
      '       --or (:param = 0))')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 355
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      DisplayLabel = #1050#1086#1088#1077#1085
      FieldName = 'KOREN'
    end
    object tblRMREBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblRMREID_RE_FIRMA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1092#1080#1088#1084#1072
      FieldName = 'ID_RE_FIRMA'
    end
    object tblRMRERAKOVODENJE: TFIBBCDField
      FieldName = 'RAKOVODENJE'
      Size = 2
    end
    object tblRMREDENOVI_ODMOR: TFIBIntegerField
      FieldName = 'DENOVI_ODMOR'
    end
    object tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblRMREODGOVARA_PRED: TFIBStringField
      FieldName = 'ODGOVARA_PRED'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSLOVI_RABOTA_NAZIV: TFIBStringField
      DisplayLabel = #1059#1089#1083#1086#1074#1080' '#1087#1086#1090#1077#1096#1082#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1083#1085#1080#1090#1077
      FieldName = 'USLOVI_RABOTA_NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSLOVI_RABOTA_PROCENT: TFIBBCDField
      DisplayLabel = #1059#1089#1083#1086#1074#1080' '#1087#1086#1090#1077#1096#1082#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1083#1085#1080#1090#1077' ('#1087#1088#1086#1094#1077#1085#1090') '
      FieldName = 'USLOVI_RABOTA_PROCENT'
      Size = 2
    end
    object tblRMREUSLOVI_RABOTA: TFIBIntegerField
      DisplayLabel = #1059#1089#1083#1086#1074#1080' '#1087#1086#1090#1077#1096#1082#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1083#1085#1080#1090#1077' (ID)'
      FieldName = 'USLOVI_RABOTA'
    end
    object tblRMREBENEFICIRAN_STAZ: TFIBBCDField
      DisplayLabel = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078
      FieldName = 'BENEFICIRAN_STAZ'
      Size = 2
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 115
    Top = 355
  end
  object qMaxObrazovanie: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select max(coalesce(po.id,0))+1 maks'
      'from plt_obrazovanie po')
    Left = 672
    Top = 16
  end
  object tblDrzavjanstvo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_DRZAVJANSTVO'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_DRZAVJANSTVO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_DRZAVJANSTVO('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    d.id,'
      '    d.naziv,'
      '    d.ts_ins,'
      '    d.ts_upd,'
      '    d.usr_ins,'
      '    d.usr_upd'
      'from'
      '    hr_drzavjanstvo d'
      ''
      ' WHERE '
      '        D.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    d.id,'
      '    d.naziv,'
      '    d.ts_ins,'
      '    d.ts_upd,'
      '    d.usr_ins,'
      '    d.usr_upd'
      'from'
      '    hr_drzavjanstvo d')
    AutoUpdateOptions.UpdateTableName = 'HR_DRZAVJANSTVO'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_DRZAVJANSTVO_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 411
    oRefreshDeletedRecord = True
    object tblDrzavjanstvoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDrzavjanstvoNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDrzavjanstvoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblDrzavjanstvoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblDrzavjanstvoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDrzavjanstvoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDrzavjanstvo: TDataSource
    DataSet = tblDrzavjanstvo
    Left = 115
    Top = 411
  end
  object tblStranskiJazici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_VID_STRANSKI_JAZIK'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_VID_STRANSKI_JAZIK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_VID_STRANSKI_JAZIK('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    vsj.id,'
      '    vsj.naziv,'
      '    vsj.ts_ins,'
      '    vsj.ts_upd,'
      '    vsj.usr_ins,'
      '    vsj.usr_upd'
      'from'
      '    hr_vid_stranski_jazik vsj'
      ' WHERE '
      '        VSJ.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    vsj.id,'
      '    vsj.naziv,'
      '    vsj.ts_ins,'
      '    vsj.ts_upd,'
      '    vsj.usr_ins,'
      '    vsj.usr_upd'
      'from'
      '    hr_vid_stranski_jazik vsj')
    AutoUpdateOptions.UpdateTableName = 'HR_VID_STRANSKI_JAZIK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_VID_STRANSKI_JAZIK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 475
    oRefreshDeletedRecord = True
    object tblStranskiJaziciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStranskiJaziciNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStranskiJaziciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblStranskiJaziciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblStranskiJaziciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStranskiJaziciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStranskiJazici: TDataSource
    DataSet = tblStranskiJazici
    Left = 115
    Top = 475
  end
  object tblOglas: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_EVIDENCIJA_OGLAS'
      'SET '
      '    ID = :ID,'
      '    BROJ = :BROJ,'
      '    DATUM_OBJAVEN = :DATUM_OBJAVEN,'
      '    DATUM_ISTEKUVA = :DATUM_ISTEKUVA,'
      '    OBJAVEN = :OBJAVEN,'
      '    PRIJAVUVANJE_DO = :PRIJAVUVANJE_DO,'
      '    OPIS = :OPIS,'
      '    STATUS = :STATUS,'
      '    PROMENET_OD = :PROMENET_OD,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_EVIDENCIJA_OGLAS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_EVIDENCIJA_OGLAS('
      '    ID,'
      '    BROJ,'
      '    DATUM_OBJAVEN,'
      '    DATUM_ISTEKUVA,'
      '    OBJAVEN,'
      '    PRIJAVUVANJE_DO,'
      '    OPIS,'
      '    STATUS,'
      '    PROMENET_OD,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    ID_RE_FIRMA'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :DATUM_OBJAVEN,'
      '    :DATUM_ISTEKUVA,'
      '    :OBJAVEN,'
      '    :PRIJAVUVANJE_DO,'
      '    :OPIS,'
      '    :STATUS,'
      '    :PROMENET_OD,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :ID_RE_FIRMA'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    og.id,'
      '    og.broj,'
      '    og.datum_objaven,'
      '    og.datum_istekuva,'
      '    og.objaven,'
      '    og.prijavuvanje_do,'
      '    og.opis,'
      '    og.status,'
      '    case og.status when '#39'1'#39' then '#39#1040#1082#1090#1080#1074#1077#1085#39
      '                   else '#39#1053#1077#1072#1082#1090#1080#1074#1077#1085#39
      '    end "statusNaziv",'
      '    og.promenet_od,'
      '    og.ts_ins,'
      '    og.ts_upd,'
      '    og.usr_ins,'
      '    og.usr_upd,'
      
        '    og.broj || '#39'/'#39' || extractyear(og.datum_objaven) as BrojGodin' +
        'a,'
      '    extractyear(og.datum_objaven) as Godina,'
      '    og.id_re_firma'
      'from hr_evidencija_oglas og'
      'where(  id like '#39'%'#39' --and og.id_re_firma like :firma'
      '     ) and (     OG.ID = :OLD_ID'
      '     )'
      
        'order by og.datum_objaven, og.prijavuvanje_do,og.datum_istekuva ' +
        '   ')
    SelectSQL.Strings = (
      'select'
      '    og.id,'
      '    og.broj,'
      '    og.datum_objaven,'
      '    og.datum_istekuva,'
      '    og.objaven,'
      '    og.prijavuvanje_do,'
      '    og.opis,'
      '    og.status,'
      '    case og.status when '#39'1'#39' then '#39#1040#1082#1090#1080#1074#1077#1085#39
      '                   else '#39#1053#1077#1072#1082#1090#1080#1074#1077#1085#39
      '    end "statusNaziv",'
      '    og.promenet_od,'
      '    og.ts_ins,'
      '    og.ts_upd,'
      '    og.usr_ins,'
      '    og.usr_upd,'
      
        '    og.broj || '#39'/'#39' || extractyear(og.datum_objaven) as BrojGodin' +
        'a,'
      '    extractyear(og.datum_objaven) as Godina,'
      '    og.id_re_firma'
      'from hr_evidencija_oglas og'
      'where id like '#39'%'#39' --and og.id_re_firma like :firma'
      'order by og.datum_objaven, og.prijavuvanje_do,og.datum_istekuva ')
    AutoUpdateOptions.UpdateTableName = 'HR_EVIDENCIJA_OGLAS'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_EVIDENCIJA_OGLAS_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 224
    Top = 15
    oRefreshDeletedRecord = True
    object tblOglasID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOglasDATUM_ISTEKUVA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1090#1077#1082#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ISTEKUVA'
    end
    object tblOglasOBJAVEN: TFIBStringField
      DisplayLabel = #1054#1073#1112#1072#1074#1077#1085' '#1074#1086'...'
      FieldName = 'OBJAVEN'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasPRIJAVUVANJE_DO: TFIBDateField
      DisplayLabel = #1055#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077' '#1076#1086
      FieldName = 'PRIJAVUVANJE_DO'
    end
    object tblOglasOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblOglasPROMENET_OD: TFIBStringField
      DisplayLabel = #1055#1088#1086#1084#1077#1085#1077#1090' '#1086#1076
      FieldName = 'PROMENET_OD'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblOglasTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblOglasBROJGODINA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112'/'#1043#1086#1076#1080#1085#1072
      FieldName = 'BROJGODINA'
      Size = 32
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblOglasstatusNaziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'statusNaziv'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasID_RE_FIRMA: TFIBIntegerField
      DisplayLabel = #1060#1080#1088#1084#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_RE_FIRMA'
    end
    object tblOglasUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOglasDATUM_OBJAVEN: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1073#1112#1072#1074#1091#1074#1072#1114#1077
      FieldName = 'DATUM_OBJAVEN'
    end
    object tblOglasBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsOglas: TDataSource
    DataSet = tblOglas
    Left = 299
    Top = 15
  end
  object tblRabotnoIskustvo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RABOTNO_ISKUSTVO'
      'SET '
      '    ID_MOLBA = :ID_MOLBA,'
      '    MB_VRABOTEN = :MB_VRABOTEN,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    RABOTODAVAC = :RABOTODAVAC,'
      '    VID_DEJNOST = :VID_DEJNOST,'
      '    RABOTNO_MESTO = :RABOTNO_MESTO,'
      '    ZADACI = :ZADACI,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_RABOTNO_ISKUSTVO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_RABOTNO_ISKUSTVO('
      '    ID,'
      '    ID_MOLBA,'
      '    MB_VRABOTEN,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    RABOTODAVAC,'
      '    VID_DEJNOST,'
      '    RABOTNO_MESTO,'
      '    ZADACI,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_MOLBA,'
      '    :MB_VRABOTEN,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :RABOTODAVAC,'
      '    :VID_DEJNOST,'
      '    :RABOTNO_MESTO,'
      '    :ZADACI,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hri.id,'
      '       hri.id_molba,'
      '       hri.mb_vraboten,'
      '       hri.datum_od,'
      '       hri.datum_do,'
      '       hri.rabotodavac,'
      '       hri.vid_dejnost,'
      '       hri.rabotno_mesto,'
      '       hri.zadaci,'
      '       hri.ts_ins,'
      '       hri.ts_upd,'
      '       hri.usr_ins,'
      '       hri.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrm.naziv_vraboten as ImePrezime,'
      
        '       coalesce(hri.datum_od,'#39' '#39') || '#39' - '#39'||coalesce(hri.datum_d' +
        'o,'#39' '#39') as period'
      'from hr_rabotno_iskustvo hri'
      'left outer join hr_evidencija_molba hrm on hrm.id = hri.id_molba'
      'left outer join hr_vraboten hrv on hrv.mb=hri.mb_vraboten'
      
        'where(  coalesce(hri.mb_vraboten, 0) like :MB and coalesce(hri.i' +
        'd_molba, 0) like :MolbaID'
      '     ) and (     HRI.ID = :OLD_ID'
      '     )'
      
        'order by  hri.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n   ')
    SelectSQL.Strings = (
      'select hri.id,'
      '       hri.id_molba,'
      '       hri.mb_vraboten,'
      '       hri.datum_od,'
      '       hri.datum_do,'
      '       hri.rabotodavac,'
      '       hri.vid_dejnost,'
      '       hri.rabotno_mesto,'
      '       hri.zadaci,'
      '       hri.ts_ins,'
      '       hri.ts_upd,'
      '       hri.usr_ins,'
      '       hri.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrm.naziv_vraboten as ImePrezime,'
      
        '       coalesce(hri.datum_od,'#39' '#39') || '#39' - '#39'||coalesce(hri.datum_d' +
        'o,'#39' '#39') as period'
      'from hr_rabotno_iskustvo hri'
      'left outer join hr_evidencija_molba hrm on hrm.id = hri.id_molba'
      'left outer join hr_vraboten hrv on hrv.mb=hri.mb_vraboten'
      
        'where coalesce(hri.mb_vraboten, 0) like :MB and coalesce(hri.id_' +
        'molba, 0) like :MolbaID'
      
        'order by  hri.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n'
      '')
    AutoUpdateOptions.UpdateTableName = 'HR_RABOTNO_ISKUSTVO'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RABOTNO_ISKUSTVO_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 16
    object tblRabotnoIskustvoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRabotnoIskustvoID_MOLBA: TFIBIntegerField
      DisplayLabel = 'M'#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblRabotnoIskustvoMB_VRABOTEN: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB_VRABOTEN'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoDATUM_OD: TFIBDateField
      DisplayLabel = #1054#1076
      FieldName = 'DATUM_OD'
    end
    object tblRabotnoIskustvoDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1086
      FieldName = 'DATUM_DO'
    end
    object tblRabotnoIskustvoRABOTODAVAC: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1086#1076#1072#1074#1072#1095
      FieldName = 'RABOTODAVAC'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoVID_DEJNOST: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1077#1112#1085#1086#1089#1090
      FieldName = 'VID_DEJNOST'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoRABOTNO_MESTO: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNO_MESTO'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoZADACI: TFIBStringField
      DisplayLabel = #1047#1072#1076#1072#1095#1080
      FieldName = 'ZADACI'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblRabotnoIskustvoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblRabotnoIskustvoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoIMEPREZIME: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'IMEPREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoPERIOD: TFIBStringField
      DisplayLabel = #1055#1077#1088#1080#1086#1076
      FieldName = 'PERIOD'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRabotnoIskustvoNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRabotnoIskustvo: TDataSource
    DataSet = tblRabotnoIskustvo
    Left = 523
    Top = 16
  end
  object tblObrazovanieObuka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_OBRAZOVANIE_OBUKA'
      'SET '
      '    ID_MOLBA = :ID_MOLBA,'
      '    MB_VRABOTEN = :MB_VRABOTEN,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    KVALIFIKACIJA = :KVALIFIKACIJA,'
      '    ZNAENJE = :ZNAENJE,'
      '    IME_ORGANIZACIJA = :IME_ORGANIZACIJA,'
      '    NASOKA = :NASOKA,'
      '    NIVO = :NIVO,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    STEPEN_OBRAZOVANIE = :STEPEN_OBRAZOVANIE'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_OBRAZOVANIE_OBUKA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_OBRAZOVANIE_OBUKA('
      '    ID,'
      '    ID_MOLBA,'
      '    MB_VRABOTEN,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    KVALIFIKACIJA,'
      '    ZNAENJE,'
      '    IME_ORGANIZACIJA,'
      '    NASOKA,'
      '    NIVO,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    STEPEN_OBRAZOVANIE'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_MOLBA,'
      '    :MB_VRABOTEN,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :KVALIFIKACIJA,'
      '    :ZNAENJE,'
      '    :IME_ORGANIZACIJA,'
      '    :NASOKA,'
      '    :NIVO,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :STEPEN_OBRAZOVANIE'
      ')')
    RefreshSQL.Strings = (
      'select  hro.id,'
      '        hro.id_molba,'
      '        hro.mb_vraboten,'
      '        hro.datum_od,'
      '        hro.datum_do,'
      '        hro.kvalifikacija,'
      '        hro.znaenje,'
      '        hro.ime_organizacija,'
      '        hro.nasoka,'
      '        hro.nivo,'
      '        hro.ts_ins,'
      '        hro.ts_upd,'
      '        hro.usr_ins,'
      '        hro.usr_upd,'
      '        hrv.naziv_vraboten_ti,'
      '        hrv.naziv_vraboten,'
      '        hrm.naziv_vraboten ImePrezime,'
      
        '        coalesce(hro.datum_od,'#39' '#39') || '#39' - '#39'||coalesce(hro.datum_' +
        'do,'#39' '#39') as period,'
      '        hri.naziv as OINaziv,'
      '        hrn.naziv as NasokaNaziv,'
      '        hro.stepen_obrazovanie,'
      '        hszo.naziv as SZONaziv'
      'from hr_obrazovanie_obuka hro'
      'left outer join hr_evidencija_molba hrm on hrm.id = hro.id_molba'
      'left outer join hr_vraboten hrv on hrv.mb=hro.mb_vraboten'
      
        'left outer join hr_obrazovna_institucija hri on hri.id = hro.ime' +
        '_organizacija'
      'left outer join hr_nasoka_obrazovanie hrn on hrn.id = hro.nasoka'
      
        'left outer join hr_sz_obrazovanie hszo on hszo.id = hro.stepen_o' +
        'brazovanie'
      
        'where(  coalesce(hro.mb_vraboten, 0) like :MB and coalesce(hro.i' +
        'd_molba, 0) like :MolbaID'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      
        ' order by  hro.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabot' +
        'en   ')
    SelectSQL.Strings = (
      'select  hro.id,'
      '        hro.id_molba,'
      '        hro.mb_vraboten,'
      '        hro.datum_od,'
      '        hro.datum_do,'
      '        hro.kvalifikacija,'
      '        hro.znaenje,'
      '        hro.ime_organizacija,'
      '        hro.nasoka,'
      '        hro.nivo,'
      '        hro.ts_ins,'
      '        hro.ts_upd,'
      '        hro.usr_ins,'
      '        hro.usr_upd,'
      '        hrv.naziv_vraboten_ti,'
      '        hrv.naziv_vraboten,'
      '        hrm.naziv_vraboten ImePrezime,'
      
        '        coalesce(hro.datum_od,'#39' '#39') || '#39' - '#39'||coalesce(hro.datum_' +
        'do,'#39' '#39') as period,'
      '        hri.naziv as OINaziv,'
      '        hrn.naziv as NasokaNaziv,'
      '        hro.stepen_obrazovanie,'
      '        hszo.naziv as SZONaziv'
      'from hr_obrazovanie_obuka hro'
      'left outer join hr_evidencija_molba hrm on hrm.id = hro.id_molba'
      'left outer join hr_vraboten hrv on hrv.mb=hro.mb_vraboten'
      
        'left outer join hr_obrazovna_institucija hri on hri.id = hro.ime' +
        '_organizacija'
      'left outer join hr_nasoka_obrazovanie hrn on hrn.id = hro.nasoka'
      
        'left outer join hr_sz_obrazovanie hszo on hszo.id = hro.stepen_o' +
        'brazovanie'
      
        'where coalesce(hro.mb_vraboten, 0) like :MB and coalesce(hro.id_' +
        'molba, 0) like :MolbaID'
      
        'order by  hro.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n')
    AutoUpdateOptions.UpdateTableName = 'HR_OBRAZOVANIE_OBUKA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_OBRAZOVANIE_OBUKA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 72
    object tblObrazovanieObukaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblObrazovanieObukaID_MOLBA: TFIBIntegerField
      DisplayLabel = #1052#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblObrazovanieObukaMB_VRABOTEN: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1041#1088'.'
      FieldName = 'MB_VRABOTEN'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaDATUM_OD: TFIBDateField
      DisplayLabel = #1054#1076
      FieldName = 'DATUM_OD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tblObrazovanieObukaDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1086
      FieldName = 'DATUM_DO'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object tblObrazovanieObukaKVALIFIKACIJA: TFIBStringField
      DisplayLabel = #1050#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1112#1072
      FieldName = 'KVALIFIKACIJA'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaZNAENJE: TFIBStringField
      DisplayLabel = #1047#1085#1072#1077#1114#1077
      FieldName = 'ZNAENJE'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaNIVO: TFIBStringField
      DisplayLabel = #1053#1080#1074#1086
      FieldName = 'NIVO'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblObrazovanieObukaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblObrazovanieObukaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaPERIOD: TFIBStringField
      DisplayLabel = #1055#1077#1088#1080#1086#1076
      FieldName = 'PERIOD'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaIME_ORGANIZACIJA: TFIBIntegerField
      DisplayLabel = #1054#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072
      FieldName = 'IME_ORGANIZACIJA'
    end
    object tblObrazovanieObukaNASOKA: TFIBIntegerField
      DisplayLabel = #1053#1072#1089#1086#1082#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'NASOKA'
    end
    object tblObrazovanieObukaOINAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072
      FieldName = 'OINAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaNASOKANAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1089#1086#1082#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'NASOKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaSTEPEN_OBRAZOVANIE: TFIBIntegerField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'STEPEN_OBRAZOVANIE'
    end
    object tblObrazovanieObukaSZONAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'SZONAZIV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObrazovanieObukaIMEPREZIME: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'IMEPREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsObrazovanieObuka: TDataSource
    DataSet = tblObrazovanieObuka
    Left = 531
    Top = 72
  end
  object tblMolba: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_EVIDENCIJA_MOLBA'
      'SET '
      '    NAZIV_VRABOTEN = :IMEPREZIME,'
      '    MB = :MB,'
      '    ID_OGLAS = :ID_OGLAS,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    MOMINSKO_PREZIME = :MOMINSKO_PREZIME,'
      '    DATUM_RADJANJE = :DATUM_RADJANJE,'
      '    ZDR_SOSTOJBA = :ZDR_SOSTOJBA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    SLIKA = :SLIKA,'
      '    ID_NACIONALNOST = :ID_NACIONALNOST,'
      '    ID_VEROISPOVED = :ID_VEROISPOVED,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    BRACNA_SOSTOJBA = :BRACNA_SOSTOJBA,'
      '    POL = :POL,'
      '    BROJ = :MOLBABROJ,'
      '    DATUM = :DATUM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_EVIDENCIJA_MOLBA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_EVIDENCIJA_MOLBA('
      '    ID,'
      '    NAZIV_VRABOTEN,'
      '    MB,'
      '    ID_OGLAS,'
      '    ID_RM_RE,'
      '    MOMINSKO_PREZIME,'
      '    DATUM_RADJANJE,'
      '    ZDR_SOSTOJBA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    SLIKA,'
      '    ID_NACIONALNOST,'
      '    ID_VEROISPOVED,'
      '    IME,'
      '    PREZIME,'
      '    BRACNA_SOSTOJBA,'
      '    POL,'
      '    BROJ,'
      '    DATUM'
      ')'
      'VALUES('
      '    :ID,'
      '    :IMEPREZIME,'
      '    :MB,'
      '    :ID_OGLAS,'
      '    :ID_RM_RE,'
      '    :MOMINSKO_PREZIME,'
      '    :DATUM_RADJANJE,'
      '    :ZDR_SOSTOJBA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :SLIKA,'
      '    :ID_NACIONALNOST,'
      '    :ID_VEROISPOVED,'
      '    :IME,'
      '    :PREZIME,'
      '    :BRACNA_SOSTOJBA,'
      '    :POL,'
      '    :MOLBABROJ,'
      '    :DATUM'
      ')')
    RefreshSQL.Strings = (
      'select hrm.id,'
      '       hrm.naziv_vraboten as ImePrezime,'
      '       hrm.mb,'
      '       hrm.id_oglas,'
      '       hrm.id_rm_re,'
      '       hrm.mominsko_prezime,'
      '       hrm.datum_radjanje,'
      '       hrm.zdr_sostojba,'
      '       hrm.ts_ins,'
      '       hrm.ts_upd,'
      '       hrm.usr_ins,'
      '       hrm.usr_upd,'
      '       hrm.slika,'
      '       hrm.id_nacionalnost,'
      '       hrm.id_veroispoved,'
      '       hrm.ime,'
      '       hrm.prezime,'
      '       hrn.naziv as NacionalnostNaziv,'
      '       hrv.naziv as VeroispovedNaziv,'
      
        '       hro.broj || '#39'/'#39' || extractyear(hro.datum_objaven) as Broj' +
        'Godina,'
      '       hrrm.naziv,'
      '       case hrm.pol when '#39'1'#39' then '#39#1052#1072#1096#1082#1080#39
      '                        when '#39'2'#39' then '#39#1046#1077#1085#1089#1082#1080#39
      '       end "PolNaziv",'
      '       case hrm.bracna_sostojba when '#39'1'#39' then '#39#1044#1072#39
      '                        when '#39'0'#39' then '#39#1053#1077#39
      '       end "Brak",'
      '       hrm.bracna_sostojba,'
      '       hrm.pol,'
      '       hrm.broj as MolbaBroj,'
      '       hrm.datum'
      '       '
      'from hr_evidencija_molba hrm'
      
        'left outer join hr_nacionalnost hrn on hrn.id = hrm.id_nacionaln' +
        'ost'
      
        'left outer join hr_veroispoved hrv on hrv.id = hrm.id_veroispove' +
        'd'
      'left outer join hr_evidencija_oglas hro on hro.id = hrm.id_oglas'
      'inner join hr_rm_re hrr on hrr.id = hrm.id_rm_re'
      'inner join hr_rabotno_mesto hrrm on hrrm.id = hrr.id_rm'
      
        'where(  hrm.id like :ID and coalesce(hrm.id_oglas, 0) like :ogla' +
        'sID'
      '--and hrr.id_re_firma like :firma'
      '     ) and (     HRM.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select hrm.id,'
      '       hrm.naziv_vraboten as ImePrezime,'
      '       hrm.mb,'
      '       hrm.id_oglas,'
      '       hrm.id_rm_re,'
      '       hrm.mominsko_prezime,'
      '       hrm.datum_radjanje,'
      '       hrm.zdr_sostojba,'
      '       hrm.ts_ins,'
      '       hrm.ts_upd,'
      '       hrm.usr_ins,'
      '       hrm.usr_upd,'
      '       hrm.slika,'
      '       hrm.id_nacionalnost,'
      '       hrm.id_veroispoved,'
      '       hrm.ime,'
      '       hrm.prezime,'
      '       hrn.naziv as NacionalnostNaziv,'
      '       hrv.naziv as VeroispovedNaziv,'
      
        '       hro.broj || '#39'/'#39' || extractyear(hro.datum_objaven) as Broj' +
        'Godina,'
      '       hrrm.naziv,'
      '       case hrm.pol when '#39'1'#39' then '#39#1052#1072#1096#1082#1080#39
      '                        when '#39'2'#39' then '#39#1046#1077#1085#1089#1082#1080#39
      '       end "PolNaziv",'
      '       case hrm.bracna_sostojba when '#39'1'#39' then '#39#1044#1072#39
      '                        when '#39'0'#39' then '#39#1053#1077#39
      '       end "Brak",'
      '       hrm.bracna_sostojba,'
      '       hrm.pol,'
      '       hrm.broj as MolbaBroj,'
      '       hrm.datum'
      'from hr_evidencija_molba hrm'
      
        'left outer join hr_nacionalnost hrn on hrn.id = hrm.id_nacionaln' +
        'ost'
      
        'left outer join hr_veroispoved hrv on hrv.id = hrm.id_veroispove' +
        'd'
      'left outer join hr_evidencija_oglas hro on hro.id = hrm.id_oglas'
      'inner join hr_rm_re hrr on hrr.id = hrm.id_rm_re'
      'inner join hr_rabotno_mesto hrrm on hrrm.id = hrr.id_rm'
      
        'where hrm.id like :ID and coalesce(hrm.id_oglas, 0) like :oglasI' +
        'D'
      '--and hrr.id_re_firma like :firma'
      'order by hrm.broj desc')
    AutoUpdateOptions.UpdateTableName = 'HR_EVIDENCIJA_MOLBA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_EVIDENCIJA_MOLBA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    BeforeOpen = tblMolbaBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 128
    object tblMolbaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblMolbaIMEPREZIME: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'IMEPREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1041#1088'.'
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaID_OGLAS: TFIBIntegerField
      DisplayLabel = #1054#1075#1083#1072#1089' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_OGLAS'
    end
    object tblMolbaID_RM_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1052' '#1074#1086' '#1056#1045
      FieldName = 'ID_RM_RE'
    end
    object tblMolbaMOMINSKO_PREZIME: TFIBStringField
      DisplayLabel = #1052#1086#1084#1080#1085#1089#1082#1086' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'MOMINSKO_PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaDATUM_RADJANJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077
      FieldName = 'DATUM_RADJANJE'
    end
    object tblMolbaZDR_SOSTOJBA: TFIBStringField
      DisplayLabel = #1047#1076#1088#1072#1089#1090#1074#1077#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
      FieldName = 'ZDR_SOSTOJBA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblMolbaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblMolbaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaSLIKA: TFIBBlobField
      DisplayLabel = #1057#1083#1080#1082#1072
      FieldName = 'SLIKA'
      Size = 8
    end
    object tblMolbaID_NACIONALNOST: TFIBIntegerField
      DisplayLabel = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_NACIONALNOST'
    end
    object tblMolbaID_VEROISPOVED: TFIBIntegerField
      DisplayLabel = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_VEROISPOVED'
    end
    object tblMolbaIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaNACIONALNOSTNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090
      FieldName = 'NACIONALNOSTNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaVEROISPOVEDNAZIV: TFIBStringField
      DisplayLabel = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076
      FieldName = 'VEROISPOVEDNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaPolNaziv: TFIBStringField
      DisplayLabel = #1055#1086#1083
      FieldName = 'PolNaziv'
      Size = 6
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaBrak: TFIBStringField
      FieldName = 'Brak'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaBRACNA_SOSTOJBA: TFIBIntegerField
      DisplayLabel = #1041#1088#1072#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
      FieldName = 'BRACNA_SOSTOJBA'
    end
    object tblMolbaPOL: TFIBIntegerField
      FieldName = 'POL'
    end
    object tblMolbaMOLBABROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1084#1086#1083#1073#1072
      FieldName = 'MOLBABROJ'
    end
    object tblMolbaBROJGODINA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      FieldName = 'BROJGODINA'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblMolbaDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1084#1086#1083#1073#1072
      FieldName = 'DATUM'
    end
  end
  object dsMolba: TDataSource
    DataSet = tblMolba
    Left = 507
    Top = 128
  end
  object tblLVK: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_LVK'
      'SET '
      '    ID_MOLBA = :ID_MOLBA,'
      '    MB_VRABOTEN = :MB_VRABOTEN,'
      '    MAJCIN_JAZIK = :MAJCIN_JAZIK,'
      '    DRUSTVENI_V = :DRUSTVENI_V,'
      '    ORGANIZACISKI_V = :ORGANIZACISKI_V,'
      '    TEHNICKI_V = :TEHNICKI_V,'
      '    KOMPJUTERSKI_V = :KOMPJUTERSKI_V,'
      '    UMETNICKI_V = :UMETNICKI_V,'
      '    DRUGI_V = :DRUGI_V,'
      '    VOZACKA_DOZVOLA = :VOZACKA_DOZVOLA,'
      '    KATEGORIJA_VD = :KATEGORIJA_VD,'
      '    DOPOLNITELNI_INFORMACII = :DOPOLNITELNI_INFORMACII,'
      '    PRILOZI = :PRILOZI,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_LVK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_LVK('
      '    ID,'
      '    ID_MOLBA,'
      '    MB_VRABOTEN,'
      '    MAJCIN_JAZIK,'
      '    DRUSTVENI_V,'
      '    ORGANIZACISKI_V,'
      '    TEHNICKI_V,'
      '    KOMPJUTERSKI_V,'
      '    UMETNICKI_V,'
      '    DRUGI_V,'
      '    VOZACKA_DOZVOLA,'
      '    KATEGORIJA_VD,'
      '    DOPOLNITELNI_INFORMACII,'
      '    PRILOZI,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_MOLBA,'
      '    :MB_VRABOTEN,'
      '    :MAJCIN_JAZIK,'
      '    :DRUSTVENI_V,'
      '    :ORGANIZACISKI_V,'
      '    :TEHNICKI_V,'
      '    :KOMPJUTERSKI_V,'
      '    :UMETNICKI_V,'
      '    :DRUGI_V,'
      '    :VOZACKA_DOZVOLA,'
      '    :KATEGORIJA_VD,'
      '    :DOPOLNITELNI_INFORMACII,'
      '    :PRILOZI,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrl.id,'
      '       hrl.id_molba,'
      '       hrl.mb_vraboten,'
      '       hrl.majcin_jazik,'
      '       hrl.drustveni_v,'
      '       hrl.organizaciski_v,'
      '       hrl.tehnicki_v,'
      '       hrl.kompjuterski_v,'
      '       hrl.umetnicki_v,'
      '       hrl.drugi_v,'
      '       hrl.vozacka_dozvola,'
      '       hrl.kategorija_vd,'
      '       hrl.dopolnitelni_informacii,'
      '       hrl.prilozi,'
      '       hrl.ts_ins,'
      '       hrl.ts_upd,'
      '       hrl.usr_ins,'
      '       hrl.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrm.naziv_vraboten as ImePrezime,'
      '       case hrl.vozacka_dozvola when '#39'1'#39' then '#39#1048#1084#1072#39
      '                        when '#39'0'#39' then '#39#1053#1077#1084#1072#39
      '       end "Vozacka"'
      ''
      'from hr_lvk hrl'
      'left outer join hr_evidencija_molba hrm on hrm.id = hrl.id_molba'
      'left outer join hr_vraboten hrv on hrv.mb=hrl.mb_vraboten'
      
        'where(  coalesce(hrl.mb_vraboten, 0) like :MB and coalesce(hrl.i' +
        'd_molba, 0) like :MolbaID'
      '     ) and (     HRL.ID = :OLD_ID'
      '     )'
      
        'order by  hrl.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n    ')
    SelectSQL.Strings = (
      'select hrl.id,'
      '       hrl.id_molba,'
      '       hrl.mb_vraboten,'
      '       hrl.majcin_jazik,'
      '       hrl.drustveni_v,'
      '       hrl.organizaciski_v,'
      '       hrl.tehnicki_v,'
      '       hrl.kompjuterski_v,'
      '       hrl.umetnicki_v,'
      '       hrl.drugi_v,'
      '       hrl.vozacka_dozvola,'
      '       hrl.kategorija_vd,'
      '       hrl.dopolnitelni_informacii,'
      '       hrl.prilozi,'
      '       hrl.ts_ins,'
      '       hrl.ts_upd,'
      '       hrl.usr_ins,'
      '       hrl.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrm.naziv_vraboten as ImePrezime,'
      '       case hrl.vozacka_dozvola when '#39'1'#39' then '#39#1048#1084#1072#39
      '                        when '#39'0'#39' then '#39#1053#1077#1084#1072#39
      '       end "Vozacka"'
      ''
      'from hr_lvk hrl'
      'left outer join hr_evidencija_molba hrm on hrm.id = hrl.id_molba'
      'left outer join hr_vraboten hrv on hrv.mb=hrl.mb_vraboten'
      
        'where coalesce(hrl.mb_vraboten, 0) like :MB and coalesce(hrl.id_' +
        'molba, 0) like :MolbaID'
      
        'order by  hrl.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n'
      '')
    AutoUpdateOptions.UpdateTableName = 'HR_LVK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_LVK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 184
    object tblLVKID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLVKID_MOLBA: TFIBIntegerField
      DisplayLabel = #1052#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblLVKMB_VRABOTEN: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1041#1088'.'
      FieldName = 'MB_VRABOTEN'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKMAJCIN_JAZIK: TFIBStringField
      DisplayLabel = #1052#1072#1112#1095#1080#1085' '#1112#1072#1079#1080#1082
      FieldName = 'MAJCIN_JAZIK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKDRUSTVENI_V: TFIBStringField
      DisplayLabel = #1044#1088#1091#1096#1090#1074#1077#1085#1080' - '#1057#1086#1094#1080#1112#1072#1083#1085#1080' '#1074#1077#1096#1090#1080#1085#1080
      FieldName = 'DRUSTVENI_V'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKORGANIZACISKI_V: TFIBStringField
      DisplayLabel = #1054#1088#1075#1072#1085#1080#1079#1080#1088#1072#1095#1082#1080' '#1074#1077#1096#1090#1080#1085#1080
      FieldName = 'ORGANIZACISKI_V'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKTEHNICKI_V: TFIBStringField
      DisplayLabel = #1058#1077#1093#1085#1080#1095#1082#1080' '#1074#1077#1096#1090#1080#1085#1080
      FieldName = 'TEHNICKI_V'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKKOMPJUTERSKI_V: TFIBStringField
      DisplayLabel = #1050#1086#1084#1087#1112#1091#1090#1077#1088#1089#1082#1080' '#1074#1077#1096#1090#1080#1085#1080
      FieldName = 'KOMPJUTERSKI_V'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKUMETNICKI_V: TFIBStringField
      DisplayLabel = #1059#1084#1077#1090#1085#1080#1095#1082#1080' '#1074#1077#1096#1090#1080#1085#1080
      FieldName = 'UMETNICKI_V'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKDRUGI_V: TFIBStringField
      DisplayLabel = #1044#1088#1091#1075#1080' '#1074#1077#1096#1090#1080#1085#1080
      FieldName = 'DRUGI_V'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKVOZACKA_DOZVOLA: TFIBIntegerField
      DisplayLabel = #1042#1086#1079#1072#1095#1082#1072' '#1076#1086#1079#1074#1086#1083#1072
      FieldName = 'VOZACKA_DOZVOLA'
    end
    object tblLVKKATEGORIJA_VD: TFIBStringField
      DisplayLabel = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072
      FieldName = 'KATEGORIJA_VD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKDOPOLNITELNI_INFORMACII: TFIBStringField
      DisplayLabel = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080
      FieldName = 'DOPOLNITELNI_INFORMACII'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKPRILOZI: TFIBStringField
      DisplayLabel = #1055#1088#1080#1083#1086#1079#1080
      FieldName = 'PRILOZI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblLVKTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblLVKUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKIMEPREZIME: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'IMEPREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKVozacka: TFIBStringField
      DisplayLabel = #1042#1086#1079#1072#1095#1082#1072' '#1076#1086#1079#1074#1086#1083#1072
      FieldName = 'Vozacka'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLVK: TDataSource
    DataSet = tblLVK
    Left = 507
    Top = 184
  end
  object tblKontakt: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_KONTAKT'
      'SET '
      '    ID_MOLBA = :ID_MOLBA,'
      '    MB_VRABOTEN = :MB_VRABOTEN,'
      '    ADRESA = :ADRESA,'
      '    BROJ = :BROJ,'
      '    MESTO = :MESTO,'
      '    DRZAVA = :DRZAVA,'
      '    DRZAVJANSTVO = :DRZAVJANSTVO,'
      '    TEL = :TEL,'
      '    MOBILEN = :MOBILEN,'
      '    EMAIL = :EMAIL,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    ITNO_SRODSTVO = :ITNO_SRODSTVO,'
      '    ITNO_IME_PREZIME = :ITNO_IME_PREZIME,'
      '    ITNO_ADRESA = :ITNO_ADRESA,'
      '    ITNO_TELEFON = :ITNO_TELEFON,'
      '    LK = :LK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_KONTAKT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_KONTAKT('
      '    ID,'
      '    ID_MOLBA,'
      '    MB_VRABOTEN,'
      '    ADRESA,'
      '    BROJ,'
      '    MESTO,'
      '    DRZAVA,'
      '    DRZAVJANSTVO,'
      '    TEL,'
      '    MOBILEN,'
      '    EMAIL,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    ITNO_SRODSTVO,'
      '    ITNO_IME_PREZIME,'
      '    ITNO_ADRESA,'
      '    ITNO_TELEFON,'
      '    LK'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_MOLBA,'
      '    :MB_VRABOTEN,'
      '    :ADRESA,'
      '    :BROJ,'
      '    :MESTO,'
      '    :DRZAVA,'
      '    :DRZAVJANSTVO,'
      '    :TEL,'
      '    :MOBILEN,'
      '    :EMAIL,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :ITNO_SRODSTVO,'
      '    :ITNO_IME_PREZIME,'
      '    :ITNO_ADRESA,'
      '    :ITNO_TELEFON,'
      '    :LK'
      ')')
    RefreshSQL.Strings = (
      'select hrk.id,'
      '       hrk.id_molba,'
      '       hrk.mb_vraboten,'
      '       hrk.adresa,'
      '       hrk.broj,'
      '       hrk.mesto,'
      '       hrk.drzava,'
      '       hrk.drzavjanstvo,'
      '       hrk.tel,'
      '       hrk.mobilen,'
      '       hrk.email,'
      '       hrk.ts_ins,'
      '       hrk.ts_upd,'
      '       hrk.usr_ins,'
      '       hrk.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrmo.naziv_vraboten as ImePrezime,'
      '       mm.naziv as mestoNaziv,'
      '       md.naziv as drzavaNaziv,'
      '       hrd.naziv as drzavjanstvoNaziv,'
      '       hrk.itno_srodstvo,'
      '       hrk.itno_ime_prezime,'
      '       hrk.itno_adresa,'
      '       hrk.itno_telefon,'
      '       hrk.lk,'
      '       case when hrk.lk = 1 then '#39#1044#1072#39
      '            else '#39#1053#1077#39
      '       end "lkNaziv"'
      'from hr_kontakt hrk'
      'left outer join hr_vraboten hrv on hrv.mb = hrk.mb_vraboten'
      
        'left outer join hr_evidencija_molba hrmo on hrmo.id = hrk.id_mol' +
        'ba'
      'inner join mat_mesto mm on mm.id = hrk.mesto'
      'left outer join mat_drzava md on md.id = hrk.drzava'
      'left outer join hr_drzavjanstvo hrd on hrd.id = hrk.drzavjanstvo'
      
        'where(  coalesce(hrk.mb_vraboten, 0) like :MB and coalesce(hrk.i' +
        'd_molba, 0) like :MolbaID'
      '     ) and (     HRK.ID = :OLD_ID'
      '     )'
      
        'order by  hrk.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n'
      '    ')
    SelectSQL.Strings = (
      'select hrk.id,'
      '       hrk.id_molba,'
      '       hrk.mb_vraboten,'
      '       hrk.adresa,'
      '       hrk.broj,'
      '       hrk.mesto,'
      '       hrk.drzava,'
      '       hrk.drzavjanstvo,'
      '       hrk.tel,'
      '       hrk.mobilen,'
      '       hrk.email,'
      '       hrk.ts_ins,'
      '       hrk.ts_upd,'
      '       hrk.usr_ins,'
      '       hrk.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrmo.naziv_vraboten as ImePrezime,'
      '       mm.naziv as mestoNaziv,'
      '       md.naziv as drzavaNaziv,'
      '       hrd.naziv as drzavjanstvoNaziv,'
      '       hrk.itno_srodstvo,'
      '       hrk.itno_ime_prezime,'
      '       hrk.itno_adresa,'
      '       hrk.itno_telefon,'
      '       hrk.lk,'
      '       case when hrk.lk = 1 then '#39#1044#1072#39
      '            else '#39#1053#1077#39
      '       end "lkNaziv"'
      'from hr_kontakt hrk'
      'left outer join hr_vraboten hrv on hrv.mb = hrk.mb_vraboten'
      
        'left outer join hr_evidencija_molba hrmo on hrmo.id = hrk.id_mol' +
        'ba'
      'inner join mat_mesto mm on mm.id = hrk.mesto'
      'left outer join mat_drzava md on md.id = hrk.drzava'
      'left outer join hr_drzavjanstvo hrd on hrd.id = hrk.drzavjanstvo'
      
        'where coalesce(hrk.mb_vraboten, 0) like :MB and coalesce(hrk.id_' +
        'molba, 0) like :MolbaID'
      
        'order by  hrk.id_molba, hrv.naziv_vraboten_ti, hrv.naziv_vrabote' +
        'n'
      '')
    AutoUpdateOptions.UpdateTableName = 'HR_KONTAKT'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_KONTAKT_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 248
    object tblKontaktID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblKontaktID_MOLBA: TFIBIntegerField
      DisplayLabel = #1052#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblKontaktMB_VRABOTEN: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'. '
      FieldName = 'MB_VRABOTEN'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' ('#1086#1076' '#1072#1076#1088#1077#1089#1072')'
      FieldName = 'BROJ'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTO'
    end
    object tblKontaktDRZAVA: TFIBStringField
      DisplayLabel = #1044#1088#1078#1072#1074#1072
      FieldName = 'DRZAVA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktDRZAVJANSTVO: TFIBIntegerField
      DisplayLabel = #1044#1088#1078#1072#1074#1112#1072#1085#1089#1090#1074#1086
      FieldName = 'DRZAVJANSTVO'
    end
    object tblKontaktTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TEL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktMOBILEN: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktEMAIL: TFIBStringField
      DisplayLabel = 'eMail '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblKontaktTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblKontaktUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktIMEPREZIME: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'IMEPREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktMESTONAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktDRZAVANAZIV: TFIBStringField
      DisplayLabel = #1044#1088#1078#1072#1074#1072
      FieldName = 'DRZAVANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktDRZAVJANSTVONAZIV: TFIBStringField
      DisplayLabel = #1044#1088#1078#1072#1074#1112#1072#1085#1089#1090#1074#1086
      FieldName = 'DRZAVJANSTVONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktITNO_SRODSTVO: TFIBStringField
      DisplayLabel = #1057#1088#1086#1076#1089#1090#1074#1086' '#1089#1086' '#1082#1086#1085#1090#1072#1082#1090' '#1079#1072' '#1080#1090#1085#1080' '#1087#1086#1090#1088#1077#1073#1080
      FieldName = 'ITNO_SRODSTVO'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktITNO_IME_PREZIME: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1079#1072' '#1080#1090#1085#1080' '#1087#1086#1090#1088#1077#1073#1080
      FieldName = 'ITNO_IME_PREZIME'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktITNO_ADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1079#1072' '#1080#1090#1085#1080' '#1087#1086#1090#1088#1077#1073#1080
      FieldName = 'ITNO_ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktITNO_TELEFON: TFIBStringField
      DisplayLabel = 'T'#1077#1083#1077#1092#1086#1085' '#1085#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1079#1072' '#1080#1090#1085#1080' '#1087#1086#1090#1088#1077#1073#1080
      FieldName = 'ITNO_TELEFON'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktLK: TFIBSmallIntField
      DisplayLabel = #1051#1080#1095#1085#1072' '#1082#1072#1088#1090#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'LK'
    end
    object tblKontaktlkNaziv: TFIBStringField
      DisplayLabel = #1054#1076' '#1083#1080#1095#1085#1072' '#1082#1072#1088#1090#1072
      FieldName = 'lkNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKontaktNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKontakt: TDataSource
    DataSet = tblKontakt
    Left = 507
    Top = 248
  end
  object tblLVKStranskiJazik: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_STRANSKI_JAZIK'
      'SET '
      '    ID_LVK = :ID_LVK,'
      '    VID_STRANSKI_JAZIK = :VID_STRANSKI_JAZIK,'
      '    RAZBIRANJE_SLUSANJE = :RAZBIRANJE_SLUSANJE,'
      '    RAZBIRANJE_CITANJE = :RAZBIRANJE_CITANJE,'
      '    GOVOR_INTERAKCIJA = :GOVOR_INTERAKCIJA,'
      '    GOVOR_PRODUKCIJA = :GOVOR_PRODUKCIJA,'
      '    PISUVANJE = :PISUVANJE,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_STRANSKI_JAZIK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_STRANSKI_JAZIK('
      '    ID,'
      '    ID_LVK,'
      '    VID_STRANSKI_JAZIK,'
      '    RAZBIRANJE_SLUSANJE,'
      '    RAZBIRANJE_CITANJE,'
      '    GOVOR_INTERAKCIJA,'
      '    GOVOR_PRODUKCIJA,'
      '    PISUVANJE,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_LVK,'
      '    :VID_STRANSKI_JAZIK,'
      '    :RAZBIRANJE_SLUSANJE,'
      '    :RAZBIRANJE_CITANJE,'
      '    :GOVOR_INTERAKCIJA,'
      '    :GOVOR_PRODUKCIJA,'
      '    :PISUVANJE,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrj.id,'
      '       hrj.id_lvk,'
      '       hrj.vid_stranski_jazik,'
      '       hrj.razbiranje_slusanje,'
      '       hrj.razbiranje_citanje,'
      '       hrj.govor_interakcija,'
      '       hrj.govor_produkcija,'
      '       hrj.pisuvanje,'
      '       hrj.ts_ins,'
      '       hrj.ts_upd,'
      '       hrj.usr_ins,'
      '       hrj.usr_upd,'
      '       hrvj.naziv,'
      '       hrl.id_molba,'
      '       case hrj.razbiranje_slusanje when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "RazbiranjeSlusanje",'
      '       case hrj.razbiranje_citanje when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "RazbiranjeCitanje",'
      '       case hrj.pisuvanje when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "PisuvanjeNaziv",'
      '       case hrj.govor_interakcija when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "GovorInterakcija",'
      '       case hrj.govor_produkcija when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "GovorProdukcija"'
      ''
      'from hr_stranski_jazik hrj'
      
        'inner join hr_vid_stranski_jazik hrvj on hrvj.id = hrj.vid_stran' +
        'ski_jazik'
      'inner join hr_lvk hrl on hrl.id = hrj.id_lvk'
      'where(  hrj.id_lvk like :lvkID'
      '     ) and (     HRJ.ID = :OLD_ID'
      '     )'
      'order by hrvj.naziv    ')
    SelectSQL.Strings = (
      'select hrj.id,'
      '       hrj.id_lvk,'
      '       hrj.vid_stranski_jazik,'
      '       hrj.razbiranje_slusanje,'
      '       hrj.razbiranje_citanje,'
      '       hrj.govor_interakcija,'
      '       hrj.govor_produkcija,'
      '       hrj.pisuvanje,'
      '       hrj.ts_ins,'
      '       hrj.ts_upd,'
      '       hrj.usr_ins,'
      '       hrj.usr_upd,'
      '       hrvj.naziv,'
      '       hrl.id_molba,'
      '       case hrj.razbiranje_slusanje when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "RazbiranjeSlusanje",'
      '       case hrj.razbiranje_citanje when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "RazbiranjeCitanje",'
      '       case hrj.pisuvanje when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "PisuvanjeNaziv",'
      '       case hrj.govor_interakcija when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "GovorInterakcija",'
      '       case hrj.govor_produkcija when '#39'1'#39' then '#39#1051#1086#1096#1086#39
      '                                    when '#39'2'#39' then '#39#1044#1086#1073#1088#1086#39
      '                                    when '#39'3'#39' then '#39#1054#1076#1083#1080#1095#1085#1086#39
      '       end "GovorProdukcija"'
      ''
      'from hr_stranski_jazik hrj'
      
        'inner join hr_vid_stranski_jazik hrvj on hrvj.id = hrj.vid_stran' +
        'ski_jazik'
      'inner join hr_lvk hrl on hrl.id = hrj.id_lvk'
      'where hrj.id_lvk like :lvkID'
      'order by hrvj.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_STRANSKI_JAZIK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_STRANSKI_JAZIK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 312
    object tblLVKStranskiJazikID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLVKStranskiJazikID_LVK: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1051#1042#1050
      FieldName = 'ID_LVK'
    end
    object tblLVKStranskiJazikVID_STRANSKI_JAZIK: TFIBIntegerField
      DisplayLabel = #1057#1090#1088#1072#1085#1089#1082#1080' '#1112#1072#1079#1080#1082'(ID)'
      FieldName = 'VID_STRANSKI_JAZIK'
    end
    object tblLVKStranskiJazikRAZBIRANJE_SLUSANJE: TFIBIntegerField
      DisplayLabel = #1056#1072#1079#1073#1080#1088#1072#1114#1077' '#1089#1083#1091#1096#1072#1114#1077
      FieldName = 'RAZBIRANJE_SLUSANJE'
    end
    object tblLVKStranskiJazikRAZBIRANJE_CITANJE: TFIBIntegerField
      DisplayLabel = #1056#1072#1079#1073#1080#1088#1072#1114#1077' '#1095#1080#1090#1072#1114#1077
      FieldName = 'RAZBIRANJE_CITANJE'
    end
    object tblLVKStranskiJazikGOVOR_INTERAKCIJA: TFIBIntegerField
      DisplayLabel = #1043#1086#1074#1086#1088' - '#1048#1085#1090#1077#1088#1072#1082#1094#1080#1112#1072
      FieldName = 'GOVOR_INTERAKCIJA'
    end
    object tblLVKStranskiJazikGOVOR_PRODUKCIJA: TFIBIntegerField
      DisplayLabel = #1043#1086#1074#1086#1088' - '#1055#1088#1086#1076#1091#1082#1094#1080#1112#1072
      FieldName = 'GOVOR_PRODUKCIJA'
    end
    object tblLVKStranskiJazikPISUVANJE: TFIBIntegerField
      DisplayLabel = #1055#1080#1096#1091#1074#1072#1114#1077
      FieldName = 'PISUVANJE'
    end
    object tblLVKStranskiJazikTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblLVKStranskiJazikTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblLVKStranskiJazikUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1088#1072#1085#1089#1082#1080' '#1112#1072#1079#1080#1082
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikID_MOLBA: TFIBIntegerField
      DisplayLabel = #1052#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblLVKStranskiJazikRazbiranjeSlusanje: TFIBStringField
      DisplayLabel = #1057#1083#1091#1096#1072#1114#1077
      FieldName = 'RazbiranjeSlusanje'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikRazbiranjeCitanje: TFIBStringField
      DisplayLabel = #1063#1080#1090#1072#1114#1077
      FieldName = 'RazbiranjeCitanje'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikPisuvanjeNaziv: TFIBStringField
      DisplayLabel = #1055#1080#1096#1091#1074#1072#1114#1077
      FieldName = 'PisuvanjeNaziv'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikGovorInterakcija: TFIBStringField
      DisplayLabel = #1043#1086#1074#1086#1088' - '#1048#1085#1090#1077#1088#1072#1082#1094#1080#1112#1072
      FieldName = 'GovorInterakcija'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLVKStranskiJazikGovorProdukcija: TFIBStringField
      DisplayLabel = #1043#1086#1074#1086#1088' - '#1055#1088#1086#1076#1091#1082#1094#1080#1112#1072
      FieldName = 'GovorProdukcija'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLVKStranskiJazik: TDataSource
    DataSet = tblLVKStranskiJazik
    Left = 507
    Top = 312
  end
  object tblRMOglas: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RM_OGLAS'
      'SET '
      '    ID_OGLAS = :ID_OGLAS,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    EMAIL = :EMAIL,'
      '    URL = :URL,'
      '    TEL = :TEL,'
      '    OPIS = :OPIS,'
      '    BR_RABOTNI_IMESTA = :BR_RABOTNI_IMESTA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_RM_OGLAS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_RM_OGLAS('
      '    ID,'
      '    ID_OGLAS,'
      '    ID_RM_RE,'
      '    EMAIL,'
      '    URL,'
      '    TEL,'
      '    OPIS,'
      '    BR_RABOTNI_IMESTA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_OGLAS,'
      '    :ID_RM_RE,'
      '    :EMAIL,'
      '    :URL,'
      '    :TEL,'
      '    :OPIS,'
      '    :BR_RABOTNI_IMESTA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    ho.broj broj_oglas,'
      '    hro.id,'
      
        '    ho.broj || '#39'/'#39' || extractyear(ho.datum_objaven) as BrGodinaO' +
        'glas,'
      '    hro.id_oglas,'
      
        '    '#39' '#1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' '#39'||ho.broj||'#39' '#1086#1076' '#1076#1072#1090#1091#1084' '#39'||ho.datum_objaven o' +
        'glas,'
      '    hro.id_rm_re,'
      '    hrm.naziv naziv_rm,'
      '    m.naziv as naziv_re,'
      '    hro.email,'
      '    hro.url,'
      '    hro.tel,'
      '    hro.opis,'
      '    hro.br_rabotni_imesta,'
      '    hro.ts_ins,'
      '    hro.ts_upd,'
      '    hro.usr_ins,'
      '    hro.usr_upd'
      'from hr_rm_oglas hro'
      'inner join hr_evidencija_oglas ho on ho.id=hro.id_oglas'
      'inner join hr_rm_re hrr on hrr.id=hro.id_rm_re'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join mat_re m on m.id = hrr.id_re'
      'where(  hro.id_oglas like :id --and ho.id_re_firma like :firma'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      'order by hrm.naziv')
    SelectSQL.Strings = (
      'select'
      '    ho.broj broj_oglas,'
      '    hro.id,'
      
        '    ho.broj || '#39'/'#39' || extractyear(ho.datum_objaven) as BrGodinaO' +
        'glas,'
      '    hro.id_oglas,'
      
        '    '#39' '#1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' '#39'||ho.broj||'#39' '#1086#1076' '#1076#1072#1090#1091#1084' '#39'||ho.datum_objaven o' +
        'glas,'
      '    hro.id_rm_re,'
      '    hrm.naziv naziv_rm,'
      '    m.naziv as naziv_re,'
      '    hro.email,'
      '    hro.url,'
      '    hro.tel,'
      '    hro.opis,'
      '    hro.br_rabotni_imesta,'
      '    hro.ts_ins,'
      '    hro.ts_upd,'
      '    hro.usr_ins,'
      '    hro.usr_upd'
      'from hr_rm_oglas hro'
      'inner join hr_evidencija_oglas ho on ho.id=hro.id_oglas'
      'inner join hr_rm_re hrr on hrr.id=hro.id_rm_re'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join mat_re m on m.id = hrr.id_re'
      'where hro.id_oglas like :id --and ho.id_re_firma like :firma'
      'order by hrm.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_OGLAS'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_OGLAS_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsOglas
    Left = 224
    Top = 71
    oRefreshDeletedRecord = True
    object tblRMOglasID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMOglasID_OGLAS: TFIBIntegerField
      DisplayLabel = #1054#1075#1083#1072#1089' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_OGLAS'
    end
    object tblRMOglasID_RM_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1052' '#1074#1086' '#1056#1045
      FieldName = 'ID_RM_RE'
    end
    object tblRMOglasEMAIL: TFIBStringField
      DisplayLabel = 'E-MAIL'
      FieldName = 'EMAIL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasURL: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'URL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TEL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblRMOglasTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblRMOglasUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasNAZIV_RM: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasOGLAS: TFIBStringField
      FieldName = 'OGLAS'
      Size = 55
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasBR_RABOTNI_IMESTA: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
      FieldName = 'BR_RABOTNI_IMESTA'
    end
    object tblRMOglasNAZIV_RE: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasBROJ_OGLAS: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      FieldName = 'BROJ_OGLAS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMOglasBRGODINAOGLAS: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      FieldName = 'BRGODINAOGLAS'
      Size = 32
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRMOglas: TDataSource
    DataSet = tblRMOglas
    Left = 299
    Top = 71
  end
  object tblOrgRMRE: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAT_RE'
      'WHERE'
      '        ')
    RefreshSQL.Strings = (
      'select'
      '    0 broj,'
      '    0 br,'
      '    hrr.id_re,'
      '    hrm.naziv naziv_re,'
      '    hrr.id||'#39'00'#39' id_rmre,'
      '    cast('#39#39' as varchar(100)) naziv_vraboten,'
      '    '#39'16777215'#39'  boja,'
      '    cast('#39#39' as blob) slika'
      'from   mat_re mr'
      'inner join hr_rm_re hrr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'where mr.r = 1 and hs.do_datum is null and hs.id_re_firma = :re ' +
        'and  mr.poteklo like :re||'#39',%'#39
      'union'
      'select'
      '    0 broj,'
      '    0 br,'
      
        '   (select id from mat_re where koren is null and r=1 and id = :' +
        're) id_re,'
      '    mr.naziv naziv_re,'
      '    mr.id id_rmre,'
      '    cast('#39#39' as varchar(100)) naziv_vraboten,'
      '    16777215 boja,'
      '    cast('#39#39' as blob) slika'
      'from   mat_re mr'
      'inner join hr_rm_re hrr on mr.id=hrr.id_re'
      
        'where mr.r = 1 /*$$IBEC$$ and mr.id like :re $$IBEC$$*/  and  mr' +
        '.poteklo like :re||'#39',%'#39
      'union'
      'select'
      '    0 broj,'
      '    0 br,'
      '    cast('#39#39' as varchar20) id_re,'
      '    mr.naziv naziv_re,'
      '    mr.id id_rmre,'
      '    cast('#39#39' as varchar(100)) naziv_vraboten,'
      '    16777215 boja,'
      '    cast('#39#39' as blob) slika'
      'from mat_re mr'
      'where mr.r = 1 and mr.koren is null and mr.re=:re'
      'union'
      'select'
      '    p.broj,'
      '    p.br,'
      '    p.id_re,'
      '    p.naziv_re,'
      '    p.id_rmre,'
      '    p.naziv_vraboten,'
      '    p.boja,'
      '    p.slika'
      'from proc_hr_org_rm(:re) p')
    SelectSQL.Strings = (
      'select'
      '    0 broj,'
      '    0 br,'
      '    hrr.id_re,'
      '    hrm.naziv naziv_re,'
      '    hrr.id||'#39'00'#39' id_rmre,'
      '    cast('#39#39' as varchar(100)) naziv_vraboten,'
      '    '#39'16777215'#39'  boja,'
      '    cast('#39#39' as blob) slika'
      'from   mat_re mr'
      'inner join hr_rm_re hrr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'where mr.r = 1 and hs.do_datum is null and hs.id_re_firma = :re ' +
        'and  mr.poteklo like :re||'#39',%'#39
      'union'
      'select'
      '    0 broj,'
      '    0 br,'
      
        '   (select id from mat_re where koren is null and r=1 and id = :' +
        're) id_re,'
      '    mr.naziv naziv_re,'
      '    mr.id id_rmre,'
      '    cast('#39#39' as varchar(100)) naziv_vraboten,'
      '    16777215 boja,'
      '    cast('#39#39' as blob) slika'
      'from   mat_re mr'
      'inner join hr_rm_re hrr on mr.id=hrr.id_re'
      
        'where mr.r = 1 /*$$IBEC$$ and mr.id like :re $$IBEC$$*/  and  mr' +
        '.poteklo like :re||'#39',%'#39
      'union'
      'select'
      '    0 broj,'
      '    0 br,'
      '    cast('#39#39' as varchar20) id_re,'
      '    mr.naziv naziv_re,'
      '    mr.id id_rmre,'
      '    cast('#39#39' as varchar(100)) naziv_vraboten,'
      '    16777215 boja,'
      '    cast('#39#39' as blob) slika'
      'from mat_re mr'
      'where mr.r = 1 and mr.koren is null and mr.re=:re'
      'union'
      'select'
      '    p.broj,'
      '    p.br,'
      '    p.id_re,'
      '    p.naziv_re,'
      '    p.id_rmre,'
      '    p.naziv_vraboten,'
      '    p.boja,'
      '    p.slika'
      'from proc_hr_org_rm(:re) p')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 363
    oRefreshDeletedRecord = True
    object tblOrgRMREBR: TFIBIntegerField
      FieldName = 'BR'
    end
    object tblOrgRMREID_RE: TFIBStringField
      FieldName = 'ID_RE'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOrgRMRENAZIV_RE: TFIBStringField
      FieldName = 'NAZIV_RE'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOrgRMREID_RMRE: TFIBStringField
      FieldName = 'ID_RMRE'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOrgRMRENAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 151
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOrgRMREBOJA: TFIBStringField
      FieldName = 'BOJA'
      Size = 11
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOrgRMRESLIKA: TFIBBlobField
      FieldName = 'SLIKA'
      Size = 8
    end
    object tblOrgRMREBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
  end
  object dsOrgRMRE: TDataSource
    DataSet = tblOrgRMRE
    Left = 507
    Top = 363
  end
  object tblVrabRM: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '   vrm.id,'
      '   vrm.mb,'
      '   hv.naziv_vraboten, hv.naziv_vraboten_ti,'
      '   vrm.id_rm_re,'
      '   hrm.naziv rabotno_mesto,'
      '   vrm.datum_od,'
      '   vrm.datum_do,'
      '   vrm.dokument,'
      '   vrm.document_id,'
      '   vd.naziv as DocumentNaziv,'
      '   vrm.z_dokument_id, vrm.z_dokument, vrm.z_dokument_datum,'
      '   vrm.pro_dokument_id, vrm.pro_dokument_datum,'
      '   vrm.ts_ins,'
      '   vrm.ts_upd,'
      '   vrm.usr_ins,'
      '   vrm.usr_upd'
      'from hr_vraboten_rm vrm'
      'inner join hr_vraboten hv on hv.mb=vrm.mb'
      'inner join hr_rm_re hrr on hrr.id=vrm.id_rm_re'
      'inner join hr_rabotno_mesto hrm on hrr.id_rm=hrm.id'
      'left outer join hr_vid_dokument vd on vd.id = vrm.dokument'
      'where vrm.mb=:MB and vrm.id_re_firma = :firma')
    AutoUpdateOptions.UpdateTableName = 'HR_VRABOTEN_RM'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_VRABOTEN_RM_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 427
    oRefreshDeletedRecord = True
    object tblVrabRMID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblVrabRMMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMID_RM_RE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_RM_RE'
    end
    object tblVrabRMRABOTNO_MESTO: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNO_MESTO'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1086#1076
      FieldName = 'DATUM_OD'
    end
    object tblVrabRMDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblVrabRMTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblVrabRMTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblVrabRMUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMDOKUMENT: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090' ('#1064#1080#1092#1088#1072')'
      FieldName = 'DOKUMENT'
    end
    object tblVrabRMDOCUMENT_ID: TFIBIntegerField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090' ('#1064#1080#1092#1088#1072')'
      FieldName = 'DOCUMENT_ID'
    end
    object tblVrabRMDOCUMENTNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'DOCUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVrabRMZ_DOKUMENT_ID: TFIBIntegerField
      DisplayLabel = '('#1047#1044')'#1064#1080#1092#1088#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'Z_DOKUMENT_ID'
    end
    object tblVrabRMZ_DOKUMENT: TFIBIntegerField
      DisplayLabel = '('#1047#1044')'#1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'Z_DOKUMENT'
    end
    object tblVrabRMZ_DOKUMENT_DATUM: TFIBDateField
      FieldName = 'Z_DOKUMENT_DATUM'
    end
    object tblVrabRMPRO_DOKUMENT_ID: TFIBIntegerField
      FieldName = 'PRO_DOKUMENT_ID'
    end
    object tblVrabRMPRO_DOKUMENT_DATUM: TFIBDateField
      FieldName = 'PRO_DOKUMENT_DATUM'
    end
  end
  object dsVrabRM: TDataSource
    DataSet = tblVrabRM
    Left = 507
    Top = 427
  end
  object tblKategorja: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_KATEGORIJA'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    BROJ = :BROJ,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_KATEGORIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_KATEGORIJA('
      '    ID,'
      '    NAZIV,'
      '    BROJ,'
      '    ID_RM_RE,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :BROJ,'
      '    :ID_RM_RE,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select '
      '    hk.id,'
      '    hk.naziv,'
      '    hk.broj,'
      '    hk.id_rm_re,'
      '    hk.ts_ins,'
      '    hk.ts_upd,'
      '    hk.usr_ins,'
      '    hk.usr_upd,'
      '    hrm.id id_rm,'
      '    hrm.naziv naziv_rm'
      'from  hr_kategorija hk'
      '   left outer join hr_rm_re hrr on hrr.id = hk.id_rm_re'
      '   left outer join hr_rabotno_mesto hrm on hrr.id_rm = hrm.id'
      
        'where(  (hk.id_rm_re like :rm and hrr.id_re_firma=:firma) or (hk' +
        '.id_rm_re is null)'
      '     ) and (     HK.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select '
      '    hk.id,'
      '    hk.naziv,'
      '    hk.broj,'
      '    hk.id_rm_re,'
      '    hk.ts_ins,'
      '    hk.ts_upd,'
      '    hk.usr_ins,'
      '    hk.usr_upd,'
      '    hrm.id id_rm,'
      '    hrm.naziv naziv_rm'
      'from  hr_kategorija hk'
      '   left outer join hr_rm_re hrr on hrr.id = hk.id_rm_re'
      '   left outer join hr_rabotno_mesto hrm on hrr.id_rm = hrm.id'
      
        'where (hk.id_rm_re like :rm and hrr.id_re_firma=:firma) or (hk.i' +
        'd_rm_re is null)')
    AutoUpdateOptions.UpdateTableName = 'HR_KATEGORIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_KATEGORIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 224
    Top = 127
    oRefreshDeletedRecord = True
    object tblKategorjaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblKategorjaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorjaID_RM_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1052
      FieldName = 'ID_RM_RE'
    end
    object tblKategorjaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblKategorjaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblKategorjaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorjaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorjaID_RM: TFIBIntegerField
      FieldName = 'ID_RM'
    end
    object tblKategorjaNAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKategorjaBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
  end
  object dsKategorija: TDataSource
    DataSet = tblKategorja
    Left = 299
    Top = 127
  end
  object tblTipKS: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_TIP_KRITERIUM_SELEKCIJA'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    MAX_POENI = :MAX_POENI,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_TIP_KRITERIUM_SELEKCIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_TIP_KRITERIUM_SELEKCIJA('
      '    ID,'
      '    NAZIV,'
      '    MAX_POENI,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :MAX_POENI,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    htks.id,'
      '    htks.naziv,'
      '    htks.max_poeni,'
      '    htks.ts_ins,'
      '    htks.ts_upd,'
      '    htks.usr_ins,'
      '    htks.usr_upd'
      'from hr_tip_kriterium_selekcija htks'
      ''
      ' WHERE '
      '        HTKS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    htks.id,'
      '    htks.naziv,'
      '    htks.max_poeni,'
      '    htks.ts_ins,'
      '    htks.ts_upd,'
      '    htks.usr_ins,'
      '    htks.usr_upd'
      'from hr_tip_kriterium_selekcija htks')
    AutoUpdateOptions.UpdateTableName = 'HR_TIP_KRITERIUM_SELEKCIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_TIP_KS_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 224
    Top = 191
    oRefreshDeletedRecord = True
    object tblTipKSID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTipKSNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipKSTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblTipKSTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblTipKSUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipKSUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipKSMAX_POENI: TFIBIntegerField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1080' '#1087#1086#1077#1085#1080
      FieldName = 'MAX_POENI'
    end
  end
  object dsTipKS: TDataSource
    DataSet = tblTipKS
    Left = 299
    Top = 191
  end
  object tblKS: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_KRITERIUM_SELEKCIJA'
      'SET '
      '    ID = :ID,'
      '    ID_MOLBA = :ID_MOLBA,'
      '    ID_TIP_KS = :ID_TIP_KS,'
      '    INFORMACII = :INFORMACII,'
      '    DODELENI_POENI = :DODELENI_POENI,'
      '    ZABELESKA = :ZABELESKA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_KRITERIUM_SELEKCIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_KRITERIUM_SELEKCIJA('
      '    ID,'
      '    ID_MOLBA,'
      '    ID_TIP_KS,'
      '    INFORMACII,'
      '    DODELENI_POENI,'
      '    ZABELESKA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_MOLBA,'
      '    :ID_TIP_KS,'
      '    :INFORMACII,'
      '    :DODELENI_POENI,'
      '    :ZABELESKA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    hks.id,'
      '    hks.id_molba,'
      '    hem.naziv_vraboten naziv_molba,'
      '    hks.id_tip_ks,'
      '    htks.naziv,'
      '    htks.max_poeni,'
      '    hks.informacii,'
      '    hks.dodeleni_poeni,'
      '    hks.zabeleska,'
      '    hks.ts_ins,'
      '    hks.ts_upd,'
      '    hks.usr_ins,'
      '    hks.usr_upd'
      'from hr_kriterium_selekcija hks'
      
        'inner join hr_tip_kriterium_selekcija htks on htks.id=hks.id_tip' +
        '_ks'
      'inner join hr_evidencija_molba hem on hem.id=hks.id_molba'
      'inner join hr_rm_re hrr on hrr.id=hem.id_rm_re'
      'where(  hrr.id_re_firma=:firma'
      '     ) and (     HKS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hks.id,'
      '    hks.id_molba,'
      '    hem.naziv_vraboten naziv_molba, '
      '    hks.id_tip_ks,'
      '    htks.naziv,'
      '    htks.max_poeni,'
      '    hks.informacii,'
      '    hks.dodeleni_poeni,'
      '    hks.zabeleska,'
      '    hks.ts_ins,'
      '    hks.ts_upd,'
      '    hks.usr_ins,'
      '    hks.usr_upd'
      'from hr_kriterium_selekcija hks'
      
        'inner join hr_tip_kriterium_selekcija htks on htks.id=hks.id_tip' +
        '_ks'
      'inner join hr_evidencija_molba hem on hem.id=hks.id_molba'
      'inner join hr_rm_re hrr on hrr.id=hem.id_rm_re'
      'where hrr.id_re_firma=:firma')
    AutoUpdateOptions.UpdateTableName = 'HR_KRITERIUM_SELEKCIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_KRITERIUM_SELEKCIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 224
    Top = 263
    oRefreshDeletedRecord = True
    object tblKSID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblKSID_MOLBA: TFIBIntegerField
      DisplayLabel = #1052#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblKSNAZIV_MOLBA: TFIBStringField
      DisplayLabel = #1052#1086#1083#1073#1072' '#1085#1072' '
      FieldName = 'NAZIV_MOLBA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKSID_TIP_KS: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1050#1057
      FieldName = 'ID_TIP_KS'
    end
    object tblKSNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1050#1057
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKSMAX_POENI: TFIBIntegerField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1080' '#1087#1086#1077#1085#1080
      FieldName = 'MAX_POENI'
    end
    object tblKSINFORMACII: TFIBStringField
      DisplayLabel = #1048#1079#1074#1086#1088' '#1085#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080
      FieldName = 'INFORMACII'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKSDODELENI_POENI: TFIBIntegerField
      DisplayLabel = #1044#1086#1076#1077#1083#1077#1085#1080' '#1087#1086#1077#1085#1080
      FieldName = 'DODELENI_POENI'
    end
    object tblKSZABELESKA: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKSTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblKSTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblKSUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKSUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKS: TDataSource
    DataSet = tblKS
    Left = 299
    Top = 263
  end
  object pMaxBrOglas: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_HR_MAXBROGLAS')
    Left = 672
    Top = 72
  end
  object pMaxBrMolba: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_HR_MAXBRMOLBA')
    Left = 672
    Top = 128
  end
  object pMaxBrIntervju: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_MAXBRINTERVJU ')
    StoredProcName = 'PROC_HR_MAXBRINTERVJU'
    Left = 672
    Top = 184
  end
  object pMaxBrRMRE: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_MAXBRRMRE (?FIRMA, ?ID_S)')
    StoredProcName = 'PROC_HR_MAXBRRMRE'
    Left = 672
    Top = 240
  end
  object pCountRMREMolba: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_COUNTRM_MOLBA (?RMREID, ?OGLASID)')
    Left = 672
    Top = 304
  end
  object pStatusOglas: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_STATUS_OGLAS (?ID)')
    Left = 672
    Top = 368
  end
  object pSistematizacijaID: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_SISTEMATIZACIJA_ID(:firma)')
    Left = 672
    Top = 424
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 592
    Top = 424
  end
  object pSistematizacija: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_SISTEMATIZACIJA (?FIRMA, ?SID_PO_IN, ?' +
        'SID_PRED_IN)')
    StoredProcName = 'PROC_HR_SISTEMATIZACIJA'
    Left = 680
    Top = 481
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pInsertOtsustvo: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_INSERT_OTSUSTVO (?PRICINA_OPIS, ?PRICI' +
        'NA, ?OD_VREME, ?DO_VREME, ?MB, ?DO_VREME_L)')
    StoredProcName = 'PROC_HR_INSERT_OTSUSTVO'
    Left = 600
    Top = 482
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblSZObrazovanie: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_SZ_OBRAZOVANIE'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    DENOVI_GO = :DENOVI_GO,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    KREDITI = :KREDITI'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_SZ_OBRAZOVANIE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_SZ_OBRAZOVANIE('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    DENOVI_GO,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    KREDITI'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :DENOVI_GO,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :KREDITI'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    denovi_go,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    krediti'
      'FROM'
      '    HR_SZ_OBRAZOVANIE'
      ''
      ' WHERE '
      '        HR_SZ_OBRAZOVANIE.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    denovi_go,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    krediti'
      'FROM'
      '    HR_SZ_OBRAZOVANIE')
    AutoUpdateOptions.UpdateTableName = 'HR_SZ_OBRAZOVANIE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_SZ_OBRAZOVANIE_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 483
    oRefreshDeletedRecord = True
    object tblSZObrazovanieID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSZObrazovanieNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSZObrazovanieOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSZObrazovanieTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblSZObrazovanieTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblSZObrazovanieUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSZObrazovanieUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSZObrazovanieDENOVI_GO: TFIBSmallIntField
      DisplayLabel = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054
      FieldName = 'DENOVI_GO'
    end
    object tblSZObrazovanieKREDITI: TFIBIntegerField
      DisplayLabel = #1050#1088#1077#1076#1080#1090#1080
      FieldName = 'KREDITI'
    end
  end
  object dsSZObrazovanie: TDataSource
    DataSet = tblSZObrazovanie
    Left = 507
    Top = 483
  end
  object tblTipRabotnoVreme: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_TIP_RABOTNO_VREME'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_TIP_RABOTNO_VREME'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_TIP_RABOTNO_VREME('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      'FROM'
      '    HR_TIP_RABOTNO_VREME '
      ''
      ' WHERE '
      '        HR_TIP_RABOTNO_VREME.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      'FROM'
      '    HR_TIP_RABOTNO_VREME ')
    AutoUpdateOptions.UpdateTableName = 'HR_TIP_RABOTNO_VREME'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_TIP_RABOTNO_VREME_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 409
    Top = 539
    oRefreshDeletedRecord = True
    object tblTipRabotnoVremeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTipRabotnoVremeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipRabotnoVremeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblTipRabotnoVremeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblTipRabotnoVremeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipRabotnoVremeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTipRabotnoVreme: TDataSource
    DataSet = tblTipRabotnoVreme
    Left = 507
    Top = 539
  end
  object tblReVoFirma: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_VID_STRANSKI_JAZIK'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_VID_STRANSKI_JAZIK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_VID_STRANSKI_JAZIK('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    mr.id,'
      '    mr.naziv,'
      '    mr.tip_partner,'
      '    mr.partner,'
      '    mr.koren,'
      '    mr.spisok,'
      '    mr.re,'
      '    mr.poteklo,'
      '    mr.rakovoditel,'
      '    mr.r,'
      '    mr.m,'
      '    mr.t'
      'from mat_re mr'
      
        'where (((mr.poteklo||'#39'%'#39' like  '#39'%,'#39'||:re ||'#39',%'#39')and (not mr.pote' +
        'klo like :re||'#39','#39')) or'
      '      (mr.poteklo like :re||'#39',%'#39'))'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    mr.id,'
      '    mr.naziv,'
      '    mr.tip_partner,'
      '    mr.partner,'
      '    mr.koren,'
      '    mr.spisok,'
      '    mr.re,'
      '    mr.poteklo,'
      '    mr.rakovoditel,'
      '    mr.r,'
      '    mr.m,'
      '    mr.t'
      'from mat_re mr'
      
        'where (((mr.poteklo||'#39'%'#39' like  '#39'%,'#39'||:re ||'#39',%'#39')and (not mr.pote' +
        'klo like :re||'#39','#39')) or'
      '      (mr.poteklo like :re||'#39',%'#39'))'
      '--where mr.poteklo like :re||'#39',%'#39' and mr.r=1')
    AutoUpdateOptions.UpdateTableName = 'HR_VID_STRANSKI_JAZIK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_VID_STRANSKI_JAZIK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 547
    oRefreshDeletedRecord = True
    object tblReVoFirmaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblReVoFirmaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReVoFirmaTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblReVoFirmaPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblReVoFirmaKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblReVoFirmaSPISOK: TFIBStringField
      FieldName = 'SPISOK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReVoFirmaRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblReVoFirmaPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReVoFirmaRAKOVODITEL: TFIBStringField
      FieldName = 'RAKOVODITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReVoFirmaR: TFIBSmallIntField
      FieldName = 'R'
    end
    object tblReVoFirmaM: TFIBSmallIntField
      FieldName = 'M'
    end
    object tblReVoFirmaT: TFIBSmallIntField
      FieldName = 'T'
    end
  end
  object dsReVoFirma: TDataSource
    DataSet = tblReVoFirma
    Left = 115
    Top = 547
  end
  object pFIBDataSet1: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RM_OGLAS'
      'SET '
      '    ID_OGLAS = :ID_OGLAS,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    EMAIL = :EMAIL,'
      '    URL = :URL,'
      '    TEL = :TEL,'
      '    OPIS = :OPIS,'
      '    BR_RABOTNI_IMESTA = :BR_RABOTNI_IMESTA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_RM_OGLAS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_RM_OGLAS('
      '    ID,'
      '    ID_OGLAS,'
      '    ID_RM_RE,'
      '    EMAIL,'
      '    URL,'
      '    TEL,'
      '    OPIS,'
      '    BR_RABOTNI_IMESTA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_OGLAS,'
      '    :ID_RM_RE,'
      '    :EMAIL,'
      '    :URL,'
      '    :TEL,'
      '    :OPIS,'
      '    :BR_RABOTNI_IMESTA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    ho.broj broj_oglas,'
      '    hro.id,'
      
        '    ho.broj || '#39'/'#39' || extractyear(ho.datum_objaven) as BrGodinaO' +
        'glas,'
      '    hro.id_oglas,'
      
        '    '#39' '#1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' '#39'||ho.broj||'#39' '#1086#1076' '#1076#1072#1090#1091#1084' '#39'||ho.datum_objaven o' +
        'glas,'
      '    hro.id_rm_re,'
      '    hrm.naziv naziv_rm,'
      '    m.naziv as naziv_re,'
      '    hro.email,'
      '    hro.url,'
      '    hro.tel,'
      '    hro.opis,'
      '    hro.br_rabotni_imesta,'
      '    hro.ts_ins,'
      '    hro.ts_upd,'
      '    hro.usr_ins,'
      '    hro.usr_upd'
      'from hr_rm_oglas hro'
      'inner join hr_evidencija_oglas ho on ho.id=hro.id_oglas'
      'inner join hr_rm_re hrr on hrr.id=hro.id_rm_re'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join mat_re m on m.id = hrr.id_re'
      'where(  hro.id_oglas like :id --and ho.id_re_firma like :firma'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      'order by hrm.naziv')
    SelectSQL.Strings = (
      'select'
      '    ho.broj broj_oglas,'
      '    hro.id,'
      
        '    ho.broj || '#39'/'#39' || extractyear(ho.datum_objaven) as BrGodinaO' +
        'glas,'
      '    hro.id_oglas,'
      
        '    '#39' '#1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' '#39'||ho.broj||'#39' '#1086#1076' '#1076#1072#1090#1091#1084' '#39'||ho.datum_objaven o' +
        'glas,'
      '    hro.id_rm_re,'
      '    hrm.naziv naziv_rm,'
      '    m.naziv as naziv_re,'
      '    hro.email,'
      '    hro.url,'
      '    hro.tel,'
      '    hro.opis,'
      '    hro.br_rabotni_imesta,'
      '    hro.ts_ins,'
      '    hro.ts_upd,'
      '    hro.usr_ins,'
      '    hro.usr_upd'
      'from hr_rm_oglas hro'
      'inner join hr_evidencija_oglas ho on ho.id=hro.id_oglas'
      'inner join hr_rm_re hrr on hrr.id=hro.id_rm_re'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join mat_re m on m.id = hrr.id_re'
      'where hro.id_oglas like :id --and ho.id_re_firma like :firma'
      'order by hrm.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_OGLAS'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_OGLAS_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsOglas
    Left = 232
    Top = 335
    oRefreshDeletedRecord = True
    object FIBIntegerField1: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object FIBIntegerField2: TFIBIntegerField
      DisplayLabel = #1054#1075#1083#1072#1089' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_OGLAS'
    end
    object FIBIntegerField3: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1052' '#1074#1086' '#1056#1045
      FieldName = 'ID_RM_RE'
    end
    object FIBStringField1: TFIBStringField
      DisplayLabel = 'E-MAIL'
      FieldName = 'EMAIL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'URL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField3: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TEL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBDateTimeField1: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object FIBDateTimeField2: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object FIBStringField5: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField6: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField7: TFIBStringField
      FieldName = 'OGLAS'
      Size = 46
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField8: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      FieldName = 'BRGODINAOGLAS'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField9: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBIntegerField4: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      FieldName = 'BROJ_OGLAS'
    end
    object FIBSmallIntField1: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
      FieldName = 'BR_RABOTNI_IMESTA'
    end
    object FIBStringField10: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
end
