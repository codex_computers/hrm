unit dmReportUnit;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, frxClass, frxDesgn, frxDBSet,
  frxRich, frxDCtrl, frxExportRTF, Dialogs;

type
  TdmReport = class(TDataModule)
    tblReportDizajn2: TpFIBDataSet;
    tblReportDizajn2SQL: TFIBBlobField;
    tblReportDizajn2DATA: TFIBBlobField;
    tblSqlReport: TpFIBDataSet;
    frxDBDataset1: TfrxDBDataset;
    frxDesigner1: TfrxDesigner;
    frxReport1: TfrxReport;
    tblDetail: TpFIBDataSet;
    frxDBDataset2: TfrxDBDataset;
    dsMaster: TDataSource;
    frxRichObject1: TfrxRichObject;
    frxDialogControls1: TfrxDialogControls;
    frxBaranjeZaGO: TfrxDBDataset;
    dsTemplate: TDataSource;
    Template: TpFIBDataSet;
    TemplateID: TFIBIntegerField;
    TemplateVID: TFIBIntegerField;
    TemplateNAZIV: TFIBStringField;
    TemplateREPORT: TFIBBlobField;
    TemplateTS_INS: TFIBDateTimeField;
    TemplateTS_UPD: TFIBDateTimeField;
    TemplateUSR_UPD: TFIBStringField;
    TemplateUSR_INS: TFIBStringField;
    TemplateBROJ: TFIBIntegerField;
    frxDogovorVrabotuvanje: TfrxDBDataset;
    DogovorZaVrabotuvanje: TpFIBDataSet;
    BaranjeZaGO: TpFIBDataSet;
    BaranjeZaGOID: TFIBIntegerField;
    BaranjeZaGOMB: TFIBStringField;
    BaranjeZaGOGODINA: TFIBIntegerField;
    BaranjeZaGODATUM: TFIBDateField;
    BaranjeZaGOVID_DOCUMENT: TFIBIntegerField;
    BaranjeZaGOVIDDOKUMENTNAZIV: TFIBStringField;
    BaranjeZaGOODOBRENIE: TFIBSmallIntField;
    BaranjeZaGOKORISTENJE: TFIBIntegerField;
    BaranjeZaGOPRV_DEL: TFIBIntegerField;
    BaranjeZaGOVTOR_DEL: TFIBIntegerField;
    BaranjeZaGOMB_RAKOVODITEL: TFIBStringField;
    BaranjeZaGONAZIVVRABOTEN: TFIBStringField;
    BaranjeZaGORMNAZIV: TFIBStringField;
    BaranjeZaGOOdobrenieNaziv: TFIBStringField;
    BaranjeZaGOKoristenjeNaziv: TFIBStringField;
    frxResenieGO: TfrxDBDataset;
    ResenieZaGO: TpFIBDataSet;
    ResenieZaPreraspredelba: TpFIBDataSet;
    ResenieZaPreraspredelbaID: TFIBIntegerField;
    ResenieZaPreraspredelbaVID_DOKUMENT: TFIBIntegerField;
    ResenieZaPreraspredelbaVIDDOKUMENTNAZIV: TFIBStringField;
    ResenieZaPreraspredelbaMB: TFIBStringField;
    ResenieZaPreraspredelbaVRABOTENNAZIV: TFIBStringField;
    ResenieZaPreraspredelbaDATUM_OD: TFIBDateField;
    ResenieZaPreraspredelbaDATUM_DO: TFIBDateField;
    ResenieZaPreraspredelbaNOVO_RM: TFIBIntegerField;
    ResenieZaPreraspredelbaSTARO_RM: TFIBIntegerField;
    ResenieZaPreraspredelbaSTATUS: TFIBSmallIntField;
    ResenieZaPreraspredelbaSTARONAZIV: TFIBStringField;
    ResenieZaPreraspredelbaNOVONAZIV: TFIBStringField;
    frxResenieZaPreraspredelba: TfrxDBDataset;
    frxPrekinRabotenOdnos: TfrxDBDataset;
    PrestanokNaRabotenOdnos: TpFIBDataSet;
    PrestanokNaRabotenOdnosID: TFIBIntegerField;
    PrestanokNaRabotenOdnosMB: TFIBStringField;
    PrestanokNaRabotenOdnosID_RE_FIRMA: TFIBIntegerField;
    PrestanokNaRabotenOdnosID_RM_RE: TFIBIntegerField;
    PrestanokNaRabotenOdnosDATUM_DO: TFIBDateField;
    PrestanokNaRabotenOdnosOBRAZLOZENIE: TFIBStringField;
    PrestanokNaRabotenOdnosPRICINA: TFIBIntegerField;
    PrestanokNaRabotenOdnosDIREKTOR: TFIBStringField;
    PrestanokNaRabotenOdnosLICENAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosRABOTNOMESONAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosRENAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosPRICINANAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosTS_INS: TFIBDateTimeField;
    PrestanokNaRabotenOdnosTS_UPD: TFIBDateTimeField;
    PrestanokNaRabotenOdnosUSR_INS: TFIBStringField;
    PrestanokNaRabotenOdnosUSR_UPD: TFIBStringField;
    PrestanokNaRabotenOdnosDATA: TFIBBlobField;
    OpenDialog1: TOpenDialog;
    Sistematizacija: TpFIBDataSet;
    SistematizacijaID: TFIBIntegerField;
    SistematizacijaOPIS: TFIBStringField;
    SistematizacijaOD_DATUM: TFIBDateField;
    SistematizacijaDO_DATUM: TFIBDateField;
    SistematizacijaDOKUMENT: TFIBStringField;
    SistematizacijaTS_INS: TFIBDateTimeField;
    SistematizacijaTS_UPD: TFIBDateTimeField;
    SistematizacijaUSR_INS: TFIBStringField;
    SistematizacijaUSR_UPD: TFIBStringField;
    SistematizacijaID_RE_FIRMA: TFIBIntegerField;
    SistematizacijaDATA: TFIBBlobField;
    frxSistematizacija: TfrxDBDataset;
    ResenieZaGOID: TFIBIntegerField;
    ResenieZaGOMB: TFIBStringField;
    ResenieZaGOGODINA: TFIBIntegerField;
    ResenieZaGODATUM: TFIBDateField;
    ResenieZaGOVID_DOKUMENT: TFIBIntegerField;
    ResenieZaGOVIDDOKUMENTNAZIV: TFIBStringField;
    ResenieZaGOKORISTENJE: TFIBSmallIntField;
    ResenieZaGOPRV_DEL: TFIBIntegerField;
    ResenieZaGOVTOR_DEL: TFIBIntegerField;
    ResenieZaGOMB_RAKOVODITEL: TFIBStringField;
    ResenieZaGONAZIVVRABOTEN: TFIBStringField;
    ResenieZaGORMNAZIV: TFIBStringField;
    ResenieZaGORENAZIV: TFIBStringField;
    ResenieZaGORE: TFIBIntegerField;
    ResenieZaGODATUM1_OD: TFIBDateField;
    ResenieZaGODATUM1_DO: TFIBDateField;
    ResenieZaGODATUM2_OD: TFIBDateField;
    ResenieZaGODATUM2_DO: TFIBDateField;
    ResenieZaGOKoristenjeNaziv: TFIBStringField;
    ResenieZaGOBARANJE_GO: TFIBIntegerField;
    ResenieZaGOTS_INS: TFIBDateTimeField;
    ResenieZaGOTS_UPD: TFIBDateTimeField;
    ResenieZaGOUSR_INS: TFIBStringField;
    ResenieZaGOUSR_UPD: TFIBStringField;
    ResenieZaGOBROJ: TFIBStringField;
    ResenieZaGOMB_ZAMENA: TFIBStringField;
    ResenieZaGOZAKONSKI_MINIMUM: TFIBIntegerField;
    ResenieZaGOSTAZ: TFIBSmallIntField;
    ResenieZaGOSTAZ_DENOVI: TFIBSmallIntField;
    ResenieZaGOSTEPEN_NA_SLOZENOST: TFIBSmallIntField;
    ResenieZaGOSZ_NAZIV: TFIBStringField;
    ResenieZaGOPOSEBNI_USLOVI: TFIBSmallIntField;
    ResenieZaGOPOL_VOZRAST: TFIBSmallIntField;
    ResenieZaGOZDRASTVENA_SOSTOJBA: TFIBSmallIntField;
    ResenieZaGODATA: TFIBBlobField;
    frxAneks: TfrxDBDataset;
    DogovorZaVrabotuvanjeID: TFIBIntegerField;
    DogovorZaVrabotuvanjeDATUM: TFIBDateField;
    DogovorZaVrabotuvanjeBROJ: TFIBStringField;
    DogovorZaVrabotuvanjeVID_DOKUMENT: TFIBIntegerField;
    DogovorZaVrabotuvanjeNAZIV_VID_DOKUMENT: TFIBStringField;
    DogovorZaVrabotuvanjeMB: TFIBStringField;
    DogovorZaVrabotuvanjeID_RE_FIRMA: TFIBIntegerField;
    DogovorZaVrabotuvanjePREZIME: TFIBStringField;
    DogovorZaVrabotuvanjeTATKOVO_IME: TFIBStringField;
    DogovorZaVrabotuvanjeIME: TFIBStringField;
    DogovorZaVrabotuvanjeADRESA: TFIBStringField;
    DogovorZaVrabotuvanjeMESTO: TFIBIntegerField;
    DogovorZaVrabotuvanjeSTEPEN_STRUCNA_PODGOTOVKA: TFIBIntegerField;
    DogovorZaVrabotuvanjeVID_VRABOTUVANJE: TFIBIntegerField;
    DogovorZaVrabotuvanjeNAZIV_VID_VRAB: TFIBStringField;
    DogovorZaVrabotuvanjeDATUM_OD: TFIBDateField;
    DogovorZaVrabotuvanjeDATUM_DO: TFIBDateField;
    DogovorZaVrabotuvanjeRABOTNO_VREME: TFIBBCDField;
    DogovorZaVrabotuvanjeRABOTNO_MESTO: TFIBIntegerField;
    DogovorZaVrabotuvanjeNAZIV_MESTO: TFIBStringField;
    DogovorZaVrabotuvanjeOBRAZOVANIE: TFIBStringField;
    DogovorZaVrabotuvanjeRABMESTONAZIV: TFIBStringField;
    DogovorZaVrabotuvanjeMB_DIREKTOR: TFIBStringField;
    DogovorZaVrabotuvanjePLATA: TFIBBCDField;
    DogovorZaVrabotuvanjeSTATUS: TFIBSmallIntField;
    DogovorZaVrabotuvanjeDATA: TFIBBlobField;
    DogovorZaVrabotuvanjeGRUPAOZNAKA: TFIBStringField;
    DogovorZaVrabotuvanjeRAKOVODENJEPROCENT: TFIBBCDField;
    DogovorZaVrabotuvanjeSZNAZIV: TFIBStringField;
    DogovorZaVrabotuvanjeSTAZ_GODINA: TFIBBCDField;
    DogovorZaVrabotuvanjePROCENTSTAZKOL: TFIBBCDField;
    DogovorZaVrabotuvanjeRABEDINICANAZIV: TFIBStringField;
    frxDBDogVolonteri: TfrxDBDataset;
    tblDogVolonteriodberi: TpFIBDataSet;
    tblDogVolonteriodberiID: TFIBIntegerField;
    tblDogVolonteriodberiMB: TFIBStringField;
    tblDogVolonteriodberiPREZIME: TFIBStringField;
    tblDogVolonteriodberiTATKOVO_IME: TFIBStringField;
    tblDogVolonteriodberiIME: TFIBStringField;
    tblDogVolonteriodberiNAZIVVRABOTEN: TFIBStringField;
    tblDogVolonteriodberiNAZIVRABMESTO: TFIBStringField;
    tblDogVolonteriodberiID_RM_RE: TFIBIntegerField;
    tblDogVolonteriodberiVID_DOKUMENT: TFIBIntegerField;
    tblDogVolonteriodberiID_RE_FIRMA: TFIBIntegerField;
    tblDogVolonteriodberiDATUM_OD: TFIBDateField;
    tblDogVolonteriodberiDATUM_DO: TFIBDateField;
    tblDogVolonteriodberiPLATA: TFIBBCDField;
    tblDogVolonteriodberiBROJ_CASOVI: TFIBSmallIntField;
    tblDogVolonteriodberiBROJ: TFIBStringField;
    tblDogVolonteriodberiRAKOVODITEL: TFIBStringField;
    tblDogVolonteriodberiTS_INS: TFIBDateTimeField;
    tblDogVolonteriodberiTS_UPD: TFIBDateTimeField;
    tblDogVolonteriodberiUSR_INS: TFIBStringField;
    tblDogVolonteriodberiUSR_UPD: TFIBStringField;
    tblDogVolonteriodberiDATA: TFIBBlobField;
    tblDogVolonteriodberiDATUM: TFIBDateField;
    tblDogVolonteriodberiLICNA_KARTA: TFIBStringField;
    frxOdjava: TfrxDBDataset;
    frxDBPrijava: TfrxDBDataset;
    frxJSOpstiPodatoci: TfrxDBDataset;
    DogovorZaVrabotuvanjeOPIS_RABOTNO_MESTO: TFIBStringField;
    DogovorZaVrabotuvanjeUSLOVIRABOTANAZIV: TFIBStringField;
    ResenieZaGOSUMAGO: TFIBIntegerField;
    frxPotvrdaRabotenOdnos: TfrxDBDataset;
    PotvrdaRabotenOdnos: TpFIBDataSet;
    PotvrdaRabotenOdnosID: TFIBIntegerField;
    PotvrdaRabotenOdnosMB: TFIBStringField;
    PotvrdaRabotenOdnosARHIVSKI_BROJ: TFIBStringField;
    PotvrdaRabotenOdnosDATUM: TFIBDateField;
    PotvrdaRabotenOdnosGODINA: TFIBIntegerField;
    PotvrdaRabotenOdnosZABELESKA: TFIBStringField;
    PotvrdaRabotenOdnosVIDDOKUMENTNAZIV: TFIBStringField;
    PotvrdaRabotenOdnosBROJ: TFIBStringField;
    PotvrdaRabotenOdnosNAZIV_VRABOTEN: TFIBStringField;
    PotvrdaRabotenOdnosNAZIV_VRABOTEN_TI: TFIBStringField;
    PotvrdaRabotenOdnosRABOTNAEDINICANAZIV: TFIBStringField;
    PotvrdaRabotenOdnosRABOTNOMESTONAZIV: TFIBStringField;
    PotvrdaRabotenOdnosDATA: TFIBBlobField;
    frxRTFExport1: TfrxRTFExport;
    BaranjeZaGODATA: TFIBBlobField;
    BaranjeZaGODATUM1_OD: TFIBDateField;
    BaranjeZaGODATUM1_DO: TFIBDateField;
    BaranjeZaGODATUM2_OD: TFIBDateField;
    BaranjeZaGODATUM2_DO: TFIBDateField;
    tblAneksReport: TpFIBDataSet;
    tblAneksReportBROJANEKS: TFIBStringField;
    tblAneksReportDATUM: TFIBDateField;
    tblAneksReportDATUM_POCETOK: TFIBDateField;
    tblAneksReportDATUM_DO: TFIBDateField;
    tblAneksReportBROJ_CASOVI: TFIBBCDField;
    tblAneksReportRAKOVODITEL: TFIBStringField;
    tblAneksReportKOEFICIENT: TFIBBCDField;
    tblAneksReportRAKOVODENJE: TFIBBCDField;
    tblAneksReportUSLOVI_RABOTA: TFIBIntegerField;
    tblAneksReportDEN_USLOVI_RABOTA: TFIBIntegerField;
    tblAneksReportPLATA: TFIBBCDField;
    tblAneksReportMB: TFIBStringField;
    tblAneksReportBROJDOGOVOR: TFIBStringField;
    tblAneksReportNAZIV_VRABOTEN: TFIBStringField;
    tblAneksReportNAZIV_VRABOTEN_TI: TFIBStringField;
    tblAneksReportVIDVRABOTUVANJE: TFIBStringField;
    tblAneksReportARHIVSKI_BROJ: TFIBStringField;
    tblAneksReportRABOTNOMESTONAZIV: TFIBStringField;
    tblAneksReportADRESA: TFIBStringField;
    tblAneksReportBROJODADRESA: TFIBStringField;
    tblAneksReportMESTONAZIV: TFIBStringField;
    tblAneksReportOPIS: TFIBBlobField;
    tblAneksReportDATA: TFIBBlobField;
    frxVozrastNaDatum: TfrxDBDataset;
    ResenieZaGOCUVANJE_DETE: TFIBSmallIntField;
    ResenieZaGOINVALID_DENOVI: TFIBSmallIntField;
    ResenieZaGOSUMPOSEBNIUSLOVI: TFIBBCDField;
    tblAneksReportUSLOVIRABNAZIV: TFIBStringField;
    DogovorZaVrabotuvanjeBENEFICIRAN_STAZ: TFIBBCDField;
    DogovorZaVrabotuvanjeDEN_USLOVI_RABOTA: TFIBIntegerField;
    BaranjeZaGORENAZIV: TFIBStringField;
    ResenieZaPreraspredelbaBROJ: TFIBStringField;
    ResenieZaPreraspredelbaDATUM_KREIRANJE: TFIBDateField;
    ResenieZaPreraspredelbaDOGOVOR_BROJ: TFIBStringField;
    ResenieZaPreraspredelbaDATA: TFIBBlobField;
    tblAneksReportID: TFIBIntegerField;
    tblAneksReportRABOTNAEDINICANAZIV: TFIBStringField;
    tblAneksReportSTEPENSTRUCNAPODGOTOVKANAZIV: TFIBStringField;
    tblAneksReportSTEPENZAVRSENOOBRAZOVANIE: TFIBStringField;
    tblAneksReportSTEPENZAVRSENOOBRAZOVANIEOPIS: TFIBStringField;
    tblAneksReportNASOKA: TFIBIntegerField;
    tblAneksReportNASOKANAZIV: TFIBStringField;
    tblAneksReportZNAENJE: TFIBStringField;
    DogovorZaVrabotuvanjeNASOKANAZIV: TFIBStringField;
    DogovorZaVrabotuvanjeZNAENJE: TFIBStringField;
    ResenieZaPreraspredelbaZVANJE: TFIBStringField;
    PrestanokNaRabotenOdnosZVANJE: TFIBStringField;
    tblDogVolonteriodberiZVANJE: TFIBStringField;
    ResenieZaGOZVANJE: TFIBStringField;
    tblAneksReportZVANJE: TFIBStringField;
    DogovorZaVrabotuvanjeZVANJE: TFIBStringField;
    frxizjavaProdolzuvanjeRO: TfrxDBDataset;
    tblizjavaProdolzuvanjeRO: TpFIBDataSet;
    tblizjavaProdolzuvanjeROID: TFIBIntegerField;
    tblizjavaProdolzuvanjeROMB: TFIBStringField;
    tblizjavaProdolzuvanjeROARHIVSKI_BROJ: TFIBStringField;
    tblizjavaProdolzuvanjeRODATUM: TFIBDateField;
    tblizjavaProdolzuvanjeRODATUM_OD: TFIBDateField;
    tblizjavaProdolzuvanjeRODATUM_DO: TFIBDateField;
    tblizjavaProdolzuvanjeROGODINA: TFIBIntegerField;
    tblizjavaProdolzuvanjeROZABELESKA: TFIBStringField;
    tblizjavaProdolzuvanjeROVIDDOKUMENTNAZIV: TFIBStringField;
    tblizjavaProdolzuvanjeROBROJ: TFIBStringField;
    tblizjavaProdolzuvanjeRONAZIV_VRABOTEN: TFIBStringField;
    tblizjavaProdolzuvanjeRONAZIV_VRABOTEN_TI: TFIBStringField;
    tblizjavaProdolzuvanjeRORABOTNAEDINICANAZIV: TFIBStringField;
    tblizjavaProdolzuvanjeRORABOTNOMESTONAZIV: TFIBStringField;
    tblizjavaProdolzuvanjeRODATA: TFIBBlobField;
    frxOtsustvaPoGodina: TfrxDBDataset;
    tblOtsustvaPoGodina: TpFIBDataSet;
    tblOtsustvaPoGodinaMB: TFIBStringField;
    tblOtsustvaPoGodinaNAZIV_VRABOTEN: TFIBStringField;
    tblOtsustvaPoGodinaOTSUSTVO_ID: TFIBIntegerField;
    tblOtsustvaPoGodinaRESENIE_ID: TFIBIntegerField;
    tblOtsustvaPoGodinaID: TFIBIntegerField;
    tblOtsustvaPoGodinaGODINA: TFIBSmallIntField;
    tblOtsustvaPoGodinaGODINA_RESENIE: TFIBIntegerField;
    tblOtsustvaPoGodinaOD_VREME: TFIBStringField;
    tblOtsustvaPoGodinaDO_VREME: TFIBStringField;
    tblOtsustvaPoGodinaDENOVI: TFIBIntegerField;
    tblOtsustvaPoGodinaDENOVI_PO_RESENIE: TFIBIntegerField;
    tblOtsustvaPoGodinaRABOTNAEDINICANAZIV: TFIBStringField;
    tblResenieZaPreraspredelbaID_DOG: TFIBIntegerField;
    tblResenieZaPreraspredelbaDOG_DATUM: TFIBDateField;
    tblDogovorZaVrabotuvanjeBODRAKOVODENJE_PROCENT: TFIBBCDField;
    tblDogovorZaVrabotuvanjeBODRAKOVODENJE: TFIBBCDField;
    tblDogovorZaVrabotuvanjeBODSTEPENSLOZENOST: TFIBBCDField;
    tblDogovorZaVrabotuvanjeBODOVISTAZ: TFIBBCDField;
    tblDogovorZaVrabotuvanjeVKUPNOBODOVI: TFIBBCDField;
    tblDogovorZaVrabotuvanjeUSLOVI_RABOTA_PROCENT: TFIBBCDField;
    tblDogovorZaVrabotuvanjeUSLOVI_RABOTA: TFIBBCDField;
    tblReseniePreraspredelbaTemplate: TpFIBDataSet;
    frxReseniePreraspredelbaTemplate: TfrxDBDataset;
    tblReseniePreraspredelbaTemplateDATUM_KREIRANJE: TFIBDateField;
    tblReseniePreraspredelbaTemplateIME: TFIBStringField;
    tblReseniePreraspredelbaTemplatePREZIME: TFIBStringField;
    tblReseniePreraspredelbaTemplateSTEPENSTRUCNAPODGOTOVKA: TFIBStringField;
    tblReseniePreraspredelbaTemplateSTARORABMESTONAZIV: TFIBStringField;
    tblReseniePreraspredelbaTemplateSTARARABEDINICA: TFIBStringField;
    tblReseniePreraspredelbaTemplateNOVORABMESTONAZIV: TFIBStringField;
    tblReseniePreraspredelbaTemplateNOVORABEDINICA: TFIBStringField;
    tblReseniePreraspredelbaTemplateBODSTEPENSLOZENOST: TFIBBCDField;
    tblReseniePreraspredelbaTemplateUSLOVI_RABOTA_PROCENT: TFIBBCDField;
    tblReseniePreraspredelbaTemplateUSLOVI_RABOTA: TFIBBCDField;
    tblReseniePreraspredelbaTemplateBODRAKOVODENJE_PROCENT: TFIBBCDField;
    tblReseniePreraspredelbaTemplateBODRAKOVODENJE: TFIBBCDField;
    tblReseniePreraspredelbaTemplatestaz_godina: TFIBBCDField;
    tblReseniePreraspredelbaTemplatestaz_meseci: TFIBBCDField;
    tblReseniePreraspredelbaTemplatePROCENTSTAZKOLDOG: TFIBBCDField;
    tblReseniePreraspredelbaTemplateBODOVISTAZ: TFIBBCDField;
    tblReseniePreraspredelbaTemplateSTAZ_DENOVI: TFIBIntegerField;
    tblReseniePreraspredelbaTemplateBROJ: TFIBStringField;
    tblReseniePreraspredelbaTemplateVKUPNO: TFIBBCDField;
    tblReseniePreraspredelbaTemplateDATA: TFIBBlobField;
    tblDogovorZaVrabotuvanjeBODOVI: TFIBBCDField;
    tblDogovorZaVrabotuvanjeNETO: TFIBBCDField;
    tblAneksReportNETO: TFIBBCDField;
    tblAneksReportBODOVI: TFIBBCDField;
    tblDogovorZaVrabotuvanjeID_RE_FIRMA_NAZIV: TFIBStringField;
    tblPrestanokNaRabotenOdnosID_RE_FIRMA_NAZIV: TFIBStringField;
    tblBaranjeZaGOID_RE_FIRMA_NAZIV: TFIBStringField;
    tblResenieZaGOID_RE_FIRMA_NAZIV: TFIBStringField;
    function frxDesigner1SaveReport(Report: TfrxReport;
      SaveAs: Boolean): Boolean;
    function frxDesignSaveReport(Report: TfrxReport;
    SaveAs: Boolean; tag: integer): Boolean;
    procedure frxDBDataset1Close(Sender: TObject);
    procedure frxDBDataset1First(Sender: TObject);
    procedure frxDBDataset1Next(Sender: TObject);
    procedure frxDBDataset1Open(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Spremi(id: Integer; app:string);
    procedure BrisiStarReport(id: Integer);
  end;

var
  dmReport: TdmReport;

implementation

uses dmKonekcija, dmUnit;



{$R *.dfm}

procedure TdmReport.frxDBDataset1Close(Sender: TObject);
begin
//  frxDBDataset2.Close;
//  frxTblDetailTuzeniSmetki.Close;
end;

procedure TdmReport.frxDBDataset1First(Sender: TObject);
begin
//  frxDBDataset2.First;
//  frxTblDetailTuzeniSmetki.First;
end;

procedure TdmReport.frxDBDataset1Next(Sender: TObject);
begin
//  frxDBDataset2.Open;
//  frxTblDetailTuzeniSmetki.Open;
end;

procedure TdmReport.frxDBDataset1Open(Sender: TObject);
begin
//  frxDBDataset2.Open;
//  frxTblDetailTuzeniSmetki.Open;
end;

function TdmReport.frxDesigner1SaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var template : TStream;
begin
      template := TMemoryStream.Create;
      template.Position := 0;
      dmReport.frxReport1.SaveToStream(template);
      if frxDesigner1.Tag = 7 then
         begin
            dmReport.Sistematizacija.Open;
            dmReport.Sistematizacija.edit;
            (dmReport.SistematizacijaDATA as TBlobField).LoadFromStream(template);
            dmReport.Sistematizacija.Post;
         end
      else
          begin
             dmReport.tblReportDizajn2.Open;
             dmReport.tblReportDizajn2.edit;
            (dmReport.tblReportDizajn2DATA as TBlobField).LoadFromStream(template);
             dmReport.tblReportDizajn2.Post;
          end ;
end;

function TdmReport.frxDesignSaveReport(Report: TfrxReport;
  SaveAs: Boolean; tag: integer): Boolean;
var template : TStream;
begin
  if tag = 1 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        BaranjeZaGO.Open;
        BaranjeZaGO.edit;
        (BaranjeZaGODATA as TBlobField).LoadFromStream(template);
        BaranjeZaGO.Post;
     end;

  if tag = 2 then
     begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        DogovorZaVrabotuvanje.Open;
        DogovorZaVrabotuvanje.edit;
        (DogovorZaVrabotuvanjeDATA as TBlobField).LoadFromStream(template);
        DogovorZaVrabotuvanje.Post;
     end;

  if tag = 3 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        ResenieZaGO.Open;
        ResenieZaGO.edit;
        (ResenieZaGODATA as TBlobField).LoadFromStream(template);
        ResenieZaGO.Post;
     end;

    if tag = 4 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        tblReseniePreraspredelbaTemplate.Open;
        tblReseniePreraspredelbaTemplate.edit;
        (tblReseniePreraspredelbaTemplateDATA as TBlobField).LoadFromStream(template);
        tblReseniePreraspredelbaTemplate.Post;
     end;

    if tag = 5 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        PrestanokNaRabotenOdnos.Open;
        PrestanokNaRabotenOdnos.edit;
        (PrestanokNaRabotenOdnosDATA as TBlobField).LoadFromStream(template);
        PrestanokNaRabotenOdnos.Post;
     end;

    if tag = 6 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        Sistematizacija.Open;
        Sistematizacija.edit;
        (SistematizacijaDATA as TBlobField).LoadFromStream(template);
        Sistematizacija.Post;
     end;

    if tag = 8 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
       // dmReport.tblAneksReport.Open;
        dm.tblAneks.Edit;
        (dm.tblAneksDATA as TBlobField).LoadFromStream(template);
        dm.tblAneks.Post;
     end;
    if tag = 9 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        tblDogVolonteriodberi.Open;
        tblDogVolonteriodberi.Edit;
        (tblDogVolonteriodberiDATA as TBlobField).LoadFromStream(template);
        tblDogVolonteriodberi.Post;
     end;
   if tag = 10 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        PotvrdaRabotenOdnos.Open;
        PotvrdaRabotenOdnos.edit;
        (PotvrdaRabotenOdnosDATA as TBlobField).LoadFromStream(template);
        PotvrdaRabotenOdnos.Post;
     end;
   if tag = 11 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        tblizjavaProdolzuvanjeRO.Open;
        tblizjavaProdolzuvanjeRO.edit;
        (tblizjavaProdolzuvanjeRODATA as TBlobField).LoadFromStream(template);
        tblizjavaProdolzuvanjeRO.Post;
     end;
end;

procedure TdmReport.Spremi(id :Integer ; app:string);
var
  RptStream: TStream;
  SqlStream: TStream;
begin
  frxReport1.Clear;
  frxReport1.Script.Clear;

  tblReportDizajn2.Close;
  tblReportDizajn2.Params.ParamByName('br').Value := id;
  tblReportDizajn2.Params.ParamByName('app').Value:= app;
  tblReportDizajn2.Open;

  tblSqlReport.Close;
  SqlStream := tblReportDizajn2.CreateBlobStream(tblReportDizajn2SQL , bmRead);
  tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);

  RptStream := tblReportDizajn2.CreateBlobStream(tblReportDizajn2DATA , bmRead);
  frxReport1.LoadFromStream(RptStream);

  frxDesigner1.Tag:=0;
end;

procedure TdmReport.BrisiStarReport(id :Integer);
var
  SqlStream: TStream;
begin
  tblReportDizajn2.Close;
  tblReportDizajn2.Params.ParamByName('id').Value := id;
  tblReportDizajn2.Open;

  tblSqlReport.Close;
  SqlStream := tblReportDizajn2.CreateBlobStream(tblReportDizajn2SQL , bmRead);
  tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);
end;

end.
