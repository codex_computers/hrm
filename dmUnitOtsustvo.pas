unit dmUnitOtsustvo;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 28.02.2012                                }
{                                                       }
{     ������ : 1.1.1.18                                }
{                                                       }
{*******************************************************}

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, Variants, Controls, FIBQuery,
  pFIBQuery, pFIBStoredProc, FIBDatabase, pFIBDatabase;


type
  TdmOtsustvo = class(TDataModule)
    dsNacionalnost: TDataSource;
    tblNacionalnost: TpFIBDataSet;
    tblVeroispoved: TpFIBDataSet;
    dsVeroispoved: TDataSource;
    tblPraznici: TpFIBDataSet;
    dsPraznici: TDataSource;
    tblPrazniciID: TFIBIntegerField;
    tblPrazniciDATUM: TFIBDateField;
    tblPrazniciNAZIV: TFIBStringField;
    tblPrazniciVEROISPOVED: TFIBIntegerField;
    tblPrazniciVEROISPOVEDNAZIV: TFIBStringField;
    tblPrazniciNACIONALNOST: TFIBIntegerField;
    tblPrazniciNACIONALNOSTNAZIV: TFIBStringField;
    tblTipOtsustvo: TpFIBDataSet;
    dsTipOtsustvo: TDataSource;
    tblNacionalnostID: TFIBIntegerField;
    tblNacionalnostNAZIV: TFIBStringField;
    tblNacionalnostTS_INS: TFIBDateTimeField;
    tblNacionalnostTS_UPD: TFIBDateTimeField;
    tblNacionalnostUSR_INS: TFIBStringField;
    tblNacionalnostUSR_UPD: TFIBStringField;
    tblVeroispovedID: TFIBIntegerField;
    tblVeroispovedNAZIV: TFIBStringField;
    tblVeroispovedTS_INS: TFIBDateTimeField;
    tblVeroispovedTS_UPD: TFIBDateTimeField;
    tblVeroispovedUSR_INS: TFIBStringField;
    tblVeroispovedUSR_UPD: TFIBStringField;
    tblTipOtsustvoID: TFIBIntegerField;
    tblTipOtsustvoNAZIV: TFIBStringField;
    tblTipOtsustvoTS_INS: TFIBDateTimeField;
    tblTipOtsustvoTS_UPD: TFIBDateTimeField;
    tblTipOtsustvoUSR_INS: TFIBStringField;
    tblTipOtsustvoUSR_UPD: TFIBStringField;
    tblPrazniciTS_INS: TFIBDateTimeField;
    tblPrazniciTS_UPD: TFIBDateTimeField;
    tblPrazniciUSR_INS: TFIBStringField;
    tblPrazniciUSR_UPD: TFIBStringField;
    tblOtsustva: TpFIBDataSet;
    dsOtsustva: TDataSource;
    tblOtsustvaID: TFIBIntegerField;
    tblOtsustvaTIP_ZAPIS: TFIBIntegerField;
    tblOtsustvaPRICINA: TFIBIntegerField;
    tblOtsustvaPRICINA_OPIS: TFIBStringField;
    tblOtsustvaOD_VREME: TFIBDateTimeField;
    tblOtsustvaDO_VREME: TFIBDateTimeField;
    tblOtsustvaTS_INS: TFIBDateTimeField;
    tblOtsustvaTS_UPD: TFIBDateTimeField;
    tblOtsustvaUSR_INS: TFIBStringField;
    tblOtsustvaUSR_UPD: TFIBStringField;
    tblOtsustvaVRABOTENPREZIME: TFIBStringField;
    tblOtsustvaVRABOTENTATKOVOIME: TFIBStringField;
    tblOtsustvaVRABOTENIME: TFIBStringField;
    tblOtsustvaOPIS: TFIBStringField;
    tblOtsustvaOPTIONS: TFIBIntegerField;
    tblOtsustvaEVENT_TYPE: TFIBIntegerField;
    tblOtsustvaTASK_LINKS_FIELD: TFIBMemoField;
    tblPodsektori: TpFIBDataSet;
    dsPodsektori: TDataSource;
    tblPodsektoriID: TFIBIntegerField;
    tblPodsektoriNAZIV: TFIBStringField;
    tblPodsektoriKOREN: TFIBIntegerField;
    tblPodsektoriSPISOK: TFIBStringField;
    tblPodsektoriRE: TFIBIntegerField;
    tblPodsektoriPOTEKLO: TFIBStringField;
    tblPodsektoriR: TFIBSmallIntField;
    tblOtsustvaPLATENO: TFIBSmallIntField;
    tblOtsustvaPlatenoDaNe: TFIBStringField;
    tblObuki: TpFIBDataSet;
    dsObuki: TDataSource;
    tblObukiID: TFIBSmallIntField;
    tblObukiNAZIV: TFIBStringField;
    tblObukiOPIS: TFIBStringField;
    tblObukiPODGOTVENOST: TFIBStringField;
    tblObukiCEL: TFIBStringField;
    tblObukiSREDSTVA: TFIBBCDField;
    tblObukiPERIOD_OD: TFIBDateField;
    tblObukiPERIOD_DO: TFIBDateField;
    tblObukiMESTO: TFIBStringField;
    tblObukiODGOVORNO_LICE: TFIBStringField;
    tblObukiZABELESKA: TFIBStringField;
    tblObukiSTATUS: TFIBIntegerField;
    tblObukiSTATUSNAZIV: TFIBStringField;
    tblObukiOPTIONS: TFIBIntegerField;
    tblObukiEVENT_TYPE: TFIBIntegerField;
    tblObukiTASK_LINKS_FIELD: TFIBMemoField;
    tblObukiSTATUS_BOJA: TFIBIntegerField;
    tblObukaUcesnici: TpFIBDataSet;
    dsObukaUcesnici: TDataSource;
    tblObukaUcesniciID: TFIBSmallIntField;
    tblObukaUcesniciOBUKA_ID: TFIBSmallIntField;
    tblObukaUcesniciMB: TFIBStringField;
    tblObukaUcesniciOCENKA: TFIBStringField;
    tblObukaUcesniciSERTIFIKATI: TFIBStringField;
    tblObukaUcesniciTS_INS: TFIBDateTimeField;
    tblObukaUcesniciTS_UPD: TFIBDateTimeField;
    tblObukaUcesniciUSR_INS: TFIBStringField;
    tblObukaUcesniciUSR_UPD: TFIBStringField;
    CountUcesnikObuka: TpFIBStoredProc;
    tblOtsustvaPregled: TpFIBDataSet;
    dsOtsustvaPregled: TDataSource;
    WorkDays: TpFIBStoredProc;
    tblOtsustvaPregledPLATENO_OUT: TFIBIntegerField;
    tblOtsustvaPregledNEPLATENO_OUT: TFIBIntegerField;
    tblOtsustvaPregledCOUNT_1_OUT: TFIBIntegerField;
    tblOtsustvaPregledCOUNT_0_OUT: TFIBIntegerField;
    tblOtsustvaPregledMB_OUT: TFIBStringField;
    tblIntBrUcesnici: TpFIBDataSet;
    dsIntBrUcesnici: TDataSource;
    tblIntBrUcesniciID: TFIBIntegerField;
    tblIntBrUcesniciNAZIV: TFIBStringField;
    tblIntBrUcesniciOPIS: TFIBStringField;
    tblIntBrUcesniciTS_INS: TFIBDateTimeField;
    tblIntBrUcesniciTS_UPD: TFIBDateTimeField;
    tblIntBrUcesniciUSR_INS: TFIBStringField;
    tblIntBrUcesniciUSR_UPD: TFIBStringField;
    tblIntOblik: TpFIBDataSet;
    dsIntOblik: TDataSource;
    tblIntOblikID: TFIBIntegerField;
    tblIntOblikNAZIV: TFIBStringField;
    tblIntOblikOPIS: TFIBStringField;
    tblIntOblikTS_INS: TFIBDateTimeField;
    tblIntOblikTS_UPD: TFIBDateTimeField;
    tblIntOblikUSR_INS: TFIBStringField;
    tblIntOblikUSR_UPD: TFIBStringField;
    tblIntSelekcija: TpFIBDataSet;
    dsIntSelekcija: TDataSource;
    tblIntSelekcijaID: TFIBIntegerField;
    tblIntSelekcijaNAZIV: TFIBStringField;
    tblIntSelekcijaOPIS: TFIBStringField;
    tblIntSelekcijaTS_INS: TFIBDateTimeField;
    tblIntSelekcijaTS_UPD: TFIBDateTimeField;
    tblIntSelekcijaUSR_INS: TFIBStringField;
    tblIntSelekcijaUSR_UPD: TFIBStringField;
    tblIntervju: TpFIBDataSet;
    dsIntervju: TDataSource;
    tblIntervjuID: TFIBIntegerField;
    tblIntervjuID_MOLBA: TFIBIntegerField;
    tblIntervjuMB_INTERVJUIST: TFIBStringField;
    tblIntervjuID_OBLIK: TFIBIntegerField;
    tblIntervjuID_BROJ_UCESNICI: TFIBIntegerField;
    tblIntervjuID_SELEKCIJA: TFIBIntegerField;
    tblIntervjuOPIS_POCETOK: TFIBStringField;
    tblIntervjuOPIS_SREDINA: TFIBStringField;
    tblIntervjuOPIS_KRAJ: TFIBStringField;
    tblIntervjuTS_INS: TFIBDateTimeField;
    tblIntervjuTS_UPD: TFIBDateTimeField;
    tblIntervjuUSR_INS: TFIBStringField;
    tblIntervjuUSR_UPD: TFIBStringField;
    tblIntervjuINTERVJUISTNAZIV: TFIBStringField;
    tblIntervjuMOLBANAZIV: TFIBStringField;
    tblIntervjuBRUCESNICINAZIV: TFIBStringField;
    tblIntervjuOBLIKNAZIV: TFIBStringField;
    tblIntervjuSELEKCIJANAZIV: TFIBStringField;
    tblIntervjuDATUM: TFIBDateField;
    tblIntervjuID_RM_RE: TFIBIntegerField;
    tblIntervjuNAZIV_RM: TFIBStringField;
    tblIntervjuID_RM_RE_MOLBA: TFIBIntegerField;
    tblIntervjuNAZIV_RM_MOLBA: TFIBStringField;
    TransakcijaP: TpFIBTransaction;
    pMAXBrojKategorija: TpFIBStoredProc;
    pMaksKBroj: TpFIBQuery;
    tblIntervjuBROJ_MOLBA: TFIBIntegerField;
    tblIntervjuBROJ: TFIBIntegerField;
    tblOtsustvaMB: TFIBStringField;
    dsStatus: TDataSource;
    tblStatus: TpFIBDataSet;
    tblStatusID: TFIBIntegerField;
    tblStatusGRUPA: TFIBIntegerField;
    tblStatusNAZIV: TFIBStringField;
    tblStatusBOJA: TFIBIntegerField;
    tblOtsustvaDENOVI: TFIBIntegerField;
    tblOtsustvaPregledRENAZIV_OUT: TFIBStringField;
    tblOtsustvaPregledRABMESTONAZIV_OUT: TFIBStringField;
    tblTipOtsustvoTIPNADOMESTOPIS: TFIBStringField;
    tblTipOtsustvoTIPCASOPIS: TFIBStringField;
    tblTipOtsustvoSIFRA: TFIBSmallIntField;
    tblTipOtsustvoTIP_PLATA: TFIBSmallIntField;
    tblTipOtsustvoFIRMA: TFIBIntegerField;
    tblTipOtsustvoSIF_NAD: TFIBStringField;
    tblTipOtsustvoTERET: TFIBSmallIntField;
    tblOtsustvaFLAG: TFIBIntegerField;
    pKopirajPraznici: TpFIBStoredProc;
    pMaksBrojNull: TpFIBQuery;
    tblOtsustvaDO_VREME_L: TFIBDateTimeField;
    tblResenieOtsustva: TpFIBDataSet;
    dsResenieOtsustva: TDataSource;
    tblResenieOtsustvaID: TFIBIntegerField;
    tblResenieOtsustvaBRGODINARESENIE: TFIBStringField;
    pOstanatiDenovi: TpFIBStoredProc;
    pDatumDo: TpFIBStoredProc;
    tblOstanatiDenoviGO: TpFIBDataSet;
    dsOstanatiDenoviGO: TDataSource;
    tblOstanatiDenoviGOMB: TFIBStringField;
    tblOstanatiDenoviGOBRGODINARESENIE: TFIBStringField;
    tblOstanatiDenoviGOOSTANATIDENOVI: TFIBIntegerField;
    tblOstanatiDenoviGORESENIEDENOVI: TFIBIntegerField;
    tblOstanatiDenoviGORABOTNOMESTONAZIV: TFIBStringField;
    tblOstanatiDenoviGORESENIEID: TFIBStringField;
    qDeleteDokument: TpFIBQuery;
    tblTipOtsustvoDENOVI: TFIBSmallIntField;
    tblTipOtsustvoPLATENO: TFIBSmallIntField;
    tblTipOtsustvoPlatenoNaziv: TFIBStringField;
    tblTipOtsustvoDOGOVOR: TFIBSmallIntField;
    tblTipOtsustvoDogovorNaziv: TFIBStringField;
    pKontrolaOtsustva: TpFIBStoredProc;
    pNeplateniOstanati: TpFIBStoredProc;
    pInsertVolonteri: TpFIBStoredProc;
    qDeleteDokVolonter: TpFIBQuery;
    tblObukiDOKUMENT: TFIBStringField;
    qDeleteAneks: TpFIBQuery;
    qUpdateVrabotenRM: TpFIBQuery;
    qDeleteVrabotenRM: TpFIBQuery;
    pKopirajDogovor: TpFIBStoredProc;
    tblRealizacijaGO: TpFIBDataSet;
    dsRealizacijaGO: TDataSource;
    tblRealizacijaGOMB_OUT: TFIBStringField;
    tblRealizacijaGONAZIVVRABOTEN_OUT: TFIBStringField;
    tblRealizacijaGOPLATENO_OUT: TFIBIntegerField;
    tblRealizacijaGONEPLATENO_OUT: TFIBIntegerField;
    tblRealizacijaGOVKUPNO_TEKOVNA_GODINA: TFIBIntegerField;
    tblRealizacijaGOVKUPNO_PREDHODNA_GODINA: TFIBIntegerField;
    tblRealizacijaGOOSTANATI_TEKOVNA: TFIBIntegerField;
    pPreraspredelbaOtsustva: TpFIBStoredProc;
    tblOstanatiDenoviGODENOVI: TFIBIntegerField;
    qDeleteResenieOtsustva: TpFIBQuery;
    tblObukaUcesniciNAZIV_VRABOTEN: TFIBStringField;
    tblObukaUcesniciNAZIV_VRABOTEN_TI: TFIBStringField;
    tblOtsustvaNAZIV_VRABOTEN: TFIBStringField;
    tblOtsustvaNAZIV_VRABOTEN_TI: TFIBStringField;
    tblOtsustvaTIPOTSUSTVONAZIV: TFIBStringField;
    tblOtsustvaKolektivenDogDaNe: TFIBStringField;
    pZapisiOtsustvoRaspredeleno: TpFIBStoredProc;
    pOtsustva_Realizacija: TpFIBStoredProc;
    pOstanatiGOGodina: TpFIBStoredProc;
    qDeleteOtsustvo: TpFIBQuery;
    tblTipOtsustvoBOJA: TFIBIntegerField;
    tblOtsustvaBOJA: TFIBIntegerField;
    qDeleteBaranjeGO: TpFIBQuery;
    qDeleteReseniGO: TpFIBQuery;
    qDeleteReseniePreraspredelba: TpFIBQuery;
    qDeleteResenieOtkaz: TpFIBQuery;
    tblOtsustvaPregledNAZIVVRABOTEN_OUT: TFIBStringField;
    tblOtsustvaPregledNAZIV_VRABOTEN_TI_OUT: TFIBStringField;
    tblOtsustvaPregledRE_OUT: TFIBIntegerField;
    tblOstanatiDenoviGONAZIV_VRABOTEN: TFIBStringField;
    tblOstanatiDenoviGONAZIV_VRABOTEN_TI: TFIBStringField;
    qDeletePotvrdaRabOdnos: TpFIBQuery;
    tblOtsustvaPregledHRMRABID_OUT: TFIBIntegerField;
    tblOtsustvaPregledRMRE_OUT: TFIBIntegerField;
    tblOstanatiDenoviGORE: TFIBIntegerField;
    tblOstanatiDenoviGORM_ID: TFIBIntegerField;
    tblOstanatiDenoviGORABOTNO_MESTO: TFIBIntegerField;
    tblOstanatiDenoviGORABOTNAEDINICANAZIV: TFIBStringField;
    tblOtsustva2: TpFIBDataSet;
    dsOtsustva2: TDataSource;
    tblOtsustva2ID: TFIBIntegerField;
    tblOtsustva2TIP_ZAPIS: TFIBIntegerField;
    tblOtsustva2MB: TFIBStringField;
    tblOtsustva2PRICINA: TFIBIntegerField;
    tblOtsustva2PRICINA_OPIS: TFIBStringField;
    tblOtsustva2TIPOTSUSTVONAZIV: TFIBStringField;
    tblOtsustva2KolektivenDogDaNe: TFIBStringField;
    tblOtsustva2OD_VREME: TFIBDateTimeField;
    tblOtsustva2DO_VREME: TFIBDateTimeField;
    tblOtsustva2DO_VREME_L: TFIBDateTimeField;
    tblOtsustva2TS_INS: TFIBDateTimeField;
    tblOtsustva2TS_UPD: TFIBDateTimeField;
    tblOtsustva2USR_INS: TFIBStringField;
    tblOtsustva2USR_UPD: TFIBStringField;
    tblOtsustva2VRABOTENPREZIME: TFIBStringField;
    tblOtsustva2VRABOTENTATKOVOIME: TFIBStringField;
    tblOtsustva2VRABOTENIME: TFIBStringField;
    tblOtsustva2OPIS: TFIBStringField;
    tblOtsustva2NAZIV_VRABOTEN: TFIBStringField;
    tblOtsustva2NAZIV_VRABOTEN_TI: TFIBStringField;
    tblOtsustva2OPTIONS: TFIBIntegerField;
    tblOtsustva2EVENT_TYPE: TFIBIntegerField;
    tblOtsustva2TASK_LINKS_FIELD: TFIBMemoField;
    tblOtsustva2PLATENO: TFIBSmallIntField;
    tblOtsustva2FLAG: TFIBIntegerField;
    tblOtsustva2PlatenoDaNe: TFIBStringField;
    tblOtsustva2DENOVI: TFIBIntegerField;
    tblOtsustva2BOJA: TFIBIntegerField;
    tblPlanOtsustva: TpFIBDataSet;
    dsPlanOtsustva: TDataSource;
    tblPlanOtsustvaID: TFIBIntegerField;
    tblPlanOtsustvaTIP_ZAPIS: TFIBIntegerField;
    tblPlanOtsustvaMB: TFIBStringField;
    tblPlanOtsustvaPRICINA: TFIBIntegerField;
    tblPlanOtsustvaPRICINA_OPIS: TFIBStringField;
    tblPlanOtsustvaOD_VREME: TFIBDateTimeField;
    tblPlanOtsustvaDO_VREME: TFIBDateTimeField;
    tblPlanOtsustvaDO_VREME_L: TFIBDateTimeField;
    tblPlanOtsustvaSTATUS: TFIBIntegerField;
    tblPlanOtsustvaTS_INS: TFIBDateTimeField;
    tblPlanOtsustvaTS_UPD: TFIBDateTimeField;
    tblPlanOtsustvaUSR_INS: TFIBStringField;
    tblPlanOtsustvaUSR_UPD: TFIBStringField;
    tblPlanOtsustvaTIPOTSUSTVONAZIV: TFIBStringField;
    tblPlanOtsustvaVRABOTENPREZIME: TFIBStringField;
    tblPlanOtsustvaVRABOTENTATKOVOIME: TFIBStringField;
    tblPlanOtsustvaVRABOTENIME: TFIBStringField;
    tblPlanOtsustvaSTATUSNAZIV: TFIBStringField;
    tblPlanOtsustvaSTATUS_BOJA: TFIBIntegerField;
    tblPlanOtsustvaOPIS: TFIBStringField;
    tblPlanOtsustvaNAZIV_VRABOTEN: TFIBStringField;
    tblPlanOtsustvaNAZIV_VRABOTEN_TI: TFIBStringField;
    tblPlanOtsustvaOPTIONS: TFIBIntegerField;
    tblPlanOtsustvaEVENT_TYPE: TFIBIntegerField;
    tblPlanOtsustvaTASK_LINKS_FIELD: TFIBMemoField;
    tblPlanOtsustvaDENOVI: TFIBIntegerField;
    tblPlanOtsustva2: TpFIBDataSet;
    dsPlanOtsustva2: TDataSource;
    tblPlanOtsustva2ID: TFIBIntegerField;
    tblPlanOtsustva2TIP_ZAPIS: TFIBIntegerField;
    tblPlanOtsustva2MB: TFIBStringField;
    tblPlanOtsustva2PRICINA: TFIBIntegerField;
    tblPlanOtsustva2PRICINA_OPIS: TFIBStringField;
    tblPlanOtsustva2OD_VREME: TFIBDateTimeField;
    tblPlanOtsustva2DO_VREME: TFIBDateTimeField;
    tblPlanOtsustva2DO_VREME_L: TFIBDateTimeField;
    tblPlanOtsustva2STATUS: TFIBIntegerField;
    tblPlanOtsustva2TS_INS: TFIBDateTimeField;
    tblPlanOtsustva2TS_UPD: TFIBDateTimeField;
    tblPlanOtsustva2USR_INS: TFIBStringField;
    tblPlanOtsustva2USR_UPD: TFIBStringField;
    tblPlanOtsustva2TIPOTSUSTVONAZIV: TFIBStringField;
    tblPlanOtsustva2VRABOTENPREZIME: TFIBStringField;
    tblPlanOtsustva2VRABOTENTATKOVOIME: TFIBStringField;
    tblPlanOtsustva2VRABOTENIME: TFIBStringField;
    tblPlanOtsustva2STATUSNAZIV: TFIBStringField;
    tblPlanOtsustva2STATUS_BOJA: TFIBIntegerField;
    tblPlanOtsustva2OPIS: TFIBStringField;
    tblPlanOtsustva2NAZIV_VRABOTEN: TFIBStringField;
    tblPlanOtsustva2NAZIV_VRABOTEN_TI: TFIBStringField;
    tblPlanOtsustva2OPTIONS: TFIBIntegerField;
    tblPlanOtsustva2EVENT_TYPE: TFIBIntegerField;
    tblPlanOtsustva2TASK_LINKS_FIELD: TFIBMemoField;
    tblPlanOtsustva2DENOVI: TFIBIntegerField;
    qDeleteIzjavaProdolzenRabOdnos: TpFIBQuery;
    tblCountTipOtsustvo: TpFIBDataSet;
    dsCountTipOtsustvo: TDataSource;
    tblCountTipOtsustvoRE_OUT: TFIBIntegerField;
    tblCountTipOtsustvoPLATENO_OUT: TFIBIntegerField;
    tblCountTipOtsustvoNEPLATENO_OUT: TFIBIntegerField;
    tblCountTipOtsustvoCOUNT_1_OUT: TFIBIntegerField;
    tblCountTipOtsustvoCOUNT_0_OUT: TFIBIntegerField;
    tblCountTipOtsustvoMB_OUT: TFIBStringField;
    tblCountTipOtsustvoNAZIVVRABOTEN_OUT: TFIBStringField;
    tblCountTipOtsustvoRMRE_OUT: TFIBIntegerField;
    tblCountTipOtsustvoRENAZIV_OUT: TFIBStringField;
    tblCountTipOtsustvoRABMESTONAZIV_OUT: TFIBStringField;
    tblCountTipOtsustvoHRMRABID_OUT: TFIBIntegerField;
    tblCountTipOtsustvoNAZIV_VRABOTEN_TI_OUT: TFIBStringField;
    tblCountTipOtsustvoTIP_OTSUSTVO: TFIBIntegerField;
    tblCountTipOtsustvoTIP_OTSUSTVO_NAZIV: TFIBStringField;
    tblCountTipOtsustvoCOUNT_TIP_OTSUSTVO: TFIBIntegerField;
    pPROC_HR_STAZ_DOGOVOR: TpFIBStoredProc;
    function zemiBroj(Proc : TpFIBStoredProc; p1,  p2, p3, p4, v1, v2, v3, v4, broj : Variant):Variant;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    function insert(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6,p7, p8, v1, v2, v3, v4, v5, v6, v7, v8 : Variant):Variant;
    function zemiRezultat(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):Variant;
    function zemiRezultat7(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5, p6, p7,v1, v2, v3,v4, v5,v6, v7, broj : Variant):Variant;
    function insert12(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8,p9,p10,p11,P12, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12,broj : Variant):Variant;
    function insert10(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8, p9,P10, v1, v2, v3, v4, v5, v6, v7, v8, v9,v10 : Variant):Variant;
    function insert25(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8,p9,p10,p11,p12, p13,  p14, p15, p16, p17,p18, p19,P20,p21,p22,p23,p24,p25, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,v12, v13,  v14, v15, v16, v17,v18, v19,v20,v21,v22,v23,v24,v25, broj : Variant):Variant;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmOtsustvo: TdmOtsustvo;
  StatGrupaPlanOts, StatGrupaEvidOts,StatGrupaSpecijalizacija, StatGrupaObuki,firma, odobren_plan, statusGrupaOtsustva,
  dogovorZaRabota, reAdministracija, reseniePreraspredelba, rabotnoMesto, baranjeGO,
  resenieGO, dogHonorarci, resPrekinRabOdnos, dogSezonci, aneksDogovor, dogVolonter, godisenOdmor, prijava, odjava:integer;
  patekaDokScan: string;
  pom_tab_baranje_go, pom_tab, pom_tab_resGO, potvrda_raboten_odnos, izjava_prodolzen_ro: Integer;
implementation

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, DaNe;

{$R *.dfm}

function TdmOtsustvo.zemiBroj(Proc : TpFIBStoredProc; p1,  p2, p3, p4,v1, v2, v3,v4,broj : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;
function TdmOtsustvo.insert(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8, v1, v2, v3, v4, v5, v6, v7, v8 : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         if(not VarIsNull(p7)) then
           Proc.ParamByName(VarToStr(p7)).Value := v7;
         if(not VarIsNull(p8)) then
           Proc.ParamByName(VarToStr(p8)).Value := v8;
         Proc.ExecProc;
    end;

function TdmOtsustvo.insert10(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8, p9,P10, v1, v2, v3, v4, v5, v6, v7, v8, v9,v10 : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         if(not VarIsNull(p7)) then
           Proc.ParamByName(VarToStr(p7)).Value := v7;
         if(not VarIsNull(p8)) then
           Proc.ParamByName(VarToStr(p8)).Value := v8;
         if(not VarIsNull(p9)) then
           Proc.ParamByName(VarToStr(p9)).Value := v9;
         if(not VarIsNull(p10)) then
           Proc.ParamByName(VarToStr(p10)).Value := v10;
         Proc.ExecProc;
    end;


function TdmOtsustvo.insert12(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8,p9,p10,p11,p12, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,v12, broj : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         if(not VarIsNull(p7)) then
           Proc.ParamByName(VarToStr(p7)).Value := v7;
         if(not VarIsNull(p8)) then
           Proc.ParamByName(VarToStr(p8)).Value := v8;
         if(not VarIsNull(p9)) then
           Proc.ParamByName(VarToStr(p9)).Value := v9;
         if(not VarIsNull(p10)) then
           Proc.ParamByName(VarToStr(p10)).Value := v10;
         if(not VarIsNull(p11)) then
           Proc.ParamByName(VarToStr(p11)).Value := v11;
         if(not VarIsNull(p12)) then
           Proc.ParamByName(VarToStr(p12)).Value := v12;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;

function TdmOtsustvo.zemiRezultat(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;

function TdmOtsustvo.zemiRezultat7(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5, p6, p7,v1, v2, v3,v4, v5, v6, v7, broj : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         if(not VarIsNull(p7)) then
           Proc.ParamByName(VarToStr(p7)).Value := v7;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;

function TdmOtsustvo.insert25(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8,p9,p10,p11,p12, p13,  p14, p15, p16, p17,p18, p19,P20,p21,p22,p23,p24,p25, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,v12, v13,  v14, v15, v16, v17,v18, v19,v20,v21,v22,v23,v24,v25, broj : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         if(not VarIsNull(p7)) then
           Proc.ParamByName(VarToStr(p7)).Value := v7;
         if(not VarIsNull(p8)) then
           Proc.ParamByName(VarToStr(p8)).Value := v8;
         if(not VarIsNull(p9)) then
           Proc.ParamByName(VarToStr(p9)).Value := v9;
         if(not VarIsNull(p10)) then
           Proc.ParamByName(VarToStr(p10)).Value := v10;
         if(not VarIsNull(p11)) then
           Proc.ParamByName(VarToStr(p11)).Value := v11;
         if(not VarIsNull(p12)) then
           Proc.ParamByName(VarToStr(p12)).Value := v12;
         if(not VarIsNull(p13)) then
           Proc.ParamByName(VarToStr(p13)).Value := v13;
         if(not VarIsNull(p14)) then
           Proc.ParamByName(VarToStr(p14)).Value := v14;
         if(not VarIsNull(p15)) then
           Proc.ParamByName(VarToStr(p15)).Value := v15;
         if(not VarIsNull(p16)) then
           Proc.ParamByName(VarToStr(p16)).Value := v16;
         if(not VarIsNull(p17)) then
           Proc.ParamByName(VarToStr(p17)).Value := v17;
         if(not VarIsNull(p18)) then
           Proc.ParamByName(VarToStr(p18)).Value := v18;
         if(not VarIsNull(p19)) then
           Proc.ParamByName(VarToStr(p19)).Value := v19;
         if(not VarIsNull(p20)) then
           Proc.ParamByName(VarToStr(p20)).Value := v20;
         if(not VarIsNull(p21)) then
           Proc.ParamByName(VarToStr(p21)).Value := v21;
         if(not VarIsNull(p22)) then
           Proc.ParamByName(VarToStr(p22)).Value := v22;
         if(not VarIsNull(p23)) then
           Proc.ParamByName(VarToStr(p23)).Value := v23;
         if(not VarIsNull(p24)) then
           Proc.ParamByName(VarToStr(p24)).Value := v24;
         if(not VarIsNull(p25)) then
           Proc.ParamByName(VarToStr(p25)).Value := v25;
         Proc.ExecProc;

         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;


procedure TdmOtsustvo.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe :=  TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

end.
