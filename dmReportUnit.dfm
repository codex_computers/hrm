object dmReport: TdmReport
  OldCreateOrder = False
  Height = 982
  Width = 665
  object tblReportDizajn2: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_REPORT'
      'SET '
      '    ID = :ID,'
      '    "SQL" = :"SQL",'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SYS_REPORT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SYS_REPORT('
      '    ID,'
      '    "SQL",'
      '    DATA'
      ')'
      'VALUES('
      '    :ID,'
      '    :"SQL",'
      '    :DATA'
      ')')
    RefreshSQL.Strings = (
      'select id,sql,data '
      'from sys_report '
      'where(  br = :br and tabela_grupa = :app'
      '     ) and (     SYS_REPORT.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select id,sql,data '
      'from sys_report '
      ''
      'where br = :br and tabela_grupa = :app')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 152
    object tblReportDizajn2SQL: TFIBBlobField
      FieldName = 'SQL'
      Size = 8
    end
    object tblReportDizajn2DATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object tblSqlReport: TpFIBDataSet
    SelectSQL.Strings = (
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 136
    Top = 152
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxDBDataset1'
    OnFirst = frxDBDataset1First
    OnNext = frxDBDataset1Next
    CloseDataSource = False
    OnClose = frxDBDataset1Close
    OnOpen = frxDBDataset1Open
    DataSet = tblSqlReport
    BCDToCurrency = False
    Left = 224
    Top = 144
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnSaveReport = frxDesigner1SaveReport
    Left = 304
    Top = 144
  end
  object frxReport1: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40519.596180625000000000
    ReportOptions.LastChange = 42143.635914594900000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    Left = 384
    Top = 144
  end
  object tblDetail: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    g.lokaciin_id,  '
      '    g.id smetka,'
      '    g.broj_faktura,'
      '    g.mesec,'
      '    g.godina,'
      '    round(g.iznos_vkupno,0) iznos,'
      '    coalesce(sum(ku.iznos),0) plateno ,'
      '    round(g.iznos_vkupno,0)-coalesce(sum(ku.iznos),0) dolzi'
      ''
      'from  kom_presmetka_g g'
      '    left join kom_uplati ku on g.id =  ku.presmetka_g_id'
      'where g.lokaciin_id = :mas_id'
      '    and g.tip = 1'
      
        '    and  encodedate(01,g.mesec,g.godina) between :mas_od_datum a' +
        'nd :mas_do_datum'
      
        'group BY  g.lokaciin_id, g.id, g.mesec, g.godina, G.iznos_vkupno' +
        ', g.broj_faktura'
      'order by g.godina, g.mesec'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsMaster
    Left = 136
    Top = 96
  end
  object frxDBDataset2: TfrxDBDataset
    UserName = 'frxDBDataset2'
    CloseDataSource = False
    DataSet = tblDetail
    BCDToCurrency = False
    Left = 224
    Top = 88
  end
  object dsMaster: TDataSource
    DataSet = tblSqlReport
    Left = 32
    Top = 104
  end
  object frxRichObject1: TfrxRichObject
    Left = 376
    Top = 80
  end
  object frxDialogControls1: TfrxDialogControls
    Left = 392
    Top = 8
  end
  object frxBaranjeZaGO: TfrxDBDataset
    UserName = #1041#1072#1088#1072#1114#1077#1047#1072#1043#1054
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'GODINA='#1043#1086#1076#1080#1085#1072
      'DATUM='#1044#1072#1090#1091#1084
      'VID_DOCUMENT='#1064#1080#1092#1088#1072' '#1085#1072' '#1074#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      'VIDDOKUMENTNAZIV='#1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      'ODOBRENIE='#1054#1076#1086#1073#1088#1077#1085#1086' '#1086#1076
      'KORISTENJE='#1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' ('#1064#1080#1092#1088#1072')'
      'PRV_DEL='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1087#1088#1074' '#1076#1077#1083
      'VTOR_DEL='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1074#1090#1086#1088' '#1076#1077#1083
      'MB_RAKOVODITEL='#1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      'NAZIVVRABOTEN='#1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086#1048#1084#1077' '#1048#1084#1077
      'RMNAZIV='#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'OdobrenieNaziv='#1054#1076#1086#1073#1088#1077#1085#1086' ('#1076#1072'/'#1085#1077')'
      'KoristenjeNaziv='#1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
      'DATA=DATA'
      'DATUM1_OD='#1055#1088#1074' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1086#1076')'
      'DATUM1_DO='#1087#1088#1074' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1076#1086')'
      'DATUM2_OD='#1074#1090#1086#1088' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1086#1076')'
      'DATUM2_DO='#1074#1090#1086#1088' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1076#1086')'
      'RENAZIV='#1056#1072#1073#1086#1090#1085#1072' '#1045#1076#1080#1085#1080#1094#1072
      'ID_RE_FIRMA_NAZIV=ID_RE_FIRMA_NAZIV')
    DataSet = BaranjeZaGO
    BCDToCurrency = False
    Left = 264
    Top = 288
  end
  object dsTemplate: TDataSource
    DataSet = Template
    Left = 152
    Top = 224
  end
  object Template: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select hrt.id, hrt.vid, hrt.broj, hrt.naziv, hrt.report, hrt.ts_' +
        'ins, hrt.ts_upd, hrt.usr_upd, hrt.usr_ins'
      'from hr_report_tamplate hrt'
      'where hrt.broj = :broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 56
    Top = 224
    object TemplateID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TemplateVID: TFIBIntegerField
      FieldName = 'VID'
    end
    object TemplateNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object TemplateREPORT: TFIBBlobField
      FieldName = 'REPORT'
      Size = 8
    end
    object TemplateTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object TemplateTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object TemplateUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object TemplateUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object TemplateBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
  end
  object frxDogovorVrabotuvanje: TfrxDBDataset
    UserName = #1044#1086#1075#1042#1088#1072#1073
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'DATUM='#1044#1072#1090#1091#1084
      'BROJ='#1041#1088#1086#1112
      'VID_DOKUMENT='#1042#1080#1076#1044#1086#1082#1091#1084#1077#1085#1090'('#1064#1080#1092#1088#1072')'
      'NAZIV_VID_DOKUMENT='#1042#1080#1076#1053#1072#1044#1086#1082#1091#1084#1077#1085#1090
      'MB='#1052#1072#1090#1041#1088
      'ID_RE_FIRMA='#1064#1080#1092#1088#1072#1053#1072#1060#1080#1088#1084#1072
      'PREZIME='#1055#1088#1077#1079#1080#1084#1077
      'TATKOVO_IME='#1058#1072#1090#1082#1086#1074#1086#1048#1084#1077
      'IME='#1048#1084#1077
      'ADRESA='#1040#1076#1088#1077#1089#1072
      'MESTO='#1052#1077#1089#1090#1086'('#1064#1080#1092#1088#1072')'
      'STEPEN_STRUCNA_PODGOTOVKA='#1057#1057#1055
      'VID_VRABOTUVANJE='#1042#1080#1076#1053#1072#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077'('#1064#1080#1092#1088#1072')'
      'NAZIV_VID_VRAB='#1042#1080#1076#1053#1072#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      'DATUM_OD='#1044#1072#1090#1091#1084#1054#1076
      'DATUM_DO='#1044#1072#1090#1091#1084#1044#1086
      'RABOTNO_VREME='#1041#1088#1056#1072#1073#1063#1072#1089
      'RABOTNO_MESTO='#1064#1080#1092#1088#1072#1053#1072#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'NAZIV_MESTO='#1052#1077#1089#1090#1086
      'OBRAZOVANIE='#1057#1057#1055'('#1064#1080#1092#1088#1072')'
      'RABMESTONAZIV='#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'MB_DIREKTOR='#1044#1080#1088#1077#1082#1090#1086#1088
      'PLATA='#1055#1083#1072#1090#1072
      'STATUS=STATUS'
      'DATA=DATA'
      'GRUPAOZNAKA='#1043#1088#1091#1087#1072#1054#1079#1085#1072#1082#1072
      'RAKOVODENJEPROCENT='#1041#1086#1076#1056#1072#1082#1086#1074#1086#1076#1077#1114#1077#1055#1088#1086#1094#1077#1085#1090
      'SZNAZIV='#1057#1047#1054
      'STAZ_GODINA='#1057#1090#1072#1078#1043#1086#1076
      'PROCENTSTAZKOL='#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1077#1085#1055#1088#1086#1094#1077#1085#1090#1057#1090#1072#1078
      'RABEDINICANAZIV='#1056#1072#1073#1086#1090#1085#1072#1045#1076#1080#1085#1080#1094#1072
      'OPIS_RABOTNO_MESTO='#1054#1087#1080#1089#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'USLOVIRABOTANAZIV='#1059#1089#1083#1086#1074#1080#1055#1086#1090#1077#1096#1082#1080#1054#1076#1053#1086#1088#1084#1072#1083#1085#1080#1090#1077
      'BENEFICIRAN_STAZ=BenificiranStaz'
      'DEN_USLOVI_RABOTA=DenUsloviRabota'
      'NASOKANAZIV='#1053#1072#1089#1086#1082#1072#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      'ZNAENJE='#1057#1090#1077#1082#1085#1072#1090#1086#1047#1085#1072#1077#1114#1077#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      'ZVANJE='#1047#1074#1072#1114#1077
      'BODRAKOVODENJE_PROCENT='#1041#1086#1076#1056#1072#1082#1086#1074#1086#1076#1077#1114#1077#1055#1088#1086#1094#1077#1085#1090
      'BODRAKOVODENJE='#1041#1086#1076#1056#1072#1082#1086#1074#1086#1076#1077#1114#1077
      'BODSTEPENSLOZENOST='#1057#1090#1077#1087#1077#1085#1057#1083#1086#1078#1077#1085#1086#1089#1090
      'BODOVISTAZ='#1041#1086#1076#1086#1074#1080#1057#1090#1072#1078
      'VKUPNOBODOVI='#1042#1082#1091#1087#1085#1086#1041#1086#1076
      'USLOVI_RABOTA_PROCENT=USLOVI_RABOTA_PROCENT'
      'USLOVI_RABOTA=USLOVI_RABOTA'
      'BODOVI=BODOVI'
      'NETO=NETO'
      'ID_RE_FIRMA_NAZIV=ID_RE_FIRMA_NAZIV')
    DataSet = DogovorZaVrabotuvanje
    BCDToCurrency = False
    Left = 272
    Top = 224
  end
  object DogovorZaVrabotuvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_DOGOVOR_VRABOTUVANJE'
      'SET '
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    MB = :MB,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    PREZIME = :PREZIME,'
      '    TATKOVO_IME = :TATKOVO_IME,'
      '    IME = :IME,'
      '    ADRESA = :ADRESA,'
      '    MESTO = :MESTO,'
      '    STEPEN_STRUCNA_PODGOTOVKA = :STEPEN_STRUCNA_PODGOTOVKA,'
      '    VID_VRABOTUVANJE = :VID_VRABOTUVANJE,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    RABOTNO_VREME = :RABOTNO_VREME,'
      '    RABOTNO_MESTO = :RABOTNO_MESTO,'
      '    MB_DIREKTOR = :MB_DIREKTOR,'
      '    PLATA = :PLATA,'
      '    STATUS = :STATUS,'
      '    DATA = :DATA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_UPD = :USR_UPD,'
      '    USR_INS = :USR_INS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hdv.id,'
      '    hdv.datum,'
      '    hdv.broj,'
      '    hdv.vid_dokument,'
      '    hvd.naziv naziv_vid_dokument,'
      '    hdv.mb,'
      '    hdv.id_re_firma,'
      '    m_id_re_firma.naziv as  id_re_firma_naziv,'
      '    hdv.prezime,'
      '    hdv.tatkovo_ime,'
      '    hdv.ime,'
      '    hdv.adresa,'
      '    hdv.mesto,'
      '    hdv.stepen_strucna_podgotovka,'
      '    hdv.vid_vrabotuvanje,'
      '    hvv.naziv naziv_vid_vrab,'
      '    hdv.datum_od,'
      '    hdv.datum_do,'
      '    hdv.rabotno_vreme,'
      '    hdv.rabotno_mesto,'
      '    mm.naziv naziv_mesto,'
      '    po.opis obrazovanie,'
      '    hrmrab.naziv as rabMestoNaziv,'
      '    hrmrab.opis opis_rabotno_mesto,'
      '    hdv.mb_direktor,'
      '    hdv.plata,'
      '    hdv.status,'
      '    hdv.data,'
      '    hdv.den_uslovi_rabota,'
      '    hdv.BODOVI,'
      '    hdv.NETO,'
      
        '    cast(coalesce(hrm.rakovodenje,0) as numeric(15,4)) as BodRak' +
        'ovodenje_procent,'
      
        '    cast(coalesce(hdv.koeficient,0) * coalesce(hrm.rakovodenje,0' +
        ')/100 as numeric(15,4))  as BodRakovodenje,'
      
        '    cast(coalesce(hdv.koeficient,0) as numeric(15,4)) as BodStep' +
        'enSlozenost,'
      '    (select sum(ku.procent) from hr_rmre_uslovi_rabota us'
      
        '                                left join hr_koeficient_uslovira' +
        'b ku on ku.id = us.uslovi_rabota'
      
        '                            where (hdv.rabotno_mesto = us.id_rm_' +
        're )) uslovi_rabota_procent,'
      '    cast((select sum(ku.procent) from hr_rmre_uslovi_rabota us'
      
        '                                left join hr_koeficient_uslovira' +
        'b ku on ku.id = us.uslovi_rabota'
      
        '                            where (hdv.rabotno_mesto = us.id_rm_' +
        're )) *coalesce(hdv.koeficient,0)/100 as numeric(15,4)) uslovi_r' +
        'abota,'
      
        '   -- cast(coalesce(hrk.procent,0) as numeric(15,4)) uslovi_rabo' +
        'ta_procent,'
      
        '   -- cast(coalesce(hdv.koeficient,0) * coalesce(hrk.procent,0)/' +
        '100 as numeric(15,4)) uslovi_rabota,'
      ''
      '    hrg.oznaka as grupaOznaka,'
      '    hdv.beneficiran_staz,'
      
        '    --cast((coalesce(hrm.uslovi_rabota,0))*coalesce(hdv.koeficie' +
        'nt,0)/100 as numeric(15,3)) as usloviRabotaProcent,'
      '    --coalesce(hrk.procent,0)  as usloviRabotaProcent,'
      '    coalesce(hrm.rakovodenje,0) as rakovodenjeProcent,'
      '    cast((select p.dodatok'
      '     from hr_parametri_kd p'
      
        '     where :param between p.datum_od and coalesce(p.datum_do, cu' +
        'rrent_date)) * coalesce((hdv.staz_godina +  (extractyear(current' +
        '_date) - extractyear(hdv.datum_od))), 0) * coalesce(hdv.koeficie' +
        'nt,0)/100 as numeric(15,4)) as BodoviStaz,'
      '    coalesce(hrs.naziv, '#39#39') as szNaziv,'
      
        '    cast((coalesce(hrm.uslovi_rabota,0)*coalesce(hdv.koeficient,' +
        '0)/100 + coalesce(hrm.rakovodenje,0)*coalesce(hdv.koeficient,0)/' +
        '100 +  coalesce(hdv.koeficient,0) +'
      '    (select p.dodatok'
      '     from hr_parametri_kd p'
      
        '     where :param between p.datum_od and coalesce(p.datum_do, cu' +
        'rrent_date))*coalesce(hdv.koeficient,0)/100* coalesce((hdv.staz_' +
        'godina +  (extractyear(current_date) - extractyear(hdv.datum_od)' +
        ')), 0))as numeric(15,4)) as VkupnoBodovi,'
      
        '    coalesce((hdv.staz_godina +  (extractyear(current_date) - ex' +
        'tractyear(hdv.datum_od))), 0) as staz_godina,'
      '    (select p.dodatok'
      '     from hr_parametri_kd p'
      
        '     where :param between p.datum_od and coalesce(p.datum_do, cu' +
        'rrent_date)) as ProcentStazKol,'
      '     m.naziv as RabEdinicaNaziv,'
      '   --  hrk.naziv as usloviRabotaNaziv'
      '   (select proc_hr_template.uslovirabotanaziv'
      '    from proc_hr_template(hdv.rabotno_mesto)) usloviRabotaNaziv,'
      '    n.naziv as nasokaNaziv,'
      '    oo.znaenje,'
      '    lice.zvanje'
      ''
      'from hr_dogovor_vrabotuvanje hdv'
      'inner join hr_vid_dokument hvd on hvd.id=hdv.vid_dokument'
      'left outer join mat_mesto mm on mm.id=hdv.mesto'
      
        'inner join plt_obrazovanie po on po.id=hdv.stepen_strucna_podgot' +
        'ovka'
      'inner join hr_rm_re hrm on hrm.id = hdv.rabotno_mesto'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hdv.uslo' +
        'vi_rabota'
      'inner join mat_re m on m.id = hrm.id_re'
      
        'inner join mat_re m_id_re_firma on m_id_re_firma.id = hdv.id_re_' +
        'firma'
      'inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id_rm'
      
        'left join hr_rmre_uslovi_rabota ur on ur.id_rm_re = hrm.id and h' +
        'rk.id = ur.uslovi_rabota'
      'left outer join hr_grupa_rm hrg on hrg.id = hrmrab.grupa'
      
        'left outer join hr_sz_obrazovanie hrs on hrs.id = hrmrab.id_sz_o' +
        'brazovanie'
      
        'inner join hr_sistematizacija hs on hs.id = hrm.id_sistematizaci' +
        'ja'
      
        'inner join hr_vid_vrabotuvanje hvv on hvv.id=hdv.vid_vrabotuvanj' +
        'e'
      
        'left outer join hr_obrazovanie_obuka oo on oo.mb_vraboten = hdv.' +
        'mb'
      'left outer join hr_nasoka_obrazovanie n on n.id = oo.nasoka'
      'inner join hr_vraboten lice on lice.mb = hdv.mb'
      'where hdv.id = :ID'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 400
    Top = 224
    object DogovorZaVrabotuvanjeID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DogovorZaVrabotuvanjeDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object DogovorZaVrabotuvanjeBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object DogovorZaVrabotuvanjeNAZIV_VID_DOKUMENT: TFIBStringField
      FieldName = 'NAZIV_VID_DOKUMENT'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeID_RE_FIRMA: TFIBIntegerField
      FieldName = 'ID_RE_FIRMA'
    end
    object DogovorZaVrabotuvanjePREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeTATKOVO_IME: TFIBStringField
      FieldName = 'TATKOVO_IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeIME: TFIBStringField
      FieldName = 'IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeMESTO: TFIBIntegerField
      FieldName = 'MESTO'
    end
    object DogovorZaVrabotuvanjeSTEPEN_STRUCNA_PODGOTOVKA: TFIBIntegerField
      FieldName = 'STEPEN_STRUCNA_PODGOTOVKA'
    end
    object DogovorZaVrabotuvanjeVID_VRABOTUVANJE: TFIBIntegerField
      FieldName = 'VID_VRABOTUVANJE'
    end
    object DogovorZaVrabotuvanjeNAZIV_VID_VRAB: TFIBStringField
      FieldName = 'NAZIV_VID_VRAB'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object DogovorZaVrabotuvanjeDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object DogovorZaVrabotuvanjeRABOTNO_VREME: TFIBBCDField
      FieldName = 'RABOTNO_VREME'
      Size = 2
    end
    object DogovorZaVrabotuvanjeRABOTNO_MESTO: TFIBIntegerField
      FieldName = 'RABOTNO_MESTO'
    end
    object DogovorZaVrabotuvanjeNAZIV_MESTO: TFIBStringField
      FieldName = 'NAZIV_MESTO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeOBRAZOVANIE: TFIBStringField
      FieldName = 'OBRAZOVANIE'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeRABMESTONAZIV: TFIBStringField
      FieldName = 'RABMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeMB_DIREKTOR: TFIBStringField
      FieldName = 'MB_DIREKTOR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjePLATA: TFIBBCDField
      FieldName = 'PLATA'
      Size = 2
    end
    object DogovorZaVrabotuvanjeSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
    object DogovorZaVrabotuvanjeDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object DogovorZaVrabotuvanjeGRUPAOZNAKA: TFIBStringField
      FieldName = 'GRUPAOZNAKA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeRAKOVODENJEPROCENT: TFIBBCDField
      FieldName = 'RAKOVODENJEPROCENT'
      Size = 2
    end
    object DogovorZaVrabotuvanjeSZNAZIV: TFIBStringField
      FieldName = 'SZNAZIV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeSTAZ_GODINA: TFIBBCDField
      FieldName = 'STAZ_GODINA'
      Size = 0
    end
    object DogovorZaVrabotuvanjePROCENTSTAZKOL: TFIBBCDField
      FieldName = 'PROCENTSTAZKOL'
      Size = 8
    end
    object DogovorZaVrabotuvanjeRABEDINICANAZIV: TFIBStringField
      FieldName = 'RABEDINICANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeOPIS_RABOTNO_MESTO: TFIBStringField
      FieldName = 'OPIS_RABOTNO_MESTO'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeUSLOVIRABOTANAZIV: TFIBStringField
      FieldName = 'USLOVIRABOTANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeBENEFICIRAN_STAZ: TFIBBCDField
      FieldName = 'BENEFICIRAN_STAZ'
      Size = 2
    end
    object DogovorZaVrabotuvanjeDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object DogovorZaVrabotuvanjeNASOKANAZIV: TFIBStringField
      FieldName = 'NASOKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeZNAENJE: TFIBStringField
      FieldName = 'ZNAENJE'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeZVANJE: TFIBStringField
      DisplayLabel = #1047#1074#1072#1114#1077
      FieldName = 'ZVANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorZaVrabotuvanjeBODRAKOVODENJE_PROCENT: TFIBBCDField
      FieldName = 'BODRAKOVODENJE_PROCENT'
      DisplayFormat = '#,##0.0000'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeBODRAKOVODENJE: TFIBBCDField
      FieldName = 'BODRAKOVODENJE'
      DisplayFormat = '#,##0.0000'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeBODSTEPENSLOZENOST: TFIBBCDField
      FieldName = 'BODSTEPENSLOZENOST'
      DisplayFormat = '#,##0.0000'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeBODOVISTAZ: TFIBBCDField
      FieldName = 'BODOVISTAZ'
      DisplayFormat = '#,##0.0000'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeVKUPNOBODOVI: TFIBBCDField
      FieldName = 'VKUPNOBODOVI'
      DisplayFormat = '#,##0.0000'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeUSLOVI_RABOTA_PROCENT: TFIBBCDField
      FieldName = 'USLOVI_RABOTA_PROCENT'
      DisplayFormat = '#,##0.0000'
      Size = 2
    end
    object tblDogovorZaVrabotuvanjeUSLOVI_RABOTA: TFIBBCDField
      FieldName = 'USLOVI_RABOTA'
      DisplayFormat = '#,##0.0000'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeBODOVI: TFIBBCDField
      FieldName = 'BODOVI'
      Size = 2
    end
    object tblDogovorZaVrabotuvanjeNETO: TFIBBCDField
      FieldName = 'NETO'
      Size = 8
    end
    object tblDogovorZaVrabotuvanjeID_RE_FIRMA_NAZIV: TFIBStringField
      FieldName = 'ID_RE_FIRMA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object BaranjeZaGO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_BARANJE_GO'
      'SET '
      '    MB = :MB,'
      '    GODINA = :GODINA,'
      '    DATUM = :DATUM,'
      '    VID_DOCUMENT = :VID_DOCUMENT,'
      '    ODOBRENIE = :ODOBRENIE,'
      '    KORISTENJE = :KORISTENJE,'
      '    PRV_DEL = :PRV_DEL,'
      '    VTOR_DEL = :VTOR_DEL,'
      '    MB_RAKOVODITEL = :MB_RAKOVODITEL,'
      '    DATUM1_OD = :DATUM1_OD,'
      '    DATUM1_DO = :DATUM1_DO,'
      '    DATUM2_OD = :DATUM2_OD,'
      '    DATUM2_DO = :DATUM2_DO,'
      '    DATA = :DATA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_UPD = :USR_UPD,'
      '    USR_INS = :USR_INS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select go.id,'
      '       go.mb,'
      '       go.godina,'
      '       go.datum,'
      '       go.vid_document,'
      '       hvd.naziv as vidDokumentNaziv,'
      '       go.odobrenie,'
      '       go.koristenje,'
      '       go.prv_del,'
      '       go.vtor_del,'
      '       go.mb_rakovoditel,'
      '       hrv.prezime || '#39' '#39'|| hrv.ime as NazivVraboten,'
      '       (select rabm.naziv'
      '       from hr_vraboten_rm hvr'
      '       inner join hr_rm_re hrm on hrm.id = hvr.id_rm_re'
      '       inner join hr_rabotno_mesto rabm on rabm.id = hrm.id_rm'
      
        '       where hvr.mb = go.mb and (go.datum between hvr.datum_od a' +
        'nd coalesce(hvr.datum_do, current_date)))  as RMNaziv,'
      '       (select mr.naziv'
      '       from hr_vraboten_rm hvr'
      '       inner join hr_rm_re hrm on hrm.id = hvr.id_rm_re'
      '       inner join mat_re mr on mr.id = hrm.id_re'
      
        '       where hvr.mb = go.mb and (go.datum between hvr.datum_od a' +
        'nd coalesce(hvr.datum_do, current_date)))  as RENaziv,'
      '       (select mr.naziv'
      '       from hr_vraboten_rm hvrr'
      '       inner join mat_re mr on mr.id = hvrr.id_rm_re'
      
        '       where hvrr.mb = go.mb and (go.datum between hvrr.datum_od' +
        ' and coalesce(hvrr.datum_do, current_date)))  as id_re_firma_naz' +
        'iv,'
      '       go.datum1_od,'
      '       go.datum1_do,'
      '       go.datum2_od, '
      '       go.datum2_do,'
      '       case go.odobrenie when '#39'0'#39' then '#39#1053#1077#39
      '                         when '#39'1'#39' then '#39#1044#1072#39
      '       end "OdobrenieNaziv",'
      '       case go.koristenje when '#39'1'#39' then '#39#1045#1076#1085#1086#1082#1088#1072#1090#1085#1086#39
      '                          when '#39'2'#39' then '#39#1044#1074#1086#1082#1088#1072#1090#1085#1086#39
      '       end "KoristenjeNaziv",'
      '       go.data,'
      '       go.ts_ins,'
      '       go.ts_upd,'
      '       go.usr_upd,'
      '       go.usr_ins'
      'from hr_baranje_go go'
      'inner join hr_vid_dokument hvd on hvd.id = go.vid_document'
      'inner join hr_vraboten hrv on hrv.mb = go.mb'
      'where go.id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 400
    Top = 288
    object BaranjeZaGOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object BaranjeZaGOMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGOGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object BaranjeZaGODATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object BaranjeZaGOVID_DOCUMENT: TFIBIntegerField
      FieldName = 'VID_DOCUMENT'
    end
    object BaranjeZaGOVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGOODOBRENIE: TFIBSmallIntField
      FieldName = 'ODOBRENIE'
    end
    object BaranjeZaGOKORISTENJE: TFIBIntegerField
      FieldName = 'KORISTENJE'
    end
    object BaranjeZaGOPRV_DEL: TFIBIntegerField
      FieldName = 'PRV_DEL'
    end
    object BaranjeZaGOVTOR_DEL: TFIBIntegerField
      FieldName = 'VTOR_DEL'
    end
    object BaranjeZaGOMB_RAKOVODITEL: TFIBStringField
      FieldName = 'MB_RAKOVODITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGONAZIVVRABOTEN: TFIBStringField
      FieldName = 'NAZIVVRABOTEN'
      Size = 151
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGORMNAZIV: TFIBStringField
      FieldName = 'RMNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGOOdobrenieNaziv: TFIBStringField
      FieldName = 'OdobrenieNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGOKoristenjeNaziv: TFIBStringField
      FieldName = 'KoristenjeNaziv'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object BaranjeZaGODATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object BaranjeZaGODATUM1_OD: TFIBDateField
      FieldName = 'DATUM1_OD'
    end
    object BaranjeZaGODATUM1_DO: TFIBDateField
      FieldName = 'DATUM1_DO'
    end
    object BaranjeZaGODATUM2_OD: TFIBDateField
      FieldName = 'DATUM2_OD'
    end
    object BaranjeZaGODATUM2_DO: TFIBDateField
      FieldName = 'DATUM2_DO'
    end
    object BaranjeZaGORENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjeZaGOID_RE_FIRMA_NAZIV: TFIBStringField
      FieldName = 'ID_RE_FIRMA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxResenieGO: TfrxDBDataset
    UserName = #1056#1043#1054
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1080#1095#1077#1085#1041#1088
      'GODINA='#1043#1086#1076#1080#1085#1072
      'DATUM='#1044#1072#1090#1091#1084
      'VID_DOKUMENT=VID_DOKUMENT'
      'VIDDOKUMENTNAZIV=VIDDOKUMENTNAZIV'
      'KORISTENJE=KORISTENJE'
      'PRV_DEL='#1041#1088#1044#1077#1085'1'
      'VTOR_DEL='#1041#1088#1044#1077#1085'2'
      'MB_RAKOVODITEL='#1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      'NAZIVVRABOTEN='#1055#1088#1077#1079#1080#1084#1077#1048#1084#1077
      'RMNAZIV='#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'RENAZIV='#1056#1072#1073#1086#1090#1085#1072#1045#1076#1080#1085#1080#1094#1072
      'RE=RE'
      'DATUM1_OD='#1054#1044'1'
      'DATUM1_DO='#1044#1054'1'
      'DATUM2_OD='#1054#1044'2'
      'DATUM2_DO='#1044#1054'2'
      'KoristenjeNaziv='#1053#1072#1095#1080#1085
      'BARANJE_GO='#1041#1043#1054
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'BROJ='#1041#1088#1086#1112
      'MB_ZAMENA='#1047#1072#1084#1077#1085#1072
      'ZAKONSKI_MINIMUM='#1047#1072#1082#1086#1085#1052#1080#1085
      'STAZ='#1057#1090#1072#1078
      'STAZ_DENOVI='#1044#1077#1085#1057#1090#1072#1078
      'STEPEN_NA_SLOZENOST='#1044#1077#1085#1057#1057
      'SZ_NAZIV='#1057#1047#1053#1072#1079#1080#1074
      'POSEBNI_USLOVI='#1055#1086#1089#1077#1073#1085#1080#1059#1089#1083#1086#1074#1080
      'POL_VOZRAST='#1055#1086#1074#1086#1079#1088#1072#1089#1085#1080
      'ZDRASTVENA_SOSTOJBA='#1044#1077#1085#1058#1077#1083#1077#1089#1085#1086
      'DATA=DATA'
      'SUMAGO='#1042#1082#1091#1087#1085#1086
      'CUVANJE_DETE='#1063#1091#1074#1072#1114#1077#1044#1077#1090#1077
      'INVALID_DENOVI='#1048#1085#1074#1072#1083#1080#1076
      'SUMPOSEBNIUSLOVI='#1057#1091#1084#1072#1055#1086#1089#1077#1073#1085#1080#1059#1089#1083#1086#1074#1080
      'ZVANJE='#1047#1074#1072#1114#1077
      'ID_RE_FIRMA_NAZIV=ID_RE_FIRMA_NAZIV')
    DataSet = ResenieZaGO
    BCDToCurrency = False
    Left = 264
    Top = 344
  end
  object ResenieZaGO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RESENIE_GO'
      'SET '
      '    MB = :MB,'
      '    GODINA = :GODINA,'
      '    DATUM = :DATUM,'
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    KORISTENJE = :KORISTENJE,'
      '    PRV_DEL = :PRV_DEL,'
      '    VTOR_DEL = :VTOR_DEL,'
      '    MB_RAKOVODITEL = :MB_RAKOVODITEL,'
      '    DATUM1_OD = :DATUM1_OD,'
      '    DATUM1_DO = :DATUM1_DO,'
      '    DATUM2_OD = :DATUM2_OD,'
      '    DATUM2_DO = :DATUM2_DO,'
      '    BARANJE_GO = :BARANJE_GO,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    BROJ = :BROJ,'
      '    MB_ZAMENA = :MB_ZAMENA,'
      '    ZAKONSKI_MINIMUM = :ZAKONSKI_MINIMUM,'
      '    STAZ = :STAZ,'
      '    STAZ_DENOVI = :STAZ_DENOVI,'
      '    STEPEN_NA_SLOZENOST = :STEPEN_NA_SLOZENOST,'
      '    SZ_NAZIV = :SZ_NAZIV,'
      '    POSEBNI_USLOVI = :POSEBNI_USLOVI,'
      '    POL_VOZRAST = :POL_VOZRAST,'
      '    ZDRASTVENA_SOSTOJBA = :ZDRASTVENA_SOSTOJBA,'
      '    DATA = :DATA,'
      '    VKUPNO_DENOVI = :SUMAGO'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select go.id,'
      '       go.mb,'
      '       go.godina,'
      '       go.datum,'
      '       go.vid_dokument,'
      '       hvd.naziv as vidDokumentNaziv,'
      '       go.koristenje,'
      '       go.prv_del,'
      '       go.vtor_del,'
      '       go.mb_rakovoditel,'
      '       hrv.naziv_vraboten_ti as NazivVraboten,'
      '       hrv.rabotnomestonaziv as RMNaziv,'
      '       hrv.rabotnaedinicanaziv as RENaziv,'
      '       hrv.re,'
      '       hrv.id_re_firma,'
      '       go.datum1_od,'
      '       go.datum1_do,'
      '       go.datum2_od,'
      '       go.datum2_do,'
      '       case go.koristenje when '#39'1'#39' then '#39#1045#1076#1085#1086#1082#1088#1072#1090#1085#1086#39
      '                          when '#39'2'#39' then '#39#1044#1074#1086#1082#1088#1072#1090#1085#1086#39
      '       end "KoristenjeNaziv",'
      '       go.baranje_go,   '
      '       go.ts_ins,'
      '       go.ts_upd,'
      '       go.usr_ins,'
      '       go.usr_upd,'
      '       go.broj,'
      '       go.mb_zamena,'
      '       go.zakonski_minimum,'
      '       go.staz,'
      '       go.staz_denovi,'
      '       go.stepen_na_slozenost,'
      '       go.SZ_NAZIV,'
      '       go.posebni_uslovi,'
      '       go.pol_vozrast,'
      '       go.zdrastvena_sostojba,'
      '       go.data,'
      '       go.vkupno_denovi as sumaGO,'
      '       go.cuvanje_dete,'
      '       go.invalid_denovi,'
      
        '       (go.cuvanje_dete + go.invalid_denovi + go.zdrastvena_sost' +
        'ojba + go.pol_vozrast) as SumPosebniUslovi,'
      '       hrv.zvanje,'
      '       mr.naziv as id_re_firma_naziv'
      'from hr_resenie_go go'
      'inner join hr_vid_dokument hvd on hvd.id = go.vid_dokument'
      'inner join view_hr_vraboteni hrv on hrv.mb = go.mb'
      'inner join mat_re mr on mr.id = hrv.id_re_firma'
      'where go.id=:id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 400
    Top = 344
    object ResenieZaGOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object ResenieZaGOMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object ResenieZaGODATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object ResenieZaGOVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object ResenieZaGOVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOKORISTENJE: TFIBSmallIntField
      FieldName = 'KORISTENJE'
    end
    object ResenieZaGOPRV_DEL: TFIBIntegerField
      FieldName = 'PRV_DEL'
    end
    object ResenieZaGOVTOR_DEL: TFIBIntegerField
      FieldName = 'VTOR_DEL'
    end
    object ResenieZaGOMB_RAKOVODITEL: TFIBStringField
      FieldName = 'MB_RAKOVODITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGONAZIVVRABOTEN: TFIBStringField
      FieldName = 'NAZIVVRABOTEN'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGORMNAZIV: TFIBStringField
      FieldName = 'RMNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGORENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGORE: TFIBIntegerField
      FieldName = 'RE'
    end
    object ResenieZaGODATUM1_OD: TFIBDateField
      FieldName = 'DATUM1_OD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ResenieZaGODATUM1_DO: TFIBDateField
      FieldName = 'DATUM1_DO'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ResenieZaGODATUM2_OD: TFIBDateField
      FieldName = 'DATUM2_OD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ResenieZaGODATUM2_DO: TFIBDateField
      FieldName = 'DATUM2_DO'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ResenieZaGOKoristenjeNaziv: TFIBStringField
      FieldName = 'KoristenjeNaziv'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOBARANJE_GO: TFIBIntegerField
      FieldName = 'BARANJE_GO'
    end
    object ResenieZaGOTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object ResenieZaGOTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object ResenieZaGOUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOMB_ZAMENA: TFIBStringField
      FieldName = 'MB_ZAMENA'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOZAKONSKI_MINIMUM: TFIBIntegerField
      FieldName = 'ZAKONSKI_MINIMUM'
    end
    object ResenieZaGOSTAZ: TFIBSmallIntField
      FieldName = 'STAZ'
    end
    object ResenieZaGOSTAZ_DENOVI: TFIBSmallIntField
      FieldName = 'STAZ_DENOVI'
    end
    object ResenieZaGOSTEPEN_NA_SLOZENOST: TFIBSmallIntField
      FieldName = 'STEPEN_NA_SLOZENOST'
    end
    object ResenieZaGOSZ_NAZIV: TFIBStringField
      FieldName = 'SZ_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaGOPOSEBNI_USLOVI: TFIBSmallIntField
      FieldName = 'POSEBNI_USLOVI'
    end
    object ResenieZaGOPOL_VOZRAST: TFIBSmallIntField
      FieldName = 'POL_VOZRAST'
    end
    object ResenieZaGOZDRASTVENA_SOSTOJBA: TFIBSmallIntField
      FieldName = 'ZDRASTVENA_SOSTOJBA'
    end
    object ResenieZaGODATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object ResenieZaGOSUMAGO: TFIBIntegerField
      FieldName = 'SUMAGO'
    end
    object ResenieZaGOCUVANJE_DETE: TFIBSmallIntField
      FieldName = 'CUVANJE_DETE'
    end
    object ResenieZaGOINVALID_DENOVI: TFIBSmallIntField
      FieldName = 'INVALID_DENOVI'
    end
    object ResenieZaGOSUMPOSEBNIUSLOVI: TFIBBCDField
      FieldName = 'SUMPOSEBNIUSLOVI'
      Size = 0
    end
    object ResenieZaGOZVANJE: TFIBStringField
      FieldName = 'ZVANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenieZaGOID_RE_FIRMA_NAZIV: TFIBStringField
      FieldName = 'ID_RE_FIRMA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object ResenieZaPreraspredelba: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RESENIE_PRERASPREDELBA'
      'SET '
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    MB = :MB,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    NOVO_RM = :NOVO_RM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    STARO_RM = :STARO_RM,'
      '    STATUS = :STATUS,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      ' select hrp.id,'
      '       hrp.vid_dokument,'
      '       hvd.naziv as vidDokumentNaziv,'
      '       hrp.broj,'
      '       hrp.datum_kreiranje,'
      '       hrp.mb,'
      '       hrv.prezime || '#39' '#39' || hrv.ime as VrabotenNaziv,'
      '       hrp.datum_od,'
      '       hrp.datum_do,'
      '       hrp.novo_rm,'
      '       hrp.staro_rm,'
      '       hrp.status, '
      '       (select hrmrab.naziv as staroNaziv'
      '        from hr_rm_re hrm'
      
        '        inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id' +
        '_rm'
      
        '        inner join hr_sistematizacija hs on hs.id = hrm.id_siste' +
        'matizacija'
      '        where hs.do_datum is null and hrm.id = staro_rm),'
      '       (select hrmrab.naziv as novoNaziv'
      '        from hr_rm_re hrm'
      
        '        inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id' +
        '_rm'
      
        '        inner join hr_sistematizacija hs on hs.id = hrm.id_siste' +
        'matizacija'
      '        where hs.do_datum is null and hrm.id = novo_rm),'
      '       dv.broj as dogovor_broj,  '
      '       dv.ID id_dog,'
      '       dv.DATUM dog_datum ,'
      '       hrp.data,'
      '       hrv.zvanje'
      'from hr_resenie_preraspredelba hrp'
      'inner join hr_vid_dokument hvd on hvd.id = hrp.vid_dokument'
      'inner join hr_vraboten hrv on hrv.mb = hrp.mb'
      
        'inner join hr_dogovor_vrabotuvanje dv on dv.mb = hrp.mb and hrp.' +
        'datum_kreiranje between dv.datum_od and coalesce(dv.datum_do, cu' +
        'rrent_date)'
      'where  hrp.id = :id'
      'order by hrp.datum_od ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 400
    Top = 400
    object ResenieZaPreraspredelbaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object ResenieZaPreraspredelbaVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object ResenieZaPreraspredelbaVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaVRABOTENNAZIV: TFIBStringField
      FieldName = 'VRABOTENNAZIV'
      Size = 151
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object ResenieZaPreraspredelbaDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object ResenieZaPreraspredelbaNOVO_RM: TFIBIntegerField
      FieldName = 'NOVO_RM'
    end
    object ResenieZaPreraspredelbaSTARO_RM: TFIBIntegerField
      FieldName = 'STARO_RM'
    end
    object ResenieZaPreraspredelbaSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
    object ResenieZaPreraspredelbaSTARONAZIV: TFIBStringField
      FieldName = 'STARONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaNOVONAZIV: TFIBStringField
      FieldName = 'NOVONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaDATUM_KREIRANJE: TFIBDateField
      FieldName = 'DATUM_KREIRANJE'
    end
    object ResenieZaPreraspredelbaDOGOVOR_BROJ: TFIBStringField
      FieldName = 'DOGOVOR_BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object ResenieZaPreraspredelbaZVANJE: TFIBStringField
      FieldName = 'ZVANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenieZaPreraspredelbaID_DOG: TFIBIntegerField
      FieldName = 'ID_DOG'
    end
    object tblResenieZaPreraspredelbaDOG_DATUM: TFIBDateField
      FieldName = 'DOG_DATUM'
    end
  end
  object frxResenieZaPreraspredelba: TfrxDBDataset
    UserName = #1056#1077#1096#1077#1085#1080#1077#1047#1072#1055#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'VID_DOKUMENT=VID_DOKUMENT'
      'VIDDOKUMENTNAZIV=VIDDOKUMENTNAZIV'
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'VRABOTENNAZIV='#1055#1088#1077#1079#1080#1084#1077' '#1080' '#1048#1084#1077
      'DATUM_OD='#1044#1072#1090#1091#1084' '#1086#1076
      'DATUM_DO='#1044#1072#1090#1091#1084' '#1076#1086
      'NOVO_RM=NOVO_RM'
      'STARO_RM=STARO_RM'
      'STATUS=STATUS'
      'STARONAZIV='#1057#1090#1072#1088#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'NOVONAZIV='#1053#1086#1074#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'BROJ='#1041#1088#1086#1112#1056#1077#1096#1077#1085#1080#1077
      'DATUM_KREIRANJE='#1044#1072#1090#1091#1084#1050#1088#1077#1080#1088#1072#1114#1077
      'DOGOVOR_BROJ='#1041#1088#1086#1112#1044#1086#1075#1086#1074#1086#1088#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      'DATA=DATA'
      'ZVANJE='#1047#1074#1072#1114#1077)
    DataSet = ResenieZaPreraspredelba
    BCDToCurrency = False
    Left = 264
    Top = 400
  end
  object frxPrekinRabotenOdnos: TfrxDBDataset
    UserName = #1055#1088#1077#1082#1080#1085#1053#1072#1056#1072#1073#1086#1090#1077#1085#1054#1076#1085#1086#1089
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'ID_RE_FIRMA=ID_RE_FIRMA'
      'ID_RM_RE=ID_RM_RE'
      'DATUM_DO='#1044#1072#1090#1091#1084' '#1076#1086
      'OBRAZLOZENIE='#1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077
      'PRICINA='#1055#1088#1080#1095#1080#1085#1072' ('#1064#1080#1092#1088#1072')'
      'DIREKTOR='#1044#1080#1088#1077#1082#1090#1086#1088
      'LICENAZIV='#1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1048#1084#1077' '#1048#1084#1077
      'RABOTNOMESONAZIV='#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'RENAZIV='#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      'PRICINANAZIV='#1055#1088#1080#1095#1080#1085#1072
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'DATA=DATA'
      'ZVANJE='#1047#1074#1072#1114#1077
      'ID_RE_FIRMA_NAZIV=ID_RE_FIRMA_NAZIV')
    DataSet = PrestanokNaRabotenOdnos
    BCDToCurrency = False
    Left = 264
    Top = 464
  end
  object PrestanokNaRabotenOdnos: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RESENIE_OTKAZ'
      'SET '
      '    MB = :MB,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    DATUM_DO = :DATUM_DO,'
      '    OBRAZLOZENIE = :OBRAZLOZENIE,'
      '    PRICINA = :PRICINA,'
      '    DIREKTOR = :DIREKTOR,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.mb,'
      '       hro.id_re_firma,'
      '       m_id_re_firma.naziv as  id_re_firma_naziv,'
      '       hro.id_rm_re,'
      '       hro.datum_do,'
      '       hro.obrazlozenie,'
      '       hro.pricina,'
      '       hro.direktor,'
      
        '       hrv.prezime || '#39' '#39' || coalesce(hrv.tatkovo_ime, '#39#39') ||'#39' '#39 +
        '|| hrv.ime as LiceNaziv,'
      '       hrrabm.naziv as RabotnoMesoNaziv,'
      '       mr.naziv as ReNaziv,'
      '       hrvo.naziv as pricinaNaziv,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hro.data,'
      '       hrv.zvanje'
      'from hr_resenie_otkaz hro'
      'inner join hr_vraboten hrv on hrv.mb = hro.mb'
      'inner join hr_rm_re hrm on hrm.id = hro.id_rm_re'
      'inner join mat_re mr on mr.id = hrm.id_re'
      
        'inner join mat_re m_id_re_firma on m_id_re_firma.id = hro.id_re_' +
        'firma'
      'inner join hr_rabotno_mesto hrrabm on hrrabm.id = hrm.id_rm'
      
        'left outer join hr_vid_prekin_rab_odnos hrvo on hrvo.id = hro.pr' +
        'icina'
      'where hro.id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 400
    Top = 464
    object PrestanokNaRabotenOdnosID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PrestanokNaRabotenOdnosMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosID_RE_FIRMA: TFIBIntegerField
      FieldName = 'ID_RE_FIRMA'
    end
    object PrestanokNaRabotenOdnosID_RM_RE: TFIBIntegerField
      FieldName = 'ID_RM_RE'
    end
    object PrestanokNaRabotenOdnosDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object PrestanokNaRabotenOdnosOBRAZLOZENIE: TFIBStringField
      FieldName = 'OBRAZLOZENIE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosPRICINA: TFIBIntegerField
      FieldName = 'PRICINA'
    end
    object PrestanokNaRabotenOdnosDIREKTOR: TFIBStringField
      FieldName = 'DIREKTOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosLICENAZIV: TFIBStringField
      FieldName = 'LICENAZIV'
      Size = 202
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosRABOTNOMESONAZIV: TFIBStringField
      FieldName = 'RABOTNOMESONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosRENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosPRICINANAZIV: TFIBStringField
      FieldName = 'PRICINANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object PrestanokNaRabotenOdnosTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object PrestanokNaRabotenOdnosUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object PrestanokNaRabotenOdnosZVANJE: TFIBStringField
      FieldName = 'ZVANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrestanokNaRabotenOdnosID_RE_FIRMA_NAZIV: TFIBStringField
      FieldName = 'ID_RE_FIRMA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Rich Text  Format|*.rtf'
    InitialDir = '...\HRM\Template'
    Title = #1048#1079#1073#1077#1088#1077#1090#1077' '#1090#1077#1088#1082' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
    Left = 120
    Top = 320
  end
  object Sistematizacija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_SISTEMATIZACIJA'
      'SET '
      '    OPIS = :OPIS,'
      '    OD_DATUM = :OD_DATUM,'
      '    DO_DATUM = :DO_DATUM,'
      '    DOKUMENT = :DOKUMENT,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.id_re_firma,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd,'
      '    hs.data'
      'from hr_sistematizacija hs'
      'where hs.id_re_firma=:firma and hs.do_datum is null')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 400
    Top = 523
    oRefreshDeletedRecord = True
    object SistematizacijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object SistematizacijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object SistematizacijaOD_DATUM: TFIBDateField
      DisplayLabel = #1054#1076' '#1076#1072#1090#1091#1084
      FieldName = 'OD_DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object SistematizacijaDO_DATUM: TFIBDateField
      DisplayLabel = #1044#1086' '#1076#1072#1090#1091#1084
      FieldName = 'DO_DATUM'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object SistematizacijaDOKUMENT: TFIBStringField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'DOKUMENT'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object SistematizacijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object SistematizacijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object SistematizacijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object SistematizacijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object SistematizacijaID_RE_FIRMA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1092#1086#1088#1084#1072
      FieldName = 'ID_RE_FIRMA'
    end
    object SistematizacijaDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxSistematizacija: TfrxDBDataset
    UserName = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'OPIS=OPIS'
      'OD_DATUM=OD_DATUM'
      'DO_DATUM=DO_DATUM'
      'DOKUMENT=DOKUMENT'
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'ID_RE_FIRMA=ID_RE_FIRMA'
      'DATA=DATA')
    DataSet = Sistematizacija
    BCDToCurrency = False
    Left = 264
    Top = 528
  end
  object frxAneks: TfrxDBDataset
    UserName = #1040#1085#1077#1082#1089
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJANEKS='#1041#1088#1086#1112
      'DATUM='#1044#1072#1090#1091#1084
      'DATUM_POCETOK='#1044#1072#1090#1091#1084#1054#1076
      'DATUM_DO='#1044#1072#1090#1091#1084#1044#1086
      'BROJ_CASOVI='#1041#1088#1063#1072#1089#1086#1074#1080
      'RAKOVODITEL='#1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      'KOEFICIENT='#1050#1086#1077#1092#1080#1094#1080#1077#1085#1090
      'RAKOVODENJE='#1056#1072#1082#1086#1074#1086#1076#1077#1114#1077
      'USLOVI_RABOTA='#1059#1089#1083#1086#1074#1080#1056#1072#1073#1086#1090#1072
      'DEN_USLOVI_RABOTA='#1044#1077#1085#1059#1089#1083#1086#1074#1080#1056#1072#1073#1086#1090#1072
      'PLATA='#1055#1083#1072#1090#1072
      'MB=MB'
      'BROJDOGOVOR='#1041#1088#1044#1086#1075#1086#1074#1086#1088
      'NAZIV_VRABOTEN='#1053#1072#1079#1080#1074#1042#1088#1072#1073#1086#1090#1077#1085
      'NAZIV_VRABOTEN_TI='#1053#1072#1079#1080#1074#1042#1088#1072#1073#1086#1090#1077#1085#1058#1048
      'VIDVRABOTUVANJE='#1042#1080#1076#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      'ARHIVSKI_BROJ='#1040#1088#1093#1080#1074#1089#1082#1080#1041#1088#1086#1112
      'RABOTNOMESTONAZIV='#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086#1053#1072#1079#1080#1074
      'ADRESA='#1040#1076#1088#1077#1089#1072
      'BROJODADRESA='#1041#1088#1054#1076#1040#1076#1088#1077#1089#1072
      'MESTONAZIV='#1052#1077#1089#1090#1086
      'OPIS='#1054#1087#1080#1089
      'DATA=DATA'
      'USLOVIRABNAZIV='#1059#1089#1083#1086#1074#1080#1056#1072#1073#1053#1072#1079#1080#1074
      'ID=ID'
      'RABOTNAEDINICANAZIV='#1056#1072#1073#1086#1090#1085#1072#1045#1076#1080#1085#1080#1094#1072#1053#1072#1079#1080#1074
      'STEPENSTRUCNAPODGOTOVKANAZIV='#1057#1090#1077#1087#1077#1085#1057#1090#1088#1091#1095#1085#1072#1055#1086#1076#1075#1086#1090#1086#1074#1082#1072#1053#1072#1079#1080#1074
      'STEPENZAVRSENOOBRAZOVANIE='#1057#1090#1077#1087#1077#1085#1047#1072#1074#1088#1096#1077#1085#1086#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      'STEPENZAVRSENOOBRAZOVANIEOPIS='#1057#1090#1077#1087#1077#1085#1047#1072#1074#1088#1096#1077#1085#1086#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077#1054#1087#1080#1089
      'NASOKA=NASOKA'
      'NASOKANAZIV='#1053#1072#1086#1082#1072#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      'ZNAENJE='#1057#1090#1077#1082#1085#1072#1086#1090#1086#1047#1085#1072#1077#1114#1077#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      'ZVANJE='#1047#1074#1072#1114#1077
      'NETO=NETO'
      'BODOVI=BODOVI')
    DataSet = tblAneksReport
    BCDToCurrency = False
    Left = 256
    Top = 592
  end
  object frxDBDogVolonteri: TfrxDBDataset
    UserName = #1044#1086#1075#1042#1086#1083#1086#1085
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'MB='#1052#1072#1090#1041#1088
      'PREZIME='#1055#1088#1077#1079#1080#1084#1077
      'TATKOVO_IME='#1058#1072#1090#1082#1086#1074#1086#1048#1084#1077
      'IME='#1048#1084#1077
      'NAZIVVRABOTEN='#1055#1088#1077#1079#1080#1084#1077#1058#1072#1090#1082#1086#1074#1086#1048#1084#1077
      'NAZIVRABMESTO='#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'ID_RM_RE=ID_RM_RE'
      'VID_DOKUMENT=VID_DOKUMENT'
      'ID_RE_FIRMA=ID_RE_FIRMA'
      'DATUM_OD='#1044#1072#1090#1091#1084#1054#1076
      'DATUM_DO='#1044#1072#1090#1091#1084#1044#1086
      'PLATA='#1055#1083#1072#1090#1072
      'BROJ_CASOVI='#1041#1088#1063#1072#1089#1086#1074#1080
      'BROJ='#1041#1088#1086#1112
      'RAKOVODITEL='#1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'DATA=DATA'
      'DATUM='#1044#1072#1090#1091#1084
      'LICNA_KARTA='#1041#1088#1051#1080#1095#1085#1072#1050#1072#1088#1090#1072
      'ZVANJE='#1047#1074#1072#1114#1077)
    DataSet = tblDogVolonteriodberi
    BCDToCurrency = False
    Left = 256
    Top = 656
  end
  object tblDogVolonteriodberi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_DOGOVOR_VOLONTERI'
      'SET '
      '    MB = :MB,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    PLATA = :PLATA,'
      '    BROJ_CASOVI = :BROJ_CASOVI,'
      '    BROJ = :BROJ,'
      '    RAKOVODITEL = :RAKOVODITEL,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DATA = :DATA,'
      '    DATUM = :DATUM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hdv.id,'
      '       hdv.mb,'
      '       hrv.prezime,'
      '       hrv.tatkovo_ime,'
      '       hrv.ime,'
      
        '       hrv.prezime || '#39' '#39' || coalesce(hrv.tatkovo_ime, '#39' '#39') || '#39 +
        ' '#39' || hrv.ime as NazivVraboten,'
      '       hrmrab.naziv as NazivRabMesto,'
      '       hdv.id_rm_re,'
      '       hdv.vid_dokument,'
      '       hdv.id_re_firma,'
      '       hdv.datum_od,'
      '       hdv.datum_do,'
      '       hdv.plata,'
      '       hdv.broj_casovi,'
      '       hdv.broj,'
      '       hdv.rakovoditel,'
      '       hdv.ts_ins,'
      '       hdv.ts_upd,'
      '       hdv.usr_ins,'
      '       hdv.usr_upd,'
      '       hdv.data,'
      '       hdv.datum,'
      '       hrv.LICNA_KARTA,'
      '       hrv.zvanje'
      'from hr_dogovor_volonteri hdv'
      'inner join hr_vraboten hrv on hrv.mb = hdv.mb'
      'inner join hr_rm_re hrm on hrm.id = hdv.id_rm_re'
      'inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id_rm'
      
        'inner join hr_sistematizacija hrs on hrs.id = hrm.id_sistematiza' +
        'cija'
      'where hrs.do_datum is null and hdv.id = :id'
      '')
    AutoUpdateOptions.UpdateTableName = 'HR_DOGOVOR_VOLONTERI'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_DOGOVOR_VOLONTERI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 388
    Top = 656
    object tblDogVolonteriodberiID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblDogVolonteriodberiMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiPREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiTATKOVO_IME: TFIBStringField
      FieldName = 'TATKOVO_IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiIME: TFIBStringField
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiNAZIVVRABOTEN: TFIBStringField
      FieldName = 'NAZIVVRABOTEN'
      Size = 202
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiNAZIVRABMESTO: TFIBStringField
      FieldName = 'NAZIVRABMESTO'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiID_RM_RE: TFIBIntegerField
      FieldName = 'ID_RM_RE'
    end
    object tblDogVolonteriodberiVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object tblDogVolonteriodberiID_RE_FIRMA: TFIBIntegerField
      FieldName = 'ID_RE_FIRMA'
    end
    object tblDogVolonteriodberiDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object tblDogVolonteriodberiDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblDogVolonteriodberiPLATA: TFIBBCDField
      FieldName = 'PLATA'
      Size = 2
    end
    object tblDogVolonteriodberiBROJ_CASOVI: TFIBSmallIntField
      FieldName = 'BROJ_CASOVI'
    end
    object tblDogVolonteriodberiBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiRAKOVODITEL: TFIBStringField
      FieldName = 'RAKOVODITEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblDogVolonteriodberiTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblDogVolonteriodberiUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object tblDogVolonteriodberiDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblDogVolonteriodberiLICNA_KARTA: TFIBStringField
      FieldName = 'LICNA_KARTA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogVolonteriodberiZVANJE: TFIBStringField
      FieldName = 'ZVANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxOdjava: TfrxDBDataset
    UserName = #1054#1076#1112#1072#1074#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'TIP=TIP'
      'VID_DOKUMENT=VID_DOKUMENT'
      'MB=MB'
      'DATUM=DATUM'
      'PROBNA=PROBNA'
      'OSNOV=OSNOV'
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'INSTITUCIJA=INSTITUCIJA'
      'PREZIME=PREZIME'
      'IME=IME'
      'RABMESTONAZIV=RABMESTONAZIV'
      'ProbnoNaziv=ProbnoNaziv'
      'OSNOVNAZIV=OSNOVNAZIV'
      'ID_RM_RE=ID_RM_RE'
      'PO_PRIJAVA_ID=PO_PRIJAVA_ID'
      'BROJ=BROJ')
    DataSet = dm.tblOdjava
    BCDToCurrency = False
    Left = 256
    Top = 728
  end
  object frxDBPrijava: TfrxDBDataset
    UserName = #1055#1088#1080#1112#1072#1074#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'TIP=TIP'
      'VID_DOKUMENT=VID_DOKUMENT'
      'MB=MB'
      'DATUM=DATUM'
      'PROBNA=PROBNA'
      'OSNOV=OSNOV'
      'STAZ=STAZ'
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'INSTITUCIJA=INSTITUCIJA'
      'PREZIME=PREZIME'
      'IME=IME'
      'RABMESTONAZIV=RABMESTONAZIV'
      'RAKOVODNO=RAKOVODNO'
      'RakovodnoNaziv=RakovodnoNaziv'
      'ProbnoNaziv=ProbnoNaziv'
      'OSNOVNAZIV=OSNOVNAZIV'
      'ID_RM_RE=ID_RM_RE'
      'BROJ=BROJ')
    DataSet = dm.tblPrijava
    BCDToCurrency = False
    Left = 384
    Top = 728
  end
  object frxJSOpstiPodatoci: TfrxDBDataset
    UserName = #1032#1057#1054#1087#1096#1090#1080#1055#1086#1076#1072#1090#1086#1094#1080
    CloseDataSource = False
    FieldAliases.Strings = (
      'MB=MB'
      'PREZIME=PREZIME'
      'TATKOVO_IME=TATKOVO_IME'
      'IME=IME'
      'POL=POL'
      'PolNaziv=PolNaziv'
      'DATUM_RADJANJE=DATUM_RADJANJE'
      'MESTO_RADJANJE=MESTO_RADJANJE'
      'MESTORAGJANJENAZIV=MESTORAGJANJENAZIV'
      'DRZAVARAGJANJENAZIV=DRZAVARAGJANJENAZIV'
      'OPSTINARAGJANJENAZIV=OPSTINARAGJANJENAZIV'
      'PRIPADNOSTZAEDNICA=PRIPADNOSTZAEDNICA'
      'ADRESAZIVEENJE=ADRESAZIVEENJE'
      'MESTOZIVENJENAZIV=MESTOZIVENJENAZIV'
      'ID_NACIONALNOST=ID_NACIONALNOST'
      'OPSTINAZIVENJENAZIV=OPSTINAZIVENJENAZIV'
      'NASOKA=NASOKA'
      'STEPEN_OBRAZOVANIE=STEPEN_OBRAZOVANIE'
      'IME_ORGANIZACIJA=IME_ORGANIZACIJA'
      'NASOKANAZIV=NASOKANAZIV'
      'OINAZIV=OINAZIV'
      'SZNAZIV=SZNAZIV')
    DataSet = dm.tblJSOpstiPodatoci
    BCDToCurrency = False
    Left = 264
    Top = 784
  end
  object frxPotvrdaRabotenOdnos: TfrxDBDataset
    UserName = #1055#1056#1056
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1041#1088
      'ARHIVSKI_BROJ='#1040#1088#1093#1080#1074#1089#1082#1080#1041#1088
      'DATUM='#1044#1072#1090#1091#1084
      'GODINA='#1043#1086#1076#1080#1085#1072
      'ZABELESKA='#1047#1072#1073#1077#1083#1077#1096#1082#1072
      'VIDDOKUMENTNAZIV='#1042#1080#1076#1044#1086#1082#1091#1084#1077#1085#1090
      'BROJ='#1041#1088#1086#1112
      'NAZIV_VRABOTEN='#1053#1072#1079#1080#1074#1051#1080#1094#1077
      'NAZIV_VRABOTEN_TI='#1053#1072#1079#1080#1074#1051#1080#1094#1077#1058#1048
      'RABOTNAEDINICANAZIV='#1056#1072#1073#1086#1090#1085#1072#1045#1076#1080#1085#1080#1094#1072
      'RABOTNOMESTONAZIV='#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'DATA='#1044#1086#1082#1091#1084#1077#1085#1090)
    DataSet = PotvrdaRabotenOdnos
    BCDToCurrency = False
    Left = 32
    Top = 400
  end
  object PotvrdaRabotenOdnos: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_POTVRDA_RRO'
      'SET '
      '    MB = :MB,'
      '    ARHIVSKI_BROJ = :ARHIVSKI_BROJ,'
      '    DATUM = :DATUM,'
      '    GODINA = :GODINA,'
      '    ZABELESKA = :ZABELESKA,'
      '    BROJ = :BROJ,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select rro.id,'
      '       rro.mb,'
      '       rro.arhivski_broj,'
      '       rro.datum,'
      '       rro.godina,'
      '       rro.zabeleska,'
      '       v.naziv as vidDokumentNaziv,'
      '       rro.broj,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrv.rabotnaedinicanaziv,'
      '       hrv.rabotnomestonaziv,'
      '       rro.data'
      'from hr_potvrda_rro rro'
      'inner join hr_vid_dokument v on v.id = rro.vid_dokument'
      'inner join view_hr_vraboteni hrv on hrv.mb = rro.mb'
      'where rro.id = :id'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 120
    Top = 400
    object PotvrdaRabotenOdnosID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PotvrdaRabotenOdnosMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosARHIVSKI_BROJ: TFIBStringField
      FieldName = 'ARHIVSKI_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object PotvrdaRabotenOdnosGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object PotvrdaRabotenOdnosZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosNAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosNAZIV_VRABOTEN_TI: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosRABOTNAEDINICANAZIV: TFIBStringField
      FieldName = 'RABOTNAEDINICANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosRABOTNOMESTONAZIV: TFIBStringField
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object PotvrdaRabotenOdnosDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    DefaultPath = 'template'
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PictureType = gpPNG
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 24
    Top = 320
  end
  object tblAneksReport: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_DV_ANEKS'
      'SET '
      '    BROJ = :BROJANEKS,'
      '    DATUM = :DATUM,'
      '    DATUM_POCETOK = :DATUM_POCETOK,'
      '    DATUM_DO = :DATUM_DO,'
      '    BROJ_CASOVI = :BROJ_CASOVI,'
      '    RAKOVODITEL = :RAKOVODITEL,'
      '    KOEFICIENT = :KOEFICIENT,'
      '    RAKOVODENJE = :RAKOVODENJE,'
      '    USLOVI_RABOTA = :USLOVI_RABOTA,'
      '    DEN_USLOVI_RABOTA = :DEN_USLOVI_RABOTA,'
      '    PLATA = :PLATA,'
      '    ARHIVSKI_BROJ = :ARHIVSKI_BROJ,'
      '    OPIS = :OPIS,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select hda.id,'
      '       hda.broj as brojAneks,'
      '       hda.datum,'
      '       hda.datum_pocetok,'
      '       hda.datum_do,'
      '       hda.broj_casovi,'
      '       hda.rakovoditel,'
      '       hda.koeficient,'
      '       hda.rakovodenje,'
      '       hda.uslovi_rabota,'
      '       hda.den_uslovi_rabota,'
      '       hda.plata,'
      '       hdv.mb,'
      '       hdv.broj as BrojDogovor,'
      '       hdv.naziv_vraboten, hdv.naziv_vraboten_ti,'
      '       hrv.naziv as VidVrabotuvanje,'
      '       hda.arhivski_broj,'
      '       hrmrab.naziv as RabotnoMestoNaziv,'
      '       hrk.id as usloviRabID,'
      '       hrk.naziv as usloviRabNaziv,'
      '       hrk.procent as usloviRabProcent,'
      '       k.adresa, k.broj as brojOdAdresa,  m.naziv as mestoNaziv,'
      '       hda.Opis,'
      '       hda.data'
      'from hr_dv_aneks hda'
      
        'inner join hr_dogovor_vrabotuvanje hdv on hdv.id = hda.id_dogovo' +
        'r_vrabouvanje'
      'inner join hr_vraboten lice on lice.mb = hdv.mb'
      
        'left outer join hr_kontakt k on k.mb_vraboten = lice.mb and k.lk' +
        ' = 1'
      'left outer join mat_mesto m on m.id = k.mesto'
      
        'inner join hr_vid_vrabotuvanje hrv on hrv.id = hda.vid_vrabotuva' +
        'nje'
      'inner join hr_rm_re hrm on hrm.id = hda.id_rm_re'
      'inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id_rm'
      
        'inner join hr_sistematizacija hrs on hrs.id = hrm.id_sistematiza' +
        'cija  and hrs.id_re_firma = hdv.id_re_firma'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hda.uslo' +
        'vi_rabota'
      'where(  hda.id_dogovor_vrabouvanje = :dogv and hda.id like :id'
      '     ) and (     HDA.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select hda.id,'
      '       hda.broj as brojAneks,'
      '       hda.datum,'
      '       hda.datum_pocetok,'
      '       hda.datum_do,'
      '       hda.broj_casovi,'
      '       hda.rakovoditel,'
      '       hda.koeficient,'
      '       hda.rakovodenje,'
      '       hda.uslovi_rabota,'
      '       hda.den_uslovi_rabota,'
      '       hda.plata,'
      '       hdv.mb,'
      '       hdv.broj as BrojDogovor,'
      '       hdv.naziv_vraboten, hdv.naziv_vraboten_ti,'
      '       hrv.naziv as VidVrabotuvanje,'
      '       hda.arhivski_broj,'
      '       hrmrab.naziv as RabotnoMestoNaziv,'
      
        '       hdv.ADRESA, k.broj as brojOdAdresa,  m.naziv as mestoNazi' +
        'v,'
      '       hda.Opis,'
      '       hda.data,'
      '       (select proc_hr_template.uslovirabotanaziv'
      '        from proc_hr_template(hda.id_rm_re)) usloviRabNaziv,'
      '      mr.naziv as rabotnaEdinicaNaziv,'
      '      po.opis as stepenStrucnaPodgotovkaNaziv,'
      '      hrso.naziv as StepenZavrsenoObrazovanie,'
      '      hrso.opis  as StepenZavrsenoObrazovanieOpis,'
      '      oo.nasoka,'
      '      n.naziv as nasokaNaziv,'
      '      oo.znaenje,'
      '      lice.zvanje,'
      '      hdv.neto,'
      '      hdv.bodovi'
      'from hr_dv_aneks hda'
      
        'inner join hr_dogovor_vrabotuvanje hdv on hdv.id = hda.id_dogovo' +
        'r_vrabouvanje'
      'inner join hr_vraboten lice on lice.mb = hdv.mb'
      
        'left outer join hr_kontakt k on k.mb_vraboten = lice.mb and k.lk' +
        ' = 1'
      'left outer join mat_mesto m on m.id = hdv.mesto'
      
        'inner join hr_vid_vrabotuvanje hrv on hrv.id = hda.vid_vrabotuva' +
        'nje'
      'inner join hr_rm_re hrm on hrm.id = hda.id_rm_re'
      'inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id_rm'
      
        'inner join hr_sistematizacija hrs on hrs.id = hrm.id_sistematiza' +
        'cija  and hrs.id_re_firma = hdv.id_re_firma'
      'inner join mat_re mr on mr.id = hrm.id_re'
      
        'inner join plt_obrazovanie po on po.id = hdv.stepen_strucna_podg' +
        'otovka'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hda.uslo' +
        'vi_rabota'
      
        'left outer join hr_sz_obrazovanie hrso on hrso.id = hrmrab.id_sz' +
        '_obrazovanie'
      
        'left outer join hr_obrazovanie_obuka oo on oo.mb_vraboten = lice' +
        '.mb'
      'left outer join hr_nasoka_obrazovanie n on n.id = oo.nasoka'
      'where hda.id_dogovor_vrabouvanje = :dogv and hda.id like :id'
      'order by hda.datum_do')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 392
    Top = 584
    object tblAneksReportBROJANEKS: TFIBStringField
      FieldName = 'BROJANEKS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblAneksReportDATUM_POCETOK: TFIBDateField
      FieldName = 'DATUM_POCETOK'
    end
    object tblAneksReportDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblAneksReportBROJ_CASOVI: TFIBBCDField
      FieldName = 'BROJ_CASOVI'
      Size = 2
    end
    object tblAneksReportRAKOVODITEL: TFIBStringField
      FieldName = 'RAKOVODITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportKOEFICIENT: TFIBBCDField
      FieldName = 'KOEFICIENT'
      Size = 8
    end
    object tblAneksReportRAKOVODENJE: TFIBBCDField
      FieldName = 'RAKOVODENJE'
      Size = 2
    end
    object tblAneksReportUSLOVI_RABOTA: TFIBIntegerField
      FieldName = 'USLOVI_RABOTA'
    end
    object tblAneksReportDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblAneksReportPLATA: TFIBBCDField
      FieldName = 'PLATA'
      Size = 2
    end
    object tblAneksReportMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportBROJDOGOVOR: TFIBStringField
      FieldName = 'BROJDOGOVOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportNAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportNAZIV_VRABOTEN_TI: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportVIDVRABOTUVANJE: TFIBStringField
      FieldName = 'VIDVRABOTUVANJE'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportARHIVSKI_BROJ: TFIBStringField
      FieldName = 'ARHIVSKI_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportRABOTNOMESTONAZIV: TFIBStringField
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportBROJODADRESA: TFIBStringField
      FieldName = 'BROJODADRESA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportMESTONAZIV: TFIBStringField
      FieldName = 'MESTONAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportOPIS: TFIBBlobField
      FieldName = 'OPIS'
      Size = 8
    end
    object tblAneksReportDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
    object tblAneksReportUSLOVIRABNAZIV: TFIBStringField
      FieldName = 'USLOVIRABNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblAneksReportRABOTNAEDINICANAZIV: TFIBStringField
      FieldName = 'RABOTNAEDINICANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportSTEPENSTRUCNAPODGOTOVKANAZIV: TFIBStringField
      FieldName = 'STEPENSTRUCNAPODGOTOVKANAZIV'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportSTEPENZAVRSENOOBRAZOVANIE: TFIBStringField
      FieldName = 'STEPENZAVRSENOOBRAZOVANIE'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportSTEPENZAVRSENOOBRAZOVANIEOPIS: TFIBStringField
      FieldName = 'STEPENZAVRSENOOBRAZOVANIEOPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportNASOKA: TFIBIntegerField
      FieldName = 'NASOKA'
    end
    object tblAneksReportNASOKANAZIV: TFIBStringField
      FieldName = 'NASOKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportZNAENJE: TFIBStringField
      FieldName = 'ZNAENJE'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportZVANJE: TFIBStringField
      FieldName = 'ZVANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAneksReportNETO: TFIBBCDField
      FieldName = 'NETO'
      Size = 8
    end
    object tblAneksReportBODOVI: TFIBBCDField
      FieldName = 'BODOVI'
      Size = 2
    end
  end
  object frxVozrastNaDatum: TfrxDBDataset
    UserName = 'frxVozrastNaDatum'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GODINA_OUT=GODINA_OUT'
      'MESEC_OUT=MESEC_OUT'
      'VRABNAZIV_OUT=VRABNAZIV_OUT'
      'DATUMRAGJANJE_OUT=DATUMRAGJANJE_OUT'
      'RM_RE_OUT=RM_RE_OUT'
      'RABMESTONAZIV=RABMESTONAZIV'
      'RE_OUT=RE_OUT'
      'RENAZIV_OUT=RENAZIV_OUT'
      'POTEKLO_OUT=POTEKLO_OUT'
      'PARAM=PARAM'
      'NAZIV_VRABOTEN_TI=NAZIV_VRABOTEN_TI'
      'STAZ_GODINI_OUT=STAZ_GODINI_OUT'
      'STAZ_MESECI_OUT=STAZ_MESECI_OUT'
      'STAZ_DENOVI_OUT=STAZ_DENOVI_OUT'
      'MB_OUT=MB_OUT')
    DataSet = dm.tblVozrastNaDatum
    BCDToCurrency = False
    Left = 384
    Top = 784
  end
  object frxizjavaProdolzuvanjeRO: TfrxDBDataset
    UserName = #1048#1055#1056#1054
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'MB='#1052#1072#1090#1041#1088
      'ARHIVSKI_BROJ=ARHIVSKI_BROJ'
      'DATUM='#1044#1072#1090#1091#1084
      'GODINA='#1043#1086#1076#1080#1085#1072
      'ZABELESKA='#1047#1072#1073#1077#1083#1077#1096#1082#1072
      'VIDDOKUMENTNAZIV=VIDDOKUMENTNAZIV'
      'BROJ='#1041#1088#1086#1112
      'NAZIV_VRABOTEN='#1053#1072#1079#1080#1074#1051#1080#1094#1077
      'NAZIV_VRABOTEN_TI='#1053#1072#1079#1080#1074#1051#1080#1094#1077#1058#1048
      'RABOTNAEDINICANAZIV='#1056#1072#1073#1086#1090#1085#1072#1045#1076#1080#1085#1080#1094#1072
      'RABOTNOMESTONAZIV='#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'DATA=DATA'
      'DATUM_OD='#1044#1072#1090#1091#1084'_'#1086#1076
      'DATUM_DO='#1044#1072#1090#1091#1084'_'#1076#1086)
    DataSet = tblizjavaProdolzuvanjeRO
    BCDToCurrency = False
    Left = 32
    Top = 464
  end
  object tblizjavaProdolzuvanjeRO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE hr_izjava_prodolzen_ro'
      'SET '
      '    MB = :MB,'
      '    ARHIVSKI_BROJ = :ARHIVSKI_BROJ,'
      '    DATUM = :DATUM,'
      '    GODINA = :GODINA,'
      '    ZABELESKA = :ZABELESKA,'
      '    BROJ = :BROJ,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select rro.id,'
      '       rro.mb,'
      '       rro.arhivski_broj,'
      '       rro.datum,'
      '       rro.datum_od, rro.datum_do,'
      '       rro.godina,'
      '       rro.zabeleska,'
      '       v.naziv as vidDokumentNaziv,'
      '       rro.broj,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrv.rabotnaedinicanaziv,'
      '       hrv.rabotnomestonaziv,'
      '       rro.data'
      'from hr_izjava_prodolzen_ro rro'
      'inner join hr_vid_dokument v on v.id = rro.vid_dokument'
      'inner join view_hr_vraboteni hrv on hrv.mb = rro.mb'
      'where(  rro.id = :id'
      '     ) and (     RRO.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select rro.id,'
      '       rro.mb,'
      '       rro.arhivski_broj,'
      '       rro.datum,'
      '       rro.datum_od, rro.datum_do,'
      '       rro.godina,'
      '       rro.zabeleska,'
      '       v.naziv as vidDokumentNaziv,'
      '       rro.broj,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrv.rabotnaedinicanaziv,'
      '       hrv.rabotnomestonaziv,'
      '       rro.data'
      'from hr_izjava_prodolzen_ro rro'
      'inner join hr_vid_dokument v on v.id = rro.vid_dokument'
      'inner join view_hr_vraboteni hrv on hrv.mb = rro.mb'
      'where rro.id = :id'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 144
    Top = 464
    object tblizjavaProdolzuvanjeROID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblizjavaProdolzuvanjeROMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeROARHIVSKI_BROJ: TFIBStringField
      FieldName = 'ARHIVSKI_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeRODATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblizjavaProdolzuvanjeRODATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object tblizjavaProdolzuvanjeRODATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblizjavaProdolzuvanjeROGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblizjavaProdolzuvanjeROZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeROVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeROBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeRONAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeRONAZIV_VRABOTEN_TI: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeRORABOTNAEDINICANAZIV: TFIBStringField
      FieldName = 'RABOTNAEDINICANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeRORABOTNOMESTONAZIV: TFIBStringField
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblizjavaProdolzuvanjeRODATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxOtsustvaPoGodina: TfrxDBDataset
    UserName = #1054#1090#1089#1091#1089#1090#1074#1086#1055#1086#1043#1086#1076#1080#1085#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'MB=MB'
      'NAZIV_VRABOTEN=NAZIV_VRABOTEN'
      'OTSUSTVO_ID=OTSUSTVO_ID'
      'RESENIE_ID=RESENIE_ID'
      'ID=ID'
      'GODINA=GODINA'
      'GODINA_RESENIE=GODINA_RESENIE'
      'OD_VREME=OD_VREME'
      'DO_VREME=DO_VREME'
      'DENOVI=DENOVI'
      'DENOVI_PO_RESENIE=DENOVI_PO_RESENIE'
      'RABOTNAEDINICANAZIV=RABOTNAEDINICANAZIV')
    DataSet = tblOtsustvaPoGodina
    BCDToCurrency = False
    Left = 256
    Top = 856
  end
  object tblOtsustvaPoGodina: TpFIBDataSet
    SelectSQL.Strings = (
      '                        select'
      '    V.MB,'
      '    V.NAZIV_VRABOTEN,'
      '    O1.id as otsustvo_id,'
      '    o.resenie_id,'
      '    o.id,'
      '    O1.godina,'
      '    r.godina as godina_resenie,'
      '    O1.OD_VREME ,'
      '    O1.do_vreme ,'
      
        '    (select proc_hr_work_days.denovi from proc_hr_work_days(O1.O' +
        'D_VREME,O1.DO_VREME, o1.mb)) denovi,'
      '    o.denovi denovi_po_resenie,'
      '    v.rabotnaedinicanaziv'
      'from (select o.id,'
      '             o.mb,'
      '             o.pricina,'
      '             extract(year from O.OD_VREME) GODINA,'
      
        '             case when (extract(year from o.od_vreme) < extract(' +
        'year from o.do_vreme) )then '#39'31.12.'#39'||extract(year from o.od_vre' +
        'me) else CAST (o.do_vreme AS date)  end DO_VREME,'
      
        '             case when (extract(year from o.od_vreme) < extract(' +
        'year from o.do_vreme)) and ((extract(year from o.od_vreme) = ext' +
        'ract(year from o.do_vreme)))then '#39'01.01.'#39'||extract(year from o.d' +
        'o_vreme) else CAST (o.od_vreme AS date) end OD_VREME'
      
        '      from hr_otsustva o where o.tip_zapis =1 and o.pricina = 5)' +
        ' O1'
      'left join HR_OTSUSTVA_RESENIJA O on O.OTSUSTVO_ID = O1.ID'
      'join HR_RESENIE_GO R  on O.RESENIE_ID = R.ID'
      'join view_hr_vraboteni V on o1.MB = V.MB'
      'where v.mb = '#39'1103986488003'#39)
    AutoUpdateOptions.UpdateTableName = 'HR_DOGOVOR_VOLONTERI'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_DOGOVOR_VOLONTERI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 388
    Top = 856
    object tblOtsustvaPoGodinaMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPoGodinaNAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPoGodinaOTSUSTVO_ID: TFIBIntegerField
      FieldName = 'OTSUSTVO_ID'
    end
    object tblOtsustvaPoGodinaRESENIE_ID: TFIBIntegerField
      FieldName = 'RESENIE_ID'
    end
    object tblOtsustvaPoGodinaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblOtsustvaPoGodinaGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object tblOtsustvaPoGodinaGODINA_RESENIE: TFIBIntegerField
      FieldName = 'GODINA_RESENIE'
    end
    object tblOtsustvaPoGodinaOD_VREME: TFIBStringField
      FieldName = 'OD_VREME'
      Size = 12
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPoGodinaDO_VREME: TFIBStringField
      FieldName = 'DO_VREME'
      Size = 12
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPoGodinaDENOVI: TFIBIntegerField
      FieldName = 'DENOVI'
    end
    object tblOtsustvaPoGodinaDENOVI_PO_RESENIE: TFIBIntegerField
      FieldName = 'DENOVI_PO_RESENIE'
    end
    object tblOtsustvaPoGodinaRABOTNAEDINICANAZIV: TFIBStringField
      FieldName = 'RABOTNAEDINICANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblReseniePreraspredelbaTemplate: TpFIBDataSet
    UpdateSQL.Strings = (
      '  UPDATE HR_RESENIE_PRERASPREDELBA'
      'SET '
      ''
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID')
    SelectSQL.Strings = (
      'select rp.datum_kreiranje,'
      '       rp.broj,'
      '       rp.data,'
      '       hdv.ime,'
      '       hdv.prezime,'
      '       po.opis StepenStrucnaPodgotovka,'
      '       hrmrab_staro.naziv StaroRabMestoNaziv,'
      '       re_staro.naziv StaraRabEdinica,'
      '       hrmrab_novo.naziv NovoRabMestoNaziv,'
      '       re_novo.naziv NovoRabEdinica,'
      ''
      
        '       cast(coalesce(hrmrab_novo.stepen_slozenost,0) as numeric(' +
        '15,4)) as BodStepenSlozenost,'
      ''
      '       (select sum(ku.procent) from hr_rmre_uslovi_rabota us'
      
        '                                left join hr_koeficient_uslovira' +
        'b ku on ku.id = us.uslovi_rabota'
      
        '                            where (rp.novo_rm = us.id_rm_re )) u' +
        'slovi_rabota_procent,'
      
        '       cast((select sum(ku.procent) from hr_rmre_uslovi_rabota u' +
        's'
      
        '                                left join hr_koeficient_uslovira' +
        'b ku on ku.id = us.uslovi_rabota'
      
        '                            where (rp.novo_rm = us.id_rm_re )) *' +
        'coalesce(hrmrab_novo.stepen_slozenost,0)/100 as numeric(15,4)) u' +
        'slovi_rabota,'
      ''
      
        '       cast(coalesce(hrm_novo.rakovodenje,0) as numeric(15,4)) a' +
        's BodRakovodenje_procent,'
      
        '       cast(coalesce(hrmrab_novo.stepen_slozenost,0) * coalesce(' +
        'hrm_novo.rakovodenje,0)/100 as numeric(15,4))  as BodRakovodenje' +
        ','
      ''
      ''
      
        '        case when (hdv.staz_mesec +  datediff(month,hdv.datum_od' +
        ', current_date)) >=12 then div(hdv.staz_mesec +  datediff(month,' +
        'hdv.datum_od, current_date),12) + hdv.staz_godina'
      
        '            when (hdv.staz_mesec +  datediff(month,hdv.datum_od,' +
        ' current_date)) < 12 then 0 + hdv.staz_godina'
      '       end "staz_godina",'
      ''
      
        '       case when (hdv.staz_mesec +  datediff(month,hdv.datum_od,' +
        ' current_date)) >=12 then mod(hdv.staz_mesec +  datediff(month,h' +
        'dv.datum_od, current_date),12)'
      
        '            when (hdv.staz_mesec +  datediff(month,hdv.datum_od,' +
        ' current_date)) < 12 then (hdv.staz_mesec +  datediff(month,hdv.' +
        'datum_od, current_date))'
      '       end "staz_meseci",'
      '       '
      '       0 staz_denovi,'
      ''
      '       (select p.dodatok'
      '        from hr_parametri_kd p'
      
        '        where :param between p.datum_od and coalesce(p.datum_do,' +
        ' current_date)) as ProcentStazKolDog,'
      ''
      '       cast((select p.dodatok'
      '             from hr_parametri_kd p'
      
        '             where :param between p.datum_od and coalesce(p.datu' +
        'm_do, current_date)) *'
      
        '             (case when (hdv.staz_mesec +  datediff(month,hdv.da' +
        'tum_od, current_date)) >=12 then div(hdv.staz_mesec +  datediff(' +
        'month,hdv.datum_od, current_date),12) + hdv.staz_godina'
      
        '                   when (hdv.staz_mesec +  datediff(month,hdv.da' +
        'tum_od, current_date)) < 12 then 0 + hdv.staz_godina end) *'
      
        '              coalesce(hrmrab_novo.stepen_slozenost,0)/100 as nu' +
        'meric(15,4)) as BodoviStaz,'
      '      cast((select p.dodatok'
      '             from hr_parametri_kd p'
      
        '             where :param between p.datum_od and coalesce(p.datu' +
        'm_do, current_date)) *'
      
        '             (case when (hdv.staz_mesec +  datediff(month,hdv.da' +
        'tum_od, current_date)) >=12 then div(hdv.staz_mesec +  datediff(' +
        'month,hdv.datum_od, current_date),12) + hdv.staz_godina'
      
        '                   when (hdv.staz_mesec +  datediff(month,hdv.da' +
        'tum_od, current_date)) < 12 then 0 + hdv.staz_godina end) *'
      
        '              coalesce(hrmrab_novo.stepen_slozenost,0)/100 as nu' +
        'meric(15,4))'
      
        '     + cast((select coalesce(sum(ku.procent),0) from hr_rmre_usl' +
        'ovi_rabota us'
      
        '                                left join hr_koeficient_uslovira' +
        'b ku on ku.id = us.uslovi_rabota'
      
        '                            where (rp.novo_rm = us.id_rm_re )) *' +
        'coalesce(hrmrab_novo.stepen_slozenost,0)/100 as numeric(15,4))'
      
        '     + cast(coalesce(hrmrab_novo.stepen_slozenost,0) * coalesce(' +
        'hrm_novo.rakovodenje,0)/100 as numeric(15,4))'
      
        '     + cast(coalesce(hrmrab_novo.stepen_slozenost,0) as numeric(' +
        '15,4)) as vkupno'
      'from  hr_resenie_preraspredelba rp'
      'inner join hr_dogovor_vrabotuvanje hdv on hdv.mb = rp.mb'
      
        'inner join hr_vraboten_rm hv on hv.mb = hdv.mb and hv.id_re_firm' +
        'a = hdv.id_re_firma and (:param  between hv.datum_od and coalesc' +
        'e(hv.datum_do, :param))'
      
        'inner join plt_obrazovanie po on po.id=hdv.stepen_strucna_podgot' +
        'ovka'
      'inner join hr_rm_re hrm_staro on hrm_staro.id = rp.staro_rm'
      
        'inner join hr_rabotno_mesto hrmrab_staro on hrmrab_staro.id = hr' +
        'm_staro.id_rm'
      'inner join mat_re re_staro on re_staro.id = hrm_staro.id_re'
      
        'inner join hr_sistematizacija hs_staro on hs_staro.id = hrm_star' +
        'o.id_sistematizacija'
      'inner join hr_rm_re hrm_novo on hrm_novo.id = rp.novo_rm'
      
        'inner join hr_rabotno_mesto hrmrab_novo on hrmrab_novo.id = hrm_' +
        'novo.id_rm'
      'inner join mat_re re_novo on re_novo.id = hrm_novo.id_re'
      
        'inner join hr_sistematizacija hs_novo on hs_novo.id = hrm_novo.i' +
        'd_sistematizacija'
      ''
      'where rp.id = :ID')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 456
    Top = 920
    object tblReseniePreraspredelbaTemplateDATUM_KREIRANJE: TFIBDateField
      FieldName = 'DATUM_KREIRANJE'
    end
    object tblReseniePreraspredelbaTemplateIME: TFIBStringField
      FieldName = 'IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplatePREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateSTEPENSTRUCNAPODGOTOVKA: TFIBStringField
      FieldName = 'STEPENSTRUCNAPODGOTOVKA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateSTARORABMESTONAZIV: TFIBStringField
      FieldName = 'STARORABMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateSTARARABEDINICA: TFIBStringField
      FieldName = 'STARARABEDINICA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateNOVORABMESTONAZIV: TFIBStringField
      FieldName = 'NOVORABMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateNOVORABEDINICA: TFIBStringField
      FieldName = 'NOVORABEDINICA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateBODSTEPENSLOZENOST: TFIBBCDField
      FieldName = 'BODSTEPENSLOZENOST'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplateUSLOVI_RABOTA_PROCENT: TFIBBCDField
      FieldName = 'USLOVI_RABOTA_PROCENT'
      Size = 2
    end
    object tblReseniePreraspredelbaTemplateUSLOVI_RABOTA: TFIBBCDField
      FieldName = 'USLOVI_RABOTA'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplateBODRAKOVODENJE_PROCENT: TFIBBCDField
      FieldName = 'BODRAKOVODENJE_PROCENT'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplateBODRAKOVODENJE: TFIBBCDField
      FieldName = 'BODRAKOVODENJE'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplatestaz_godina: TFIBBCDField
      FieldName = 'staz_godina'
      Size = 0
    end
    object tblReseniePreraspredelbaTemplatestaz_meseci: TFIBBCDField
      FieldName = 'staz_meseci'
      Size = 0
    end
    object tblReseniePreraspredelbaTemplatePROCENTSTAZKOLDOG: TFIBBCDField
      FieldName = 'PROCENTSTAZKOLDOG'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplateBODOVISTAZ: TFIBBCDField
      FieldName = 'BODOVISTAZ'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplateSTAZ_DENOVI: TFIBIntegerField
      FieldName = 'STAZ_DENOVI'
    end
    object tblReseniePreraspredelbaTemplateBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReseniePreraspredelbaTemplateVKUPNO: TFIBBCDField
      FieldName = 'VKUPNO'
      Size = 8
    end
    object tblReseniePreraspredelbaTemplateDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxReseniePreraspredelbaTemplate: TfrxDBDataset
    UserName = #1056#1077#1096#1077#1085#1080#1077#1047#1072#1055#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072#1058#1077#1084#1087#1083#1077#1112#1090
    CloseDataSource = False
    FieldAliases.Strings = (
      'DATUM_KREIRANJE=DATUM_KREIRANJE'
      'IME=IME'
      'PREZIME=PREZIME'
      'STEPENSTRUCNAPODGOTOVKA=STEPENSTRUCNAPODGOTOVKA'
      'STARORABMESTONAZIV=STARORABMESTONAZIV'
      'STARARABEDINICA=STARARABEDINICA'
      'NOVORABMESTONAZIV=NOVORABMESTONAZIV'
      'NOVORABEDINICA=NOVORABEDINICA'
      'BODSTEPENSLOZENOST=BODSTEPENSLOZENOST'
      'USLOVI_RABOTA_PROCENT=USLOVI_RABOTA_PROCENT'
      'USLOVI_RABOTA=USLOVI_RABOTA'
      'BODRAKOVODENJE_PROCENT=BODRAKOVODENJE_PROCENT'
      'BODRAKOVODENJE=BODRAKOVODENJE'
      'staz_godina=staz_godina'
      'staz_meseci=staz_meseci'
      'PROCENTSTAZKOLDOG=PROCENTSTAZKOLDOG'
      'BODOVISTAZ=BODOVISTAZ'
      'STAZ_DENOVI=STAZ_DENOVI'
      'BROJ=BROJ'
      'VKUPNO=VKUPNO')
    DataSet = tblReseniePreraspredelbaTemplate
    BCDToCurrency = False
    Left = 248
    Top = 920
  end
end
