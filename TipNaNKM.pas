unit TipNaNKM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxGroupBox, cxRadioGroup, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxScreenTip;

type
//  niza = Array[1..5] of Variant;

  TfrmTipNKM = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dPanel: TPanel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    Label2: TLabel;
    NAZIV: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    lPanel: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1FLAG: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1NAZIV: TcxGridDBColumn;
    cxGridDBTableView1FLAG: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxTabSheet3: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBTableView2ID: TcxGridDBColumn;
    cxGridDBTableView2NAZIV: TcxGridDBColumn;
    cxGridDBTableView2FLAG: TcxGridDBColumn;
    cxGridDBTableView2TS_INS: TcxGridDBColumn;
    cxGridDBTableView2TS_UPD: TcxGridDBColumn;
    cxGridDBTableView2USR_INS: TcxGridDBColumn;
    cxGridDBTableView2USR_UPD: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxComponentPrinter1Link3: TdxGridReportLink;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmTipNKM: TfrmTipNKM;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmTipNKM.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmTipNKM.aNovExecute(Sender: TObject);
begin
  if(dm.tblTipNKM.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if cxPageControl1.ActivePage = cxTabSheet1 then
      begin
       cxGrid1DBTableView1.DataController.DataSet.Insert;
       dm.tblTipNKMFLAG.Value:=1;
      end;
    if cxPageControl1.ActivePage = cxTabSheet2 then
      begin
       cxGridDBTableView1.DataController.DataSet.Insert;
       dm.tblTipNKMFLAG.Value:=2;
      end;
    if cxPageControl1.ActivePage = cxTabSheet3 then
      begin
       cxGridDBTableView2.DataController.DataSet.Insert;
       dm.tblTipNKMFLAG.Value:=3;
      end;
    NAZIV.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmTipNKM.aAzurirajExecute(Sender: TObject);
begin
  if(dm.tblTipNKM.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if cxPageControl1.ActivePage = cxTabSheet1 then
      begin
       cxGrid1DBTableView1.DataController.DataSet.Edit;
       dm.tblTipNKMFLAG.Value:=1;
      end;
    if cxPageControl1.ActivePage = cxTabSheet2 then
      begin
       cxGridDBTableView1.DataController.DataSet.Edit;
       dm.tblTipNKMFLAG.Value:=2;
      end;
    if cxPageControl1.ActivePage = cxTabSheet3 then
      begin
       cxGridDBTableView2.DataController.DataSet.Edit;
       dm.tblTipNKMFLAG.Value:=3;
      end;
    NAZIV.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmTipNKM.aBrisiExecute(Sender: TObject);
begin
  if(dm.tblTipNKM.State = dsBrowse) then
   begin
    if cxPageControl1.ActivePage = cxTabSheet1 then
       cxGrid1DBTableView1.DataController.DataSet.Delete();
    if cxPageControl1.ActivePage = cxTabSheet2 then
       cxGridDBTableView1.DataController.DataSet.Delete();
    if cxPageControl1.ActivePage = cxTabSheet3 then
       cxGridDBTableView2.DataController.DataSet.Delete();
   end;
end;

//	����� �� ���������� �� ����������
procedure TfrmTipNKM.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmTipNKM.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmTipNKM.aSnimiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  if cxPageControl1.ActivePage = cxTabSheet2 then
     zacuvajGridVoBaza(Name,cxGridDBTableView1);
  if cxPageControl1.ActivePage = cxTabSheet3 then
     zacuvajGridVoBaza(Name,cxGridDBTableView2);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmTipNKM.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajVoExcel(cxGrid1, Caption);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajVoExcel(cxGrid2, Caption);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        zacuvajVoExcel(cxGrid3, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmTipNKM.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmTipNKM.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmTipNKM.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmTipNKM.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmTipNKM.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmTipNKM.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmTipNKM.prefrli;
begin
end;

procedure TfrmTipNKM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (dm.tblTipNKM.State = dsEdit) or (dm.tblTipNKM.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            dm.tblTipNKM.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            dm.tblTipNKM.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmTipNKM.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmTipNKM.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
    procitajGridOdBaza(Name,cxGridDBTableView2,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
    procitajPrintOdBaza(Name,cxGridDBTableView2.Name, dxComponentPrinter1Link3);

    dm.tblTipNKM.close;
    dm.tblTipNKM.ParamByName('flag').Value:= 1;
    dm.tblTipNKM.open;

end;
//------------------------------------------------------------------------------

procedure TfrmTipNKM.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmTipNKM.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmTipNKM.cxPageControl1Change(Sender: TObject);
begin
      if cxPageControl1.ActivePage = cxTabSheet1 then
         begin
           dm.tblTipNKM.ParamByName('flag').Value:= 1;
           dm.tblTipNKM.FullRefresh;
         end;
      if cxPageControl1.ActivePage = cxTabSheet2 then
         begin
           dm.tblTipNKM.ParamByName('flag').Value:= 2;
           dm.tblTipNKM.FullRefresh;
         end;
      if cxPageControl1.ActivePage = cxTabSheet3 then
         begin
           dm.tblTipNKM.ParamByName('flag').Value:= 3;
           dm.tblTipNKM.FullRefresh;
         end;
end;

//  ����� �� �����
procedure TfrmTipNKM.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := dm.tblTipNKM.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        dm.tblTipNKM.Post;
//        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxPageControl1.ActivePage.SetFocus;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmTipNKM.aOtkaziExecute(Sender: TObject);
begin
  if (dm.tblTipNKM.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      dm.tblTipNKM.Cancel;
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxPageControl1.ActivePage.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmTipNKM.aPageSetupExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1Link1.PageSetup;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        dxComponentPrinter1Link2.PageSetup;
     if cxPageControl1.ActivePage = cxTabSheet3 then
        dxComponentPrinter1Link3.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmTipNKM.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link3);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmTipNKM.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
      if cxPageControl1.ActivePage = cxTabSheet1 then
          dxComponentPrinter1Link1.DesignReport();
      if cxPageControl1.ActivePage = cxTabSheet2 then
          dxComponentPrinter1Link2.DesignReport();
      if cxPageControl1.ActivePage = cxTabSheet3 then
          dxComponentPrinter1Link3.DesignReport();
end;

procedure TfrmTipNKM.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link2);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        zacuvajPrintVoBaza(Name,cxGridDBTableView2.Name,dxComponentPrinter1Link3);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmTipNKM.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        brisiPrintOdBaza(Name,cxGridDBTableView2.Name, dxComponentPrinter1Link3);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmTipNKM.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmTipNKM.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmTipNKM.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
