unit PrijavaOdjava;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, FIBDataSet, pFIBDataSet, cxGroupBox, cxRadioGroup,
  cxDropDownEdit, cxCalendar, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, FIBQuery, pFIBQuery, dxRibbonSkins, cxPCdxBarPopupMenu,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk,
  dxPScxPivotGridLnk, dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinOffice2013White,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, cxNavigator, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmPrijavaOdjava = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    dPanel: TPanel;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1PROBNA: TcxGridDBColumn;
    cxGrid1DBTableView1OSNOV: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1INSTITUCIJA: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODNO: TcxGridDBColumn;
    cxGrid1DBTableView1RakovodnoNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1ProbnoNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1OSNOVNAZIV: TcxGridDBColumn;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    Label8: TLabel;
    MB: TcxDBTextEdit;
    Lica: TcxDBLookupComboBox;
    Label6: TLabel;
    Datum: TcxDBDateEdit;
    Label2: TLabel;
    RM: TcxDBTextEdit;
    RM_NAZIV: TcxDBLookupComboBox;
    cxDBRadioGroup1: TcxDBRadioGroup;
    Label4: TLabel;
    OsnovNaziv: TcxDBLookupComboBox;
    Label5: TLabel;
    staz: TcxDBTextEdit;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    tblRMRERAKOVODENJE: TFIBBCDField;
    tblRMREDENOVI_ODMOR: TFIBIntegerField;
    tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField;
    tblRMRESTEPEN_SLOZENOST: TFIBBCDField;
    dsRMRE: TDataSource;
    Label3: TLabel;
    cxDBCheckBox1: TcxDBCheckBox;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    Label7: TLabel;
    MB_lice: TcxDBTextEdit;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    Label9: TLabel;
    cxDBTextEdit2: TcxDBTextEdit;
    Label10: TLabel;
    datum_odjava: TcxDBDateEdit;
    cxDBCheckBox2: TcxDBCheckBox;
    Label12: TLabel;
    OsnovOdjava: TcxDBTextEdit;
    cxDBLookupComboBox3: TcxDBLookupComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    tblOsnovNaOsiguruvanje: TpFIBDataSet;
    tblOsnovNaOsiguruvanjeID: TFIBIntegerField;
    tblOsnovNaOsiguruvanjeBROJ: TFIBIntegerField;
    tblOsnovNaOsiguruvanjeNAZIV: TFIBStringField;
    tblOsnovNaOsiguruvanjeTS_INS: TFIBDateTimeField;
    tblOsnovNaOsiguruvanjeTS_UPD: TFIBDateTimeField;
    tblOsnovNaOsiguruvanjeUSR_INS: TFIBStringField;
    tblOsnovNaOsiguruvanjeUSR_UPD: TFIBStringField;
    tblOsnovNaOsiguruvanjeTIP_OSNOV: TFIBSmallIntField;
    dsOsnovNaOsiguruvanje: TDataSource;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1TIP: TcxGridDBColumn;
    cxGrid2DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid2DBTableView1MB: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1PROBNA: TcxGridDBColumn;
    cxGrid2DBTableView1OSNOV: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1INSTITUCIJA: TcxGridDBColumn;
    cxGrid2DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid2DBTableView1IME: TcxGridDBColumn;
    cxGrid2DBTableView1ProbnoNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1OSNOVNAZIV: TcxGridDBColumn;
    dxComponentPrinter1Link2: TdxGridReportLink;
    aKreirajOdjava: TAction;
    Panel3: TPanel;
    cxButton3: TcxButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aJS_2: TAction;
    aDizajnReport: TAction;
    qBrOdjavi: TpFIBQuery;
    qStaroMesto: TpFIBQuery;
    qStazPrijava: TpFIBQuery;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aKreirajOdjavaExecute(Sender: TObject);
    procedure aJS_2Execute(Sender: TObject);
    procedure aDizajnReportExecute(Sender: TObject);
    procedure OsnovNazivPropertiesChange(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmPrijavaOdjava: TfrmPrijavaOdjava;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmMaticni,
  dmUnitOtsustvo, dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmPrijavaOdjava.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmPrijavaOdjava.aNovExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        begin
          if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
              dPanel.Enabled:=True;
              lPanel.Enabled:=False;
              Panel3.Enabled:=false;
              MB.SetFocus;
              cxGrid1DBTableView1.DataController.DataSet.Insert;
              dm.tblPrijavaVID_DOKUMENT.Value:=prijava;
              dm.tblPrijavaTIP.Value:=0;
            end
          else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
        end;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        begin
          if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
             qBrOdjavi.Close;
             qBrOdjavi.ParamByName('mb').Value:=dm.tblPrijavaMB.Value;
             qBrOdjavi.ParamByName('prijava').Value:=dm.tblPrijavaID.Value;
             qBrOdjavi.ExecQuery;
             if qBrOdjavi.FldByName['broj'].Value = 0 then
                begin
                  Panel2.Enabled:=True;
                  Panel1.Enabled:=False;
                  cxGrid2DBTableView1.DataController.DataSet.Insert;
                  dm.tblOdjavaVID_DOKUMENT.Value:=odjava;
                  dm.tblOdjavaTIP.Value:=1;
                  dm.tblOdjavaPO_PRIJAVA_ID.value:= dm.tblPrijavaID.Value;
                  dm.tblOdjavaMB.Value:=dm.tblPrijavaMB.Value;
                  dm.tblOdjavaPROBNA.Value:=dm.tblPrijavaPROBNA.Value;
                  datum_odjava.Date:=Now;
                  OsnovOdjava.SetFocus;
                end
             else
                ShowMessage('����� ������������ ����� �� ������������� ������ !!!');
            end
          else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
        end;
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPrijavaOdjava.aAzurirajExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
          begin
            dPanel.Enabled:=True;
            lPanel.Enabled:=False;
            MB.SetFocus;
            cxGrid1DBTableView1.DataController.DataSet.Edit;
          end
        else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
      end;
  if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
          begin
            Panel2.Enabled:=True;
            Panel1.Enabled:=False;
            MB_lice.SetFocus;
            cxGrid2DBTableView1.DataController.DataSet.Edit;
          end
        else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
      end;
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPrijavaOdjava.aBrisiExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
       if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
         cxGrid1DBTableView1.DataController.DataSet.Delete();
     end;
  if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
       if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
         cxGrid2DBTableView1.DataController.DataSet.Delete();
     end;
end;

//	����� �� ���������� �� ����������
procedure TfrmPrijavaOdjava.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPrijavaOdjava.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmPrijavaOdjava.aJS_2Execute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30301;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;



     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;

end;

procedure TfrmPrijavaOdjava.aKreirajOdjavaExecute(Sender: TObject);
begin
     cxPageControl1.ActivePage:=cxTabSheet2;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        begin
          if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
            qBrOdjavi.Close;
             qBrOdjavi.ParamByName('mb').Value:=dm.tblPrijavaMB.Value;
             qBrOdjavi.ParamByName('prijava').Value:=dm.tblPrijavaID.Value;
             qBrOdjavi.ExecQuery;
             if qBrOdjavi.FldByName['broj'].Value = 0 then
                begin
                  Panel2.Enabled:=True;
                  Panel1.Enabled:=False;
                  cxGrid2DBTableView1.DataController.DataSet.Insert;
                  dm.tblOdjavaVID_DOKUMENT.Value:=odjava;
                  dm.tblOdjavaTIP.Value:=1;
                  dm.tblOdjavaPO_PRIJAVA_ID.value:= dm.tblPrijavaID.Value;
                  dm.tblOdjavaMB.Value:=dm.tblPrijavaMB.Value;
                  dm.tblOdjavaPROBNA.Value:=dm.tblPrijavaPROBNA.Value;
                  datum_odjava.Date:=Now;
                  OsnovOdjava.SetFocus;
                end
             else
                ShowMessage('����� ������������ ����� �� ������������� ������ !!!');
            end
          else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
        end;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPrijavaOdjava.aSnimiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1)
  else
     zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPrijavaOdjava.aZacuvajExcelExecute(Sender: TObject);
begin
   if cxPageControl1.ActivePage = cxTabSheet1 then
     zacuvajVoExcel(cxGrid1, '��������� �� ������')
   else
     zacuvajVoExcel(cxGrid2, '��������� �� �����')
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPrijavaOdjava.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPrijavaOdjava.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPrijavaOdjava.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (Sender = MB) or (Sender = Lica)then
     begin
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
        begin
          if (Lica.Text<> '')then
             begin
                qStaroMesto.Close;
                qStaroMesto.ParamByName('mb').Value:=mb.Text;
                qStaroMesto.ParamByName('firma').Value:=dmKon.re;
                qStaroMesto.ExecQuery;
                dm.tblPrijavaID_RM_RE.Value:= qStaroMesto.FldByName['id_rm_re'].Value;
             end;
          if (lica.Text <> '') and (Datum.Text <> '')  then
             begin
               qStazPrijava.close;
               qStazPrijava.ParamByName('mb').Value:=mb.Text;
               qStazPrijava.ParamByName('firma').Value:=dmKon.re;
               qStazPrijava.ParamByName('datumprijava').Value:=Datum.EditValue;
               qStazPrijava.ExecQuery;
               dm.tblPrijavaSTAZ.Value:=qStazPrijava.FldByName['stazPrijava'].Value;
             end;
        end;
     end;
    if sender = Datum then
       begin
          if (lica.Text <> '') and (Datum.Text <> '')  then
             begin
               qStazPrijava.close;
               qStazPrijava.ParamByName('mb').Value:=mb.Text;
               qStazPrijava.ParamByName('firma').Value:=dmKon.re;
               qStazPrijava.ParamByName('datumprijava').Value:=Datum.EditValue;
               qStazPrijava.ExecQuery;
               dm.tblPrijavaSTAZ.Value:=qStazPrijava.FldByName['stazPrijava'].Value;
             end;
       end;
end;

procedure TfrmPrijavaOdjava.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPrijavaOdjava.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmPrijavaOdjava.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmPrijavaOdjava.OsnovNazivPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPrijavaOdjava.prefrli;
begin
end;

procedure TfrmPrijavaOdjava.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmPrijavaOdjava.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dm.tblLica.Close;
  dm.tblLica.ParamByName('mb').Value:='%';
  dm.tblLica.Open;

  tblRMRE.ParamByName('firma').Value:=dmKon.re;
  tblRMRE.Open;

  dm.tblOsnovNaOsiguruvanje.Close;
  dm.tblOsnovNaOsiguruvanje.ParamByName('tip').Value:=1;
  dm.tblOsnovNaOsiguruvanje.Open;

  tblOsnovNaOsiguruvanje.Close;
  tblOsnovNaOsiguruvanje.ParamByName('tip').Value:=2;
  tblOsnovNaOsiguruvanje.Open;

  dm.tblPrijava.Close;
  dm.tblPrijava.ParamByName('firma').Value:=firma;
  dm.tblPrijava.open;

  dm.tblOdjava.Close;
  dm.tblOdjava.ParamByName('firma').Value:=firma;
  dm.tblOdjava.open;

end;

//------------------------------------------------------------------------------

procedure TfrmPrijavaOdjava.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
  //  PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := '��������� �� ������';
    dxComponentPrinter1Link2.ReportTitle.Text := '��������� �� �����';

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;
//------------------------------------------------------------------------------

procedure TfrmPrijavaOdjava.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPrijavaOdjava.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPrijavaOdjava.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

//  ����� �� �����
procedure TfrmPrijavaOdjava.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
        st := cxGrid1DBTableView1.DataController.DataSet.State;
        if st in [dsEdit,dsInsert] then
          begin
            if (Validacija(dPanel) = false) then
              begin
                cxGrid1DBTableView1.DataController.DataSet.Post;
                dPanel.Enabled:=false;
                lPanel.Enabled:=true;
                Panel3.Enabled:=true;
                cxGrid1.SetFocus;
              end;
          end;
     end;
  if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
        st := cxGrid2DBTableView1.DataController.DataSet.State;
        if st in [dsEdit,dsInsert] then
          begin
            if (Validacija(Panel2) = false) then
              begin
                cxGrid2DBTableView1.DataController.DataSet.Post;
                Panel2.Enabled:=false;
                Panel1.Enabled:=true;
                cxGrid2.SetFocus;
              end;
          end;
     end;
end;

//	����� �� ���������� �� �������
procedure TfrmPrijavaOdjava.aOtkaziExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
        if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
          begin
            ModalResult := mrCancel;
            Close();
          end
        else
          begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            RestoreControls(dPanel);
            dPanel.Enabled := false;
            lPanel.Enabled := true;
            Panel3.Enabled := true;
            cxGrid1.SetFocus;
          end;
     end;
  if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
        if (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
          begin
            ModalResult := mrCancel;
            Close();
          end
        else
          begin
            cxGrid2DBTableView1.DataController.DataSet.Cancel;
            RestoreControls(Panel2);
            Panel2.Enabled := false;
            Panel1.Enabled := true;
            cxGrid2.SetFocus;
          end;
     end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPrijavaOdjava.aPageSetupExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1Link1.PageSetup
     else
        dxComponentPrinter1Link2.PageSetup
end;

//	����� �� ������� �� ������
procedure TfrmPrijavaOdjava.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1)
     else
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2)
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPrijavaOdjava.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1Link1.DesignReport()
     else
        dxComponentPrinter1Link2.DesignReport()
end;

procedure TfrmPrijavaOdjava.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1)
     else
       zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2)
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPrijavaOdjava.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1)
     else
        brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2)
end;

procedure TfrmPrijavaOdjava.aDizajnReportExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30301;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;



     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
//     dmReport.frxReport1.ShowReport;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPrijavaOdjava.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPrijavaOdjava.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPrijavaOdjava.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
