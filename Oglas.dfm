inherited frmOglas: TfrmOglas
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1086#1075#1083#1072#1089#1080
  ClientHeight = 717
  ClientWidth = 996
  ExplicitWidth = 1012
  ExplicitHeight = 756
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 996
    Height = 242
    ExplicitWidth = 996
    ExplicitHeight = 242
    inherited cxGrid1: TcxGrid
      Width = 992
      Height = 238
      PopupMenu = PopupMenu1
      ExplicitWidth = 992
      ExplicitHeight = 238
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFilterCustomization = nil
        DataController.DataSource = dmSis.dsOglas
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1STATUS1: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089' ('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'STATUS'
          Visible = False
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'statusNaziv'
          Width = 76
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
        end
        object cxGrid1DBTableView1BROJ1: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJGODINA'
          Width = 81
        end
        object cxGrid1DBTableView1DATUM_OBJAVEN: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OBJAVEN'
          Width = 118
        end
        object cxGrid1DBTableView1PRIJAVUVANJE_DO: TcxGridDBColumn
          DataBinding.FieldName = 'PRIJAVUVANJE_DO'
          Width = 92
        end
        object cxGrid1DBTableView1DATUM_ISTEKUVA: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_ISTEKUVA'
          Width = 127
        end
        object cxGrid1DBTableView1OBJAVEN: TcxGridDBColumn
          DataBinding.FieldName = 'OBJAVEN'
          Width = 104
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 250
        end
        object cxGrid1DBTableView1PROMENET_OD: TcxGridDBColumn
          DataBinding.FieldName = 'PROMENET_OD'
          Width = 286
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 368
    Width = 996
    Height = 326
    ExplicitTop = 368
    ExplicitWidth = 996
    ExplicitHeight = 326
    inherited Label1: TLabel
      Left = 381
      Visible = False
      ExplicitLeft = 381
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 30
      Width = 88
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 13
      Top = 52
      Width = 88
      Height = 34
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1073#1112#1072#1074#1091#1074#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel [3]
      Left = 272
      Top = 60
      Width = 109
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077' '#1076#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 544
      Top = 60
      Width = 143
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1090#1077#1082#1091#1074#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [5]
      Left = 58
      Top = 114
      Width = 43
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [6]
      Left = 13
      Top = 88
      Width = 88
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1073#1112#1072#1074#1077#1085' '#1074#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label8: TLabel [7]
      Left = 13
      Top = 266
      Width = 88
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1088#1086#1084#1077#1085#1077#1090' '#1086#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label9: TLabel [8]
      Left = 172
      Top = 30
      Width = 4
      Height = 13
      Caption = '/'
    end
    inherited Sifra: TcxDBTextEdit
      Left = 458
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsOglas
      TabOrder = 6
      Visible = False
      ExplicitLeft = 458
    end
    inherited OtkaziButton: TcxButton
      Left = 905
      Top = 286
      TabOrder = 9
      ExplicitLeft = 905
      ExplicitTop = 286
    end
    inherited ZapisiButton: TcxButton
      Left = 824
      Top = 286
      TabOrder = 8
      ExplicitLeft = 824
      ExplicitTop = 286
    end
    object txtBroj: TcxDBTextEdit
      Tag = 1
      Left = 107
      Top = 27
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dmSis.dsOglas
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object txtDatumObj: TcxDBDateEdit
      Tag = 1
      Left = 107
      Top = 57
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1086#1112' '#1077' '#1086#1073#1112#1072#1074#1077#1085' '#1086#1075#1083#1072#1089#1086#1090
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM_OBJAVEN'
      DataBinding.DataSource = dmSis.dsOglas
      Style.Color = clWhite
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 142
    end
    object txtPrijavuvanjeDo: TcxDBDateEdit
      Left = 387
      Top = 57
      Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1112' '#1084#1086#1078#1077' '#1076#1072' '#1089#1077' '#1087#1088#1080#1084#1072#1072#1090' '#1087#1088#1080#1112#1072#1074#1080' '#1089#1087#1086#1088#1077#1076' '#1086#1074#1086#1112' '#1086#1075#1083#1072#1089' '
      BeepOnEnter = False
      DataBinding.DataField = 'PRIJAVUVANJE_DO'
      DataBinding.DataSource = dmSis.dsOglas
      Style.Color = clWhite
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 134
    end
    object txtDatumIstekuva: TcxDBDateEdit
      Left = 693
      Top = 57
      Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1075#1072' '#1074#1072#1078#1080' '#1086#1074#1086#1112' '#1086#1075#1083#1072#1089
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM_ISTEKUVA'
      DataBinding.DataSource = dmSis.dsOglas
      Style.Color = clWhite
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object txtOpis: TcxDBMemo
      Left = 107
      Top = 111
      Hint = 
        #1054#1087#1080#1089' '#1085#1072' '#1086#1075#1083#1072#1089#1086#1090' ('#1094#1077#1083#1086#1089#1077#1085' '#1086#1075#1083#1072#1089') . '#1053#1072' '#1076#1074#1086#1077#1085' '#1082#1083#1080#1082', '#1076#1072' '#1089#1077' '#1091#1088#1077#1076#1080' '#1090#1077#1082 +
        #1089#1090#1086#1090'!'
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmSis.dsOglas
      Properties.WantReturns = False
      TabOrder = 5
      OnDblClick = txtOpisDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 146
      Width = 707
    end
    object txtObjaven: TcxDBTextEdit
      Left = 107
      Top = 84
      Hint = 
        #1050#1072#1076#1077' '#1077' '#1086#1073#1112#1072#1074#1077#1085' '#1086#1075#1083#1072#1089#1086#1090' ('#1089#1083#1091#1078#1073#1077#1085' '#1074#1077#1089#1085#1080#1082', '#1086#1075#1083#1072#1089#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072'.' +
        '..)'
      BeepOnEnter = False
      DataBinding.DataField = 'OBJAVEN'
      DataBinding.DataSource = dmSis.dsOglas
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 707
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 642
      Top = 6
      Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1086#1075#1083#1072#1089#1086#1090
      TabStop = False
      Caption = ' '#1057#1090#1072#1090#1091#1089' '#1085#1072' '#1054#1075#1083#1072#1089
      DataBinding.DataField = 'STATUS'
      DataBinding.DataSource = dmSis.dsOglas
      ParentBackground = False
      Properties.Columns = 2
      Properties.DefaultValue = 1
      Properties.Items = <
        item
          Caption = #1040#1082#1090#1080#1074#1077#1085
          Value = 1
        end
        item
          Caption = #1053#1077' '#1072#1082#1090#1080#1074#1077#1085
          Value = 0
        end>
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.TextColor = clRed
      Style.TextStyle = []
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 10
      Height = 41
      Width = 172
    end
    object txtPromenetOd: TcxDBTextEdit
      Left = 107
      Top = 263
      Hint = 
        #1051#1080#1094#1077#1090#1086' '#1082#1086#1077' '#1112#1072' '#1085#1072#1087#1088#1072#1074#1080#1083#1086' '#1087#1088#1086#1084#1077#1085#1090#1072', '#1072#1082#1086' '#1080#1084#1072' '#1085#1077#1082#1086#1112#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1074#1086' '#1086#1075#1083#1072 +
        #1089#1086#1090' '#1080#1083#1080' '#1087#1072#1082' '#1089#1077' '#1089#1090#1086#1088#1085#1080#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'PROMENET_OD'
      DataBinding.DataSource = dmSis.dsOglas
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 707
    end
    object cxDBLabel1: TcxDBLabel
      Left = 176
      Top = 26
      DataBinding.DataField = 'GODINA'
      DataBinding.DataSource = dmSis.dsOglas
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 21
      Width = 33
      AnchorY = 37
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 996
    ExplicitWidth = 996
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 694
    Width = 996
    ExplicitTop = 694
    ExplicitWidth = 996
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 856
    Top = 16
  end
  inherited PopupMenu1: TPopupMenu
    Left = 856
    Top = 48
    object N2: TMenuItem [0]
      Action = aRMOglas
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 920
    Top = 16
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1054#1075#1083#1072#1089
      DockedLeft = 125
      FloatClientWidth = 136
      FloatClientHeight = 156
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 461
      FloatClientWidth = 111
      FloatClientHeight = 178
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 886
      FloatClientHeight = 104
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      CaptionButtons = <>
      DockedLeft = 397
      DockedTop = 0
      FloatLeft = 853
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      CaptionButtons = <>
      DockedLeft = 736
      DockedTop = 0
      FloatLeft = 853
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 861
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 68
          Visible = True
          ItemName = 'cGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPromeniStatus
      Category = 0
      LargeImageIndex = 14
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aRMOglas
      Category = 0
      LargeImageIndex = 16
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPregledNaMolbi
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1084#1086#1083#1073#1080' '#1079#1072
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton1: TdxBarButton
      Action = aPregledNaMolbi
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = PregledNaMolbiZaOdbranOglas
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aMolbi
      Category = 0
      LargeImageIndex = 40
    end
    object dxBarButton3: TdxBarButton
      Action = aPregledMolbiStatus
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aOglas
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aOglas
      Category = 0
      LargeImageIndex = 19
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1084#1086#1083#1073#1080' '#1079#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end>
    end
    object dxBarButton5: TdxBarButton
      Action = aPregledNaMolbi
      Category = 0
      ImageIndex = 19
    end
    object dxBarButton6: TdxBarButton
      Action = aPregledMolbiStatus
      Category = 0
      ImageIndex = 19
    end
    object dxBarButton7: TdxBarButton
      Action = PregledNaMolbiZaOdbranOglas
      Category = 0
      ImageIndex = 19
    end
    object cGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cGodinaChange
      Items.Strings = (
        #1057#1080#1090#1077
        '2010'
        '2011'
        '2012'
        '2013'
        '2014')
      ItemIndex = -1
    end
  end
  inherited ActionList1: TActionList
    Left = 824
    Top = 16
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aRMOglas: TAction [9]
      Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' '#1087#1086' '#1086#1075#1083#1072#1089
      OnExecute = aRMOglasExecute
    end
    object aPromeniStatus: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1089#1090#1072#1090#1091#1089
      OnExecute = aPromeniStatusExecute
    end
    object aPregledNaMolbi: TAction
      Caption = #1057#1080#1090#1077' '#1086#1075#1083#1072#1089#1080
      OnExecute = aPregledNaMolbiExecute
    end
    object PregledNaMolbiZaOdbranOglas: TAction
      Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1086#1075#1083#1072#1089
      OnExecute = PregledNaMolbiZaOdbranOglasExecute
    end
    object aMolbi: TAction
      Caption = #1052#1086#1083#1073#1080
      OnExecute = aMolbiExecute
    end
    object aPregledMolbiStatus: TAction
      Caption = #1057#1080#1090#1077' '#1072#1082#1090#1080#1074#1085#1080' '#1086#1075#1083#1072#1089#1080
      OnExecute = aPregledMolbiStatusExecute
    end
    object aOglas: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1086#1075#1083#1072#1089
      OnExecute = aOglasExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 824
    Top = 48
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40274.604637766210000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 952
    Top = 16
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 888
    Top = 16
  end
end
