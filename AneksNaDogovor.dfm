inherited frmAneksNaDogovor: TfrmAneksNaDogovor
  Caption = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
  ClientHeight = 741
  ClientWidth = 1064
  Constraints.MinWidth = 605
  WindowState = wsMaximized
  ExplicitWidth = 1080
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1064
    Height = 193
    ExplicitWidth = 1064
    ExplicitHeight = 193
    inherited cxGrid1: TcxGrid
      Width = 1060
      Height = 189
      ExplicitWidth = 1060
      ExplicitHeight = 189
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsAneks
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
        OptionsView.Footer = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1ID_DOGOVOR_VRABOUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_DOGOVOR_VRABOUVANJE'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1BROJDOGOVOR: TcxGridDBColumn
          DataBinding.FieldName = 'BROJDOGOVOR'
          Width = 104
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 79
        end
        object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 138
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'ARHIVSKI_BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 87
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 100
        end
        object cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'VID_VRABOTUVANJE'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1VIDVRABOTUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'VIDVRABOTUVANJE'
          Width = 100
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 300
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1BROJ_CASOVI: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_CASOVI'
        end
        object cxGrid1DBTableView1RAKOVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'RAKOVODITEL'
          Width = 100
        end
        object cxGrid1DBTableView1KOEFICIENT: TcxGridDBColumn
          DataBinding.FieldName = 'KOEFICIENT'
          Width = 100
        end
        object cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn
          DataBinding.FieldName = 'RAKOVODENJE'
          Width = 100
        end
        object cxGrid1DBTableView1USLOVIRABID: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVIRABID'
          VisibleForCustomization = False
          Width = 100
        end
        object cxGrid1DBTableView1USLOVIRABNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVIRABNAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1USLOVIRABPROCENT: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVIRABPROCENT'
          Width = 100
        end
        object cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'DEN_USLOVI_RABOTA'
          Width = 100
        end
        object cxGrid1DBTableView1BENEFICIRAN_STAZ: TcxGridDBColumn
          DataBinding.FieldName = 'BENEFICIRAN_STAZ'
          Width = 122
        end
        object cxGrid1DBTableView1PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'PLATA'
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_POCETOK: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_POCETOK'
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 100
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA'
          Visible = False
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 319
    Width = 1064
    Height = 399
    ExplicitTop = 319
    ExplicitWidth = 1064
    ExplicitHeight = 399
    DesignSize = (
      1064
      399)
    inherited Label1: TLabel
      Left = 437
      Top = 38
      Visible = False
      ExplicitLeft = 437
      ExplicitTop = 38
    end
    object Label2: TLabel [1]
      Left = 15
      Top = 22
      Width = 77
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 236
      Top = 22
      Width = 188
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1085#1072' '#1072#1085#1077#1082#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [3]
      Left = 733
      Top = 72
      Width = 183
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 509
      Top = 30
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsAneks
      TabOrder = 2
      Visible = False
      ExplicitLeft = 509
      ExplicitTop = 30
    end
    inherited OtkaziButton: TcxButton
      Left = 922
      Top = 341
      TabOrder = 8
      ExplicitLeft = 922
      ExplicitTop = 341
    end
    inherited ZapisiButton: TcxButton
      Left = 841
      Top = 341
      TabOrder = 7
      ExplicitLeft = 841
      ExplicitTop = 341
    end
    object Broj: TcxDBTextEdit
      Tag = 1
      Left = 98
      Top = 19
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1072#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsAneks
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 132
    end
    object DATUM: TcxDBDateEdit
      Tag = 1
      Left = 427
      Top = 19
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1085#1072' '#1072#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsAneks
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 132
    end
    object PeriodNaVazenje: TcxGroupBox
      Left = 21
      Top = 303
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1072#1085#1077#1082#1089' '
      TabOrder = 4
      Height = 63
      Width = 570
      object Label5: TLabel
        Left = 357
        Top = 26
        Width = 29
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 16
        Top = 26
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DO_VREME: TcxDBDateEdit
        Left = 392
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsAneks
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikRegExpr
        ShowHint = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 146
      end
      object OD_VREME: TcxDBDateEdit
        Tag = 1
        Left = 72
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_POCETOK'
        DataBinding.DataSource = dm.dsAneks
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikRegExpr
        ShowHint = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 146
      end
    end
    object Plata: TcxGroupBox
      Left = 597
      Top = 303
      Caption = #1055#1083#1072#1090#1072' ('#1076#1077#1085'.)'
      TabOrder = 5
      DesignSize = (
        130
        63)
      Height = 63
      Width = 130
      object txtPlata: TcxDBTextEdit
        Left = 16
        Top = 23
        Hint = 
          #1055#1083#1072#1090#1072' '#1074#1086' '#1076#1077#1085#1072#1088#1080'. '#1057#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1089#1077' '#1087#1088#1072#1074#1080' '#1076#1086#1075#1086#1074#1086#1088' '#1085#1072' '#1092#1080#1082#1089#1085#1072 +
          ' '#1087#1083#1072#1090#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'PLATA'
        DataBinding.DataSource = dm.dsAneks
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 100
      end
    end
    object PodatociZaVrabotuvanje: TcxGroupBox
      Left = 21
      Top = 46
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      TabOrder = 3
      DesignSize = (
        706
        251)
      Height = 251
      Width = 706
      object Label4: TLabel
        Left = 20
        Top = 26
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 14
        Top = 53
        Width = 95
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 10
        Top = 80
        Width = 99
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1080' '#1095#1072#1089#1086#1074#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 321
        Top = 80
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtVidVrabotuvanje: TcxDBTextEdit
        Tag = 1
        Left = 115
        Top = 23
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1042#1080#1076' '#1085#1072' '#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
        BeepOnEnter = False
        DataBinding.DataField = 'VID_VRABOTUVANJE'
        DataBinding.DataSource = dm.dsAneks
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 58
      end
      object cbVidVrabotuvanje: TcxDBLookupComboBox
        Tag = 1
        Left = 174
        Top = 23
        Hint = #1054#1087#1080#1089' '#1085#1072' '#1042#1080#1076' '#1085#1072' '#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'VID_VRABOTUVANJE'
        DataBinding.DataSource = dm.dsAneks
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'ID'
          end
          item
            Width = 600
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsVidVrabotuvanje
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 517
      end
      object ID_RM_RE: TcxDBTextEdit
        Tag = 1
        Left = 115
        Top = 50
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dm.dsAneks
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 58
      end
      object ID_RM_RE_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 174
        Top = 50
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dm.dsAneks
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV_RE'
          end
          item
            FieldName = 'NAZIV_RM'
          end
          item
            FieldName = 'SIS_OPIS'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsRMRE
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 517
      end
      object RABOTNI_CASOVI: TcxDBTextEdit
        Left = 115
        Top = 77
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1095#1072#1089#1086#1074#1080
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_CASOVI'
        DataBinding.DataSource = dm.dsAneks
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 159
      end
      object MB: TcxDBTextEdit
        Left = 416
        Top = 77
        Hint = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'RAKOVODITEL'
        DataBinding.DataSource = dm.dsAneks
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 159
      end
      object cxGroupBox1: TcxGroupBox
        Left = 16
        Top = 104
        Anchors = [akLeft, akTop, akRight]
        Caption = '  '#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1088#1080#1084#1072#1114#1072'  '
        Style.LookAndFeel.Kind = lfOffice11
        Style.LookAndFeel.NativeStyle = False
        Style.TextStyle = [fsBold]
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.NativeStyle = False
        TabOrder = 6
        Height = 137
        Width = 677
        object Label21: TLabel
          Left = 86
          Top = 52
          Width = 74
          Height = 18
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1082#1086#1074#1086#1076#1077#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label23: TLabel
          Left = 3
          Top = 73
          Width = 154
          Height = 26
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1077#1085'. '#1079#1072' '#1043#1054' '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label25: TLabel
          Left = 264
          Top = 52
          Width = 24
          Height = 12
          AutoSize = False
          Caption = '%'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label26: TLabel
          Left = 6
          Top = 26
          Width = 154
          Height = 18
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label10: TLabel
          Left = 6
          Top = 107
          Width = 154
          Height = 18
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label11: TLabel
          Left = 260
          Top = 107
          Width = 24
          Height = 12
          AutoSize = False
          Caption = '%'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Rakovodenje: TcxDBTextEdit
          Left = 166
          Top = 50
          Hint = 
            #1047#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1080#1086#1090' '#1073#1086#1076' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1088#1072#1082#1086#1074#1086#1076#1085#1072#1090#1072' '#1092#1091#1085#1082#1094#1080#1112 +
            #1072
          BeepOnEnter = False
          DataBinding.DataField = 'RAKOVODENJE'
          DataBinding.DataSource = dm.dsAneks
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
        object DenUsloviGO: TcxDBTextEdit
          Left = 166
          Top = 77
          Hint = 
            #1050#1086#1083#1082#1091' '#1089#1083#1086#1073#1086#1076#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1076#1086#1076#1072#1074#1072#1072#1090' '#1085#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088', '#1082#1072#1082#1086' '#1088#1077#1079#1091#1083 +
            #1090#1072#1090' '#1085#1072' '#1087#1086#1089#1077#1073#1085#1080#1090#1077' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1072
          BeepOnEnter = False
          DataBinding.DataField = 'DEN_USLOVI_RABOTA'
          DataBinding.DataSource = dm.dsAneks
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
        object koeficient: TcxDBTextEdit
          Left = 166
          Top = 23
          Hint = 
            #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' ('#1086#1074#1086#1112' '#1082#1086#1077#1092#1080#1094#1080#1077#1085#1090' '#1089#1077' '#1087#1088#1077#1074#1079#1077#1084#1072' '#1086#1076' '#1082#1086#1077#1092#1080#1094#1080#1077#1085#1090#1086#1090' '#1085#1072' '#1089#1083#1086#1078#1077 +
            #1085#1086#1089#1090' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1086#1090#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086')'
          BeepOnEnter = False
          DataBinding.DataField = 'KOEFICIENT'
          DataBinding.DataSource = dm.dsAneks
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
        object BENEFICIRAN_STAZ: TcxDBTextEdit
          Left = 166
          Top = 104
          Hint = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078' '#1087#1088#1086#1094#1077#1085#1090
          BeepOnEnter = False
          DataBinding.DataField = 'BENEFICIRAN_STAZ'
          DataBinding.DataSource = dm.dsAneks
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
      end
    end
    object OPIS: TcxDBRichEdit
      Left = 750
      Top = 96
      Hint = 
        #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1072#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088'. *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' ' +
        #1079#1072'  '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsAneks
      ParentFont = False
      Properties.WantReturns = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 6
      OnDblClick = OPISDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 201
      Width = 247
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1064
    OnTabChanging = dxRibbon1TabChanging
    ExplicitWidth = 1064
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 1064
    ExplicitTop = 718
    ExplicitWidth = 1064
  end
  object txtArhivskiBroj: TcxTextEdit [4]
    Left = 8
    Top = 75
    AutoSize = False
    StyleDisabled.Color = clWindow
    StyleDisabled.TextColor = clWindowText
    TabOrder = 4
    OnKeyDown = EnterKakoTab
    Height = 21
    Width = 84
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 544
    Top = 152
  end
  inherited PopupMenu1: TPopupMenu
    Left = 648
  end
  inherited dxBarManager1: TdxBarManager
    Left = 872
    Top = 264
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 94
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 577
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 897
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 277
      DockedTop = 0
      FloatLeft = 694
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 796
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = '   '
      CaptionButtons = <>
      DockedLeft = 460
      DockedTop = 0
      FloatLeft = 1098
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aKreirajDokument
      Category = 0
      LargeImageIndex = 10
    end
    object dxBarEdit1: TdxBarEdit
      Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Category = 0
      Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Visible = ivAlways
      Width = 120
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Category = 0
      Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
      CanSelect = False
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object f: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      OnExit = fExit
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aUsloviPoteskiOdNormalni
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 856
    Top = 296
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aKreirajDokument: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 19
      OnExecute = aKreirajDokumentExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
    object aUsloviPoteskiOdNormalni: TAction
      Caption = #1059#1089#1083#1086#1074#1080' '#1087#1086#1090#1077#1096#1082#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1083#1085#1080#1090#1077
      ImageIndex = 92
      OnExecute = aUsloviPoteskiOdNormalniExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 704
    Top = 176
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40694.600725960650000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 1000
    PixelsPerInch = 96
  end
  object tblRMRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hrr.rakovodenje,'
      '    hrr.denovi_odmor,'
      '    hrr.den_uslovi_rabota,'
      '    hrr.uslovi_rabota,'
      '    hrm.stepen_slozenost,'
      
        '    hrk.id as usloviRabotaID,  hrk.procent as usloviRabotaProcen' +
        't,'
      '    hrr.beneficiran_staz,'
      '    hs.opis as sis_opis'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hrr.uslo' +
        'vi_rabota'
      
        'where /*$$IBEC$$ hs.do_datum is null and $$IBEC$$*/ hs.id_re_fir' +
        'ma = :firma'
      'order by 4, 8')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 832
    Top = 235
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRMRERAKOVODENJE: TFIBBCDField
      FieldName = 'RAKOVODENJE'
      Size = 2
    end
    object tblRMREDENOVI_ODMOR: TFIBIntegerField
      FieldName = 'DENOVI_ODMOR'
    end
    object tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblRMRESTEPEN_SLOZENOST: TFIBBCDField
      FieldName = 'STEPEN_SLOZENOST'
      Size = 8
    end
    object tblRMREUSLOVI_RABOTA: TFIBIntegerField
      FieldName = 'USLOVI_RABOTA'
    end
    object tblRMREUSLOVIRABOTAID: TFIBIntegerField
      FieldName = 'USLOVIRABOTAID'
    end
    object tblRMREUSLOVIRABOTAPROCENT: TFIBBCDField
      FieldName = 'USLOVIRABOTAPROCENT'
      Size = 2
    end
    object tblRMREBENEFICIRAN_STAZ: TFIBBCDField
      FieldName = 'BENEFICIRAN_STAZ'
      Size = 2
    end
    object tblRMRESIS_OPIS: TFIBStringField
      DisplayLabel = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'SIS_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 771
    Top = 267
  end
  object qMaxBroj: TpFIBQuery
    Transaction = dm.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select cast((coalesce(max(cast(coalesce(substring(hra.broj from ' +
        'strlen(hra.arhivski_broj)+2 for 3),0) as integer)),0)) as intege' +
        'r) maks'
      'from hr_dv_aneks hra'
      'where cast(hra.arhivski_broj as varchar(20)) = :arhivski_broj')
    Left = 768
    Top = 216
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblUsloviPoteskiOdNormalni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_KOEFICIENT_USLOVIRAB'
      'SET '
      '    NAZIV = :NAZIV,'
      '    PROCENT = :PROCENT,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_KOEFICIENT_USLOVIRAB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_KOEFICIENT_USLOVIRAB('
      '    ID,'
      '    NAZIV,'
      '    PROCENT,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :PROCENT,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select k.id,'
      '       k.naziv,'
      '       k.procent,'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from hr_koeficient_uslovirab k'
      ''
      ' WHERE '
      '        K.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select k.id,'
      '       k.naziv,'
      '       k.procent,'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from hr_koeficient_uslovirab k'
      'order by k.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_KOEFICIENT_USLOVIRAB'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_KOEFICIENT_USLOVIRAB_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 496
    Top = 1816
    object tblUsloviPoteskiOdNormalniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblUsloviPoteskiOdNormalniNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUsloviPoteskiOdNormalniPROCENT: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090
      FieldName = 'PROCENT'
      Size = 2
    end
    object tblUsloviPoteskiOdNormalniTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblUsloviPoteskiOdNormalniTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblUsloviPoteskiOdNormalniUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUsloviPoteskiOdNormalniUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsUsloviPoteskiOdNormalni: TDataSource
    DataSet = tblUsloviPoteskiOdNormalni
    Left = 600
    Top = 1816
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 152
  end
  object qCountAneksZaDogovor: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(ha.id) as c'
      'from hr_dv_aneks ha'
      'where ha.id_dogovor_vrabouvanje = :id_dogovor')
    Left = 320
    Top = 168
  end
end
