object frmResenijeZaGodisenOdmor: TfrmResenijeZaGodisenOdmor
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
  ClientHeight = 749
  ClientWidth = 1012
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1012
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    OnTabChanged = dxRibbon1TabChanged
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 726
    Width = 1012
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 1012
    Height = 169
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 1008
      Height = 157
      Align = alClient
      PopupMenu = PopupMenu1
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsResenieGO
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
        end
        object cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDDOKUMENTNAZIV'
          Visible = False
        end
        object cxGrid1DBTableView1BARANJE_GO: TcxGridDBColumn
          Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088' '
          DataBinding.FieldName = 'BARANJE_GO'
          Visible = False
        end
        object cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'ARHIVSKI_BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 100
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RENAZIV'
          Width = 166
        end
        object cxGrid1DBTableView1RMNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RMNAZIV'
          Width = 170
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 77
        end
        object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 200
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Width = 92
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 109
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'VKUPNO_DENOVI'
          Width = 78
        end
        object cxGrid1DBTableView1KORISTENJE: TcxGridDBColumn
          DataBinding.FieldName = 'KORISTENJE'
          Visible = False
        end
        object cxGrid1DBTableView1KoristenjeNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'KoristenjeNaziv'
          Width = 121
        end
        object cxGrid1DBTableView1PRV_DEL: TcxGridDBColumn
          DataBinding.FieldName = 'PRV_DEL'
          Width = 85
        end
        object cxGrid1DBTableView1DATUM1_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM1_OD'
          SortIndex = 0
          SortOrder = soAscending
          Width = 77
        end
        object cxGrid1DBTableView1DATUM1_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM1_DO'
          SortIndex = 1
          SortOrder = soAscending
          Width = 81
        end
        object cxGrid1DBTableView1VTOR_DEL: TcxGridDBColumn
          DataBinding.FieldName = 'VTOR_DEL'
          Width = 86
        end
        object cxGrid1DBTableView1DATUM2_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM2_OD'
        end
        object cxGrid1DBTableView1DATUM2_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM2_DO'
          Width = 71
        end
        object cxGrid1DBTableView1MB_RAKOVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'MB_RAKOVODITEL'
          Width = 160
        end
        object cxGrid1DBTableView1MB_ZAMENA: TcxGridDBColumn
          DataBinding.FieldName = 'MB_ZAMENA'
          Visible = False
        end
        object cxGrid1DBTableView1ZAKONSKI_MINIMUM: TcxGridDBColumn
          DataBinding.FieldName = 'ZAKONSKI_MINIMUM'
          Visible = False
        end
        object cxGrid1DBTableView1STAZ: TcxGridDBColumn
          DataBinding.FieldName = 'STAZ'
          Visible = False
        end
        object cxGrid1DBTableView1STAZ_MESECI: TcxGridDBColumn
          DataBinding.FieldName = 'STAZ_MESECI'
          Visible = False
          Width = 72
        end
        object cxGrid1DBTableView1STAZ_DENOVI: TcxGridDBColumn
          DataBinding.FieldName = 'STAZ_DENOVI'
          Visible = False
        end
        object cxGrid1DBTableView1STEPEN_NA_SLOZENOST: TcxGridDBColumn
          DataBinding.FieldName = 'STEPEN_NA_SLOZENOST'
          Visible = False
        end
        object cxGrid1DBTableView1POSEBNI_USLOVI: TcxGridDBColumn
          DataBinding.FieldName = 'POSEBNI_USLOVI'
          Visible = False
        end
        object cxGrid1DBTableView1POL_VOZRAST: TcxGridDBColumn
          DataBinding.FieldName = 'POL_VOZRAST'
          Visible = False
        end
        object cxGrid1DBTableView1CUVANJE_DETE: TcxGridDBColumn
          DataBinding.FieldName = 'CUVANJE_DETE'
          Width = 91
        end
        object cxGrid1DBTableView1ZDRASTVENA_SOSTOJBA: TcxGridDBColumn
          DataBinding.FieldName = 'ZDRASTVENA_SOSTOJBA'
          Visible = False
        end
        object cxGrid1DBTableView1SZ_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'SZ_NAZIV'
        end
        object cxGrid1DBTableView1INVALID_DENOVI: TcxGridDBColumn
          DataBinding.FieldName = 'INVALID_DENOVI'
          Visible = False
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1076#1086#1076#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 2
      Top = 159
      Width = 1008
      Height = 8
      AlignSplitter = salBottom
      Control = dPanel
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 295
    Width = 1012
    Height = 431
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Enabled = False
    TabOrder = 3
    DesignSize = (
      1012
      431)
    object Label8: TLabel
      Tag = 15
      Left = 55
      Top = 39
      Width = 89
      Height = 26
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel
      Left = 80
      Top = 13
      Width = 66
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = -13
      Top = 25
      Width = 159
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1076#1086#1085#1077#1089#1091#1074#1072#1114#1077' '#1088#1077#1096#1077#1085#1080#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 55
      Top = 74
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 236
      Top = 20
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label28: TLabel
      Left = -7
      Top = 390
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1047#1072#1084#1077#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object MB: TcxDBTextEdit
      Tag = 1
      Left = 150
      Top = 44
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Properties.OnEditValueChanged = MBPropertiesEditValueChanged
      ShowHint = True
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = t
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object VRABOTENIME: TcxDBLookupComboBox
      Tag = 1
      Left = 273
      Top = 44
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      ShowHint = True
      StyleDisabled.TextColor = clBtnText
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = t
      OnKeyDown = EnterKakoTab
      Width = 273
    end
    object DatumKreiranje: TcxDBDateEdit
      Left = 152
      Top = 17
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1076#1086#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikRegExpr
      ShowHint = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = DatumKreiranjeExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object cxGroupBox1: TcxGroupBox
      Left = 462
      Top = 98
      Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      TabOrder = 8
      Height = 283
      Width = 383
      object Label1: TLabel
        Left = -7
        Top = 39
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072' '#1075#1086#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object godina: TcxDBTextEdit
        Left = 88
        Top = 36
        Hint = #1043#1086#1076#1080#1085#1072' '#1079#1072' '#1082#1086#1112#1072' '#1074#1072#1078#1080' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088
        BeepOnEnter = False
        DataBinding.DataField = 'GODINA'
        DataBinding.DataSource = dm.dsResenieGO
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 89
      end
      object cxGroupBox3: TcxGroupBox
        Left = 15
        Top = 163
        Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1074#1090#1086#1088#1080#1086#1090' '#1076#1077#1083' '#1086#1076' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088
        Caption = #1042#1090#1086#1088' '#1076#1077#1083
        Enabled = False
        TabOrder = 3
        Height = 86
        Width = 354
        object Label5: TLabel
          Left = -9
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 16
          Top = 54
          Width = 96
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 160
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Datum2Od: TcxDBDateEdit
          Left = 47
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM2_OD'
          DataBinding.DataSource = dm.dsResenieGO
          ParentFont = False
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikRegExpr
          ShowHint = True
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = Datum2OdExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object VtorDel: TcxDBTextEdit
          Left = 118
          Top = 51
          BeepOnEnter = False
          DataBinding.DataField = 'VTOR_DEL'
          DataBinding.DataSource = dm.dsResenieGO
          ParentFont = False
          ParentShowHint = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          ShowHint = True
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = VtorDelExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object Datum2Do: TcxDBDateEdit
          Left = 216
          Top = 24
          TabStop = False
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM2_DO'
          DataBinding.DataSource = dm.dsResenieGO
          ParentFont = False
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikRegExpr
          ShowHint = True
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = Datum2DoExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 15
        Top = 71
        Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1087#1088#1074#1080#1086#1090' '#1076#1077#1083' '#1086#1076' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088
        Caption = #1055#1088#1074' '#1076#1077#1083
        TabOrder = 2
        Height = 86
        Width = 354
        object Label2: TLabel
          Left = -9
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 16
          Top = 54
          Width = 96
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 160
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DatumOd: TcxDBDateEdit
          Left = 47
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM1_OD'
          DataBinding.DataSource = dm.dsResenieGO
          ParentFont = False
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikRegExpr
          ShowHint = True
          TabOrder = 0
          OnEnter = t
          OnExit = DatumOdExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object PrvDel: TcxDBTextEdit
          Left = 118
          Top = 51
          BeepOnEnter = False
          DataBinding.DataField = 'PRV_DEL'
          DataBinding.DataSource = dm.dsResenieGO
          ParentFont = False
          ParentShowHint = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          ShowHint = True
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = PrvDelExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object DatumDo: TcxDBDateEdit
          Left = 216
          Top = 24
          TabStop = False
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM1_DO'
          DataBinding.DataSource = dm.dsResenieGO
          ParentFont = False
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikRegExpr
          ShowHint = True
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = DatumDoExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
      end
      object cxDBRadioGroup2: TcxDBRadioGroup
        Left = 184
        Top = 25
        Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1080#1114#1077' '#1085#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088'. '#1045#1076#1085#1086#1082#1088#1072#1090#1085#1086'/'#1044#1074#1086#1082#1088#1072#1090#1085#1086
        TabStop = False
        Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
        DataBinding.DataField = 'KORISTENJE'
        DataBinding.DataSource = dm.dsResenieGO
        ParentFont = False
        ParentShowHint = False
        Properties.Columns = 2
        Properties.ImmediatePost = True
        Properties.Items = <
          item
            Caption = #1045#1076#1085#1086#1082#1088#1072#1090#1085#1086
            Value = 1
          end
          item
            Caption = #1044#1074#1086#1082#1088#1072#1090#1085#1086
            Value = 2
          end>
        Properties.OnEditValueChanged = cxDBRadioGroup2PropertiesEditValueChanged
        ShowHint = True
        TabOrder = 1
        Height = 40
        Width = 185
      end
    end
    object OtkaziButton: TcxButton
      Left = 893
      Top = 387
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 10
    end
    object ZapisiButton: TcxButton
      Left = 812
      Top = 387
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 9
    end
    object Odobreno_od: TcxDBTextEdit
      Left = 150
      Top = 71
      Hint = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' '#1086#1076' '#1082#1086#1112' '#1077' '#1076#1086#1085#1077#1089#1077#1085#1086' '#1088#1077#1096#1077#1085#1080#1077#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'MB_RAKOVODITEL'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Properties.OnChange = Odobreno_odPropertiesChange
      ShowHint = True
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = t
      OnKeyDown = EnterKakoTab
      Width = 396
    end
    object cxGroupBox4: TcxGroupBox
      Left = 23
      Top = 98
      Hint = #1055#1088#1072#1074#1080#1083#1086' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      Caption = 
        #1044#1086#1083#1078#1080#1085#1072#1090#1072' '#1085#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1072' '#1089#1087#1086#1088#1077#1076' '#1089#1083#1077#1076#1085#1080#1074#1077' '#1082#1088#1080#1090#1077#1088 +
        #1080#1091#1084#1080
      TabOrder = 5
      Height = 283
      Width = 439
      object Label12: TLabel
        Left = 17
        Top = 26
        Width = 106
        Height = 13
        AutoSize = False
        Caption = #1047#1072#1082#1086#1085#1089#1082#1080' '#1084#1080#1085#1080#1084#1091#1084
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 17
        Top = 53
        Width = 105
        Height = 18
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1086#1076' '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 17
        Top = 80
        Width = 131
        Height = 16
        AutoSize = False
        Caption = #1057#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1072' '#1088#1072#1073#1086#1090#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 17
        Top = 161
        Width = 304
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1089#1086' '#1085#1072#1112#1084#1072#1083#1082#1091' 60% '#1090#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114#1077
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 17
        Top = 244
        Width = 146
        Height = 13
        AutoSize = False
        Caption = #1042#1082#1091#1087#1085#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 184
        Top = 53
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1075#1086#1076#1080#1085#1080
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 401
        Top = 25
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 17
        Top = 107
        Width = 243
        Height = 13
        AutoSize = False
        Caption = #1055#1086#1089#1077#1073#1085#1080' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label23: TLabel
        Left = 17
        Top = 134
        Width = 289
        Height = 13
        AutoSize = False
        Caption = #1047#1072' '#1087#1086#1074#1086#1079#1088#1072#1089#1085#1080' '#1088#1072#1073#1086#1090#1085#1080#1094#1080' (57 - '#1078#1077#1085#1080' '#1080' 59 - '#1084#1072#1078#1080')'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label24: TLabel
        Left = 401
        Top = 53
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label25: TLabel
        Left = 401
        Top = 80
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 401
        Top = 107
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 401
        Top = 134
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 401
        Top = 161
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label20: TLabel
        Left = 401
        Top = 244
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label29: TLabel
        Left = 17
        Top = 183
        Width = 322
        Height = 23
        AutoSize = False
        Caption = #1047#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1080#1085#1074#1072#1083#1080#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label30: TLabel
        Left = 403
        Top = 215
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label31: TLabel
        Left = 17
        Top = 210
        Width = 289
        Height = 31
        AutoSize = False
        Caption = 
          #1047#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1082#1086#1112' '#1085#1077#1075#1091#1074#1072' '#1080' '#1095#1091#1074#1072' '#1076#1077#1090#1077' '#1089#1086' '#1090#1077#1083#1077#1089#1077#1085' '#1080#1083#1080' '#1076#1091#1096#1077#1074#1077#1085' '#1085#1077#1076#1086#1089#1090 +
          #1072#1090#1086#1082' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label32: TLabel
        Left = 401
        Top = 188
        Width = 30
        Height = 13
        AutoSize = False
        Caption = #1076#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label33: TLabel
        Left = 288
        Top = 53
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1084#1077#1089#1077#1094#1080
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object STAZ: TcxDBTextEdit
        Left = 128
        Top = 50
        Hint = #1056#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
        BeepOnEnter = False
        DataBinding.DataField = 'STAZ'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object STEPEN_NA_SLOZENOST: TcxDBTextEdit
        Left = 345
        Top = 77
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1088#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1089#1090#1077#1087 +
          #1077#1085#1086#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1072' '#1088#1072#1073#1086#1090#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'STEPEN_NA_SLOZENOST'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object ZDRASTVENA_SOSTOJBA: TcxDBTextEdit
        Left = 345
        Top = 158
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1088#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1079#1076#1088#1072 +
          #1089#1090#1074#1077#1085#1072#1090#1072' '#1089#1086#1089#1090#1086#1112#1073#1072'. '#1047#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1089#1086' '#1085#1072#1112#1084#1072#1083#1082#1091' 60% '#1090#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114 +
          #1077
        BeepOnEnter = False
        DataBinding.DataField = 'ZDRASTVENA_SOSTOJBA'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object SUMAGO: TcxDBTextEdit
        Left = 345
        Top = 241
        Hint = #1042#1082#1091#1087#1085#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
        BeepOnEnter = False
        DataBinding.DataField = 'VKUPNO_DENOVI'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object ZakonskiMinimum: TcxDBTextEdit
        Left = 345
        Top = 23
        Hint = #1047#1072#1082#1086#1085#1089#1082#1080' '#1084#1080#1085#1080#1084#1091#1084' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
        BeepOnEnter = False
        DataBinding.DataField = 'ZAKONSKI_MINIMUM'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Properties.OnChange = ZakonskiMinimumPropertiesChange
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object DenoviOdStaz: TcxDBTextEdit
        Left = 345
        Top = 50
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1089#1090#1072 +
          #1078#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'STAZ_DENOVI'
        DataBinding.DataSource = dm.dsResenieGO
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object SlozenostNaRabota: TcxDBTextEdit
        Left = 154
        Top = 77
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        BeepOnEnter = False
        DataBinding.DataField = 'SZ_NAZIV'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 175
      end
      object PosebniUsloviZaRabota: TcxDBTextEdit
        Left = 345
        Top = 104
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1072' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1089#1090#1077#1087#1077#1085#1086#1090' '#1085#1072' '#1089#1083 +
          #1086#1078#1077#1085#1086#1089#1090#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'POSEBNI_USLOVI'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object PovozrasniVrabotenii: TcxDBTextEdit
        Left = 345
        Top = 131
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1072' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1074#1086#1079#1088#1072#1089#1090#1072' '#1080' '#1087#1086#1083 +
          #1086#1090'. '#1047#1072' '#1087#1086#1074#1086#1079#1088#1072#1089#1085#1080' '#1088#1072#1073#1086#1090#1085#1080#1094#1080' (57 - '#1078#1077#1085#1080' '#1080' 59 - '#1084#1072#1078#1080')'
        BeepOnEnter = False
        DataBinding.DataField = 'POL_VOZRAST'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object cuvanje_dete: TcxDBTextEdit
        Left = 345
        Top = 212
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1088#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1079#1076#1088#1072 +
          #1089#1090#1074#1077#1085#1072#1090#1072' '#1089#1086#1089#1090#1086#1112#1073#1072'. '#1047#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1089#1086' '#1085#1072#1112#1084#1072#1083#1082#1091' 60% '#1090#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114 +
          #1077
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'CUVANJE_DETE'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object Invalid: TcxDBTextEdit
        Left = 345
        Top = 185
        Hint = 
          #1047#1072' '#1082#1086#1083#1082#1091' '#1088#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1079#1076#1088#1072 +
          #1089#1090#1074#1077#1085#1072#1090#1072' '#1089#1086#1089#1090#1086#1112#1073#1072'. '#1047#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1089#1086' '#1085#1072#1112#1084#1072#1083#1082#1091' 60% '#1090#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114 +
          #1077
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'INVALID_DENOVI'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 10
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
      object CheckBox1: TCheckBox
        Left = 320
        Top = 215
        Width = 19
        Height = 17
        TabStop = False
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        Visible = False
        OnClick = CheckBox1Click
      end
      object STAZ_MESECI: TcxDBTextEdit
        Left = 232
        Top = 50
        Hint = #1056#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
        BeepOnEnter = False
        DataBinding.DataField = 'STAZ_MESECI'
        DataBinding.DataSource = dm.dsResenieGO
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 12
        OnEnter = cxDBTextEditAllEnter
        OnExit = t
        OnKeyDown = EnterKakoTab
        Width = 50
      end
    end
    object Broj: TcxDBTextEdit
      Left = 331
      Top = 17
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      StyleDisabled.BorderColor = clWindow
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clBtnText
      StyleDisabled.TextStyle = []
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = t
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object mbzamena: TcxDBTextEdit
      Left = 88
      Top = 387
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      BeepOnEnter = False
      DataBinding.DataField = 'MB_ZAMENA'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = t
      OnKeyDown = EnterKakoTab
      Width = 92
    end
    object vrabotenZamena: TcxDBLookupComboBox
      Left = 180
      Top = 387
      Hint = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085' '#1082#1086#1112' '#1077' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1079#1072' '#1079#1072#1084#1077#1085#1072
      BeepOnEnter = False
      DataBinding.DataField = 'MB_ZAMENA'
      DataBinding.DataSource = dm.dsResenieGO
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      ShowHint = True
      StyleDisabled.TextColor = clBtnText
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = t
      OnKeyDown = EnterKakoTab
      Width = 268
    end
  end
  object txtArhivskiBroj: TcxTextEdit
    Left = 135
    Top = 72
    AutoSize = False
    StyleDisabled.Color = clWindow
    StyleDisabled.TextColor = clWindowText
    TabOrder = 4
    OnExit = t
    OnKeyDown = EnterKakoTab
    Height = 21
    Width = 122
  end
  object PopupMenu1: TPopupMenu
    Left = 16
    Top = 80
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 184
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 268
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 617
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 824
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 443
      DockedTop = 0
      FloatLeft = 909
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = '                                                '
      CaptionButtons = <>
      DockedLeft = 115
      DockedTop = 0
      FloatLeft = 901
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 900
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 58
          Visible = True
          ItemName = 'cGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPecati
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076
      Category = 0
      LargeImageIndex = 19
    end
    object dxBarEdit1: TdxBarEdit
      Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Category = 0
      Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Visible = ivAlways
    end
    object txtArhivskiBroj1: TcxBarEditItem
      Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Category = 0
      Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Visible = ivAlways
      OnExit = txtArhivskiBroj1Exit
      PropertiesClassName = 'TcxTextEditProperties'
      InternalEditValue = ''
    end
    object dxBarEdit2: TdxBarEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Category = 0
      Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object CustomdxBarCombo1: TCustomdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivNever
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112'     '
      Category = 0
      Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112'     '
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object lcGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072'2'
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072'2'
      Visible = ivAlways
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.ListColumns = <>
    end
    object cxBarEditItem4: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object cxBarEditItem5: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object cGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cGodinaChange
      Items.Strings = (
        #1057#1080#1090#1077
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023')
      ItemIndex = -1
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
    object aPrevzemiVraboten: TAction
      Caption = 'aPrevzemiVraboten'
      OnExecute = aPrevzemiVrabotenExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 48
    Top = 48
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44509.356819699070000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 16
    Top = 48
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 48
    Top = 16
  end
  object qZakonskiMinimum: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select p.denovi_odmor,'
      
        '       p.go_staz_1, p.go_staz_2, p.go_staz_3, p.INVALID_DENOVI, ' +
        'p.cuvanje_dete, p.max_denovi_go1, p.max_denovi_go2'
      'from hr_parametri_kd p'
      
        'where :param between p.datum_od and coalesce(p.datum_do, current' +
        '_date)')
    Left = 176
    Top = 64
  end
  object qVrabotenParam: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select hdv.staz as staz,'
      '       hdv.staz_meseci as staz_meseci, '
      '       hdv.szo_naziv as slozenost_rabota,'
      '       hdv.szodnenovigo as denovi_go,'
      '       hdv.den_uslovi_rabota as den_uslovi_rabota,'
      ''
      
        '       (select a.den_uslovi_rabota from hr_dv_aneks a where (a.i' +
        'd_dogovor_vrabouvanje = hdv.id) and (current_date between a.datu' +
        'm_pocetok and coalesce(a.datum_do, current_date)))as denOdAneks,'
      ''
      
        '       case when (case  when extractmonth(hdv.datum_radjanje) <=' +
        ' extractmonth(current_date) then (extractyear(current_date) - ex' +
        'tractyear(hdv.datum_radjanje))'
      
        '                        else (extractyear(current_date) - extrac' +
        'tyear(hdv.datum_radjanje) - 1)'
      '                  end) >= 57 and hdv.pol = 2'
      
        '            then  (select p.den_zeni from hr_parametri_kd p wher' +
        'e :param between p.datum_od and coalesce(p.datum_do, current_dat' +
        'e))'
      
        '            when (case  when extractmonth(hdv.datum_radjanje) <=' +
        ' extractmonth(current_date) then (extractyear(current_date) - ex' +
        'tractyear(hdv.datum_radjanje))'
      
        '                        else (extractyear(current_date) - extrac' +
        'tyear(hdv.datum_radjanje) - 1)'
      '                  end) >= 59 and hdv.pol = 1'
      
        '            then  (select p.den_mazi from hr_parametri_kd p wher' +
        'e :param between p.datum_od and coalesce(p.datum_do, current_dat' +
        'e))'
      '            else 0'
      '       end "pol_vozrast",'
      
        '       case when hdv.telesno_ostetuvanje >= 60 then (select p.de' +
        'n_tel_ost from hr_parametri_kd p where :param between p.datum_od' +
        ' and coalesce(p.datum_do, current_date))'
      '             else 0'
      '       end "telesno_ostetuvanje",'
      '       hdv.invalid'
      'from view_hr_vraboteni hdv'
      ''
      'WHERE ID_RE_FIRMA = :FIRMA AND '
      '      RM_RE_ID_RE_FIRMA = :FIRMA AND'
      '      SIS_ID_RE_FIRMA = :FIRMA AND'
      '      MB like :MB AND'
      
        '     (current_date between datum_od and coalesce(datum_do, curre' +
        'nt_date))')
    Left = 184
    Top = 128
  end
  object qMaxBroj: TpFIBQuery
    Transaction = dm.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select cast((coalesce(max(cast(coalesce(substring(hrg.broj from ' +
        'char_length(hrg.arhivski_broj)+2 for 3),0) as integer)),0)) as i' +
        'nteger) maks'
      'from hr_resenie_go hrg'
      'where cast(hrg.arhivski_broj as varchar(20)) = :arhivski_broj')
    Left = 80
    Top = 48
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 80
    Top = 16
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 112
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
