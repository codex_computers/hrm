object frmStazKalkulator: TfrmStazKalkulator
  Left = 0
  Top = 0
  Caption = #1050#1072#1083#1082#1091#1083#1072#1090#1086#1088
  ClientHeight = 339
  ClientWidth = 319
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 319
    Height = 339
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGroupBox1: TcxGroupBox
      Left = 32
      Top = 150
      Caption = #1056#1077#1079#1091#1083#1090#1072#1090
      Enabled = False
      TabOrder = 0
      Height = 114
      Width = 257
      object cxLabel6: TcxLabel
        Left = 50
        Top = 24
        Caption = #1043#1086#1076#1080#1085#1072' :'
        Transparent = True
      end
      object RGodina: TcxTextEdit
        Left = 104
        Top = 21
        TabOrder = 1
        Width = 121
      end
      object cxLabel7: TcxLabel
        Left = 56
        Top = 51
        Caption = #1052#1077#1089#1077#1094' :'
        Transparent = True
      end
      object RDen: TcxTextEdit
        Left = 104
        Top = 75
        TabOrder = 3
        Width = 121
      end
      object cxLabel8: TcxLabel
        Left = 67
        Top = 78
        Caption = #1044#1077#1085' :'
        Transparent = True
      end
      object RMesec: TcxTextEdit
        Left = 104
        Top = 48
        TabOrder = 5
        Width = 121
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 32
      Top = 20
      TabOrder = 1
      Height = 117
      Width = 257
      object cxLabel1: TcxLabel
        Left = 16
        Top = 28
        Caption = #1055#1086#1095#1077#1090#1085#1072' '#1076#1072#1090#1072' :'
        Transparent = True
      end
      object cxLabel5: TcxLabel
        Left = 25
        Top = 57
        Caption = #1050#1088#1072#1112#1085#1072' '#1076#1072#1090#1072' :'
        Transparent = True
      end
      object MDatum: TcxDateEdit
        Tag = 1
        Left = 104
        Top = 54
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DatumDogovor: TcxDateEdit
        Tag = 1
        Left = 104
        Top = 27
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object cxButton1: TcxButton
        Left = 150
        Top = 81
        Width = 75
        Height = 25
        Caption = #1055#1088#1077#1089#1084#1077#1090#1072#1112
        TabOrder = 4
        OnClick = cxButton1Click
      end
    end
    object cxHyperLinkEdit1: TcxHyperLinkEdit
      Left = 32
      Top = 294
      Properties.Prefix = 
        'http://www.timeanddate.com/date/duration.html?y1=2005&m1=3&d1=16' +
        '&y2=2011&m2=3&d2=16&ti=on'
      TabOrder = 2
      Text = 
        'http://www.timeanddate.com/date/duration.html?y1=2005&m1=3&d1=16' +
        '&y2=2011&m2=3&d2=16&ti=on'
      Width = 257
    end
  end
end
