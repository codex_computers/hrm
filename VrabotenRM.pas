{*******************************************************}
{                                                       }
{     ��������� : ������ ��������
                  ������ �������                       }
{                                                       }
{     ����� : 14.12.2011                                }
{                                                       }
{     ������ : 1.1.1.17                                }
{                                                       }
{*******************************************************}

unit VrabotenRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxLabel, cxCalendar, cxGroupBox, cxRadioGroup, cxHint, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmPromenaRM = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cbRmRe: TcxDBLookupComboBox;
    txtRmRE: TcxDBTextEdit;
    Label2: TLabel;
    cxRadioGroup1: TcxRadioGroup;
    Label3: TLabel;
    txtDatumDo: TcxDBDateEdit;
    txtDatumOd: TcxDBDateEdit;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    txtMB: TcxTextEdit;
    txtVraboten: TcxTextEdit;
    cxHintStyleController1: TcxHintStyleController;
    Label5: TLabel;
    txtDokument: TcxDBTextEdit;
    cxButton1: TcxButton;
    OpenDialog1: TOpenDialog;
    dsRMRE: TDataSource;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMRENAZIV_RM: TFIBStringField;
    cxGrid1DBTableView1DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1DOCUMENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1DOCUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1Z_DOKUMENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1Z_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1Z_DOKUMENT_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1PRO_DOKUMENT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PRO_DOKUMENT_DATUM: TcxGridDBColumn;
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPromenaRM: TfrmPromenaRM;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmPromenaRM.aHelpExecute(Sender: TObject);
begin
  inherited;
   Application.HelpContext(20);
end;

procedure TfrmPromenaRM.cxButton1Click(Sender: TObject);
begin
  inherited;
//   openDialog1.Filter := 'PDF ��� Word ���������|*.txt;*.doc;*.pdf';
//   if OpenDialog1.Execute then
//   begin
//      dmsis.tblVrabRMDOKUMENT.Value:=OpenDialog1.FileName;
//   end;
end;

procedure TfrmPromenaRM.FormCreate(Sender: TObject);
begin
  inherited;
  dmsis.tblVrabRM.close;
  dmsis.tblVrabRM.ParamByName('mb').Value:= dm.tblLicaVraboteniMB.Value;
  dmsis.tblVrabRM.ParamByName('firma').Value:= dmkon.re;
  dmsis.tblVrabRM.open;
  dxBarManager1Bar1.Visible:=false;
  tblRMRE.ParamByName('firma').Value:=dmKon.re;
  tblRMRE.Open;
end;

procedure TfrmPromenaRM.FormShow(Sender: TObject);
begin
  inherited;
   txtMB.Text:=dm.tblLicaVraboteniMB.Value;
   txtVraboten.Text:=dm.tblLicaVraboteniNAZIV_VRABOTEN_TI.Value
end;

end.
