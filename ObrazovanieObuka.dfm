inherited frmObrazovanieObuka: TfrmObrazovanieObuka
  Caption = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1080' '#1086#1073#1091#1082#1072
  ClientHeight = 845
  ClientWidth = 843
  ExplicitWidth = 859
  ExplicitHeight = 883
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 843
    Height = 279
    ExplicitWidth = 843
    ExplicitHeight = 279
    inherited cxGrid1: TcxGrid
      Width = 839
      Height = 275
      ExplicitWidth = 839
      ExplicitHeight = 275
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsObrazovanieObuka
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'MB_VRABOTEN'
          Width = 84
        end
        object cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MOLBA'
          Visible = False
          Width = 109
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 250
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'IMEPREZIME'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 86
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 83
        end
        object cxGrid1DBTableView1PERIOD: TcxGridDBColumn
          DataBinding.FieldName = 'PERIOD'
          Visible = False
        end
        object cxGrid1DBTableView1STEPEN_OBRAZOVANIE: TcxGridDBColumn
          DataBinding.FieldName = 'STEPEN_OBRAZOVANIE'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1SZONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'SZONAZIV'
          Width = 148
        end
        object cxGrid1DBTableView1IME_ORGANIZACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'IME_ORGANIZACIJA'
          Visible = False
          Width = 87
        end
        object cxGrid1DBTableView1OINAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OINAZIV'
          Width = 250
        end
        object cxGrid1DBTableView1NASOKA: TcxGridDBColumn
          DataBinding.FieldName = 'NASOKA'
          Visible = False
          Width = 112
        end
        object cxGrid1DBTableView1NASOKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NASOKANAZIV'
          Width = 205
        end
        object cxGrid1DBTableView1KVALIFIKACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'KVALIFIKACIJA'
          Width = 156
        end
        object cxGrid1DBTableView1ZNAENJE: TcxGridDBColumn
          DataBinding.FieldName = 'ZNAENJE'
          Width = 143
        end
        object cxGrid1DBTableView1NIVO: TcxGridDBColumn
          DataBinding.FieldName = 'NIVO'
          Width = 114
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 405
    Width = 843
    Height = 417
    ExplicitTop = 405
    ExplicitWidth = 843
    ExplicitHeight = 417
    inherited Label1: TLabel
      Left = 578
      Top = 30
      Visible = False
      ExplicitLeft = 578
      ExplicitTop = 30
    end
    object Label11: TLabel [1]
      Left = 39
      Top = 28
      Width = 93
      Height = 32
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label10: TLabel [2]
      Left = 82
      Top = 17
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1086#1083#1073#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 634
      Top = 27
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsObrazovanieObuka
      TabOrder = 1
      Visible = False
      ExplicitLeft = 634
      ExplicitTop = 27
      ExplicitWidth = 96
      Width = 96
    end
    inherited OtkaziButton: TcxButton
      Left = 752
      Top = 369
      TabOrder = 8
      ExplicitLeft = 752
      ExplicitTop = 369
    end
    inherited ZapisiButton: TcxButton
      Left = 671
      Top = 369
      TabOrder = 7
      ExplicitLeft = 671
      ExplicitTop = 369
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Left = 138
      Top = 33
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsObrazovanieObuka
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object ID_MOLBA: TcxDBTextEdit
      Left = 138
      Top = 14
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1086#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsObrazovanieObuka
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object GroupBox1: TGroupBox
      Left = 17
      Top = 116
      Width = 648
      Height = 285
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1054#1087#1080#1089' '#1085#1072' '#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077'/'#1054#1073#1091#1082#1072
      TabOrder = 6
      DesignSize = (
        648
        285)
      object Label18: TLabel
        Left = 13
        Top = 204
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1080#1074#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 13
        Top = 163
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1077#1082#1085#1072#1090#1086' '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 14
        Top = 122
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 13
        Top = 174
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1079#1085#1072#1077#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = -18
        Top = 63
        Width = 133
        Height = 32
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label2: TLabel
        Left = -18
        Top = 101
        Width = 133
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1072#1089#1086#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label5: TLabel
        Left = 8
        Top = 36
        Width = 107
        Height = 31
        Hint = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object NIVO: TcxDBMemo
        Left = 121
        Top = 204
        Hint = 
          #1053#1080#1074#1086' '#1089#1087#1086#1088#1077#1076' '#1085#1072#1094#1080#1086#1085#1072#1083#1085#1072' '#1080#1083#1080' '#1084#1077#1107#1091#1085#1072#1088#1086#1076#1085#1072' '#1082#1083#1072#1094#1080#1089#1092#1080#1082#1072#1094#1080#1112#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' ' +
          #1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'NIVO'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        ParentFont = False
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        TabOrder = 8
        OnDblClick = NIVODblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 34
        Width = 504
      end
      object ZNAENJE: TcxDBMemo
        Left = 121
        Top = 163
        Hint = 
          #1043#1083#1072#1074#1085#1080' '#1087#1088#1077#1076#1084#1077#1090#1080' / '#1057#1090#1077#1082#1085#1072#1090#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1080' '#1074#1077#1096#1090#1080#1085#1080'  *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' ' +
          #1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ZNAENJE'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        ParentFont = False
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        TabOrder = 7
        OnDblClick = ZNAENJEDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 32
        Width = 504
      end
      object KVALIFIKACIJA: TcxDBMemo
        Left = 121
        Top = 122
        Hint = 
          #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1090#1077#1082#1085#1072#1090#1072' '#1082#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1112#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076 +
          #1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'KVALIFIKACIJA'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        ParentFont = False
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        TabOrder = 6
        OnDblClick = KVALIFIKACIJADblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 32
        Width = 504
      end
      object Obrazovna: TcxDBTextEdit
        Left = 121
        Top = 68
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072
        DataBinding.DataField = 'IME_ORGANIZACIJA'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object Institucija: TcxDBLookupComboBox
        Left = 202
        Top = 68
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'IME_ORGANIZACIJA'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsObrazovnaInstitucija
        StyleDisabled.TextColor = clBtnText
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 423
      end
      object Nasoka: TcxDBTextEdit
        Left = 121
        Top = 95
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1072#1089#1086#1082#1072
        DataBinding.DataField = 'NASOKA'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object NasokaNaziv: TcxDBLookupComboBox
        Left = 202
        Top = 95
        Hint = #1053#1072#1089#1086#1082#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'NASOKA'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'OINAZIV'
          end>
        Properties.ListSource = dm.dsNasokaObrazovanie
        StyleDisabled.TextColor = clBtnText
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 423
      end
      object txtSZObrazovanie: TcxDBTextEdit
        Left = 121
        Top = 41
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        BeepOnEnter = False
        DataBinding.DataField = 'SZONAZIV'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object cbSZObrazovanie: TcxDBLookupComboBox
        Left = 202
        Top = 41
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'SZONAZIV'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'NAZIV'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'opis'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmSis.dsSZObrazovanie
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 423
      end
    end
    object IMEPREZIME: TcxDBLookupComboBox
      Left = 235
      Top = 14
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' '#1082#1086#1077' '#1112#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072' '#1084#1086#1083#1073#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsObrazovanieObuka
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'ID'
        end
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'IMEPREZIME'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dmSis.dsMolba
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 407
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Left = 235
      Top = 33
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsObrazovanieObuka
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      ShowHint = True
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 407
    end
    object GroupBox2: TGroupBox
      Left = 89
      Top = 60
      Width = 363
      Height = 50
      Caption = #1055#1077#1088#1080#1086#1076
      TabOrder = 5
      object Label13: TLabel
        Left = 126
        Top = 23
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = -50
        Top = 23
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 225
        Top = 20
        Hint = #1044#1072#1090#1091#1084' '#1082#1086#1075#1072' '#1077' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077#1090#1086'/ '#1086#1073#1091#1082#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = DATUM_DOExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DATUM_OD: TcxDBDateEdit
        Left = 49
        Top = 20
        Hint = #1044#1072#1090#1091#1084' '#1082#1086#1075#1072' '#1077' '#1087#1086#1095#1085#1072#1090#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077#1090#1086'/ '#1086#1073#1091#1082#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dmSis.dsObrazovanieObuka
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = DATUM_ODExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 843
    ExplicitWidth = 843
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 822
    Width = 843
    ExplicitTop = 822
    ExplicitWidth = 843
  end
  inherited dxBarManager1: TdxBarManager
    Left = 432
    Top = 200
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077'/'#1054#1073#1091#1082#1072
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aUrediTextOpis: TAction
      Caption = 'aUrediTextOpis'
    end
    object aUrediTextUslovi: TAction
      Caption = 'aUrediTextUslovi'
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page # of Pages #]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40700.570037719910000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
