inherited frmGrupaPrasanja: TfrmGrupaPrasanja
  Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1087#1088#1072#1096#1072#1114#1072
  ClientWidth = 664
  ExplicitWidth = 680
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 664
    Height = 274
    ExplicitWidth = 664
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Width = 660
      Height = 270
      ExplicitLeft = -12
      ExplicitTop = -38
      ExplicitWidth = 660
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmUcinok.dsGrupaPrasanja
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
        end
        object cxGrid1DBTableView1NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'NORMATIV'
          Width = 134
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Width = 664
    Height = 130
    ExplicitTop = 400
    ExplicitWidth = 664
    ExplicitHeight = 130
    inherited Label1: TLabel
      Left = 24
      Top = 6
      Visible = False
      ExplicitLeft = 24
      ExplicitTop = 6
    end
    object Label2: TLabel [1]
      Left = 24
      Top = 33
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 2
      Top = 60
      Width = 72
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1086#1088#1084#1072#1090#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 80
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmUcinok.dsGrupaPrasanja
      TabOrder = 4
      Visible = False
      ExplicitLeft = 80
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 573
      Top = 90
      TabOrder = 3
      ExplicitLeft = 573
      ExplicitTop = 90
    end
    inherited ZapisiButton: TcxButton
      Left = 492
      Top = 90
      TabOrder = 2
      ExplicitLeft = 492
      ExplicitTop = 90
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 80
      Top = 30
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmUcinok.dsGrupaPrasanja
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 401
    end
    object NORMATIV: TcxDBTextEdit
      Left = 80
      Top = 57
      BeepOnEnter = False
      DataBinding.DataField = 'NORMATIV'
      DataBinding.DataSource = dmUcinok.dsGrupaPrasanja
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 664
    ExplicitWidth = 664
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 664
    ExplicitTop = 32000
    ExplicitWidth = 664
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 184
  end
  inherited PopupMenu1: TPopupMenu
    Left = 296
    Top = 304
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40379.519805856480000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
