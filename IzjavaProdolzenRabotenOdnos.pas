unit IzjavaProdolzenRabotenOdnos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, ActnList, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons,
  cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, dxCustomHint, cxHint,
  cxMemo, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit,
  cxCalendar, cxLabel, FIBQuery, pFIBQuery,frxRich, frxClass, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinOffice2013White, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions, cxGroupBox;

type
  TfrmIzjavaProdolzenRabotenOdnos = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    BROJ: TcxDBTextEdit;
    Label3: TLabel;
    DatumKreiranje: TcxDBDateEdit;
    VRABOTENIME: TcxDBLookupComboBox;
    MB: TcxDBTextEdit;
    Label8: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    OBRAZLOZENIE: TcxDBMemo;
    GODINA: TcxDBComboBox;
    cxHintStyleController1: TcxHintStyleController;
    dxBarManager1Bar5: TdxBar;
    cGodina: TdxBarCombo;
    dxBarManager1Bar6: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    txtArhivskiBroj: TcxTextEdit;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    qMaxBroj: TpFIBQuery;
    cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aKreirajPregledaj: TAction;
    aBrisiDokument: TAction;
    cxGroupBox1: TcxGroupBox;
    Label5: TLabel;
    DATUM_OD: TcxDBDateEdit;
    Label6: TLabel;
    DATUM_DO: TcxDBDateEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aNovExecute(Sender: TObject);
    procedure cGodinaChange(Sender: TObject);
    procedure t(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aKreirajPregledajExecute(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzjavaProdolzenRabotenOdnos: TfrmIzjavaProdolzenRabotenOdnos;
  arhivski_broj:string;
implementation

uses dmUnit, dmKonekcija, dmUnitOtsustvo, DaNe, dmReportUnit, Utils;

{$R *.dfm}

procedure TfrmIzjavaProdolzenRabotenOdnos.aBrisiDokumentExecute(Sender: TObject);
begin
  frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteIzjavaProdolzenRabOdnos.Close;
            dmOtsustvo.qDeleteIzjavaProdolzenRabOdnos.ParamByName('id').Value:=dm.tblIzjavaProdolzenROID.Value;
            dmOtsustvo.qDeleteIzjavaProdolzenRabOdnos.ExecQuery;
        end;
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.aKreirajPregledajExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;
//
  dmReport.tblizjavaProdolzuvanjeRO.close;
  dmReport.tblizjavaProdolzuvanjeRO.ParamByName('id').Value:=dm.tblIzjavaProdolzenROID.Value;
  dmReport.tblizjavaProdolzuvanjeRO.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.tblizjavaProdolzuvanjeRODATA.IsNull then
     begin
       RptStream := dmReport.tblizjavaProdolzuvanjeRO.CreateBlobStream(dmReport.tblizjavaProdolzuvanjeRODATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
        begin
           RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
           dmReport.frxReport1.LoadFromStream(RptStream);

            RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
            If RichView <> Nil Then
              begin
                RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
                RichView.DataSet:=dmReport.frxizjavaProdolzuvanjeRO;
              end;
            MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
            if MasterData <> Nil then
              MasterData.DataSet:=dmReport.frxizjavaProdolzuvanjeRO;

            dmReport.frxReport1.ShowReport;

            frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
            if (frmDaNe.ShowModal = mrYes) then
             begin
              dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 11);
              dm.tblIzjavaProdolzenRO.Refresh;
             end;
         end;
     end;
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.aNovExecute(Sender: TObject);
begin
  if txtArhivskiBroj.Text='' then
  begin
    frmDaNe := TfrmDaNe.Create(self, '������ ����', '������ ������� �������� ���. ���� ������ �� ����������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
           abort;
        end
       else
          begin
            arhivski_broj:='';
          end;
  end
  else
    arhivski_broj:=txtArhivskiBroj.Text;

  txtArhivskiBroj.Enabled:=False;

  inherited;
  txtArhivskiBroj.text:=arhivski_broj;
  dm.tblIzjavaProdolzenROARHIVSKI_BROJ.value:=txtArhivskiBroj.text;
  qMaxBroj.Close;
  qMaxBroj.ParamByName('arhivski_broj').AsString:=txtArhivskiBroj.text;
  qMaxBroj.ExecQuery;
  dm.tblIzjavaProdolzenROBROJ.Value:=txtArhivskiBroj.text+'/'+inttostr(qMaxBroj.FldByName['maks'].AsInteger+1);
  dm.tblIzjavaProdolzenRODATUM.Value:=now;
  dm.tblIzjavaProdolzenROGODINA.Value:=dmKon.godina;
  dm.tblIzjavaProdolzenROVID_DOKUMENT.Value:=izjava_prodolzen_ro;
  dm.tblIzjavaProdolzenROID_RE_FIRMA.Value:=dmKon.firma_id;

  mb.SetFocus;
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + cGodina.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
      begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
      end;
    end;
  end;
  txtArhivskiBroj.Enabled:=True;
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.cGodinaChange(Sender: TObject);
begin
  inherited;
  if (cGodina.Text = '����') then
         cxGrid1DBTableView1.DataController.Filter.Clear
     else if cGodina.Text <> '' then
      begin
          cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(cGodina.Text), cGodina.Text);
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
     end;
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  dm.tblIzjavaProdolzenRO.close;
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblIzjavaProdolzenRO.ParamByName('firma').Value:=dmKon.firma_id;
  dm.tblIzjavaProdolzenRO.ParamByName('Re').Value:='%';
  dm.tblIzjavaProdolzenRO.ParamByName('MB').Value:='%';
  dm.tblIzjavaProdolzenRO.open;
  cGodina.Text:=IntToStr(dmKon.godina);
end;

procedure TfrmIzjavaProdolzenRabotenOdnos.t(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;


end.
