inherited frmIntSelekcija: TfrmIntSelekcija
  Caption = #1057#1077#1083#1077#1082#1094#1080#1112#1072
  ClientHeight = 592
  ClientWidth = 668
  ExplicitWidth = 676
  ExplicitHeight = 626
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 668
    Height = 266
    ExplicitWidth = 668
    ExplicitHeight = 276
    inherited cxGrid1: TcxGrid
      Width = 664
      Height = 272
      ExplicitWidth = 664
      ExplicitHeight = 272
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dmOtsustvo.dsIntSelekcija
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 282
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 445
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 392
    Width = 668
    Height = 177
    ExplicitTop = 392
    ExplicitWidth = 668
    ExplicitHeight = 177
    inherited Label1: TLabel
      Left = 477
      Visible = False
      ExplicitLeft = 477
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 25
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = -39
      Top = 52
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 533
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsIntSelekcija
      TabOrder = 1
      Visible = False
      ExplicitLeft = 533
    end
    inherited OtkaziButton: TcxButton
      Left = 553
      Top = 121
      TabOrder = 4
      ExplicitLeft = 553
      ExplicitTop = 121
    end
    inherited ZapisiButton: TcxButton
      Left = 472
      Top = 121
      TabOrder = 3
      ExplicitLeft = 472
      ExplicitTop = 121
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 22
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmOtsustvo.dsIntSelekcija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 364
    end
    object txtOpis: TcxDBMemo
      Left = 69
      Top = 49
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmOtsustvo.dsIntSelekcija
      Properties.ScrollBars = ssVertical
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 99
      Width = 364
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 668
    ExplicitWidth = 668
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 569
    Width = 668
    ExplicitTop = 569
    ExplicitWidth = 668
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40295.353043900460000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
