inherited frmHRMVraboteni: TfrmHRMVraboteni
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1083#1080#1094#1072
  ClientHeight = 741
  ClientWidth = 1341
  WindowState = wsMaximized
  ExplicitWidth = 1357
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1341
    Height = 169
    ExplicitWidth = 1341
    ExplicitHeight = 169
    inherited cxGrid1: TcxGrid
      Width = 1337
      Height = 157
      ExplicitWidth = 1337
      ExplicitHeight = 157
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsLicaVraboteni
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ACTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'ACTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1ActivenNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'ActivenNaziv'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNAEDINICANAZIV'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
          Width = 30
        end
        object cxGrid1DBTableView1NAZIV_VID_DOKUMENT: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          DataBinding.FieldName = 'NAZIV_VID_DOKUMENT'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 88
        end
        object cxGrid1DBTableView1LICNA_KARTA: TcxGridDBColumn
          DataBinding.FieldName = 'LICNA_KARTA'
          Width = 118
        end
        object cxGrid1DBTableView1BROJ_PASOS: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_PASOS'
          Width = 100
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Width = 100
        end
        object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
          DataBinding.FieldName = 'TATKOVO_IME'
          Width = 100
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Width = 100
        end
        object cxGrid1DBTableView1MOMINSKO_PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'MOMINSKO_PREZIME'
          Width = 100
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 300
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 214
        end
        object cxGrid1DBTableView1DATUM_RADJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_RADJANJE'
          Width = 97
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' ('#1086#1076' '#1083#1080#1095#1085#1072' '#1082#1072#1088#1090#1072')'
          DataBinding.FieldName = 'ADRESA'
          Visible = False
          Width = 140
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1MESTO_RADJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_RADJANJE'
          Width = 100
        end
        object cxGrid1DBTableView1DRZAVA_RADJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'DRZAVA_RADJANJE'
          Width = 111
        end
        object cxGrid1DBTableView1POL: TcxGridDBColumn
          DataBinding.FieldName = 'POL'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1POLNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'POLNAZIV'
          Width = 52
        end
        object cxGrid1DBTableView1BRACNA_SOSTOJBA: TcxGridDBColumn
          DataBinding.FieldName = 'BRACNA_SOSTOJBA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1BRAK: TcxGridDBColumn
          DataBinding.FieldName = 'BRAK'
          Width = 87
        end
        object cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn
          DataBinding.FieldName = 'OBRAZOVANIE'
          Visible = False
        end
        object cxGrid1DBTableView1ZVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'ZVANJE'
          Width = 154
        end
        object cxGrid1DBTableView1STEPEN_STRUCNA_PODGOTOVKA: TcxGridDBColumn
          DataBinding.FieldName = 'SSTRUCNAPNAZIV'
          Width = 163
        end
        object cxGrid1DBTableView1ZDR_SOSTOJBA: TcxGridDBColumn
          DataBinding.FieldName = 'ZDR_SOSTOJBA'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Width = 110
        end
        object cxGrid1DBTableView1TELESNO_OSTETUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'TELESNO_OSTETUVANJE'
        end
        object cxGrid1DBTableView1INVALID: TcxGridDBColumn
          DataBinding.FieldName = 'INVALID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1INVALIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'INVALIDNAZIV'
          Width = 53
        end
        object cxGrid1DBTableView1ID_NACIONALNOST: TcxGridDBColumn
          DataBinding.FieldName = 'ID_NACIONALNOST'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1NACIONALNOSNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NACIONALNOSNAZIV'
          Width = 84
        end
        object cxGrid1DBTableView1ID_VEROISPOVED: TcxGridDBColumn
          DataBinding.FieldName = 'ID_VEROISPOVED'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1VEROISPOVEDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VEROISPOVEDNAZIV'
          Width = 82
        end
        object cxGrid1DBTableView1RABOTNOMESTO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTO'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1OPSTINA_RADJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'OPSTINA_RADJANJE'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1SSTRUCNAPNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'SSTRUCNAPNAZIV'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1SLIKA: TcxGridDBColumn
          DataBinding.FieldName = 'SLIKA'
          PropertiesClassName = 'TcxImageProperties'
          Properties.GraphicClassName = 'TdxSmartImage'
          Width = 58
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RM_RE_ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'RM_RE_ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1SIS_ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'SIS_ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'RM_ID'
          Visible = False
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 122
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 115
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 115
        end
      end
    end
    object SplitterdPanel: TcxSplitter
      Left = 2
      Top = 159
      Width = 1337
      Height = 8
      HotZoneClassName = 'TcxMediaPlayer9Style'
      HotZone.SizePercent = 32
      AlignSplitter = salBottom
      AllowHotZoneDrag = False
      InvertDirection = True
      Control = dPanel
    end
  end
  inherited dPanel: TPanel
    Top = 295
    Width = 1341
    Height = 423
    ExplicitTop = 295
    ExplicitWidth = 1341
    ExplicitHeight = 423
    inherited Label1: TLabel
      Left = 190
      Top = 17
      Width = 85
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' :'
      ExplicitLeft = 190
      ExplicitTop = 17
      ExplicitWidth = 85
    end
    object lblCustom1: TLabel [1]
      Left = 158
      Top = 362
      Width = 109
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lblCustom1'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblCustom2: TLabel [2]
      Left = 366
      Top = 362
      Width = 87
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lblCustom2'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblCustom3: TLabel [3]
      Left = 557
      Top = 362
      Width = 87
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lblCustom3'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 283
      Top = 14
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsLicaVraboteni
      Properties.OnValidate = SifraPropertiesValidate
      StyleDisabled.TextColor = clBackground
      OnExit = SifraExit
      ExplicitLeft = 283
      ExplicitTop = 14
      ExplicitWidth = 137
      Width = 137
    end
    inherited OtkaziButton: TcxButton
      Left = 1181
      Top = 378
      TabOrder = 7
      ExplicitLeft = 1181
      ExplicitTop = 378
    end
    inherited ZapisiButton: TcxButton
      Left = 1100
      Top = 378
      TabOrder = 6
      ExplicitLeft = 1100
      ExplicitTop = 378
    end
    object GroupBox1: TGroupBox
      Left = 22
      Top = 50
      Width = 1271
      Height = 303
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1051#1080#1095#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090
      TabOrder = 2
      DesignSize = (
        1271
        303)
      object Label3: TLabel
        Left = 177
        Top = 65
        Width = 84
        Height = 13
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 177
        Top = 90
        Width = 87
        Height = 13
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 177
        Top = 115
        Width = 29
        Height = 13
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 177
        Top = 137
        Width = 84
        Height = 26
        AutoSize = False
        Caption = #1052#1086#1084#1080#1085#1089#1082#1086' '#1087#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label9: TLabel
        Left = 536
        Top = 129
        Width = 113
        Height = 29
        AutoSize = False
        Caption = #1047#1076#1088#1072#1074#1089#1090#1074#1077#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label10: TLabel
        Left = 536
        Top = 244
        Width = 113
        Height = 13
        AutoSize = False
        Caption = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 536
        Top = 271
        Width = 113
        Height = 13
        AutoSize = False
        Caption = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 536
        Top = 71
        Width = 113
        Height = 28
        AutoSize = False
        Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label2: TLabel
        Left = 536
        Top = 167
        Width = 113
        Height = 29
        AutoSize = False
        Caption = #1058#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label15: TLabel
        Left = 712
        Top = 172
        Width = 113
        Height = 13
        AutoSize = False
        Caption = '%'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 177
        Top = 32
        Width = 87
        Height = 29
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1095#1085#1072' '#1082#1072#1088#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label17: TLabel
        Left = 333
        Top = 38
        Width = 87
        Height = 24
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1089#1086#1096' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label18: TLabel
        Left = 536
        Top = 105
        Width = 87
        Height = 13
        AutoSize = False
        Caption = #1047#1074#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 261
        Top = 62
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object TATKOVO_IME: TcxDBTextEdit
        Left = 261
        Top = 87
        BeepOnEnter = False
        DataBinding.DataField = 'TATKOVO_IME'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 261
        Top = 112
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object MOMINSKO_PREZIME: TcxDBTextEdit
        Left = 261
        Top = 139
        BeepOnEnter = False
        DataBinding.DataField = 'MOMINSKO_PREZIME'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object Pol: TcxDBRadioGroup
        Left = 749
        Top = 29
        TabStop = False
        Caption = #1055#1086#1083
        DataBinding.DataField = 'POL'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1046#1077#1085#1089#1082#1080
            Value = 2
          end
          item
            Caption = #1052#1072#1096#1082#1080
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 44
        Width = 142
      end
      object BracnaSostojba: TcxDBRadioGroup
        Left = 651
        Top = 29
        TabStop = False
        Caption = #1041#1088#1072#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        DataBinding.DataField = 'BRACNA_SOSTOJBA'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1053#1077
            Value = 0
          end
          item
            Caption = #1044#1072
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 44
        Width = 92
      end
      object ZDR_SOSTOJBA: TcxDBMemo
        Left = 651
        Top = 129
        Hint = ' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ZDR_SOSTOJBA'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 12
        OnDblClick = ZDR_SOSTOJBADblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 32
        Width = 591
      end
      object NACIONALNOSTID: TcxDBTextEdit
        Left = 651
        Top = 241
        BeepOnEnter = False
        DataBinding.DataField = 'ID_NACIONALNOST'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 15
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object NACIONALNOST: TcxDBLookupComboBox
        Left = 707
        Top = 241
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_NACIONALNOST'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmOtsustvo.dsNacionalnost
        TabOrder = 16
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 535
      end
      object VEROISPOVED: TcxDBLookupComboBox
        Left = 707
        Top = 268
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_VEROISPOVED'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmOtsustvo.dsVeroispoved
        TabOrder = 18
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 535
      end
      object VEROISPOVEDID: TcxDBTextEdit
        Left = 651
        Top = 268
        BeepOnEnter = False
        DataBinding.DataField = 'ID_VEROISPOVED'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 17
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object Slika: TcxDBImage
        Left = 14
        Top = 35
        Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090
        TabStop = False
        DataBinding.DataField = 'SLIKA'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.FitMode = ifmProportionalStretch
        Properties.GraphicClassName = 'TdxSmartImage'
        Properties.PopupMenuLayout.MenuItems = [pmiCut, pmiCopy, pmiPaste, pmiDelete, pmiLoad, pmiSave, pmiCustom]
        TabOrder = 19
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 118
        Width = 147
      end
      object Button1: TButton
        Left = 27
        Top = 159
        Width = 122
        Height = 25
        Hint = 'Bitmap  image |*.bmp;*.dib'
        Caption = '...'
        TabOrder = 20
        TabStop = False
        OnClick = Button1Click
      end
      object OBRAZOVANIE_naziv: TcxDBLookupComboBox
        Tag = 1
        Left = 707
        Top = 75
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'OBRAZOVANIE'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'OPIS'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsObrazovanie
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 10
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 535
      end
      object OBRAZOVANIE_ID: TcxDBTextEdit
        Tag = 1
        Left = 651
        Top = 75
        BeepOnEnter = False
        DataBinding.DataField = 'OBRAZOVANIE'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object cxDBRadioGroup1: TcxDBRadioGroup
        Left = 651
        Top = 194
        TabStop = False
        Caption = #1058#1088#1091#1076#1086#1074' '#1080#1085#1074#1072#1083#1080#1076
        DataBinding.DataField = 'INVALID'
        DataBinding.DataSource = dm.dsLicaVraboteni
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1053#1077
            Value = 0
          end
          item
            Caption = #1044#1072
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        TabOrder = 14
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 41
        Width = 240
      end
      object gbRadjanje: TcxGroupBox
        Left = 166
        Top = 172
        Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1072#1107#1072#1114#1077
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.NativeStyle = True
        TabOrder = 6
        Height = 118
        Width = 331
        object Label7: TLabel
          Left = 14
          Top = 25
          Width = 84
          Height = 13
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label14: TLabel
          Left = 14
          Top = 48
          Width = 64
          Height = 17
          AutoSize = False
          Caption = #1052#1077#1089#1090#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label8: TLabel
          Left = 14
          Top = 72
          Width = 59
          Height = 19
          AutoSize = False
          Caption = #1054#1087#1096#1090#1080#1085#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label13: TLabel
          Left = 14
          Top = 95
          Width = 64
          Height = 18
          AutoSize = False
          Caption = #1044#1088#1078#1072#1074#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object DATUM_RADJANJE: TcxDBDateEdit
          Tag = 1
          Left = 74
          Top = 22
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM_RADJANJE'
          DataBinding.DataSource = dm.dsLicaVraboteni
          StyleDisabled.Color = clBtnFace
          StyleDisabled.TextColor = clBackground
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 158
        end
        object txtMesto: TcxDBTextEdit
          Tag = 1
          Left = 74
          Top = 44
          BeepOnEnter = False
          DataBinding.DataField = 'MESTO'
          DataBinding.DataSource = dm.dsLicaVraboteni
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 44
        end
        object cbMesto: TcxDBLookupComboBox
          Tag = 1
          Left = 118
          Top = 44
          BeepOnEnter = False
          DataBinding.DataField = 'MESTO'
          DataBinding.DataSource = dm.dsLicaVraboteni
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'ID'
            end
            item
              FieldName = 'Naziv'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dmMat.dsMesto
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 195
        end
        object txtDrzava: TcxTextEdit
          Left = 74
          Top = 92
          Enabled = False
          Properties.ReadOnly = True
          StyleDisabled.BorderColor = clNavy
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clNone
          TabOrder = 4
          Width = 239
        end
        object txtOpstina: TcxTextEdit
          Left = 74
          Top = 69
          Enabled = False
          Properties.ReadOnly = True
          StyleDisabled.BorderColor = clNavy
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clNone
          TabOrder = 3
          Width = 239
        end
      end
      object Telesno: TcxDBTextEdit
        Left = 651
        Top = 167
        Hint = #1058#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090' ('#1074#1086' '#1087#1088#1086#1094#1077#1085#1090#1080')'
        BeepOnEnter = False
        DataBinding.DataField = 'TELESNO_OSTETUVANJE'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 13
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object BrLicnaKarta: TcxDBTextEdit
        Left = 261
        Top = 35
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1095#1085#1072' '#1082#1072#1088#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'LICNA_KARTA'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 68
      end
      object BROJ_PASOS: TcxDBTextEdit
        Left = 421
        Top = 35
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1089#1086#1096
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_PASOS'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 68
      end
      object ZVANJE: TcxDBTextEdit
        Left = 651
        Top = 102
        BeepOnEnter = False
        DataBinding.DataField = 'ZVANJE'
        DataBinding.DataSource = dm.dsLicaVraboteni
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.Color = clBtnFace
        StyleDisabled.TextColor = clBackground
        TabOrder = 11
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 334
      end
    end
    object rgStatus: TcxDBRadioGroup
      Left = 459
      Top = 6
      Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1083#1080#1094#1077' ('#1040#1082#1090#1080#1074#1077#1085'/'#1053#1077#1072#1082#1090#1080#1074#1077#1085')'
      Caption = ' '#1057#1090#1072#1090#1091#1089
      DataBinding.DataField = 'ACTIVEN'
      DataBinding.DataSource = dm.dsLicaVraboteni
      ParentBackground = False
      ParentColor = False
      Properties.Columns = 2
      Properties.DefaultValue = 1
      Properties.Items = <
        item
          Caption = #1040#1082#1090#1080#1074#1077#1085
          Value = 1
        end
        item
          Caption = #1053#1077#1072#1082#1090#1080#1074#1077#1085
          Value = 0
        end>
      Style.Color = clBtnFace
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.TextColor = clRed
      Style.TextStyle = []
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 1
      Height = 43
      Width = 184
    end
    object txtCustom1: TcxDBTextEdit
      Left = 273
      Top = 359
      BeepOnEnter = False
      DataBinding.DataField = 'CUSTOM1'
      DataBinding.DataSource = dm.dsLicaVraboteni
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.Color = clBtnFace
      StyleDisabled.TextColor = clBackground
      TabOrder = 3
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 78
    end
    object txtCustom2: TcxDBTextEdit
      Left = 459
      Top = 359
      BeepOnEnter = False
      DataBinding.DataField = 'CUSTOM2'
      DataBinding.DataSource = dm.dsLicaVraboteni
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.Color = clBtnFace
      StyleDisabled.TextColor = clBackground
      TabOrder = 4
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 78
    end
    object txtCustom3: TcxDBTextEdit
      Left = 650
      Top = 359
      BeepOnEnter = False
      DataBinding.DataField = 'CUSTOM3'
      DataBinding.DataSource = dm.dsLicaVraboteni
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.Color = clBtnFace
      StyleDisabled.TextColor = clBackground
      TabOrder = 5
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 78
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1341
    ExplicitWidth = 1341
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 1341
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', F' +
          '10 - '#1044#1086#1089#1080#1077', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 718
    ExplicitWidth = 1341
  end
  inherited PopupMenu1: TPopupMenu
    object aObrazovanieObuka1: TMenuItem [0]
      Action = aObrazovanieObuka
    end
    object N2: TMenuItem [1]
      Action = aRabotnoIskustvo
    end
    object N3: TMenuItem [2]
      Action = aLVK
    end
    object N4: TMenuItem [3]
      Action = aKontakt
    end
    object N5: TMenuItem [4]
      Action = aVrabRM
    end
    inherited N1: TMenuItem
      Action = aSnimiIzgled
    end
    inherited Excel1: TMenuItem
      Action = aZacuvajExcel
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 432
    Top = 248
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1051#1080#1094#1077
      DockedLeft = 149
      FloatClientWidth = 64
      FloatClientHeight = 156
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 1205
      FloatClientWidth = 111
      FloatClientHeight = 126
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 1283
      FloatClientHeight = 104
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1080' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1086#1089#1090#1072#1085#1072#1090#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1086#1076#1073#1088#1072#1085#1086' '#1083#1080#1094#1077
      CaptionButtons = <>
      DockedLeft = 301
      DockedTop = 0
      FloatLeft = 946
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072
      CaptionButtons = <>
      DockedLeft = 939
      DockedTop = 0
      FloatLeft = 1031
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = #1044#1086#1075#1086#1075#1074#1086#1088' '#1079#1072
      CaptionButtons = <>
      DockedLeft = 705
      DockedTop = 0
      FloatLeft = 1024
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton31'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar [8]
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 650
      DockedTop = 0
      FloatLeft = 1078
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar [9]
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1078
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aObrazovanieObuka
      Category = 0
      LargeImageIndex = 2
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aRabotnoIskustvo
      Category = 0
      SyncImageIndex = False
      ImageIndex = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aLVK
      Category = 0
      LargeImageIndex = 3
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aKontakt
      Category = 0
      LargeImageIndex = 32
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aDosie
      Category = 0
      LargeImageIndex = 8
      SyncImageIndex = False
      ImageIndex = 19
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 27
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aDeca
      Category = 0
      LargeImageIndex = 44
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aVrabRM
      Category = 0
      LargeImageIndex = 16
      SyncImageIndex = False
      ImageIndex = 16
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aDogovorVrabotuvanje
      Category = 0
      SyncImageIndex = False
      ImageIndex = 26
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Tag = 7
      Action = aDogovorZaHonorarci
      Caption = #1044#1077#1083#1086
      Category = 0
      LargeImageIndex = 26
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aDokumenti
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aDokumenti
      Category = 0
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
      ItemLinks = <>
    end
    object dxBarButton2: TdxBarButton
      Action = aDokumenti
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Caption = #1041#1072#1088#1072#1114#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      Category = 0
      Visible = ivAlways
      ImageIndex = 15
      LargeImageIndex = 15
      OnClick = aBaranjeZaGodisenOdmorExecute
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aDokumenti
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Tag = 7
      Action = aSezonskoRabotewe
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aVrabRM
      Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1054#1089#1090#1072#1085#1072#1090#1080' '#1087#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton4: TdxBarButton
      Action = aPovredi
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aNKM
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton7: TdxBarButton
      Action = aDosie
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Action = aDokumenti
      Category = 0
    end
    object dxBarButton9: TdxBarButton
      Action = aVrabRM
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = aSpecijalizacija
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aSpecijalizacija
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = aPodatociLekari
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Tag = 7
      Action = aDogVolonteri
      Category = 0
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1040#1082#1090#1080#1074#1085#1080
      Category = 0
      Hint = #1040#1082#1090#1080#1074#1085#1080
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Items = <>
    end
    object cxBarEditItem3: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077
          Value = '0'
        end
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080
          Value = '1'
        end>
      Properties.OnChange = cxBarEditItem3PropertiesChange
      InternalEditValue = '1'
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aObrazovanieObuka: TAction
      Caption = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1080' '#1086#1073#1091#1082#1072
      OnExecute = aObrazovanieObukaExecute
    end
    object aRabotnoIskustvo: TAction
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086
      ImageIndex = 0
      OnExecute = aRabotnoIskustvoExecute
    end
    object aLVK: TAction
      Caption = #1051#1080#1095#1085#1080' '#1074#1077#1096#1090#1080#1085#1080' '#1080' '#1082#1086#1084#1087#1077#1085#1090#1077#1085#1094#1080#1080
      OnExecute = aLVKExecute
    end
    object aKontakt: TAction
      Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1080' '#1082#1086#1085#1090#1072#1082#1090
      OnExecute = aKontaktExecute
    end
    object aDosie: TAction
      Caption = #1044#1086#1089#1080#1077
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aDosieExecute
    end
    object aVrabRM: TAction
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080' '#1052#1077#1089#1090#1072
      ImageIndex = 19
      ShortCut = 8315
      OnExecute = aVrabRMExecute
    end
    object aDeca: TAction
      Caption = #1044#1077#1094#1072', '#1057#1086#1087#1088#1091#1075'/'#1057#1086#1087#1088#1091#1075#1072
      OnExecute = aDecaExecute
    end
    object aDosieDizajn: TAction
      Caption = 'aDosieDizajn'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDosieDizajnExecute
    end
    object aDogovorVrabotuvanje: TAction
      Caption = #1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      ImageIndex = 26
      OnExecute = aDogovorVrabotuvanjeExecute
    end
    object aDogovorZaHonorarci: TAction
      Tag = 7
      Caption = #1061#1086#1085#1086#1088#1072#1088#1085#1072' '#1089#1086#1088#1072#1073#1086#1090#1082#1072
      OnExecute = aDogovorZaHonorarciExecute
    end
    object aDokumenti: TAction
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1080
      ImageIndex = 19
      OnExecute = aDokumentiExecute
    end
    object aSezonskoRabotewe: TAction
      AutoCheck = True
      Caption = #1057#1077#1079#1086#1085#1089#1082#1086' '#1088#1072#1073#1086#1090#1077#1114#1077
      ImageIndex = 26
      OnExecute = aSezonskoRaboteweExecute
    end
    object aPovredi: TAction
      Caption = #1055#1086#1074#1088#1077#1076#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      ImageIndex = 19
      OnExecute = aPovrediExecute
    end
    object aNKM: TAction
      Caption = #1050#1072#1079#1085#1080', '#1053#1072#1075#1088#1072#1076#1080' '#1080'  '#1044#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1080' '#1084#1077#1088#1082#1080
      ImageIndex = 19
      OnExecute = aNKMExecute
    end
    object aSpecijalizacija: TAction
      Caption = #1057#1087#1077#1094#1080#1112#1072#1083#1080#1079#1072#1094#1080#1112#1072
      ImageIndex = 26
      OnExecute = aSpecijalizacijaExecute
    end
    object aPodatociLekari: TAction
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1083#1077#1082#1072#1088#1080
      ImageIndex = 33
      OnExecute = aPodatociLekariExecute
    end
    object aDogVolonteri: TAction
      Caption = #1042#1086#1083#1086#1085#1090#1077#1088#1080
      ImageIndex = 26
      OnExecute = aDogVolonteriExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40893.564157071760000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 616
    Top = 264
    PixelsPerInch = 96
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 120
    Top = 328
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 872
    Top = 184
  end
  object qEditPLTVraboteni: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        ' insert into plt_vraboteni (mb,firma,prezime,tatkovo_ime,ime, me' +
        'sto,re_id)'
      ' values (:mb,:firma ,:prezime,:tatkovo_ime,:ime,:mesto,:re_id)')
    Left = 560
    Top = 200
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qRERM: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select hrr.id_re re_id'
      'from hr_rm_re hrr'
      'where hrr.id=:re_id')
    Left = 632
    Top = 200
    qoStartTransaction = True
  end
end
