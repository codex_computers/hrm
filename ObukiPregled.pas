unit ObukiPregled;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxGroupBox, cxRadioGroup, cxImage, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxMemo, ExtDlgs, FIBDataSet, pFIBDataSet,
  cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxCustomHint;

type
  TfrmObukiPregled = class(TForm)
    Panel1: TPanel;
    buttonZapisi: TcxButton;
    ActionList1: TActionList;
    aPregled: TAction;
    aIzlez: TAction;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    OD_DATA: TcxDateEdit;
    DO_DATA: TcxDateEdit;
    cxButton1: TcxButton;
    aIscistiDatum: TAction;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    cxButton2: TcxButton;
    aIscistiSttus: TAction;
    STATUS: TcxLookupComboBox;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    dsObukiVraboteni: TDataSource;
    tblObukiVraboteni: TpFIBDataSet;
    tblObukiVraboteniID: TFIBSmallIntField;
    tblObukiVraboteniOBUKA_ID: TFIBIntegerField;
    tblObukiVraboteniMB: TFIBStringField;
    tblObukiVraboteniOCENKA: TFIBStringField;
    tblObukiVraboteniSERTIFIKATI: TFIBStringField;
    tblObukiVraboteniTS_INS: TFIBDateTimeField;
    tblObukiVraboteniTS_UPD: TFIBDateTimeField;
    tblObukiVraboteniUSR_INS: TFIBStringField;
    tblObukiVraboteniUSR_UPD: TFIBStringField;
    tblObukiVraboteniOBUKANAZIV: TFIBStringField;
    tblObukiVraboteniSTATUSNAZIV: TFIBStringField;
    tblObukiVraboteniPERIOD_OD: TFIBDateField;
    tblObukiVraboteniPERIOD_DO: TFIBDateField;
    tblObukiVraboteniSTATUS: TFIBIntegerField;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1OCENKA: TcxGridDBColumn;
    cxGrid1DBTableView1SERTIFIKATI: TcxGridDBColumn;
    cxGrid1DBTableView1OBUKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD_OD: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD_DO: TcxGridDBColumn;
    ActionList2: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    Action1: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    aDizajnReport: TAction;
    cxHintStyleController1: TcxHintStyleController;
    tblObukiVraboteniNAZIV_VRABOTEN: TFIBStringField;
    tblObukiVraboteniNAZIV_VRABOTEN_TI: TFIBStringField;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    procedure aPregledExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aIscistiDatumExecute(Sender: TObject);
    procedure aIscistiSttusExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aDizajnReportExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmObukiPregled: TfrmObukiPregled;

implementation

uses dmUnit, dmMaticni, dmResources, dmUnitOtsustvo, dmKonekcija, dmReportUnit,
  Utils;

{$R *.dfm}

procedure TfrmObukiPregled.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmObukiPregled.aDizajnReportExecute(Sender: TObject);
  var RptStream :TStream;
begin
          if (OD_DATA.Text <> '') and (DO_DATA.Text <>'')then
        begin

           dmReport.tblReportDizajn2.Close;
           dmReport.tblReportDizajn2.ParamByName('br').Value:=30078;
           dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
           dmReport.tblReportDizajn2.Open;

           dm.tblObukiVraboteniPeriod.Close;
           dm.tblObukiVraboteniPeriod.ParamByName('obukaID').Value:='%';
           dm.tblObukiVraboteniPeriod.ParamByName('OD').Value:=OD_DATA.Date;
           dm.tblObukiVraboteniPeriod.ParamByName('DO').Value:=DO_DATA.Date;
           if STATUS.Text <> '' then
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:=STATUS.EditValue
           else
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:='%';
           dm.tblObukiVraboteniPeriod.Open;

           RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
           dmReport.frxReport1.LoadFromStream(RptStream) ;

           dmReport.frxReport1.DesignReport();
//         dmReport.frxReport1.ShowReport;
       end
     else
     if (OD_DATA.Text = '') and (DO_DATA.Text ='')then
            begin
              dm.tblObukiVraboteni.Close;
              dm.tblObukiVraboteni.ParamByName('obukaID').Value:='%';
              if STATUS.Text <> '' then
                 dm.tblObukiVraboteni.ParamByName('statusID').Value:=STATUS.EditValue
              else
                 dm.tblObukiVraboteni.ParamByName('statusID').Value:='%';
              dm.tblObukiVraboteni.Open;

              dmReport.tblReportDizajn2.Close;
              dmReport.tblReportDizajn2.ParamByName('br').Value:=30076;
              dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
              dmReport.tblReportDizajn2.Open;

              RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream) ;

              dmReport.frxReport1.DesignReport();
//              dmReport.frxReport1.ShowReport;
            end
     else
     if (OD_DATA.Text = '') and (DO_DATA.Text ='')then
        begin
          dm.tblObukiVraboteni.Close;
          dm.tblObukiVraboteni.ParamByName('obukaID').Value:='%';
          if STATUS.Text <> '' then
             dm.tblObukiVraboteni.ParamByName('statusID').Value:=STATUS.EditValue
          else
             dm.tblObukiVraboteni.ParamByName('statusID').Value:='%';
          dm.tblObukiVraboteni.Open;

          dmReport.tblReportDizajn2.Close;
          dmReport.tblReportDizajn2.ParamByName('br').Value:=30077;
          dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
          dmReport.tblReportDizajn2.Open;

          RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream) ;

         dmReport.frxReport1.DesignReport();
//       dmReport.frxReport1.ShowReport;
       end
     else
     if (OD_DATA.Text <> '') and (DO_DATA.Text <>'')then
        begin
          dm.tblObukiVraboteniPeriod.Close;
          dm.tblObukiVraboteniPeriod.ParamByName('obukaID').Value:='%';
          dm.tblObukiVraboteniPeriod.ParamByName('OD').Value:=OD_DATA.Date;
          dm.tblObukiVraboteniPeriod.ParamByName('DO').Value:=DO_DATA.Date;
          if STATUS.Text <> '' then
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:=STATUS.EditValue
           else
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:='%';
          dm.tblObukiVraboteniPeriod.Open;

          dmReport.tblReportDizajn2.Close;
          dmReport.tblReportDizajn2.ParamByName('br').Value:=30079;
          dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
          dmReport.tblReportDizajn2.Open;

          RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream) ;

         dmReport.frxReport1.DesignReport();
//       dmReport.frxReport1.ShowReport;
       end

end;

procedure TfrmObukiPregled.aIscistiDatumExecute(Sender: TObject);
begin
     OD_DATA.Text:='';
     DO_DATA.Text:='';
end;

procedure TfrmObukiPregled.aIscistiSttusExecute(Sender: TObject);
begin
     STATUS.Text:='';
end;

procedure TfrmObukiPregled.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmObukiPregled.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmObukiPregled.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmObukiPregled.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmObukiPregled.aPregledExecute(Sender: TObject);
  var RptStream :TStream;
begin
     if (OD_DATA.Text <> '') and (DO_DATA.Text <>'')then
        begin

           dmReport.tblReportDizajn2.Close;
           dmReport.tblReportDizajn2.ParamByName('br').Value:=30078;
           dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
           dmReport.tblReportDizajn2.Open;

           dm.tblObukiVraboteniPeriod.Close;
           dm.tblObukiVraboteniPeriod.ParamByName('obukaID').Value:='%';
           dm.tblObukiVraboteniPeriod.ParamByName('OD').Value:=OD_DATA.Date;
           dm.tblObukiVraboteniPeriod.ParamByName('DO').Value:=DO_DATA.Date;
           if STATUS.Text <> '' then
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:=STATUS.EditValue
           else
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:='%';
           dm.tblObukiVraboteniPeriod.Open;

           RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
           dmReport.frxReport1.LoadFromStream(RptStream) ;

//           dmReport.frxReport1.DesignReport();
         dmReport.frxReport1.ShowReport;
       end
     else
     if (OD_DATA.Text = '') and (DO_DATA.Text ='')then
            begin
              dm.tblObukiVraboteni.Close;
              dm.tblObukiVraboteni.ParamByName('obukaID').Value:='%';
              if STATUS.Text <> '' then
                 dm.tblObukiVraboteni.ParamByName('statusID').Value:=STATUS.EditValue
              else
                 dm.tblObukiVraboteni.ParamByName('statusID').Value:='%';
              dm.tblObukiVraboteni.Open;

              dmReport.tblReportDizajn2.Close;
              dmReport.tblReportDizajn2.ParamByName('br').Value:=30076;
              dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
              dmReport.tblReportDizajn2.Open;

              RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream) ;

//              dmReport.frxReport1.DesignReport();
              dmReport.frxReport1.ShowReport;
            end
     else
     if (OD_DATA.Text = '') and (DO_DATA.Text ='')then
        begin
          dm.tblObukiVraboteni.Close;
          dm.tblObukiVraboteni.ParamByName('obukaID').Value:='%';
          if STATUS.Text <> '' then
             dm.tblObukiVraboteni.ParamByName('statusID').Value:=STATUS.EditValue
          else
             dm.tblObukiVraboteni.ParamByName('statusID').Value:='%';
          dm.tblObukiVraboteni.Open;

          dmReport.tblReportDizajn2.Close;
          dmReport.tblReportDizajn2.ParamByName('br').Value:=30077;
          dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
          dmReport.tblReportDizajn2.Open;

          RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream) ;

//         dmReport.frxReport1.DesignReport();
       dmReport.frxReport1.ShowReport;
       end
     else
     if (OD_DATA.Text <> '') and (DO_DATA.Text <>'')then
        begin
          dm.tblObukiVraboteniPeriod.Close;
          dm.tblObukiVraboteniPeriod.ParamByName('obukaID').Value:='%';
          dm.tblObukiVraboteniPeriod.ParamByName('OD').Value:=OD_DATA.Date;
          dm.tblObukiVraboteniPeriod.ParamByName('DO').Value:=DO_DATA.Date;
          if STATUS.Text <> '' then
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:=STATUS.EditValue
           else
              dm.tblObukiVraboteniPeriod.ParamByName('status').Value:='%';
          dm.tblObukiVraboteniPeriod.Open;

          dmReport.tblReportDizajn2.Close;
          dmReport.tblReportDizajn2.ParamByName('br').Value:=30079;
          dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
          dmReport.tblReportDizajn2.Open;

          RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream) ;

//         dmReport.frxReport1.DesignReport();
       dmReport.frxReport1.ShowReport;
       end

end;

procedure TfrmObukiPregled.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmObukiPregled.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmObukiPregled.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmObukiPregled.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmObukiPregled.FormCreate(Sender: TObject);
begin

     dmMat.tblStatusGrupa.Open;
     dmMat.tblStatusGrupa.Locate('APP; TABELA', VarArrayOf([dmkon.aplikacija, 'HR_OBUKI']), []);
     StatGrupaObuki:= dmMat.tblStatusGrupaID.Value;
//     dmMat.tblStatusGrupa.Close;

     dmOtsustvo.tblStatus.Close;
     dmOtsustvo.tblStatus.ParamByName('GRUPA').Value:=StatGrupaObuki;
     dmOtsustvo.tblStatus.Open;

     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.Open;

     tblObukiVraboteni.Close;
     tblObukiVraboteni.ParamByName('obukaID').Value:='%';
     tblObukiVraboteni.ParamByName('statusID').Value:='%';
     tblObukiVraboteni.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmObukiPregled.FormShow(Sender: TObject);
begin
     dxBarManager1Bar1.Caption := Caption;
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

end.
