object frmDecaSopruznici: TfrmDecaSopruznici
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1076#1077#1094#1072' '#1080' '#1089#1086#1087#1088#1091#1078#1085#1080#1094#1080
  ClientHeight = 738
  ClientWidth = 783
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 783
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 715
    Width = 783
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 783
    Height = 589
    Align = alClient
    TabOrder = 6
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 589
    ClientRectRight = 783
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1076#1077#1094#1072
      ImageIndex = 0
      object dPanel: TPanel
        Left = 0
        Top = 247
        Width = 783
        Height = 318
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvSpace
        Color = 15790320
        Enabled = False
        ParentBackground = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        DesignSize = (
          783
          318)
        object Label1: TLabel
          Left = 571
          Top = 30
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label11: TLabel
          Left = 31
          Top = 17
          Width = 132
          Height = 30
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Sifra: TcxDBTextEdit
          Tag = 1
          Left = 627
          Top = 27
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsDeca
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 4
          Visible = False
          Width = 80
        end
        object OtkaziButton: TcxButton
          Left = 682
          Top = 278
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 5
        end
        object ZapisiButton: TcxButton
          Left = 601
          Top = 278
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 3
        end
        object VRABOTENNAZIV: TcxDBLookupComboBox
          Tag = 1
          Left = 266
          Top = 22
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
          BeepOnEnter = False
          DataBinding.DataField = 'RODITEL'
          DataBinding.DataSource = dm.dsDeca
          ParentFont = False
          ParentShowHint = False
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'MB'
          Properties.ListColumns = <
            item
              Width = 500
              FieldName = 'MB'
            end
            item
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
              Width = 800
              FieldName = 'VRABOTENNAZIV'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsLica
          ShowHint = True
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 278
        end
        object MB_VRABOTEN: TcxDBTextEdit
          Tag = 1
          Left = 169
          Top = 22
          Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
          BeepOnEnter = False
          DataBinding.DataField = 'RODITEL'
          DataBinding.DataSource = dm.dsDeca
          ParentFont = False
          ParentShowHint = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          ShowHint = True
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 96
        end
        object cxGroupBox1: TcxGroupBox
          Left = 31
          Top = 49
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1076#1077#1090#1077
          TabOrder = 2
          Height = 217
          Width = 538
          object Label4: TLabel
            Left = 39
            Top = 29
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 39
            Top = 58
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1088#1077#1079#1080#1084#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 39
            Top = 80
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label6: TLabel
            Left = 39
            Top = 107
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1048#1084#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 271
            Top = 137
            Width = 53
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1042#1086#1079#1088#1072#1089#1090' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 18
            Top = 137
            Width = 114
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object MB: TcxDBTextEdit
            Left = 138
            Top = 26
            Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1090#1077
            BeepOnEnter = False
            DataBinding.DataField = 'MAT_BR'
            DataBinding.DataSource = dm.dsDeca
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Properties.OnEditValueChanged = MBPropertiesEditValueChanged
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 96
          end
          object Prezime: TcxDBTextEdit
            Left = 138
            Top = 53
            Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1076#1077#1090#1077
            BeepOnEnter = False
            DataBinding.DataField = 'PRZIME'
            DataBinding.DataSource = dm.dsDeca
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 375
          end
          object tatkovo_ime: TcxDBTextEdit
            Left = 138
            Top = 80
            Hint = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1085#1072' '#1076#1077#1090#1077
            BeepOnEnter = False
            DataBinding.DataField = 'TATKOVO_IME'
            DataBinding.DataSource = dm.dsDeca
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 375
          end
          object ime: TcxDBTextEdit
            Left = 138
            Top = 107
            Hint = #1048#1084#1077' '#1085#1072' '#1076#1077#1090#1077
            BeepOnEnter = False
            DataBinding.DataField = 'IME'
            DataBinding.DataSource = dm.dsDeca
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 375
          end
          object vozrast: TcxDBTextEdit
            Left = 330
            Top = 134
            Hint = #1042#1086#1079#1088#1072#1089#1090' '#1085#1072' '#1076#1077#1090#1077
            BeepOnEnter = False
            DataBinding.DataField = 'vozrast'
            DataBinding.DataSource = dm.dsDeca
            Enabled = False
            ParentFont = False
            ParentShowHint = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            ShowHint = True
            Style.Shadow = False
            StyleDisabled.TextColor = clDefault
            TabOrder = 5
            Width = 47
          end
          object DATUM_RADJANJE: TcxDBDateEdit
            Left = 138
            Top = 134
            Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' '#1085#1072' '#1076#1077#1090#1077
            BeepOnEnter = False
            DataBinding.DataField = 'DATA_RAGJANJE'
            DataBinding.DataSource = dm.dsDeca
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 127
          end
          object Pol: TcxDBRadioGroup
            Left = 138
            Top = 161
            Hint = #1055#1086#1083' '#1085#1072' '#1076#1077#1090#1077
            TabStop = False
            Caption = #1055#1086#1083' '#1085#1072' '#1076#1077#1090#1077
            DataBinding.DataField = 'POL'
            DataBinding.DataSource = dm.dsDeca
            Properties.Columns = 2
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = #1046#1077#1085#1089#1082#1080
                Value = 2
              end
              item
                Caption = #1052#1072#1096#1082#1080
                Value = 1
              end>
            Style.LookAndFeel.Kind = lfUltraFlat
            Style.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.Kind = lfUltraFlat
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.Kind = lfUltraFlat
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.Kind = lfUltraFlat
            StyleHot.LookAndFeel.NativeStyle = False
            TabOrder = 6
            Height = 43
            Width = 143
          end
          object cxDBRadioGroup1: TcxDBRadioGroup
            Left = 306
            Top = 161
            TabStop = False
            Caption = #1054#1089#1080#1075#1091#1088#1077#1085#1080#1082
            DataBinding.DataField = 'OSIGURENIK'
            DataBinding.DataSource = dm.dsDeca
            Properties.Columns = 2
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = #1044#1072
                Value = 1
              end
              item
                Caption = #1053#1077
                Value = 0
              end>
            TabOrder = 7
            Height = 43
            Width = 136
          end
        end
      end
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 783
        Height = 247
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 1
        object cxGrid1: TcxGrid
          Left = 2
          Top = 2
          Width = 779
          Height = 243
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            DataController.DataSource = dm.dsDeca
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1RODITEL: TcxGridDBColumn
              DataBinding.FieldName = 'RODITEL'
            end
            object cxGrid1DBTableView1RODITELNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
              Width = 139
            end
            object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN'
              Visible = False
              Width = 250
            end
            object cxGrid1DBTableView1MAT_BR: TcxGridDBColumn
              DataBinding.FieldName = 'MAT_BR'
              Width = 100
            end
            object cxGrid1DBTableView1PRZIME: TcxGridDBColumn
              DataBinding.FieldName = 'PRZIME'
              Width = 100
            end
            object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
              DataBinding.FieldName = 'TATKOVO_IME'
              Width = 100
            end
            object cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn
              Caption = #1048#1084#1077' '#1085#1072' '#1076#1077#1090#1077
              DataBinding.FieldName = 'IME'
              Width = 139
            end
            object cxGrid1DBTableView1DATA_RAGJANJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATA_RAGJANJE'
              Width = 117
            end
            object cxGrid1DBTableView1POL: TcxGridDBColumn
              DataBinding.FieldName = 'PolNaziv'
              Width = 67
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              DataBinding.FieldName = 'vozrast'
            end
            object cxGrid1DBTableView1OSIGURENIK: TcxGridDBColumn
              DataBinding.FieldName = 'OSIGURENIK'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1OsigurenikDaNe: TcxGridDBColumn
              DataBinding.FieldName = 'OsigurenikDaNe'
              Width = 100
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1089#1086#1087#1088#1091#1075'/'#1089#1086#1087#1088#1091#1075#1072
      ImageIndex = 1
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 247
        Width = 783
        Height = 318
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvSpace
        Color = 15790320
        Enabled = False
        ParentBackground = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        DesignSize = (
          783
          318)
        object Label8: TLabel
          Left = 571
          Top = 30
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label9: TLabel
          Left = 35
          Top = 17
          Width = 128
          Height = 32
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object cxDBTextEdit1: TcxDBTextEdit
          Tag = 1
          Left = 627
          Top = 27
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsSopruznik
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 4
          Visible = False
          Width = 80
        end
        object cxButton1: TcxButton
          Left = 682
          Top = 278
          Width = 75
          Height = 25
          Action = aOtkaziSopruznik
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 5
        end
        object cxButton2: TcxButton
          Left = 601
          Top = 278
          Width = 75
          Height = 25
          Action = aZapisiSopruznik
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 3
        end
        object cxDBLookupComboBox1: TcxDBLookupComboBox
          Tag = 1
          Left = 266
          Top = 22
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
          BeepOnEnter = False
          DataBinding.DataField = 'RODITEL'
          DataBinding.DataSource = dm.dsSopruznik
          ParentFont = False
          ParentShowHint = False
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'MB'
          Properties.ListColumns = <
            item
              Width = 500
              FieldName = 'MB'
            end
            item
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
              Width = 800
              FieldName = 'VRABOTENNAZIV'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsLica
          ShowHint = True
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 278
        end
        object VMB: TcxDBTextEdit
          Tag = 1
          Left = 169
          Top = 22
          Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
          BeepOnEnter = False
          DataBinding.DataField = 'RODITEL'
          DataBinding.DataSource = dm.dsSopruznik
          ParentFont = False
          ParentShowHint = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          ShowHint = True
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 96
        end
        object cxGroupBox2: TcxGroupBox
          Left = 35
          Top = 65
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1089#1086#1087#1088#1091#1075'/'#1089#1086#1087#1088#1091#1075#1072
          TabOrder = 2
          Height = 176
          Width = 538
          object Label10: TLabel
            Left = 39
            Top = 80
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 39
            Top = 109
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1088#1077#1079#1080#1084#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label14: TLabel
            Left = 39
            Top = 131
            Width = 93
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1048#1084#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SMB: TcxDBTextEdit
            Left = 138
            Top = 77
            Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
            BeepOnEnter = False
            DataBinding.DataField = 'MAT_BR'
            DataBinding.DataSource = dm.dsSopruznik
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Properties.OnEditValueChanged = MBPropertiesEditValueChanged
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 96
          end
          object cxDBTextEdit4: TcxDBTextEdit
            Left = 138
            Top = 104
            Hint = #1055#1088#1077#1079#1080#1084#1077
            BeepOnEnter = False
            DataBinding.DataField = 'PRZIME'
            DataBinding.DataSource = dm.dsSopruznik
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 375
          end
          object cxDBTextEdit6: TcxDBTextEdit
            Left = 138
            Top = 131
            Hint = #1048#1084#1077' '
            BeepOnEnter = False
            DataBinding.DataField = 'IME'
            DataBinding.DataSource = dm.dsSopruznik
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 375
          end
          object cxDBRadioGroup4: TcxDBRadioGroup
            Left = 138
            Top = 28
            Hint = #1057#1088#1086#1076#1089#1090#1074#1086
            TabStop = False
            Caption = #1057#1088#1086#1076#1089#1090#1074#1086
            DataBinding.DataField = 'SRODSTVO'
            DataBinding.DataSource = dm.dsSopruznik
            Properties.Columns = 2
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Caption = #1057#1086#1087#1088#1091#1075#1072
                Value = 1
              end
              item
                Caption = #1057#1086#1087#1088#1091#1075
                Value = 2
              end>
            Style.LookAndFeel.Kind = lfUltraFlat
            Style.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.Kind = lfUltraFlat
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.Kind = lfUltraFlat
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.Kind = lfUltraFlat
            StyleHot.LookAndFeel.NativeStyle = False
            TabOrder = 3
            Height = 43
            Width = 143
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 783
        Height = 247
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 1
        object cxGrid2: TcxGrid
          Left = 2
          Top = 2
          Width = 779
          Height = 243
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGridDBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            DataController.DataSource = dm.dsSopruznik
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGridDBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Width = 100
            end
            object cxGridDBTableView1RODITEL: TcxGridDBColumn
              Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
              DataBinding.FieldName = 'RODITEL'
              Width = 143
            end
            object cxGridDBTableView1RODITELNAZIV: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
              Width = 110
            end
            object cxGridDBTableView1Column1: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
              DataBinding.FieldName = 'NAZIV_VRABOTEN'
              Visible = False
              Width = 250
            end
            object cxGridDBTableView1Sopruznik: TcxGridDBColumn
              DataBinding.FieldName = 'Sopruznik'
              Width = 100
            end
            object cxGridDBTableView1MAT_BR: TcxGridDBColumn
              DataBinding.FieldName = 'MAT_BR'
              Width = 100
            end
            object cxGridDBTableView1IME: TcxGridDBColumn
              DataBinding.FieldName = 'IME'
              Width = 100
            end
            object cxGridDBTableView1PRZIME: TcxGridDBColumn
              DataBinding.FieldName = 'PRZIME'
              Width = 100
            end
            object cxGridDBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 100
            end
            object cxGridDBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 100
            end
            object cxGridDBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 100
            end
            object cxGridDBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 100
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridDBTableView1
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 256
    Top = 280
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 256
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 456
    Top = 264
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 382
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 256
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aZapisiSopruznik: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiSopruznikExecute
    end
    object aOtkaziSopruznik: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziSopruznikExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40893.556900011570000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository2
      Styles.StyleSheet = dxGridReportLinkStyleSheet2
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40893.556900046290000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 544
    Top = 48
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 368
    Top = 320
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle14
      Styles.Caption = cxStyle15
      Styles.CardCaptionRow = cxStyle16
      Styles.CardRowCaption = cxStyle17
      Styles.Content = cxStyle18
      Styles.ContentEven = cxStyle19
      Styles.ContentOdd = cxStyle20
      Styles.FilterBar = cxStyle21
      Styles.Footer = cxStyle22
      Styles.Group = cxStyle23
      Styles.Header = cxStyle24
      Styles.Preview = cxStyle25
      Styles.Selection = cxStyle26
      BuiltIn = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 704
    Top = 136
  end
end
