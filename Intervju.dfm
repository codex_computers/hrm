object frmIntervju: TfrmIntervju
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1048#1085#1090#1077#1088#1074#1112#1091
  ClientHeight = 699
  ClientWidth = 1024
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1024
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab3: TdxRibbonTab
      Caption = #1057#1090#1088#1091#1082#1090#1091#1088#1080#1088#1072#1085#1086' '#1080#1085#1090'.'
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Visible = False
      Index = 1
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 2
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 676
    Width = 1024
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 1024
    Height = 211
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 1020
      Height = 207
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dmOtsustvo.dsIntervju
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MOLBA'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_MOLBA'
          Width = 80
        end
        object cxGrid1DBTableView1MOLBANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MOLBANAZIV'
          Width = 124
        end
        object cxGrid1DBTableView1ID_RM_RE_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE_MOLBA'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_RM_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RM_MOLBA'
          Width = 143
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 93
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RM'
          Visible = False
          Width = 169
        end
        object cxGrid1DBTableView1MB_INTERVJUIST: TcxGridDBColumn
          DataBinding.FieldName = 'MB_INTERVJUIST'
          Width = 151
        end
        object cxGrid1DBTableView1INTERVJUISTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'INTERVJUISTNAZIV'
          Width = 121
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 75
        end
        object cxGrid1DBTableView1ID_BROJ_UCESNICI: TcxGridDBColumn
          DataBinding.FieldName = 'ID_BROJ_UCESNICI'
          Visible = False
        end
        object cxGrid1DBTableView1BRUCESNICINAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'BRUCESNICINAZIV'
          Width = 77
        end
        object cxGrid1DBTableView1ID_SELEKCIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_SELEKCIJA'
          Visible = False
        end
        object cxGrid1DBTableView1SELEKCIJANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'SELEKCIJANAZIV'
          Width = 112
        end
        object cxGrid1DBTableView1ID_OBLIK: TcxGridDBColumn
          DataBinding.FieldName = 'ID_OBLIK'
          Visible = False
        end
        object cxGrid1DBTableView1OBLIKNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OBLIKNAZIV'
          Width = 104
        end
        object cxGrid1DBTableView1OPIS_POCETOK: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS_POCETOK'
          Width = 94
        end
        object cxGrid1DBTableView1OPIS_SREDINA: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS_SREDINA'
          Width = 99
        end
        object cxGrid1DBTableView1OPIS_KRAJ: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS_KRAJ'
          Width = 99
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 337
    Width = 1024
    Height = 148
    Align = alBottom
    Enabled = False
    ParentBackground = False
    TabOrder = 3
    DesignSize = (
      1024
      148)
    object Label1: TLabel
      Left = 461
      Top = -2
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1064#1080#1092#1088#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel
      Left = 1
      Top = 16
      Width = 98
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1086#1083#1073#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 0
      Top = 40
      Width = 98
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Sifra: TcxDBTextEdit
      Tag = 1
      Left = 517
      Top = -5
      BeepOnEnter = False
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsIntervju
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 7
      Visible = False
      Width = 80
    end
    object ID_MOLBANaziv: TcxDBLookupComboBox
      Tag = 1
      Left = 206
      Top = 13
      Hint = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' '#1085#1072' '#1083#1080#1094#1077#1090#1086' '#1082#1086#1077' '#1112#1072' '#1076#1086#1089#1090#1072#1074#1080#1083#1086' '#1084#1086#1083#1073#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ_MOLBA'
      DataBinding.DataSource = dmOtsustvo.dsIntervju
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MOLBABROJ'
      Properties.ListColumns = <
        item
          FieldName = 'IMEPREZIME'
        end>
      Properties.ListSource = dmSis.dsMolba
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 241
    end
    object ID_MOLBA: TcxDBTextEdit
      Tag = 1
      Left = 106
      Top = 13
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1086#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ_MOLBA'
      DataBinding.DataSource = dmOtsustvo.dsIntervju
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 100
    end
    object ZapisiButton: TcxButton
      Left = 811
      Top = 109
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 6
      OnKeyDown = EnterKakoTab
    end
    object OtkaziButton: TcxButton
      Left = 892
      Top = 109
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 8
      OnKeyDown = EnterKakoTab
    end
    object rgTip: TcxDBRadioGroup
      Left = 486
      Top = 86
      Hint = #1048#1079#1073#1077#1088#1077#1090#1077' '#1058#1080#1087' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091
      Anchors = [akRight, akBottom]
      Caption = ' '#1058#1080#1087' '#1080#1085#1090#1077#1088#1074#1112#1091' '
      DataBinding.DataField = 'ID_OBLIK'
      DataBinding.DataSource = dmOtsustvo.dsIntervju
      ParentFont = False
      Properties.Columns = 2
      Properties.DefaultValue = 0
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = ' '#1053#1077#1089#1090#1088#1091#1082#1090#1091#1088#1080#1088#1072#1085#1086
          Value = 0
        end
        item
          Caption = ' '#1057#1090#1088#1091#1082#1090#1091#1088#1080#1088#1072#1085#1086
          Value = 1
        end>
      Style.BorderStyle = ebsUltraFlat
      StyleDisabled.TextColor = clBlue
      StyleDisabled.TextStyle = []
      TabOrder = 5
      Height = 47
      Width = 243
    end
    object cxGroupBox1: TcxGroupBox
      Left = 14
      Top = 64
      Caption = ' '#1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 3
      Height = 73
      Width = 451
      object Label5: TLabel
        Left = -13
        Top = 20
        Width = 98
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1085#1090#1077#1088#1074#1112#1091#1080#1089#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = -13
        Top = 48
        Width = 98
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MB_INTERVJUIST: TcxDBTextEdit
        Tag = 1
        Left = 91
        Top = 21
        Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1080#1089#1090#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'MB_INTERVJUIST'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 100
      end
      object MB_INTERVJUISTNaziv: TcxDBLookupComboBox
        Tag = 1
        Left = 192
        Top = 21
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1080#1089#1090#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'MB_INTERVJUIST'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'MB'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV_VRABOTEN_TI'
          end>
        Properties.ListSource = dm.dsViewVraboteni
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 241
      end
      object DATUM: TcxDBDateEdit
        Tag = 1
        Left = 91
        Top = 45
        Hint = #1044#1072#1090#1091#1084' '#1082#1086#1075#1072' '#1077' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086
        DataBinding.DataField = 'DATUM'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 101
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 486
      Top = 8
      Anchors = [akRight, akBottom]
      Caption = ' '#1042#1080#1076' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091' '
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 4
      DesignSize = (
        515
        72)
      Height = 72
      Width = 515
      object Label10: TLabel
        Left = 10
        Top = 20
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1091#1095#1077#1089#1085#1080#1094#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 19
        Top = 45
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1057#1077#1083#1077#1082#1094#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ID_BROJ_UCESNICI: TcxDBTextEdit
        Left = 125
        Top = 17
        Hint = 
          #1064#1080#1092#1088#1072' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086' '#1087#1086' '#1073#1088#1086#1112' '#1085#1072' '#1091#1095#1077#1089#1085#1080#1094#1080' ('#1080#1085#1076#1080#1074#1080#1076#1091#1072#1083#1085#1086', '#1089#1077#1082#1074#1077#1085#1094#1080#1112 +
          #1072#1083#1085#1086', '#1075#1088#1091#1087#1085#1086'...)'
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_BROJ_UCESNICI'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object ID_BROJ_UCESNICINaziv: TcxDBLookupComboBox
        Left = 180
        Top = 17
        Hint = 
          #1053#1072#1079#1080#1074' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086' '#1087#1086' '#1073#1088#1086#1112' '#1085#1072' '#1091#1095#1077#1089#1085#1080#1094#1080' ('#1080#1085#1076#1080#1074#1080#1076#1091#1072#1083#1085#1086', '#1089#1077#1082#1074#1077#1085#1094#1080#1112 +
          #1072#1083#1085#1086', '#1075#1088#1091#1087#1085#1086'...)'
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_BROJ_UCESNICI'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmOtsustvo.dsIntBrUcesnici
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 301
      end
      object ID_SELEKCIJANaZIV: TcxDBLookupComboBox
        Left = 180
        Top = 42
        Hint = 
          #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1080#1076#1086#1090' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086' '#1089#1087#1086#1088#1077#1076' '#1091#1083#1086#1075#1072#1090#1072' '#1074#1086' '#1087#1088#1086#1094#1077#1089#1086#1090' '#1085#1072' '#1089#1077#1083#1077#1082 +
          #1094#1080#1112#1072' ('#1087#1088#1077#1083#1080#1084#1080#1085#1072#1088#1085#1086', '#1076#1080#1112#1072#1075#1085#1086#1089#1090#1080#1095#1082#1086', '#1087#1088#1080#1084#1072#1088#1085#1086')'
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_SELEKCIJA'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmOtsustvo.dsIntSelekcija
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 301
      end
      object ID_SELEKCIJA: TcxDBTextEdit
        Left = 125
        Top = 42
        Hint = 
          #1064#1080#1092#1088#1072' '#1085#1072' '#1074#1080#1076#1086#1090' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086' '#1089#1087#1086#1088#1077#1076' '#1091#1083#1086#1075#1072#1090#1072' '#1074#1086' '#1087#1088#1086#1094#1077#1089#1086#1090' '#1085#1072' '#1089#1077#1083#1077#1082 +
          #1094#1080#1112#1072' ('#1087#1088#1077#1083#1080#1084#1080#1085#1072#1088#1085#1086', '#1076#1080#1112#1072#1075#1085#1086#1089#1090#1080#1095#1082#1086', '#1087#1088#1080#1084#1072#1088#1085#1086')'
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_SELEKCIJA'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
    end
    object txtBroj: TcxDBTextEdit
      Tag = 1
      Left = 106
      Top = 37
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dmOtsustvo.dsIntervju
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 100
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 485
    Width = 1024
    Height = 191
    Align = alBottom
    Color = 15923449
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 8
    DesignSize = (
      1024
      191)
    object GroupBox2: TGroupBox
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 429
      Height = 183
      Align = alLeft
      Anchors = [akRight, akBottom]
      Caption = '    '#1054#1087#1080#1089' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086
      Color = 15397876
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      OnEnter = gbGridPEnter
      OnExit = gbGridPExit
      DesignSize = (
        429
        183)
      object Label6: TLabel
        Left = -28
        Top = 16
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1086#1095#1077#1090#1086#1082' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = -28
        Top = 128
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1088#1072#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = -28
        Top = 73
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1088#1077#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object OPIS_POCETOK: TcxDBMemo
        Left = 80
        Top = 13
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OPIS_POCETOK'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 0
        OnDblClick = OPIS_POCETOKDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 54
        Width = 343
      end
      object OPIS_SREDINA: TcxDBMemo
        Left = 80
        Top = 69
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OPIS_SREDINA'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 1
        OnDblClick = OPIS_SREDINADblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 54
        Width = 343
      end
      object OPIS_KRAJ: TcxDBMemo
        Left = 80
        Top = 125
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OPIS_KRAJ'
        DataBinding.DataSource = dmOtsustvo.dsIntervju
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 2
        OnDblClick = OPIS_KRAJDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 54
        Width = 343
      end
    end
    object gbGridP: TcxGroupBox
      Left = 438
      Top = 1
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1055#1088#1072#1096#1072#1114#1072' '#1079#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1090#1086' '
      ParentBackground = False
      ParentColor = False
      Style.BorderStyle = ebsNone
      Style.Color = 15923449
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Visible = False
      OnEnter = gbGridPEnter
      OnExit = gbGridPExit
      Height = 159
      Width = 571
      object cxGrid2: TcxGrid
        Left = 2
        Top = 18
        Width = 567
        Height = 139
        Hint = #1055#1088#1072#1096#1072#1114#1072' '#1079#1072' '#1056#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086'.  '#1042#1085#1077#1089#1077#1090#1077' '#1079#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1086#1076#1075#1086#1074#1086#1088#1080#1090#1077'!'
        Align = alClient
        Anchors = [akRight, akBottom]
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 14544622
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu2
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        ExplicitTop = 5
        ExplicitHeight = 152
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid2DBTableView1KeyDown
          DataController.DataSource = dsIntKategorija
          DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsView.ColumnAutoWidth = True
          object cxGrid2DBTableView1BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Options.Editing = False
          end
          object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Options.Editing = False
            Width = 401
          end
          object cxGrid2DBTableView1ZABELESKA: TcxGridDBColumn
            DataBinding.FieldName = 'ZABELESKA'
            PropertiesClassName = 'TcxBlobEditProperties'
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Properties.ImmediatePost = True
            Width = 555
          end
          object cxGrid2DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object cxGrid2DBTableView1ID_INTERVJU: TcxGridDBColumn
            DataBinding.FieldName = 'ID_INTERVJU'
            Visible = False
          end
          object cxGrid2DBTableView1ID_KATEGORIJA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_KATEGORIJA'
            Visible = False
          end
        end
        object cxGrid2DBTableView2: TcxGridDBTableView
          DataController.DataSource = dsIntKategorija
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.CellHints = True
          OptionsBehavior.DragHighlighting = False
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.IncSearch = True
          OptionsBehavior.PullFocusing = True
          OptionsSelection.HideSelection = True
          OptionsView.FocusRect = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView2BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Options.Editing = False
            Width = 57
          end
          object cxGrid2DBTableView2NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Options.Editing = False
            Width = 251
          end
          object cxGrid2DBTableView2ZABELESKA: TcxGridDBColumn
            DataBinding.FieldName = 'ZABELESKA'
            PropertiesClassName = 'TcxBlobEditProperties'
            Properties.BlobEditKind = bekMemo
            Properties.BlobPaintStyle = bpsText
            Width = 271
          end
          object cxGrid2DBTableView2ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object cxGrid2DBTableView2ID_INTERVJU: TcxGridDBColumn
            DataBinding.FieldName = 'ID_INTERVJU'
            Visible = False
          end
          object cxGrid2DBTableView2ID_KATEGORIJA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_KATEGORIJA'
            Visible = False
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView2
        end
      end
    end
    object cxButton1: TcxButton
      AlignWithMargins = True
      Left = 439
      Top = 162
      Width = 87
      Height = 25
      Action = aZavrsi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnKeyDown = EnterKakoTab
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 688
    Top = 24
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 200
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 640
    Top = 64
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bsStrukturirano'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 283
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 490
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1088#1072#1096#1072#1114#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1050
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aBrisiPrasanja
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPecatiP
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aAzurirajP
      Category = 0
    end
    object bsStrukturirano: TdxBarSubItem
      Caption = #1057#1090#1088#1091#1082#1090#1091#1088#1080#1088#1072#1085#1086
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 41
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aAzurirajP
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPecatiP
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aBrisiPrasanja
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aDodadiP
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aDodadiP
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    OnExecute = ActionList1Execute
    Left = 656
    Top = 80
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiPrasanja: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 20
      OnExecute = aBrisiPrasanjaExecute
    end
    object aBrisiEdnoP: TAction
      Caption = #1041#1088#1080#1096#1080' '#1087#1088#1072#1096#1072#1114#1077
      ImageIndex = 13
      OnExecute = aBrisiEdnoPExecute
    end
    object aDodadiP: TAction
      Caption = #1044#1086#1076#1072#1076#1080' '#1087#1088#1072#1096#1072#1114#1077
      ImageIndex = 10
      OnExecute = aDodadiPExecute
    end
    object aPecatiP: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiPExecute
    end
    object aAzurirajP: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajPExecute
    end
    object aZavrsi: TAction
      Caption = #1047#1072#1074#1088#1096#1080
      ImageIndex = 6
      OnExecute = aZavrsiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 744
    Top = 8
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40896.546677141200000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository2
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40896.546677164360000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      OptionsView.GroupFooters = False
      StyleRepository = cxStyleRepository1
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.FilterBar = cxStyle3
      Styles.Footer = cxStyle4
      Styles.Header = cxStyle5
      Styles.Selection = cxStyle6
      Styles.StyleSheet = dxGridReportLinkStyleSheet2
      BuiltInReportLink = True
    end
  end
  object tblKategorja: TpFIBDataSet
    RefreshSQL.Strings = (
      'select '
      '    hk.id,'
      '    hk.naziv,'
      '    hk.id_rm_re,'
      '    hk.ts_ins,'
      '    hk.ts_upd,'
      '    hk.usr_ins,'
      '    hk.usr_upd,'
      '    hrm.id id_rm,'
      '    hrm.naziv naziv_rm'
      'from  hr_kategorija hk'
      '   left outer join hr_rm_re hrr on hrr.id = hk.id_rm_re '
      '   left outer join hr_rabotno_mesto hrm on hrr.id_rm = hrm.id'
      
        'where(  (hk.id_rm_re=:rm and hrr.id_re_firma=:firma) or (hk.id_r' +
        'm_re is null)'
      '     ) and (     HK.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select '
      '    hk.id,'
      '    hk.naziv,'
      '    hk.id_rm_re,'
      '    hk.ts_ins,'
      '    hk.ts_upd,'
      '    hk.usr_ins,'
      '    hk.usr_upd,'
      '    hrm.id id_rm,'
      '    hrm.naziv naziv_rm'
      'from  hr_kategorija hk'
      '   left outer join hr_rm_re hrr on hrr.id = hk.id_rm_re '
      '   left outer join hr_rabotno_mesto hrm on hrr.id_rm = hrm.id'
      
        'where (hk.id_rm_re=:rm and hrr.id_re_firma=:firma) or (hk.id_rm_' +
        're is null)')
    AutoUpdateOptions.UpdateTableName = 'HR_KATEGORIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_KATEGORIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 848
    Top = 31
    oRefreshDeletedRecord = True
    object tblKategorjaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblKategorjaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object tblKategorjaID_RM_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1052
      FieldName = 'ID_RM_RE'
    end
    object tblKategorjaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblKategorjaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblKategorjaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblKategorjaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblKategorjaID_RM: TFIBIntegerField
      FieldName = 'ID_RM'
    end
    object tblKategorjaNAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsKategorija: TDataSource
    DataSet = tblKategorja
    Left = 923
    Top = 31
  end
  object qInsertKategorija: TpFIBQuery
    Transaction = dmOtsustvo.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'insert into  hr_int_kategorija'
      '(   id_intervju,'
      '    id_kategorija'
      ')'
      'values'
      '(    :id_intervju,'
      '     :id_kategorija'
      ')')
    Left = 848
    Top = 80
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblIntKategorija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_INT_KATEGORIJA'
      'SET '
      '    ZABELESKA = :ZABELESKA'
      'WHERE'
      '    id_intervju=:MAS_ID'
      'and id = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_INT_KATEGORIJA'
      'WHERE'
      '        ID = :OLD_ID and '
      'id_intervju=:MAS_ID'
      '    ')
    RefreshSQL.Strings = (
      'select hik.id,'
      '    hik.id_intervju,'
      '    hik.id_kategorija,'
      '    hk.broj,'
      '    hk.naziv,'
      '    hik.zabeleska'
      'from hr_int_kategorija hik'
      'inner join hr_kategorija hk on hk.id=hik.id_kategorija'
      'where(  (hik.id_intervju=:MAS_ID)'
      '     ) and (     HIK.ID = :OLD_ID     )'
      '    ')
    SelectSQL.Strings = (
      'select hik.id,'
      '    hik.id_intervju,'
      '    hik.id_kategorija,'
      '    hk.broj,'
      '    hk.naziv,'
      '    hik.zabeleska'
      'from hr_int_kategorija hik'
      'inner join hr_kategorija hk on hk.id=hik.id_kategorija'
      'where (hik.id_intervju=:MAS_ID)'
      'order by hk.broj')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dmOtsustvo.dsIntervju
    Left = 848
    Top = 127
    oRefreshDeletedRecord = True
    object tblIntKategorijaBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblIntKategorijaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object tblIntKategorijaZABELESKA: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'ZABELESKA'
      Size = 200
      EmptyStrToNull = True
    end
    object tblIntKategorijaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIntKategorijaID_INTERVJU: TFIBIntegerField
      FieldName = 'ID_INTERVJU'
    end
    object tblIntKategorijaID_KATEGORIJA: TFIBIntegerField
      FieldName = 'ID_KATEGORIJA'
    end
  end
  object dsIntKategorija: TDataSource
    DataSet = tblIntKategorija
    Left = 923
    Top = 127
  end
  object qBrisiKategorija: TpFIBQuery
    Transaction = dmOtsustvo.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete'
      'from hr_int_kategorija hik'
      'where hik.id_intervju=:ID')
    Left = 920
    Top = 80
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object PopupMenu2: TPopupMenu
    Left = 664
    Top = 544
    object aBrisiEdnoP1: TMenuItem
      Action = aBrisiEdnoP
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clWindow
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle27: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle28: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle29: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle30: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle31: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle32: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle20
      Styles.Caption = cxStyle21
      Styles.CardCaptionRow = cxStyle22
      Styles.CardRowCaption = cxStyle23
      Styles.Content = cxStyle24
      Styles.ContentEven = cxStyle25
      Styles.ContentOdd = cxStyle26
      Styles.FilterBar = cxStyle27
      Styles.Footer = cxStyle28
      Styles.Group = cxStyle29
      Styles.Header = cxStyle30
      Styles.Preview = cxStyle31
      Styles.Selection = cxStyle32
      BuiltIn = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 72
  end
  object cxStyleRepository2: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle7
      Styles.Caption = cxStyle8
      Styles.CardCaptionRow = cxStyle9
      Styles.CardRowCaption = cxStyle10
      Styles.Content = cxStyle11
      Styles.ContentEven = cxStyle12
      Styles.ContentOdd = cxStyle13
      Styles.FilterBar = cxStyle14
      Styles.Footer = cxStyle15
      Styles.Group = cxStyle16
      Styles.Header = cxStyle17
      Styles.Preview = cxStyle18
      Styles.Selection = cxStyle19
      BuiltIn = True
    end
  end
end
