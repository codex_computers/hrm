object dmUcinok: TdmUcinok
  OldCreateOrder = False
  Height = 530
  Width = 512
  object tblRatingFrom: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_RATING_FROM'
      'SET '
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_RATING_FROM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_RATING_FROM('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrf.id, hrf.naziv,'
      '       hrf.ts_ins,'
      '       hrf.ts_upd,'
      '       hrf.usr_ins,'
      '       hrf.usr_upd'
      'from hr_360_rating_from hrf'
      ''
      ' WHERE '
      '        HRF.ID = :OLD_ID'
      '    '
      'order by hrf.id')
    SelectSQL.Strings = (
      'select hrf.id, hrf.naziv,'
      '       hrf.ts_ins,'
      '       hrf.ts_upd,'
      '       hrf.usr_ins,'
      '       hrf.usr_upd'
      'from hr_360_rating_from hrf'
      'order by hrf.id')
    AutoUpdateOptions.UpdateTableName = 'HR_360_RATING_FROM'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_RATING_FROM_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 168
    object tblRatingFromID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRatingFromNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRatingFromTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblRatingFromTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblRatingFromUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRatingFromUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsRatingFrom: TDataSource
    DataSet = tblRatingFrom
    Left = 128
    Top = 168
  end
  object tblFeedBack: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_FEEDBACK'
      'SET '
      '    BROJ = :BROJ,'
      '    DATUM = :DATUM,'
      '    DATUM_REVIZIJA = :DATUM_REVIZIJA,'
      '    OPIS = :OPIS,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_FEEDBACK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_FEEDBACK('
      '    ID,'
      '    BROJ,'
      '    DATUM,'
      '    DATUM_REVIZIJA,'
      '    OPIS,'
      '    ID_RE_FIRMA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :DATUM,'
      '    :DATUM_REVIZIJA,'
      '    :OPIS,'
      '    :ID_RE_FIRMA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrf.id,'
      '       hrf.broj,'
      '       hrf.datum,'
      '       HRF.datum_revizija,'
      '       hrf.opis,'
      '       hrf.id_re_firma,'
      '       hrf.ts_ins, hrf.ts_upd, hrf.usr_ins, hrf.usr_upd'
      'from hr_360_feedback hrf'
      'where(  hrf.id_re_firma = :firma'
      '     ) and (     HRF.ID = :OLD_ID'
      '     )'
      '    '
      'order by hrf.broj')
    SelectSQL.Strings = (
      'select hrf.id,'
      '       hrf.broj,'
      '       hrf.datum,'
      '       HRF.datum_revizija,'
      '       hrf.opis,'
      '       hrf.id_re_firma,'
      '       hrf.ts_ins, hrf.ts_upd, hrf.usr_ins, hrf.usr_upd'
      'from hr_360_feedback hrf'
      'where hrf.id_re_firma = :firma'
      'order by hrf.broj'
      '')
    AutoUpdateOptions.UpdateTableName = 'HR_360_FEEDBACK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_FEEDBACK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 232
    object tblFeedBackID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblFeedBackBROJ: TFIBIntegerField
      Alignment = taLeftJustify
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblFeedBackDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblFeedBackDATUM_REVIZIJA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
      FieldName = 'DATUM_REVIZIJA'
    end
    object tblFeedBackOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblFeedBackID_RE_FIRMA: TFIBIntegerField
      DisplayLabel = #1060#1080#1088#1084#1072
      FieldName = 'ID_RE_FIRMA'
    end
    object tblFeedBackTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblFeedBackTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblFeedBackUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblFeedBackUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsFeedBack: TDataSource
    DataSet = tblFeedBack
    Left = 128
    Top = 232
  end
  object tblPrasanja: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_PRASANJA'
      'SET '
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    GRUPA = :GRUPA,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_PRASANJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_PRASANJA('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    GRUPA,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :GRUPA,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrp.id,'
      '       hrp.naziv,'
      '       hrp.ts_ins,'
      '       hrp.grupa,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd,'
      '       hgp.naziv as GrupaNaziv,'
      '       hgp.normativ'
      'from hr_360_prasanja hrp'
      'inner join hr_360_grupa_prasanja hgp on hgp.id = hrp.grupa'
      ''
      ' WHERE '
      '        HRP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hrp.id,'
      '       hrp.naziv,'
      '       hrp.ts_ins,'
      '       hrp.grupa,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd,'
      '       hgp.naziv as GrupaNaziv,'
      '       hgp.normativ'
      'from hr_360_prasanja hrp'
      'inner join hr_360_grupa_prasanja hgp on hgp.id = hrp.grupa')
    AutoUpdateOptions.UpdateTableName = 'HR_360_PRASANJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_PRASANJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 48
    object tblPrasanjaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPrasanjaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblPrasanjaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblPrasanjaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblPrasanjaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPrasanjaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPrasanjaGRUPA: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA'
    end
    object tblPrasanjaGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPrasanjaNORMATIV: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072#1090#1080#1074
      FieldName = 'NORMATIV'
    end
  end
  object dsPrasanja: TDataSource
    DataSet = tblPrasanja
    Left = 128
    Top = 48
  end
  object tblGrupaPrasanja: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_GRUPA_PRASANJA'
      'SET '
      '    NAZIV = :NAZIV,'
      '    NORMATIV = :NORMATIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_GRUPA_PRASANJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_GRUPA_PRASANJA('
      '    ID,'
      '    NAZIV,'
      '    NORMATIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :NORMATIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hgp.id,'
      '       hgp.naziv,'
      '       hgp.normativ,'
      '       hgp.ts_ins,'
      '       hgp.ts_upd,'
      '       hgp.usr_ins,'
      '       hgp.usr_upd'
      'from hr_360_grupa_prasanja hgp'
      ''
      ' WHERE '
      '        HGP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hgp.id,'
      '       hgp.naziv,'
      '       hgp.normativ,'
      '       hgp.ts_ins,'
      '       hgp.ts_upd,'
      '       hgp.usr_ins,'
      '       hgp.usr_upd'
      'from hr_360_grupa_prasanja hgp')
    AutoUpdateOptions.UpdateTableName = 'HR_360_GRUPA_PRASANJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_GRUPA_PRASANJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 104
    object tblGrupaPrasanjaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblGrupaPrasanjaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      EmptyStrToNull = True
    end
    object tblGrupaPrasanjaNORMATIV: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072#1090#1080#1074
      FieldName = 'NORMATIV'
    end
    object tblGrupaPrasanjaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblGrupaPrasanjaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblGrupaPrasanjaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblGrupaPrasanjaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsGrupaPrasanja: TDataSource
    DataSet = tblGrupaPrasanja
    Left = 128
    Top = 104
  end
  object tblIspitanici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_F_ISPITANICI'
      'SET '
      '    FEEDBACK_ID = :FEEDBACK_ID,'
      '    MB = :MB,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_F_ISPITANICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_F_ISPITANICI('
      '    ID,'
      '    FEEDBACK_ID,'
      '    MB,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :FEEDBACK_ID,'
      '    :MB,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hri.id,'
      '       hri.feedback_id,'
      '       hri.mb,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hri.ts_ins,'
      '       hri.ts_upd,'
      '       hri.usr_ins,'
      '       hri.usr_upd'
      'from hr_360_f_ispitanici hri'
      'inner join hr_vraboten hrv on hrv.mb = hri.mb'
      'where(  hri.feedback_id = :mas_ID'
      '     ) and (     HRI.ID = :OLD_ID'
      '     )'
      '    '
      'order by 4')
    SelectSQL.Strings = (
      'select hri.id,'
      '       hri.feedback_id,'
      '       hri.mb,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hri.ts_ins,'
      '       hri.ts_upd,'
      '       hri.usr_ins,'
      '       hri.usr_upd'
      'from hr_360_f_ispitanici hri'
      'inner join hr_vraboten hrv on hrv.mb = hri.mb'
      'where hri.feedback_id = :mas_ID'
      'order by 4')
    AutoUpdateOptions.UpdateTableName = 'HR_360_F_ISPITANICI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_F_ISPITANICI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsFeedBack
    Left = 40
    Top = 296
    object tblIspitaniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIspitaniciFEEDBACK_ID: TFIBIntegerField
      DisplayLabel = 'FEEDBACK(ID)'
      FieldName = 'FEEDBACK_ID'
    end
    object tblIspitaniciMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object tblIspitaniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblIspitaniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblIspitaniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblIspitaniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblIspitaniciNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      EmptyStrToNull = True
    end
    object tblIspitaniciNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsIspitanici: TDataSource
    DataSet = tblIspitanici
    Left = 128
    Top = 296
  end
  object tblOcenuvaci: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_F_OCENUVACI'
      'SET '
      '    FEEDBACK_ID = :FEEDBACK_ID,'
      '    RATING_FROM_ID = :RATING_FROM_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_F_OCENUVACI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_F_OCENUVACI('
      '    ID,'
      '    FEEDBACK_ID,'
      '    RATING_FROM_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :FEEDBACK_ID,'
      '    :RATING_FROM_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.feedback_id,'
      '       hro.rating_from_id,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrf.naziv as OcenuvacNaziv'
      'from hr_360_f_ocenuvaci hro'
      'inner join hr_360_rating_from hrf on hrf.id = hro.rating_from_id'
      'where(  hro.feedback_id = :mas_ID'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      '    '
      'order by 8')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.feedback_id,'
      '       hro.rating_from_id,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrf.naziv as OcenuvacNaziv'
      'from hr_360_f_ocenuvaci hro'
      'inner join hr_360_rating_from hrf on hrf.id = hro.rating_from_id'
      'where hro.feedback_id = :mas_ID'
      'order by 8')
    AutoUpdateOptions.UpdateTableName = 'HR_360_F_OCENUVACI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_F_OCENUVACI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsFeedBack
    Left = 40
    Top = 360
    object tblOcenuvaciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOcenuvaciFEEDBACK_ID: TFIBIntegerField
      DisplayLabel = 'FEEDBACK (ID)'
      FieldName = 'FEEDBACK_ID'
    end
    object tblOcenuvaciRATING_FROM_ID: TFIBIntegerField
      DisplayLabel = #1054#1094#1077#1085#1091#1074#1072#1095' (ID)'
      FieldName = 'RATING_FROM_ID'
    end
    object tblOcenuvaciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblOcenuvaciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblOcenuvaciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblOcenuvaciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblOcenuvaciOCENUVACNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'OCENUVACNAZIV'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsOcenuvaci: TDataSource
    DataSet = tblOcenuvaci
    Left = 128
    Top = 360
  end
  object tblPrasalnik: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_F_PRASALNIK'
      'SET '
      '    FEEDBACK_ID = :FEEDBACK_ID,'
      '    PRASANJE_ID = :PRASANJE_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_F_PRASALNIK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_F_PRASALNIK('
      '    ID,'
      '    FEEDBACK_ID,'
      '    PRASANJE_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :FEEDBACK_ID,'
      '    :PRASANJE_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrp.id,'
      '       hrp.feedback_id,'
      '       hrp.prasanje_ID,'
      '       hp.grupa,'
      '       hp.naziv as prasanjeNaziv,'
      '       hg.naziv as GrupaNaziv,'
      '       hrp.ts_ins,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd'
      'from hr_360_f_prasalnik hrp'
      'inner join hr_360_prasanja hp on hp.id = hrp.prasanje_ID'
      'inner join hr_360_grupa_prasanja hg on hg.id = hp.grupa'
      'where(  hrp.feedback_id = :mas_ID'
      '     ) and (     HRP.ID = :OLD_ID'
      '     )'
      '    '
      'order by 4, 5')
    SelectSQL.Strings = (
      'select hrp.id,'
      '       hrp.feedback_id,'
      '       hrp.prasanje_ID,'
      '       hp.grupa,'
      '       hp.naziv as prasanjeNaziv,'
      '       hg.naziv as GrupaNaziv,'
      '       hrp.ts_ins,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd'
      'from hr_360_f_prasalnik hrp'
      'inner join hr_360_prasanja hp on hp.id = hrp.prasanje_ID'
      'inner join hr_360_grupa_prasanja hg on hg.id = hp.grupa'
      'where hrp.feedback_id = :mas_ID'
      'order by 4, 5')
    AutoUpdateOptions.UpdateTableName = 'HR_360_F_PRASALNIK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_F_PRASALNIK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsFeedBack
    Left = 32
    Top = 424
    object tblPrasalnikID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPrasalnikFEEDBACK_ID: TFIBIntegerField
      DisplayLabel = 'FEEDBACK (ID)'
      FieldName = 'FEEDBACK_ID'
    end
    object tblPrasalnikPRASANJE_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1072#1096#1072#1114#1077' (ID)'
      FieldName = 'PRASANJE_ID'
    end
    object tblPrasalnikGRUPA: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
    end
    object tblPrasalnikPRASANJENAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1072#1096#1072#1114#1077
      FieldName = 'PRASANJENAZIV'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblPrasalnikGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPrasalnikTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblPrasalnikTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblPrasalnikUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPrasalnikUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object dsPrasalnik: TDataSource
    DataSet = tblPrasalnik
    Left = 128
    Top = 424
  end
  object pMaxBrojFeedback: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_FEEDBACK_BROJ ')
    StoredProcName = 'PROC_HR_FEEDBACK_BROJ'
    Left = 368
    Top = 336
  end
  object frxpRASANJA: TfrxDBDataset
    UserName = 'frxPrasanja'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'FEEDBACK_ID=FEEDBACK_ID'
      'PRASANJE_ID=PRASANJE_ID'
      'GRUPA=GRUPA'
      'PRASANJENAZIV=PRASANJENAZIV'
      'GRUPANAZIV=GRUPANAZIV'
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD')
    DataSource = dsPrasalnik
    BCDToCurrency = False
    Left = 224
    Top = 56
  end
  object tblRezultati: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_360_F_OCENI'
      'SET '
      '    MB = :MB,'
      '    FEEDBACK_ID = :FEEDBACK_ID,'
      '    PRASANJE_ID = :PRASANJE_ID,'
      '    OCENUVAC_ID = :OCENUVAC_ID,'
      '    OCENA = :OCENA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_360_F_OCENI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_360_F_OCENI('
      '    ID,'
      '    MB,'
      '    FEEDBACK_ID,'
      '    PRASANJE_ID,'
      '    OCENUVAC_ID,'
      '    OCENA'
      ')'
      'VALUES('
      '    :ID,'
      '    :MB,'
      '    :FEEDBACK_ID,'
      '    :PRASANJE_ID,'
      '    :OCENUVAC_ID,'
      '    :OCENA'
      ')')
    RefreshSQL.Strings = (
      'select ho.id,'
      '       ho.mb,'
      '       ho.feedback_id,'
      '       ho.prasanje_id,'
      '       ho.ocenuvac_id,'
      '       ho.ocena,'
      '       hrv.prezime || '#39' '#39' || hrv.ime as vrabotenNaziv,'
      '       hp.naziv as prasanjeNaziv,'
      '       hgp.naziv as grupaPrasanjeNaziv,'
      '       hgp.normativ,'
      '       hr.naziv as ocenuvacNaziv'
      'from hr_360_f_oceni ho'
      'inner join hr_vraboten hrv on hrv.mb = ho.mb'
      'inner join hr_360_prasanja hp on hp.id = ho.prasanje_id'
      'inner join hr_360_grupa_prasanja hgp on hgp.id = hp.grupa'
      'inner join hr_360_rating_from hr on hr.id = ho.ocenuvac_id'
      'where(  ho.feedback_id = :fID and ho.mb = :MB'
      '     ) and (     HO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ho.id,'
      '       ho.mb,'
      '       ho.feedback_id,'
      '       ho.prasanje_id,'
      '       ho.ocenuvac_id,'
      '       ho.ocena,'
      '       hrv.prezime || '#39' '#39' || hrv.ime as vrabotenNaziv,'
      '       hp.naziv as prasanjeNaziv,'
      '       hgp.naziv as grupaPrasanjeNaziv,'
      '       hgp.normativ,'
      '       hr.naziv as ocenuvacNaziv'
      'from hr_360_f_oceni ho'
      'inner join hr_vraboten hrv on hrv.mb = ho.mb'
      'inner join hr_360_prasanja hp on hp.id = ho.prasanje_id'
      'inner join hr_360_grupa_prasanja hgp on hgp.id = hp.grupa'
      'inner join hr_360_rating_from hr on hr.id = ho.ocenuvac_id'
      'where ho.feedback_id = :fID and ho.mb = :MB')
    AutoUpdateOptions.UpdateTableName = 'HR_360_F_OCENI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_360_F_OCENI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 312
    Top = 56
    object tblRezultatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRezultatiMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'.'
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object tblRezultatiFEEDBACK_ID: TFIBIntegerField
      DisplayLabel = 'FEEDBACK (ID)'
      FieldName = 'FEEDBACK_ID'
    end
    object tblRezultatiPRASANJE_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1072#1096#1072#1114#1077' (ID)'
      FieldName = 'PRASANJE_ID'
    end
    object tblRezultatiOCENUVAC_ID: TFIBIntegerField
      DisplayLabel = #1054#1094#1077#1085#1091#1074#1072#1095' (ID)'
      FieldName = 'OCENUVAC_ID'
    end
    object tblRezultatiVRABOTENNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077
      FieldName = 'VRABOTENNAZIV'
      Size = 151
      EmptyStrToNull = True
    end
    object tblRezultatiPRASANJENAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1072#1096#1072#1114#1077
      FieldName = 'PRASANJENAZIV'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblRezultatiOCENUVACNAZIV: TFIBStringField
      DisplayLabel = #1054#1094#1077#1085#1091#1074#1072#1095
      FieldName = 'OCENUVACNAZIV'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRezultatiOCENA: TFIBFloatField
      DisplayLabel = #1054#1094#1077#1085#1082#1072
      FieldName = 'OCENA'
    end
    object tblRezultatiGRUPAPRASANJENAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' '#1085#1072' '#1087#1088#1072#1096#1072#1114#1077
      FieldName = 'GRUPAPRASANJENAZIV'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRezultatiNORMATIV: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072#1090#1080#1074
      FieldName = 'NORMATIV'
    end
  end
  object dsRezultati: TDataSource
    DataSet = tblRezultati
    Left = 376
    Top = 56
  end
  object pInsertRezultati: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_INSERT_REZULTATI (?MB_IN, ?FEEDBACKID_' +
        'IN)')
    StoredProcName = 'PROC_HR_INSERT_REZULTATI'
    Left = 376
    Top = 280
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pCountMBRezultati: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_COUNTMBREZULTATI (?MB, ?FID)')
    StoredProcName = 'PROC_HR_COUNTMBREZULTATI'
    Left = 376
    Top = 216
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    TimeoutAction = TARollback
    Left = 368
    Top = 144
  end
  object pCountFedbackRezultati: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_COUNTFEEDBACKVOREZULTAT (?FID)')
    StoredProcName = 'PROC_HR_COUNTFEEDBACKVOREZULTAT'
    Left = 368
    Top = 400
  end
end
