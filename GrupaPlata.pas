{*******************************************************}
{                                                       }
{     ��������� : ������ ��������                      }
{                                                       }
{     ����� : 11.03.2010                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************} 

unit GrupaPlata;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxSpinEdit,
  cxGroupBox, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip;

type
  TfrmGrupaPlata = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid1DBTableView1MINIMALNA: TcxGridDBColumn;
    cxGrid1DBTableView1MAKSIMALNA: TcxGridDBColumn;
    cxGrid1DBTableView1STAPKA: TcxGridDBColumn;
    Label2: TLabel;
    txtNaziv: TcxDBTextEdit;
    Label3: TLabel;
    txtValuta: TcxDBTextEdit;
    cbValuta: TcxDBLookupComboBox;
    cxGroupBox1: TcxGroupBox;
    txtMin: TcxDBSpinEdit;
    txtMaks: TcxDBSpinEdit;
    Label4: TLabel;
    Label5: TLabel;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    txtStapka: TcxDBTextEdit;
    Label6: TLabel;
    procedure txtMaksKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtMaksExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupaPlata: TfrmGrupaPlata;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit;

{$R *.dfm}

procedure TfrmGrupaPlata.txtMaksExit(Sender: TObject);
begin
  //��������� �� �������� �� ��������� �� ������� ��� ����� �� ������ �� �������� �����
  //���� �������� � ��� �� ������ �� ������ �� �� ����� ��� �������
  inherited;
   if txtMaks.value < dmSis.tblGrupaPlataMINIMALNA.Value then
            begin
              MessageDlg('������������ ������� �� ������� � ������ �� �����������!', mtError,
                [mbOk], 0, mbOk);
               txtMaks.SetFocus;
            end
            else
            begin
              TEdit(Sender).Color:=clWhite;
              txtStapka.SetFocus;
            end;
end;

procedure TfrmGrupaPlata.txtMaksKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
   case Key of
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN,VK_DOWN:
        begin
        if txtMaks.Value < dmSis.tblGrupaPlataMINIMALNA.Value then
            begin
              MessageDlg('������������ ������� �� ������� � ������ �� �����������!', mtError,
                [mbOk], 0, mbOk);
               Abort;
            end
            else
              PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

end.
