inherited frmVestiniKvalifikacii: TfrmVestiniKvalifikacii
  Caption = #1051#1080#1095#1085#1080' '#1074#1077#1096#1090#1080#1085#1080' '#1080' '#1082#1086#1084#1087#1077#1085#1090#1077#1085#1094#1080#1080
  ClientHeight = 733
  ClientWidth = 978
  ExplicitWidth = 994
  ExplicitHeight = 771
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 978
    Height = 207
    ExplicitWidth = 978
    ExplicitHeight = 207
    inherited cxGrid1: TcxGrid
      Width = 974
      Height = 203
      ExplicitWidth = 974
      ExplicitHeight = 203
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsLVK
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'MB_VRABOTEN'
          Width = 87
        end
        object cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MOLBA'
          Visible = False
          Width = 107
        end
        object cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 140
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'IMEPREZIME'
          Visible = False
          Width = 105
        end
        object cxGrid1DBTableView1MAJCIN_JAZIK: TcxGridDBColumn
          DataBinding.FieldName = 'MAJCIN_JAZIK'
          Width = 101
        end
        object cxGrid1DBTableView1DRUSTVENI_V: TcxGridDBColumn
          DataBinding.FieldName = 'DRUSTVENI_V'
          Width = 201
        end
        object cxGrid1DBTableView1ORGANIZACISKI_V: TcxGridDBColumn
          DataBinding.FieldName = 'ORGANIZACISKI_V'
          Width = 160
        end
        object cxGrid1DBTableView1TEHNICKI_V: TcxGridDBColumn
          DataBinding.FieldName = 'TEHNICKI_V'
          Width = 122
        end
        object cxGrid1DBTableView1KOMPJUTERSKI_V: TcxGridDBColumn
          DataBinding.FieldName = 'KOMPJUTERSKI_V'
          Width = 146
        end
        object cxGrid1DBTableView1UMETNICKI_V: TcxGridDBColumn
          DataBinding.FieldName = 'UMETNICKI_V'
          Width = 129
        end
        object cxGrid1DBTableView1DRUGI_V: TcxGridDBColumn
          DataBinding.FieldName = 'DRUGI_V'
          Width = 105
        end
        object cxGrid1DBTableView1VOZACKA_DOZVOLA1: TcxGridDBColumn
          DataBinding.FieldName = 'VOZACKA_DOZVOLA'
          Visible = False
        end
        object cxGrid1DBTableView1VOZACKA_DOZVOLA: TcxGridDBColumn
          DataBinding.FieldName = 'Vozacka'
          Width = 125
        end
        object cxGrid1DBTableView1KATEGORIJA_VD: TcxGridDBColumn
          DataBinding.FieldName = 'KATEGORIJA_VD'
          Width = 109
        end
        object cxGrid1DBTableView1DOPOLNITELNI_INFORMACII: TcxGridDBColumn
          DataBinding.FieldName = 'DOPOLNITELNI_INFORMACII'
          Width = 177
        end
        object cxGrid1DBTableView1PRILOZI: TcxGridDBColumn
          DataBinding.FieldName = 'PRILOZI'
          Width = 136
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 333
    Width = 978
    Height = 377
    ExplicitTop = 333
    ExplicitWidth = 978
    ExplicitHeight = 377
    inherited Label1: TLabel
      Left = 487
      Top = 40
      Visible = False
      ExplicitLeft = 487
      ExplicitTop = 40
    end
    object Label10: TLabel [1]
      Left = 79
      Top = 18
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1086#1083#1073#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 36
      Top = 30
      Width = 93
      Height = 28
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label3: TLabel [3]
      Left = 27
      Top = 64
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1112#1095#1080#1085' '#1112#1072#1079#1080#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [4]
      Left = 594
      Top = 180
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = #1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel [5]
      Left = 594
      Top = 164
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel [6]
      Left = 594
      Top = 241
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = #1055#1088#1080#1083#1086#1079#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 551
      Top = 12
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsLVK
      TabOrder = 1
      Visible = False
      ExplicitLeft = 551
      ExplicitTop = 12
      ExplicitWidth = 96
      Width = 96
    end
    inherited OtkaziButton: TcxButton
      Left = 886
      Top = 323
      TabOrder = 11
      ExplicitLeft = 886
      ExplicitTop = 323
    end
    inherited ZapisiButton: TcxButton
      Left = 805
      Top = 323
      TabOrder = 10
      ExplicitLeft = 805
      ExplicitTop = 323
    end
    object ID_MOLBA: TcxDBTextEdit
      Left = 135
      Top = 15
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1086#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsLVK
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object IMEPREZIME: TcxDBLookupComboBox
      Left = 232
      Top = 15
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' '#1082#1086#1077' '#1112#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072' '#1084#1086#1083#1073#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsLVK
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'ID'
        end
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Width = 800
          FieldName = 'IMEPREZIME'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dmSis.dsMolba
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 349
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Left = 232
      Top = 34
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsLVK
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      ShowHint = True
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 349
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Left = 135
      Top = 34
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsLVK
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object GroupBox1: TGroupBox
      Left = 14
      Top = 93
      Width = 587
      Height = 277
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = ' '
      TabOrder = 6
      DesignSize = (
        587
        277)
      object Label18: TLabel
        Left = 13
        Top = 148
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1086#1084#1087#1112#1091#1090#1077#1088#1089#1082#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 13
        Top = 66
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1089#1082#1080':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 13
        Top = 25
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1088#1091#1096#1090#1074#1077#1085#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 19
        Top = 107
        Width = 96
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1077#1093#1085#1080#1095#1082#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 13
        Top = 189
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1059#1084#1077#1090#1085#1080#1095#1082#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 13
        Top = 230
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1088#1091#1075#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object KOMPJUTERSKI_V: TcxDBMemo
        Left = 121
        Top = 145
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'KOMPJUTERSKI_V'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 3
        OnDblClick = KOMPJUTERSKI_VDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 446
      end
      object TEHNICKI_V: TcxDBMemo
        Left = 121
        Top = 104
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'TEHNICKI_V'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 2
        OnDblClick = TEHNICKI_VDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 446
      end
      object ORGANIZACISKI_V: TcxDBMemo
        Left = 121
        Top = 63
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ORGANIZACISKI_V'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 1
        OnDblClick = ORGANIZACISKI_VDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 446
      end
      object DRUSTVENI_V: TcxDBMemo
        Left = 121
        Top = 22
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'DRUSTVENI_V'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 0
        OnDblClick = DRUSTVENI_VDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 446
      end
      object UMETNICKI_V: TcxDBMemo
        Left = 121
        Top = 186
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'UMETNICKI_V'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 4
        OnDblClick = UMETNICKI_VDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 446
      end
      object DRUGI_V: TcxDBMemo
        Left = 121
        Top = 227
        Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'DRUGI_V'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 5
        OnDblClick = DRUGI_VDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 446
      end
    end
    object MajcinJazik: TcxDBTextEdit
      Tag = 1
      Left = 135
      Top = 61
      BeepOnEnter = False
      DataBinding.DataField = 'MAJCIN_JAZIK'
      DataBinding.DataSource = dmSis.dsLVK
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 446
    end
    object GroupBox3: TGroupBox
      Left = 702
      Top = 40
      Width = 231
      Height = 107
      Anchors = [akRight, akBottom]
      Caption = #1042#1086#1079#1072#1095#1082#1072' '#1076#1086#1079#1074#1086#1083#1072
      TabOrder = 7
      object Label8: TLabel
        Left = -10
        Top = 74
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Kategorija: TcxDBTextEdit
        Left = 98
        Top = 71
        BeepOnEnter = False
        DataBinding.DataField = 'KATEGORIJA_VD'
        DataBinding.DataSource = dmSis.dsLVK
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 103
      end
      object cxDBRadioGroup1: TcxDBRadioGroup
        Left = 16
        Top = 19
        DataBinding.DataField = 'VOZACKA_DOZVOLA'
        DataBinding.DataSource = dmSis.dsLVK
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1053#1077#1084#1072
            Value = 0
          end
          item
            Caption = #1048#1084#1072
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfOffice11
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 46
        Width = 185
      end
    end
    object PRILOZI: TcxDBMemo
      Left = 702
      Top = 238
      Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'PRILOZI'
      DataBinding.DataSource = dmSis.dsLVK
      Properties.ScrollBars = ssVertical
      TabOrder = 9
      OnDblClick = PRILOZIDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 54
      Width = 259
    end
    object DOPOLNITELNI_INFORMACII: TcxDBMemo
      Left = 702
      Top = 161
      Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'DOPOLNITELNI_INFORMACII'
      DataBinding.DataSource = dmSis.dsLVK
      Properties.ScrollBars = ssVertical
      TabOrder = 8
      OnDblClick = DOPOLNITELNI_INFORMACIIDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 71
      Width = 259
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 978
    ExplicitWidth = 978
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 710
    Width = 978
    ExplicitTop = 710
    ExplicitWidth = 978
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1051#1080#1095#1085#1080' '#1074#1077#1096#1090#1080#1085#1080' '#1080' '#1082#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1080
      FloatClientWidth = 64
      FloatClientHeight = 156
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 268
      FloatClientWidth = 111
      FloatClientHeight = 126
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 475
      FloatClientHeight = 104
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
    object dxBarManager1Bar5: TdxBar [4]
      CaptionButtons = <>
      DockedLeft = 191
      DockedTop = 0
      FloatLeft = 1087
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aStranskiJazici
      Category = 0
      LargeImageIndex = 43
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Top = 200
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40280.465406921300000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object ActionList2: TActionList
    Images = dm.cxLargeImages
    Left = 552
    Top = 144
    object aStranskiJazici: TAction
      Caption = #1057#1090#1088#1072#1085#1089#1082#1080' '#1112#1072#1079#1080#1094#1080
      ImageIndex = 71
      OnExecute = aStranskiJaziciExecute
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
