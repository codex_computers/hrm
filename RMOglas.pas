{*******************************************************}
{                                                       }
{     ��������� :  ������ ��������                     }
{                  ������ �������                      }
{     ����� :  15.12.2011                               }
{                                                       }
{     ������ : 1.1.1.17                                }
{                                                       }
{*******************************************************}

unit RMOglas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMemo,
  cxGroupBox, cxCalendar, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, FIBDataSet, pFIBDataSet, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint;

type
  TfrmRMOglas = class(TfrmMaster)
    cxGrid1DBTableView1BROJ_OGLAS: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_OGLAS: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1URL: TcxGridDBColumn;
    cxGrid1DBTableView1TEL: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label2: TLabel;
    txtRMRE: TcxDBTextEdit;
    cbRMRE: TcxDBLookupComboBox;
    Label6: TLabel;
    txtOpis: TcxDBMemo;
    gbOlas: TcxGroupBox;
    Label7: TLabel;
    txtDatumObj: TcxDBDateEdit;
    Label8: TLabel;
    txtDatumIstekuva: TcxDBDateEdit;
    dxBarLargeButton10: TdxBarLargeButton;
    cxGroupBox2: TcxGroupBox;
    Label3: TLabel;
    txtEmail: TcxDBTextEdit;
    Label4: TLabel;
    txtURL: TcxDBTextEdit;
    Label5: TLabel;
    txtTelefon: TcxDBTextEdit;
    cxHintStyleController1: TcxHintStyleController;
    dsRMRE: TDataSource;
    tblRMRE: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBIntegerField5: TFIBIntegerField;
    FIBDateTimeField1: TFIBDateTimeField;
    FIBDateTimeField2: TFIBDateTimeField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBStringField3: TFIBStringField;
    FIBStringField4: TFIBStringField;
    FIBStringField5: TFIBStringField;
    FIBIntegerField6: TFIBIntegerField;
    Label9: TLabel;
    BrRabotnMesta: TcxDBTextEdit;
    cxGrid1DBTableView1BR_RABOTNI_IMESTA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn;
    procedure aNovExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtOpisDblClick(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRMOglas: TfrmRMOglas;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,Notepad,
  dmUnitOtsustvo, Utils;

{$R *.dfm}

procedure TfrmRMOglas.aAzurirajExecute(Sender: TObject);
var pom:Integer;datum:string;
begin
     datum:=DateToStr(Now);
     pom:=dmMat.zemiMax(dmSis.pCountRMREMolba, 'RMREID', 'OGLASID', Null, dmSis.tblRMOglasID_RM_RE.Value, dmSis.tblRMOglasID_OGLAS.Value, Null, 'COUNTT') - 1;
     if (dmSis.tblOglasSTATUS.Value=0) or (dmSis.tblOglasDATUM_OBJAVEN.Value<StrToDate(datum)) then
        ShowMessage('������� � ��� ������ ��� �� � �������!!!')
     else
     if pom <> 0 then
        ShowMessage('�������� �� ���������. �� ��� ������� ����� ��� ������������ ����� !!!')
     else
        begin
          if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
              dPanel.Enabled:=True;
              lPanel.Enabled:=False;
              tblRMRE.ParamByName('firma').Value:=firma;
              tblRMRE.ParamByName('datum').Value:='11.11.1111';
              tblRMRE.FullRefresh;
              txtRMRE.SetFocus;
              cxGrid1DBTableView1.DataController.DataSet.Edit;
              dmSis.tblRMOglasID_OGLAS.Value:=dmSis.tblOglasID.Value;
            end
          else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
        end;
 // dmSis.dsOglas.Enabled:=False;
     gbOlas.Caption:=' ����� ��� ';
end;

procedure TfrmRMOglas.aBrisiExecute(Sender: TObject);
var pom:Integer; datum:string;
begin
     datum:=DateToStr(Now);
     pom:=dmMat.zemiMax(dmSis.pCountRMREMolba, 'RMREID', 'OGLASID', Null, dmSis.tblRMOglasID_RM_RE.Value, dmSis.tblRMOglasID_OGLAS.Value, Null, 'COUNTT') - 1;
     if (dmSis.tblOglasSTATUS.Value=0) or (dmSis.tblOglasDATUM_OBJAVEN.Value<StrToDate(datum)) then
        ShowMessage('������� � ��� ������ ��� �� � �������!')
     else
     if pom <> 0 then
        ShowMessage('�������� �� �������. �� ��� ������� ����� ��� ������������ ����� !!!')
     else
        cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmRMOglas.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(131);
end;

procedure TfrmRMOglas.aNovExecute(Sender: TObject);
var datum:string;
begin
     datum:=DateToStr(Now);
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          if (dmSis.tblOglasSTATUS.Value=0) or (dmSis.tblOglasDATUM_OBJAVEN.Value<StrToDate(datum)) then
             begin
               ShowMessage('������� � ��� ������ ��� �� � �������!');
             end
          else
             begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               tblRMRE.ParamByName('firma').Value:=firma;
               tblRMRE.ParamByName('datum').Value:='11.11.1111';
               tblRMRE.FullRefresh;
               txtRMRE.SetFocus;
               cxGrid1DBTableView1.DataController.DataSet.Insert;
               dmSis.tblRMOglasID_OGLAS.Value:=dmSis.tblOglasID.Value;
               dmSis.tblRMOglasBRGODINAOGLAS.Value:=dmSis.tblOglasBROJGODINA.Value;
             end;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
 // dmSis.dsOglas.Enabled:=False;
     gbOlas.Caption:=' ����� ��� ';
end;

procedure TfrmRMOglas.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  tblRMRE.ParamByName('firma').Value:='%';
  tblRMRE.ParamByName('datum').Value:='%';
  tblRMRE.FullRefresh;
end;

procedure TfrmRMOglas.aZapisiExecute(Sender: TObject);
var  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
        tblRMRE.ParamByName('firma').Value:='%';
        tblRMRE.ParamByName('datum').Value:='%';
        tblRMRE.FullRefresh;
    end;
  end;
end;

procedure TfrmRMOglas.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if (Sender = txtemail) or (Sender = txtURL) then
        ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

procedure TfrmRMOglas.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if(Sender = txtemail) or (Sender = txtURL) then
        ActivateKeyboardLayout($042F042F, KLF_REORDER);
end;

procedure TfrmRMOglas.FormShow(Sender: TObject);
begin
  inherited;
   dmsis.tblRMOglas.close;
   dmsis.tblRMOglas.Open;
   tblRMRE.ParamByName('firma').Value:='%';
   tblRMRE.ParamByName('datum').Value:='%';
   tblRMRE.Open;
   gbOlas.Caption:='����� ��� '+ dmSis.tblOglasBROJGODINA.Value;
   //dmSis.tblOglasBROJ.AsString+' ';
end;

procedure TfrmRMOglas.txtOpisDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRMOglasOPIS.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRMOglasOPIS.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

end.
