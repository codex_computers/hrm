{*******************************************************}
{                                                       }
{     ��������� : ������ ��������                      }
{                                                       }
{     ����� : 17.03.2010                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

unit RabMestaRE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxorgchr,
  dxdborgc, dxPSGraphicLnk, dxPSdxOCLnk, dxPSdxDBOCLnk, cxLabel,
  cxColorComboBox, cxVGrid, cxInplaceContainer, cxHint, ImgList, FIBDataSet,
  pFIBDataSet, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxGroupBox, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxScreenTip,
  dxCustomHint, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxRadioGroup, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmRMRE = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE: TcxGridDBColumn;
    cxGrid1DBTableView1ID_SISTEMATIZACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_IZVRSITELI: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_SISTEMATIZACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn;
    cbRE: TcxDBLookupComboBox;
    Label2: TLabel;
    txtSis: TcxDBTextEdit;
    cbSis: TcxDBLookupComboBox;
    Label3: TLabel;
    txtRM: TcxDBTextEdit;
    cbRM: TcxDBLookupComboBox;
    Label4: TLabel;
    txtBrIzvrsiteli: TcxDBTextEdit;
    rtOrganogram: TdxRibbonTab;
    Panel1: TPanel;
    orgRM: TdxDbOrgChart;
    dxBarManager1Bar5: TdxBar;
    aHorizontalno: TAction;
    aVeritikalno: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    dxComponentPrinter1Link2: TdxDBOrgChartReportLink;
    aPecatiOrg: TAction;
    Panel2: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    cxVerticalGrid1EditorRow1: TcxEditorRow;
    cxVerticalGrid1EditorRow2: TcxEditorRow;
    cxHintStyleController1: TcxHintStyleController;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    aSnimiPecatenjeOrg: TAction;
    aBrisiPodPecOrg: TAction;
    aPodesuvanjePecatenjeOrg: TAction;
    aPageSetupOrg: TAction;
    tblSistematizacija: TpFIBDataSet;
    tblSistematizacijaID: TFIBIntegerField;
    tblSistematizacijaOPIS: TFIBStringField;
    tblSistematizacijaOD_DATUM: TFIBDateField;
    tblSistematizacijaDO_DATUM: TFIBDateField;
    tblSistematizacijaDOKUMENT: TFIBStringField;
    tblSistematizacijaTS_INS: TFIBDateTimeField;
    tblSistematizacijaTS_UPD: TFIBDateTimeField;
    tblSistematizacijaUSR_INS: TFIBStringField;
    tblSistematizacijaUSR_UPD: TFIBStringField;
    dsSitematizacija: TDataSource;
    dxBarLargeButton24: TdxBarLargeButton;
    cxBarEditItem2: TcxBarEditItem;
    cxCheckBox1: TcxCheckBox;
    aVraboten: TAction;
    Label5: TLabel;
    txtBroj: TcxDBTextEdit;
    cxGrid1DBTableView1KOREN: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    tblSisSite: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBDateField1: TFIBDateField;
    FIBDateField2: TFIBDateField;
    FIBStringField2: TFIBStringField;
    FIBDateTimeField1: TFIBDateTimeField;
    FIBDateTimeField2: TFIBDateTimeField;
    FIBStringField3: TFIBStringField;
    FIBStringField4: TFIBStringField;
    dsSisSite: TDataSource;
    Label18: TLabel;
    cxGroupBox1: TcxGroupBox;
    Label17: TLabel;
    Rakovodenje: TcxDBTextEdit;
    Label19: TLabel;
    DenUsloviGO: TcxDBTextEdit;
    txtOdgovaraPred: TcxDBTextEdit;
    cbSisSite: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1ODGOVARA_PRED: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA_PROCENT: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn;
    tblUsloviPoteskiOdNormalni: TpFIBDataSet;
    tblUsloviPoteskiOdNormalniID: TFIBIntegerField;
    tblUsloviPoteskiOdNormalniNAZIV: TFIBStringField;
    tblUsloviPoteskiOdNormalniPROCENT: TFIBBCDField;
    tblUsloviPoteskiOdNormalniTS_INS: TFIBDateTimeField;
    tblUsloviPoteskiOdNormalniTS_UPD: TFIBDateTimeField;
    tblUsloviPoteskiOdNormalniUSR_INS: TFIBStringField;
    tblUsloviPoteskiOdNormalniUSR_UPD: TFIBStringField;
    Label7: TLabel;
    BENIFICIRAN_STAZ: TcxDBTextEdit;
    cxGrid1DBTableView1BENIFICIRAN_STAZ: TcxGridDBColumn;
    dxBarManager1Bar8: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aUsloviPoteskiOdNormalnite: TAction;
    Label11: TLabel;
    dxBarManager1Bar9: TdxBar;
    StatusSistematizacija: TcxBarEditItem;
    lbl1: TLabel;
    lbl2: TLabel;
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure orgRMDeletion(Sender: TObject; Node: TdxOcNode);
    procedure orgRMCreateNode(Sender: TObject; Node: TdxOcNode);
    procedure FormCreate(Sender: TObject);
    procedure aHorizontalnoExecute(Sender: TObject);
    procedure aVeritikalnoExecute(Sender: TObject);
    procedure aPecatiOrgExecute(Sender: TObject);
    procedure orgRMSetText(Sender: TObject; Node: TdxOcNode;
      const Text: string);
    procedure aPodesuvanjePecatenjeOrgExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxBarEditItem2CurChange(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aUsloviPoteskiOdNormalniteExecute(Sender: TObject);
    procedure StatusSistematizacijaPropertiesChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRMRE: TfrmRMRE;
  pom_sistematizacija: Integer;
implementation

uses dmKonekcija, dmMaticni,  dmResources, dmSistematizacija,dmUnit,dmSystem,
  VraboteniHRM, VrabotenRM, dmUnitOtsustvo, Utils, RabotnoMestoUsloviRabota;
type
 TNodeData = record
    Color: TColor;
    Name: string;
    slika: TImage;
  end;
  PNodeData = ^TNodeData;

{$R *.dfm}

procedure TfrmRMRE.orgRMCreateNode(Sender: TObject; Node: TdxOcNode);
begin
  inherited;
  with Node, dmSis.tblOrgRMRE do
  begin
   // if FindField('id').AsInteger > 50 then
    //  Width := FindField('id').AsInteger;
   // if FindField('height').AsInteger > 50 then
    //  Height := FindField('height').AsInteger;
  //  Color := FindField('color').AsInteger;
  //  Node.Text:=getd
//     slika := FindField('slika')Value;
    Color := FindField('boja').AsInteger;
    Data := New(PNodeData);

    PNodeData(Data)^.Color := Color;
    PNodeData(Data)^.Name := FindField('NAZIV_RE').AsString;
   // PNodeData(Data)^.slika := FindField('SLIKA').AsExtended;
  end;
end;

procedure TfrmRMRE.orgRMDeletion(Sender: TObject; Node: TdxOcNode);
begin
  inherited;
 if Assigned(Node.Data) then
    Dispose(PNodeData(Node.Data));
end;

procedure TfrmRMRE.orgRMSetText(Sender: TObject; Node: TdxOcNode;
  const Text: string);
begin
  inherited;
//   if dmsis.tblOrgRMRENAZIV_VRABOTEN.Text<>'' then
//   begin
//     Node.Text:='('+dmSis.tblOrgRMRENAZIV_VRABOTEN.AsString+')';
//   end;
end;

procedure TfrmRMRE.StatusSistematizacijaPropertiesChange(Sender: TObject);
begin
  inherited;
     dmSis.tblRMRE.Close;
     dmSis.tblRMRE.ParamByName('param').Value:=StatusSistematizacija.EditValue;
     dmSis.tblRMRE.ParamByName('firma').AsInteger:=firma;
     dmSis.tblRMRE.Open;
end;

procedure TfrmRMRE.aAzurirajExecute(Sender: TObject);
begin
  inherited;
  cbSis.Visible:=true;
  cbSisSite.Visible:=false;
end;

procedure TfrmRMRE.aHelpExecute(Sender: TObject);
begin
  inherited;
   Application.HelpContext(7);
end;

procedure TfrmRMRE.aHorizontalnoExecute(Sender: TObject);
begin
  inherited;
  orgRM.Rotated:=False;
end;

procedure TfrmRMRE.aNovExecute(Sender: TObject);
begin
  inherited;
    cbSis.Visible:=true;
    cbSisSite.Visible:=false;
    dmSis.tblRMREID_RE_FIRMA.Value:=firma;
end;

procedure TfrmRMRE.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  cbSis.Visible:=False;
  cbSisSite.Visible:=True;
end;

procedure TfrmRMRE.aPecatiOrgExecute(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1Link2.ReportTitle.Text := '������� ����� �� ������� �������';
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmRMRE.aPodesuvanjePecatenjeOrgExecute(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmRMRE.aUsloviPoteskiOdNormalniteExecute(Sender: TObject);
begin
  inherited;
  frmRmReUsloviRabota:=TfrmRmReUsloviRabota.Create(Self, true);
  frmRmReUsloviRabota.Tag:=1;
  frmRmReUsloviRabota.ShowModal;
  frmRmReUsloviRabota.Free;
end;

procedure TfrmRMRE.aVeritikalnoExecute(Sender: TObject);
begin
  inherited;
  orgRM.Rotated:=true;
end;

procedure TfrmRMRE.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        //dmSis.tblRMREBROJ.Value:=dmMat.zemiMax(dmsis.pMaxBrRMRE,'firma', 'id_s', Null, firma, txtSis.Text, Null, 'MAXBR');
        cxGrid1DBTableView1.DataController.DataSet.Post;
        cbSis.Visible:=False;
        cbSisSite.Visible:=True;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
    end;
  end;

end;

procedure TfrmRMRE.cxBarEditItem2CurChange(Sender: TObject);
begin
  inherited;
  if orgRM.zoom then orgRM.Zoom:=false else orgRM.zoom:=true;
end;

procedure TfrmRMRE.cxCheckBox1Click(Sender: TObject);
begin
  inherited;
  if orgRM.zoom then orgRM.Zoom:=false else orgRM.zoom:=true;
end;

procedure TfrmRMRE.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if (Sender=TEdit(txtBroj)) and (dmSis.tblRMRE.State=dsInsert) then
  begin
       dmSis.tblRMREBROJ.Value:=dmMat.zemiMax(dmsis.pMaxBrRMRE,'firma', 'id_s', Null, firma, txtSis.Text, Null, 'MAXBR');
  end;
end;

procedure TfrmRMRE.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
  inherited;
     if ANewTab = rtOrganogram then
        begin
          dmSis.tblOrgRMRE.Close;
          dmSis.tblOrgRMRE.ParamByName('re').Asinteger:=firma;
          dmSis.tblOrgRMRE.Open;
          lpanel.Align:=alNone;
          dpanel.Align:=alNone;
          lPanel.Width:=0;
          dPanel.Width:=0;
          panel1.Align:=alClient;
          WindowState:=wsMaximized;
        end
     else if (ANewTab = dxRibbon1Tab1) or (ANewTab = dxRibbon1Tab2)
      then
        begin
          lPanel.Width:=667;
          dPanel.Width:=649;
          panel1.Align:=alNone;
          panel1.Width:=0;
          dpanel.Align:=alBottom;
          lpanel.Align:=alClient;
          WindowState:=wsNormal;
        end;
end;

procedure TfrmRMRE.FormCreate(Sender: TObject);
begin
  inherited;
   lPanel.Width:=667;
   dPanel.Width:=649;
   panel1.Width:=0;
   dpanel.Align:=alBottom;
   lpanel.Align:=alClient;
end;

procedure TfrmRMRE.FormShow(Sender: TObject);
begin
  inherited;
   tblSistematizacija.ParamByName('firma').AsInteger:=firma;
   tblSistematizacija.Open;
   tblSisSite.ParamByName('firma').AsInteger:=firma;
   tblSisSite.Open;
   dmsis.tblReVoFirma.close;
   dmSis.tblReVoFirma.ParamByName('re').AsInteger:=firma;
   dmsis.tblReVoFirma.open;

   //pom_sistematizacija:=0;
   StatusSistematizacija.EditValue:=1;
end;

end.
