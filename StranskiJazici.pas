unit StranskiJazici;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus, cxCheckBox, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPSCore, dxPScxCommon,  ActnList, dxBar, cxBarEditItem,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, cxDropDownEdit, dxBarSkinnedCustForm,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk,
  dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxScreenTip;

type
  TfrmStarnskiJazik = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxDBTextEdit1: TcxDBTextEdit;
    Label2: TLabel;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStarnskiJazik: TfrmStarnskiJazik;

implementation

uses dmMaticni, dmSistematizacija, Utils;

{$R *.dfm}

procedure TfrmStarnskiJazik.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
   case Key of
       VK_RETURN:
       begin
         if (tag = 1) and (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
            Close;
       end;
  end;
end;

end.
