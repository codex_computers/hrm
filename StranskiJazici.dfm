inherited frmStarnskiJazik: TfrmStarnskiJazik
  Caption = #1057#1090#1088#1072#1085#1089#1082#1080' '#1032#1072#1079#1080#1094#1080
  ClientHeight = 536
  ClientWidth = 602
  ExplicitWidth = 618
  ExplicitHeight = 574
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 274
    ExplicitWidth = 602
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 270
      ExplicitWidth = 598
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dmSis.dsStranskiJazici
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 329
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Width = 602
    Height = 113
    ExplicitTop = 400
    ExplicitWidth = 602
    ExplicitHeight = 113
    inherited Label1: TLabel
      Top = 9
      Visible = False
      ExplicitTop = 9
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 36
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsStranskiJazici
      TabOrder = 1
      Visible = False
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 73
      TabOrder = 3
      ExplicitLeft = 511
      ExplicitTop = 73
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 73
      TabOrder = 2
      ExplicitLeft = 430
      ExplicitTop = 73
    end
    object cxDBTextEdit1: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 33
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsStranskiJazici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 356
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 602
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 513
    Width = 602
    ExplicitTop = 513
    ExplicitWidth = 602
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40336.435646087960000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
