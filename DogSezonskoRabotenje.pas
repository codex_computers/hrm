unit DogSezonskoRabotenje;

{*******************************************************}
{                                                       }
{     ��������� : ������ ��������                      }
{                 ������ �������                       }
{                                                       }
{     ����� : 21.12.2011                                }
{                                                       }
{     ������ : 1.1.1.17                                }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon, ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, cxCalendar, cxVariants,
  cxCheckGroup, cxCheckBox, cxCheckComboBox, cxRadioGroup, dxRibbonSkins,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxPScxSchedulerLnk,
  dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxCustomHint, cxHint;
  type
  TfrmDogSezonskoRabotenje = class(TfrmMaster)
    Period: TcxGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    DATUM_OD: TcxDBDateEdit;
    DATUM_DO: TcxDBDateEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1FIRMANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1PLATA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarButton1: TdxBarButton;
    aFilterAktivni: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    afilterSite: TAction;
    cxBarEditItem2: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    cxBarEditItem4: TcxBarEditItem;
    cxBarEditItem5: TcxBarEditItem;
    cxBarEditItem6: TcxBarEditItem;
    dxBarButton4: TdxBarButton;
    Label10: TLabel;
    Broj: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    PodatociZaRabotnik: TcxGroupBox;
    IME: TcxDBTextEdit;
    Label5: TLabel;
    TATKOVO_IME: TcxDBTextEdit;
    Label4: TLabel;
    PREZIME: TcxDBTextEdit;
    Label3: TLabel;
    MB: TcxDBTextEdit;
    Label2: TLabel;
    PodatociZaVrabotuvanje: TcxGroupBox;
    RABOTNO_MESTO: TcxDBTextEdit;
    Label14: TLabel;
    RABOTNO_MESTO_NAZIV: TcxDBLookupComboBox;
    MB_DIREKTOR: TcxDBTextEdit;
    Label9: TLabel;
    PLATA: TcxDBTextEdit;
    Label8: TLabel;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxBarEditItem6PropertiesChange(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDogSezonskoRabotenje: TfrmDogSezonskoRabotenje;

implementation

uses dmKonekcija, dmUnit, dmSistematizacija, dmUnitOtsustvo, Utils;

{$R *.dfm}

procedure TfrmDogSezonskoRabotenje.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(17);
end;

procedure TfrmDogSezonskoRabotenje.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblDogSezonciVID_DOKUMENT.Value:=dogSezonci;
    dm.tblDogSezonciID_RE_FIRMA.Value:=dmKon.re;
    dm.tblDogSezonciMB.Value:=dm.tblLicaVraboteniMB.Value;
    dm.tblDogSezonciPREZIME.Value:=dm.tblLicaVraboteniPREZIME.Value;
    dm.tblDogSezonciTATKOVO_IME.Value:=dm.tblLicaVraboteniTATKOVO_IME.Value;
    dm.tblDogSezonciIME.Value:=dm.tblLicaVraboteniIME.Value;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmDogSezonskoRabotenje.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        if (DATUM_OD.Text <> '')and (DATUM_DO.Text <> '') and (DATUM_DO.Date < DATUM_OD.Date ) then
          begin
               ShowMessage('������ � �������� ������ !!!');
               DATUM_DO.SetFocus;
          end
        else
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;
            cxGrid1.SetFocus;
          end;
    end;
  end;
end;

procedure TfrmDogSezonskoRabotenje.cxBarEditItem6PropertiesChange(
  Sender: TObject);
begin

   if cxBarEditItem6.EditValue = 0 then
   begin
      cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_OD, foLessEqual, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_DO, foGreater, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
   end
   else
     cxGrid1DBTableView1.DataController.Filter.Clear;
end;

procedure TfrmDogSezonskoRabotenje.FormCreate(Sender: TObject);
begin
  inherited;
   dmSis.tblRabotnoMesto.ParamByName('firma').Value:=dmkon.re;
   dmsis.tblRabotnoMesto.Open;
end;

procedure TfrmDogSezonskoRabotenje.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 1 then
     begin
       dm.tblDogSezonci.Close;
       dm.tblDogSezonci.parambyName('mb').value:=dm.tblLicaVraboteniMB.Value;
       dm.tblDogSezonci.parambyname('firma').value:=dmKon.re;
       dm.tblDogSezonci.Open;
     end
  else
     begin
       dm.tblDogSezonci.Close;
       dm.tblDogSezonci.parambyName('mb').value:='%';
       dm.tblDogSezonci.parambyname('firma').value:= dmKon.re;
       dm.tblDogSezonci.Open;

       dxBarManager1Bar1.Visible:=false;
     end
end;

end.
