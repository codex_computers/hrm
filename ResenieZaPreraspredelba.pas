unit ResenieZaPreraspredelba;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxCalendar, FIBDataSet, pFIBDataSet,
  cxGroupBox, FIBQuery, pFIBQuery, frxDesgn,
  frxClass, frxDBSet, frxRich, frxCross, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint, cxHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, cxNavigator, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmReseniePreraspredelba = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dPanel: TPanel;
    MB: TcxDBTextEdit;
    VRABOTENIME: TcxDBLookupComboBox;
    Label8: TLabel;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1STARO_RM: TcxGridDBColumn;
    cxGrid1DBTableView1NOVO_RM: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    Panel1: TPanel;
    dsRMRE: TDataSource;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1MB: TcxGridDBColumn;
    cxGridDBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    cxGridDBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGridDBTableView1DATUM: TcxGridDBColumn;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    NOVO_RM_NAZIV: TcxDBLookupComboBox;
    NOVO_RM: TcxDBTextEdit;
    Novo_od: TcxDBDateEdit;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    staro_rm: TcxDBTextEdit;
    ID_RM_RE_NAZIV: TcxDBLookupComboBox;
    Label6: TLabel;
    staro_do: TcxDBDateEdit;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    aPreraspredeliRabotnik: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPecatiPreraspredelba: TAction;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGridDBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGridDBTableView1NOVO_RM: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    cxGridDBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGridDBTableView1STARO_RM: TcxGridDBColumn;
    cxGridDBTableView1STATUS: TcxGridDBColumn;
    aPrisilnoBrisenje: TAction;
    dxBarManager1Bar5: TdxBar;
    Label3: TLabel;
    Broj: TcxDBTextEdit;
    cxGridDBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGridDBTableView1BROJ: TcxGridDBColumn;
    Label4: TLabel;
    txtDatumKreiranje: TcxDBDateEdit;
    cxGridDBTableView1DATUM_KREIRANJE: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    dxBarLargeButton20: TdxBarLargeButton;
    aBrisiDokument: TAction;
    cxGridDBTableView1DATA: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarLargeButton21: TdxBarLargeButton;
    qStaroMesto: TpFIBQuery;
    dxbrlrgbtn1: TdxBarLargeButton;
    tblRMRESIS_OPIS: TFIBStringField;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VRABOTENIMEExit(Sender: TObject);
    procedure VRABOTENIMEPropertiesEditValueChanged(Sender: TObject);
    procedure aPecatiPreraspredelbaExecute(Sender: TObject);
    procedure aPrisilnoBrisenjeExecute(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure dxBarLargeButton7Click(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmReseniePreraspredelba: TfrmReseniePreraspredelba;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmUnitOtsustvo,
  dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmReseniePreraspredelba.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmReseniePreraspredelba.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    Broj.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblReseniePreraspredelbaDATUM_KREIRANJE.Value:=now;
    dm.tblReseniePreraspredelbaVID_DOKUMENT.Value:=reseniePreraspredelba;
    dm.tblReseniePreraspredelbaDATUM_OD.Value:=Now + 1;
    dm.tblReseniePreraspredelbaDATUM_DO.Value:=Now;
    dm.tblReseniePreraspredelbaID_RE_FIRMA.value:=dmKon.re;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmReseniePreraspredelba.aAzurirajExecute(Sender: TObject);
var pom:Integer;
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//       begin
//          pom:=dmOtsustvo.zemiBroj(dm.pUslovPlata, 'DATUM_OD', 'DATUM_DO', 'MB', 'FIRMA', dm.tblReseniePreraspredelbaDATUM_OD.Value,Now, dm.tblReseniePreraspredelbaMB.Value, dmKon.re, 'BROJ');
//          if pom > 0 then
//             begin
//                ShowMessage('�������� �� �� ��������� ��� �����. ��� � ���������� ����� �� ��� ������ !!!');
//             end
//          else
//             begin
//               dPanel.Enabled:=True;
//               lPanel.Enabled:=False;
//               staro_do.SetFocus;
//               MB.Enabled:=false;
//               VRABOTENIME.Enabled:=false;
//               cxGrid1DBTableView1.DataController.DataSet.Edit;
//             end;
//       end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmReseniePreraspredelba.aBrisiDokumentExecute(Sender: TObject);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteReseniePreraspredelba.Close;
            dmOtsustvo.qDeleteReseniePreraspredelba.ParamByName('id').Value:=dm.tblReseniePreraspredelbaID.Value;
            dmOtsustvo.qDeleteReseniePreraspredelba.ExecQuery;
        end;
end;

procedure TfrmReseniePreraspredelba.aBrisiExecute(Sender: TObject);
var pom, pom1, f, d:Integer;
    m:string;
begin
      if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
          begin
               frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
               if (frmDaNe.ShowModal <> mrYes) then
                 Abort
               else   dmOtsustvo.insert10(dm.pPreraspredelbaRM, 'MB', 'FIRMA', 'DATUM_KREIRANJE', 'RM', 'DATUMNOV_OD', 'DATUMSTAR_DO', 'DOCUMENTID','DOCUMENTVID', 'TIP',null,
                                 dm.tblReseniePreraspredelbaMB.Value,dmKon.re, dm.tblReseniePreraspredelbaDATUM_KREIRANJE.Value,
                                 dm.tblReseniePreraspredelbaNOVO_RM.Value, dm.tblReseniePreraspredelbaDATUM_OD.Value, dm.tblReseniePreraspredelbaDATUM_DO.Value,
                                 dm.tblReseniePreraspredelbaID.Value, dm.tblReseniePreraspredelbaVID_DOKUMENT.Value,2,null);
                                 dm.tblReseniePreraspredelba.FullRefresh;
          end;
end;

//	����� �� ���������� �� ����������
procedure TfrmReseniePreraspredelba.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmReseniePreraspredelba.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmReseniePreraspredelba.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGridDBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmReseniePreraspredelba.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid2, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmReseniePreraspredelba.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmReseniePreraspredelba.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmReseniePreraspredelba.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmReseniePreraspredelba.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmReseniePreraspredelba.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmReseniePreraspredelba.VRABOTENIMEExit(Sender: TObject);
var pom:String;
begin
     TEdit(Sender).Color:=clWhite;
     if (cxGridDBTableView1.DataController.DataSource.State = dsEdit) or (cxGridDBTableView1.DataController.DataSource.State = dsInsert) then
        begin
          if (VRABOTENIME.Text<> '')then
             begin
                qStaroMesto.Close;
                qStaroMesto.ParamByName('mb').Value:=mb.Text;
                qStaroMesto.ParamByName('firma').Value:=dmKon.re;
                qStaroMesto.ParamByName('datum_kreiranje').Value:=dm.tblReseniePreraspredelbaDATUM_KREIRANJE.Value;
                qStaroMesto.ExecQuery;
                dm.tblReseniePreraspredelbastaro_rm.Value:= qStaroMesto.FldByName['id_rm_re'].Value;
             end;
        end;
end;

procedure TfrmReseniePreraspredelba.VRABOTENIMEPropertiesEditValueChanged(
  Sender: TObject);
begin
      
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmReseniePreraspredelba.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmReseniePreraspredelba.prefrli;
begin
end;

procedure TfrmReseniePreraspredelba.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
    dm.viewVraboteni.Close;
    dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
    dm.viewVraboteni.ParamByName('mb').Value:='%';
    dm.viewVraboteni.ParamByName('re').Value:='%';
    dm.viewVraboteni.Open;
end;
procedure TfrmReseniePreraspredelba.FormCreate(Sender: TObject);
begin
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
     // ProcitajFormaIzgled(self);
     dm.tblReseniePreraspredelba.close;
     dm.tblReseniePreraspredelba.ParamByName('firma').Value:=dmKon.re;
     dm.tblReseniePreraspredelba.open;

     tblRMRE.ParamByName('firma').Value:=dmKon.re;
     tblRMRE.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmReseniePreraspredelba.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
end;
//------------------------------------------------------------------------------

procedure TfrmReseniePreraspredelba.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmReseniePreraspredelba.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmReseniePreraspredelba.dxBarLargeButton7Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmReseniePreraspredelba.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
        dmOtsustvo.insert10(dm.pPreraspredelbaRM, 'MB', 'FIRMA', 'DATUM_KREIRANJE', 'RM', 'DATUMNOV_OD', 'DATUMSTAR_DO', 'DOCUMENTID','DOCUMENTVID', 'TIP',null,
                          dm.tblReseniePreraspredelbaMB.Value,dmKon.re, dm.tblReseniePreraspredelbaDATUM_KREIRANJE.Value,
                          dm.tblReseniePreraspredelbaNOVO_RM.Value, dm.tblReseniePreraspredelbaDATUM_OD.Value, dm.tblReseniePreraspredelbaDATUM_DO.Value,
                          dm.tblReseniePreraspredelbaID.Value, dm.tblReseniePreraspredelbaVID_DOKUMENT.Value,1,Null);
        dm.tblReseniePreraspredelba.FullRefresh;
    end;
  end;
  end;

//	����� �� ���������� �� �������
procedure TfrmReseniePreraspredelba.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      MB.Enabled:=true;
      VRABOTENIME.Enabled:=true;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmReseniePreraspredelba.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmReseniePreraspredelba.aPecatiPreraspredelbaExecute(
  Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.tblReseniePreraspredelbaTemplate.close;
  dmReport.tblReseniePreraspredelbaTemplate.ParamByName('param').Value:=dm.tblReseniePreraspredelbaDATUM_KREIRANJE.Value;
  dmReport.tblReseniePreraspredelbaTemplate.ParamByName('id').Value:=dm.tblReseniePreraspredelbaID.Value;
  dmReport.tblReseniePreraspredelbaTemplate.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.tblReseniePreraspredelbaTemplateDATA.IsNull then
     begin
       RptStream := dmReport.tblReseniePreraspredelbaTemplate.CreateBlobStream(dmReport.tblReseniePreraspredelbaTemplateDATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
       begin
       RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
       If RichView <> Nil Then
         begin
           RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
           RichView.DataSet:=dmReport.frxReseniePreraspredelbaTemplate;
         end;
       MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
       if MasterData <> Nil then
          MasterData.DataSet:=dmReport.frxReseniePreraspredelbaTemplate;

       dmReport.frxReport1.ShowReport;

       frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 4);
       end;
     end;


end;
procedure TfrmReseniePreraspredelba.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmReseniePreraspredelba.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmReseniePreraspredelba.aPrisilnoBrisenjeExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
       cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmReseniePreraspredelba.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmReseniePreraspredelba.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmReseniePreraspredelba.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmReseniePreraspredelba.aHelpExecute(Sender: TObject);
begin
      Application.HelpContext(123);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmReseniePreraspredelba.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmReseniePreraspredelba.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
