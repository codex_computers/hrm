unit IskustvoRabotno;


(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxDropDownEdit,
  cxCalendar, cxMemo, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint;

type
  TfrmIskustvoRabotno = class(TfrmMaster)
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTODAVAC: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DEJNOST: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1ZADACI: TcxGridDBColumn;
    Label10: TLabel;
    Label11: TLabel;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label17: TLabel;
    Label16: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    ZADACI: TcxDBMemo;
    RABOTODAVAC: TcxDBMemo;
    VID_DEJNOST: TcxDBMemo;
    RABOTNO_MESTO: TcxDBMemo;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    Label12: TLabel;
    DATUM_DO: TcxDBDateEdit;
    DATUM_OD: TcxDBDateEdit;
    ID_MOLBA: TcxDBTextEdit;
    IMEPREZIME: TcxDBLookupComboBox;
    MB_VRABOTEN: TcxDBTextEdit;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    procedure RABOTNO_MESTODblClick(Sender: TObject);
    procedure VID_DEJNOSTDblClick(Sender: TObject);
    procedure RABOTODAVACDblClick(Sender: TObject);
    procedure ZADACIDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure DATUM_ODExit(Sender: TObject);
    procedure DATUM_DOExit(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIskustvoRabotno: TfrmIskustvoRabotno;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  Notepad, Utils, Molbi;

{$R *.dfm}

procedure TfrmIskustvoRabotno.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2) then
       MB_VRABOTEN.SetFocus;
    if (tag = 4) then
       ID_MOLBA.SetFocus;
    if (tag = 5 ) or (tag = 3)then
       DATUM_OD.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmIskustvoRabotno.aNovExecute(Sender: TObject);
begin
    if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
      begin
        dPanel.Enabled:=True;
        lPanel.Enabled:=False;
        if (tag = 2) then
          MB_VRABOTEN.SetFocus;
        if (tag = 4) then
          ID_MOLBA.SetFocus;
        cxGrid1DBTableView1.DataController.DataSet.Insert;
        if tag = 5 then
           begin
             dmSis.tblRabotnoIskustvoID_MOLBA.Value:=dmSis.tblMolbaID.Value;
             DATUM_OD.SetFocus;
           end;
        if tag = 3 then
           begin
             dmSis.tblRabotnoIskustvoMB_VRABOTEN.Value:=dm.tblLicaVraboteniMB.Value;
             DATUM_OD.SetFocus;
           end;
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmIskustvoRabotno.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if (DATUM_OD.Text <> '')and (DATUM_DO.Text <> '') and (DATUM_DO.Date < DATUM_OD.Date ) then
        begin
          ShowMessage('������ � �������� ������ !!!');
          DATUM_DO.SetFocus;
        end
      else
        begin
          if ((st = dsInsert) and inserting) then
            begin
              cxGrid1DBTableView1.DataController.DataSet.Post;
              aNov.Execute;
            end;

          if ((st = dsInsert) and (not inserting)) then
            begin
              cxGrid1DBTableView1.DataController.DataSet.Post;
              dPanel.Enabled:=false;
              lPanel.Enabled:=true;
              cxGrid1.SetFocus;
            end;
          if (st = dsEdit) then
            begin
              cxGrid1DBTableView1.DataController.DataSet.Post;
              dPanel.Enabled:=false;
              lPanel.Enabled:=true;
              cxGrid1.SetFocus;
            end;
        end;
      end;
    end
end;

procedure TfrmIskustvoRabotno.DATUM_DOExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmIskustvoRabotno.DATUM_ODExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmIskustvoRabotno.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if tag = 3 then
     begin
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 0) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 1) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:=dmSis.tblOglasID.Value;
       dmSis.tblMolba.FullRefresh;
     end;
end;

procedure TfrmIskustvoRabotno.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 2 then
     begin
       dmSis.tblRabotnoIskustvo.close;
       dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:='%';
       dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:='0';
       dmSis.tblRabotnoIskustvo.open;

       dm.tblLica.close;
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.Open;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;
  if tag = 3 then
     begin
       dmSis.tblRabotnoIskustvo.close;
       dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:='0';
       dmSis.tblRabotnoIskustvo.open;

       dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblLica.FullRefresh;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;

  if tag = 4 then
     begin
       dmSis.tblRabotnoIskustvo.close;
       dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:='0';
       dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:='%';
       dmSis.tblRabotnoIskustvo.open;

       dmSis.tblMolba.close;
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.Open;

       VRABOTENNAZIV.Visible:=False;
       MB_VRABOTEN.Visible:=False;
       Label11.Visible:=False;

       cxGrid1DBTableView1ID_MOLBA.Visible:=true;
       cxGrid1DBTableView1Column2.Visible:=true;
       cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
       cxGrid1DBTableView1Column1.Visible:=false;

       ID_MOLBA.Tag:=1;
       IMEPREZIME.Tag:=1;
     end;
  if tag = 5 then
     begin
        dmSis.tblRabotnoIskustvo.close;
        dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:='0';
        dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblRabotnoIskustvo.open;

        dmSis.tblMolba.ParamByName('ID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblMolba.ParamByName('oglasID').Value:='%';
        dmSis.tblMolba.FullRefresh;

        VRABOTENNAZIV.Visible:=False;
        MB_VRABOTEN.Visible:=False;
        Label11.Visible:=False;

        cxGrid1DBTableView1ID_MOLBA.Visible:=true;
        cxGrid1DBTableView1Column2.Visible:=true;
        cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
        cxGrid1DBTableView1Column1.Visible:=false;

        ID_MOLBA.Tag:=1;
        IMEPREZIME.Tag:=1;
     end;
end;

procedure TfrmIskustvoRabotno.RABOTNO_MESTODblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoIskustvoRABOTNO_MESTO.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRabotnoIskustvoRABOTNO_MESTO.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmIskustvoRabotno.RABOTODAVACDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoIskustvoRABOTODAVAC.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRabotnoIskustvoRABOTODAVAC.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmIskustvoRabotno.VID_DEJNOSTDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoIskustvoVID_DEJNOST.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRabotnoIskustvoVID_DEJNOST.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmIskustvoRabotno.ZADACIDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblRabotnoIskustvoZADACI.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblRabotnoIskustvoZADACI.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

end.
