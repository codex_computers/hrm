{*******************************************************}
{                                                       }
{     ��������� : ������ ��������                      }
{                                                       }
{     ����� : 03.05.2010                                }
{                                                       }
{     ������: 1.0.0.0                                  }
{                                                       }
{*******************************************************}

unit Kategorija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMemo, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxHint,
  FIBQuery, pFIBQuery, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint;

type
  TfrmKategorija = class(TfrmMaster)
    cbRmRe: TcxDBLookupComboBox;
    Label2: TLabel;
    txtMemo: TcxDBMemo;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn;
    txtRmRe: TcxDBTextEdit;
    cxHintStyleController1: TcxHintStyleController;
    Label3: TLabel;
    txtBroj: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    qMaxBezRM: TpFIBQuery;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    od_kade:integer;
    { Public declarations }
  end;

var
  frmKategorija: TfrmKategorija;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmKategorija.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if (Sender=TEdit(cbRmRe)) and (cxGrid1DBTableView1.DataController.DataSet.State=dsInsert) and (cbRmRe.Text<>'')
       then
          begin
            dmOtsustvo.pMaksKBroj.close;
            dmOtsustvo.pMaksKBroj.ParamByName('rm').AsString:=txtRmRe.Text;
            dmOtsustvo.pMaksKBroj.ExecQuery;
            dmSis.tblKategorjaBROJ.Value:=dmOtsustvo.pMaksKBroj.FldByName['maks'].Value;
            txtmemo.SetFocus;
          end
    else if (Sender=TEdit(cbRmRe)) and (cxGrid1DBTableView1.DataController.DataSet.State=dsInsert) and (cbRmRe.Text='')  then
    begin
       dmOtsustvo.pMaksBrojNull.close;
       dmOtsustvo.pMaksBrojNull.ExecQuery;
       dmSis.tblKategorjaBROJ.Value:=dmOtsustvo.pMaksBrojNull.FldByName['maks'].Value;
       txtmemo.SetFocus;
    end;
end;

procedure TfrmKategorija.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  //inherited;

end;

procedure TfrmKategorija.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
   if (key=vk_RETURN) and (not dPanel.Enabled) and (tag=1)  then
   begin
     close;
   end;
end;

procedure TfrmKategorija.FormShow(Sender: TObject);
begin
  dmSis.tblKategorja.close;
  if tag=1 then
     dmSis.tblKategorja.ParamByName('rm').Value:=dmOtsustvo.tblIntervjuID_RM_RE_MOLBA.Value
  else
    dmsis.tblKategorja.ParamByName('rm').AsString:='%';
  dmsis.tblKategorja.ParamByName('firma').asinteger:=firma;
  dmsis.tblKategorja.Open;
  inherited;

end;

end.
