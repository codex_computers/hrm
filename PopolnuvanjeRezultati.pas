unit PopolnuvanjeRezultati;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls,
  cxContainer, StdCtrls, cxLabel, cxDBLabel, cxCustomPivotGrid, cxDBPivotGrid,
  cxGridChartView, cxGridDBChartView, dxmdaset, cxPivotGridCustomDataSet,
  cxPivotGridSummaryDataSet, cxPC, cxPivotGridDrillDownDataSet, ActnList,
  ComCtrls, dxSkinsdxBarPainter, cxCheckBox, dxSkinsdxRibbonPainter, dxRibbon,
  dxBar, cxBarEditItem, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,
  cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, Menus, cxPCdxBarPopupMenu, dxRibbonSkins,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk;

type
  TfrmVnesNaOcenki = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    cxDBLabel1: TcxDBLabel;
    Label5: TLabel;
    Label1: TLabel;
    cxDBLabel2: TcxDBLabel;
    cxPivotGridSummaryDataSet1: TcxPivotGridSummaryDataSet;
    dsPivot1: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1PRASANJENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OCENUVACNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OCENA: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ActionList2: TActionList;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    Action1: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    sys: TcxDBPivotGrid;
    cxDBPivotGrid1Prasanje: TcxDBPivotGridField;
    cxDBPivotGrid1Ocenuvac: TcxDBPivotGridField;
    cxDBPivotGrid1Ocena: TcxDBPivotGridField;
    cxTabSheet4: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBChartView1: TcxGridDBChartView;
    cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid2Level1: TcxGridLevel;
    dxBarLargeButton10: TdxBarLargeButton;
    aPecatiTabelaren: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    aPecatiGraficki: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPecatiRezultati: TAction;
    dxComponentPrinter1Link2: TcxPivotGridReportLink;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxComponentPrinter1Link3: TdxGridReportLink;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxDBPivotGrid1Field1: TcxDBPivotGridField;
    cxGrid2DBChartView1DataGroup3: TcxGridDBChartDataGroup;
    PopupMenu1: TPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure aPecatiTabelarenExecute(Sender: TObject);
    procedure aPecatiGrafickiExecute(Sender: TObject);
    procedure aPecatiRezultatiExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVnesNaOcenki: TfrmVnesNaOcenki;

implementation

uses dmUnitUcinok, Utils, dmResources;

{$R *.dfm}

procedure TfrmVnesNaOcenki.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmVnesNaOcenki.Action1Execute(Sender: TObject);
begin
     Close;
end;

procedure TfrmVnesNaOcenki.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmVnesNaOcenki.aPecatiGrafickiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link3.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link3);
end;

procedure TfrmVnesNaOcenki.aPecatiRezultatiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmVnesNaOcenki.aPecatiTabelarenExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmVnesNaOcenki.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmVnesNaOcenki.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmVnesNaOcenki.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmVnesNaOcenki.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmVnesNaOcenki.FormCreate(Sender: TObject);
begin
     dmUcinok.tblRezultati.Close;
     dmUcinok.tblRezultati.ParamByName('fID').Value:=dmUcinok.tblFeedBackID.Value;
     dmUcinok.tblRezultati.ParamByName('MB').Value:=dmUcinok.tblIspitaniciMB.Value;
     dmUcinok.tblRezultati.Open;
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmVnesNaOcenki.FormShow(Sender: TObject);
begin
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     cxGrid1.SetFocus;

     cxDBPivotGrid1Field1.ExpandAll;
end;

end.
