unit PregledScanDocument;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, ShellConsts, ShellAPI, ShellCtrls, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxScreenTip;

type
//  niza = Array[1..5] of Variant;

  TfrmPregledScanDokument = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ZapisiButton: TcxButton;
    aOtvoriDokument: TAction;
    dxBarManager1Bar5: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    cxBarRabEdinica: TcxBarEditItem;
    cxBarGodina: TcxBarEditItem;
    Panel3: TPanel;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label8: TLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    OpenDialog1: TOpenDialog;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PATEKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    OtkaziButton: TcxButton;
    aBrisiDokument: TAction;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aOtvoriDokumentExecute(Sender: TObject);
    procedure VRABOTENIMEPropertiesChange(Sender: TObject);
    procedure cxBarRabEdinicaChange(Sender: TObject);
    procedure cxBarGodinaChange(Sender: TObject);
    procedure cxLookupComboBox2PropertiesEditValueChanged(Sender: TObject);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmPregledScanDokument: TfrmPregledScanDokument;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmUnitOtsustvo,
  dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmPregledScanDokument.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmPregledScanDokument.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPregledScanDokument.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPregledScanDokument.aBrisiDokumentExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort
    else
      begin
        if (not DeleteFile(dm.tblPregledSkeniraniDokPATEKA.Value)) then
           ShowMessage(dm.tblPregledSkeniraniDokPATEKA.Value+' �� ������ ��� �� ���� �� �� �������!')
        else
            dm.tblPregledSkeniraniDok.Delete;
      end;
end;

procedure TfrmPregledScanDokument.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmPregledScanDokument.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPregledScanDokument.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPregledScanDokument.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPregledScanDokument.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPregledScanDokument.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPregledScanDokument.cxBarGodinaChange(Sender: TObject);
begin
     cxLookupComboBox1.Text:='';
     cxLookupComboBox2.Text:='';
     if cxBarRabEdinica.EditValue = dmKon.re then
        begin
          dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
          dm.tblPregledSkeniraniDok.ParamByName('re').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
          dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
          dm.tblPregledSkeniraniDok.FullRefresh;
        end
     else
        begin
          dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
          dm.tblPregledSkeniraniDok.ParamByName('re').Value:=cxBarRabEdinica.EditValue;
          dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
          dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
          dm.tblPregledSkeniraniDok.FullRefresh;
        end
end;

procedure TfrmPregledScanDokument.cxBarRabEdinicaChange(Sender: TObject);
begin
     cxLookupComboBox1.Text:='';
     cxLookupComboBox2.Text:='';
     if cxBarRabEdinica.EditValue = dmKon.re then
        begin
          dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
          dm.tblPregledSkeniraniDok.ParamByName('re').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
          dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
          dm.tblPregledSkeniraniDok.FullRefresh;
        end
     else
        begin
          dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
          dm.tblPregledSkeniraniDok.ParamByName('re').Value:=cxBarRabEdinica.EditValue;
          dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
          dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
          dm.tblPregledSkeniraniDok.FullRefresh;
        end
end;

procedure TfrmPregledScanDokument.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPregledScanDokument.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPregledScanDokument.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPregledScanDokument.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmPregledScanDokument.VRABOTENIMEPropertiesChange(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmPregledScanDokument.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmPregledScanDokument.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPregledScanDokument.prefrli;
begin
end;

procedure TfrmPregledScanDokument.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmPregledScanDokument.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dmOtsustvo.tblPodsektori.close;
  dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
  dmOtsustvo.tblPodsektori.Open;

end;

//------------------------------------------------------------------------------

procedure TfrmPregledScanDokument.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    cxBarGodina.EditValue:=dmKon.godina;
    cxBarRabEdinica.EditValue:=dmKon.re;

    dm.tblPregledSkeniraniDok.close;
    dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
    dm.tblPregledSkeniraniDok.ParamByName('re').Value:='%';;
    dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
    dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
    dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
    dm.tblPregledSkeniraniDok.Open;

end;
//------------------------------------------------------------------------------

procedure TfrmPregledScanDokument.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPregledScanDokument.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPregledScanDokument.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
          if cxBarRabEdinica.EditValue = dmKon.re then
            dm.tblPregledSkeniraniDok.ParamByName('re').Value:='%'
          else
            dm.tblPregledSkeniraniDok.ParamByName('re').Value:=cxBarRabEdinica.EditValue;
          if cxLookupComboBox1.Text <> '' then
             dm.tblPregledSkeniraniDok.ParamByName('mb').Value:=cxLookupComboBox1.EditValue
          else
             dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
          if cxLookupComboBox2.Text <> '' then
             dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:=cxLookupComboBox2.EditValue
          else
             dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
          dm.tblPregledSkeniraniDok.FullRefresh;
end;

procedure TfrmPregledScanDokument.cxLookupComboBox2PropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPregledSkeniraniDok.ParamByName('godina').Value:=cxBarGodina.EditValue;
          if cxBarRabEdinica.EditValue = dmKon.re then
            dm.tblPregledSkeniraniDok.ParamByName('re').Value:='%'
          else
            dm.tblPregledSkeniraniDok.ParamByName('re').Value:=cxBarRabEdinica.EditValue;
          if cxLookupComboBox1.Text <> '' then
             dm.tblPregledSkeniraniDok.ParamByName('mb').Value:=cxLookupComboBox1.EditValue
          else
             dm.tblPregledSkeniraniDok.ParamByName('mb').Value:='%';
          dm.tblPregledSkeniraniDok.ParamByName('firma').Value:=dmKon.re;
          if cxLookupComboBox2.Text <> '' then
             dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:=cxLookupComboBox2.EditValue
          else
             dm.tblPregledSkeniraniDok.ParamByName('vid_dokument').Value:='%';
          dm.tblPregledSkeniraniDok.FullRefresh;
end;

//  ����� �� �����
procedure TfrmPregledScanDokument.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmPregledScanDokument.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

procedure TfrmPregledScanDokument.aOtvoriDokumentExecute(Sender: TObject);
var pom: String;
begin
//      OpenDialog1.InitialDir:=dm.tblPregledSkeniraniDokPATEKA.Value;
//      OpenDialog1.FileName:=dm.tblPregledSkeniraniDokNAZIV.Value;
//      OpenDialog1.Execute();
//      if OpenDialog1.FileName <> '' then
//       begin
//       end;

     pom:=dm.tblPregledSkeniraniDokPATEKA.Value;
     shellExecute(handle,'open',PChar(pom),nil,nil,SW_SHOWNORMAL);

   end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPregledScanDokument.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmPregledScanDokument.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPregledScanDokument.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPregledScanDokument.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPregledScanDokument.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPregledScanDokument.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPregledScanDokument.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPregledScanDokument.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
