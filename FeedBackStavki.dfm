object frmFeedBackStavki: TfrmFeedBackStavki
  Left = 0
  Top = 0
  Caption = '360 '#1055#1086#1074#1088#1090#1072#1085#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1112#1072
  ClientHeight = 467
  ClientWidth = 751
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 751
    Height = 444
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 444
    ClientRectRight = 751
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080
      ImageIndex = 0
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 751
        Height = 382
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGridDBTableView2: TcxGridDBTableView
          OnKeyPress = cxGridDBTableView2KeyPress
          DataController.DataSource = dsHRVraboteni
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnFilteredItemsList = True
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          FilterRow.ApplyChanges = fracImmediately
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          object cxGridDBTableView2Column1: TcxGridDBColumn
            Caption = #1054#1076#1073#1077#1088#1080
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.NullStyle = nssUnchecked
          end
          object cxGridDBTableView2MB_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'MB_OUT'
          end
          object cxGridDBTableView2VRABOTENNAZIV_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'VRABOTENNAZIV_OUT'
            Width = 193
          end
          object cxGridDBTableView2RENAZIV_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'RENAZIV_OUT'
            Width = 163
          end
          object cxGridDBTableView2RABMESTONAZIV_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'RABMESTONAZIV_OUT'
            Width = 216
          end
          object cxGridDBTableView2OBRAZOVANIENAZIV_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'OBRAZOVANIENAZIV_OUT'
            Width = 224
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 382
        Width = 751
        Height = 38
        Align = alBottom
        BevelInner = bvLowered
        TabOrder = 1
        DesignSize = (
          751
          38)
        object ZapisiButton: TcxButton
          Left = 33
          Top = 6
          Width = 75
          Height = 24
          Action = aOdberiIspitanici
          Anchors = []
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 0
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1054#1094#1077#1085#1091#1074#1072#1095#1080
      ImageIndex = 1
      ExplicitTop = 0
      ExplicitWidth = 626
      ExplicitHeight = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 751
        Height = 382
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 626
        object cxGridDBTableView1: TcxGridDBTableView
          OnKeyPress = cxGridDBTableView1KeyPress
          DataController.DataSource = dsRatingFrom
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnFilteredItemsList = True
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          FilterRow.ApplyChanges = fracImmediately
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          object cxGridDBTableView1Column1: TcxGridDBColumn
            Caption = #1054#1076#1073#1077#1088#1080
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.NullStyle = nssUnchecked
          end
          object cxGridDBTableView1ID_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'ID_OUT'
          end
          object cxGridDBTableView1NAZIV_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV_OUT'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 382
        Width = 751
        Height = 38
        Align = alBottom
        BevelInner = bvLowered
        TabOrder = 1
        ExplicitWidth = 626
        DesignSize = (
          751
          38)
        object cxButton1: TcxButton
          Left = 33
          Top = 6
          Width = 75
          Height = 24
          Action = aOdberiOcenuvaci
          Anchors = []
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 0
          ExplicitLeft = 21
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #1055#1088#1072#1096#1072#1083#1085#1080#1082
      ImageIndex = 2
      ExplicitTop = 0
      ExplicitWidth = 626
      ExplicitHeight = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 41
        Width = 751
        Height = 341
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 626
        object cxGridDBTableView3: TcxGridDBTableView
          OnKeyPress = cxGridDBTableView3KeyPress
          DataController.DataSource = dsPrasanja
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnFilteredItemsList = True
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          FilterRow.ApplyChanges = fracImmediately
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          object cxGridDBTableView3Column1: TcxGridDBColumn
            Caption = #1054#1076#1073#1077#1088#1080
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.NullStyle = nssUnchecked
          end
          object cxGridDBTableView3ID_OUT: TcxGridDBColumn
            DataBinding.FieldName = 'ID_OUT'
            Width = 54
          end
          object cxGridDBTableView3Column5: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV_OUT'
            Width = 300
          end
          object cxGridDBTableView3Column2: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA_OUT'
            Width = 67
          end
          object cxGridDBTableView3Column3: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANAZIV_OUT'
            Width = 300
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 382
        Width = 751
        Height = 38
        Align = alBottom
        BevelInner = bvLowered
        TabOrder = 1
        ExplicitWidth = 626
        DesignSize = (
          751
          38)
        object cxButton2: TcxButton
          Left = 33
          Top = 6
          Width = 75
          Height = 24
          Action = aOdberiPrasanja
          Anchors = []
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 0
          ExplicitLeft = 21
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 751
        Height = 41
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 2
        ExplicitWidth = 626
        object Label3: TLabel
          Left = 15
          Top = 14
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1043#1088#1091#1087#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Grupa: TcxLookupComboBox
          Left = 71
          Top = 11
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmUcinok.dsGrupaPrasanja
          Properties.OnEditValueChanged = GrupaPropertiesEditValueChanged
          TabOrder = 0
          Width = 402
        end
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 444
    Width = 751
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dsRatingFrom: TDataSource
    DataSet = tblRatingFrom
    Left = 392
    Top = 272
  end
  object tblRatingFrom: TpFIBDataSet
    SelectSQL.Strings = (
      'select  proc_hr_ocenuvaci.id_out, proc_hr_ocenuvaci.naziv_out'
      'from proc_hr_ocenuvaci(:mas_ID)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dmUcinok.dsFeedBack
    Left = 296
    Top = 272
    object tblRatingFromID_OUT: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID_OUT'
    end
    object tblRatingFromNAZIV_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV_OUT'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object tblHRVraboteni: TpFIBDataSet
    SelectSQL.Strings = (
      'select  proc_hr_ispitanici.mb_out,'
      '        proc_hr_ispitanici.rabmestonaziv_out,'
      '        proc_hr_ispitanici.vrabotennaziv_out,'
      '        proc_hr_ispitanici.renaziv_out,'
      '        proc_hr_ispitanici.obrazovanienaziv_out'
      'from proc_hr_ispitanici(:mas_ID,:FIRMA)'
      'order by 4, 2, 3')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dmUcinok.dsFeedBack
    Left = 296
    Top = 224
    object tblHRVraboteniMB_OUT: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'.'
      FieldName = 'MB_OUT'
      Size = 13
      EmptyStrToNull = True
    end
    object tblHRVraboteniRABMESTONAZIV_OUT: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABMESTONAZIV_OUT'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblHRVraboteniVRABOTENNAZIV_OUT: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077
      FieldName = 'VRABOTENNAZIV_OUT'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblHRVraboteniRENAZIV_OUT: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV_OUT'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblHRVraboteniOBRAZOVANIENAZIV_OUT: TFIBStringField
      DisplayLabel = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      FieldName = 'OBRAZOVANIENAZIV_OUT'
      Size = 1024
      EmptyStrToNull = True
    end
  end
  object dsHRVraboteni: TDataSource
    DataSet = tblHRVraboteni
    Left = 392
    Top = 224
  end
  object dsPrasanja: TDataSource
    DataSet = tblPrasanja
    Left = 392
    Top = 336
  end
  object tblPrasanja: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_hr_prasalnik.id_out,'
      '       proc_hr_prasalnik.naziv_out,'
      '       proc_hr_prasalnik.grupa_out,'
      '       proc_hr_prasalnik.grupanaziv_out,'
      '       proc_hr_prasalnik.normativ'
      'from proc_hr_prasalnik(:mas_ID)'
      'where proc_hr_prasalnik.grupa_out = :grupa')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dmUcinok.dsFeedBack
    Left = 304
    Top = 336
    object tblPrasanjaID_OUT: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID_OUT'
    end
    object tblPrasanjaNAZIV_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV_OUT'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblPrasanjaGRUPA_OUT: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' (ID)'
      FieldName = 'GRUPA_OUT'
    end
    object tblPrasanjaGRUPANAZIV_OUT: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV_OUT'
      Size = 1024
      EmptyStrToNull = True
    end
    object tblPrasanjaNORMATIV: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072#1090#1080#1074
      FieldName = 'NORMATIV'
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 264
    Top = 112
    object aOdberiIspitanici: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 6
      OnExecute = aOdberiIspitaniciExecute
    end
    object aOdberiOcenuvaci: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 6
      OnExecute = aOdberiOcenuvaciExecute
    end
    object aOdberiPrasanja: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 6
      OnExecute = aOdberiPrasanjaExecute
    end
    object aIzlez: TAction
      Caption = 'aIzlez'
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 144
    Top = 160
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 40
    Top = 144
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 248
    Top = 160
  end
end
