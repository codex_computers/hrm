object frmStarosnaStruktura: TfrmStarosnaStruktura
  Left = 0
  Top = 0
  Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1089#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072
  ClientHeight = 783
  ClientWidth = 1047
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1047
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 1
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 760
    Width = 1047
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090', Shift+Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Exce' +
          'l'
        Width = 330
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl2: TcxPageControl
    Left = 0
    Top = 161
    Width = 1047
    Height = 599
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = cxTabSheet3
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 599
    ClientRectRight = 1047
    ClientRectTop = 24
    object cxTabSheet3: TcxTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1089#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072' '#1080' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078
      ImageIndex = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 284
        Width = 1047
        Height = 291
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 0
        object cxPageControl1: TcxPageControl
          Left = 2
          Top = 2
          Width = 1043
          Height = 287
          Align = alClient
          TabOrder = 0
          Properties.ActivePage = cxTabSheet1
          Properties.CustomButtons.Buttons = <>
          ClientRectBottom = 287
          ClientRectRight = 1043
          ClientRectTop = 24
          object cxTabSheet1: TcxTabSheet
            Caption = #1043#1088#1072#1092#1080#1095#1082#1080' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1089#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
            ImageIndex = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object cxGrid1: TcxGrid
              Left = 0
              Top = 0
              Width = 1043
              Height = 263
              Align = alClient
              TabOrder = 0
              object cxGrid1DBChartView1: TcxGridDBChartView
                DataController.DataSource = dm.dsVozrastVraboteniSektor
                DiagramBar.Active = True
                Title.Text = #1057#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072
                ToolBox.DiagramSelector = True
                object cxGrid1DBChartView1DataGroup1: TcxGridDBChartDataGroup
                  DataBinding.FieldName = 'RENAZIV_OUT'
                end
                object cxGrid1DBChartView1DataGroup2: TcxGridDBChartDataGroup
                  DataBinding.FieldName = 'RABMESTONAZIV'
                end
                object cxGrid1DBChartView1Series1: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM1_OUT'
                end
                object cxGrid1DBChartView1Series2: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM2_OUT'
                end
                object cxGrid1DBChartView1Series3: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM3_OUT'
                end
                object cxGrid1DBChartView1Series4: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM4_OUT'
                end
                object cxGrid1DBChartView1Series5: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM5_OUT'
                end
                object cxGrid1DBChartView1Series6: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTNADPARAM5_OUT'
                end
              end
              object cxGrid1Level1: TcxGridLevel
                GridView = cxGrid1DBChartView1
              end
            end
          end
          object cxTabSheet2: TcxTabSheet
            Caption = #1043#1088#1072#1092#1080#1095#1082#1080' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1089#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072' '#1085#1072' '#1094#1077#1083#1072' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1112#1072
            ImageIndex = 1
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object cxGrid3: TcxGrid
              Left = 0
              Top = 0
              Width = 1043
              Height = 263
              Align = alClient
              TabOrder = 0
              object cxGridDBChartView1: TcxGridDBChartView
                DataController.DataSource = dm.dsStarosnaStruktura
                DiagramBar.Active = True
                Title.Text = #1057#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072
                ToolBox.DiagramSelector = True
                object cxGridDBChartView1Series1: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM1_OUT'
                end
                object cxGridDBChartView1Series2: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM2_OUT'
                end
                object cxGridDBChartView1Series3: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM3_OUT'
                end
                object cxGridDBChartView1Series4: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM4_OUT'
                end
                object cxGridDBChartView1Series5: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTPODPARAM5_OUT'
                end
                object cxGridDBChartView1Series6: TcxGridDBChartSeries
                  DataBinding.FieldName = 'COUNTNADPARAM5_OUT'
                end
              end
              object cxGridLevel2: TcxGridLevel
                GridView = cxGridDBChartView1
              end
            end
          end
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1047
        Height = 284
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 1
        object cxGrid2: TcxGrid
          Left = 2
          Top = 2
          Width = 1043
          Height = 280
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsVozrastVraboteni
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1RE_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RE_OUT'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1RENAZIV_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV_OUT'
              Width = 279
            end
            object cxGrid1DBTableView1RM_RE_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RM_RE_OUT'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABMESTONAZIV'
              Width = 161
            end
            object cxGrid1DBTableView1NAZIVVRABOTEN_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIVVRABOTEN_OUT'
              Visible = False
              Width = 250
            end
            object cxGrid1DBTableView1NAZIVVRABOTENTI: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIVVRABOTENTI'
              Width = 250
            end
            object cxGrid1DBTableView1MB_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'MB_OUT'
              Width = 84
            end
            object cxGrid1DBTableView1DATUM_RADJANJE_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_RADJANJE_OUT'
              Width = 100
            end
            object cxGrid1DBTableView1VOZRAST_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'VOZRAST_OUT'
              Width = 47
            end
            object cxGrid1DBTableView1STAZ_GODINI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ_GODINI_OUT'
              Width = 82
            end
            object cxGrid1DBTableView1STAZ_MESECI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ_MESECI_OUT'
              Width = 80
            end
            object cxGrid1DBTableView1STAZ_DENOVI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ_DENOVI_OUT'
              Width = 86
            end
            object cxGrid1DBTableView1BENEFICIRAN_STAZ: TcxGridDBColumn
              DataBinding.FieldName = 'BENEFICIRAN_STAZ_GODINA'
              Width = 150
            end
            object cxGrid1DBTableView1BENEFICIRAN_STAZ_K: TcxGridDBColumn
              DataBinding.FieldName = 'BENEFICIRAN_STAZ_K'
              Width = 190
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 
        #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1089#1090#1072#1088#1086#1089#1085#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1072' '#1080' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1085#1072' '#1086#1076#1088#1077#1076#1077#1085 +
        ' '#1076#1072#1090#1091#1084
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 49
        Width = 1047
        Height = 526
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 0
        object cxGrid4: TcxGrid
          Left = 2
          Top = 2
          Width = 1043
          Height = 522
          Align = alClient
          TabOrder = 0
          object cxGrid4DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid4DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsVozrastNaDatum
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid4DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV_OUT'
              Width = 87
            end
            object cxGrid4DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABMESTONAZIV'
              Width = 88
            end
            object cxGrid4DBTableView1MB_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'MB_OUT'
            end
            object cxGrid4DBTableView1VRABNAZIV_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'VRABNAZIV_OUT'
              Visible = False
              Width = 250
            end
            object cxGrid4DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
              Width = 219
            end
            object cxGrid4DBTableView1DATUMRAGJANJE_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'DATUMRAGJANJE_OUT'
              Width = 87
            end
            object cxGrid4DBTableView1GODINA_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'GODINA_OUT'
              Width = 88
            end
            object cxGrid4DBTableView1MESEC_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'MESEC_OUT'
              Width = 87
            end
            object cxGrid4DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
              Width = 100
            end
            object cxGrid4DBTableView1RABOTNO_MESTO: TcxGridDBColumn
              DataBinding.FieldName = 'RM_RE_OUT'
              Visible = False
              Width = 100
            end
            object cxGrid4DBTableView1STAZ_GODINI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ_GODINI_OUT'
              Width = 82
            end
            object cxGrid4DBTableView1STAZ_MESECI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ_MESECI_OUT'
              Width = 78
            end
            object cxGrid4DBTableView1STAZ_DENOVI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ_DENOVI_OUT'
              Width = 87
            end
          end
          object cxGrid4Level1: TcxGridLevel
            GridView = cxGrid4DBTableView1
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1047
        Height = 49
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 1
        object Datum: TcxDateEdit
          Left = 107
          Top = 19
          Properties.DisplayFormat = 'dd.mm.yyyy'
          Properties.ShowTime = False
          TabOrder = 0
          OnExit = DatumExit
          Width = 121
        end
        object cxLabel1: TcxLabel
          Left = 52
          Top = 22
          Caption = #1044#1072#1090#1091#1084' :'
          Style.TextColor = clNavy
          Style.TextStyle = [fsBold]
        end
      end
    end
  end
  object panel: TPanel
    Left = 0
    Top = 126
    Width = 1047
    Height = 35
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 32
      Top = 11
      Width = 55
      Height = 13
      Caption = #1043#1088#1072#1085#1080#1094#1072' 1:'
    end
    object Label2: TLabel
      Left = 159
      Top = 11
      Width = 55
      Height = 13
      Caption = #1043#1088#1072#1085#1080#1094#1072' 2:'
    end
    object Label3: TLabel
      Left = 287
      Top = 11
      Width = 61
      Height = 13
      Caption = #1043#1088#1072#1085#1080#1094#1072' 13:'
    end
    object Label4: TLabel
      Left = 421
      Top = 11
      Width = 55
      Height = 13
      Caption = #1043#1088#1072#1085#1080#1094#1072' 4:'
    end
    object Label5: TLabel
      Left = 557
      Top = 11
      Width = 55
      Height = 13
      Caption = #1043#1088#1072#1085#1080#1094#1072' 5:'
    end
    object param1: TcxTextEdit
      Left = 90
      Top = 8
      TabOrder = 0
      Text = '20'
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object param2: TcxTextEdit
      Left = 218
      Top = 8
      TabOrder = 1
      Text = '30'
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object param3: TcxTextEdit
      Left = 354
      Top = 8
      TabOrder = 2
      Text = '40'
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object param4: TcxTextEdit
      Left = 482
      Top = 8
      TabOrder = 3
      Text = '50'
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object param5: TcxTextEdit
      Left = 618
      Top = 8
      TabOrder = 4
      Text = '60'
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object cxButton1: TcxButton
      Left = 704
      Top = 4
      Width = 75
      Height = 25
      Action = aPromeniParametri
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 496
    Top = 56
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 584
    Top = 248
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1040#1082#1094#1080#1080' '
      CaptionButtons = <>
      DockedLeft = 612
      DockedTop = 0
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1085#1072' '#1090#1072#1073#1077#1083#1072' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 984
      DockedTop = 0
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 752
      DockedTop = 0
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton14'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1048#1079#1073#1086#1088' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 876
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      SyncImageIndex = False
      ImageIndex = 9
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton3: TdxBarButton
      Action = aPecatiCelaOrganizacija
      Category = 0
      ImageIndex = 30
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPecatiPoSektori
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aPecatiCelaOrganizacija
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = aPecatiPoSektori
      Category = 0
      LargeImageIndex = 30
      SyncImageIndex = False
      ImageIndex = 30
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPecatiCelaOrganizacija
      Category = 0
      LargeImageIndex = 30
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      Category = 0
      Hint = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      Visible = ivAlways
      OnChange = cxBarEditItem1Change
      Width = 500
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsPodsektori
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPregledajPecati
      Category = 0
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42965.404138298610000000
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Component = cxGrid4
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 352
    Top = 256
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1058#1072#1073#1077#1083#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
    end
    object aPecatiPoSektori: TAction
      Caption = #1043#1088#1072#1092#1080#1082#1086#1085' '#39#1055#1086' '#1089#1077#1082#1090#1086#1088#1080#39
      OnExecute = aPecatiPoSektoriExecute
    end
    object aPecatiCelaOrganizacija: TAction
      Caption = #1043#1088#1072#1092#1080#1082#1086#1085'  '#39#1047#1072' '#1094#1077#1083#1072#1090#1072' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1112#1072#39
      OnExecute = aPecatiCelaOrganizacijaExecute
    end
    object aPregledajPecati: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aPregledajPecatiExecute
    end
    object aDizajnPecati: TAction
      Caption = 'aDizajnPecati'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDizajnPecatiExecute
    end
    object aPromeniParametri: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 19
      OnExecute = aPromeniParametriExecute
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 328
    Top = 360
  end
  object PopupMenu1: TPopupMenu
    Left = 456
    Top = 360
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxComponentPrinter2: TdxComponentPrinter
    CurrentLink = dxGridReportLink1
    Version = 0
    Left = 224
    Top = 256
    object dxGridReportLink1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42965.404138333330000000
      BuiltInReportLink = True
    end
  end
  object dxComponentPrinter3: TdxComponentPrinter
    CurrentLink = dxGridReportLink2
    Version = 0
    Left = 144
    Top = 328
    object dxGridReportLink2: TdxGridReportLink
      Component = cxGrid3
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40631.368288888890000000
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 792
    Top = 64
  end
  object dxComponentPrinter4: TdxComponentPrinter
    CurrentLink = dxGridReportLink3
    Version = 0
    Left = 256
    Top = 328
    object dxGridReportLink3: TdxGridReportLink
      Active = True
      Component = cxGrid4
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42965.404138333330000000
      BuiltInReportLink = True
    end
  end
end
