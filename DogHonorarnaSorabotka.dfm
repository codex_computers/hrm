inherited frmDogHonorarnaSorabotka: TfrmDogHonorarnaSorabotka
  Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1076#1077#1083#1086
  ClientHeight = 650
  ExplicitTop = 0
  ExplicitWidth = 749
  ExplicitHeight = 688
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 274
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Height = 270
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsDogHonorarci
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 118
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 109
        end
        object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
          DataBinding.FieldName = 'TATKOVO_IME'
          Visible = False
          Width = 105
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 102
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 250
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 71
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 70
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABMESTONAZIV'
          Width = 170
        end
        object cxGrid1DBTableView1FIRMANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'FIRMANAZIV'
          Width = 194
        end
        object cxGrid1DBTableView1MB_DIREKTOR: TcxGridDBColumn
          Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
          DataBinding.FieldName = 'MB_DIREKTOR'
          Width = 220
        end
        object cxGrid1DBTableView1PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'PLATA'
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Height = 227
    ExplicitTop = 400
    ExplicitHeight = 227
    inherited Label1: TLabel
      Left = 47
      Top = 185
      Visible = False
      ExplicitLeft = 47
      ExplicitTop = 185
    end
    object Label14: TLabel [1]
      Left = 273
      Top = 31
      Width = 94
      Height = 17
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label2: TLabel [2]
      Left = 13
      Top = 55
      Width = 84
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'. :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 13
      Top = 82
      Width = 84
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1088#1077#1079#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [4]
      Left = 13
      Top = 109
      Width = 84
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [5]
      Left = 13
      Top = 136
      Width = 84
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1048#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel [6]
      Left = 321
      Top = 124
      Width = 46
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1083#1072#1090#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label9: TLabel [7]
      Left = 273
      Top = 150
      Width = 94
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label11: TLabel [8]
      Left = -32
      Top = 17
      Width = 129
      Height = 32
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 103
      Top = 182
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsDogHonorarci
      TabOrder = 11
      Visible = False
      ExplicitLeft = 103
      ExplicitTop = 182
    end
    inherited OtkaziButton: TcxButton
      Top = 187
      TabOrder = 12
      OnKeyDown = EnterKakoTab
      ExplicitTop = 187
    end
    inherited ZapisiButton: TcxButton
      Top = 187
      TabOrder = 10
      OnKeyDown = EnterKakoTab
      ExplicitTop = 187
    end
    object cxGroupBox1: TcxGroupBox
      Left = 317
      Top = 55
      Caption = #1055#1077#1088#1080#1086#1076
      TabOrder = 3
      Height = 56
      Width = 400
      object Label6: TLabel
        Left = 8
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 200
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 55
        Top = 22
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsDogHonorarci
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 247
        Top = 22
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsDogHonorarci
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object RABOTNO_MESTO: TcxDBTextEdit
      Tag = 1
      Left = 373
      Top = 28
      BeepOnEnter = False
      DataBinding.DataField = 'RABOTNO_MESTO'
      DataBinding.DataSource = dm.dsDogHonorarci
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 48
    end
    object RABOTNO_MESTO_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 421
      Top = 28
      BeepOnEnter = False
      DataBinding.DataField = 'RABOTNO_MESTO'
      DataBinding.DataSource = dm.dsDogHonorarci
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 200
          FieldName = 'ID'
        end
        item
          Width = 800
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmSis.dsRabotnoMesto
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 296
    end
    object MB: TcxDBTextEdit
      Tag = 1
      Left = 103
      Top = 52
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsDogHonorarci
      Enabled = False
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
    object PREZIME: TcxDBTextEdit
      Tag = 1
      Left = 103
      Top = 79
      BeepOnEnter = False
      DataBinding.DataField = 'PREZIME'
      DataBinding.DataSource = dm.dsDogHonorarci
      Enabled = False
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
    object TATKOVO_IME: TcxDBTextEdit
      Tag = 1
      Left = 103
      Top = 106
      BeepOnEnter = False
      DataBinding.DataField = 'TATKOVO_IME'
      DataBinding.DataSource = dm.dsDogHonorarci
      Enabled = False
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
    object IME: TcxDBTextEdit
      Tag = 1
      Left = 103
      Top = 133
      BeepOnEnter = False
      DataBinding.DataField = 'IME'
      DataBinding.DataSource = dm.dsDogHonorarci
      Enabled = False
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
    object PLATA: TcxDBTextEdit
      Left = 373
      Top = 120
      BeepOnEnter = False
      DataBinding.DataField = 'PLATA'
      DataBinding.DataSource = dm.dsDogHonorarci
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 124
    end
    object MB_DIREKTOR: TcxDBTextEdit
      Left = 373
      Top = 147
      BeepOnEnter = False
      DataBinding.DataField = 'MB_DIREKTOR'
      DataBinding.DataSource = dm.dsDogHonorarci
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 344
    end
    object DatumKreiranje: TcxDBDateEdit
      Tag = 1
      Left = 103
      Top = 22
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsDogHonorarci
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 627
    ExplicitTop = 627
  end
  inherited dxBarManager1: TdxBarManager
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 263
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 438
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 645
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
    object dxBarManager1Bar5: TdxBar [4]
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object cxBarEditItem2: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 1
        end
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 0
        end>
      Properties.OnChange = cxBarEditItem2PropertiesChange
      InternalEditValue = 1
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40673.402545520830000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 872
    Top = 184
  end
end
