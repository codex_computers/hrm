unit IzvestajDokumenti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxMaskEdit, cxDropDownEdit, cxCalendar, cxGridChartView,
  cxGridDBChartView, cxDBLookupComboBox, dxRibbonSkins, cxPCdxBarPopupMenu,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip;

type
//  niza = Array[1..5] of Variant;

  TfrmIzvestajDokumenti = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    Panel1: TPanel;
    OD_DATA: TcxDateEdit;
    DO_DATA: TcxDateEdit;
    Label4: TLabel;
    Label1: TLabel;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxComponentPrinter1Link2: TdxGridReportLink;
    Panel3: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBChartView1: TcxGridDBChartView;
    cxGrid1DBChartView1Series2: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series4: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series3: TcxGridDBChartSeries;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBChartView1: TcxGridDBChartView;
    cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series2: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series3: TcxGridDBChartSeries;
    cxGrid2Level1: TcxGridLevel;
    cxTabSheet3: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1MB: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIVLICE: TcxGridDBColumn;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid3DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    aPregledNaDokumenti: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxComponentPrinter1Link3: TdxGridReportLink;
    cxGrid1DBChartView1Series5: TcxGridDBChartSeries;
    dxBarManager1Bar1: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    cxGrid3DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DO_DATAPropertiesEditValueChanged(Sender: TObject);
    procedure OD_DATAPropertiesEditValueChanged(Sender: TObject);
    procedure cxGrid3DBTableView1DblClick(Sender: TObject);
    procedure aPregledNaDokumentiExecute(Sender: TObject);
    procedure cxBarEditItem2Change(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmIzvestajDokumenti: TfrmIzvestajDokumenti;
  AItemList: TcxFilterCriteriaItemList;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmReportUnit,
  dmUnitOtsustvo;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIzvestajDokumenti.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmIzvestajDokumenti.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmIzvestajDokumenti.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmIzvestajDokumenti.aBrisiExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmIzvestajDokumenti.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIzvestajDokumenti.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIzvestajDokumenti.aSnimiIzgledExecute(Sender: TObject);
begin
//  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIzvestajDokumenti.aZacuvajExcelExecute(Sender: TObject);
begin
//  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIzvestajDokumenti.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIzvestajDokumenti.cxBarEditItem2Change(Sender: TObject);
begin
     dm.tblIzvestajDokumentSektori.Close;
     dm.tblIzvestajDokumentSektori.parambyname('re_in').value:= cxBarEditItem2.EditValue ;//intToStr(dmkon.re) + ','+'%';
     dm.tblIzvestajDokumentSektori.Open;
end;

procedure TfrmIzvestajDokumenti.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIzvestajDokumenti.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmIzvestajDokumenti.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmIzvestajDokumenti.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmIzvestajDokumenti.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmIzvestajDokumenti.OD_DATAPropertiesEditValueChanged(
  Sender: TObject);
var  AItemList: TcxFilterCriteriaItemList;
begin
     if (OD_DATA.Text <> '')and (DO_DATA.Text <> '') then
        begin
          if DO_DATA.Date < OD_DATA.Date then
            begin
              ShowMessage('������ � �������� ������ !!!');
              OD_DATA.SetFocus;
            end
          else
            begin
              dm.tblIzvestajDokumenti.close;
              dm.tblIzvestajDokumenti.parambyname('firma').value:=dmKon.re;
              dm.tblIzvestajDokumenti.parambyname('datumOd').value:=OD_DATA.EditValue;
              dm.tblIzvestajDokumenti.parambyname('datumDo').value:=DO_DATA.EditValue;
              dm.tblIzvestajDokumenti.Open;

              dm.tblIzvestajDokumentSektori.close;
              dm.tblIzvestajDokumentSektori.parambyname('firma').value:=dmKon.re;
              dm.tblIzvestajDokumentSektori.parambyname('datumOd').value:=OD_DATA.EditValue;
              dm.tblIzvestajDokumentSektori.parambyname('datumDo').value:=DO_DATA.EditValue;
              dm.tblIzvestajDokumentSektori.parambyname('re_in').value:=cxBarEditItem2.EditValue;
              dm.tblIzvestajDokumentSektori.Open;

              cxGrid3DBTableView1.DataController.Filter.BeginUpdate;
                try
                  cxGrid3DBTableView1.DataController.Filter.Root.Clear;
                  AItemList := cxGrid3DBTableView1.DataController.Filter.Root.AddItemList(fboOr);
                  AItemList.AddItem(cxGrid3DBTableView1DATUM_DO, foLess, DO_DATA.Date, DateToStr(DO_DATA.Date));
                  AItemList.AddItem(cxGrid3DBTableView1DATUM_DO, foEqual, Null, 'null');
                  cxGrid3DBTableView1.DataController.Filter.Root.AddItem(cxGrid3DBTableView1DATUM_OD, foGreaterEqual, OD_DATA.Date, DateToStr(OD_DATA.Date));
                  cxGrid3DBTableView1.DataController.Filter.Active:=True;
                finally
                  cxGrid3DBTableView1.DataController.Filter.EndUpdate;
                end;
            end;
        end;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmIzvestajDokumenti.prefrli;
begin
end;

procedure TfrmIzvestajDokumenti.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmIzvestajDokumenti.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

end;

//------------------------------------------------------------------------------

procedure TfrmIzvestajDokumenti.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
      cxBarEditItem2.EditValue:=dmKon.re;
  //	������� �� ������������ �� ������ �� ������
//    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
//    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

     OD_DATA.Date:=StrToDate('01.01.'+IntToStr(dmKon.godina));
     DO_DATA.Date:=StrToDate('31.12.'+IntToStr(dmKon.godina));

     dmOtsustvo.tblPodsektori.close;
     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.Open;

     dm.tblIzvestajDokumenti.close;
     dm.tblIzvestajDokumenti.parambyname('firma').value:=dmKon.re;
     dm.tblIzvestajDokumenti.parambyname('datumOd').value:=OD_DATA.EditValue;
     dm.tblIzvestajDokumenti.parambyname('datumDo').value:=DO_DATA.EditValue;
     dm.tblIzvestajDokumenti.Open;

     dm.tblIzvestajDokumentSektori.close;
     dm.tblIzvestajDokumentSektori.parambyname('firma').value:=dmKon.re;
     dm.tblIzvestajDokumentSektori.parambyname('datumOd').value:=OD_DATA.EditValue;
     dm.tblIzvestajDokumentSektori.parambyname('datumDo').value:=DO_DATA.EditValue;
     dm.tblIzvestajDokumentSektori.parambyname('re_in').value:= cxBarEditItem2.EditValue ;//intToStr(dmkon.re) + ','+'%';
     dm.tblIzvestajDokumentSektori.Open;

     dm.tblDokumenti.Close;
     dm.tblDokumenti.ParamByName('mb').Value:='%';
     dm.tblDokumenti.ParamByName('firma').Value:=dmKon.re;
     dm.tblDokumenti.Open;

     cxGrid3DBTableView1.DataController.Filter.BeginUpdate;
        try
          cxGrid3DBTableView1.DataController.Filter.Root.Clear;
          AItemList := cxGrid3DBTableView1.DataController.Filter.Root.AddItemList(fboOr);
          AItemList.AddItem(cxGrid3DBTableView1DATUM_DO, foLess, DO_DATA.Date, DateToStr(DO_DATA.Date));
          AItemList.AddItem(cxGrid3DBTableView1DATUM_DO, foEqual, Null, 'null');
          cxGrid3DBTableView1.DataController.Filter.Root.AddItem(cxGrid3DBTableView1DATUM_OD, foGreaterEqual, OD_DATA.Date, DateToStr(OD_DATA.Date));
          cxGrid3DBTableView1.DataController.Filter.Active:=True;
        finally
          cxGrid3DBTableView1.DataController.Filter.EndUpdate;
        end;
end;
//------------------------------------------------------------------------------

procedure TfrmIzvestajDokumenti.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmIzvestajDokumenti.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzvestajDokumenti.cxGrid3DBTableView1DblClick(Sender: TObject);
var
     RptStream: TStream;
     SqlStream: TStream;
begin
     if dm.tblDokumentiVID_DOKUMENT.Value = reseniePreraspredelba then
        begin
          dmReport.ResenieZaPreraspredelba.close;
          dmReport.ResenieZaPreraspredelba.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.ResenieZaPreraspredelba.Open;
          if not dmReport.ResenieZaPreraspredelbaDATA.IsNull then
            begin
              RptStream := dmReport.ResenieZaPreraspredelba.CreateBlobStream(dmReport.ResenieZaPreraspredelbaDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = resPrekinRabOdnos then
        begin
          dmReport.PrestanokNaRabotenOdnos.close;
          dmReport.PrestanokNaRabotenOdnos.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.PrestanokNaRabotenOdnos.Open;
          if not dmReport.PrestanokNaRabotenOdnosDATA.IsNull then
            begin
              RptStream := dmReport.PrestanokNaRabotenOdnos.CreateBlobStream(dmReport.PrestanokNaRabotenOdnosDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = dogovorZaRabota then
        begin
          dmReport.DogovorZaVrabotuvanje.close;
          dmReport.DogovorZaVrabotuvanje.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dmReport.DogovorZaVrabotuvanje.Open;

          if not dmReport.DogovorZaVrabotuvanjeDATA.IsNull then
            begin
             RptStream := dmReport.DogovorZaVrabotuvanje.CreateBlobStream(dmReport.DogovorZaVrabotuvanjeDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
end;

procedure TfrmIzvestajDokumenti.DO_DATAPropertiesEditValueChanged(
  Sender: TObject);
begin
      if (OD_DATA.Text <> '')and (DO_DATA.Text <> '') then
        begin
          if DO_DATA.Date < OD_DATA.Date then
            begin
              ShowMessage('������ � �������� ������ !!!');
              DO_DATA.SetFocus;
            end
          else
            begin
              dm.tblIzvestajDokumenti.close;
              dm.tblIzvestajDokumenti.parambyname('firma').value:=dmKon.re;
              dm.tblIzvestajDokumenti.parambyname('datumOd').value:=OD_DATA.EditValue;
              dm.tblIzvestajDokumenti.parambyname('datumDo').value:=DO_DATA.EditValue;
              dm.tblIzvestajDokumenti.Open;

              dm.tblIzvestajDokumentSektori.close;
              dm.tblIzvestajDokumentSektori.parambyname('firma').value:=dmKon.re;
              dm.tblIzvestajDokumentSektori.parambyname('datumOd').value:=OD_DATA.EditValue;
              dm.tblIzvestajDokumentSektori.parambyname('datumDo').value:=DO_DATA.EditValue;
              dm.tblIzvestajDokumentSektori.parambyname('re_in').value:=cxBarEditItem2.EditValue;
              dm.tblIzvestajDokumentSektori.Open;

              cxGrid3DBTableView1.DataController.Filter.BeginUpdate;
                try
                  cxGrid3DBTableView1.DataController.Filter.Root.Clear;
                  AItemList := cxGrid3DBTableView1.DataController.Filter.Root.AddItemList(fboOr);
                  AItemList.AddItem(cxGrid3DBTableView1DATUM_DO, foLess, DO_DATA.Date, DateToStr(DO_DATA.Date));
                  AItemList.AddItem(cxGrid3DBTableView1DATUM_DO, foEqual, Null, 'null');
                  cxGrid3DBTableView1.DataController.Filter.Root.AddItem(cxGrid3DBTableView1DATUM_OD, foGreaterEqual, OD_DATA.Date, DateToStr(OD_DATA.Date));
                  cxGrid3DBTableView1.DataController.Filter.Active:=True;
                finally
                  cxGrid3DBTableView1.DataController.Filter.EndUpdate;
                end;
            end;
        end;
end;

//  ����� �� �����
procedure TfrmIzvestajDokumenti.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmIzvestajDokumenti.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIzvestajDokumenti.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIzvestajDokumenti.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        dxComponentPrinter1.Preview(true, dxComponentPrinter1Link3);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIzvestajDokumenti.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmIzvestajDokumenti.aPregledNaDokumentiExecute(Sender: TObject);
var
     RptStream: TStream;
     SqlStream: TStream;
begin
     if dm.tblDokumentiVID_DOKUMENT.Value = reseniePreraspredelba then
        begin
          dmReport.ResenieZaPreraspredelba.close;
          dmReport.ResenieZaPreraspredelba.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.ResenieZaPreraspredelba.Open;
          if not dmReport.ResenieZaPreraspredelbaDATA.IsNull then
            begin
              RptStream := dmReport.ResenieZaPreraspredelba.CreateBlobStream(dmReport.ResenieZaPreraspredelbaDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = resPrekinRabOdnos then
        begin
          dmReport.PrestanokNaRabotenOdnos.close;
          dmReport.PrestanokNaRabotenOdnos.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.PrestanokNaRabotenOdnos.Open;
          if not dmReport.PrestanokNaRabotenOdnosDATA.IsNull then
            begin
              RptStream := dmReport.PrestanokNaRabotenOdnos.CreateBlobStream(dmReport.PrestanokNaRabotenOdnosDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = dogovorZaRabota then
        begin
          dmReport.DogovorZaVrabotuvanje.close;
          dmReport.DogovorZaVrabotuvanje.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dmReport.DogovorZaVrabotuvanje.Open;

          if not dmReport.DogovorZaVrabotuvanjeDATA.IsNull then
            begin
             RptStream := dmReport.DogovorZaVrabotuvanje.CreateBlobStream(dmReport.DogovorZaVrabotuvanjeDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
end;

procedure TfrmIzvestajDokumenti.aSnimiPecatenjeExecute(Sender: TObject);
begin
//      zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIzvestajDokumenti.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmIzvestajDokumenti.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmIzvestajDokumenti.aHelpExecute(Sender: TObject);
begin
     Close;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmIzvestajDokumenti.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmIzvestajDokumenti.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
