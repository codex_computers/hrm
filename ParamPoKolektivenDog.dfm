inherited frmParamKolektivenDog: TfrmParamKolektivenDog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1080' '#1087#1086' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088
  ClientHeight = 782
  ClientWidth = 861
  ExplicitWidth = 877
  ExplicitHeight = 821
  PixelsPerInch = 96
  TextHeight = 13
  object Label21: TLabel [0]
    Left = 400
    Top = 55
    Width = 112
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = #1052#1080#1085#1080#1084#1072#1083#1077#1085' '#1073#1088#1086#1112' :'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited lPanel: TPanel
    Width = 861
    Height = 154
    ExplicitWidth = 861
    ExplicitHeight = 154
    inherited cxGrid1: TcxGrid
      Width = 857
      Height = 142
      ExplicitWidth = 857
      ExplicitHeight = 142
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsParamKolektivenDog
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 66
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 75
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 76
        end
        object cxGrid1DBTableView1GO_STAZ_1: TcxGridDBColumn
          DataBinding.FieldName = 'GO_STAZ_1'
          Width = 100
        end
        object cxGrid1DBTableView1GO_STAZ_2: TcxGridDBColumn
          DataBinding.FieldName = 'GO_STAZ_2'
          Width = 100
        end
        object cxGrid1DBTableView1GO_STAZ_3: TcxGridDBColumn
          DataBinding.FieldName = 'GO_STAZ_3'
          Width = 100
        end
        object cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn
          DataBinding.FieldName = 'DENOVI_ODMOR'
          Width = 102
        end
        object cxGrid1DBTableView1MAX_DENOVI_GO1: TcxGridDBColumn
          DataBinding.FieldName = 'MAX_DENOVI_GO1'
          Width = 100
        end
        object cxGrid1DBTableView1MAX_DENOVI_GO2: TcxGridDBColumn
          DataBinding.FieldName = 'MAX_DENOVI_GO2'
          Width = 100
        end
        object cxGrid1DBTableView1DEN_ZENI: TcxGridDBColumn
          DataBinding.FieldName = 'DEN_ZENI'
          Width = 153
        end
        object cxGrid1DBTableView1DEN_MAZI: TcxGridDBColumn
          DataBinding.FieldName = 'DEN_MAZI'
          Width = 116
        end
        object cxGrid1DBTableView1DEN_TEL_OST: TcxGridDBColumn
          DataBinding.FieldName = 'DEN_TEL_OST'
          Width = 115
        end
        object cxGrid1DBTableView1INVALID_DENOVI: TcxGridDBColumn
          DataBinding.FieldName = 'INVALID_DENOVI'
        end
        object cxGrid1DBTableView1CUVANJE_DETE: TcxGridDBColumn
          DataBinding.FieldName = 'CUVANJE_DETE'
        end
        object cxGrid1DBTableView1MAX_NEPLATENI_DENOVI: TcxGridDBColumn
          Caption = #1052#1072#1093' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1087#1083#1072#1090#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
          DataBinding.FieldName = 'MAX_NEPLATENI_DENOVI'
          Width = 100
        end
        object cxGrid1DBTableView1MAX_TIPOTSUSTVO_DEN: TcxGridDBColumn
          Caption = #1052#1072#1093' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1085#1077#1087#1083#1072#1090#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
          DataBinding.FieldName = 'MAX_TIPOTSUSTVO_DEN'
          Width = 100
        end
        object cxGrid1DBTableView1DODATOK: TcxGridDBColumn
          DataBinding.FieldName = 'DODATOK'
          Width = 111
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 66
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 66
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 177
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 178
        end
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 2
      Top = 144
      Width = 857
      Height = 8
      AlignSplitter = salBottom
      Control = dPanel
    end
  end
  inherited dPanel: TPanel
    Top = 280
    Width = 861
    Height = 479
    ExplicitTop = 280
    ExplicitWidth = 861
    ExplicitHeight = 479
    inherited Label1: TLabel
      Left = 238
      Top = 7
      Visible = False
      ExplicitLeft = 238
      ExplicitTop = 7
    end
    inherited Sifra: TcxDBTextEdit
      Left = 294
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsParamKolektivenDog
      TabOrder = 2
      Visible = False
      ExplicitLeft = 294
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 746
      Top = 439
      TabOrder = 6
      ExplicitLeft = 746
      ExplicitTop = 439
    end
    inherited ZapisiButton: TcxButton
      Left = 665
      Top = 439
      TabOrder = 5
      ExplicitLeft = 665
      ExplicitTop = 439
    end
    object Period: TcxGroupBox
      Left = 16
      Top = 6
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088
      TabOrder = 0
      Height = 59
      Width = 392
      object Label2: TLabel
        Left = -1
        Top = 28
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 181
        Top = 28
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object datumOd: TcxDBDateEdit
        Tag = 1
        Left = 55
        Top = 25
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsParamKolektivenDog
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DatumDo: TcxDBDateEdit
        Left = 237
        Top = 25
        Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1075#1072' '#1074#1072#1078#1080' '#1082#1086#1083#1077#1082#1090#1080#1074#1085#1080#1086#1090' '#1076#1086#1075#1086#1074#1086#1088
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsParamKolektivenDog
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object Otsustva: TcxGroupBox
      Left = 16
      Top = 351
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1080' '#1079#1072' '#1086#1090#1089#1091#1089#1090#1074#1072
      TabOrder = 3
      Height = 90
      Width = 392
      object Label17: TLabel
        Left = 12
        Top = 26
        Width = 266
        Height = 23
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1093' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1087#1083#1072#1090#1077#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label18: TLabel
        Left = 12
        Top = 55
        Width = 266
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1093' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1085#1077#1087#1083#1072#1090#1077#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object maxPlateno: TcxDBTextEdit
        Left = 284
        Top = 25
        BeepOnEnter = False
        DataBinding.DataField = 'MAX_TIPOTSUSTVO_DEN'
        DataBinding.DataSource = dm.dsParamKolektivenDog
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object maxneplateno: TcxDBTextEdit
        Left = 284
        Top = 52
        BeepOnEnter = False
        DataBinding.DataField = 'MAX_NEPLATENI_DENOVI'
        DataBinding.DataSource = dm.dsParamKolektivenDog
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
    end
    object Plata: TcxGroupBox
      Left = 414
      Top = 351
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1080' '#1079#1072' '#1087#1083#1072#1090#1072
      TabOrder = 4
      Height = 75
      Width = 407
      object Label8: TLabel
        Left = 26
        Top = 22
        Width = 193
        Height = 31
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086#1076#1072#1090#1086#1082' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1072#1090#1072' '#1087#1083#1072#1090#1072' '#1079#1072' '#1089#1077#1082#1086#1112#1072' '#1075#1086#1076#1080#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label19: TLabel
        Left = 311
        Top = 28
        Width = 18
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '%'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Staz: TcxDBTextEdit
        Left = 225
        Top = 25
        Hint = #1047#1072' '#1089#1077#1082#1086#1112#1072' '#1075#1086#1076#1080#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1080#1084#1072' '#1076#1086#1076#1072#1090#1086#1082' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1072#1090#1072' '#1087#1083#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'DODATOK'
        DataBinding.DataSource = dm.dsParamKolektivenDog
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
    end
    object ParametriZaResenieGO: TcxGroupBox
      Left = 16
      Top = 71
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1080' '#1079#1072' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      TabOrder = 1
      Height = 274
      Width = 805
      object cxGroupBox3: TcxGroupBox
        Left = 15
        Top = 25
        Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078
        TabOrder = 0
        Height = 112
        Width = 377
        object Label9: TLabel
          Left = -152
          Top = 28
          Width = 289
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '1 - 10 '#1075#1086#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = -152
          Top = 55
          Width = 289
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '11 - 20 '#1075#1086#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = -152
          Top = 82
          Width = 289
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1085#1072#1076' 20 '#1075#1086#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 229
          Top = 28
          Width = 23
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1076#1077#1085
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 229
          Top = 55
          Width = 23
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1076#1077#1085
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label14: TLabel
          Left = 229
          Top = 82
          Width = 23
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1076#1077#1085
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object prv: TcxDBTextEdit
          Left = 143
          Top = 25
          Hint = 
            #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1043#1054' '#1079#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1087#1086#1084#1077#1107#1091' 1 '#1080' 10 '#1075#1086 +
            #1076#1080#1085#1080
          BeepOnEnter = False
          DataBinding.DataField = 'GO_STAZ_1'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object vtor: TcxDBTextEdit
          Left = 143
          Top = 52
          Hint = 
            #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1043#1054' '#1079#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1087#1086#1084#1077#1107#1091' 11 '#1080' 20 '#1075 +
            #1086#1076#1080#1085#1080
          BeepOnEnter = False
          DataBinding.DataField = 'GO_STAZ_2'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object tret: TcxDBTextEdit
          Left = 143
          Top = 79
          Hint = #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1043#1054' '#1079#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1085#1072#1076' 20 '#1075#1086#1076#1080#1085#1080
          BeepOnEnter = False
          DataBinding.DataField = 'GO_STAZ_3'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 398
        Top = 25
        Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
        TabOrder = 2
        Height = 176
        Width = 383
        object Label5: TLabel
          Left = 65
          Top = 28
          Width = 209
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1047#1072' '#1078#1077#1085#1080' '#1087#1086#1074#1086#1079#1088#1072#1089#1085#1080' '#1086#1076' 57 '#1075#1086#1076#1080#1085#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 65
          Top = 55
          Width = 209
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1047#1072' '#1084#1072#1078#1080' '#1087#1086#1074#1086#1079#1088#1072#1089#1085#1080' '#1086#1076' 59 '#1075#1086#1076#1080#1085#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = -39
          Top = 75
          Width = 313
          Height = 30
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1040#1082#1086' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1077' '#1089#1086' '#1085#1072#1112#1084#1072#1083#1082#1091' 60% '#1090#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label15: TLabel
          Left = -39
          Top = 109
          Width = 313
          Height = 19
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1040#1082#1086' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1077' '#1080#1085#1074#1072#1083#1080#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label16: TLabel
          Left = 8
          Top = 130
          Width = 266
          Height = 33
          Alignment = taRightJustify
          AutoSize = False
          Caption = 
            #1040#1082#1086' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1085#1077#1075#1091#1074#1072' '#1080' '#1095#1091#1074#1072' '#1076#1077#1090#1077' '#1089#1086' '#1090#1077#1083#1077#1089#1077#1085' '#1080#1083#1080' '#1076#1091#1096#1077#1074#1077#1085' '#1085#1077#1076#1086#1089#1090#1072 +
            #1090#1086#1082' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Zeni: TcxDBTextEdit
          Left = 280
          Top = 25
          Hint = 
            #1047#1072' '#1078#1077#1085#1080' '#1087#1086#1074#1086#1079#1088#1072#1089#1085#1080' '#1086#1076' 57 '#1075#1086#1076#1080#1085#1080', '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1079#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080 +
            ' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072
          BeepOnEnter = False
          DataBinding.DataField = 'DEN_ZENI'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object Mazi: TcxDBTextEdit
          Left = 280
          Top = 52
          Hint = 
            #1047#1072' '#1084#1072#1078#1080' '#1087#1086#1074#1086#1079#1088#1072#1089#1085#1080' '#1086#1076' 59 '#1075#1086#1076#1080#1085#1080', '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088' '#1079#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080 +
            ' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072
          BeepOnEnter = False
          DataBinding.DataField = 'DEN_MAZI'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object Telesno: TcxDBTextEdit
          Left = 280
          Top = 79
          Hint = 
            #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088', '#1072#1082#1086' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1077' '#1089 +
            #1086' '#1085#1072#1112#1084#1072#1083#1082#1091' 60% '#1090#1077#1083#1077#1089#1085#1086' '#1086#1096#1090#1077#1090#1091#1074#1072#1114#1077'.'
          BeepOnEnter = False
          DataBinding.DataField = 'DEN_TEL_OST'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object Invalid: TcxDBTextEdit
          Left = 280
          Top = 106
          Hint = 
            #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088', '#1072#1082#1086' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1077' '#1080 +
            #1085#1074#1072#1083#1080#1076'.'
          BeepOnEnter = False
          DataBinding.DataField = 'INVALID_DENOVI'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object CUVANJE_DETE: TcxDBTextEdit
          Left = 280
          Top = 133
          Hint = 
            #1047#1072' '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1079#1075#1086#1083#1077#1084#1091#1074#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088',  a'#1082#1086' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1085#1077 +
            #1075#1091#1074#1072' '#1080' '#1095#1091#1074#1072' '#1076#1077#1090#1077' '#1089#1086' '#1090#1077#1083#1077#1089#1077#1085' '#1080#1083#1080' '#1076#1091#1096#1077#1074#1077#1085' '#1085#1077#1076#1086#1089#1090#1072#1090#1086#1082
          BeepOnEnter = False
          DataBinding.DataField = 'CUVANJE_DETE'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
      end
      object cxGroupBox7: TcxGroupBox
        Left = 15
        Top = 143
        Caption = #1054#1075#1088#1072#1085#1080#1095#1091#1074#1072#1114#1072' '#1085#1072' '#1074#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
        TabOrder = 1
        Height = 113
        Width = 377
        object Label4: TLabel
          Left = 26
          Top = 28
          Width = 112
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1052#1080#1085#1080#1084#1072#1083#1077#1085' '#1073#1088#1086#1112' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 5
          Top = 55
          Width = 134
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1077#1085' '#1073#1088#1086#1112' 1 :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label22: TLabel
          Left = 6
          Top = 82
          Width = 131
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1077#1085' '#1073#1088#1086#1112' 2 :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object MinBR: TcxDBTextEdit
          Left = 144
          Top = 25
          Hint = #1047#1072#1082#1086#1085#1089#1082#1080' '#1084#1080#1085#1080#1084#1091#1084' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1074#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'DENOVI_ODMOR'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object maxBr2: TcxDBTextEdit
          Left = 143
          Top = 79
          Hint = 
            #1052#1072#1082#1089#1080#1084#1072#1083#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088' '#1079#1072' '#1083#1080#1094#1072' '#1082#1086#1080' '#1082#1086#1088#1080#1089#1090#1072#1090' ' +
            #1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'MAX_DENOVI_GO2'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object maxBr1: TcxDBTextEdit
          Left = 145
          Top = 52
          Hint = #1052#1072#1082#1089#1080#1084#1072#1083#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'MAX_DENOVI_GO1'
          DataBinding.DataSource = dm.dsParamKolektivenDog
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 80
        end
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 861
    ExplicitWidth = 861
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 759
    Width = 861
    ExplicitTop = 759
    ExplicitWidth = 861
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 304
    Top = 96
  end
  inherited PopupMenu1: TPopupMenu
    Left = 376
    Top = 112
  end
  inherited dxBarManager1: TdxBarManager
    Top = 120
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 176
    Top = 128
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 96
    Top = 120
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page # of Pages #]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40660.481735428240000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
