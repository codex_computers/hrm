object frmUcesniciObuka: TfrmUcesniciObuka
  Left = 0
  Top = 0
  Caption = #1059#1095#1077#1089#1085#1080#1094#1080' '#1074#1086' '#1086#1073#1091#1082#1072#1090#1072
  ClientHeight = 701
  ClientWidth = 886
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 171
    Width = 443
    Height = 507
    Align = alLeft
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGrid2: TcxGrid
      Left = 2
      Top = 2
      Width = 439
      Height = 503
      Align = alClient
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        DataController.DataSource = dmOtsustvo.dsObukaUcesnici
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.CellHints = True
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGridDBTableView1OBUKA_ID: TcxGridDBColumn
          Caption = #1054#1073#1091#1082#1072' ('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'OBUKA_ID'
          Visible = False
        end
        object cxGridDBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGridDBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 95
        end
        object cxGridDBTableView1VRABOTENNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 176
        end
        object cxGridDBTableView1OCENKA: TcxGridDBColumn
          DataBinding.FieldName = 'OCENKA'
          Width = 207
        end
        object cxGridDBTableView1SERTIFIKATI: TcxGridDBColumn
          DataBinding.FieldName = 'SERTIFIKATI'
          Width = 181
        end
        object cxGridDBTableView1TS_INS: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGridDBTableView1TS_UPD: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGridDBTableView1USR_INS: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGridDBTableView1USR_UPD: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 443
    Top = 171
    Width = 443
    Height = 507
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 439
      Height = 503
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dm.dsViewVraboteni
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.CellHints = True
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
        end
        object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 146
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          Caption = #1060#1080#1088#1084#1072' ('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RM_RE_ID_RE_FIRMA: TcxGridDBColumn
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'RM_RE_ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' ('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 139
        end
        object cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNAEDINICANAZIV'
          Width = 162
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 126
    Width = 886
    Height = 45
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 2
    object cxLabel2: TcxLabel
      Left = 20
      Top = 17
      Caption = #1059#1095#1077#1089#1085#1080#1094#1080' '#1074#1086' '#1086#1073#1091#1082#1072' : '
    end
    object cxLabel4: TcxLabel
      Left = 446
      Top = 17
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080' : '
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 886
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 3
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 678
    Width = 886
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Enter - '#1044#1086#1076#1072#1076#1080' '#1074#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1091#1095#1077#1089#1085#1080#1094#1080' '#1079#1072' '#1086#1073#1091#1082#1072
        Width = 244
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F8 - '#1041#1088#1080#1096#1080', F7 -  '#1054#1089#1074#1077#1078#1080' '
        Width = 210
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080'/'#1048#1079#1083#1077#1079
        Width = 50
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel8: TPanel
    Left = 19
    Top = 261
    Width = 371
    Height = 256
    ParentBackground = False
    TabOrder = 9
    Visible = False
    object GroupBox1: TGroupBox
      Left = 8
      Top = 8
      Width = 355
      Height = 240
      TabOrder = 0
      object Ocenka: TcxDBMemo
        Left = 20
        Top = 41
        DataBinding.DataField = 'OCENKA'
        DataBinding.DataSource = dmOtsustvo.dsObukaUcesnici
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 72
        Width = 317
      end
      object cxLabel3: TcxLabel
        Left = 20
        Top = 17
        Caption = #1054#1094#1077#1085#1082#1072' : '
      end
      object cxLabel1: TcxLabel
        Left = 20
        Top = 122
        Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1080' : '
      end
      object Sertifikati: TcxDBMemo
        Left = 20
        Top = 145
        DataBinding.DataField = 'SERTIFIKATI'
        DataBinding.DataSource = dmOtsustvo.dsObukaUcesnici
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 72
        Width = 317
      end
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 528
    Top = 72
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 336
    Top = 112
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = dm.cxLargeImages
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 648
    Top = 136
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 384
      DockedTop = 0
      FloatLeft = 911
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton41'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton40'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1059#1095#1077#1089#1085#1080#1094#1080' '#1074#1086' '#1086#1073#1091#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 912
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton43'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton42'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton51'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 912
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton46'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton44'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton48'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton47'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 192
      DockedTop = 0
      FloatLeft = 912
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton49'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton50'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem7'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 35
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 42
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aDodadiPlanZaOtsustvo
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPecatenje
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarButton3: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton4: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aStatusGrupa
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aStatus
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.ListColumns = <>
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end>
    end
    object dxBarButton5: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aDodadiPlanZaOtsustvo
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
    object cxBarEditRabEdinica: TcxBarEditItem
      Caption = #1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072
      Category = 0
      Hint = #1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'ID'
        end
        item
          Width = 650
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsPodsektori
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end>
    end
    object dxBarButton7: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton32: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditVraboten: TcxBarEditItem
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085
      Category = 0
      Hint = #1042#1088#1072#1073#1086#1090#1077#1085
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 350
          FieldName = 'MB'
        end
        item
          FieldName = 'VRABOTENNAZIV'
        end>
    end
    object dxBarLargeButton33: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton34: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarLargeButton35: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton36: TdxBarLargeButton
      Action = aUcesnici
      Category = 0
    end
    object dxBarLargeButton37: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton38: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton39: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton40: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton41: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton42: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton43: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton44: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton45: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton46: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarSubItem6: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton47: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton48: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton49: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton50: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarSubItem7: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton8'
        end>
    end
    object dxBarButton8: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton51: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 544
    Top = 136
    object aEvidentirajObuka: TAction
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1087#1083#1072#1085' '#1079#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
    end
    object aDodadiPlanZaOtsustvo: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 10
      ShortCut = 116
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aAzurirajPlan: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajPlanExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aOsvezi: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      OnExecute = aOsveziExecute
    end
    object aSnimiGoIzgledot: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiGoIzgledotExecute
    end
    object aZacuvajVoExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajVoExcelExecute
    end
    object aPecatenje: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      ImageIndex = 30
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aStatusGrupa: TAction
      Caption = 'aStatusGrupa'
    end
    object aStatus: TAction
      Caption = 'aStatus'
    end
    object aPrebaraj: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
    end
    object aIscisti: TAction
      Caption = #1048#1089#1095#1080#1089#1090#1080
      ImageIndex = 22
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPomos: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      ShortCut = 112
    end
    object aUcesnici: TAction
      Caption = #1044#1086#1076#1072#1076#1080'/'#1040#1078#1091#1088#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 27
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 704
    Top = 32
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 256
      PrinterPage.Footer = 5080
      PrinterPage.Header = 2540
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210820
      PrinterPage.PageSize.Y = 297180
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40906.494772627320000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
