inherited frmPraznici: TfrmPraznici
  Caption = #1055#1088#1072#1079#1085#1080#1094#1080
  ClientHeight = 724
  ClientWidth = 755
  ExplicitWidth = 771
  ExplicitHeight = 763
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 755
    Height = 398
    ExplicitWidth = 755
    ExplicitHeight = 398
    inherited cxGrid1: TcxGrid
      Width = 751
      Height = 394
      ExplicitWidth = 751
      ExplicitHeight = 394
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmOtsustvo.dsPraznici
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 74
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 71
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 270
        end
        object cxGrid1DBTableView1VEROISPOVED: TcxGridDBColumn
          DataBinding.FieldName = 'VEROISPOVED'
          Visible = False
        end
        object cxGrid1DBTableView1VEROISPOVEDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VEROISPOVEDNAZIV'
          Width = 173
        end
        object cxGrid1DBTableView1NACIONALNOST: TcxGridDBColumn
          DataBinding.FieldName = 'NACIONALNOST'
          Visible = False
        end
        object cxGrid1DBTableView1NACIONALNOSTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NACIONALNOSTNAZIV'
          Width = 214
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 524
    Width = 755
    Height = 177
    ExplicitTop = 524
    ExplicitWidth = 755
    ExplicitHeight = 177
    inherited Label1: TLabel
      Left = 61
      Top = 10
      Visible = False
      ExplicitLeft = 61
      ExplicitTop = 10
    end
    object Label2: TLabel [1]
      Left = 61
      Top = 60
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 61
      Top = 33
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 24
      Top = 114
      Width = 87
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 13
      Top = 87
      Width = 98
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 117
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      TabOrder = 1
      Visible = False
      ExplicitLeft = 117
      ExplicitTop = 6
      ExplicitWidth = 108
      Width = 108
    end
    inherited OtkaziButton: TcxButton
      Left = 664
      Top = 137
      TabOrder = 8
      ExplicitLeft = 664
      ExplicitTop = 137
    end
    inherited ZapisiButton: TcxButton
      Left = 583
      Top = 137
      TabOrder = 7
      ExplicitLeft = 583
      ExplicitTop = 137
    end
    object DATUM: TcxDBDateEdit
      Tag = 1
      Left = 117
      Top = 57
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 108
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 117
      Top = 30
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 441
    end
    object VEROISPOVED: TcxDBLookupComboBox
      Left = 169
      Top = 111
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'VEROISPOVED'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsVeroispoved
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 389
    end
    object NACIONALNOST: TcxDBLookupComboBox
      Left = 169
      Top = 84
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NACIONALNOST'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsNacionalnost
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 389
    end
    object NACIONALNOSTID: TcxDBTextEdit
      Left = 117
      Top = 84
      BeepOnEnter = False
      DataBinding.DataField = 'NACIONALNOST'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object VEROISPOVEDID: TcxDBTextEdit
      Left = 117
      Top = 111
      BeepOnEnter = False
      DataBinding.DataField = 'VEROISPOVED'
      DataBinding.DataSource = dmOtsustvo.dsPraznici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 755
    ExplicitWidth = 755
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 701
    Width = 755
    ExplicitTop = 701
    ExplicitWidth = 755
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1072#1079#1085#1080#1082
      DockedLeft = 122
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 456
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 625
      FloatClientWidth = 59
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      CaptionButtons = <>
      DockedLeft = 305
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 65
          Visible = True
          ItemName = 'cbGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aKopirajPraznici
      Category = 0
      LargeImageIndex = 8
      SyncImageIndex = False
      ImageIndex = 8
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.Items.Strings = (
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022')
      InternalEditValue = nil
    end
  end
  inherited ActionList1: TActionList
    object aKopirajPraznici: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1075#1080' '#1087#1088#1072#1079#1085#1080#1094#1080#1090#1077' '#1086#1076' '#1087#1088#1077#1076#1093#1086#1076#1085#1072#1090#1072' '#1075#1086#1076#1080#1085#1072
      OnExecute = aKopirajPrazniciExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40248.537579143520000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 616
    Top = 136
    PixelsPerInch = 96
  end
end
