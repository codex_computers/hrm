unit ObrazovanieObuka;


(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                   }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxMemo, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TfrmObrazovanieObuka = class(TfrmMaster)
    MB_VRABOTEN: TcxDBTextEdit;
    Label11: TLabel;
    Label10: TLabel;
    ID_MOLBA: TcxDBTextEdit;
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1KVALIFIKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ZNAENJE: TcxGridDBColumn;
    cxGrid1DBTableView1NIVO: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    NIVO: TcxDBMemo;
    Label18: TLabel;
    ZNAENJE: TcxDBMemo;
    Label16: TLabel;
    KVALIFIKACIJA: TcxDBMemo;
    Label14: TLabel;
    Label3: TLabel;
    IMEPREZIME: TcxDBLookupComboBox;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    GroupBox2: TGroupBox;
    DATUM_DO: TcxDBDateEdit;
    Label13: TLabel;
    DATUM_OD: TcxDBDateEdit;
    Label12: TLabel;
    aUrediTextOpis: TAction;
    aUrediTextUslovi: TAction;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD: TcxGridDBColumn;
    Label4: TLabel;
    Obrazovna: TcxDBTextEdit;
    Institucija: TcxDBLookupComboBox;
    Label2: TLabel;
    Nasoka: TcxDBTextEdit;
    NasokaNaziv: TcxDBLookupComboBox;
    cxGrid1DBTableView1IME_ORGANIZACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1NASOKA: TcxGridDBColumn;
    cxGrid1DBTableView1OINAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NASOKANAZIV: TcxGridDBColumn;
    Label5: TLabel;
    txtSZObrazovanie: TcxDBTextEdit;
    cbSZObrazovanie: TcxDBLookupComboBox;
    cxGrid1DBTableView1STEPEN_OBRAZOVANIE: TcxGridDBColumn;
    cxGrid1DBTableView1SZONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn;
    procedure KVALIFIKACIJADblClick(Sender: TObject);
    procedure ZNAENJEDblClick(Sender: TObject);
    procedure NIVODblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure DATUM_ODExit(Sender: TObject);
    procedure DATUM_DOExit(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmObrazovanieObuka: TfrmObrazovanieObuka;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  Notepad, Utils, Molbi;

{$R *.dfm}

procedure TfrmObrazovanieObuka.aAzurirajExecute(Sender: TObject);
begin
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2) then
       MB_VRABOTEN.SetFocus;
    if (tag = 4) then
       ID_MOLBA.SetFocus;
    if (tag = 5) or (tag = 3)then
       DATUM_OD.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmObrazovanieObuka.aHelpExecute(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmObrazovanieObuka.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2) then
       MB_VRABOTEN.SetFocus;
    if (tag = 4)then
       ID_MOLBA.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if tag = 5 then
       begin
         dmSis.tblObrazovanieObukaID_MOLBA.Value:=dmSis.tblMolbaID.Value;
         DATUM_OD.SetFocus;
       end;
     if tag = 3 then
        begin
          dmSis.tblObrazovanieObukaMB_VRABOTEN.Value:=dm.tblLicaVraboteniMB.Value;
          DATUM_OD.SetFocus;
        end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmObrazovanieObuka.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if (DATUM_OD.Text <> '')and (DATUM_DO.Text <> '') and (DATUM_DO.Date < DATUM_OD.Date ) then
          begin
               ShowMessage('������ � �������� ������ !!!');
               DATUM_DO.SetFocus;
            end
          else
            begin
              if ((st = dsInsert) and inserting) then
                begin
                  cxGrid1DBTableView1.DataController.DataSet.Post;
                  aNov.Execute;
                end;

              if ((st = dsInsert) and (not inserting)) then
                begin
                  cxGrid1DBTableView1.DataController.DataSet.Post;
                  dPanel.Enabled:=false;
                  lPanel.Enabled:=true;
                  cxGrid1.SetFocus;
                end;
              if (st = dsEdit) then
                begin
                  cxGrid1DBTableView1.DataController.DataSet.Post;
                  dPanel.Enabled:=false;
                  lPanel.Enabled:=true;
                  cxGrid1.SetFocus;
                end;
            end;
        end;
    end
end;


procedure TfrmObrazovanieObuka.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if (Sender = Institucija) or (Sender = Obrazovna) then
      begin
        if (Institucija.Text <> '')and(Obrazovna.Text <> '') then
         begin
          dm.tblNasokaObrazovanie.ParamByName('idObrazovna').Value:=Institucija.EditValue;
          dm.tblNasokaObrazovanie.FullRefresh;
         end;
      end;
  if (Sender as TWinControl)= cbSZObrazovanie then
     begin
       dmSis.tblObrazovanieObukaSTEPEN_OBRAZOVANIE.Value:=dmSis.tblSZObrazovanieID.Value;
     end;
end;

procedure TfrmObrazovanieObuka.DATUM_DOExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmObrazovanieObuka.DATUM_ODExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmObrazovanieObuka.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if tag = 3 then
     begin
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 0) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 1) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:=dmSis.tblOglasID.Value;
       dmSis.tblMolba.FullRefresh;
     end;
end;

procedure TfrmObrazovanieObuka.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblObrazovnaInstitucija.Close;
  dm.tblObrazovnaInstitucija.Open;
  dm.tblNasokaObrazovanie.close;
  dm.tblNasokaObrazovanie.ParamByName('idObrazovna').Value:='%';
  dm.tblNasokaObrazovanie.Open;
end;

procedure TfrmObrazovanieObuka.FormShow(Sender: TObject);
begin
  //inherited;
  if tag = 2 then
     begin
        dmSis.tblObrazovanieObuka.close;
        dmSis.tblObrazovanieObuka.ParamByName('MB').Value:='%';
        dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:='0';
        dmSis.tblObrazovanieObuka.Open;

        dm.tblLica.close;
        dm.tblLica.ParamByName('MB').Value:='%';
        dm.tblLica.Open;

        ID_MOLBA.Visible:=False;
        IMEPREZIME.Visible:=False;
        Label10.Visible:=False;

        MB_VRABOTEN.Tag:=1;
        VRABOTENNAZIV.Tag:=1;
     end;
  if tag = 3 then
     begin
       dmSis.tblObrazovanieObuka.close;
       dmSis.tblObrazovanieObuka.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:='0';
       dmSis.tblObrazovanieObuka.Open;

       //dm.tblLica.close;
       dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblLica.FullRefresh;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;

  if tag = 4 then
     begin
        dmSis.tblObrazovanieObuka.close;
        dmSis.tblObrazovanieObuka.ParamByName('MB').Value:='0';
        dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:='%';
        dmSis.tblObrazovanieObuka.Open;

        dmSis.tblMolba.close;
        dmSis.tblMolba.ParamByName('ID').Value:='%';
        dmSis.tblMolba.ParamByName('oglasID').Value:='%';
        dmSis.tblMolba.Open;

        VRABOTENNAZIV.Visible:=False;
        MB_VRABOTEN.Visible:=False;
        Label11.Visible:=False;

        cxGrid1DBTableView1ID_MOLBA.Visible:=True;
        cxGrid1DBTableView1IMEPREZIME.Visible:=true;
        cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
        cxGrid1DBTableView1NAZIV_VRABOTEN_TI.Visible:=false;

        ID_MOLBA.Tag:=1;
        IMEPREZIME.Tag:=1;
     end;

  if tag = 5 then
     begin
        dmSis.tblObrazovanieObuka.close;
        dmSis.tblObrazovanieObuka.ParamByName('MB').Value:='0';
        dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblObrazovanieObuka.Open;

        dmSis.tblMolba.ParamByName('ID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblMolba.ParamByName('oglasID').Value:='%';
        dmSis.tblMolba.FullRefresh;

        VRABOTENNAZIV.Visible:=False;
        MB_VRABOTEN.Visible:=False;
        Label11.Visible:=False;

        cxGrid1DBTableView1ID_MOLBA.Visible:=True;
        cxGrid1DBTableView1IMEPREZIME.Visible:=true;
        cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
        cxGrid1DBTableView1NAZIV_VRABOTEN_TI.Visible:=false;

        ID_MOLBA.Tag:=1;
        IMEPREZIME.Tag:=1;
     end;
end;

procedure TfrmObrazovanieObuka.KVALIFIKACIJADblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblObrazovanieObukaKVALIFIKACIJA.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblObrazovanieObukaKVALIFIKACIJA.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmObrazovanieObuka.NIVODblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblObrazovanieObukaNIVO.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblObrazovanieObukaNIVO.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmObrazovanieObuka.ZNAENJEDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblObrazovanieObukaZNAENJE.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblObrazovanieObukaZNAENJE.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

end.
