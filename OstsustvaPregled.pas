unit OstsustvaPregled;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.18                  }
{                                       }
{   28.02.2012                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxStatusBar, dxRibbonStatusBar, ExtCtrls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxContainer, StdCtrls, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxCalendar, cxGroupBox, ActnList, dxSkinsdxBarPainter, cxCheckBox,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxSkinsdxRibbonPainter, dxRibbon, Menus,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxPSCore, dxPScxCommon,
   dxBar, cxBarEditItem, cxHint, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, cxPC, cxPCdxBarPopupMenu,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk,
  dxScreenTip, dxCustomHint, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinOffice2013White, cxNavigator, Vcl.ComCtrls, dxCore, cxDateUtils,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxBarBuiltInMenu, dxRibbonCustomizationForm,
  cxButtons;

type
  TfrmPregedOtsustva = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1PLATENO_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1NEPLATENO_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1COUNT_1_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1COUNT_0_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1MB_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN_OUT: TcxGridDBColumn;
    Label4: TLabel;
    Re: TcxLookupComboBox;
    lPanel: TPanel;
    ActionList2: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarLargeButton10: TdxBarLargeButton;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxHintStyleController1: TcxHintStyleController;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Panel3: TPanel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1MB_OUT: TcxGridDBColumn;
    cxGridDBTableView1COUNT_0_OUT: TcxGridDBColumn;
    cxGridDBTableView1COUNT_1_OUT: TcxGridDBColumn;
    cxGridDBTableView1PLATENO_OUT: TcxGridDBColumn;
    cxGridDBTableView1NEPLATENO_OUT: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    OD_DATA: TcxDateEdit;
    DO_DATA: TcxDateEdit;
    Panel5: TPanel;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBTableView2MB: TcxGridDBColumn;
    cxGridDBTableView2RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGridDBTableView2BRGODINARESENIE: TcxGridDBColumn;
    cxGridDBTableView2RESENIEDENOVI: TcxGridDBColumn;
    cxGridDBTableView2OSTANATIDENOVI: TcxGridDBColumn;
    cxGridDBTableView2ID_RESENIE_GO: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    godina: TcxComboBox;
    Label2: TLabel;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxGridDBTableView2DENOVI: TcxGridDBColumn;
    cxGridDBTableView1NAZIVVRABOTEN_OUT: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_VRABOTEN_TI_OUT: TcxGridDBColumn;
    cxGridDBTableView2NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGridDBTableView2NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxGridDBTableView1RE_OUT: TcxGridDBColumn;
    cxGridDBTableView1HRMRABID_OUT: TcxGridDBColumn;
    cxGridDBTableView1RMRE_OUT: TcxGridDBColumn;
    cxGridDBTableView2RE: TcxGridDBColumn;
    cxGridDBTableView2RM_ID: TcxGridDBColumn;
    cxGridDBTableView2RABOTNO_MESTO: TcxGridDBColumn;
    cxGridDBTableView2RABOTNAEDINICANAZIV: TcxGridDBColumn;
    dxBarLargeButton20: TdxBarLargeButton;
    aPregledOtsustvaPoGodina: TAction;
    aDPregledOtsustvaPoGodina: TAction;
    cxTabSheet3: TcxTabSheet;
    pnl1: TPanel;
    grp1: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    OD_DATA_TO: TcxDateEdit;
    DO_DATA_TO: TcxDateEdit;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3RE_OUT: TcxGridDBColumn;
    cxGridDBTableView3MB_OUT: TcxGridDBColumn;
    cxGridDBTableView3NAZIVVRABOTEN_OUT: TcxGridDBColumn;
    cxGridDBTableView3RMRE_OUT: TcxGridDBColumn;
    cxGridDBTableView3RENAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView3RABMESTONAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView3HRMRABID_OUT: TcxGridDBColumn;
    cxGridDBTableView3NAZIV_VRABOTEN_TI_OUT: TcxGridDBColumn;
    cxGridDBTableView3TIP_OTSUSTVO: TcxGridDBColumn;
    cxGridDBTableView3TIP_OTSUSTVO_NAZIV: TcxGridDBColumn;
    cxGridDBTableView3COUNT_TIP_OTSUSTVO: TcxGridDBColumn;
    btnPregledPlateniNeplateni: TcxButton;
    btnPregledDenoviTipOtsustvo: TcxButton;
    actPregledPlateniNeplateni: TAction;
    actPregledDenoviTipOtsustvo: TAction;
    dxComponentPrinter1Link3: TdxGridReportLink;
    btnPregledPlateniNeplateni1: TcxButton;
    actPregledajOstanatiDenovi: TAction;
    procedure FormShow(Sender: TObject);
    procedure OD_DATAExit(Sender: TObject);
    procedure DO_DATAExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aIzlezExecute(Sender: TObject);
    procedure RePropertiesEditValueChanged(Sender: TObject);
    procedure ReExit(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure cxGridDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure godinaPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure cxGridDBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure aPregledOtsustvaPoGodinaExecute(Sender: TObject);
    procedure aDPregledOtsustvaPoGodinaExecute(Sender: TObject);
    procedure actPregledPlateniNeplateniExecute(Sender: TObject);
    procedure actPregledDenoviTipOtsustvoExecute(Sender: TObject);
    procedure OD_DATA_TOExit(Sender: TObject);
    procedure DO_DATA_TOExit(Sender: TObject);
    procedure actPregledajOstanatiDenoviExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPregedOtsustva: TfrmPregedOtsustva;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnitOtsustvo, Utils,
  dmUnit, dmReportUnit;

{$R *.dfm}

procedure TfrmPregedOtsustva.DO_DATAExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPregedOtsustva.DO_DATA_TOExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPregedOtsustva.FormCreate(Sender: TObject);
begin
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmPregedOtsustva.FormShow(Sender: TObject);
begin
//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
    procitajGridOdBaza(Name,cxGridDBTableView2,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGridDBTableView2.Name, dxComponentPrinter1Link2);


     OD_DATA.Date:=StrToDate('01.01.'+IntToStr(dmKon.godina));
     DO_DATA.Date:=StrToDate('31.12.'+IntToStr(dmKon.godina));

     OD_DATA_TO.Date:=StrToDate('01.01.'+IntToStr(dmKon.godina));
     DO_DATA_TO.Date:=StrToDate('31.12.'+IntToStr(dmKon.godina));

     re.EditValue:=firma;
     godina.EditValue:=dmkon.godina;

//     dmOtsustvo.tblOtsustvaPregled.ParamByName('Re').Value:= '%';
//     dmOtsustvo.tblOtsustvaPregled.ParamByName('odDatum').Value:=OD_DATA.Date;
//     dmOtsustvo.tblOtsustvaPregled.ParamByName('doDatum').Value:=DO_DATA.Date + 1;
//     dmOtsustvo.tblOtsustvaPregled.ParamByName('firma').Value:=dmKon.re;
//     dmOtsustvo.tblOtsustvaPregled.Open;

     dmOtsustvo.tblPodsektori.close;
     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.Open;

//     dmOtsustvo.tblOstanatiDenoviGO.Close;
//     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('firma').Value:=firma;
//     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('MB').Value:='%';
//     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('re').Value:='%';
//     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('godina').Value:=godina.EditValue;
//     dmOtsustvo.tblOstanatiDenoviGO.Open;

//     dmOtsustvo.tblRealizacijaGO.Close;
//     dmOtsustvo.tblRealizacijaGO.ParamByName('firma').Value:=firma;
//     dmOtsustvo.tblRealizacijaGO.ParamByName('godina').Value:=godina.EditValue;
//     dmOtsustvo.tblRealizacijaGO.Open;

//     dmOtsustvo.tblCountTipOtsustvo.ParamByName('Re').Value:= '%';
//     dmOtsustvo.tblCountTipOtsustvo.ParamByName('od_datum_in').Value:=OD_DATA_TO.Date;
//     dmOtsustvo.tblCountTipOtsustvo.ParamByName('do_datum_in').Value:=DO_DATA_TO.Date + 1;
//     dmOtsustvo.tblCountTipOtsustvo.ParamByName('firma').Value:=dmKon.re;
//     dmOtsustvo.tblCountTipOtsustvo.Open;

     cxPageControl1.ActivePage:=cxTabSheet1;

     cxGrid1.SetFocus;
end;

procedure TfrmPregedOtsustva.godinaPropertiesChange(Sender: TObject);
begin
       {  if godina.Text <> '' then
            begin
              dmOtsustvo.tblOstanatiDenoviGO.Close;
              dmOtsustvo.tblOstanatiDenoviGO.ParamByName('godina').Value:=godina.EditValue;
              dmOtsustvo.tblOstanatiDenoviGO.Open;
            end;      }
end;

procedure TfrmPregedOtsustva.OD_DATAExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;

end;

procedure TfrmPregedOtsustva.OD_DATA_TOExit(Sender: TObject);
begin
      TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPregedOtsustva.aBrisiIzgledExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
        begin
           brisiGridVoBaza(Name,cxGridDBTableView1);
           BrisiFormaIzgled(self);
        end
     else if cxPageControl1.ActivePage=cxTabSheet2 then
        begin
           brisiGridVoBaza(Name,cxGridDBTableView2);
           BrisiFormaIzgled(self);
        end
     else if cxPageControl1.ActivePage=cxTabSheet3 then
        begin
           brisiGridVoBaza(Name,cxGridDBTableView3);
           BrisiFormaIzgled(self);
        end

end;

procedure TfrmPregedOtsustva.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
        brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1)
     else if cxPageControl1.ActivePage=cxTabSheet2 then
        brisiPrintOdBaza(Name,cxGridDBTableView2.Name, dxComponentPrinter1Link2)
     else if cxPageControl1.ActivePage=cxTabSheet3 then
        brisiPrintOdBaza(Name,cxGridDBTableView3.Name, dxComponentPrinter1Link3)
end;

procedure TfrmPregedOtsustva.actPregledajOstanatiDenoviExecute(Sender: TObject);
begin

     dmOtsustvo.tblOstanatiDenoviGO.Close;
     if re.EditValue = firma then
        begin
           dmOtsustvo.tblOstanatiDenoviGO.ParamByName('RE').Value:='%';
        end
     else
        begin
           dmOtsustvo.tblOstanatiDenoviGO.ParamByName('RE').Value:= re.EditValue;
        end;
     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('godina').Value:=godina.EditValue;
     dmOtsustvo.tblOstanatiDenoviGO.Open;
end;

procedure TfrmPregedOtsustva.actPregledDenoviTipOtsustvoExecute(
  Sender: TObject);
begin
      if (OD_DATA_TO.Text <> '')and (DO_DATA_TO.Text <> '') then
        begin
           dmOtsustvo.tblCountTipOtsustvo.Close;
           if re.EditValue = firma then
             begin
               dmOtsustvo.tblCountTipOtsustvo.ParamByName('RE').Value:='%';
             end
           else
             begin
               dmOtsustvo.tblCountTipOtsustvo.ParamByName('RE').Value:= re.EditValue;
             end;
          dmOtsustvo.tblCountTipOtsustvo.ParamByName('od_datum_in').Value:=OD_DATA_TO.Date;
          dmOtsustvo.tblCountTipOtsustvo.ParamByName('do_datum_in').Value:=DO_DATA_TO.Date + 1;
          dmOtsustvo.tblCountTipOtsustvo.ParamByName('firma').Value:=dmKon.re;
          dmOtsustvo.tblCountTipOtsustvo.Open;
        end;
end;

procedure TfrmPregedOtsustva.actPregledPlateniNeplateniExecute(Sender: TObject);
begin
      if (OD_DATA.Text <> '')and (DO_DATA.Text <> '') then
        begin
          dmOtsustvo.tblOtsustvaPregled.close;
          if re.EditValue = firma then
             begin
               dmOtsustvo.tblOtsustvaPregled.ParamByName('Re').Value:= '%';
             end
          else
             begin
               dmOtsustvo.tblOtsustvaPregled.ParamByName('Re').Value:= re.EditValue;
             end;
         dmOtsustvo.tblOtsustvaPregled.ParamByName('odDatum').Value:=OD_DATA.Date;
         dmOtsustvo.tblOtsustvaPregled.ParamByName('doDatum').Value:=DO_DATA.Date + 1;
         dmOtsustvo.tblOtsustvaPregled.ParamByName('firma').Value:=dmKon.re;
         dmOtsustvo.tblOtsustvaPregled.Open;
        end;
end;

procedure TfrmPregedOtsustva.aDPregledOtsustvaPoGodinaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=8;
     dmReport.tblReportDizajn2.ParamByName('app').Value:='MM';
     dmReport.tblReportDizajn2.Open;

     dmReport.tblOtsustvaPoGodina.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
end;

procedure TfrmPregedOtsustva.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPregedOtsustva.aPageSetupExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
        dxComponentPrinter1Link1.PageSetup
     else if cxPageControl1.ActivePage=cxTabSheet2 then
        dxComponentPrinter1Link2.PageSetup
     else if cxPageControl1.ActivePage=cxTabSheet3 then
        dxComponentPrinter1Link3.PageSetup;

end;

procedure TfrmPregedOtsustva.aPecatiTabelaExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage=cxTabSheet1 then
  begin
     dxComponentPrinter1Link1.ReportTitle.Text := '������� �� �������/���������, ���������/����������� ��������';

     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� �������: ' + intTostr(Re.EditValue) + '. ' + Re.EditText);
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ �� ��������: ' + DateToStr(OD_DATA.Date) + ' - ' + DateToStr(DO_DATA.Date));

    dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
  end
  else if cxPageControl1.ActivePage=cxTabSheet2 then
  begin
      dxComponentPrinter1Link2.ReportTitle.Text := '������� �� �������� ������ ������ ������� �� ��';

      dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
      dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
      dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

      dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
      dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� �������: ' + intTostr(Re.EditValue) + '. ' + Re.EditText);
      dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: ' + godina.Text);

      dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
  end
  else if cxPageControl1.ActivePage=cxTabSheet3 then
  begin
      dxComponentPrinter1Link2.ReportTitle.Text := '������� �� ��������� ������ ������ ��� �� ��������';

      dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
      dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
      dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

      dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
      dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� �������: ' + intTostr(Re.EditValue) + '. ' + Re.EditText);
      dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: ' + godina.Text);

      dxComponentPrinter1.Preview(true, dxComponentPrinter1Link3);
  end;
end;

procedure TfrmPregedOtsustva.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
        dxComponentPrinter1Link1.DesignReport()
     else if cxPageControl1.ActivePage=cxTabSheet2 then
        dxComponentPrinter1Link2.DesignReport()
     else if cxPageControl1.ActivePage=cxTabSheet3 then
        dxComponentPrinter1Link3.DesignReport();
end;

procedure TfrmPregedOtsustva.aPregledOtsustvaPoGodinaExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=8;
     dmReport.tblReportDizajn2.ParamByName('app').Value:='MM';
     dmReport.tblReportDizajn2.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmPregedOtsustva.aSnimiIzgledExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
        zacuvajGridVoBaza(Name,cxGridDBTableView1)
     else if cxPageControl1.ActivePage=cxTabSheet2 then
        zacuvajGridVoBaza(Name,cxGridDBTableView2)
     else if cxPageControl1.ActivePage=cxTabSheet3 then
        zacuvajGridVoBaza(Name,cxGridDBTableView3);
end;

procedure TfrmPregedOtsustva.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
        zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link1)
     else if cxPageControl1.ActivePage=cxTabSheet2 then
       zacuvajPrintVoBaza(Name,cxGridDBTableView2.Name,dxComponentPrinter1Link2)
     else if cxPageControl1.ActivePage=cxTabSheet3 then
       zacuvajPrintVoBaza(Name,cxGridDBTableView3.Name,dxComponentPrinter1Link3)
end;

procedure TfrmPregedOtsustva.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage=cxTabSheet1 then
         zacuvajVoExcel(cxGrid2, '������� �� �������_���������_���������_����������� ��������')
     else if cxPageControl1.ActivePage=cxTabSheet2 then
         zacuvajVoExcel(cxGrid3, '������� �� �������� ������ ������ ������� �� ��')
     else if cxPageControl1.ActivePage=cxTabSheet2 then
         zacuvajVoExcel(cxGrid3, '������� �� �������� ������ ������ ������� �� ��')
end;

procedure TfrmPregedOtsustva.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmPregedOtsustva.cxGridDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView1);
end;

procedure TfrmPregedOtsustva.cxGridDBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView2);
end;

procedure TfrmPregedOtsustva.ReExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPregedOtsustva.RePropertiesEditValueChanged(Sender: TObject);
begin
   {  dmOtsustvo.tblOtsustvaPregled.close;
     dmOtsustvo.tblOstanatiDenoviGO.Close;
     dmOtsustvo.tblCountTipOtsustvo.Close;
     if re.EditValue = firma then
        begin
           dmOtsustvo.tblOtsustvaPregled.ParamByName('Re').Value:= '%';
           dmOtsustvo.tblOstanatiDenoviGO.ParamByName('RE').Value:='%';
           dmOtsustvo.tblCountTipOtsustvo.ParamByName('RE').Value:='%';
        end
     else
        begin
           dmOtsustvo.tblOtsustvaPregled.ParamByName('Re').Value:= re.EditValue;
           dmOtsustvo.tblOstanatiDenoviGO.ParamByName('RE').Value:= re.EditValue;
           dmOtsustvo.tblCountTipOtsustvo.ParamByName('RE').Value:= re.EditValue;
        end;
     dmOtsustvo.tblOtsustvaPregled.ParamByName('odDatum').Value:=OD_DATA.Date;
     dmOtsustvo.tblOtsustvaPregled.ParamByName('doDatum').Value:=DO_DATA.Date + 1;
     dmOtsustvo.tblOtsustvaPregled.ParamByName('firma').Value:=dmKon.re;
     dmOtsustvo.tblOtsustvaPregled.Open;

     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOstanatiDenoviGO.ParamByName('godina').Value:=godina.EditValue;
     dmOtsustvo.tblOstanatiDenoviGO.Open;

     dmOtsustvo.tblCountTipOtsustvo.ParamByName('od_datum_in').Value:=OD_DATA.Date;
     dmOtsustvo.tblCountTipOtsustvo.ParamByName('do_datum_in').Value:=DO_DATA.Date + 1;
     dmOtsustvo.tblCountTipOtsustvo.ParamByName('firma').Value:=dmKon.re;
     dmOtsustvo.tblCountTipOtsustvo.Open;

   }
end;

procedure TfrmPregedOtsustva.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

end.
