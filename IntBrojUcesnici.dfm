inherited frmIntBrojUcesnici: TfrmIntBrojUcesnici
  Caption = #1041#1088#1086#1112' '#1085#1072' '#1091#1095#1077#1089#1085#1080#1094#1080
  ClientHeight = 583
  ClientWidth = 696
  ExplicitWidth = 704
  ExplicitHeight = 617
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 696
    ExplicitWidth = 696
    inherited cxGrid1: TcxGrid
      Width = 692
      ExplicitWidth = 692
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dmOtsustvo.dsIntBrUcesnici
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 182
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 533
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 696
    Height = 184
    ExplicitWidth = 696
    ExplicitHeight = 184
    inherited Label1: TLabel
      Left = 405
      Top = 9
      Visible = False
      ExplicitLeft = 405
      ExplicitTop = 9
    end
    object Label2: TLabel [1]
      Left = 21
      Top = 33
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = -31
      Top = 60
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 461
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsIntBrUcesnici
      TabOrder = 4
      Visible = False
      ExplicitLeft = 461
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 597
      Top = 128
      TabOrder = 3
      ExplicitLeft = 597
      ExplicitTop = 128
    end
    inherited ZapisiButton: TcxButton
      Left = 516
      Top = 128
      TabOrder = 2
      ExplicitLeft = 516
      ExplicitTop = 128
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 77
      Top = 30
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmOtsustvo.dsIntBrUcesnici
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 396
    end
    object txtOpis: TcxDBMemo
      Left = 77
      Top = 57
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmOtsustvo.dsIntBrUcesnici
      Properties.ScrollBars = ssVertical
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 99
      Width = 396
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 696
    ExplicitWidth = 696
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 560
    Width = 696
    ExplicitTop = 560
    ExplicitWidth = 696
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40294.642262731480000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
