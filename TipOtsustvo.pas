unit TipOtsustvo;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.8                   }
{                                       }
{   26.04.2011                          }
{                                       }
(***************************************)


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxDropDownEdit,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, FIBDataSet,
  pFIBDataSet, cxGroupBox, cxRadioGroup, cxSplitter, dxRibbonSkins,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxPScxSchedulerLnk,
  dxCustomHint, cxHint, cxColorComboBox, cxDBColorComboBox, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TfrmTipOtsustvo = class(TfrmMaster)
    NAZIV: TcxDBTextEdit;
    Label2: TLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    Label10: TLabel;
    TIP_PLATA: TcxDBTextEdit;
    SIFRACAS: TcxDBTextEdit;
    Label3: TLabel;
    SIFRA_NADOMEST: TcxDBTextEdit;
    TERET: TcxDBTextEdit;
    plt_tip_nadomest: TpFIBDataSet;
    dsPlt_tip_nadomest: TDataSource;
    plt_tip_nadomestSIF_NAD: TFIBStringField;
    plt_tip_nadomestTERET: TFIBSmallIntField;
    plt_tip_nadomestDEN_DOL_GRANICA: TFIBSmallIntField;
    plt_tip_nadomestDEN_GOR_GRANICA: TFIBSmallIntField;
    plt_tip_nadomestPROC_NETO: TFIBFloatField;
    plt_tip_nadomestPROC_NETO_PRID: TFIBFloatField;
    plt_tip_nadomestOPIS: TFIBStringField;
    plt_tip_nadomestTeretNaziv: TFIBStringField;
    plt_tip_cas: TpFIBDataSet;
    dsPlt_tip_cas: TDataSource;
    plt_tip_casSIFRA: TFIBSmallIntField;
    plt_tip_casTIP_PLATA: TFIBSmallIntField;
    plt_tip_casFIRMA: TFIBIntegerField;
    plt_tip_casOPIS: TFIBStringField;
    plt_tip_casSIF_STAZ: TFIBStringField;
    plt_tip_casKOEFICIENT: TFIBFloatField;
    plt_tip_casOSN_CAS: TFIBSmallIntField;
    tipNadomest: TcxLookupComboBox;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    cxGrid1DBTableView1Column6: TcxGridDBColumn;
    tipCas: TcxLookupComboBox;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1FIRMA: TcxGridDBColumn;
    Label4: TLabel;
    denovi: TcxDBTextEdit;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGrid1DBTableView1DENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1PLATENO: TcxGridDBColumn;
    cxGrid1DBTableView1PlatenoNaziv: TcxGridDBColumn;
    SplitterdPanel: TcxSplitter;
    cxDBRadioGroup2: TcxDBRadioGroup;
    cxGrid1DBTableView1DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1DogovorNaziv: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    Label5: TLabel;
    Boja: TcxDBColorComboBox;
    cxGrid1DBTableView1BOJA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZemiNaziv();
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure TIP_PLATAExit(Sender: TObject);
    procedure SIFRACASExit(Sender: TObject);
    procedure tipCasClick(Sender: TObject);
    procedure SIFRA_NADOMESTExit(Sender: TObject);
    procedure TERETExit(Sender: TObject);
    procedure tipNadomestClick(Sender: TObject);
    procedure tipCasExit(Sender: TObject);
    procedure tipNadomestExit(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipOtsustvo: TfrmTipOtsustvo;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, dmUnitOtsustvo,
  Utils;

{$R *.dfm}

procedure TfrmTipOtsustvo.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  ZemiNaziv();
end;

procedure TfrmTipOtsustvo.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
      VK_RETURN:
        begin
          if Tag = 1  then
             Close;
        end;
  end;
end;

procedure TfrmTipOtsustvo.FormCreate(Sender: TObject);
begin
  inherited;
  dmOtsustvo.tblTipOtsustvo.ParamByName('firma').Value:=firma;
  dmOtsustvo.tbltipOtsustvo.Open;

  plt_tip_nadomest.Open;
  plt_tip_cas.Open;
end;

procedure TfrmTipOtsustvo.SIFRACASExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (dmOtsustvo.tblTipOtsustvo.State) in [dsInsert, dsEdit] then
    begin
         if (((Sender as TWinControl)= SIFRACAS) or ((Sender as TWinControl)= TIP_PLATA)) then
         begin
            ZemiNaziv;
         end;
    end;
end;

procedure TfrmTipOtsustvo.SIFRA_NADOMESTExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (dmOtsustvo.tblTipOtsustvo.State) in [dsInsert, dsEdit] then
        begin
           if (((Sender as TWinControl)= SIFRA_NADOMEST) or ((Sender as TWinControl)= TERET)) then
              begin
                ZemiNaziv;
              end;
        end;
end;

procedure TfrmTipOtsustvo.TERETExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (dmOtsustvo.tblTipOtsustvo.State) in [dsInsert, dsEdit] then
        begin
           if (((Sender as TWinControl)= SIFRA_NADOMEST) or ((Sender as TWinControl)= TERET)) then
              begin
                ZemiNaziv;
              end;
        end;
end;

procedure TfrmTipOtsustvo.tipCasClick(Sender: TObject);
begin
     dmOtsustvo.tblTipOtsustvoSIFRA.Value:=plt_tip_casSIFRA.Value;
     dmOtsustvo.tblTipOtsustvoTIP_PLATA.Value:=plt_tip_casTIP_PLATA.Value;
end;

procedure TfrmTipOtsustvo.tipCasExit(Sender: TObject);
begin
      TEdit(Sender).Color:=clWhite;
end;

procedure TfrmTipOtsustvo.tipNadomestClick(Sender: TObject);
begin
     dmOtsustvo.tblTipOtsustvoSIF_NAD.Value:=plt_tip_nadomestSIF_NAD.Value;
     dmOtsustvo.tblTipOtsustvoTERET.Value:=plt_tip_nadomestTERET.Value;
end;

procedure TfrmTipOtsustvo.tipNadomestExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmTipOtsustvo.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(111);
end;

procedure TfrmTipOtsustvo.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dmOtsustvo.tblTipOtsustvoFIRMA.Value:=firma;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;


procedure TfrmTipOtsustvo.cxDBTextEditAllExit(Sender: TObject);
begin
   inherited;
   if (dmOtsustvo.tblTipOtsustvo.State) in [dsInsert, dsEdit] then
    begin
         if (((Sender as TWinControl)= SIFRACAS) or ((Sender as TWinControl)= TIP_PLATA)) then
         begin
            ZemiNaziv;
         end;
    end;
end;

procedure TfrmTipOtsustvo.TIP_PLATAExit(Sender: TObject);
begin
   TEdit(Sender).Color:=clWhite;
   if (dmOtsustvo.tblTipOtsustvo.State) in [dsInsert, dsEdit] then
    begin
         if (((Sender as TWinControl)= SIFRACAS) or ((Sender as TWinControl)= TIP_PLATA)) then
         begin
            ZemiNaziv;
         end;
    end;
end;

procedure TfrmTipOtsustvo.ZemiNaziv();
begin
  if (SIFRACAS.Text <>'') and (TIP_PLATA.Text<>'')  then
  begin
       tipCas.EditValue := VarArrayOf([StrToInt(SIFRACAS.Text), StrToInt(TIP_PLATA.Text)]);
  end
  else
  begin
      tipCas.Clear;
  end;

  if (SIFRA_NADOMEST.Text <>'') and (TERET.Text<>'')  then
  begin
       tipNadomest.EditValue := VarArrayOf([SIFRA_NADOMEST.Text , StrToInt(TERET.Text)]);
  end
  else
  begin
      tipNadomest.Clear;
  end;

end;

end.
