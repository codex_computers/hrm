(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

unit DecaSopruznici;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxGroupBox, cxRadioGroup, cxDropDownEdit, cxCalendar,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxCustomHint, cxHint;

type
//  niza = Array[1..5] of Variant;

  TfrmDecaSopruznici = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    dPanel: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    Sifra: TcxDBTextEdit;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    MB_VRABOTEN: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    MB: TcxDBTextEdit;
    Prezime: TcxDBTextEdit;
    tatkovo_ime: TcxDBTextEdit;
    ime: TcxDBTextEdit;
    vozrast: TcxDBTextEdit;
    DATUM_RADJANJE: TcxDBDateEdit;
    Pol: TcxDBRadioGroup;
    cxDBRadioGroup1: TcxDBRadioGroup;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1RODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1RODITELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MAT_BR: TcxGridDBColumn;
    cxGrid1DBTableView1PRZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1DATA_RAGJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1POL: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1OSIGURENIK: TcxGridDBColumn;
    cxGrid1DBTableView1OsigurenikDaNe: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    VMB: TcxDBTextEdit;
    cxGroupBox2: TcxGroupBox;
    Label10: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    SMB: TcxDBTextEdit;
    cxDBTextEdit4: TcxDBTextEdit;
    cxDBTextEdit6: TcxDBTextEdit;
    Panel2: TPanel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    aZapisiSopruznik: TAction;
    aOtkaziSopruznik: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxDBRadioGroup4: TcxDBRadioGroup;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    cxGridDBTableView1RODITEL: TcxGridDBColumn;
    cxGridDBTableView1IME: TcxGridDBColumn;
    cxGridDBTableView1PRZIME: TcxGridDBColumn;
    cxGridDBTableView1MAT_BR: TcxGridDBColumn;
    cxGridDBTableView1RODITELNAZIV: TcxGridDBColumn;
    cxGridDBTableView1Sopruznik: TcxGridDBColumn;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxHintStyleController1: TcxHintStyleController;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function proverkaModul11(broj: Int64): boolean;
    procedure MBPropertiesEditValueChanged(Sender: TObject);
    procedure aZapisiSopruznikExecute(Sender: TObject);
    procedure aOtkaziSopruznikExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmDecaSopruznici: TfrmDecaSopruznici;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmDecaSopruznici.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmDecaSopruznici.aNovExecute(Sender: TObject);
begin
 if cxPageControl1.ActivePage = cxTabSheet1 then
    begin
      if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          cxGrid1DBTableView1.DataController.DataSet.Insert;
          if tag = 1 then
            begin
              dm.tblDecaRODITEL.Value:=dm.tblLicaVraboteniMB.Value;
              MB.SetFocus;
            end
          else
              MB_VRABOTEN.SetFocus;
        end
      else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
     end;
  if cxPageControl1.ActivePage = cxTabSheet2 then
    begin
      if(cxGridDBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          Panel1.Enabled:=True;
          Panel2.Enabled:=False;
          cxGridDBTableView1.DataController.DataSet.Insert;
          if tag = 1 then
            begin
              dm.tblSopruznikRODITEL.Value:=dm.tblLicaVraboteniMB.Value;
              SMB.SetFocus;
            end
          else
              VMB.SetFocus;
        end
      else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
     end;
 end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmDecaSopruznici.aAzurirajExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        begin
          if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
              dPanel.Enabled:=True;
              lPanel.Enabled:=False;
              if tag = 1  then
                MB.SetFocus
              else
                MB_VRABOTEN.SetFocus;
              cxGrid1DBTableView1.DataController.DataSet.Edit;
            end
          else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
        end;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        begin
          if(cxGridDBTableView1.DataController.DataSource.State = dsBrowse) then
            begin
              Panel1.Enabled:=True;
              Panel2.Enabled:=False;
              if tag = 1  then
                SMB.SetFocus
              else
                VMB.SetFocus;
              cxGridDBTableView1.DataController.DataSet.Edit;
            end
          else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
        end;
end;

//	����� �� ������ �� ������������� �����
procedure TfrmDecaSopruznici.aBrisiExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        begin
           if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
              cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
      else
        begin
           if(cxGridDBTableView1.DataController.DataSource.State = dsBrowse) then
              cxGridDBTableView1.DataController.DataSet.Delete();
        end;
end;

//	����� �� ���������� �� ����������
procedure TfrmDecaSopruznici.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmDecaSopruznici.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmDecaSopruznici.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  zacuvajGridVoBaza(Name,cxGridDBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmDecaSopruznici.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajVoExcel(cxGrid1, Caption)
     else
        zacuvajVoExcel(cxGrid2, Caption)

end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmDecaSopruznici.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmDecaSopruznici.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmDecaSopruznici.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmDecaSopruznici.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmDecaSopruznici.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmDecaSopruznici.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmDecaSopruznici.prefrli;
begin
end;

procedure TfrmDecaSopruznici.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            dm.tblDecasrodstvo.value:=0;
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
    if (cxGridDBTableView1.DataController.DataSource.State = dsEdit) or (cxGridDBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGridDBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(Panel1) = false) then
          begin
            cxGridDBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmDecaSopruznici.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmDecaSopruznici.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := '��������� �� ����';
    dxComponentPrinter1Link2.ReportTitle.Text := '��������� �� ������/�������';
  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);

   if tag = 1 then
     begin
       dm.tblDeca.close;
       dm.tblDeca.ParamByName('roditelMB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblDeca.Open;

       dm.tblSopruznik.close;
       dm.tblSopruznik.ParamByName('roditelMB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblSopruznik.Open;

       dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblLica.FullRefresh;
     end
  else
     begin
       dm.tblSopruznik.close;
       dm.tblSopruznik.ParamByName('roditelMB').Value:='%';
       dm.tblSopruznik.Open;

       dm.tblDeca.close;
       dm.tblDeca.ParamByName('roditelMB').Value:='%';
       dm.tblDeca.Open;

       dm.tblLica.Close;
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.Open;
     end;
end;
//------------------------------------------------------------------------------

procedure TfrmDecaSopruznici.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmDecaSopruznici.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmDecaSopruznici.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        dm.tblDecasrodstvo.value:=0;
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
    end;
  end;
end;

procedure TfrmDecaSopruznici.aZapisiSopruznikExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGridDBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(Panel1) = false) then
    begin
        cxGridDBTableView1.DataController.DataSet.Post;
        Panel1.Enabled:=false;
        Panel2.Enabled:=true;
        cxGrid2.SetFocus;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmDecaSopruznici.aOtkaziExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = cxTabSheet1 then
 begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
 end;
end;

procedure TfrmDecaSopruznici.aOtkaziSopruznikExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = cxTabSheet2 then
 begin
  if (cxGridDBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGridDBTableView1.DataController.DataSet.Cancel;
      RestoreControls(Panel1);
      Panel1.Enabled := false;
      Panel2.Enabled := true;
      cxGrid2.SetFocus;
  end;
 end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmDecaSopruznici.aPageSetupExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
      dxComponentPrinter1Link1.PageSetup
     else
       dxComponentPrinter1Link2.PageSetup
end;

//	����� �� ������� �� ������
procedure TfrmDecaSopruznici.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
      begin
         dxComponentPrinter1Link1.ReportTitle.Text := Caption;
         dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
         dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
         dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
         dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
      end
     else
      begin
         dxComponentPrinter1Link2.ReportTitle.Text := Caption;
         dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
         dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
         dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
         dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
      end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmDecaSopruznici.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1Link1.DesignReport()
     else
        dxComponentPrinter1Link2.DesignReport()
end;

procedure TfrmDecaSopruznici.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1)
     else
        zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link2)
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmDecaSopruznici.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
      if cxPageControl1.ActivePage = cxTabSheet1 then
         brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1)
      else
         brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmDecaSopruznici.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmDecaSopruznici.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmDecaSopruznici.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

function TfrmDecaSopruznici.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;


procedure TfrmDecaSopruznici.MBPropertiesEditValueChanged(Sender: TObject);
var pom, date:string;
begin
  if (mb.Text <> '')  and ((cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse)) then
      begin
        if proverkaModul11(StrToInt64(MB.Text))then
          begin
            pom:=dm.tblDecaMAT_BR.Value;
            date:= pom[1] + pom[2] + '.' + pom[3] + pom[4] + '.';
            if StrToInt(pom[5]) = 9 then
              date:=date + '1'
            else if StrToInt(pom[5]) in [0,1]then
              date:=date + '2';
            date:=date + pom[5] + pom[6]+ pom[7];
            dm.tblDecaDATA_RAGJANJE.Value:=StrToDate(date);
            if strtoint(pom[10]) in [0,1,2,3,4] then
              dm.tblDecaPOL.Value:=1
            else if strtoint(pom[10]) in [5,6,7,8,9] then
              dm.tblDecaPOL.Value:=2
          end
        else
          begin
            ShowMessage('��������� ������� ��� �� � ������� !!!');
          end;
      end;
end;



end.
