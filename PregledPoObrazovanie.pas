unit PregledPoObrazovanie;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxGroupBox, cxRadioGroup, cxImage, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxMemo, ExtDlgs, FIBDataSet, pFIBDataSet,
  cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxCustomHint, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  dxRibbonCustomizationForm, dxPScxSchedulerLnk, System.Actions;

type
  TfrmPregledPoObrazovanie = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    obrazovanie: TcxLookupComboBox;
    CheckBox1: TCheckBox;
    buttonZapisi: TcxButton;
    ActionList1: TActionList;
    aPregled: TAction;
    aIzlez: TAction;
    aIscistiDatum: TAction;
    aIscistiSttus: TAction;
    Panel2: TPanel;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ActionList2: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    Action1: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    aPregledPoSektori: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    dsVraboteniPoObrazovanie: TDataSource;
    VraboteniPoObrazovanie: TpFIBDataSet;
    VraboteniPoObrazovanieMB: TFIBStringField;
    VraboteniPoObrazovanieRABEDINICANAZIV: TFIBStringField;
    VraboteniPoObrazovanieRABMESTONAZIV: TFIBStringField;
    VraboteniPoObrazovanieREID: TFIBIntegerField;
    VraboteniPoObrazovanieRABMESTOID: TFIBIntegerField;
    VraboteniPoObrazovanieOBRAZOVANIEOPIS: TFIBStringField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1RABEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OBRAZOVANIEOPIS: TcxGridDBColumn;
    aDizajnReport: TAction;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxHintStyleController1: TcxHintStyleController;
    VraboteniPoObrazovanieNAZIV_VRABOTEN: TFIBStringField;
    VraboteniPoObrazovanieNAZIV_VRABOTEN_TI: TFIBStringField;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBox1Click(Sender: TObject);
    procedure aPregledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aDizajnReportExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPregledPoObrazovanie: TfrmPregledPoObrazovanie;

implementation

uses dmSistematizacija, dmUnit, dmKonekcija, dmReportUnit, dmResources,
  dmSystem, Utils;

{$R *.dfm}

procedure TfrmPregledPoObrazovanie.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPregledPoObrazovanie.aDizajnReportExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30081;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.VraboteniPoObrazovanie.Close;
     dm.VraboteniPoObrazovanie.ParamByName('Firma').Value:=dmKon.re;
     dm.VraboteniPoObrazovanie.ParamByName('obrazovanieID').Value:='%';
     dm.VraboteniPoObrazovanie.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
//   dmReport.frxReport1.ShowReport;
end;

procedure TfrmPregledPoObrazovanie.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPregledPoObrazovanie.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmPregledPoObrazovanie.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPregledPoObrazovanie.aPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPregledPoObrazovanie.aPregledExecute(Sender: TObject);
var RptStream :TStream;
begin
     if (CheckBox1.Checked = False) and (obrazovanie.Text = '') then
        begin
          ShowMessage('�������� ����������� !!!');
          obrazovanie.SetFocus;
        end
     else
        begin
         dmReport.tblReportDizajn2.Close;
         dmReport.tblReportDizajn2.ParamByName('br').Value:=30081;
         dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
         dmReport.tblReportDizajn2.Open;

          if CheckBox1.Checked = true then
            begin
              dm.VraboteniPoObrazovanie.Close;
              dm.VraboteniPoObrazovanie.ParamByName('Firma').Value:=dmKon.re;
              dm.VraboteniPoObrazovanie.ParamByName('obrazovanieID').Value:='%';
              dm.VraboteniPoObrazovanie.Open;
            end
          else
            begin
              dm.VraboteniPoObrazovanie.Close;
              dm.VraboteniPoObrazovanie.ParamByName('Firma').Value:=dmKon.re;
              dm.VraboteniPoObrazovanie.ParamByName('obrazovanieID').Value:=obrazovanie.EditValue;
              dm.VraboteniPoObrazovanie.Open;
            end;

          RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream) ;

//          dmReport.frxReport1.DesignReport();
        dmReport.frxReport1.ShowReport;
        end;
end;

procedure TfrmPregledPoObrazovanie.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmPregledPoObrazovanie.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPregledPoObrazovanie.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmPregledPoObrazovanie.CheckBox1Click(Sender: TObject);
begin
     if CheckBox1.Checked = true then
        begin
          obrazovanie.Text:='';
          GroupBox1.Enabled:=False;
        end
     else
        GroupBox1.Enabled:=True;
end;

procedure TfrmPregledPoObrazovanie.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPregledPoObrazovanie.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   //  dmSis.tblObrazovanie.close;
end;

procedure TfrmPregledPoObrazovanie.FormCreate(Sender: TObject);
begin
     dmSis.tblObrazovanie.close;
     dmSis.tblObrazovanie.Open;

     VraboteniPoObrazovanie.Close;
     VraboteniPoObrazovanie.ParamByName('Firma').Value:=dmKon.re;
     VraboteniPoObrazovanie.ParamByName('obrazovanieID').Value:='%';
     VraboteniPoObrazovanie.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmPregledPoObrazovanie.FormShow(Sender: TObject);
begin
      dxBarManager1Bar1.Caption := Caption;
      procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
      procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

end.
