  program HRM;

uses
  Forms,
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  Utils in '..\Share2010\Utils.pas',
  Master in '..\Share2010\Master.pas' {frmMaster},
  Login in '..\Share2010\Login.pas' {frmLogin},
  Main in 'Main.pas' {frmMain},
  Organogram in 'Organogram.pas' {frmOrganogram},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  Repository in 'Repository.pas' {Form1},
  VidVrabotuvanje in 'VidVrabotuvanje.pas' {frmVidVrabotuvanje},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  GrupaRM in 'GrupaRM.pas' {frmGrupaRM},
  GrupaPlata in 'GrupaPlata.pas' {frmGrupaPlata},
  dmUnitOtsustvo in 'dmUnitOtsustvo.pas' {dmOtsustvo: TDataModule},
  Veroispoved in 'Veroispoved.pas' {frmVeroispoved},
  Nacionalnost in 'Nacionalnost.pas' {frmNacionalnost},
  Praznici in 'Praznici.pas' {frmPraznici},
  TipOtsustvo in 'TipOtsustvo.pas' {frmTipOtsustvo},
  RabotnoMesto in 'RabotnoMesto.pas' {frmRabotnoMesto},
  dmSistematizacija in 'dmSistematizacija.pas' {dmSis: TDataModule},
  PlanZaOtsustva in 'PlanZaOtsustva.pas' {frmPlanZaOtsustvo},
  dmReportUnit in 'dmReportUnit.pas' {dmReport: TDataModule},
  dmUnitUcinok in 'dmUnitUcinok.pas' {dmUcinok: TDataModule},
  FormConfig in '..\Share2010\FormConfig.pas' {frmFormConfig},
  FeedBack in 'FeedBack.pas' {frmFeedBack},
  Kompetencija in 'Kompetencija.pas' {frmKompetencija},
  GrupaNaPrasanja in 'GrupaNaPrasanja.pas' {frmGrupaPrasanja},
  FeedBackStavki in 'FeedBackStavki.pas' {frmFeedBackStavki},
  RatingFrom in 'RatingFrom.pas' {frmRatingFrom},
  VraboteniHRM in 'VraboteniHRM.pas' {frmHRMVraboteni},
  Molbi in 'Molbi.pas' {frmMolbi},
  PopolnuvanjeRezultati in 'PopolnuvanjeRezultati.pas' {frmVnesNaOcenki},
  KriteriumSelekcija in 'KriteriumSelekcija.pas' {frmKriteriumSelekcija},
  Intervju in 'Intervju.pas' {frmIntervju},
  PregledPoPol in 'PregledPoPol.pas' {frmPregledPoPol},
  Kontakt in 'Kontakt.pas' {frmKontakt},
  Obrazovanie in 'Obrazovanie.pas' {frmObrazovanie},
  ObrazovanieObuka in 'ObrazovanieObuka.pas' {frmObrazovani
  ijaOtsustva},
  UcesniciObuka in 'UcesniciObuka.pas' {frmUcesniciObuka},
  DogovorVrabotuvanje in 'DogovorVrabotuvanje.pas' {frmDogovorVrabotuvanje},
  ResenieZaPreraspredelba in 'ResenieZaPreraspredelba.pas' {frmReseniePreraspredelba},
  VrabotenRM in 'VrabotenRM.pas' {frmPromenaRM},
  BaranjeZaGodisenOdmor in 'BaranjeZaGodisenOdmor.pas' {frmBaranjeZaGodisenOdmor},
  ResenieZaGodisenOdmor in 'ResenieZaGodisenOdmor.pas' {frmResenijeZaGodisenOdmor},
  Opstina in '..\Share2010\Opstina.pas' {frmOpstina},
  Mesto in '..\Share2010\Mesto.pas' {frmMesto},
  Drzava in '..\Share2010\Drzava.pas' {frmDrzava},
  LVKStranskiJazici in 'LVKStranskiJazici.pas' {frmStranskiJazik},
  PPR_Obrazec in 'PPR_Obrazec.pas' {frmPPR_Obrazec},
  NacinVrabotuvanje in 'NacinVrabotuvanje.pas' {frmNacinVrabotuvanje},
  OsnovNaOsiguruvanje in 'OsnovNaOsiguruvanje.pas' {frmOsnovNaOsiguruvanje},
  TipNaPovreda in 'TipNaPovreda.pas' {frmTipNaPovreda},
  Povreda in 'Povreda.pas' {frmPovreda},
  PovredaNaRM in 'PovredaNaRM.pas' {frmPovredaNaRM},
  Notepad in '..\Share2010\Notepad.pas' {frmNotepad},
  IskustvoRabotno in 'IskustvoRabotno.pas' {frmIskustvoRabotno},
  VestiniKvalifikacii in 'VestiniKvalifikacii.pas' {frmVestiniKvalifikacii},
  Deca in 'Deca.pas' {frmDeca},
  ViewVraboteni in 'ViewVraboteni.pas' {frmViewVraboteni},
  DogSezonskoRabotenje in 'DogSezonskoRabotenje.pas' {frmDogSezonskoRabotenje},
  ResenieZaOtkaz in 'ResenieZaOtkaz.pas' {frmResenieOtkaz},
  VidOtkaz in 'VidOtkaz.pas' {frmVidOtkaz},
  PregledNaDokumenti in 'PregledNaDokumenti.pas' {frmPregledNaDokumenti},
  IzvestajDokumenti in 'IzvestajDokumenti.pas' {frmIzvestajDokumenti},
  DogHonorarnaSorabotka in 'DogHonorarnaSorabotka.pas' {frmDogHonorarnaSorabotka},
  PregledNaNevraboteni in 'PregledNaNevraboteni.pas' {frmPregledNevraboteni},
  NKM in 'NKM.pas' {frmNKM},
  Kategorija in 'Kategorija.pas' {frmKategorija},
  ParamPoKolektivenDog in 'ParamPoKolektivenDog.pas' {frmParamKolektivenDog},
  AneksNaDogovor in 'AneksNaDogovor.pas' {frmAneksNaDogovor},
  Specijalizacija in 'Specijalizacija.pas' {frmSpecijalizacija},
  EvidencijaOtsustva in 'EvidencijaOtsustva.pas' {frmEvidencijaOtsustva},
  PodatociLekari in 'PodatociLekari.pas' {frmPodatociLekari},
  StazKalkulator in 'StazKalkulator.pas' {frmStazKalkulator},
  OstsustvaPregled in 'OstsustvaPregled.pas' {frmPregedOtsustva},
  DecaSopruznici in 'DecaSopruznici.pas' {frmDecaSopruznici},
  DogVolonteri in 'DogVolonteri.pas' {frmDogVolonteri},
  PrijavaOdjava in 'PrijavaOdjava.pas' {frmPrijavaOdjava},
  OsnovNaPrijavuvanje in 'OsnovNaPrijavuvanje.pas' {frmOsnovNaPrijavuvawe},
  OsnovNaOdjavuvanje in 'OsnovNaOdjavuvanje.pas' {frmOsnovOdjavuvanje},
  JSOpstiPodatoci in 'JSOpstiPodatoci.pas' {frmJSOpstiPodatoci},
  ObrazovniInstitucii in 'ObrazovniInstitucii.pas' {frmObrazovniInstitucii},
  NasokaObrazovanie in 'NasokaObrazovanie.pas' {frmNasokaObrazovanie},
  ScanDocument in 'ScanDocument.pas' {frmS canDocument},
  PregledScanDocument in 'PregledScanDocument.pas' {frmPregledScanDokument},
  UsloviPoteskiOdNormalnite in 'UsloviPoteskiOdNormalnite.pas' {frmKoeficientUsloviPoteki},
  RabMestaRE in 'RabMestaRE.pas' {frmRMRE},
  MK in '..\Share2010\MK.pas' {frmMK},
  Proba in 'Proba.pas' {FormProba},
  StarosnaStruktura in 'StarosnaStruktura.pas' {frmStarosnaStruktura},
  CustomFields in '..\Share2010\CustomFields.pas' {frmCustomFields},
  RabotnoMestoUsloviRabota in 'RabotnoMestoUsloviRabota.pas' {frmRmReUsloviRabota},
  KategorijaRM in 'KategorijaRM.pas' {frmKategorijaRM},
  PregledSistematizacija in 'PregledSistematizacija.pas' {frmPregledSistematizacija},
  IzjavaProdolzenRabotenOdnos in 'IzjavaProdolzenRabotenOdnos.pas' {frmIzjavaProdolzenRabotenOdnos},
  PregledNaPenzioneri in 'PregledNaPenzioneri.pas' {frmPregledNaPenzioneri};

{$R *.res}

begin
  Application.Initialize;

  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='HRM';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.HelpFile := 'D:\Codis\HRM\HRM_Help.hlp';
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TdmSis, dmSis);
    Application.CreateForm(TdmOtsustvo, dmOtsustvo);
    Application.CreateForm(TdmReport, dmReport);
    Application.CreateForm(TdmUcinok, dmUcinok);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
  end;
  Application.Run;
end.
