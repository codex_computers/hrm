inherited frmTipOtsustvo: TfrmTipOtsustvo
  Left = 132
  Top = 233
  Caption = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
  ClientHeight = 673
  ClientWidth = 757
  ExplicitWidth = 773
  ExplicitHeight = 712
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 757
    Height = 282
    ExplicitWidth = 757
    ExplicitHeight = 282
    inherited cxGrid1: TcxGrid
      Width = 753
      Height = 278
      ExplicitWidth = 753
      ExplicitHeight = 278
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dmOtsustvo.dsTipOtsustvo
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 45
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 237
        end
        object cxGrid1DBTableView1DENOVI: TcxGridDBColumn
          DataBinding.FieldName = 'DENOVI'
          Width = 55
        end
        object cxGrid1DBTableView1DOGOVOR: TcxGridDBColumn
          DataBinding.FieldName = 'DOGOVOR'
          Visible = False
        end
        object cxGrid1DBTableView1DogovorNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'DogovorNaziv'
          Width = 129
        end
        object cxGrid1DBTableView1PLATENO: TcxGridDBColumn
          DataBinding.FieldName = 'PLATENO'
          Visible = False
        end
        object cxGrid1DBTableView1PlatenoNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'PlatenoNaziv'
          Width = 99
        end
        object cxGrid1DBTableView1BOJA: TcxGridDBColumn
          DataBinding.FieldName = 'BOJA'
          PropertiesClassName = 'TcxColorComboBoxProperties'
          Properties.CustomColors = <>
          Width = 104
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'TIPCASOPIS'
          Width = 119
        end
        object cxGrid1DBTableView1Column3: TcxGridDBColumn
          DataBinding.FieldName = 'SIFRA'
          Visible = False
          Width = 53
        end
        object cxGrid1DBTableView1Column4: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PLATA'
          Width = 111
        end
        object cxGrid1DBTableView1Column5: TcxGridDBColumn
          DataBinding.FieldName = 'SIF_NAD'
          Visible = False
          Width = 29
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'TIPNADOMESTOPIS'
          Width = 126
        end
        object cxGrid1DBTableView1Column6: TcxGridDBColumn
          DataBinding.FieldName = 'TERET'
          Visible = False
          Width = 29
        end
        object cxGrid1DBTableView1FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 416
    Width = 757
    Height = 234
    ExplicitTop = 416
    ExplicitWidth = 757
    ExplicitHeight = 234
    inherited Label1: TLabel
      Top = 6
      Visible = False
      ExplicitTop = 6
    end
    object Label2: TLabel [1]
      Left = 72
      Top = 25
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel [2]
      Left = 24
      Top = 128
      Width = 98
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1058#1080#1087' '#1085#1072' '#1095#1072#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 13
      Top = 155
      Width = 109
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1058#1080#1087' '#1085#1072' '#1085#1072#1076#1086#1084#1077#1089#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [4]
      Left = 72
      Top = 68
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1077#1085#1086#1074#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [5]
      Left = 72
      Top = 103
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1086#1112#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Top = 3
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      TabOrder = 4
      Visible = False
      ExplicitTop = 3
    end
    inherited OtkaziButton: TcxButton
      Left = 658
      Top = 194
      TabOrder = 12
      ExplicitLeft = 658
      ExplicitTop = 194
    end
    inherited ZapisiButton: TcxButton
      Left = 577
      Top = 194
      TabOrder = 11
      ExplicitLeft = 577
      ExplicitTop = 194
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 128
      Top = 22
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 441
    end
    object TIP_PLATA: TcxDBTextEdit
      Left = 184
      Top = 125
      BeepOnEnter = False
      DataBinding.DataField = 'TIP_PLATA'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = TIP_PLATAExit
      OnKeyDown = EnterKakoTab
      Width = 55
    end
    object SIFRACAS: TcxDBTextEdit
      Left = 128
      Top = 125
      BeepOnEnter = False
      DataBinding.DataField = 'SIFRA'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = SIFRACASExit
      OnKeyDown = EnterKakoTab
      Width = 55
    end
    object SIFRA_NADOMEST: TcxDBTextEdit
      Left = 128
      Top = 152
      BeepOnEnter = False
      DataBinding.DataField = 'SIF_NAD'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = SIFRA_NADOMESTExit
      OnKeyDown = EnterKakoTab
      Width = 55
    end
    object TERET: TcxDBTextEdit
      Left = 184
      Top = 152
      BeepOnEnter = False
      DataBinding.DataField = 'TERET'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = TERETExit
      OnKeyDown = EnterKakoTab
      Width = 55
    end
    object tipNadomest: TcxLookupComboBox
      Left = 240
      Top = 152
      Anchors = [akLeft, akTop, akRight, akBottom]
      ParentShowHint = False
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'SIF_NAD;TERET'
      Properties.ListColumns = <
        item
          FieldName = 'OPIS'
        end
        item
          FieldName = 'SIF_NAD'
        end
        item
          FieldName = 'TERET'
        end>
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dsPlt_tip_nadomest
      ShowHint = True
      TabOrder = 10
      OnClick = tipNadomestClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = tipNadomestExit
      OnKeyDown = EnterKakoTab
      Width = 329
    end
    object tipCas: TcxLookupComboBox
      Left = 240
      Top = 125
      Anchors = [akLeft, akTop, akRight, akBottom]
      ParentShowHint = False
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'SIFRA; TIP_PLATA'
      Properties.ListColumns = <
        item
          FieldName = 'OPIS'
        end
        item
          FieldName = 'SIFRA'
        end
        item
          FieldName = 'TIP_PLATA'
        end>
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dsPlt_tip_cas
      ShowHint = True
      TabOrder = 7
      OnClick = tipCasClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = tipCasExit
      OnKeyDown = EnterKakoTab
      Width = 329
    end
    object denovi: TcxDBTextEdit
      Left = 128
      Top = 65
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1082#1086#1080' '#1089#1083#1077#1076#1091#1074#1072#1072#1090' '#1079#1072' '#1086#1076#1088#1077#1076#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
      BeepOnEnter = False
      DataBinding.DataField = 'DENOVI'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 81
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 215
      Top = 49
      Hint = #1055#1083#1072#1090#1077#1085#1086'/'#1053#1077#1087#1083#1072#1090#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
      TabStop = False
      Caption = #1055#1083#1072#1090#1077#1085#1086
      DataBinding.DataField = 'PLATENO'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      ParentShowHint = False
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1044#1072
          Value = 1
        end
        item
          Caption = #1053#1077
          Value = 0
        end>
      ShowHint = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clActiveCaption
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 2
      Height = 44
      Width = 170
    end
    object cxDBRadioGroup2: TcxDBRadioGroup
      Left = 391
      Top = 49
      Hint = 
        #1044#1072#1083#1080' '#1073#1088#1086#1112#1086#1090' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1077' '#1086#1076#1088#1077#1076#1077#1085' '#1089#1087#1086#1088#1077#1076' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088' ('#1076#1072'/'#1085#1077 +
        ')'
      TabStop = False
      Caption = #1055#1086' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088
      DataBinding.DataField = 'DOGOVOR'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      ParentShowHint = False
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1044#1072
          Value = 1
        end
        item
          Caption = #1053#1077
          Value = 0
        end>
      ShowHint = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clActiveCaption
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 13
      Height = 44
      Width = 178
    end
    object Boja: TcxDBColorComboBox
      Left = 128
      Top = 99
      DataBinding.DataField = 'BOJA'
      DataBinding.DataSource = dmOtsustvo.dsTipOtsustvo
      Properties.CustomColors = <>
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 181
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 757
    ExplicitWidth = 757
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 650
    Width = 757
    ExplicitTop = 650
    ExplicitWidth = 757
  end
  object SplitterdPanel: TcxSplitter [4]
    Left = 0
    Top = 408
    Width = 757
    Height = 8
    HotZoneClassName = 'TcxMediaPlayer9Style'
    HotZone.SizePercent = 32
    AlignSplitter = salBottom
    AllowHotZoneDrag = False
    InvertDirection = True
    Control = dPanel
    ExplicitWidth = 8
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 224
  end
  inherited PopupMenu1: TPopupMenu
    Top = 256
  end
  inherited dxBarManager1: TdxBarManager
    Left = 424
    Top = 224
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.Margins.Right = 9000
      PrinterPage.PageFooter.LeftTitle.Strings = (
        '')
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]'
        '')
      PrinterPage.PageHeader.LeftTitle.Strings = (
        '')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      ReportDocument.CreationDate = 40659.552658460650000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object plt_tip_nadomest: TpFIBDataSet
    SelectSQL.Strings = (
      'select ptn.sif_nad,'
      '       ptn.teret,'
      '       case ptn.teret  when '#39'0'#39' then '#39#1092#1080#1088#1084#1072#39
      '                       when '#39'1'#39' then '#39#1076#1088#1091#1075' '#1086#1088#1075#1072#1085#39
      '       end "TeretNaziv",'
      '       ptn.den_dol_granica,'
      '       ptn.den_gor_granica,'
      '       ptn.proc_neto,'
      '       ptn.proc_neto_prid,'
      '       ptn.opis'
      'from plt_tip_nadomest ptn')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 456
    Top = 280
    object plt_tip_nadomestSIF_NAD: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIF_NAD'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object plt_tip_nadomestTERET: TFIBSmallIntField
      DisplayLabel = #1058#1077#1088#1077#1090' '#1085#1072
      FieldName = 'TERET'
    end
    object plt_tip_nadomestDEN_DOL_GRANICA: TFIBSmallIntField
      FieldName = 'DEN_DOL_GRANICA'
    end
    object plt_tip_nadomestDEN_GOR_GRANICA: TFIBSmallIntField
      FieldName = 'DEN_GOR_GRANICA'
    end
    object plt_tip_nadomestPROC_NETO: TFIBFloatField
      FieldName = 'PROC_NETO'
    end
    object plt_tip_nadomestPROC_NETO_PRID: TFIBFloatField
      FieldName = 'PROC_NETO_PRID'
    end
    object plt_tip_nadomestOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object plt_tip_nadomestTeretNaziv: TFIBStringField
      DisplayLabel = #1058#1077#1088#1077#1090' '#1085#1072
      FieldName = 'TeretNaziv'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPlt_tip_nadomest: TDataSource
    DataSet = plt_tip_nadomest
    Left = 536
    Top = 280
  end
  object plt_tip_cas: TpFIBDataSet
    SelectSQL.Strings = (
      'select ptc.sifra,'
      '       ptc.tip_plata,'
      '       ptc.firma,'
      '       ptc.opis,'
      '       ptc.sif_staz,'
      '       ptc.koeficient,'
      '       ptc.osn_cas'
      'from plt_tip_cas ptc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 456
    Top = 216
    object plt_tip_casSIFRA: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
    end
    object plt_tip_casTIP_PLATA: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1083#1072#1090#1072
      FieldName = 'TIP_PLATA'
    end
    object plt_tip_casFIRMA: TFIBIntegerField
      FieldName = 'FIRMA'
    end
    object plt_tip_casOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object plt_tip_casSIF_STAZ: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1078
      FieldName = 'SIF_STAZ'
      Size = 4
      Transliterate = False
      EmptyStrToNull = True
    end
    object plt_tip_casKOEFICIENT: TFIBFloatField
      DisplayLabel = #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090
      FieldName = 'KOEFICIENT'
    end
    object plt_tip_casOSN_CAS: TFIBSmallIntField
      DisplayLabel = #1054#1089#1085#1086#1074#1080#1094#1072' '#1085#1072' '#1095#1072#1089
      FieldName = 'OSN_CAS'
    end
  end
  object dsPlt_tip_cas: TDataSource
    DataSet = plt_tip_cas
    Left = 536
    Top = 216
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 152
  end
end
