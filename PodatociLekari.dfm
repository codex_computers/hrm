﻿inherited frmPodatociLekari: TfrmPodatociLekari
  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1083#1077#1082#1072#1088#1080
  ClientHeight = 606
  ClientWidth = 722
  ExplicitWidth = 738
  ExplicitHeight = 644
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 722
    Height = 234
    ExplicitWidth = 722
    ExplicitHeight = 234
    inherited cxGrid1: TcxGrid
      Width = 718
      Height = 230
      ExplicitWidth = 718
      ExplicitHeight = 230
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPodatociLekari
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 100
        end
        object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 203
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1FAKSIMIL: TcxGridDBColumn
          DataBinding.FieldName = 'FAKSIMIL'
          Width = 100
        end
        object cxGrid1DBTableView1BROJ_LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_LICENCA'
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 360
    Width = 722
    Height = 223
    ExplicitTop = 360
    ExplicitWidth = 722
    ExplicitHeight = 223
    inherited Label1: TLabel
      Left = 419
      Top = 110
      Visible = False
      ExplicitLeft = 419
      ExplicitTop = 110
    end
    object Label5: TLabel [1]
      Left = 61
      Top = 51
      Width = 77
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1060#1072#1082#1089#1080#1084#1080#1083' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 45
      Top = 17
      Width = 93
      Height = 33
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 475
      Top = 107
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPodatociLekari
      TabOrder = 4
      Visible = False
      ExplicitLeft = 475
      ExplicitTop = 107
    end
    inherited OtkaziButton: TcxButton
      Left = 631
      Top = 183
      TabOrder = 6
      ExplicitLeft = 631
      ExplicitTop = 183
    end
    inherited ZapisiButton: TcxButton
      Left = 550
      Top = 183
      TabOrder = 5
      ExplicitLeft = 550
      ExplicitTop = 183
    end
    object cxGroupBox1: TcxGroupBox
      Left = 31
      Top = 75
      Hint = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1083#1080#1094#1077#1085#1094#1072
      Caption = #1051#1080#1094#1077#1085#1094#1072
      TabOrder = 3
      Height = 118
      Width = 268
      object Label2: TLabel
        Left = 49
        Top = 30
        Width = 58
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112'  :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 30
        Top = 57
        Width = 77
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1072#1078#1085#1086#1089#1090' '#1086#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 30
        Top = 84
        Width = 77
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1072#1078#1085#1086#1089#1090' '#1076#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Број: TcxDBTextEdit
        Left = 113
        Top = 27
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_LICENCA'
        DataBinding.DataSource = dm.dsPodatociLekari
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DatumDo: TcxDBDateEdit
        Left = 113
        Top = 81
        Hint = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072#1090#1072
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsPodatociLekari
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DatumOd: TcxDBDateEdit
        Left = 113
        Top = 54
        Hint = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072#1090#1072
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsPodatociLekari
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object Faksimil: TcxDBTextEdit
      Left = 144
      Top = 48
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1089#1080#1084#1080#1083
      BeepOnEnter = False
      DataBinding.DataField = 'FAKSIMIL'
      DataBinding.DataSource = dm.dsPodatociLekari
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Tag = 1
      Left = 144
      Top = 21
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsPodatociLekari
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 267
      Top = 21
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsPodatociLekari
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      ShowHint = True
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 349
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 722
    ExplicitWidth = 722
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 583
    Width = 722
    ExplicitTop = 583
    ExplicitWidth = 722
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Top = 232
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited ActionList1: TActionList
    Top = 200
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40612.659252002310000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
