unit LVKStranskiJazici;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.0.0.0                   }
{                                       }
{   12.04.2010                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox,
  cxRadioGroup, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint;

type
  TfrmStranskiJazik = class(TfrmMaster)
    cxGrid1DBTableView1ID_LVK: TcxGridDBColumn;
    cxGrid1DBTableView1RAZBIRANJE_SLUSANJE: TcxGridDBColumn;
    cxGrid1DBTableView1RAZBIRANJE_CITANJE: TcxGridDBColumn;
    cxGrid1DBTableView1GOVOR_INTERAKCIJA: TcxGridDBColumn;
    cxGrid1DBTableView1GOVOR_PRODUKCIJA: TcxGridDBColumn;
    cxGrid1DBTableView1PISUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    VID_STRANSKI_JAZIK_NAZIV: TcxDBLookupComboBox;
    VID_STRANSKI_JAZIK: TcxDBTextEdit;
    Label11: TLabel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxDBRadioGroup2: TcxDBRadioGroup;
    cxDBRadioGroup3: TcxDBRadioGroup;
    cxDBRadioGroup4: TcxDBRadioGroup;
    cxDBRadioGroup5: TcxDBRadioGroup;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStranskiJazik: TfrmStranskiJazik;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  Utils, StranskiJazici;

{$R *.dfm}

procedure TfrmStranskiJazik.aNovExecute(Sender: TObject);
begin
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dmSis.tblLVKStranskiJazikID_LVK.Value:=dmSis.tblLVKID.Value;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');


end;

procedure TfrmStranskiJazik.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
      VK_INSERT:begin
         if (sender = VID_STRANSKI_JAZIK_NAZIV) or (Sender = VID_STRANSKI_JAZIK)  then
          begin
            frmStarnskiJazik:=TfrmStarnskiJazik.Create(Application);
            frmStarnskiJazik.Tag:=1;
            frmStarnskiJazik.ShowModal;
            frmStarnskiJazik.Free;
            dmSis.tblLVKStranskiJazikVID_STRANSKI_JAZIK.Value:=dmSis.tblStranskiJaziciID.Value;
          end;
      end;
  end;
end;

procedure TfrmStranskiJazik.FormCreate(Sender: TObject);
begin
  inherited;
  dmSis.tblLVKStranskiJazik.Close;
  dmSis.tblLVKStranskiJazik.ParamByName('lvkID').Value:=dmSis.tblLVKID.Value;
  dmSis.tblLVKStranskiJazik.Open;
  dmSis.tblStranskiJazici.Open;
end;

end.
