object frmViewVraboteni: TfrmViewVraboteni
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080
  ClientHeight = 553
  ClientWidth = 733
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 733
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 530
    Width = 733
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 733
    Height = 404
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 404
    ClientRectRight = 733
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 733
        Height = 380
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsVraboteniPregled
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGrid1DBTableView1MB: TcxGridDBColumn
            DataBinding.FieldName = 'MB'
            Width = 100
          end
          object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIVVRABOTEN'
            Visible = False
            GroupIndex = 0
            Width = 100
          end
          object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
            DataBinding.FieldName = 'PREZIME'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
            DataBinding.FieldName = 'TATKOVO_IME'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1MOMINSKO_PREZIME: TcxGridDBColumn
            DataBinding.FieldName = 'MOMINSKO_PREZIME'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1IME: TcxGridDBColumn
            DataBinding.FieldName = 'IME'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'RABOTNO_MESTO'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1RE: TcxGridDBColumn
            DataBinding.FieldName = 'RE'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'RABOTNAEDINICANAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1GRUPARM: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPARM'
            Width = 100
          end
          object cxGrid1DBTableView1GRUPAOZNAKA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPAOZNAKA'
            Width = 100
          end
          object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'RABOTNOMESTONAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1DOGOVOROD: TcxGridDBColumn
            DataBinding.FieldName = 'DOGOVOROD'
            Width = 100
          end
          object cxGrid1DBTableView1DOGOVORDO: TcxGridDBColumn
            DataBinding.FieldName = 'DOGOVORDO'
            Width = 100
          end
          object cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn
            DataBinding.FieldName = 'VID_VRABOTUVANJE'
            Width = 100
          end
          object cxGrid1DBTableView1RABOTNO_VREME: TcxGridDBColumn
            DataBinding.FieldName = 'RABOTNO_VREME'
            Width = 100
          end
          object cxGrid1DBTableView1PLATA: TcxGridDBColumn
            DataBinding.FieldName = 'PLATA'
            Width = 100
          end
          object cxGrid1DBTableView1STAZ_DEN: TcxGridDBColumn
            DataBinding.FieldName = 'STAZ_DEN'
            Width = 100
          end
          object cxGrid1DBTableView1STAZ_MESEC: TcxGridDBColumn
            DataBinding.FieldName = 'STAZ_MESEC'
            Width = 100
          end
          object cxGrid1DBTableView1STAZ_GODINA: TcxGridDBColumn
            DataBinding.FieldName = 'STAZ_GODINA'
            Width = 100
          end
          object cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn
            DataBinding.FieldName = 'RAKOVODENJE'
            Width = 100
          end
          object cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn
            DataBinding.FieldName = 'DENOVI_ODMOR'
            Width = 100
          end
          object cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn
            DataBinding.FieldName = 'DEN_USLOVI_RABOTA'
            Width = 100
          end
          object cxGrid1DBTableView1SLIKA: TcxGridDBColumn
            DataBinding.FieldName = 'SLIKA'
            Width = 100
          end
          object cxGrid1DBTableView1DATUM_RADJANJE: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM_RADJANJE'
            Width = 100
          end
          object cxGrid1DBTableView1POL: TcxGridDBColumn
            DataBinding.FieldName = 'POL'
            Width = 100
          end
          object cxGrid1DBTableView1BRACNA_SOSTOJBA: TcxGridDBColumn
            DataBinding.FieldName = 'BRACNA_SOSTOJBA'
            Width = 100
          end
          object cxGrid1DBTableView1ZDR_SOSTOJBA: TcxGridDBColumn
            DataBinding.FieldName = 'ZDR_SOSTOJBA'
            Width = 100
          end
          object cxGrid1DBTableView1TELESNO_OSTETUVANJE: TcxGridDBColumn
            DataBinding.FieldName = 'TELESNO_OSTETUVANJE'
            Width = 100
          end
          object cxGrid1DBTableView1SSTRUCNAPNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'SSTRUCNAPNAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1STAZMOMENTALEN: TcxGridDBColumn
            DataBinding.FieldName = 'STAZMOMENTALEN'
            Width = 100
          end
          object cxGrid1DBTableView1SZO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'SZO_NAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1SZO_OPIS: TcxGridDBColumn
            DataBinding.FieldName = 'SZO_OPIS'
            Width = 100
          end
          object cxGrid1DBTableView1SZODNENOVIGO: TcxGridDBColumn
            DataBinding.FieldName = 'SZODNENOVIGO'
            Width = 100
          end
          object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Width = 100
          end
          object cxGrid1DBTableView1BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Width = 100
          end
          object cxGrid1DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Width = 100
          end
          object cxGrid1DBTableView1MESTONAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MESTONAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1DRZAVA: TcxGridDBColumn
            DataBinding.FieldName = 'DRZAVA'
            Width = 100
          end
          object cxGrid1DBTableView1DRZAVANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'DRZAVANAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1DRZAVJANSTVONAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'DRZAVJANSTVONAZIV'
            Width = 100
          end
          object cxGrid1DBTableView1TEL: TcxGridDBColumn
            DataBinding.FieldName = 'TEL'
            Width = 100
          end
          object cxGrid1DBTableView1MOBILEN: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN'
            Width = 100
          end
          object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'EMAIL'
            Width = 100
          end
          object cxGrid1DBTableView1ITNO_SRODSTVO: TcxGridDBColumn
            DataBinding.FieldName = 'ITNO_SRODSTVO'
            Width = 100
          end
          object cxGrid1DBTableView1ITNO_IME_PREZIME: TcxGridDBColumn
            DataBinding.FieldName = 'ITNO_IME_PREZIME'
            Width = 100
          end
          object cxGrid1DBTableView1ITNO_ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ITNO_ADRESA'
            Width = 100
          end
          object cxGrid1DBTableView1ITNO_TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'ITNO_TELEFON'
            Width = 100
          end
          object cxGrid1DBTableView1LICNAKARTA: TcxGridDBColumn
            DataBinding.FieldName = 'LICNAKARTA'
            Width = 100
          end
          object cxGrid1DBTableView1MAJCIN_JAZIK: TcxGridDBColumn
            DataBinding.FieldName = 'MAJCIN_JAZIK'
            Width = 100
          end
          object cxGrid1DBTableView1DRUSTVENI_V: TcxGridDBColumn
            DataBinding.FieldName = 'DRUSTVENI_V'
            Width = 100
          end
          object cxGrid1DBTableView1ORGANIZACISKI_V: TcxGridDBColumn
            DataBinding.FieldName = 'ORGANIZACISKI_V'
            Width = 100
          end
          object cxGrid1DBTableView1TEHNICKI_V: TcxGridDBColumn
            DataBinding.FieldName = 'TEHNICKI_V'
            Width = 100
          end
          object cxGrid1DBTableView1KOMPJUTERSKI_V: TcxGridDBColumn
            DataBinding.FieldName = 'KOMPJUTERSKI_V'
            Width = 100
          end
          object cxGrid1DBTableView1UMETNICKI_V: TcxGridDBColumn
            DataBinding.FieldName = 'UMETNICKI_V'
            Width = 100
          end
          object cxGrid1DBTableView1DRUGI_V: TcxGridDBColumn
            DataBinding.FieldName = 'DRUGI_V'
            Width = 100
          end
          object cxGrid1DBTableView1VOZACKA_DOZVOLA: TcxGridDBColumn
            DataBinding.FieldName = 'VOZACKA_DOZVOLA'
            Width = 100
          end
          object cxGrid1DBTableView1KATEGORIJA_VD: TcxGridDBColumn
            DataBinding.FieldName = 'KATEGORIJA_VD'
            Width = 100
          end
          object cxGrid1DBTableView1DOPOLNITELNI_INFORMACII: TcxGridDBColumn
            DataBinding.FieldName = 'DOPOLNITELNI_INFORMACII'
            Width = 100
          end
          object cxGrid1DBTableView1PRILOZI: TcxGridDBColumn
            DataBinding.FieldName = 'PRILOZI'
            Width = 100
          end
          object cxGrid1DBTableView1OODATUMOD: TcxGridDBColumn
            DataBinding.FieldName = 'OODATUMOD'
            Width = 100
          end
          object cxGrid1DBTableView1OODATUMDO: TcxGridDBColumn
            DataBinding.FieldName = 'OODATUMDO'
            Width = 100
          end
          object cxGrid1DBTableView1OOKVALIFIKACIJA: TcxGridDBColumn
            DataBinding.FieldName = 'OOKVALIFIKACIJA'
            Width = 100
          end
          object cxGrid1DBTableView1OOZNAENJE: TcxGridDBColumn
            DataBinding.FieldName = 'OOZNAENJE'
            Width = 100
          end
          object cxGrid1DBTableView1OOIMEORGANIZACIJA: TcxGridDBColumn
            DataBinding.FieldName = 'OOIMEORGANIZACIJA'
          end
          object cxGrid1DBTableView1OONIVO: TcxGridDBColumn
            DataBinding.FieldName = 'OONIVO'
            Width = 100
          end
          object cxGrid1DBTableView1RIDATUMOD: TcxGridDBColumn
            DataBinding.FieldName = 'RIDATUMOD'
            Width = 100
          end
          object cxGrid1DBTableView1RIDATUMDO: TcxGridDBColumn
            DataBinding.FieldName = 'RIDATUMDO'
            Width = 100
          end
          object cxGrid1DBTableView1RIRABOTODAVAC: TcxGridDBColumn
            DataBinding.FieldName = 'RIRABOTODAVAC'
            Width = 100
          end
          object cxGrid1DBTableView1RIVIDDEJNOST: TcxGridDBColumn
            DataBinding.FieldName = 'RIVIDDEJNOST'
            Width = 100
          end
          object cxGrid1DBTableView1RIRABOTNOMESTO: TcxGridDBColumn
            DataBinding.FieldName = 'RIRABOTNOMESTO'
            Width = 100
          end
          object cxGrid1DBTableView1RIZADACI: TcxGridDBColumn
            DataBinding.FieldName = 'RIZADACI'
            Width = 100
          end
          object cxGrid1DBTableView1NACIONALNOST_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NACIONALNOST_NAZIV'
            Width = 104
          end
          object cxGrid1DBTableView1VEROISPOVED_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'VEROISPOVED_NAZIV'
            Width = 104
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 256
    Top = 272
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 296
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 416
    Top = 272
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 207
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 160
    Top = 312
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44454.376749155090000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 544
    Top = 48
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 456
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
