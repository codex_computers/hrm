object dmOtsustvo: TdmOtsustvo
  OldCreateOrder = False
  Height = 880
  Width = 886
  object dsNacionalnost: TDataSource
    DataSet = tblNacionalnost
    Left = 128
    Top = 24
  end
  object tblNacionalnost: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_NACIONALNOST'
      'SET '
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_NACIONALNOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_NACIONALNOST('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrn.id,'
      '       hrn.naziv,'
      '       hrn.ts_ins,'
      '       hrn.ts_upd,'
      '       hrn.usr_ins,'
      '       hrn.usr_upd'
      'from hr_nacionalnost hrn'
      ''
      ' WHERE '
      '        HRN.ID = :OLD_ID'
      '    '
      'order by hrn.naziv')
    SelectSQL.Strings = (
      'select hrn.id,'
      '       hrn.naziv,'
      '       hrn.ts_ins,'
      '       hrn.ts_upd,'
      '       hrn.usr_ins,'
      '       hrn.usr_upd'
      'from hr_nacionalnost hrn'
      'order by hrn.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_NACIONALNOST'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_NACIONALNOST_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 24
    object tblNacionalnostID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblNacionalnostNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNacionalnostTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblNacionalnostTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblNacionalnostUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNacionalnostUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblVeroispoved: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_VEROISPOVED'
      'SET '
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_VEROISPOVED'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_VEROISPOVED('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrv.id,'
      '       hrv.naziv,'
      '       hrv.ts_ins,'
      '       hrv.ts_upd,'
      '       hrv.usr_ins,'
      '       hrv.usr_upd'
      'from hr_veroispoved hrv'
      ''
      ' WHERE '
      '        HRV.ID = :OLD_ID'
      ' order by hrv.naziv   ')
    SelectSQL.Strings = (
      'select hrv.id,'
      '       hrv.naziv,'
      '       hrv.ts_ins,'
      '       hrv.ts_upd,'
      '       hrv.usr_ins,'
      '       hrv.usr_upd'
      'from hr_veroispoved hrv'
      'order by hrv.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_VEROISPOVED'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_VEROISPOVED_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 72
    object tblVeroispovedID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblVeroispovedNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVeroispovedTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblVeroispovedTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblVeroispovedUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVeroispovedUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsVeroispoved: TDataSource
    DataSet = tblVeroispoved
    Left = 128
    Top = 72
  end
  object tblPraznici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_PRAZNICI'
      'SET '
      '    DATUM = :DATUM,'
      '    NAZIV = :NAZIV,'
      '    VEROISPOVED = :VEROISPOVED,'
      '    NACIONALNOST = :NACIONALNOST,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_PRAZNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_PRAZNICI('
      '    ID,'
      '    DATUM,'
      '    NAZIV,'
      '    VEROISPOVED,'
      '    NACIONALNOST,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATUM,'
      '    :NAZIV,'
      '    :VEROISPOVED,'
      '    :NACIONALNOST,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrp.id,'
      '       hrp.datum,'
      '       hrp.naziv,'
      '       hrp.veroispoved,'
      '       hrv.naziv as veroispovedNaziv,'
      '       hrp.nacionalnost,'
      '       hrn.naziv as nacionalnostNaziv,'
      '       hrp.ts_ins,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd'
      'from hr_praznici hrp'
      'left outer join hr_veroispoved hrv  on hrv.id = hrp.veroispoved'
      'left outer join hr_nacionalnost hrn on hrn.id = hrp.nacionalnost'
      'where(  extractyear(hrp.datum) = :godina'
      '     ) and (     HRP.ID = :OLD_ID'
      '     )'
      '    '
      'order by hrp.datum')
    SelectSQL.Strings = (
      'select hrp.id,'
      '       hrp.datum,'
      '       hrp.naziv,'
      '       hrp.veroispoved,'
      '       hrv.naziv as veroispovedNaziv,'
      '       hrp.nacionalnost,'
      '       hrn.naziv as nacionalnostNaziv,'
      '       hrp.ts_ins,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd'
      'from hr_praznici hrp'
      'left outer join hr_veroispoved hrv  on hrv.id = hrp.veroispoved'
      'left outer join hr_nacionalnost hrn on hrn.id = hrp.nacionalnost'
      'where extractyear(hrp.datum) = :godina'
      'order by hrp.datum')
    AutoUpdateOptions.UpdateTableName = 'HR_PRAZNICI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_PRAZNICI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 128
    object tblPrazniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPrazniciDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblPrazniciNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrazniciVEROISPOVED: TFIBIntegerField
      DisplayLabel = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076' ('#1064#1080#1092#1088#1072')'
      FieldName = 'VEROISPOVED'
    end
    object tblPrazniciVEROISPOVEDNAZIV: TFIBStringField
      DisplayLabel = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076
      FieldName = 'VEROISPOVEDNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrazniciNACIONALNOST: TFIBIntegerField
      DisplayLabel = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090' ('#1064#1080#1092#1088#1072')'
      FieldName = 'NACIONALNOST'
    end
    object tblPrazniciNACIONALNOSTNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090
      FieldName = 'NACIONALNOSTNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrazniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblPrazniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblPrazniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPrazniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPraznici: TDataSource
    DataSet = tblPraznici
    Left = 128
    Top = 128
  end
  object tblTipOtsustvo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_TIP_OTSUSTVO'
      'SET '
      '    NAZIV = :NAZIV,'
      '    SIFRA = :SIFRA,'
      '    TIP_PLATA = :TIP_PLATA,'
      '    FIRMA = :FIRMA,'
      '    SIF_NAD = :SIF_NAD,'
      '    TERET = :TERET,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DENOVI = :DENOVI,'
      '    PLATENO = :PLATENO,'
      '    DOGOVOR = :DOGOVOR,'
      '    BOJA = :BOJA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_TIP_OTSUSTVO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_TIP_OTSUSTVO('
      '    ID,'
      '    NAZIV,'
      '    SIFRA,'
      '    TIP_PLATA,'
      '    FIRMA,'
      '    SIF_NAD,'
      '    TERET,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    DENOVI,'
      '    PLATENO,'
      '    DOGOVOR,'
      '    BOJA'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :SIFRA,'
      '    :TIP_PLATA,'
      '    :FIRMA,'
      '    :SIF_NAD,'
      '    :TERET,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :DENOVI,'
      '    :PLATENO,'
      '    :DOGOVOR,'
      '    :BOJA'
      ')')
    RefreshSQL.Strings = (
      'select hrt.id,'
      '       hrt.naziv,'
      
        '       hrt.sifra, hrt.tip_plata, hrt.firma, hrt.sif_nad, hrt.ter' +
        'et,'
      '       ptn.opis as tipNadomestOpis,'
      '       ptc.opis as tipCasOpis,'
      '       hrt.ts_ins,'
      '       hrt.ts_upd,'
      '       hrt.usr_ins,'
      '       hrt.usr_upd,'
      '       hrt.denovi,'
      '       hrt.plateno,'
      '       case when hrt.plateno = 1 then '#39#1055#1083#1072#1090#1077#1085#1086#39
      '            when hrt.plateno = 0 then '#39#1053#1077#1087#1083#1072#1090#1077#1085#1086#39
      '       end "PlatenoNaziv",'
      '       hrt.dogovor,'
      '       case when hrt.dogovor = 1 then '#39#1044#1072#39
      '            when hrt.dogovor = 0 then '#39#1053#1077#39
      '       end "DogovorNaziv",'
      '       hrt.boja'
      ''
      'from hr_tip_otsustvo hrt'
      
        'left outer join plt_tip_nadomest ptn on ptn.sif_nad = hrt.sif_na' +
        'd and ptn.teret = hrt.teret'
      
        'left outer join plt_tip_cas ptc on ptc.sifra = hrt.sifra and ptc' +
        '.tip_plata = hrt.tip_plata and ptc.firma = hrt.firma'
      'where(  hrt.firma = :firma'
      '     ) and (     HRT.ID = :OLD_ID'
      '     )'
      'order by hrt.dogovor, hrt.plateno, hrt.naziv    '
      '')
    SelectSQL.Strings = (
      'select hrt.id,'
      '       hrt.naziv,'
      
        '       hrt.sifra, hrt.tip_plata, hrt.firma, hrt.sif_nad, hrt.ter' +
        'et,'
      '       ptn.opis as tipNadomestOpis,'
      '       ptc.opis as tipCasOpis,'
      '       hrt.ts_ins,'
      '       hrt.ts_upd,'
      '       hrt.usr_ins,'
      '       hrt.usr_upd,'
      '       hrt.denovi,'
      '       hrt.plateno,'
      '       case when hrt.plateno = 1 then '#39#1055#1083#1072#1090#1077#1085#1086#39
      '            when hrt.plateno = 0 then '#39#1053#1077#1087#1083#1072#1090#1077#1085#1086#39
      '       end "PlatenoNaziv",'
      '       hrt.dogovor,'
      '       case when hrt.dogovor = 1 then '#39#1044#1072#39
      '            when hrt.dogovor = 0 then '#39#1053#1077#39
      '       end "DogovorNaziv",'
      '       hrt.boja'
      ''
      'from hr_tip_otsustvo hrt'
      
        'left outer join plt_tip_nadomest ptn on ptn.sif_nad = hrt.sif_na' +
        'd and ptn.teret = hrt.teret'
      
        'left outer join plt_tip_cas ptc on ptc.sifra = hrt.sifra and ptc' +
        '.tip_plata = hrt.tip_plata and ptc.firma = hrt.firma'
      'where hrt.firma = :firma'
      ''
      'order by hrt.dogovor, hrt.plateno, hrt.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_TIP_OTSUSTVO'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_TIP_OTSUSTVO_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 184
    object tblTipOtsustvoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTipOtsustvoNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblTipOtsustvoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblTipOtsustvoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoTIPNADOMESTOPIS: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1085#1072#1076#1086#1084#1077#1089#1090
      FieldName = 'TIPNADOMESTOPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoTIPCASOPIS: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1095#1072#1089
      FieldName = 'TIPCASOPIS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoSIFRA: TFIBSmallIntField
      DisplayLabel = 'T'#1080#1087' '#1085#1072' '#1087#1083#1072#1090#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'SIFRA'
    end
    object tblTipOtsustvoTIP_PLATA: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1083#1072#1090#1072
      FieldName = 'TIP_PLATA'
    end
    object tblTipOtsustvoFIRMA: TFIBIntegerField
      DisplayLabel = #1060#1080#1088#1084#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'FIRMA'
    end
    object tblTipOtsustvoSIF_NAD: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1085#1072#1076#1086#1084#1077#1089#1090' ('#1064#1080#1092#1088#1072')'
      FieldName = 'SIF_NAD'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoTERET: TFIBSmallIntField
      DisplayLabel = #1058#1077#1088#1077#1090
      FieldName = 'TERET'
    end
    object tblTipOtsustvoDENOVI: TFIBSmallIntField
      DisplayLabel = #1044#1077#1085#1086#1074#1080
      FieldName = 'DENOVI'
    end
    object tblTipOtsustvoPLATENO: TFIBSmallIntField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086'/'#1053#1077#1087#1083#1072#1090#1077#1085#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PLATENO'
    end
    object tblTipOtsustvoPlatenoNaziv: TFIBStringField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086'/'#1053#1077#1087#1083#1072#1090#1077#1085#1086
      FieldName = 'PlatenoNaziv'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoDOGOVOR: TFIBSmallIntField
      DisplayLabel = #1055#1086' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'DOGOVOR'
    end
    object tblTipOtsustvoDogovorNaziv: TFIBStringField
      DisplayLabel = #1055#1086' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088' ('#1044#1072'/'#1053#1077')'
      FieldName = 'DogovorNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipOtsustvoBOJA: TFIBIntegerField
      DisplayLabel = #1041#1086#1112#1072
      FieldName = 'BOJA'
    end
  end
  object dsTipOtsustvo: TDataSource
    DataSet = tblTipOtsustvo
    Left = 128
    Top = 184
  end
  object tblOtsustva: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_OTSUSTVA'
      'SET '
      '    TIP_ZAPIS = :TIP_ZAPIS,'
      '    MB = :MB,'
      '    PRICINA = :PRICINA,'
      '    PRICINA_OPIS = :PRICINA_OPIS,'
      '    OD_VREME = :OD_VREME,'
      '    DO_VREME = :DO_VREME,'
      '    DO_VREME_L = :DO_VREME_L,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    OPTIONS = :OPTIONS,'
      '    EVENT_TYPE = :EVENT_TYPE,'
      '    TASK_LINKS_FIELD = :TASK_LINKS_FIELD,'
      '    PLATENO = :PLATENO,'
      '    FLAG = :FLAG'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_OTSUSTVA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_OTSUSTVA('
      '    ID,'
      '    TIP_ZAPIS,'
      '    MB,'
      '    PRICINA,'
      '    PRICINA_OPIS,'
      '    OD_VREME,'
      '    DO_VREME,'
      '    DO_VREME_L,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    OPTIONS,'
      '    EVENT_TYPE,'
      '    TASK_LINKS_FIELD,'
      '    PLATENO,'
      '    FLAG'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_ZAPIS,'
      '    :MB,'
      '    :PRICINA,'
      '    :PRICINA_OPIS,'
      '    :OD_VREME,'
      '    :DO_VREME,'
      '    :DO_VREME_L,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :OPTIONS,'
      '    :EVENT_TYPE,'
      '    :TASK_LINKS_FIELD,'
      '    :PLATENO,'
      '    :FLAG'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.tip_zapis,'
      '       hro.mb,'
      '       hro.pricina,'
      '       hro.pricina_opis,'
      '       hrto.naziv as tipOtsustvoNaziv,'
      '       case hrto.dogovor when '#39'1'#39' then '#39#1044#1072#39
      '                         when '#39'0'#39' then '#39#1053#1077#39
      '       end "KolektivenDogDaNe",'
      '       hro.od_vreme,'
      '       hro.do_vreme,'
      '       hro.do_vreme_l,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrv.prezime as vrabotenPrezime,'
      '       hrv.tatkovo_ime as vrabotenTatkovoIme,'
      '       hrv.ime as vrabotenIme,'
      
        '       hrv.naziv_vraboten || '#39' - '#39' || coalesce(hrto.naziv, '#39' '#39') ' +
        'as opis,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      '       hro.plateno,'
      '       hro.flag,'
      '       case hro.plateno when '#39'1'#39' then '#39#1055#1083#1072#1090#1077#1085#1086#39
      '                        when '#39'0'#39' then '#39#1053#1077#1087#1083#1072#1090#1077#1085#1086#39
      '       end "PlatenoDaNe",'
      
        '       (select proc_hr_work_days.denovi from proc_hr_work_days(h' +
        'ro.od_vreme, hro.DO_VREME_L, hro.mb))as denovi,'
      '       hrto.boja'
      'from hr_otsustva hro'
      'inner join hr_tip_otsustvo hrto on hrto.id = hro.pricina'
      'inner join view_hr_vraboteni hrv on hrv.mb = hro.mb'
      'where(  hrv.id_re_firma = :firma'
      '      and  hro.tip_zapis = 1'
      '      and hro.mb like :MB'
      '      and hro.pricina like :pricina'
      '      and hrv.re like :re'
      
        '      and (current_date between hrv.datum_od and coalesce(hrv.da' +
        'tum_do, current_date))'
      
        '      and ((extractyear(hro.od_vreme) = :param_godina and extrac' +
        'tyear(hro.do_vreme_l) = :param_godina)'
      
        '           or (extractyear(hro.od_vreme) = :param_godina - 1 and' +
        ' extractyear(hro.do_vreme_l) = :param_godina)'
      
        '           or (extractyear(hro.od_vreme) = :param_godina and ext' +
        'ractyear(hro.do_vreme_l) = :param_godina +1))'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      'order by hro.od_vreme, hrv.naziv_vraboten_ti    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.tip_zapis,'
      '       hro.mb,'
      '       hro.pricina,'
      '       hro.pricina_opis,'
      '       hrto.naziv as tipOtsustvoNaziv,'
      '       case hrto.dogovor when '#39'1'#39' then '#39#1044#1072#39
      '                         when '#39'0'#39' then '#39#1053#1077#39
      '       end "KolektivenDogDaNe",'
      '       hro.od_vreme,'
      '       hro.do_vreme,'
      '       hro.do_vreme_l,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrv.prezime as vrabotenPrezime,'
      '       hrv.tatkovo_ime as vrabotenTatkovoIme,'
      '       hrv.ime as vrabotenIme,'
      
        '       hrv.naziv_vraboten || '#39' - '#39' || coalesce(hrto.naziv, '#39' '#39') ' +
        'as opis,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      '       hro.plateno,'
      '       hro.flag,'
      '       case hro.plateno when '#39'1'#39' then '#39#1055#1083#1072#1090#1077#1085#1086#39
      '                        when '#39'0'#39' then '#39#1053#1077#1087#1083#1072#1090#1077#1085#1086#39
      '       end "PlatenoDaNe",'
      
        '       (select proc_hr_work_days.denovi from proc_hr_work_days(h' +
        'ro.od_vreme, hro.do_vreme_l, hro.mb))as denovi,'
      '       hrto.boja'
      'from hr_otsustva hro'
      'inner join hr_tip_otsustvo hrto on hrto.id = hro.pricina'
      'inner join view_hr_vraboteni hrv on hrv.mb = hro.mb'
      'where hrv.id_re_firma = :firma'
      '      and  hro.tip_zapis = 1'
      '      and hro.mb like :MB'
      '      and hro.pricina like :pricina'
      '      and hrv.re like :re'
      
        '      and (current_date between hrv.datum_od and coalesce(hrv.da' +
        'tum_do, current_date))'
      
        '      and ((extractyear(hro.od_vreme) = :param_godina and extrac' +
        'tyear(hro.do_vreme_l) = :param_godina)'
      
        '           or (extractyear(hro.od_vreme) = :param_godina - 1 and' +
        ' extractyear(hro.do_vreme_l) = :param_godina)'
      
        '           or (extractyear(hro.od_vreme) = :param_godina and ext' +
        'ractyear(hro.do_vreme_l) = :param_godina +1))'
      'order by hro.od_vreme, hrv.naziv_vraboten_ti')
    AutoUpdateOptions.UpdateTableName = 'HR_OTSUSTVA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_OTSUSTVA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 80
    object tblOtsustvaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOtsustvaTIP_ZAPIS: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TIP_ZAPIS'
    end
    object tblOtsustvaPRICINA: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PRICINA'
    end
    object tblOtsustvaPRICINA_OPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'PRICINA_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaOD_VREME: TFIBDateTimeField
      DisplayLabel = #1054#1076
      FieldName = 'OD_VREME'
      DisplayFormat = 'dd.mm.yyyy '
    end
    object tblOtsustvaDO_VREME: TFIBDateTimeField
      DisplayLabel = #1044#1086
      FieldName = 'DO_VREME'
      DisplayFormat = 'dd.mm.yyyy  '
    end
    object tblOtsustvaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblOtsustvaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblOtsustvaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaVRABOTENPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'VRABOTENPREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaVRABOTENTATKOVOIME: TFIBStringField
      DisplayLabel = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077
      FieldName = 'VRABOTENTATKOVOIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaVRABOTENIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'VRABOTENIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1082#1072#1083#1077#1085#1076#1072#1088
      FieldName = 'OPIS'
      Size = 353
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaOPTIONS: TFIBIntegerField
      FieldName = 'OPTIONS'
    end
    object tblOtsustvaEVENT_TYPE: TFIBIntegerField
      FieldName = 'EVENT_TYPE'
    end
    object tblOtsustvaTASK_LINKS_FIELD: TFIBMemoField
      FieldName = 'TASK_LINKS_FIELD'
      BlobType = ftMemo
      Size = 8
    end
    object tblOtsustvaPLATENO: TFIBSmallIntField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086'/'#1053#1077#1087#1083#1072#1090#1077#1085#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PLATENO'
    end
    object tblOtsustvaPlatenoDaNe: TFIBStringField
      Alignment = taCenter
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086'/'#1053#1077#1087#1083#1072#1090#1077#1085#1086
      FieldName = 'PlatenoDaNe'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaDENOVI: TFIBIntegerField
      Alignment = taCenter
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080
      FieldName = 'DENOVI'
    end
    object tblOtsustvaFLAG: TFIBIntegerField
      FieldName = 'FLAG'
    end
    object tblOtsustvaDO_VREME_L: TFIBDateTimeField
      DisplayLabel = #1044#1086
      FieldName = 'DO_VREME_L'
      DisplayFormat = 'dd.mm.yyyy '
    end
    object tblOtsustvaNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaTIPOTSUSTVONAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
      FieldName = 'TIPOTSUSTVONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaKolektivenDogDaNe: TFIBStringField
      DisplayLabel = #1055#1086' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086#1088' ('#1044#1072'/'#1053#1077')'
      FieldName = 'KolektivenDogDaNe'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaBOJA: TFIBIntegerField
      DisplayLabel = #1041#1086#1112#1072
      FieldName = 'BOJA'
    end
  end
  object dsOtsustva: TDataSource
    DataSet = tblOtsustva
    Left = 360
    Top = 80
  end
  object tblPodsektori: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    mr.ID,'
      '    mr.NAZIV,'
      '    mr.TIP_PARTNER,'
      '    mr.PARTNER,'
      '    mr.KOREN,'
      '    mr.SPISOK,'
      '    mr.RE,'
      '    mr.POTEKLO,'
      '    mr.RAKOVODITEL,'
      '    mr.R,'
      '    mr.M,'
      '    mr.T'
      'from mat_re mr'
      'where'
      
        '       (((mr.poteklo||'#39'%'#39' like  '#39'%,'#39'||:poteklo ||'#39',%'#39')and (not m' +
        'r.poteklo like :poteklo||'#39','#39')) or'
      '      (mr.poteklo like :poteklo||'#39',%'#39'))'
      '-- MAT_RE.POTEKLO like :potek')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 464
    Top = 24
    object tblPodsektoriID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPodsektoriNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodsektoriKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblPodsektoriSPISOK: TFIBStringField
      FieldName = 'SPISOK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodsektoriRE: TFIBIntegerField
      FieldName = 'RE'
    end
    object tblPodsektoriPOTEKLO: TFIBStringField
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodsektoriR: TFIBSmallIntField
      FieldName = 'R'
    end
  end
  object dsPodsektori: TDataSource
    DataSet = tblPodsektori
    Left = 552
    Top = 24
  end
  object tblObuki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_OBUKI'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    PODGOTVENOST = :PODGOTVENOST,'
      '    CEL = :CEL,'
      '    SREDSTVA = :SREDSTVA,'
      '    PERIOD_OD = :PERIOD_OD,'
      '    PERIOD_DO = :PERIOD_DO,'
      '    MESTO = :MESTO,'
      '    ODGOVORNO_LICE = :ODGOVORNO_LICE,'
      '    ZABELESKA = :ZABELESKA,'
      '    STATUS = :STATUS,'
      '    OPTIONS = :OPTIONS,'
      '    EVENT_TYPE = :EVENT_TYPE,'
      '    TASK_LINKS_FIELD = :TASK_LINKS_FIELD,'
      '    DOKUMENT = :DOKUMENT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_OBUKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_OBUKI('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    PODGOTVENOST,'
      '    CEL,'
      '    SREDSTVA,'
      '    PERIOD_OD,'
      '    PERIOD_DO,'
      '    MESTO,'
      '    ODGOVORNO_LICE,'
      '    ZABELESKA,'
      '    STATUS,'
      '    OPTIONS,'
      '    EVENT_TYPE,'
      '    TASK_LINKS_FIELD,'
      '    DOKUMENT'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :PODGOTVENOST,'
      '    :CEL,'
      '    :SREDSTVA,'
      '    :PERIOD_OD,'
      '    :PERIOD_DO,'
      '    :MESTO,'
      '    :ODGOVORNO_LICE,'
      '    :ZABELESKA,'
      '    :STATUS,'
      '    :OPTIONS,'
      '    :EVENT_TYPE,'
      '    :TASK_LINKS_FIELD,'
      '    :DOKUMENT'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.naziv,'
      '       hro.opis,'
      '       hro.podgotvenost,'
      '       hro.cel,'
      '       hro.sredstva,'
      '       hro.period_od,'
      '       hro.period_do,'
      '       hro.mesto,'
      '       hro.odgovorno_lice,'
      '       hro.zabeleska,'
      '       hro.status,'
      '       ms.naziv as statusNaziv,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      '       ms.boja as status_boja,'
      '       hro.dokument'
      'from hr_obuki hro'
      'inner join mat_status ms on ms.id = hro.status'
      'where(  hro.status like :status'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      '    '
      'order by hro.period_od ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.naziv,'
      '       hro.opis,'
      '       hro.podgotvenost,'
      '       hro.cel,'
      '       hro.sredstva,'
      '       hro.period_od,'
      '       hro.period_do,'
      '       hro.mesto,'
      '       hro.odgovorno_lice,'
      '       hro.zabeleska,'
      '       hro.status,'
      '       ms.naziv as statusNaziv,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      '       ms.boja as status_boja,'
      '       hro.dokument'
      'from hr_obuki hro'
      'inner join mat_status ms on ms.id = hro.status'
      'where hro.status like :status'
      'order by hro.period_od ')
    AutoUpdateOptions.UpdateTableName = 'HR_OBUKI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_OBUKI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 472
    Top = 112
    object tblObukiID: TFIBSmallIntField
      FieldName = 'ID'
      DisplayFormat = #1064#1080#1092#1088#1072
    end
    object tblObukiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiPODGOTVENOST: TFIBStringField
      DisplayLabel = #1055#1086#1076#1075#1086#1090#1074#1077#1085#1086#1089#1090
      FieldName = 'PODGOTVENOST'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiCEL: TFIBStringField
      DisplayLabel = #1062#1077#1083
      FieldName = 'CEL'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiSREDSTVA: TFIBBCDField
      DisplayLabel = #1057#1088#1077#1076#1089#1090#1074#1072
      FieldName = 'SREDSTVA'
      Size = 2
    end
    object tblObukiPERIOD_OD: TFIBDateField
      DisplayLabel = #1054#1076
      FieldName = 'PERIOD_OD'
    end
    object tblObukiPERIOD_DO: TFIBDateField
      DisplayLabel = #1044#1086
      FieldName = 'PERIOD_DO'
    end
    object tblObukiMESTO: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086'/'#1040#1076#1088#1077#1089#1072
      FieldName = 'MESTO'
      Size = 120
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiODGOVORNO_LICE: TFIBStringField
      DisplayLabel = #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077
      FieldName = 'ODGOVORNO_LICE'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiZABELESKA: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'ZABELESKA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiSTATUSNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukiSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' ('#1064#1080#1092#1088#1072')'
      FieldName = 'STATUS'
    end
    object tblObukiOPTIONS: TFIBIntegerField
      FieldName = 'OPTIONS'
    end
    object tblObukiEVENT_TYPE: TFIBIntegerField
      FieldName = 'EVENT_TYPE'
    end
    object tblObukiTASK_LINKS_FIELD: TFIBMemoField
      FieldName = 'TASK_LINKS_FIELD'
      BlobType = ftMemo
      Size = 8
    end
    object tblObukiSTATUS_BOJA: TFIBIntegerField
      FieldName = 'STATUS_BOJA'
    end
    object tblObukiDOKUMENT: TFIBStringField
      DisplayLabel = #1055#1072#1090#1077#1082#1072' '#1076#1086' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'DOKUMENT'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsObuki: TDataSource
    DataSet = tblObuki
    Left = 560
    Top = 112
  end
  object tblObukaUcesnici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_OBUKA_UCESNICI'
      'SET '
      '    OBUKA_ID = :OBUKA_ID,'
      '    MB = :MB,'
      '    OCENKA = :OCENKA,'
      '    SERTIFIKATI = :SERTIFIKATI,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_OBUKA_UCESNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_OBUKA_UCESNICI('
      '    ID,'
      '    OBUKA_ID,'
      '    MB,'
      '    OCENKA,'
      '    SERTIFIKATI,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OBUKA_ID,'
      '    :MB,'
      '    :OCENKA,'
      '    :SERTIFIKATI,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.obuka_id,'
      '       hro.mb,'
      '       hro.ocenka,'
      '       hro.sertifikati,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti'
      '     '
      'from hr_obuka_ucesnici hro'
      'inner join hr_vraboten hrv on hrv.mb = hro.mb'
      'where(  hro.obuka_id like :obukaID'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.obuka_id,'
      '       hro.mb,'
      '       hro.ocenka,'
      '       hro.sertifikati,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti'
      '     '
      'from hr_obuka_ucesnici hro'
      'inner join hr_vraboten hrv on hrv.mb = hro.mb'
      'where hro.obuka_id like :obukaID'
      'order by hro.obuka_id')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 472
    Top = 184
    object tblObukaUcesniciID: TFIBSmallIntField
      FieldName = 'ID'
    end
    object tblObukaUcesniciOBUKA_ID: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1073#1091#1082#1072
      FieldName = 'OBUKA_ID'
    end
    object tblObukaUcesniciMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukaUcesniciOCENKA: TFIBStringField
      DisplayLabel = #1054#1094#1077#1085#1082#1072
      FieldName = 'OCENKA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukaUcesniciSERTIFIKATI: TFIBStringField
      DisplayLabel = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1080
      FieldName = 'SERTIFIKATI'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukaUcesniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077
      FieldName = 'TS_INS'
    end
    object tblObukaUcesniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblObukaUcesniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1076#1086#1076#1072#1083
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukaUcesniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1072#1078#1091#1088#1080#1088#1072#1083
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukaUcesniciNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblObukaUcesniciNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsObukaUcesnici: TDataSource
    DataSet = tblObukaUcesnici
    Left = 560
    Top = 184
  end
  object CountUcesnikObuka: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'execute procedure PROC_HR_COUNTUCESNIKOBUKA(?OBUKAID_IN, ?MB_IN)')
    Left = 656
    Top = 184
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblOtsustvaPregled: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_hr_otsustva_pregled.plateno_out,'
      '       proc_hr_otsustva_pregled.neplateno_out,'
      '       proc_hr_otsustva_pregled.count_1_out,'
      '       proc_hr_otsustva_pregled.count_0_out,'
      '       proc_hr_otsustva_pregled.mb_out,'
      '       proc_hr_otsustva_pregled.nazivvraboten_out,'
      '       proc_hr_otsustva_pregled.naziv_vraboten_ti_out,'
      '       proc_hr_otsustva_pregled.re_out,'
      '       proc_hr_otsustva_pregled.hrmrabid_out,'
      '       proc_hr_otsustva_pregled.rmre_out,'
      '       proc_hr_otsustva_pregled.renaziv_out,'
      '       proc_hr_otsustva_pregled.rabmestonaziv_out'
      'from proc_hr_otsustva_pregled(:odDatum, :doDatum, :firma)'
      'where proc_hr_otsustva_pregled.re_out like :Re'
      'order by proc_hr_otsustva_pregled.renaziv_out,'
      '         proc_hr_otsustva_pregled.rabmestonaziv_out,'
      '         proc_hr_otsustva_pregled.nazivvraboten_out'
      '         ')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 144
    object tblOtsustvaPregledPLATENO_OUT: TFIBIntegerField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'PLATENO_OUT'
    end
    object tblOtsustvaPregledNEPLATENO_OUT: TFIBIntegerField
      DisplayLabel = #1053#1077#1087#1083#1072#1090#1077#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'NEPLATENO_OUT'
    end
    object tblOtsustvaPregledCOUNT_1_OUT: TFIBIntegerField
      DisplayLabel = #1056#1077#1072#1083#1080#1079#1080#1088#1072#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'COUNT_1_OUT'
    end
    object tblOtsustvaPregledCOUNT_0_OUT: TFIBIntegerField
      DisplayLabel = #1055#1083#1072#1085#1080#1088#1072#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'COUNT_0_OUT'
    end
    object tblOtsustvaPregledMB_OUT: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'.'
      FieldName = 'MB_OUT'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPregledRENAZIV_OUT: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPregledRABMESTONAZIV_OUT: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABMESTONAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPregledNAZIVVRABOTEN_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIVVRABOTEN_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPregledNAZIV_VRABOTEN_TI_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustvaPregledRE_OUT: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RE_OUT'
    end
    object tblOtsustvaPregledHRMRABID_OUT: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'HRMRABID_OUT'
    end
    object tblOtsustvaPregledRMRE_OUT: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RMRE_OUT'
    end
  end
  object dsOtsustvaPregled: TDataSource
    DataSet = tblOtsustvaPregled
    Left = 360
    Top = 144
  end
  object WorkDays: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_WORK_DAYS (?OD_DATUM, ?DO_DATUM, ?MB)')
    StoredProcName = 'PROC_HR_WORK_DAYS'
    Left = 656
    Top = 416
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblIntBrUcesnici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_INT_BROJ_UCESNICI'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_INT_BROJ_UCESNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_INT_BROJ_UCESNICI('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hib.id,'
      '       hib.naziv,'
      '       hib.opis,'
      '       hib.ts_ins,'
      '       hib.ts_upd,'
      '       hib.usr_ins,'
      '       hib.usr_upd'
      'from hr_int_broj_ucesnici hib'
      ' WHERE '
      '        HIB.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hib.id,'
      '       hib.naziv,'
      '       hib.opis,'
      '       hib.ts_ins,'
      '       hib.ts_upd,'
      '       hib.usr_ins,'
      '       hib.usr_upd'
      'from hr_int_broj_ucesnici hib')
    AutoUpdateOptions.UpdateTableName = 'HR_INT_BROJ_UCESNICI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_INT_BROJ_UCESNICI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 416
    object tblIntBrUcesniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIntBrUcesniciNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntBrUcesniciOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntBrUcesniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077
      FieldName = 'TS_INS'
    end
    object tblIntBrUcesniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblIntBrUcesniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1112#1072' '#1076#1086#1076#1072#1083' '#1089#1090#1072#1074#1082#1072#1090#1072
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntBrUcesniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1112#1072' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1089#1090#1072#1074#1082#1072#1090#1072
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIntBrUcesnici: TDataSource
    DataSet = tblIntBrUcesnici
    Left = 120
    Top = 416
  end
  object tblIntOblik: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_INT_OBLIK'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_INT_OBLIK'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_INT_OBLIK('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.naziv,'
      '       hro.opis,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd'
      'from hr_int_oblik hro'
      ''
      ' WHERE '
      '        HRO.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.naziv,'
      '       hro.opis,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd'
      'from hr_int_oblik hro')
    AutoUpdateOptions.UpdateTableName = 'HR_INT_OBLIK'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_INT_OBLIK_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 472
    object tblIntOblikID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIntOblikNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntOblikOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntOblikTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077
      FieldName = 'TS_INS'
    end
    object tblIntOblikTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblIntOblikUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1112#1072' '#1076#1086#1076#1072#1083' '#1089#1090#1072#1074#1082#1072#1090#1072
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntOblikUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1112#1072' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1089#1090#1072#1074#1082#1072#1090#1072
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIntOblik: TDataSource
    DataSet = tblIntOblik
    Left = 112
    Top = 472
  end
  object tblIntSelekcija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_INT_SELEKCIJA'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_INT_SELEKCIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_INT_SELEKCIJA('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hrs.id,'
      '       hrs.naziv,'
      '       hrs.opis,'
      '       hrs.ts_ins,'
      '       hrs.ts_upd,'
      '       hrs.usr_ins,'
      '       hrs.usr_upd'
      'from hr_int_selekcija hrs'
      ' WHERE '
      '        HRS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hrs.id,'
      '       hrs.naziv,'
      '       hrs.opis,'
      '       hrs.ts_ins,'
      '       hrs.ts_upd,'
      '       hrs.usr_ins,'
      '       hrs.usr_upd'
      'from hr_int_selekcija hrs')
    AutoUpdateOptions.UpdateTableName = 'HR_INT_SELEKCIJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_INT_SELEKCIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 528
    object tblIntSelekcijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIntSelekcijaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntSelekcijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntSelekcijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077
      FieldName = 'TS_INS'
    end
    object tblIntSelekcijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblIntSelekcijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1112#1072' '#1076#1086#1076#1072#1083' '#1089#1090#1072#1074#1082#1072#1090#1072
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntSelekcijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1112#1072' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1089#1090#1072#1074#1082#1072#1090#1072
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIntSelekcija: TDataSource
    DataSet = tblIntSelekcija
    Left = 112
    Top = 528
  end
  object tblIntervju: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_INTERVJU'
      'SET '
      '    ID_MOLBA = :ID_MOLBA,'
      '    BROJ = :BROJ,'
      '    MB_INTERVJUIST = :MB_INTERVJUIST,'
      '    ID_OBLIK = :ID_OBLIK,'
      '    ID_BROJ_UCESNICI = :ID_BROJ_UCESNICI,'
      '    ID_SELEKCIJA = :ID_SELEKCIJA,'
      '    OPIS_POCETOK = :OPIS_POCETOK,'
      '    OPIS_SREDINA = :OPIS_SREDINA,'
      '    OPIS_KRAJ = :OPIS_KRAJ,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    DATUM = :DATUM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_INTERVJU'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_INTERVJU('
      '    ID,'
      '    ID_MOLBA,'
      '    BROJ,'
      '    MB_INTERVJUIST,'
      '    ID_OBLIK,'
      '    ID_BROJ_UCESNICI,'
      '    ID_SELEKCIJA,'
      '    OPIS_POCETOK,'
      '    OPIS_SREDINA,'
      '    OPIS_KRAJ,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    ID_RM_RE,'
      '    DATUM'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_MOLBA,'
      '    :BROJ,'
      '    :MB_INTERVJUIST,'
      '    :ID_OBLIK,'
      '    :ID_BROJ_UCESNICI,'
      '    :ID_SELEKCIJA,'
      '    :OPIS_POCETOK,'
      '    :OPIS_SREDINA,'
      '    :OPIS_KRAJ,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :ID_RM_RE,'
      '    :DATUM'
      ')')
    RefreshSQL.Strings = (
      'select hri.id,'
      '       hri.id_molba,'
      '       hrm.broj broj_molba,'
      '       hri.broj,'
      '       hrrm.id id_rm_re_molba,'
      '       hrmm.naziv naziv_rm_molba,'
      '       hri.mb_intervjuist,'
      '       hri.id_oblik,'
      '       hri.id_broj_ucesnici,'
      '       hri.id_selekcija,'
      '       hri.opis_pocetok,'
      '       hri.opis_sredina,'
      '       hri.opis_kraj,'
      '       hri.ts_ins,'
      '       hri.ts_upd,'
      '       hri.usr_ins,'
      '       hri.usr_upd,'
      '       hri.id_rm_re,'
      '       rm.naziv naziv_rm,'
      '       hrv.naziv_vraboten_ti as IntervjuistNaziv,'
      '       hrm.naziv_vraboten as MolbaNaziv,'
      '       hrb.naziv as brUcesniciNaziv,'
      '       hro.naziv as oblikNaziv,'
      '       hrs.naziv as selekcijaNaziv,'
      '       hri.datum'
      'from hr_intervju hri'
      'inner join hr_vraboten hrv on hrv.mb = hri.mb_intervjuist'
      'left outer join hr_evidencija_molba hrm on hrm.id = hri.id_molba'
      'left outer join hr_rm_re hrrm on hrrm.id=hrm.id_rm_re'
      'left outer join hr_rabotno_mesto hrmm on hrmm.id=hrrm.id_rm'
      
        'left outer join hr_int_broj_ucesnici hrb on hrb.id = hri.id_broj' +
        '_ucesnici'
      'left outer join hr_int_oblik hro on hro.id = hri.id_oblik'
      'left outer join hr_int_selekcija hrs on hrs.id = hri.id'
      'left outer join hr_rm_re hrr on hrr.id=hri.id_rm_re'
      'left outer join hr_rabotno_mesto rm on rm.id=hrr.id_rm'
      'left outer join mat_re mr on mr.id=hrr.id_re'
      'left outer join mat_re mrr on mrr.id=hrrm.id_re'
      
        'where(  (mrr.poteklo like :firma||'#39',%'#39') or (mr.poteklo like :fir' +
        'ma||'#39',%'#39')'
      '     ) and (     HRI.ID = :OLD_ID'
      '     )'
      '    '
      'order by rm.naziv, 21')
    SelectSQL.Strings = (
      'select hri.id,'
      '       hri.id_molba,'
      '       hrm.broj broj_molba,'
      '       hri.broj,'
      '       hrrm.id id_rm_re_molba,'
      '       hrmm.naziv naziv_rm_molba,'
      '       hri.mb_intervjuist,'
      '       hri.id_oblik,'
      '       hri.id_broj_ucesnici,'
      '       hri.id_selekcija,'
      '       hri.opis_pocetok,'
      '       hri.opis_sredina,'
      '       hri.opis_kraj,'
      '       hri.ts_ins,'
      '       hri.ts_upd,'
      '       hri.usr_ins,'
      '       hri.usr_upd,'
      '       hri.id_rm_re,'
      '       rm.naziv naziv_rm,'
      '       hrv.naziv_vraboten_ti as IntervjuistNaziv,'
      '       hrm.naziv_vraboten as MolbaNaziv,'
      '       hrb.naziv as brUcesniciNaziv,'
      '       hro.naziv as oblikNaziv,'
      '       hrs.naziv as selekcijaNaziv,'
      '       hri.datum'
      'from hr_intervju hri'
      'inner join hr_vraboten hrv on hrv.mb = hri.mb_intervjuist'
      'left outer join hr_evidencija_molba hrm on hrm.id = hri.id_molba'
      'left outer join hr_rm_re hrrm on hrrm.id=hrm.id_rm_re'
      'left outer join hr_rabotno_mesto hrmm on hrmm.id=hrrm.id_rm'
      
        'left outer join hr_int_broj_ucesnici hrb on hrb.id = hri.id_broj' +
        '_ucesnici'
      'left outer join hr_int_oblik hro on hro.id = hri.id_oblik'
      'left outer join hr_int_selekcija hrs on hrs.id = hri.id'
      'left outer join hr_rm_re hrr on hrr.id=hri.id_rm_re'
      'left outer join hr_rabotno_mesto rm on rm.id=hrr.id_rm'
      'left outer join mat_re mr on mr.id=hrr.id_re'
      'left outer join mat_re mrr on mrr.id=hrrm.id_re'
      
        'where (mrr.poteklo like :firma||'#39',%'#39') or (mr.poteklo like :firma' +
        '||'#39',%'#39')'
      'order by rm.naziv, 21')
    AutoUpdateOptions.UpdateTableName = 'HR_INTERVJU'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_INTERVJU_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 584
    object tblIntervjuID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIntervjuID_MOLBA: TFIBIntegerField
      DisplayLabel = #1052#1086#1083#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_MOLBA'
    end
    object tblIntervjuMB_INTERVJUIST: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1041#1088'. '#1085#1072' '#1048#1085#1090#1077#1088#1074#1112#1091#1080#1089#1090
      FieldName = 'MB_INTERVJUIST'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuID_OBLIK: TFIBIntegerField
      DisplayLabel = #1054#1073#1083#1080#1082' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_OBLIK'
    end
    object tblIntervjuID_BROJ_UCESNICI: TFIBIntegerField
      DisplayLabel = #1041#1088'.'#1091#1095#1077#1089#1085#1080#1094#1080' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_BROJ_UCESNICI'
    end
    object tblIntervjuID_SELEKCIJA: TFIBIntegerField
      DisplayLabel = #1057#1077#1083#1077#1082#1094#1080#1112#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_SELEKCIJA'
    end
    object tblIntervjuOPIS_POCETOK: TFIBStringField
      DisplayLabel = #1055#1086#1095#1077#1090#1086#1082
      FieldName = 'OPIS_POCETOK'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuOPIS_SREDINA: TFIBStringField
      DisplayLabel = #1057#1088#1077#1076#1080#1085#1072
      FieldName = 'OPIS_SREDINA'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuOPIS_KRAJ: TFIBStringField
      DisplayLabel = #1050#1088#1072#1112
      FieldName = 'OPIS_KRAJ'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblIntervjuTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblIntervjuUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuINTERVJUISTNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091#1080#1089#1090
      FieldName = 'INTERVJUISTNAZIV'
      Size = 151
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuMOLBANAZIV: TFIBStringField
      DisplayLabel = #1052#1086#1083#1073#1072' '#1085#1072
      FieldName = 'MOLBANAZIV'
      Size = 101
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuBRUCESNICINAZIV: TFIBStringField
      DisplayLabel = #1041#1088'. '#1091#1095#1077#1089#1085#1080#1094#1080
      FieldName = 'BRUCESNICINAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuOBLIKNAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1083#1080#1082
      FieldName = 'OBLIKNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuSELEKCIJANAZIV: TFIBStringField
      DisplayLabel = #1057#1077#1083#1077#1082#1094#1080#1112#1072
      FieldName = 'SELEKCIJANAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblIntervjuID_RM_RE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ID_RM_RE'
    end
    object tblIntervjuNAZIV_RM: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuID_RM_RE_MOLBA: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072') '#1087#1086' '#1084#1086#1083#1073#1072
      FieldName = 'ID_RM_RE_MOLBA'
    end
    object tblIntervjuNAZIV_RM_MOLBA: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1087#1086' '#1084#1086#1083#1073#1072
      FieldName = 'NAZIV_RM_MOLBA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIntervjuBROJ_MOLBA: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1084#1086#1083#1073#1072
      FieldName = 'BROJ_MOLBA'
    end
    object tblIntervjuBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1085#1090#1077#1088#1074#1112#1091
      FieldName = 'BROJ'
    end
  end
  object dsIntervju: TDataSource
    DataSet = tblIntervju
    Left = 112
    Top = 584
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 648
    Top = 24
  end
  object pMAXBrojKategorija: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE MAX_PROC_HR_KATEGORIJA_BROJ (?RM)')
    StoredProcName = 'MAX_PROC_HR_KATEGORIJA_BROJ'
    Left = 648
    Top = 112
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pMaksKBroj: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(p.maks,1) maks'
      'from max_proc_hr_kategorija_broj(:rm) p')
    Left = 656
    Top = 248
  end
  object dsStatus: TDataSource
    DataSet = tblStatus
    Left = 360
    Top = 192
  end
  object tblStatus: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ms.id,'
      '       ms.grupa,'
      '       ms.naziv,'
      '       ms.boja'
      'from mat_status ms'
      'where(  ms.grupa = :grupa'
      '     ) and (     MS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ms.id,'
      '       ms.grupa,'
      '       ms.naziv,'
      '       ms.boja'
      'from mat_status ms'
      'where ms.grupa = :grupa')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 192
    object tblStatusID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStatusGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object tblStatusNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStatusBOJA: TFIBIntegerField
      DisplayLabel = #1041#1086#1112#1072
      FieldName = 'BOJA'
    end
  end
  object pKopirajPraznici: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_KOPIRAJ_PRAZNICI (?GODINA)')
    StoredProcName = 'PROC_HR_KOPIRAJ_PRAZNICI'
    Left = 656
    Top = 304
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pMaksBrojNull: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(p.maks,1) maks'
      'from MAX_PROC_HR_KATEGORIJA_BRNULL p')
    Left = 656
    Top = 368
  end
  object tblResenieOtsustva: TpFIBDataSet
    RefreshSQL.Strings = (
      'select hrg.id,'
      
        '       coalesce(hrg.broj, '#39' '#39') || '#39'\'#39' || coalesce(hrg.godina, '#39#39 +
        ') as BrGodinaResenie'
      'from hr_resenie_go hrg'
      'where(  hrg.godina = :godina and hrg.mb like :mb'
      '     ) and (     HRG.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select hrg.id,'
      
        '       coalesce(hrg.broj, '#39' '#39') || '#39'/'#39' || coalesce(hrg.godina, '#39#39 +
        ') as BrGodinaResenie'
      'from hr_resenie_go hrg'
      'where hrg.godina = :godina and hrg.mb like :mb'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 272
    Top = 416
    object tblResenieOtsustvaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblResenieOtsustvaBRGODINARESENIE: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112'\'#1043#1086#1076#1080#1085#1072
      FieldName = 'BRGODINARESENIE'
      Size = 62
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsResenieOtsustva: TDataSource
    DataSet = tblResenieOtsustva
    Left = 376
    Top = 416
  end
  object pOstanatiDenovi: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_OSTANATIDENOVI (?IDRESENIEGO)')
    StoredProcName = 'PROC_HR_OSTANATIDENOVI'
    Left = 264
    Top = 472
  end
  object pDatumDo: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_WORK_DATE (?OD_DATUM, ?DENOVI, ?MB)')
    StoredProcName = 'PROC_HR_WORK_DATE'
    Left = 376
    Top = 472
  end
  object tblOstanatiDenoviGO: TpFIBDataSet
    SelectSQL.Strings = (
      'select mb,'
      '       naziv_vraboten,'
      '       naziv_vraboten_ti,'
      '       BrGodinaResenie,'
      
        '       coalesce((select proc_hr_ostanatidenovi.iskoristeni_out f' +
        'rom proc_hr_ostanatidenovi(id)), 0) as denovi,'
      
        '       coalesce((select proc_hr_ostanatidenovi.ostanati_out from' +
        ' proc_hr_ostanatidenovi(id)), 0) as ostanatiDenovi,'
      
        '       coalesce((select proc_hr_ostanatidenovi.denoviodresenie_o' +
        'ut from proc_hr_ostanatidenovi(id)),0) as ResenieDenovi,'
      '       coalesce(id, '#39#39') as ResenieID,'
      '       rabotnomestonaziv,'
      '       rabotno_mesto,'
      '       re,'
      '       rm_id,'
      '       rabotnaedinicanaziv'
      'from'
      '(select hrv.mb,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hrg.id,'
      
        '       coalesce(hrg.broj, '#39' '#39') || '#39'/'#39' || coalesce(hrg.godina, '#39#39 +
        ') as BrGodinaResenie,'
      '       hrv.rabotnomestonaziv,'
      '       hrv.rabotno_mesto,'
      '       hrv.re,'
      '       hrv.rm_id,'
      '       hrv.rabotnaedinicanaziv'
      'from view_hr_vraboteni hrv'
      
        'left join hr_resenie_go hrg on hrg.mb = hrv.mb and hrg.godina li' +
        'ke :godina'
      
        'where hrv.id_re_firma = :firma and hrv.mb like :MB   and hrv.re ' +
        'like :re and (current_date between hrv.datum_od and coalesce(hrv' +
        '.datum_do, current_date)))'
      'order by rabotnaedinicanaziv,rabotnomestonaziv,naziv_vraboten '
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 248
    object tblOstanatiDenoviGOMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'.'
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOstanatiDenoviGOBRGODINARESENIE: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112'/'#1043#1086#1076#1080#1085#1072' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054
      FieldName = 'BRGODINARESENIE'
      Size = 62
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOstanatiDenoviGOOSTANATIDENOVI: TFIBIntegerField
      DisplayLabel = #1054#1089#1090#1072#1085#1072#1090#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054
      FieldName = 'OSTANATIDENOVI'
    end
    object tblOstanatiDenoviGORESENIEDENOVI: TFIBIntegerField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1076#1077#1085#1086#1074#1080' '#1086#1076' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054
      FieldName = 'RESENIEDENOVI'
    end
    object tblOstanatiDenoviGORABOTNOMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABOTNOMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOstanatiDenoviGORESENIEID: TFIBStringField
      FieldName = 'RESENIEID'
      Size = 11
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOstanatiDenoviGODENOVI: TFIBIntegerField
      DisplayLabel = #1048#1089#1082#1086#1088#1080#1089#1090#1077#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1087#1086#1088#1077#1076' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054
      FieldName = 'DENOVI'
    end
    object tblOstanatiDenoviGONAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOstanatiDenoviGONAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOstanatiDenoviGORE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RE'
    end
    object tblOstanatiDenoviGORM_ID: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RM_ID'
    end
    object tblOstanatiDenoviGORABOTNO_MESTO: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RABOTNO_MESTO'
    end
    object tblOstanatiDenoviGORABOTNAEDINICANAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RABOTNAEDINICANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsOstanatiDenoviGO: TDataSource
    DataSet = tblOstanatiDenoviGO
    Left = 360
    Top = 248
  end
  object qDeleteDokument: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  hr_dogovor_vrabotuvanje '
      'set data = null'
      'where id = :id')
    Left = 656
    Top = 472
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pKontrolaOtsustva: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_KONTROLAOTSUSTVA (?MB_IN, ?TIP_OTSUSTV' +
        'O_ID_IN, ?DATUM_OD_IN, ?DATUM_DO_IN, ?TIP)')
    StoredProcName = 'PROC_HR_KONTROLAOTSUSTVA'
    Left = 656
    Top = 552
  end
  object pNeplateniOstanati: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_OSTANATINEPLATENI (?MB_IN, ?TIP_OTSUST' +
        'VO_ID_IN, ?TIP, ?FLAG_IN, ?GODINA_OD_IN, ?GODINA_DO_IN)')
    StoredProcName = 'PROC_HR_OSTANATINEPLATENI'
    Left = 656
    Top = 616
  end
  object pInsertVolonteri: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_INSERTVOLONTERI (?MB, ?ID_RM_RE, ?VID_' +
        'DOKUMENT, ?ID_RE_FIRMA, ?DATUM_OD, ?DATUM_DO, ?PLATA, ?BROJ_CASO' +
        'VI, ?RAKOVODITEL, ?BROJ, ?DATUM, ?ARHIVSKI_BROJ)')
    StoredProcName = 'PROC_HR_INSERTVOLONTERI'
    Left = 544
    Top = 616
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteDokVolonter: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  HR_DOGOVOR_VOLONTERI '
      'set data = null'
      'where id = :id')
    Left = 560
    Top = 472
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteAneks: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  HR_DV_ANEKS'
      'set data = null'
      'where id = :id')
    Left = 552
    Top = 520
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateVrabotenRM: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  HR_VRABOTEN_RM r'
      'set ID_RM_RE = :ID_RM_RE,'
      ''
      'where document_id = :document_id and'
      '      dokument = :vid_dokument')
    Left = 248
    Top = 536
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteVrabotenRM: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete  from hr_vraboten_rm'
      'where document_id = :documentID  and '
      '      dokument = :vid_dokument')
    Left = 360
    Top = 544
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pKopirajDogovor: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_KOPIRAJ_DOGOVOR (?ID_DOGOVOR, ?DATUM_O' +
        'D_IN, ?DATUM_DO_IN)')
    StoredProcName = 'PROC_HR_KOPIRAJ_DOGOVOR'
    Left = 552
    Top = 408
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblRealizacijaGO: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_hr_go_realizacija.mb_out,'
      '       proc_hr_go_realizacija.nazivvraboten_out,'
      '       proc_hr_go_realizacija.plateno_out,'
      '       proc_hr_go_realizacija.neplateno_out,'
      '       proc_hr_go_realizacija.vkupno_tekovna_godina,'
      '       proc_hr_go_realizacija.vkupno_predhodna_godina,'
      '       proc_hr_go_realizacija.ostanati_tekovna'
      'from proc_hr_go_realizacija(:firma, :pricina, :godina)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 256
    Top = 304
    object tblRealizacijaGOMB_OUT: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB_OUT'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaGONAZIVVRABOTEN_OUT: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077
      FieldName = 'NAZIVVRABOTEN_OUT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaGOPLATENO_OUT: TFIBIntegerField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
      FieldName = 'PLATENO_OUT'
    end
    object tblRealizacijaGONEPLATENO_OUT: TFIBIntegerField
      DisplayLabel = #1053#1077#1087#1083#1072#1090#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
      FieldName = 'NEPLATENO_OUT'
    end
    object tblRealizacijaGOVKUPNO_TEKOVNA_GODINA: TFIBIntegerField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1076#1077#1085#1086#1074#1080' '#1089#1087#1086#1088#1077#1076' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054' ('#1090#1077#1082#1086#1074#1085#1072' '#1075#1086#1076#1080#1085#1072')'
      FieldName = 'VKUPNO_TEKOVNA_GODINA'
    end
    object tblRealizacijaGOVKUPNO_PREDHODNA_GODINA: TFIBIntegerField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1076#1077#1085#1086#1074#1080' '#1089#1087#1086#1088#1077#1076' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054' ('#1087#1088#1077#1076#1093#1086#1076#1085#1072' '#1075#1086#1076#1080#1085#1072')'
      FieldName = 'VKUPNO_PREDHODNA_GODINA'
    end
    object tblRealizacijaGOOSTANATI_TEKOVNA: TFIBIntegerField
      DisplayLabel = #1054#1089#1090#1072#1085#1072#1090#1080' '#1076#1077#1085#1086#1074#1080' ('#1086#1076' '#1090#1077#1082#1086#1074#1085#1086' '#1088#1077#1096#1077#1085#1080#1077')'
      FieldName = 'OSTANATI_TEKOVNA'
    end
  end
  object dsRealizacijaGO: TDataSource
    DataSet = tblRealizacijaGO
    Left = 352
    Top = 304
  end
  object pPreraspredelbaOtsustva: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_OTSUSTVA_PRERASPREDELBA (?OTSUSTVO_ID_' +
        'IN, ?MB_IN, ?DATUM_OD, ?DATUM_DO)')
    StoredProcName = 'PROC_HR_OTSUSTVA_PRERASPREDELBA'
    Left = 360
    Top = 600
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteResenieOtsustva: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete  from HR_OTSUSTVA_RESENIJA'
      'where OTSUSTVO_ID = :id')
    Left = 224
    Top = 600
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pZapisiOtsustvoRaspredeleno: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_OTSUSTVO_ZAPISI (?DATUM_OD, ?DATUM_DO,' +
        ' ?PLATENI, ?NEPLATENI, ?MB, ?PRICINA, ?PRICINA_OPIS)')
    StoredProcName = 'PROC_HR_OTSUSTVO_ZAPISI'
    Left = 552
    Top = 344
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pOtsustva_Realizacija: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_HR_OTSUSTVA_REALIZACIJA (?MB_IN, ?TIP_OTS' +
        'USTVO_ID_IN, ?DATUM_OD_IN, ?DATUM_DO_IN, ?TIP)')
    StoredProcName = 'PROC_HR_OTSUSTVA_REALIZACIJA'
    Left = 656
    Top = 672
  end
  object pOstanatiGOGodina: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_OSTANATIDENOVI_GODINA (?GODINA, ?MB)')
    StoredProcName = 'PROC_HR_OSTANATIDENOVI_GODINA'
    Left = 544
    Top = 672
  end
  object qDeleteOtsustvo: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    HR_OTSUSTVA'
      'WHERE'
      '        ID = :OLD_ID')
    Left = 224
    Top = 664
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteBaranjeGO: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  HR_BARANJE_GO '
      'set data = null'
      'where id = :id')
    Left = 48
    Top = 752
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteReseniGO: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  HR_RESENIE_GO '
      'set data = null'
      'where id = :id')
    Left = 144
    Top = 752
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteReseniePreraspredelba: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  hr_resenie_preraspredelba '
      'set data = null'
      'where id = :id')
    Left = 40
    Top = 800
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteResenieOtkaz: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  hr_resenie_otkaz'
      'set data = null'
      'where id = :id')
    Left = 168
    Top = 800
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeletePotvrdaRabOdnos: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  HR_POTVRDA_RRO '
      'set data = null'
      'where id = :id')
    Left = 256
    Top = 752
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblOtsustva2: TpFIBDataSet
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.tip_zapis,'
      '       hro.mb,'
      '       hro.pricina,'
      '       hro.pricina_opis,'
      '       hrto.naziv as tipOtsustvoNaziv,'
      '       case hrto.dogovor when '#39'1'#39' then '#39#1044#1072#39
      '                         when '#39'0'#39' then '#39#1053#1077#39
      '       end "KolektivenDogDaNe",'
      '       hro.od_vreme,'
      '       hro.do_vreme,'
      '       hro.do_vreme_l,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrv.prezime as vrabotenPrezime,'
      '       hrv.tatkovo_ime as vrabotenTatkovoIme,'
      '       hrv.ime as vrabotenIme,'
      
        '       hrv.naziv_vraboten || '#39' - '#39' || coalesce(hrto.naziv, '#39' '#39') ' +
        'as opis,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      '       hro.plateno,'
      '       hro.flag,'
      '       case hro.plateno when '#39'1'#39' then '#39#1055#1083#1072#1090#1077#1085#1086#39
      '                        when '#39'0'#39' then '#39#1053#1077#1087#1083#1072#1090#1077#1085#1086#39
      '       end "PlatenoDaNe",'
      
        '       (select proc_hr_work_days.denovi from proc_hr_work_days(h' +
        'ro.od_vreme, hro.do_vreme, hro.mb))as denovi,'
      '       hrto.boja'
      'from hr_otsustva hro'
      'inner join hr_tip_otsustvo hrto on hrto.id = hro.pricina'
      'inner join view_hr_vraboteni hrv on hrv.mb = hro.mb'
      'where hrv.id_re_firma = :firma'
      '      and  hro.tip_zapis = 1'
      '      and hro.mb like :MB'
      '      and hro.pricina like :pricina'
      '      and hrv.re like :re'
      
        '      and (current_date between hrv.datum_od and coalesce(hrv.da' +
        'tum_do, current_date))'
      
        '      and ((extractyear(hro.od_vreme) = :param_godina and extrac' +
        'tyear(hro.do_vreme_l) = :param_godina)'
      
        '           or (extractyear(hro.od_vreme) = :param_godina - 1 and' +
        ' extractyear(hro.do_vreme_l) = :param_godina)'
      
        '           or (extractyear(hro.od_vreme) = :param_godina and ext' +
        'ractyear(hro.do_vreme_l) = :param_godina +1))'
      'order by hro.od_vreme, hrv.naziv_vraboten_ti')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 32
    Top = 272
    object tblOtsustva2ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblOtsustva2TIP_ZAPIS: TFIBIntegerField
      FieldName = 'TIP_ZAPIS'
    end
    object tblOtsustva2MB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2PRICINA: TFIBIntegerField
      FieldName = 'PRICINA'
    end
    object tblOtsustva2PRICINA_OPIS: TFIBStringField
      FieldName = 'PRICINA_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2TIPOTSUSTVONAZIV: TFIBStringField
      FieldName = 'TIPOTSUSTVONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2KolektivenDogDaNe: TFIBStringField
      FieldName = 'KolektivenDogDaNe'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2OD_VREME: TFIBDateTimeField
      FieldName = 'OD_VREME'
    end
    object tblOtsustva2DO_VREME: TFIBDateTimeField
      FieldName = 'DO_VREME'
    end
    object tblOtsustva2DO_VREME_L: TFIBDateTimeField
      FieldName = 'DO_VREME_L'
    end
    object tblOtsustva2TS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblOtsustva2TS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblOtsustva2USR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2USR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2VRABOTENPREZIME: TFIBStringField
      FieldName = 'VRABOTENPREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2VRABOTENTATKOVOIME: TFIBStringField
      FieldName = 'VRABOTENTATKOVOIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2VRABOTENIME: TFIBStringField
      FieldName = 'VRABOTENIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2OPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 353
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2NAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2NAZIV_VRABOTEN_TI: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2OPTIONS: TFIBIntegerField
      FieldName = 'OPTIONS'
    end
    object tblOtsustva2EVENT_TYPE: TFIBIntegerField
      FieldName = 'EVENT_TYPE'
    end
    object tblOtsustva2TASK_LINKS_FIELD: TFIBMemoField
      FieldName = 'TASK_LINKS_FIELD'
      BlobType = ftMemo
      Size = 8
    end
    object tblOtsustva2PLATENO: TFIBSmallIntField
      FieldName = 'PLATENO'
    end
    object tblOtsustva2FLAG: TFIBIntegerField
      FieldName = 'FLAG'
    end
    object tblOtsustva2PlatenoDaNe: TFIBStringField
      FieldName = 'PlatenoDaNe'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOtsustva2DENOVI: TFIBIntegerField
      FieldName = 'DENOVI'
    end
    object tblOtsustva2BOJA: TFIBIntegerField
      FieldName = 'BOJA'
    end
  end
  object dsOtsustva2: TDataSource
    DataSet = tblOtsustva2
    Left = 112
    Top = 272
  end
  object tblPlanOtsustva: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_OTSUSTVA'
      'SET '
      '    TIP_ZAPIS = :TIP_ZAPIS,'
      '    MB = :MB,'
      '    PRICINA = :PRICINA,'
      '    PRICINA_OPIS = :PRICINA_OPIS,'
      '    OD_VREME = :OD_VREME,'
      '    DO_VREME = :DO_VREME,'
      '    DO_VREME_L = :DO_VREME_L,'
      '    STATUS = :STATUS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    OPTIONS = :OPTIONS,'
      '    EVENT_TYPE = :EVENT_TYPE,'
      '    TASK_LINKS_FIELD = :TASK_LINKS_FIELD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_OTSUSTVA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_OTSUSTVA('
      '    ID,'
      '    TIP_ZAPIS,'
      '    MB,'
      '    PRICINA,'
      '    PRICINA_OPIS,'
      '    OD_VREME,'
      '    DO_VREME,'
      '    DO_VREME_L,'
      '    STATUS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    OPTIONS,'
      '    EVENT_TYPE,'
      '    TASK_LINKS_FIELD'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_ZAPIS,'
      '    :MB,'
      '    :PRICINA,'
      '    :PRICINA_OPIS,'
      '    :OD_VREME,'
      '    :DO_VREME,'
      '    :DO_VREME_L,'
      '    :STATUS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :OPTIONS,'
      '    :EVENT_TYPE,'
      '    :TASK_LINKS_FIELD'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.tip_zapis,'
      '       hro.mb,'
      '       hro.pricina,'
      '       hro.pricina_opis,'
      '       hro.od_vreme,'
      '       hro.do_vreme,'
      '       hro.do_vreme_l,'
      '       hro.status,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrto.naziv as tipOtsustvoNaziv,'
      '       hrv.prezime as vrabotenPrezime,'
      '       hrv.tatkovo_ime as vrabotenTatkovoIme,'
      '       hrv.ime as vrabotenIme,'
      '       ms.naziv as statusNaziv,'
      '       ms.boja as status_boja,'
      
        '       hrv.naziv_vraboten || '#39' - '#39' || coalesce(hrto.naziv, '#39' '#39') ' +
        'as opis,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      
        '       (select proc_hr_work_days.denovi from proc_hr_work_days(h' +
        'ro.od_vreme, hro.do_vreme, hro.mb))as denovi'
      'from hr_otsustva hro'
      'inner join hr_tip_otsustvo hrto on hrto.id = hro.pricina'
      'inner join view_hr_vraboteni hrv on hrv.mb = hro.mb'
      'left outer join mat_status ms on ms.id = hro.status'
      'where(  hrv.id_re_firma = :firma and hro.tip_zapis = 0 and'
      
        '      hro.mb like :MB and hro.pricina like :pricina and hro.stat' +
        'us like :status and'
      
        '      hrv.re like :re and (current_date between hrv.datum_od and' +
        ' coalesce(hrv.datum_do, current_date)) and'
      
        '      ((extractyear(hro.od_vreme) = :param_godina and extractyea' +
        'r(hro.do_vreme_l) = :param_godina)'
      
        '        or (extractyear(hro.od_vreme) = :param_godina - 1 and ex' +
        'tractyear(hro.do_vreme_l) = :param_godina)'
      
        '        or (extractyear(hro.od_vreme) = :param_godina and extrac' +
        'tyear(hro.do_vreme_l) = :param_godina +1))'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.tip_zapis,'
      '       hro.mb,'
      '       hro.pricina,'
      '       hro.pricina_opis,'
      '       hro.od_vreme,'
      '       hro.do_vreme,'
      '       hro.do_vreme_l,'
      '       hro.status,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrto.naziv as tipOtsustvoNaziv,'
      '       hrv.prezime as vrabotenPrezime,'
      '       hrv.tatkovo_ime as vrabotenTatkovoIme,'
      '       hrv.ime as vrabotenIme,'
      '       ms.naziv as statusNaziv,'
      '       ms.boja as status_boja,'
      
        '       hrv.naziv_vraboten || '#39' - '#39' || coalesce(hrto.naziv, '#39' '#39') ' +
        'as opis,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      
        '       (select proc_hr_work_days.denovi from proc_hr_work_days(h' +
        'ro.od_vreme, hro.do_vreme, hro.mb))as denovi'
      'from hr_otsustva hro'
      'inner join hr_tip_otsustvo hrto on hrto.id = hro.pricina'
      'inner join view_hr_vraboteni hrv on hrv.mb = hro.mb'
      'left outer join mat_status ms on ms.id = hro.status'
      'where hrv.id_re_firma = :firma and hro.tip_zapis = 0 and'
      
        '      hro.mb like :MB and hro.pricina like :pricina and hro.stat' +
        'us like :status and'
      
        '      hrv.re like :re and (current_date between hrv.datum_od and' +
        ' coalesce(hrv.datum_do, current_date)) and'
      
        '      ((extractyear(hro.od_vreme) = :param_godina and extractyea' +
        'r(hro.do_vreme_l) = :param_godina)'
      
        '        or (extractyear(hro.od_vreme) = :param_godina - 1 and ex' +
        'tractyear(hro.do_vreme_l) = :param_godina)'
      
        '        or (extractyear(hro.od_vreme) = :param_godina and extrac' +
        'tyear(hro.do_vreme_l) = :param_godina +1))'
      'order by hro.od_vreme, hrv.naziv_vraboten_ti')
    AutoUpdateOptions.UpdateTableName = 'HR_OTSUSTVA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_OTSUSTVA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 264
    Top = 24
    object tblPlanOtsustvaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPlanOtsustvaTIP_ZAPIS: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TIP_ZAPIS'
    end
    object tblPlanOtsustvaMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaPRICINA: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PRICINA'
    end
    object tblPlanOtsustvaPRICINA_OPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'PRICINA_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaOD_VREME: TFIBDateTimeField
      DisplayLabel = #1054#1076
      FieldName = 'OD_VREME'
      DisplayFormat = 'dd.mm.yyyy '
    end
    object tblPlanOtsustvaDO_VREME: TFIBDateTimeField
      DisplayLabel = #1044#1086
      FieldName = 'DO_VREME'
      DisplayFormat = 'dd.mm.yyyy '
    end
    object tblPlanOtsustvaDO_VREME_L: TFIBDateTimeField
      DisplayLabel = #1044#1086
      FieldName = 'DO_VREME_L'
      DisplayFormat = 'dd.mm.yyyy '
    end
    object tblPlanOtsustvaSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' ('#1064#1080#1092#1088#1072')'
      FieldName = 'STATUS'
    end
    object tblPlanOtsustvaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblPlanOtsustvaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblPlanOtsustvaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaTIPOTSUSTVONAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
      FieldName = 'TIPOTSUSTVONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaVRABOTENPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'VRABOTENPREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaVRABOTENTATKOVOIME: TFIBStringField
      DisplayLabel = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077
      FieldName = 'VRABOTENTATKOVOIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaVRABOTENIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'VRABOTENIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaSTATUSNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaSTATUS_BOJA: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' - '#1073#1086#1112#1072
      FieldName = 'STATUS_BOJA'
    end
    object tblPlanOtsustvaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1082#1072#1083#1077#1085#1076#1072#1088
      FieldName = 'OPIS'
      Size = 353
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustvaOPTIONS: TFIBIntegerField
      FieldName = 'OPTIONS'
    end
    object tblPlanOtsustvaEVENT_TYPE: TFIBIntegerField
      FieldName = 'EVENT_TYPE'
    end
    object tblPlanOtsustvaTASK_LINKS_FIELD: TFIBMemoField
      FieldName = 'TASK_LINKS_FIELD'
      BlobType = ftMemo
      Size = 8
    end
    object tblPlanOtsustvaDENOVI: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080
      FieldName = 'DENOVI'
    end
  end
  object dsPlanOtsustva: TDataSource
    DataSet = tblPlanOtsustva
    Left = 360
    Top = 24
  end
  object tblPlanOtsustva2: TpFIBDataSet
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.tip_zapis,'
      '       hro.mb,'
      '       hro.pricina,'
      '       hro.pricina_opis,'
      '       hro.od_vreme,'
      '       hro.do_vreme,'
      '       hro.do_vreme_l,'
      '       hro.status,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hrto.naziv as tipOtsustvoNaziv,'
      '       hrv.prezime as vrabotenPrezime,'
      '       hrv.tatkovo_ime as vrabotenTatkovoIme,'
      '       hrv.ime as vrabotenIme,'
      '       ms.naziv as statusNaziv,'
      '       ms.boja as status_boja,'
      
        '       hrv.naziv_vraboten || '#39' - '#39' || coalesce(hrto.naziv, '#39' '#39') ' +
        'as opis,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       hro.options,'
      '       hro.event_type,'
      '       hro.task_links_field,'
      
        '       (select proc_hr_work_days.denovi from proc_hr_work_days(h' +
        'ro.od_vreme, hro.do_vreme, hro.mb))as denovi'
      'from hr_otsustva hro'
      'inner join hr_tip_otsustvo hrto on hrto.id = hro.pricina'
      'inner join view_hr_vraboteni hrv on hrv.mb = hro.mb'
      'left outer join mat_status ms on ms.id = hro.status'
      'where hrv.id_re_firma = :firma and hro.tip_zapis = 0 and'
      
        '      hro.mb like :MB and hro.pricina like :pricina and hro.stat' +
        'us like :status and'
      
        '      hrv.re like :re and (current_date between hrv.datum_od and' +
        ' coalesce(hrv.datum_do, current_date)) and'
      
        '      ((extractyear(hro.od_vreme) = :param_godina and extractyea' +
        'r(hro.do_vreme_l) = :param_godina)'
      
        '        or (extractyear(hro.od_vreme) = :param_godina - 1 and ex' +
        'tractyear(hro.do_vreme_l) = :param_godina)'
      
        '        or (extractyear(hro.od_vreme) = :param_godina and extrac' +
        'tyear(hro.do_vreme_l) = :param_godina +1))'
      'order by hro.od_vreme, hrv.naziv_vraboten_ti')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 32
    Top = 328
    object tblPlanOtsustva2ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblPlanOtsustva2TIP_ZAPIS: TFIBIntegerField
      FieldName = 'TIP_ZAPIS'
    end
    object tblPlanOtsustva2MB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2PRICINA: TFIBIntegerField
      FieldName = 'PRICINA'
    end
    object tblPlanOtsustva2PRICINA_OPIS: TFIBStringField
      FieldName = 'PRICINA_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2OD_VREME: TFIBDateTimeField
      FieldName = 'OD_VREME'
    end
    object tblPlanOtsustva2DO_VREME: TFIBDateTimeField
      FieldName = 'DO_VREME'
    end
    object tblPlanOtsustva2DO_VREME_L: TFIBDateTimeField
      FieldName = 'DO_VREME_L'
    end
    object tblPlanOtsustva2STATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblPlanOtsustva2TS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblPlanOtsustva2TS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblPlanOtsustva2USR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2USR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2TIPOTSUSTVONAZIV: TFIBStringField
      FieldName = 'TIPOTSUSTVONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2VRABOTENPREZIME: TFIBStringField
      FieldName = 'VRABOTENPREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2VRABOTENTATKOVOIME: TFIBStringField
      FieldName = 'VRABOTENTATKOVOIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2VRABOTENIME: TFIBStringField
      FieldName = 'VRABOTENIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2STATUSNAZIV: TFIBStringField
      FieldName = 'STATUSNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2STATUS_BOJA: TFIBIntegerField
      FieldName = 'STATUS_BOJA'
    end
    object tblPlanOtsustva2OPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 353
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2NAZIV_VRABOTEN: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2NAZIV_VRABOTEN_TI: TFIBStringField
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPlanOtsustva2OPTIONS: TFIBIntegerField
      FieldName = 'OPTIONS'
    end
    object tblPlanOtsustva2EVENT_TYPE: TFIBIntegerField
      FieldName = 'EVENT_TYPE'
    end
    object tblPlanOtsustva2TASK_LINKS_FIELD: TFIBMemoField
      FieldName = 'TASK_LINKS_FIELD'
      BlobType = ftMemo
      Size = 8
    end
    object tblPlanOtsustva2DENOVI: TFIBIntegerField
      FieldName = 'DENOVI'
    end
  end
  object dsPlanOtsustva2: TDataSource
    DataSet = tblPlanOtsustva2
    Left = 120
    Top = 328
  end
  object qDeleteIzjavaProdolzenRabOdnos: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  hr_izjava_prodolzen_ro'
      'set data = null'
      'where id = :id')
    Left = 416
    Top = 752
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblCountTipOtsustvo: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_hr_otsustva_pregled_tip.re_out,'
      '       proc_hr_otsustva_pregled_tip.plateno_out,'
      '       proc_hr_otsustva_pregled_tip.neplateno_out,'
      '       proc_hr_otsustva_pregled_tip.count_1_out,'
      '       proc_hr_otsustva_pregled_tip.count_0_out,'
      '       proc_hr_otsustva_pregled_tip.mb_out,'
      '       proc_hr_otsustva_pregled_tip.nazivvraboten_out,'
      '       proc_hr_otsustva_pregled_tip.rmre_out,'
      '       proc_hr_otsustva_pregled_tip.renaziv_out,'
      '       proc_hr_otsustva_pregled_tip.rabmestonaziv_out,'
      '       proc_hr_otsustva_pregled_tip.hrmrabid_out,'
      '       proc_hr_otsustva_pregled_tip.naziv_vraboten_ti_out,'
      '       proc_hr_otsustva_pregled_tip.tip_otsustvo,'
      '       proc_hr_otsustva_pregled_tip.tip_otsustvo_naziv,'
      '       proc_hr_otsustva_pregled_tip.count_tip_otsustvo'
      
        'from proc_hr_otsustva_pregled_tip(:od_datum_in, :do_datum_in, :f' +
        'irma, :re)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 592
    Top = 752
    object tblCountTipOtsustvoRE_OUT: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RE_OUT'
    end
    object tblCountTipOtsustvoPLATENO_OUT: TFIBIntegerField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'PLATENO_OUT'
    end
    object tblCountTipOtsustvoNEPLATENO_OUT: TFIBIntegerField
      DisplayLabel = #1053#1077#1087#1083#1072#1090#1077#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'NEPLATENO_OUT'
    end
    object tblCountTipOtsustvoCOUNT_1_OUT: TFIBIntegerField
      DisplayLabel = #1056#1077#1072#1083#1080#1079#1080#1088#1072#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'COUNT_1_OUT'
    end
    object tblCountTipOtsustvoCOUNT_0_OUT: TFIBIntegerField
      DisplayLabel = #1055#1083#1072#1085#1080#1088#1072#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
      FieldName = 'COUNT_0_OUT'
    end
    object tblCountTipOtsustvoMB_OUT: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'.'
      FieldName = 'MB_OUT'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblCountTipOtsustvoNAZIVVRABOTEN_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIVVRABOTEN_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblCountTipOtsustvoRMRE_OUT: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1074#1086' '#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RMRE_OUT'
    end
    object tblCountTipOtsustvoRENAZIV_OUT: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblCountTipOtsustvoRABMESTONAZIV_OUT: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABMESTONAZIV_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblCountTipOtsustvoHRMRABID_OUT: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' ('#1064#1080#1092#1088#1072')'
      FieldName = 'HRMRABID_OUT'
    end
    object tblCountTipOtsustvoNAZIV_VRABOTEN_TI_OUT: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI_OUT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblCountTipOtsustvoTIP_OTSUSTVO: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1086#1090#1089#1091#1089#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP_OTSUSTVO'
    end
    object tblCountTipOtsustvoTIP_OTSUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1086#1090#1089#1091#1089#1090#1074#1086
      FieldName = 'TIP_OTSUSTVO_NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblCountTipOtsustvoCOUNT_TIP_OTSUSTVO: TFIBIntegerField
      DisplayLabel = #1044#1077#1085#1086#1074#1080
      FieldName = 'COUNT_TIP_OTSUSTVO'
    end
  end
  object dsCountTipOtsustvo: TDataSource
    DataSet = tblCountTipOtsustvo
    Left = 704
    Top = 752
  end
  object pPROC_HR_STAZ_DOGOVOR: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_HR_STAZ_DOGOVOR (?MB, ?DATUM_OD_IN)')
    StoredProcName = 'PROC_HR_STAZ_DOGOVOR'
    Left = 816
    Top = 752
  end
end
