object frmScanDocument: TfrmScanDocument
  Left = 0
  Top = 0
  Caption = #1057#1082#1077#1085#1080#1088#1072#1112' '#1076#1086#1082#1091#1084#1077#1085#1090
  ClientHeight = 556
  ClientWidth = 746
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 312
    Top = 0
    Width = 434
    Height = 536
    Align = alRight
    Color = 15790320
    ParentBackground = False
    TabOrder = 0
    object cxLabel4: TcxLabel
      Left = 18
      Top = 4
      Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1112#1090#1077' '#1089#1082#1077#1085#1077#1088' :'
      ParentColor = False
      ParentFont = False
      Style.Color = 16311512
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWhite
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.SkinName = ''
      Style.TextColor = clDefault
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.SkinName = ''
      StyleFocused.LookAndFeel.SkinName = ''
      StyleHot.LookAndFeel.SkinName = ''
      Transparent = True
    end
    object cboxSource: TcxComboBox
      Left = 18
      Top = 21
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleDisabled.LookAndFeel.SkinName = ''
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.SkinName = ''
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.SkinName = ''
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 241
    end
    object cxButton1: TcxButton
      Left = 18
      Top = 48
      Width = 241
      Height = 25
      Action = aScan
      TabOrder = 1
      OnKeyDown = EnterKakoTab
    end
    object gbPodatoci: TcxGroupBox
      Left = 18
      Top = 99
      Caption = ' '#1055#1086#1076#1072#1090#1086#1094#1080' '
      Style.Shadow = False
      TabOrder = 2
      Transparent = True
      Height = 398
      Width = 399
      object Label8: TLabel
        Left = 12
        Top = 78
        Width = 189
        Height = 13
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 12
        Top = 123
        Width = 89
        Height = 13
        AutoSize = False
        Caption = #1042#1080#1076' '#1076#1086#1082#1091#1084#1077#1085#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 12
        Top = 27
        Width = 89
        Height = 13
        AutoSize = False
        Caption = #1043#1086#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object mOpis: TcxDBMemo
        Left = 12
        Top = 232
        DataBinding.DataField = 'ZABELESKA'
        DataBinding.DataSource = dm.dsSkeniraniDokumenti
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        Style.BorderStyle = ebsUltraFlat
        Style.LookAndFeel.Kind = lfUltraFlat
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.Shadow = False
        Style.TransparentBorder = True
        StyleDisabled.LookAndFeel.Kind = lfUltraFlat
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.Kind = lfUltraFlat
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.Kind = lfUltraFlat
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 124
        Width = 373
      end
      object cxLabel1: TcxLabel
        Left = 12
        Top = 212
        Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' :'
        ParentColor = False
        ParentFont = False
        Style.Color = 16311512
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWhite
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.TextColor = clNavy
        Style.IsFontAssigned = True
        Transparent = True
      end
      object btnIzvrsi: TcxButton
        Left = 12
        Top = 362
        Width = 373
        Height = 25
        Action = aZapisi
        TabOrder = 6
      end
      object cxLabel3: TcxLabel
        Left = 12
        Top = 167
        Caption = #1053#1072#1079#1080#1074' '#1076#1072#1090#1086#1090#1077#1082#1072' :'
        ParentColor = False
        ParentFont = False
        Style.Color = 16311512
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWhite
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.LookAndFeel.SkinName = ''
        Style.TextColor = clRed
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.SkinName = ''
        Transparent = True
      end
      object txtNaziv: TcxDBTextEdit
        Tag = 1
        Left = 12
        Top = 187
        DataBinding.DataField = 'NAZIV'
        DataBinding.DataSource = dm.dsSkeniraniDokumenti
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 241
      end
      object MB: TcxDBTextEdit
        Tag = 1
        Left = 12
        Top = 96
        Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
        BeepOnEnter = False
        DataBinding.DataField = 'MB'
        DataBinding.DataSource = dm.dsSkeniraniDokumenti
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clDefault
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 105
      end
      object VRABOTENIME: TcxDBLookupComboBox
        Tag = 1
        Left = 118
        Top = 96
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
        BeepOnEnter = False
        DataBinding.DataField = 'MB'
        DataBinding.DataSource = dm.dsSkeniraniDokumenti
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'MB'
        Properties.ListColumns = <
          item
            Width = 500
            FieldName = 'MB'
          end
          item
            Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
            Width = 800
            FieldName = 'NAZIV_VRABOTEN_TI'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsViewVraboteni
        StyleDisabled.TextColor = clBtnText
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 267
      end
      object cxVidDokument: TcxDBLookupComboBox
        Tag = 1
        Left = 12
        Top = 142
        Hint = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
        BeepOnEnter = False
        DataBinding.DataField = 'VID_DOKUMENT'
        DataBinding.DataSource = dm.dsSkeniraniDokumenti
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsVidDokument
        StyleDisabled.TextColor = clBtnText
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 373
      end
      object cxGodina: TcxDBComboBox
        Tag = 1
        Left = 12
        Top = 46
        DataBinding.DataField = 'GODINA'
        DataBinding.DataSource = dm.dsSkeniraniDokumenti
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          '2010'
          '2011'
          '2012'
          '2013')
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 105
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 312
    Height = 536
    Align = alClient
    Color = 16644078
    ParentBackground = False
    TabOrder = 1
    object ImageEnView1: TImageEnView
      AlignWithMargins = True
      Left = 4
      Top = 142
      Width = 304
      Height = 390
      Background = clBtnFace
      ParentCtl3D = False
      MouseInteract = [miScroll]
      BackgroundStyle = iebsPhotoLike
      EnableInteractionHints = True
      Align = alClient
      TabOrder = 0
    end
    object ImageEnMView1: TImageEnMView
      AlignWithMargins = True
      Left = 1
      Top = 4
      Width = 307
      Height = 95
      Margins.Left = 0
      Margins.Bottom = 0
      Background = clBtnFace
      ParentCtl3D = False
      StoreType = ietNormal
      ThumbWidth = 80
      ThumbHeight = 80
      HorizBorder = 4
      VertBorder = 4
      TextMargin = 0
      OnImageSelect = ImageEnMView1ImageSelect
      GridWidth = 0
      Style = iemsACD
      ThumbnailsBackground = clBtnFace
      ThumbnailsBackgroundSelected = clBtnFace
      MultiSelectionOptions = []
      ThumbnailsBorderWidth = 0
      ThumbnailsBorderColor = clBlack
      Align = alTop
      TabOrder = 1
    end
    object Panel3: TPanel
      Left = 1
      Top = 99
      Width = 310
      Height = 40
      Align = alTop
      Color = 15790320
      ParentBackground = False
      TabOrder = 2
      object cxButton6: TcxButton
        AlignWithMargins = True
        Left = 6
        Top = 6
        Width = 60
        Height = 28
        Hint = #1055#1086#1084#1077#1089#1090#1080' '#1112#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1083#1077#1074#1086
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alLeft
        Action = aShiftLeft
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object cxButton7: TcxButton
        AlignWithMargins = True
        Left = 71
        Top = 6
        Width = 60
        Height = 28
        Hint = #1055#1086#1084#1077#1089#1090#1080' '#1112#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1076#1077#1089#1085#1086
        Margins.Left = 0
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alLeft
        Action = aShiftRight
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object cxButton3: TcxButton
        AlignWithMargins = True
        Left = 141
        Top = 6
        Width = 100
        Height = 28
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alLeft
        Action = aRotiraj
        ParentShowHint = False
        ShowHint = False
        TabOrder = 2
      end
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 536
    Width = 746
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 240
    Top = 256
    object aScan: TAction
      Caption = #1057#1082#1077#1085#1080#1088#1072#1112' '#1076#1086#1082#1091#1084#1077#1085#1090'('#1080')'
      ImageIndex = 49
      OnExecute = aScanExecute
    end
    object aExit: TAction
      Caption = 'aExit'
      ImageIndex = 25
      ShortCut = 27
      OnExecute = aExitExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiExecute
    end
    object aShiftRight: TAction
      ImageIndex = 47
      OnExecute = aShiftRightExecute
    end
    object aShiftLeft: TAction
      ImageIndex = 48
      OnExecute = aShiftLeftExecute
    end
    object aRotiraj: TAction
      Caption = #1056#1086#1090#1080#1088#1072#1112
      ImageIndex = 14
      OnExecute = aRotirajExecute
    end
    object aProveriNaziv: TAction
      Caption = 'aProveriNaziv'
      OnExecute = aProveriNazivExecute
    end
  end
  object ImageEnMIO1: TImageEnMIO
    AttachedMView = ImageEnMView1
    PreviewFont.Charset = DEFAULT_CHARSET
    PreviewFont.Color = clWindowText
    PreviewFont.Height = -11
    PreviewFont.Name = 'Tahoma'
    PreviewFont.Style = []
    Left = 208
    Top = 32
  end
end
