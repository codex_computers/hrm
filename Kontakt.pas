unit Kontakt;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox,
  cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  DBCtrls, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint;

type
  TfrmKontakt = class(TfrmMaster)
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1TEL: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1MESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DRZAVANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DRZAVJANSTVONAZIV: TcxGridDBColumn;
    Label10: TLabel;
    ID_MOLBA: TcxDBTextEdit;
    IMEPREZIME: TcxDBLookupComboBox;
    Label11: TLabel;
    MB_VRABOTEN: TcxDBTextEdit;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    cxGroupBox1: TcxGroupBox;
    DRZAVANAZIV: TcxDBLookupComboBox;
    ADRESA: TcxDBTextEdit;
    DRZAVA: TcxDBTextEdit;
    Label7: TLabel;
    Label6: TLabel;
    MESTO: TcxDBTextEdit;
    MESTONAZIV: TcxDBLookupComboBox;
    BROJ: TcxDBTextEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label8: TLabel;
    DRZAVJANSTVO: TcxDBTextEdit;
    DRZAVJANSTVONAZIV: TcxDBLookupComboBox;
    cxGroupBox2: TcxGroupBox;
    Label9: TLabel;
    TEL: TcxDBTextEdit;
    Label12: TLabel;
    MOBILEN: TcxDBTextEdit;
    Label13: TLabel;
    EMAIL: TcxDBTextEdit;
    cxGroupBox3: TcxGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label14: TLabel;
    ITNO_SRODSTVO: TcxDBTextEdit;
    ITNO_IME_PREZIME: TcxDBTextEdit;
    ITNO_TELEFON: TcxDBTextEdit;
    Label15: TLabel;
    ITNO_ADRESA: TcxDBTextEdit;
    cxGrid1DBTableView1ITNO_SRODSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1ITNO_IME_PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1ITNO_ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1ITNO_TELEFON: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1DRZAVA: TcxGridDBColumn;
    cxGrid1DBTableView1DRZAVJANSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxDBCheckBox1: TcxDBCheckBox;
    cxGrid1DBTableView1lkNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmKontakt: TfrmKontakt;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  Utils, Drzavjanstvo, Drzava, Mesto, Molbi;

{$R *.dfm}

procedure TfrmKontakt.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2)  then
       MB_VRABOTEN.SetFocus;
    if (tag = 4) then
       ID_MOLBA.SetFocus;
    if tag = 5 then
       begin
         ADRESA.SetFocus;
       end;
    if Tag = 3 then
       begin
         ADRESA.SetFocus;
       end;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmKontakt.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2)  then
       MB_VRABOTEN.SetFocus;
    if (tag = 4)then
       ID_MOLBA.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if tag = 5 then
       begin
         dmSis.tblKontaktID_MOLBA.Value:=dmSis.tblMolbaID.Value;
         ADRESA.SetFocus;
       end;
    if Tag = 3 then
       begin
         dmSis.tblKontaktMB_VRABOTEN.Value:=dm.tblLicaVraboteniMB.Value;
         ADRESA.SetFocus;
       end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmKontakt.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if(Sender = email)then
    ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

procedure TfrmKontakt.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if(Sender = email)then
    ActivateKeyboardLayout($042F042F, KLF_REORDER);
end;

procedure TfrmKontakt.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
     VK_INSERT:begin
       if (Sender = DRZAVJANSTVO) or (Sender = DRZAVJANSTVONAZIV) then
          begin
            frmDrzavjanstvo:=TfrmDrzavjanstvo.Create(self, false);
            frmDrzavjanstvo.Tag:=1;
            frmDrzavjanstvo.ShowModal;
            frmDrzavjanstvo.Free;
            dmSis.tblKontaktDRZAVJANSTVO.Value:=dmSis.tblDrzavjanstvoID.Value;
          end;
       if (Sender = MESTO) or (Sender = MESTONAZIV)  then
          begin
            frmMesto:=TfrmMesto.Create(self, false);
            frmMesto.ShowModal;
            dmSis.tblKontaktMESTO.Value:=dmMat.tblMestoID.Value;
          end;
       if (Sender = DRZAVA) or (Sender = DRZAVANAZIV)  then
          begin
            frmDrzava:=TfrmDrzava.Create(self, false);
            frmDrzava.ShowModal;
            dmSis.tblKontaktDRZAVA.Value:=dmMat.tblDrzavaID.Value;
          end;
     end;
  end;
end;

procedure TfrmKontakt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if tag = 3 then
     begin
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.FullRefresh;

        dm.tblLicaVraboteni.close;
        dm.tblLicaVraboteni.ParamByName('firma').Value:=0;
        dm.tblLicaVraboteni.ParamByName('param').Value:=0;
        dm.tblLicaVraboteni.Open;

     end;
  if (tag = 5) and (frmMolbi.Tag = 0) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 1) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:=dmSis.tblOglasID.Value;
       dmSis.tblMolba.FullRefresh;
     end;
 // dmSis.tblKontakt.FullRefresh;
end;

procedure TfrmKontakt.FormCreate(Sender: TObject);
begin
   dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dmMat.tblMesto.Open;
  dmMat.tblDrzava.Open;
  dmSis.tblDrzavjanstvo.Open;
end;

procedure TfrmKontakt.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 2 then
     begin
       dmSis.tblKontakt.close;
       dmSis.tblKontakt.ParamByName('MB').Value:='%';
       dmSis.tblKontakt.ParamByName('MolbaID').Value:='0';
       dmSis.tblKontakt.open;

       dm.tblLica.close;
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.Open;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;
  if tag = 3 then
     begin
       dmSis.tblKontakt.close;
       dmSis.tblKontakt.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dmSis.tblKontakt.ParamByName('MolbaID').Value:='0';
       dmSis.tblKontakt.open;

       dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblLica.FullRefresh;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;
  if tag = 4 then
     begin
       dmSis.tblKontakt.close;
       dmSis.tblKontakt.ParamByName('MB').Value:='0';
       dmSis.tblKontakt.ParamByName('MolbaID').Value:='%';
       dmSis.tblKontakt.open;

       dmSis.tblMolba.Close;
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.Open;

       VRABOTENNAZIV.Visible:=False;
       MB_VRABOTEN.Visible:=False;
       Label11.Visible:=False;

       cxGrid1DBTableView1IMEPREZIME.Visible:=true;
       cxGrid1DBTableView1ID_MOLBA.Visible:=true;
       cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
       cxGrid1DBTableView1VRABOTENNAZIV.Visible:=false;

       ID_MOLBA.Tag:=1;
       IMEPREZIME.Tag:=1;
     end;
  if tag = 5 then
     begin
        dmSis.tblKontakt.close;
        dmSis.tblKontakt.ParamByName('MB').Value:='0';
        dmSis.tblKontakt.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblKontakt.open;

        dmSis.tblMolba.ParamByName('ID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblMolba.ParamByName('oglasID').Value:='%';
        dmSis.tblMolba.FullRefresh;

        VRABOTENNAZIV.Visible:=False;
        MB_VRABOTEN.Visible:=False;
        Label11.Visible:=False;

        cxGrid1DBTableView1IMEPREZIME.Visible:=true;
        cxGrid1DBTableView1ID_MOLBA.Visible:=true;
        cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
        cxGrid1DBTableView1VRABOTENNAZIV.Visible:=false;

        ID_MOLBA.Tag:=1;
        IMEPREZIME.Tag:=1;
     end;
end;

end.
