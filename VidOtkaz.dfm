inherited frmVidOtkaz: TfrmVidOtkaz
  Caption = #1055#1088#1080#1095#1080#1085#1080' '#1079#1072' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
  ClientHeight = 562
  ClientWidth = 700
  ExplicitWidth = 716
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 700
    Height = 315
    ExplicitWidth = 700
    ExplicitHeight = 315
    inherited cxGrid1: TcxGrid
      Width = 696
      Height = 311
      ExplicitWidth = 696
      ExplicitHeight = 311
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsVidOtkaz
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 90
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 637
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 441
    Width = 700
    Height = 98
    ExplicitTop = 432
    ExplicitWidth = 700
    ExplicitHeight = 98
    inherited Label1: TLabel
      Top = 7
      Visible = False
      ExplicitTop = 7
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 29
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsVidOtkaz
      TabOrder = 3
      Visible = False
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 609
      Top = 58
      ExplicitLeft = 609
      ExplicitTop = 58
    end
    inherited ZapisiButton: TcxButton
      Left = 528
      Top = 58
      ExplicitLeft = 528
      ExplicitTop = 58
    end
    object cxDBTextEdit1: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 26
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsVidOtkaz
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 452
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 700
    ExplicitWidth = 700
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 539
    Width = 700
    ExplicitTop = 539
    ExplicitWidth = 700
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40498.620915416660000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
