inherited frmSistematizacija: TfrmSistematizacija
  Caption = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
  ClientHeight = 548
  ClientWidth = 699
  ExplicitWidth = 707
  ExplicitHeight = 579
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 699
    Height = 265
    ExplicitWidth = 699
    ExplicitHeight = 265
    inherited cxGrid1: TcxGrid
      Width = 695
      Height = 261
      ExplicitWidth = 695
      ExplicitHeight = 261
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsSitematizacija
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 77
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 446
        end
        object cxGrid1DBTableView1OD_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'OD_DATUM'
          Width = 113
        end
        object cxGrid1DBTableView1DO_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DO_DATUM'
          Width = 118
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 391
    Width = 699
    Height = 134
    ExplicitTop = 391
    ExplicitWidth = 699
    ExplicitHeight = 134
    inherited Label1: TLabel
      Left = 561
      Visible = False
      ExplicitLeft = 561
    end
    object Label2: TLabel [1]
      Left = 59
      Top = 19
      Width = 61
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 40
      Top = 46
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1076' '#1076#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 7
      Top = 73
      Width = 113
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1086' '#1076#1072#1090#1091#1084'  :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label5: TLabel [4]
      Left = 20
      Top = 100
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 617
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsSitematizacija
      TabOrder = 1
      Visible = False
      ExplicitLeft = 617
    end
    inherited OtkaziButton: TcxButton
      Left = 608
      Top = 98
      TabOrder = 7
      ExplicitLeft = 608
      ExplicitTop = 98
    end
    inherited ZapisiButton: TcxButton
      Left = 527
      Top = 98
      TabOrder = 6
      ExplicitLeft = 527
      ExplicitTop = 98
    end
    object txtOpis: TcxDBTextEdit
      Tag = 1
      Left = 126
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmSis.dsSitematizacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 395
    end
    object txtDatumOd: TcxDBDateEdit
      Tag = 1
      Left = 126
      Top = 43
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'OD_DATUM'
      DataBinding.DataSource = dmSis.dsSitematizacija
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 128
    end
    object txtDatumDo: TcxDBDateEdit
      Left = 126
      Top = 70
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'DO_DATUM'
      DataBinding.DataSource = dmSis.dsSitematizacija
      Enabled = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 128
    end
    object txtDokument: TcxDBTextEdit
      Left = 126
      Top = 97
      Hint = 
        #1051#1086#1082#1072#1094#1080#1112#1072#1090#1072' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1090' '#1074#1086' '#1082#1086#1112' '#1089#1077' '#1086#1087#1080#1096#1072#1085#1080' '#1095#1083#1077#1085#1086#1074#1080#1090#1077' '#1086#1076' '#1089#1080#1089#1090#1077#1084#1072#1090 +
        #1080#1079#1072#1094#1080#1112#1072#1090#1072
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'DOKUMENT'
      DataBinding.DataSource = dmSis.dsSitematizacija
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 4
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 266
    end
    object cxButton1: TcxButton
      Left = 392
      Top = 71
      Width = 129
      Height = 21
      Anchors = [akRight, akBottom]
      Caption = #1053#1072#1112#1076#1080' '#1083#1086#1082#1072#1094#1080#1112#1072
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      OptionsImage.Glyph.Data = {
        360C0000424D360C000000000000360000002800000020000000200000000100
        180000000000000C0000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDB7300DB7300FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBBA62FAA639E77F02D36B01FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCA21FFCC66FFFB24BE77F02D36B
        01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B11FBBA62FAA639E77F
        02CB6501FFFFFFD36B01D36B01D36B01FD7500E77F02FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B11FBBA62E587
        12BE5C00BE5C00CC7439C58776DD898ADD898ADB8A4FFD7500F58900FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE58712BE5C
        00CB6501C36E66C36E66C36E66D28A83DD898ADD898ADD898ADB8A4FF58900FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBE5C
        009C6665A77472C36E669C6665C36E66C36E66C36E66DD898ADD898AF58900FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD36B018F52
        529C6665B3836B9C66658F5252B954479C6665C36E66DD898ADD898ADB8A4FF5
        8900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD36B018F52
        52A77472C19A838F52528F52528F52528F52529C6665C36E66DD898ADD898AE7
        7F02FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE77F028F52
        52C58776D7B5A0A774728F52528F5252A5483D8F5252C36E66C36E66DD898AFD
        7500FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE77F028F52
        52C19A83EBD4C1DAB9AD9C66658F52528F52528F5252A77472C36E66D28A83DB
        7300FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF589008F52
        529C6665EFDDCAFFF6E0CCA8959C66659C66658F5252B95447C36E66CC7439D3
        6B01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF589
        008F5252B3836BEFDDCAEFDDCACCA895A774728F52529C6665C36E66CB6501FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF589
        009C66658F52529C6665C58776B3836B9C66659C66659C6665CC7439BE5C00FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF58900FD75009C66659C66658F52528F52529C6665BE5C00BE5C00FFF9F1FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFE77F02DB7300D36B01CB6501CB6501FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 5
      Visible = False
      OnClick = cxButton1Click
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 699
    ExplicitWidth = 699
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 525
    Width = 699
    ExplicitTop = 525
    ExplicitWidth = 699
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FloatClientWidth = 131
      FloatClientHeight = 104
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 236
      FloatClientWidth = 111
      FloatClientHeight = 126
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 606
      FloatClientHeight = 104
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112
      CaptionButtons = <>
      DockedLeft = 481
      DockedTop = 0
      FloatLeft = 726
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aZatvoriSistematizacija
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aPregledajSistematizacija
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aPregledajSistematizacija
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aSistematizacijaMestaSlobodni
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aSablonZaSistematizacija
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aSistematizacijaFR
      Category = 0
      ImageIndex = 19
    end
    object dxBarButton5: TdxBarButton
      Action = aSistematizacijaFR
      Category = 0
      ImageIndex = 19
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton6: TdxBarButton
      Caption = 'Action1'
      Category = 0
      Visible = ivAlways
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aZatvoriSistematizacija: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1057#1080#1089'.'
      ImageIndex = 6
      OnExecute = aZatvoriSistematizacijaExecute
    end
    object aPregledajSistematizacija: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
      ImageIndex = 19
      OnExecute = aPregledajSistematizacijaExecute
    end
    object aSistematizacijaMestaSlobodni: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1087#1086#1087#1086#1083#1085#1077#1090#1080' '#1080' '#1085#1077#1087#1086#1087#1086#1083#1085#1077#1090#1080' '#1084#1077#1089#1090#1072
      ImageIndex = 19
      OnExecute = aSistematizacijaMestaSlobodniExecute
    end
    object aSablonZaSistematizacija: TAction
      Caption = 'aSablonZaSistematizacija'
      OnExecute = aSablonZaSistematizacijaExecute
    end
    object aSistematizacijaFR: TAction
      Caption = #1055#1088#1072#1074#1080#1083#1080#1085#1080#1082' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080#1090#1077' '#1084#1077#1089#1090#1072
      OnExecute = aSistematizacijaFRExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40254.505882476850000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 592
    Top = 152
  end
  object OpenDialog1: TOpenDialog
    InitialDir = 'C:\'
    Left = 512
    Top = 177
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 640
    Top = 96
  end
  object tblPosSis: TpFIBDataSet
    RefreshSQL.Strings = (
      'select count(*) ima'
      'from hr_sistematizacija hs'
      'where hs.do_datum is null and hs.id_re_firma=:firma')
    SelectSQL.Strings = (
      'select count(*) ima'
      'from hr_sistematizacija hs'
      'where hs.do_datum is null and hs.id_re_firma=:firma')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 640
    Top = 216
    oRefreshDeletedRecord = True
    object tblPosSisIMA: TFIBIntegerField
      FieldName = 'IMA'
    end
  end
end
