unit ScanDocument;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinOffice2007Blue, dxSkinsdxStatusBarPainter, dxStatusBar,
  ExtCtrls, ieview, imageenview, cxContainer, cxEdit, cxMemo, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel, Menus, StdCtrls, cxButtons,
  ActnList, dximctrl, cxGroupBox, dxGDIPlusClasses, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, DB, imageen, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue,imageenio, iemview, iemio, Imageenproc, StrUtils,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, System.Actions;

type
  TfrmScanDocument = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    dxStatusBar1: TdxStatusBar;
    ImageEnView1: TImageEnView;
    cxLabel4: TcxLabel;
    cboxSource: TcxComboBox;
    cxButton1: TcxButton;
    ActionList1: TActionList;
    aScan: TAction;
    aExit: TAction;
    gbPodatoci: TcxGroupBox;
    mOpis: TcxDBMemo;
    cxLabel1: TcxLabel;
    aZapisi: TAction;
    btnIzvrsi: TcxButton;
    cxLabel3: TcxLabel;
    txtNaziv: TcxDBTextEdit;
    ImageEnMView1: TImageEnMView;
    ImageEnMIO1: TImageEnMIO;
    Panel3: TPanel;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton3: TcxButton;
    aShiftRight: TAction;
    aShiftLeft: TAction;
    aRotiraj: TAction;
    aProveriNaziv: TAction;
    Label8: TLabel;
    MB: TcxDBTextEdit;
    VRABOTENIME: TcxDBLookupComboBox;
    Label1: TLabel;
    cxVidDokument: TcxDBLookupComboBox;
    Label2: TLabel;
    cxGodina: TcxDBComboBox;
    procedure aScanExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aExitExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImageEnMView1ImageSelect(Sender: TObject; idx: Integer);
    procedure aShiftLeftExecute(Sender: TObject);
    procedure aShiftRightExecute(Sender: TObject);
    procedure aRotirajExecute(Sender: TObject);
    procedure txtNazivEnter(Sender: TObject);
    procedure txtNazivExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure aProveriNazivExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);

  private
    { Private declarations }

    LFiles: TStringList;  //Used in merge
  public
    { Public declarations }
    pateka : string;
    prv_pat : boolean;
    pravilen : boolean;
  end;

var
  frmScanDocument: TfrmScanDocument;

implementation

uses Utils, dmResources, dmUnit, dmUnitOtsustvo, dmKonekcija;

{$R *.dfm}

procedure TfrmScanDocument.aExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmScanDocument.aProveriNazivExecute(Sender: TObject);
var
  tekst : AnsiString;
begin
  // Proverka dali ima nekoi specijalni karakteri vo nazivot
  pravilen := true;
  tekst := txtNaziv.Text;

  if AnsiContainsStr(tekst, '/') then pravilen := false;
  if AnsiContainsStr(tekst, '<') then pravilen := false;
  if AnsiContainsStr(tekst, '>') then pravilen := false;
  if AnsiContainsStr(tekst, '\') then pravilen := false;
  if AnsiContainsStr(tekst, '"') then pravilen := false;
  if AnsiContainsStr(tekst, ':') then pravilen := false;
  if AnsiContainsStr(tekst, '|') then pravilen := false;
  if AnsiContainsStr(tekst, '*') then pravilen := false;
  if AnsiContainsStr(tekst, '?') then pravilen := false;
end;

procedure TfrmScanDocument.aRotirajExecute(Sender: TObject);
begin
 // ImageEnMView1.Proc.Rotate(90,true,TIEAntialiasMode.ierFast,-1);
  ImageEnMView1.Proc.Rotate(90,true);
  ImageEnView1.Fit;
end;

procedure TfrmScanDocument.aScanExecute(Sender: TObject);
begin
// ������� �� ����������
  if (cboxSource.ItemIndex <> -1) then ImageEnView1.IO.TWainParams.SelectedSource := cboxSource.ItemIndex;

  ImageEnMIO1.Acquire();

  gbPodatoci.SetFocus;
end;

procedure TfrmScanDocument.aShiftLeftExecute(Sender: TObject);
begin
  if (ImageEnMView1.SelectedImage > 0) then
  begin
    ImageEnMView1.MoveImage(ImageEnMView1.SelectedImage, ImageEnMView1.SelectedImage - 1);
    ImageEnMView1.SelectedImage := ImageEnMView1.SelectedImage - 1;
  end;
end;

procedure TfrmScanDocument.aShiftRightExecute(Sender: TObject);
begin
  if (ImageEnMView1.SelectedImage < ImageEnMView1.ImageCount - 1) then
  begin
    ImageEnMView1.MoveImage(ImageEnMView1.SelectedImage, ImageEnMView1.SelectedImage + 1);
    ImageEnMView1.SelectedImage := ImageEnMView1.SelectedImage + 1;
  end;
end;

procedure TfrmScanDocument.aZapisiExecute(Sender: TObject);
var
  stream: TStream;
begin
    //kreiraj nov pdf dokument
    if (Validacija(Panel1) = false) then
    begin
      aProveriNaziv.Execute;
      if (not pravilen) then
      begin
        ShowMessage('�� ������� �� ���������� �� �� ��������� ������� : \ / < > | : " ? * ');
        txtNaziv.SetFocus;
      end
      else
      begin
        ImageEnMIO1.Params[0].PDF_Compression := ioPDF_JPEG;
        ImageEnMIO1.SaveToFilePDF(pat_dokumenti + '\' + txtNaziv.Text + '.pdf');
        dm.tblSkeniraniDokumentiPATEKA.Value := pat_dokumenti+ '\' + txtNaziv.Text + '.pdf';
        //dm.tblSkeniraniDokumentiVID_DOKUMENT.Value:=1;
       //dm.tblSkeniraniDokumentiMB.Value:='1103986488003';
        dm.tblSkeniraniDokumenti.Post;
        ShowMessage('����������� � ������� �������� �� ������!');
        Close;
      end;

    end;

end;

procedure TfrmScanDocument.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dm.tblSkeniraniDokumenti.State = dsInsert then dm.tblSkeniraniDokumenti.Cancel;
end;

procedure TfrmScanDocument.FormCreate(Sender: TObject);
var
  i: integer;
begin
  // fills TWain sources
  for i := 0 to ImageEnView1.IO.TWainParams.SourceCount - 1 do
    cboxSource.Properties.Items.Add(ImageEnView1.IO.TWainParams.SourceName[i]);
  // Select first scanner
  cboxSource.ItemIndex := 0;
end;

procedure TfrmScanDocument.FormResize(Sender: TObject);
begin
  ImageEnView1.Fit;
end;

procedure TfrmScanDocument.FormShow(Sender: TObject);
begin
    ImageEnView1.Clear;
    ImageEnMView1.Clear;

    pravilen := true;

    dm.tblSkeniraniDokumenti.Open;

     dm.tblSkeniraniDokumenti.Insert;
     cxGodina.EditValue:=dmKon.godina;
end;

procedure TfrmScanDocument.ImageEnMView1ImageSelect(Sender: TObject; idx: Integer);
begin
  ImageEnView1.Assign(ImageEnMView1.Bitmap);
  ImageEnView1.Fit;
end;

procedure TfrmScanDocument.txtNazivEnter(Sender: TObject);
begin
// if (dm.tblSkeniraniDokumentiNAZIV.IsNull) then
// begin
//    dm.tblSkeniraniDokumentiNAZIV.Value:=clBoxDokument.Text;
// end;

end;

procedure TfrmScanDocument.txtNazivExit(Sender: TObject);
begin
  aProveriNaziv.Execute;
  if (not pravilen) then
  begin
    ShowMessage('�� ������� �� ���������� �� �� ��������� �������: \ / < > | : " ? * ');
    txtNaziv.SetFocus;
  end;

end;

procedure TfrmScanDocument.EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;

end;

procedure TfrmScanDocument.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmScanDocument.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

end.
