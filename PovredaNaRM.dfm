inherited frmPovredaNaRM: TfrmPovredaNaRM
  Anchors = [akRight, akBottom]
  Caption = #1055#1086#1074#1088#1077#1076#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
  ClientHeight = 744
  ClientWidth = 1027
  ExplicitLeft = -63
  ExplicitTop = -103
  ExplicitWidth = 1043
  ExplicitHeight = 782
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1027
    Height = 274
    ExplicitWidth = 1027
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Width = 1023
      Height = 270
      ExplicitWidth = 1023
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPovredaRM
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1RABEDINICANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABEDINICANAZIV'
          SortIndex = 0
          SortOrder = soAscending
          Width = 158
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 105
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 76
        end
        object cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 94
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077
          DataBinding.FieldName = 'DATUM'
          Width = 89
        end
        object cxGrid1DBTableView1TIP_POVREDA: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_POVREDA'
          Visible = False
        end
        object cxGrid1DBTableView1TIPPOVREDANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TIPPOVREDANAZIV'
          Width = 202
        end
        object cxGrid1DBTableView1ID_POVREDA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_POVREDA'
          Visible = False
        end
        object cxGrid1DBTableView1POVREDAOPIS: TcxGridDBColumn
          DataBinding.FieldName = 'POVREDAOPIS'
          Width = 145
        end
        object cxGrid1DBTableView1BROJ_POVREDENI: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_POVREDENI'
          Width = 102
        end
        object cxGrid1DBTableView1BROJ_ZAGINATI: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_ZAGINATI'
          Width = 95
        end
        object cxGrid1DBTableView1KOLKU_PATI: TcxGridDBColumn
          Caption = #1050#1086#1083#1082#1091' '#1087#1072#1090#1080' '#1089#1077' '#1089#1083#1091#1095#1080#1083#1072' '#1085#1077#1089#1088#1077#1116#1072' '#1085#1072' '#1086#1074#1072' '#1084#1077#1089#1090#1086
          DataBinding.FieldName = 'KOLKU_PATI'
          Width = 89
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          Caption = #1054#1087#1080#1089' '#1085#1072' '#1085#1077#1089#1088#1077#1116#1072
          DataBinding.FieldName = 'OPIS'
          Width = 149
        end
        object cxGrid1DBTableView1SMRTONOSNA: TcxGridDBColumn
          DataBinding.FieldName = 'SMRTONOSNA'
          Visible = False
        end
        object cxGrid1DBTableView1SmrtoNosnaNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'SmrtoNosnaNaziv'
          Width = 72
        end
        object cxGrid1DBTableView1ZAGROZENO_RM: TcxGridDBColumn
          DataBinding.FieldName = 'ZAGROZENO_RM'
          Visible = False
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'zagrozenoNaziv'
          Width = 81
        end
        object cxGrid1DBTableView1NAREDBA: TcxGridDBColumn
          DataBinding.FieldName = 'NAREDBA'
          Visible = False
        end
        object cxGrid1DBTableView1NaredbaNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'NaredbaNaziv'
          Width = 71
        end
        object cxGrid1DBTableView1BEZBEDNOST: TcxGridDBColumn
          DataBinding.FieldName = 'BEZBEDNOST'
          Visible = False
        end
        object cxGrid1DBTableView1bezbednostNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'bezbednostNaziv'
          Width = 69
        end
        object cxGrid1DBTableView1KORISTENO: TcxGridDBColumn
          DataBinding.FieldName = 'KORISTENO'
          Visible = False
        end
        object cxGrid1DBTableView1KoristenoNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'KoristenoNaziv'
          Width = 78
        end
        object cxGrid1DBTableView1MERKI: TcxGridDBColumn
          DataBinding.FieldName = 'MERKI'
          Visible = False
        end
        object cxGrid1DBTableView1merkiNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'merkiNaziv'
          Width = 68
        end
        object cxGrid1DBTableView1ID_OSNOV_OSIGURUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_OSNOV_OSIGURUVANJE'
          Visible = False
        end
        object cxGrid1DBTableView1OSNOVNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OSNOVNAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Width = 1027
    Height = 321
    ExplicitTop = 400
    ExplicitWidth = 1027
    ExplicitHeight = 321
    inherited Label1: TLabel
      Left = 973
      Top = 17
      Visible = False
      ExplicitLeft = 973
      ExplicitTop = 17
    end
    object Label11: TLabel [1]
      Left = 47
      Top = 6
      Width = 93
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label12: TLabel [2]
      Left = 24
      Top = 41
      Width = 116
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [3]
      Left = 40
      Top = 68
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1058#1080#1087' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [4]
      Left = 32
      Top = 95
      Width = 108
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [5]
      Left = 431
      Top = 234
      Width = 154
      Height = 13
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = #1054#1089#1085#1086#1074' '#1079#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [6]
      Left = 21
      Top = 122
      Width = 119
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1077#1085#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [7]
      Left = 29
      Top = 149
      Width = 111
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1079#1072#1075#1080#1085#1072#1090#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [8]
      Left = 10
      Top = 171
      Width = 130
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1085#1077#1089#1088#1077#1116#1080' '#1085#1072' '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel [9]
      Left = 40
      Top = 201
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' '#1085#1072' '#1085#1077#1089#1088#1077#1116#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [10]
      Left = 455
      Top = 245
      Width = 130
      Height = 13
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = ' '#1086#1089#1080#1075#1091#1088#1091#1074#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel [11]
      Left = 10
      Top = 181
      Width = 130
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1086#1074#1072' '#1084#1077#1089#1090#1086' '#1080' '#1088#1072#1073#1086#1090#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 1029
      Top = 14
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPovredaRM
      TabOrder = 3
      Visible = False
      ExplicitLeft = 1029
      ExplicitTop = 14
    end
    inherited OtkaziButton: TcxButton
      Left = 882
      Top = 281
      TabOrder = 21
      ExplicitLeft = 882
      ExplicitTop = 281
    end
    inherited ZapisiButton: TcxButton
      Left = 801
      Top = 281
      TabOrder = 20
      ExplicitLeft = 801
      ExplicitTop = 281
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Tag = 1
      Left = 146
      Top = 11
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 243
      Top = 11
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      ShowHint = True
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 217
    end
    object DATUM: TcxDBDateEdit
      Tag = 1
      Left = 146
      Top = 38
      Hint = #1044#1072#1090#1091#1084' '#1080' '#1074#1088#1077#1084#1077' '#1082#1086#1075#1072' '#1089#1077' '#1089#1083#1091#1095#1080#1083#1072' '#1087#1086#1074#1088#1077#1076#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.Kind = ckDateTime
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 151
    end
    object TIP_POVREDA: TcxDBTextEdit
      Tag = 1
      Left = 146
      Top = 65
      Hint = #1058#1080#1087' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1072' - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'TIP_POVREDA'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = TIP_POVREDAExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object TIP_POVREDA_NAziv: TcxDBLookupComboBox
      Tag = 1
      Left = 243
      Top = 65
      Hint = #1058#1080#1087' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1072' - '#1085#1072#1079#1080#1074
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'TIP_POVREDA'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dm.dsTipPovreda
      ShowHint = True
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = TIP_POVREDA_NAzivExit
      OnKeyDown = EnterKakoTab
      Width = 217
    end
    object ID_POVREDA: TcxDBTextEdit
      Left = 146
      Top = 92
      Hint = #1055#1086#1074#1088#1077#1076#1072' - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_POVREDA'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object ID_POVREDA_naziv: TcxDBLookupComboBox
      Left = 243
      Top = 92
      Hint = #1055#1086#1074#1088#1077#1076#1072' - '#1086#1087#1080#1089
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'ID_POVREDA'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'OPIS'
        end>
      Properties.ListSource = dm.dsPovreda
      ShowHint = True
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 217
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 512
      Top = 9
      Anchors = [akRight, akBottom]
      Caption = #1044#1072#1083#1080' '#1087#1086#1074#1088#1077#1076#1072#1090#1072' '#1077' '#1089#1084#1088#1090#1086#1085#1086#1089#1085#1072
      DataBinding.DataField = 'SMRTONOSNA'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 12
      Height = 49
      Width = 209
    end
    object ID_OSNOV_OSIGURUVANJE: TcxDBTextEdit
      Left = 591
      Top = 236
      Hint = #1054#1089#1085#1086#1074' '#1085#1072' '#1086#1089#1080#1075#1091#1088#1091#1074#1072#1114#1077' - '#1096#1080#1092#1088#1072
      Anchors = [akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'ID_OSNOV_OSIGURUVANJE'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 18
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object ID_OSNOV_OSIGURUVANJE_naziv: TcxDBLookupComboBox
      Left = 688
      Top = 236
      Hint = #1054#1089#1085#1086#1074' '#1079#1072' '#1086#1089#1080#1075#1091#1088#1091#1074#1072#1114#1077
      Anchors = [akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'ID_OSNOV_OSIGURUVANJE'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'BROJ'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsOsnovNaOsiguruvanje
      ShowHint = True
      TabOrder = 19
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 273
    end
    object BROJ_POVREDENI: TcxDBTextEdit
      Left = 146
      Top = 119
      Hint = 
        #1050#1086#1083#1082#1091' '#1074#1082#1091#1087#1085#1086' '#1083#1080#1094#1072' '#1089#1077' '#1087#1086#1074#1088#1077#1076#1077#1085#1080' '#1074#1086' '#1087#1086#1074#1088#1077#1076#1072#1090#1072' '#1087#1088#1080' '#1088#1072#1073#1086#1090#1072' ('#1079#1072#1077#1076#1085#1086' '#1089 +
        #1086' '#1079#1072#1075#1080#1085#1072#1090#1080#1090#1077
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ_POVREDENI'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object BROJ_ZAGINATI: TcxDBTextEdit
      Left = 146
      Top = 146
      Hint = 
        #1050#1086#1083#1082#1091' '#1083#1080#1094#1072' '#1079#1072#1075#1080#1085#1072#1083#1077' '#1085#1072' '#1084#1077#1089#1090#1086#1090#1086' '#1085#1072' '#1085#1077#1089#1088#1077#1116#1072#1090#1072', '#1086#1076#1085#1086#1089#1085#1086' '#1091#1084#1088#1077#1083#1077' '#1085#1072' '#1087 +
        #1072#1090#1086#1090' '#1076#1086' '#1079#1076#1088#1072#1074#1089#1090#1074#1077#1085#1072#1090#1072' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1112#1072
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ_ZAGINATI'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object KOLKU_PATI: TcxDBTextEdit
      Left = 146
      Top = 173
      Hint = 
        #1044#1072#1083#1080' '#1089#1083#1080#1095#1085#1072' '#1085#1077#1089#1088#1077#1116#1072' '#1085#1072#1089#1090#1072#1085#1072#1083#1072' '#1087#1086#1088#1072#1085#1086' '#1085#1072' '#1080#1089#1090#1086#1090#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1080' '#1085 +
        #1072' '#1080#1089#1090#1072#1090#1072' '#1088#1072#1073#1086#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'KOLKU_PATI'
      DataBinding.DataSource = dm.dsPovredaRM
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 10
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object OPIS: TcxDBMemo
      Left = 146
      Top = 200
      Hint = #1048#1079#1074#1086#1088' '#1085#1072' '#1085#1077#1089#1088#1077#1116#1072#1090#1072', '#1087#1088#1080#1095#1080#1085#1080' '#1079#1072' '#1085#1077#1089#1088#1077#1116#1072#1090#1072
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.WantReturns = False
      TabOrder = 11
      OnDblClick = OPISDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 57
      Width = 314
    end
    object cxDBRadioGroup2: TcxDBRadioGroup
      Left = 727
      Top = 9
      Hint = #1044#1072#1083#1080' '#1077' '#1091#1090#1074#1088#1076#1077#1085#1086' '#1076#1077#1082#1072' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086' '#1073#1080#1083' '#1079#1072#1075#1088#1086#1079#1077#1085
      Anchors = [akRight, akBottom]
      Caption = #1044#1072#1083#1080' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1073#1080#1083' '#1079#1072#1075#1088#1086#1079#1077#1085' '#1085#1072' '#1056#1052
      DataBinding.DataField = 'ZAGROZENO_RM'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 13
      Height = 49
      Width = 234
    end
    object cxDBRadioGroup3: TcxDBRadioGroup
      Left = 512
      Top = 64
      Hint = 
        #1044#1072#1083#1080' '#1077' '#1085#1072#1088#1077#1076#1077#1085#1086' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1083#1080#1095#1085#1080#1090#1077' '#1079#1072#1096#1090#1080#1090#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1080' '#1083#1080#1095#1085#1072#1090#1072 +
        ' '#1079#1072#1096#1090#1080#1090#1085#1072' '#1086#1087#1088#1077#1084#1072
      Anchors = [akRight, akBottom]
      Caption = #1044#1072#1083#1080' '#1073#1080#1083#1072' '#1080#1079#1074#1088#1096#1077#1085#1072' '#1085#1072#1088#1077#1076#1073#1072' '#1079#1072' '#1085#1086#1089#1077#1114#1077' '#1085#1072' '#1079#1072#1096#1090#1080#1090#1085#1072' '#1086#1087#1088#1077#1084#1072
      DataBinding.DataField = 'NAREDBA'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 14
      Height = 49
      Width = 449
    end
    object cxDBRadioGroup4: TcxDBRadioGroup
      Left = 512
      Top = 174
      Hint = 
        #1044#1072#1083#1080' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1077#1085#1080' '#1083#1080#1095#1085#1080#1090#1077' '#1079#1072#1096#1090#1080#1090#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1080' '#1083#1080#1095#1085#1072#1090#1072' '#1079#1072#1096#1090#1080#1090#1085#1072' '#1086 +
        #1087#1088#1077#1084#1072
      Anchors = [akRight, akBottom]
      Caption = #1044#1072#1083#1080' '#1077' '#1082#1086#1088#1080#1089#1090#1077#1085#1072' '#1086#1087#1088#1077#1084#1072' '#1079#1072' '#1079#1072#1096#1090#1080#1090#1072
      DataBinding.DataField = 'KORISTENO'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 16
      Height = 49
      Width = 209
    end
    object cxDBRadioGroup5: TcxDBRadioGroup
      Left = 727
      Top = 174
      Hint = 
        #1044#1072#1083#1080' '#1089#1077' '#1087#1088#1080#1084#1077#1085#1077#1090#1080' '#1086#1087#1096#1090#1080#1090#1077' '#1080' '#1087#1086#1089#1077#1073#1085#1080#1090#1077' '#1084#1077#1088#1082#1080' '#1079#1072' '#1079#1072#1096#1090#1080#1090#1072' '#1087#1088#1080' '#1088#1072#1073#1086#1090 +
        #1072
      Anchors = [akRight, akBottom]
      Caption = #1044#1072#1083#1080' '#1089#1077' '#1087#1088#1077#1074#1079#1077#1084#1077#1085#1080' '#1089#1080#1090#1077' '#1084#1077#1088#1082#1080' '#1079#1072' '#1079#1072#1096#1090#1080#1090#1072
      DataBinding.DataField = 'MERKI'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 17
      Height = 49
      Width = 234
    end
    object cxDBRadioGroup6: TcxDBRadioGroup
      Left = 512
      Top = 119
      Hint = 
        #1044#1072#1083#1080' '#1073#1080#1083#1077' '#1086#1073#1077#1079#1073#1077#1076#1077#1085#1080' '#1089#1080#1090#1077' '#1087#1086#1090#1088#1077#1073#1085#1080' '#1083#1080#1095#1085#1080' '#1079#1072#1096#1090#1080#1090#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1080' '#1079#1072#1096 +
        #1090#1080#1090#1085#1072' '#1086#1087#1088#1077#1084#1072
      Anchors = [akRight, akBottom]
      Caption = #1044#1072#1083#1080' '#1073#1080#1083#1077' '#1086#1073#1077#1079#1073#1077#1076#1077#1085#1080' '#1089#1080#1090#1077' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1080' '#1086#1087#1088#1077#1084#1072' '#1079#1072' '#1079#1072#1096#1090#1080#1090#1072
      DataBinding.DataField = 'BEZBEDNOST'
      DataBinding.DataSource = dm.dsPovredaRM
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 15
      Height = 49
      Width = 449
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1027
    ExplicitWidth = 1027
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 721
    Width = 1027
    ExplicitTop = 721
    ExplicitWidth = 1027
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 264
  end
  inherited PopupMenu1: TPopupMenu
    Left = 368
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Left = 432
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40456.576282002320000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 648
    Top = 240
  end
end
