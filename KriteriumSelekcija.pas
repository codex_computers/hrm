{*******************************************************}
{                                                       }
{     ��������� : ������ ��������
                  ������ �������                       }
{                                                       }
{     ����� : 16.12.2011                                }
{                                                       }
{     ������: 1.1.1.17                                  }
{                                                       }
{*******************************************************}

unit KriteriumSelekcija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxHint, cxGroupBox, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip, dxCustomHint;

type
//  niza = Array[1..5] of Variant;

  TfrmKriteriumSelekcija = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    lPanel: TPanel;
    dPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Label4: TLabel;
    ID_MOLBA: TcxDBTextEdit;
    ID_MOLBANaziv: TcxDBLookupComboBox;
    Label2: TLabel;
    txtInformacii: TcxDBTextEdit;
    Label5: TLabel;
    Label6: TLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_TIP_KS: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MAX_POENI: TcxGridDBColumn;
    cxGrid1DBTableView1INFORMACII: TcxGridDBColumn;
    cxGrid1DBTableView1DODELENI_POENI: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    txtDodPoeni: TcxDBTextEdit;
    txtZabeleska: TcxDBMemo;
    cxHintStyleController1: TcxHintStyleController;
    cxGroupBox1: TcxGroupBox;
    txtMaxPoeni: TcxDBTextEdit;
    Label3: TLabel;
    Label1: TLabel;
    txtTipKS: TcxDBTextEdit;
    cbTipKS: TcxDBLookupComboBox;
    aFormProcenka: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    aDizajnerFP: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
   // procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure ID_MOLBANazivPropertiesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure txtMaxPoeniEnter(Sender: TObject);
    procedure aFormProcenkaExecute(Sender: TObject);
    procedure aDizajnerFPExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmKriteriumSelekcija: TfrmKriteriumSelekcija;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmMaticni,
  dmSistematizacija, dmSystem, dmUnit, dmUnitOtsustvo, dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmKriteriumSelekcija.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmKriteriumSelekcija.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmKriteriumSelekcija.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmKriteriumSelekcija.aBrisiExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmKriteriumSelekcija.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmKriteriumSelekcija.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmKriteriumSelekcija.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmKriteriumSelekcija.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmKriteriumSelekcija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmKriteriumSelekcija.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmKriteriumSelekcija.cxDBTextEditAllExit(Sender: TObject);
begin
    if ((Sender=TEdit(txtTipKS)) or (Sender=TEdit(cbTipKS))) and (txtTipKS.Text<>'') then
    begin
     if dmSis.tblTipKS.Locate('ID',VarArrayOf([txtTipKS.Text]),[]) then
        txtMaxPoeni.Text:=dmSis.tblTipKSMAX_POENI.AsString;
    end;
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmKriteriumSelekcija.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmKriteriumSelekcija.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmKriteriumSelekcija.txtMaxPoeniEnter(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmKriteriumSelekcija.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmKriteriumSelekcija.ID_MOLBANazivPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmKriteriumSelekcija.prefrli;
begin
end;

procedure TfrmKriteriumSelekcija.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmKriteriumSelekcija.FormCreate(Sender: TObject);
begin
  dmsis.tblKS.Close;
  dmSis.tblks.ParamByName('firma').AsInteger:=firma;
  dmsis.tblKS.open;

  dmSis.tblMolba.Close;
  dmSis.tblMolba.ParamByName('ID').Value:='%';
  dmSis.tblMolba.ParamByName('oglasID').Value:='%';
  dmSis.tblMolba.Open;

  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmKriteriumSelekcija.FormShow(Sender: TObject);
begin
//	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;
//------------------------------------------------------------------------------

procedure TfrmKriteriumSelekcija.SaveToIniFileExecute(Sender: TObject);
begin
cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmKriteriumSelekcija.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmKriteriumSelekcija.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
     begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
       lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmKriteriumSelekcija.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmKriteriumSelekcija.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmKriteriumSelekcija.aPecatiTabelaExecute(Sender: TObject);
begin
 dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmKriteriumSelekcija.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmKriteriumSelekcija.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmKriteriumSelekcija.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;
procedure TfrmKriteriumSelekcija.aDizajnerFPExecute(Sender: TObject);
begin
    dmReport.Spremi(30083,dmKon.aplikacija);
    dmReport.tblSqlReport.Params.ParamByName('molba').Asinteger:=dmsis.tblKSID_MOLBA.Value;; //tblListaTuzbiID.AsString;
    dmReport.tblSqlReport.Open;

    dmReport.frxReport1.DesignReport();
end;

procedure TfrmKriteriumSelekcija.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmKriteriumSelekcija.aFormProcenkaExecute(Sender: TObject);
begin
    dmReport.Spremi(30083,dmKon.aplikacija);
    dmReport.tblSqlReport.Params.ParamByName('molba').Asinteger:=dmsis.tblKSID_MOLBA.Value;; //tblListaTuzbiID.AsString;
    dmReport.tblSqlReport.Open;

    dmReport.frxReport1.ShowReport();
end;

procedure TfrmKriteriumSelekcija.aHelpExecute(Sender: TObject);
begin
     Application.HelpContext(134);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmKriteriumSelekcija.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmKriteriumSelekcija.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
