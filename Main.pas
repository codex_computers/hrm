{*******************************************************}
{                                                       }
{     ��������� : ������ ��������
                  ������ �������                       }
{                                                       }
{     ����� : 03.03.2010                                }
{                                                       }
{     ������: 1.0.0.0                                  }
{                                                       }
{*******************************************************}

unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsdxBarPainter, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, cxClasses, dxRibbon, cxControls, dxBar, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy,
  dxSkinSeven, dxSkinSharp, ActnList, dxStatusBar, dxRibbonStatusBar, ExtCtrls,
  ComCtrls, dxtree, dxdbtree, dxorgchr, dxdborgc,
  dxNavBar, dxNavBarCollns, dxNavBarBase, cxStyles, cxEdit, cxColorComboBox, DB,
  FIBDataSet, cxVGrid, cxDBVGrid, cxInplaceContainer, pFIBDataSet, cxTextEdit,
  dxNavBarGroupItems, jpeg, cxContainer, cxLabel, cxDropDownEdit, cxCheckBox,
  cxBarEditItem, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxDBLookupComboBox, cxRadioGroup, cxSplitter,
  cxCheckGroup, cxCheckComboBox, cxHyperLinkEdit, dxRibbonSkins,
  dxSkinsdxNavBarPainter, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinOffice2013White, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxRibbonCustomizationForm,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxSkinsdxNavBarAccordionViewPainter, dxSkinscxPCPainter, dxGDIPlusClasses;

type
  TfrmMain = class(TForm)
    dxBarManager1: TdxBarManager;
    rtSistematizacija: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    rtRegrutiranjeSelekcija: TdxRibbonTab;
    rtKariera: TdxRibbonTab;
    rtOtsustva: TdxRibbonTab;
    ActionList1: TActionList;
    aRe: TAction;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    aGrupaRM: TAction;
    aVidVrabotuvanje: TAction;
    aRM: TAction;
    aGrupaPlata: TAction;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    ActionList2: TActionList;
    aNacionalnost: TAction;
    aVeroispoved: TAction;
    aPraznici: TAction;
    aTipOtsustvo: TAction;
    dxBarManager1Bar2: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton6: TdxBarLargeButton;
    aIzlez: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    Panel1: TPanel;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    aPlanZaOtsustva: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    aSistematizacija: TAction;
    dxBarLargeButton14: TdxBarLargeButton;
    aRMRE: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aObrazovanie: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    aEvidencijaOtsustva: TAction;
    rtSifrarnici: TdxRibbonTab;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    aGrupaStatus: TAction;
    aStatus: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aDrzavjanstvo: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    aStranskiJazici: TAction;
    VidStranskiJazik: TdxBarButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    dxBarManager1Bar9: TdxBar;
    aOglas: TAction;
    Oglas: TdxBarButton;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarManager1Bar10: TdxBar;
    dxBarLargeButton30: TdxBarLargeButton;
    aObuki: TAction;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton31: TdxBarLargeButton;
    aVraboteni: TAction;
    dxBarLargeButton32: TdxBarLargeButton;
    aObrazovanieObuka: TAction;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarLargeButton34: TdxBarLargeButton;
    aIskustvoRabotno: TAction;
    dxBarLargeButton35: TdxBarLargeButton;
    dxBarLargeButton36: TdxBarLargeButton;
    aVestiniKvalifikacii: TAction;
    aKontakti: TAction;
    dxBarButton5: TdxBarButton;
    dxBarLargeButton37: TdxBarLargeButton;
    dxBarManager1Bar12: TdxBar;
    dxBarLargeButton38: TdxBarLargeButton;
    dxBarLargeButton39: TdxBarLargeButton;
    dxBarLargeButton40: TdxBarLargeButton;
    dxBarLargeButton41: TdxBarLargeButton;
    dxBarLargeButton42: TdxBarLargeButton;
    dxBarLargeButton43: TdxBarLargeButton;
    aPregledOtsustva: TAction;
    aRMOglas: TAction;
    dxBarLargeButton44: TdxBarLargeButton;
    dxBarManager1Bar11: TdxBar;
    dxBarLargeButton45: TdxBarLargeButton;
    aMolba: TAction;
    aObrazovanieObukaM: TAction;
    dxBarLargeButton46: TdxBarLargeButton;
    aVestiniKvalifikaciiM: TAction;
    dxBarLargeButton47: TdxBarLargeButton;
    aIskustvoRabotnoM: TAction;
    dxBarLargeButton48: TdxBarLargeButton;
    aKontaktiM: TAction;
    dxBarLargeButton49: TdxBarLargeButton;
    dxBarManager1Bar13: TdxBar;
    aIntervju: TAction;
    dxBarLargeButton50: TdxBarLargeButton;
    dxBarLargeButton51: TdxBarLargeButton;
    aIntBrUcesnici: TAction;
    aIntOblik: TAction;
    dxBarLargeButton52: TdxBarLargeButton;
    dxBarLargeButton53: TdxBarLargeButton;
    aIntSelekcija: TAction;
    GridPanel1: TGridPanel;
    Panel2: TPanel;
    aTreeView: TAction;
    tblProveriStatusOts: TpFIBDataSet;
    dsProveriStatusOts: TDataSource;
    tblProveriStatusOtsKOLKU: TFIBIntegerField;
    Action1: TAction;
    aProveriStatusOts: TAction;
    dsOrgRMRE: TDataSource;
    tblOrgRMRE: TpFIBDataSet;
    tblOrgRMRENAZIV_RE: TFIBStringField;
    tblOrgRMREID_RMRE: TFIBStringField;
    tblOrgRMRESLIKA: TFIBBlobField;
    tblOrgRMREID_RE: TFIBStringField;
    tblOrgRMREBOJA: TFIBIntegerField;
    dxTree: TdxDBTreeView;
    orgRM: TdxDbOrgChart;
    Panel5: TPanel;
    dxNavBar2: TdxNavBar;
    dxNavBarGroup1: TdxNavBarGroup;
    nbItemOtsustva: TdxNavBarItem;
    dxNavBarSeparator1: TdxNavBarSeparator;
    Panel3: TPanel;
    cxLabel2: TcxLabel;
    rtIzvestai: TdxRibbonTab;
    dxBarSubItem2: TdxBarSubItem;
    dxBarLargeButton54: TdxBarLargeButton;
    dxBarManager1Bar15: TdxBar;
    dxBarManager1Bar16: TdxBar;
    aObukaPregled: TAction;
    dxBarLargeButton55: TdxBarLargeButton;
    dxBarLargeButton56: TdxBarLargeButton;
    dxBarLargeButton57: TdxBarLargeButton;
    aDeca: TAction;
    dxBarLargeButton58: TdxBarLargeButton;
    aPregledVraboteniPoSektori: TAction;
    dxBarLargeButton59: TdxBarLargeButton;
    dxBarManager1Bar14: TdxBar;
    dxBarLargeButton60: TdxBarLargeButton;
    aVraboteniStranskiJazik: TAction;
    dxBarLargeButton61: TdxBarLargeButton;
    dxBarManager1Bar17: TdxBar;
    dxBarLargeButton62: TdxBarLargeButton;
    aKategorija: TAction;
    dxBarLargeButton63: TdxBarLargeButton;
    aPregledVraboteniObrazovanie: TAction;
    dxBarLargeButton64: TdxBarLargeButton;
    dxBarLargeButton65: TdxBarLargeButton;
    aTipKS: TAction;
    aKS: TAction;
    dxBarManager1Bar19: TdxBar;
    dxBarLargeButton66: TdxBarLargeButton;
    dxBarLargeButton67: TdxBarLargeButton;
    aNajdobroRang: TAction;
    aDizajner: TAction;
    dxBarLargeButton68: TdxBarLargeButton;
    dxBarLargeButton69: TdxBarLargeButton;
    dxBarLargeButton70: TdxBarLargeButton;
    rtAbout: TdxRibbonTab;
    dxBarLargeButton71: TdxBarLargeButton;
    aAbout: TAction;
    Panel4: TPanel;
    Image1: TImage;
    Pukaj: TTimer;
    Image2: TImage;
    cxLabel1: TcxLabel;
    dxBarManager1Bar20: TdxBar;
    dxBarLargeButton72: TdxBarLargeButton;
    aStarosnaStruktura: TAction;
    rtIzlez: TdxRibbonTab;
    trMerenjeUcinok: TdxRibbonTab;
    dxBarManager1Bar21: TdxBar;
    dxBarButton6: TdxBarButton;
    dxBarLargeButton73: TdxBarLargeButton;
    aFeedBack: TAction;
    dxBarManager1Bar22: TdxBar;
    dxBarLargeButton74: TdxBarLargeButton;
    aListaNaOcenuvaci: TAction;
    dxBarLargeButton75: TdxBarLargeButton;
    aKompetencija: TAction;
    dxBarLargeButton76: TdxBarLargeButton;
    dxBarLargeButton77: TdxBarLargeButton;
    aGrupaNaPrasanja: TAction;
    aGrupiraniPrasanja: TAction;
    dxBarLargeButton78: TdxBarLargeButton;
    rtSetUp: TdxRibbonTab;
    dxBarManager1Bar23: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    dxBarButton7: TdxBarButton;
    aZacuvajSkin2: TAction;
    dxBarButton8: TdxBarButton;
    aPregledPoVidNaRabotenOdnos: TAction;
    dxBarLargeButton79: TdxBarLargeButton;
    dxBarLargeButton80: TdxBarLargeButton;
    aPregledPoPol: TAction;
    rtDokumenti: TdxRibbonTab;
    dxBarLargeButton81: TdxBarLargeButton;
    dxBarLargeButton82: TdxBarLargeButton;
    dxBarLargeButton83: TdxBarLargeButton;
    dxBarLargeButton84: TdxBarLargeButton;
    dxBarLargeButton85: TdxBarLargeButton;
    dxBarLargeButton86: TdxBarLargeButton;
    dxBarManager1Bar27: TdxBar;
    aVidDokument: TAction;
    dxBarLargeButton87: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarSubItem4: TdxBarSubItem;
    aIzgledDogRabota: TAction;
    dxBarButton9: TdxBarButton;
    aDogVrabotuvanje: TAction;
    dxBarManager1Bar25: TdxBar;
    aResenieRaspredelba: TAction;
    dxBarLargeButton88: TdxBarLargeButton;
    dxBarManager1Bar26: TdxBar;
    aBaranjeZaGodOdmor: TAction;
    dxBarLargeButton89: TdxBarLargeButton;
    dxBarLargeButton90: TdxBarLargeButton;
    aResenieGO: TAction;
    dxBarManager1Bar28: TdxBar;
    dxBarLargeButton91: TdxBarLargeButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    aMesto: TAction;
    aOpstina: TAction;
    aDrzava: TAction;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton13: TdxBarButton;
    dxBarButton14: TdxBarButton;
    dxBarManager1Bar24: TdxBar;
    aPPR: TAction;
    dxBarLargeButton92: TdxBarLargeButton;
    dxBarButton15: TdxBarButton;
    aNacinNaVrabotuvanje: TAction;
    dxBarButton16: TdxBarButton;
    aOsnovNaOsiguruvanje: TAction;
    aTipNaPovreda: TAction;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;
    aPovreda: TAction;
    aPovredaNaRM: TAction;
    dxBarLargeButton93: TdxBarLargeButton;
    aSZObrazovanie: TAction;
    dxBarLargeButton94: TdxBarLargeButton;
    aTipRV: TAction;
    dxBarLargeButton95: TdxBarLargeButton;
    tblRe: TpFIBDataSet;
    tblReID: TFIBIntegerField;
    tblReNAZIV: TFIBStringField;
    tblReTIP_PARTNER: TFIBIntegerField;
    tblRePARTNER: TFIBIntegerField;
    tblReKOREN: TFIBIntegerField;
    tblReSPISOK: TFIBStringField;
    tblReRE: TFIBIntegerField;
    tblRePOTEKLO: TFIBStringField;
    tblReRAKOVODITEL: TFIBStringField;
    tblReR: TFIBSmallIntField;
    tblReM: TFIBSmallIntField;
    tblReT: TFIBSmallIntField;
    tblRePOV: TFIBSmallIntField;
    dsRe: TDataSource;
    dxBarLargeButton96: TdxBarLargeButton;
    aViewVraboteni: TAction;
    dxBarLargeButton97: TdxBarLargeButton;
    aDogHonorarci: TAction;
    dxBarButton19: TdxBarButton;
    aVidOtkaz: TAction;
    dxBarLargeButton98: TdxBarLargeButton;
    aResenieOtkaz: TAction;
    dxBarManager1Bar30: TdxBar;
    aDokumentacijaIzvestaj: TAction;
    dxBarLargeButton99: TdxBarLargeButton;
    aSezonskoRabotenje: TAction;
    dxBarLargeButton100: TdxBarLargeButton;
    aPenzioneri: TAction;
    dxBarLargeButton101: TdxBarLargeButton;
    aTipNPK: TAction;
    dxBarButton20: TdxBarButton;
    dxBarManager1Bar31: TdxBar;
    aNKP: TAction;
    dxBarLargeButton102: TdxBarLargeButton;
    dxBarManager1Bar18: TdxBar;
    dxBarLargeButton103: TdxBarLargeButton;
    dxBarLargeButton104: TdxBarLargeButton;
    dxBarLargeButton105: TdxBarLargeButton;
    dxBarManager1Bar32: TdxBar;
    dxBarManager1Bar29: TdxBar;
    dxBarLargeButton106: TdxBarLargeButton;
    dxBarLargeButton107: TdxBarLargeButton;
    dxBarManager1Bar33: TdxBar;
    aParamKolektivenDog: TAction;
    dxBarLargeButton108: TdxBarLargeButton;
    aFormConfig: TAction;
    dxBarLargeButton109: TdxBarLargeButton;
    aDogSpecijalisti: TAction;
    dxBarButton21: TdxBarButton;
    aVraboteniDetalen: TAction;
    dxBarButton22: TdxBarButton;
    aPodatociLekar: TAction;
    dxBarLargeButton110: TdxBarLargeButton;
    dxBarButton23: TdxBarButton;
    dxBarButton24: TdxBarButton;
    dxBarButton25: TdxBarButton;
    dxBarLargeButton2: TdxBarLargeButton;
    aDogVolonteri: TAction;
    dxBarLargeButton111: TdxBarLargeButton;
    aVolonteri: TAction;
    dxBarLargeButton112: TdxBarLargeButton;
    aPrijavaOdjava: TAction;
    dxBarButton26: TdxBarButton;
    aOsnovPrijavuvanje: TAction;
    aOsnovNaOdjavuvanje: TAction;
    dxBarButton27: TdxBarButton;
    aJSOpstiPodatoci: TAction;
    dxBarLargeButton113: TdxBarLargeButton;
    aObrazovniInstitucii: TAction;
    dxBarLargeButton114: TdxBarLargeButton;
    dxBarLargeButton115: TdxBarLargeButton;
    aNasokaObrazovanie: TAction;
    dxBarManager1Bar34: TdxBar;
    dxBarLargeButton116: TdxBarLargeButton;
    aSkeniraj: TAction;
    dxBarLargeButton117: TdxBarLargeButton;
    aPregledScanDokumenti: TAction;
    dxBarButton28: TdxBarButton;
    dxBarLargeButton118: TdxBarLargeButton;
    aUsloviPoteskiOdNormalni: TAction;
    dxBarManager1Bar35: TdxBar;
    dxBarLargeButton119: TdxBarLargeButton;
    aSetupModule: TAction;
    rtHonorarnaSorabotka: TdxRibbonTab;
    dxBarManager1Bar37: TdxBar;
    dxBarLargeButton120: TdxBarLargeButton;
    dxBarLargeButton121: TdxBarLargeButton;
    dxBarLargeButton122: TdxBarLargeButton;
    dxBarLargeButton123: TdxBarLargeButton;
    dxBarLargeButton124: TdxBarLargeButton;
    dxBarLargeButton125: TdxBarLargeButton;
    dxBarManager1Bar36: TdxBar;
    dxBarLargeButton126: TdxBarLargeButton;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    dxBarLargeButton127: TdxBarLargeButton;
    aPotvrdaRabotenOdnos: TAction;
    dxBarButton29: TdxBarButton;
    dxBarLargeButton128: TdxBarLargeButton;
    aRabotenStaz: TAction;
    dxBarLargeButton129: TdxBarLargeButton;
    dxBarLargeButton130: TdxBarLargeButton;
    aCustomFields: TAction;
    dxBarLargeButton131: TdxBarLargeButton;
    aRabotnoMestoUsloviPoteski: TAction;
    dxRibbon1TabPreglediReporter: TdxRibbonTab;
    dxBarLargeButton132: TdxBarLargeButton;
    aOsveziPodatoci: TAction;
    dxBarLargeButton133: TdxBarLargeButton;
    aKategorijaRM: TAction;
    dxBarManager1Bar38: TdxBar;
    dxBarLargeButton134: TdxBarLargeButton;
    aPregledSistematizacija: TAction;
    dxBarLargeButton135: TdxBarLargeButton;
    aIzjavaProdolzenRO: TAction;
    dxbrlrgbtn1: TdxBarLargeButton;
    aPregledNevraboteni: TAction;
    procedure FormCreate(Sender: TObject);
    procedure aReExecute(Sender: TObject);
    procedure OtvoriTabeli;
    procedure aVidVrabotuvanjeExecute(Sender: TObject);
    procedure aGrupaRMExecute(Sender: TObject);
    procedure aGrupaPlataExecute(Sender: TObject);
    procedure aNacionalnostExecute(Sender: TObject);
    procedure aVeroispovedExecute(Sender: TObject);
    procedure aPrazniciExecute(Sender: TObject);
    procedure aTipOtsustvoExecute(Sender: TObject);
    procedure aRMExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPlanZaOtsustvaExecute(Sender: TObject);
    procedure aSistematizacijaExecute(Sender: TObject);
    procedure aRMREExecute(Sender: TObject);
    procedure aEvidencijaOtsustvaExecute(Sender: TObject);
    procedure aStatusExecute(Sender: TObject);
    procedure aGrupaStatusExecute(Sender: TObject);
    procedure aDrzavjanstvoExecute(Sender: TObject);
    procedure aStranskiJaziciExecute(Sender: TObject);
    procedure aOglasExecute(Sender: TObject);
    procedure aObukiExecute(Sender: TObject);
    procedure aVraboteniExecute(Sender: TObject);
    procedure aObrazovanieObukaExecute(Sender: TObject);
    procedure aIskustvoRabotnoExecute(Sender: TObject);
    procedure aVestiniKvalifikaciiExecute(Sender: TObject);
    procedure aKontaktiExecute(Sender: TObject);
    procedure aObrazovanieExecute(Sender: TObject);
    procedure aPregledOtsustvaExecute(Sender: TObject);
    procedure aRMOglasExecute(Sender: TObject);
    procedure aMolbaExecute(Sender: TObject);
    procedure aObrazovanieObukaMExecute(Sender: TObject);
    procedure aVestiniKvalifikaciiMExecute(Sender: TObject);
    procedure aIskustvoRabotnoMExecute(Sender: TObject);
    procedure aKontaktiMExecute(Sender: TObject);
    procedure aIntervjuExecute(Sender: TObject);
    procedure aIntBrUcesniciExecute(Sender: TObject);
    procedure aIntOblikExecute(Sender: TObject);
    procedure aIntSelekcijaExecute(Sender: TObject);
    procedure orgRMDeletion(Sender: TObject; Node: TdxOcNode);
    procedure orgRMCreateNode(Sender: TObject; Node: TdxOcNode);
    procedure dxTreeClick(Sender: TObject);
    procedure aTreeViewExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure aProveriStatusOtsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aObukaPregledExecute(Sender: TObject);
    procedure aDecaExecute(Sender: TObject);
    procedure aPregledVraboteniPoSektoriExecute(Sender: TObject);
    procedure aVraboteniStranskiJazikExecute(Sender: TObject);
    procedure aKategorijaExecute(Sender: TObject);
    procedure aPregledVraboteniObrazovanieExecute(Sender: TObject);
    procedure aTipKSExecute(Sender: TObject);
    procedure aKSExecute(Sender: TObject);
    procedure aNajdobroRangExecute(Sender: TObject);
    procedure aDizajnerExecute(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure aAboutExecute(Sender: TObject);
    procedure PukajTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure aStarosnaStrukturaExecute(Sender: TObject);
    procedure aFeedBackExecute(Sender: TObject);
    procedure aListaNaOcenuvaciExecute(Sender: TObject);
    procedure aKompetencijaExecute(Sender: TObject);
    procedure aGrupaNaPrasanjaExecute(Sender: TObject);
    procedure aZacuvajSkin2Execute(Sender: TObject);
    procedure cxBarEditItem1PropertiesChange(Sender: TObject);
    procedure aPregledPoVidNaRabotenOdnosExecute(Sender: TObject);
    procedure aPregledPoPolExecute(Sender: TObject);
    procedure aVidDokumentExecute(Sender: TObject);
    procedure aIzgledDogRabotaExecute(Sender: TObject);
    procedure aDogVrabotuvanjeExecute(Sender: TObject);
    procedure aResenieRaspredelbaExecute(Sender: TObject);
    procedure aBaranjeZaGodOdmorExecute(Sender: TObject);
    procedure aResenieGOExecute(Sender: TObject);
    procedure aMestoExecute(Sender: TObject);
    procedure aOpstinaExecute(Sender: TObject);
    procedure aDrzavaExecute(Sender: TObject);
    procedure aPPRExecute(Sender: TObject);
    procedure aNacinNaVrabotuvanjeExecute(Sender: TObject);
    procedure aOsnovNaOsiguruvanjeExecute(Sender: TObject);
    procedure aTipNaPovredaExecute(Sender: TObject);
    procedure aPovredaExecute(Sender: TObject);
    procedure aPovredaNaRMExecute(Sender: TObject);
    procedure aSZObrazovanieExecute(Sender: TObject);
    procedure aTipRVExecute(Sender: TObject);
    procedure aViewVraboteniExecute(Sender: TObject);
    procedure aDogHonorarciExecute(Sender: TObject);
    procedure aVidOtkazExecute(Sender: TObject);
    procedure aResenieOtkazExecute(Sender: TObject);
    procedure aDokumentacijaIzvestajExecute(Sender: TObject);
    procedure aSezonskoRabotenjeExecute(Sender: TObject);
    procedure aPenzioneriExecute(Sender: TObject);
    procedure aTipNPKExecute(Sender: TObject);
    procedure aNKPExecute(Sender: TObject);
    procedure aParamKolektivenDogExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure aDogSpecijalistiExecute(Sender: TObject);
    procedure aVraboteniDetalenExecute(Sender: TObject);
    procedure aPodatociLekarExecute(Sender: TObject);
    procedure aDogVolonteriExecute(Sender: TObject);
    procedure aVolonteriExecute(Sender: TObject);
    procedure aPrijavaOdjavaExecute(Sender: TObject);
    procedure aOsnovPrijavuvanjeExecute(Sender: TObject);
    procedure aOsnovNaOdjavuvanjeExecute(Sender: TObject);
    procedure aJSOpstiPodatociExecute(Sender: TObject);
    procedure aObrazovniInstituciiExecute(Sender: TObject);
    procedure aNasokaObrazovanieExecute(Sender: TObject);
    procedure aSkenirajExecute(Sender: TObject);
    procedure aPregledScanDokumentiExecute(Sender: TObject);
    procedure aUsloviPoteskiOdNormalniExecute(Sender: TObject);
    procedure aSetupModuleExecute(Sender: TObject);
    procedure aPotvrdaRabotenOdnosExecute(Sender: TObject);
    procedure dxBarButton29Click(Sender: TObject);
    procedure aCustomFieldsExecute(Sender: TObject);
    procedure aRabotnoMestoUsloviPoteskiExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aOsveziPodatociExecute(Sender: TObject);
    procedure aKategorijaRMExecute(Sender: TObject);
    procedure aPregledSistematizacijaExecute(Sender: TObject);
    procedure aIzjavaProdolzenROExecute(Sender: TObject);
    procedure aPregledNevraboteniExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses dmKonekcija, dmMaticni, dmResources, Organogram, dmUnit, VidVrabotuvanje,
  dmSistematizacija, GrupaRM, GrupaPlata, Nacionalnost, Praznici, TipOtsustvo,
  Veroispoved, RabotnoMesto, cxConstantsMak, PlanZaOtsustva, Sistematizacija,
  RabMestaRE, dmUnitOtsustvo, EvidencijaOtsustva, Status,
  StatusGrupa, Drzavjanstvo, Oglas, Obuki, VraboteniHRM, ObrazovanieObuka,
  IskustvoRabotno, VestiniKvalifikacii, Kontakt, Obrazovanie, OstsustvaPregled,
  RMOglas, Molbi, RabotniEdinici, Intervju, IntBrojUcesnici, IntOblik,
  IntSelekcija, ObukiPregled, Deca, PregledPoStranskiJazik, Kategorija,
  PregledPoObrazovanie, TipKS, KriteriumSelekcija, dmReportUnit, AboutBox,
  StarosnaStruktura, PregledVraboteniRabMesta, StranskiJazici, FeedBack,
  RatingFrom, Kompetencija, GrupaNaPrasanja, PregledPoVidRabotenOdnos,
  PregledPoPol, VidDokument, DogovorVrabotuvanje, ResenieZaPreraspredelba,
  BaranjeZaGodisenOdmor, ResenieZaGodisenOdmor, Drzava, Mesto, Opstina,
  PPR_Obrazec, NacinVrabotuvanje, OsnovNaOsiguruvanje, TipNaPovreda, Povreda,
  PovredaNaRM, SZObrazovani, TipRabotnoVreme, ViewVraboteni,
  DogHonorarnaSorabotka, VidOtkaz, ResenieZaOtkaz, IzvestajDokumenti,
  DogSezonskoRabotenje, PregledNaPenzioneri, TipNaNKM, NKM,
  ParamPoKolektivenDog, FormConfig, Utils, Specijalizacija, PodatociLekari,
  DecaSopruznici, DogVolonteri, htmlhelpviewer, PrijavaOdjava,
  OsnovNaPrijavuvanje, AneksNaDogovor, OsnovNaOdjavuvanje, JSOpstiPodatoci,
  ObrazovniInstitucii, NasokaObrazovanie, ScanDocument, PregledScanDocument,
  UsloviPoteskiOdNormalnite, MK, Module, PotvrdaZaRabotenOdnos, Proba,
  CustomFields, RabotnoMestoUsloviRabota, KategorijaRM, PregledSistematizacija,
  IzjavaProdolzenRabotenOdnos, PregledNaNevraboteni;

type
 TNodeData = record
    Color: TColor;
    slika: TBitmapImage;
    Name: string;
  end;
  PNodeData = ^TNodeData;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
   frmAboutBox:=TfrmAboutBox.Create(Application);
   frmAboutBox.ShowModal;
   frmAboutBox.Free;
end;

procedure TfrmMain.aBaranjeZaGodOdmorExecute(Sender: TObject);
begin
      frmBaranjeZaGodisenOdmor:=TfrmBaranjeZaGodisenOdmor.Create(Application);
      frmBaranjeZaGodisenOdmor.ShowModal;
      frmBaranjeZaGodisenOdmor.Free;
end;

procedure TfrmMain.Action1Execute(Sender: TObject);
begin
    if GridPanel1.Visible then GridPanel1.Visible:=True
    else GridPanel1.Visible:=True;

end;

procedure TfrmMain.aCustomFieldsExecute(Sender: TObject);
begin
     frmCustomFields:=TfrmCustomFields.Create(Self, true);
     frmCustomFields.ShowModal;
     frmCustomFields.Free;
end;

procedure TfrmMain.aDecaExecute(Sender: TObject);
begin
     frmDecaSopruznici:=TfrmDecaSopruznici.Create(Application);
     frmDecaSopruznici.ShowModal;
     frmDecaSopruznici.Free;
end;

procedure TfrmMain.aDizajnerExecute(Sender: TObject);
begin
   dmReport.Spremi(30086,dmKon.aplikacija);
   // dmReport.tblSqlReport.Params.ParamByName('molba').Asinteger:=dmsis.tblKSID_MOLBA.Value;; //tblListaTuzbiID.AsString;
    dmReport.tblSqlReport.Open;

    dmReport.frxReport1.DesignReport();
end;

procedure TfrmMain.aDogHonorarciExecute(Sender: TObject);
begin
     frmDogHonorarnaSorabotka:=TfrmDogHonorarnaSorabotka.Create(Application);
     frmDogHonorarnaSorabotka.ShowModal;
     frmDogHonorarnaSorabotka.Free;
end;

procedure TfrmMain.aDogSpecijalistiExecute(Sender: TObject);
begin
     frmSpecijalizacija:=TfrmSpecijalizacija.Create(Application);
     frmSpecijalizacija.WindowState:=wsMaximized;
     frmSpecijalizacija.ShowModal;
     frmSpecijalizacija.Free;
end;

procedure TfrmMain.aDogVolonteriExecute(Sender: TObject);
begin
     frmDogVolonteri:=TfrmDogVolonteri.Create(Application);
     frmDogVolonteri.ShowModal;
     frmDogVolonteri.Free;
end;

procedure TfrmMain.aDogVrabotuvanjeExecute(Sender: TObject);
begin
     frmDogovorVrabotuvanje:=TfrmDogovorVrabotuvanje.Create(Application);
     frmDogovorVrabotuvanje.ShowModal;
     frmDogovorVrabotuvanje.Free;

end;

procedure TfrmMain.aDokumentacijaIzvestajExecute(Sender: TObject);
begin
     frmIzvestajDokumenti:=TfrmIzvestajDokumenti.Create(Application);
     frmIzvestajDokumenti.ShowModal;
     frmIzvestajDokumenti.Free;
end;

procedure TfrmMain.aDrzavaExecute(Sender: TObject);
begin
     frmDrzava:=TfrmDrzava.Create(Application);
     frmDrzava.ShowModal;
     frmDrzava.Free;
end;

procedure TfrmMain.aDrzavjanstvoExecute(Sender: TObject);
begin
     frmDrzavjanstvo:=TfrmDrzavjanstvo.Create(Application);
     frmDrzavjanstvo.ShowModal;
     frmDrzavjanstvo.Free;
end;

procedure TfrmMain.aEvidencijaOtsustvaExecute(Sender: TObject);
begin
     frmEvidencijaOtsustva:=TfrmEvidencijaOtsustva.Create(Application);
     frmEvidencijaOtsustva.ShowModal;
     frmEvidencijaOtsustva.Free;
end;

procedure TfrmMain.aFeedBackExecute(Sender: TObject);
begin
     frmFeedBack:=TfrmFeedBack.Create(Application);
     frmFeedBack.ShowModal;
     frmFeedBack.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
   frmFormConfig := TfrmFormConfig.Create(Application);
   frmFormConfig.formPtr := Addr(Self);
   frmFormConfig.ShowModal;
   frmFormConfig.Free;
end;

procedure TfrmMain.aGrupaNaPrasanjaExecute(Sender: TObject);
begin
     frmGrupaPrasanja:=TfrmGrupaPrasanja.Create(Application);
     frmGrupaPrasanja.ShowModal;
     frmGrupaPrasanja.Free;
end;

procedure TfrmMain.aGrupaPlataExecute(Sender: TObject);
begin
    frmGrupaPlata:=TfrmGrupaPlata.Create(Application);
    frmGrupaPlata.ShowModal;
    frmGrupaPlata.Free;
end;

procedure TfrmMain.aGrupaRMExecute(Sender: TObject);
begin
    frmGrupaRM:=TfrmGrupaRM.Create(Application);
    frmGrupaRM.ShowModal;
    frmGrupaRM.Free;
end;

procedure TfrmMain.aGrupaStatusExecute(Sender: TObject);
begin
     frmStatusGrupa:=TfrmStatusGrupa.Create(Application);
     frmStatusGrupa.ShowModal;
     frmStatusGrupa.Free;
end;

procedure TfrmMain.aIntBrUcesniciExecute(Sender: TObject);
begin
     frmIntBrojUcesnici:=TfrmIntBrojUcesnici.Create(Application);
     frmIntBrojUcesnici.ShowModal;
     frmIntBrojUcesnici.Free;
end;

procedure TfrmMain.aIntervjuExecute(Sender: TObject);
begin
     frmIntervju:=TfrmIntervju.Create(self,false);
     frmIntervju.ShowModal;
     frmIntervju.Free;
end;

procedure TfrmMain.aIntOblikExecute(Sender: TObject);
begin
     frmIntOblik:=TfrmIntOblik.Create(Application);
     frmIntOblik.ShowModal;
     frmIntOblik.Free;
end;

procedure TfrmMain.aIntSelekcijaExecute(Sender: TObject);
begin
     frmIntSelekcija:=TfrmIntSelekcija.Create(Application);
     frmIntSelekcija.ShowModal;
     frmIntSelekcija.Free;
end;

procedure TfrmMain.aIskustvoRabotnoExecute(Sender: TObject);
begin
     frmIskustvoRabotno:=TfrmIskustvoRabotno.Create(Application);
     frmIskustvoRabotno.Tag:=2;
     frmIskustvoRabotno.ShowModal;
     frmIskustvoRabotno.Free;
end;

procedure TfrmMain.aIskustvoRabotnoMExecute(Sender: TObject);
begin
     frmIskustvoRabotno:=TfrmIskustvoRabotno.Create(Application);
     frmIskustvoRabotno.Tag:=4;
     frmIskustvoRabotno.ShowModal;
     frmIskustvoRabotno.Free;
end;

procedure TfrmMain.aIzgledDogRabotaExecute(Sender: TObject);
begin
    dmReport.Spremi(30100,dmKon.aplikacija);
   // dmReport.tblSqlReport.Params.ParamByName('molba').Asinteger:=dmsis.tblKSID_MOLBA.Value;; //tblListaTuzbiID.AsString;
   // dmReport.tblSqlReport.Open;

    dmReport.frxReport1.DesignReport();

end;

procedure TfrmMain.aIzjavaProdolzenROExecute(Sender: TObject);
begin
     frmIzjavaProdolzenRabotenOdnos:=TfrmIzjavaProdolzenRabotenOdnos.Create(Application);
     frmIzjavaProdolzenRabotenOdnos.ShowModal;
     frmIzjavaProdolzenRabotenOdnos.Free;
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
    Close;
end;

procedure TfrmMain.aJSOpstiPodatociExecute(Sender: TObject);
begin
     frmJSOpstiPodatoci:=TfrmJSOpstiPodatoci.Create(Application);
     frmJSOpstiPodatoci.ShowModal;
     frmJSOpstiPodatoci.Free;
end;

procedure TfrmMain.aKategorijaExecute(Sender: TObject);
begin
    frmKategorija:=TfrmKategorija.Create(Application);
    frmKategorija.ShowModal;
    frmKategorija.Free;
end;

procedure TfrmMain.aKategorijaRMExecute(Sender: TObject);
begin
     frmKategorijaRM:=TfrmKategorijaRM.Create(self,true);
     frmKategorijaRM.ShowModal;
     frmKategorijaRM.Free;
end;

procedure TfrmMain.aKompetencijaExecute(Sender: TObject);
begin
     frmKompetencija:=TfrmKompetencija.Create(Application);
     frmKompetencija.ShowModal;
     frmKompetencija.Free;
end;

procedure TfrmMain.aKontaktiExecute(Sender: TObject);
begin
     frmKontakt:=TfrmKontakt.Create(Application);
     frmKontakt.Tag:=2;
     frmKontakt.ShowModal;
     frmKontakt.Free;
end;

procedure TfrmMain.aKontaktiMExecute(Sender: TObject);
begin
     frmKontakt:=TfrmKontakt.Create(Application);
     frmKontakt.Tag:=4;
     frmKontakt.ShowModal;
     frmKontakt.Free;
end;

procedure TfrmMain.aKSExecute(Sender: TObject);
begin
   frmKriteriumSelekcija:=TfrmKriteriumSelekcija.Create(Application);
   frmKriteriumSelekcija.ShowModal;
   frmKriteriumSelekcija.Free;
end;

procedure TfrmMain.aListaNaOcenuvaciExecute(Sender: TObject);
begin
     frmRatingFrom:=TfrmRatingFrom.Create(Application);
     frmRatingFrom.ShowModal;
     frmRatingFrom.Free;
end;

procedure TfrmMain.aMestoExecute(Sender: TObject);
begin
     frmMesto:=TfrmMesto.Create(Application);
     frmMesto.ShowModal;
     frmMesto.Free;
end;

procedure TfrmMain.aMolbaExecute(Sender: TObject);
begin
     frmMolbi:=TfrmMolbi.Create(Application);
     frmMolbi.ShowModal;
     frmMolbi.Free;
end;

procedure TfrmMain.aNacinNaVrabotuvanjeExecute(Sender: TObject);
begin
     frmNacinVrabotuvanje:=TfrmNacinVrabotuvanje.Create(Application);
     frmNacinVrabotuvanje.ShowModal;
     frmNacinVrabotuvanje.Free;
end;

procedure TfrmMain.aNacionalnostExecute(Sender: TObject);
begin
   frmNacionalnost:=TfrmNacionalnost.Create(Application);
   frmNacionalnost.ShowModal;
   frmNacionalnost.Free;
end;

procedure TfrmMain.aNajdobroRangExecute(Sender: TObject);
begin
    dmReport.Spremi(30086,dmKon.aplikacija);
    dmReport.tblSqlReport.ParamByName('firma').Asinteger:=firma;
    dmReport.tblSqlReport.Open;

    dmReport.frxReport1.ShowReport();
end;

procedure TfrmMain.aNasokaObrazovanieExecute(Sender: TObject);
begin
     frmNasokaObrazovanie:=TfrmNasokaObrazovanie.Create(Application);
     frmNasokaObrazovanie.ShowModal;
     frmNasokaObrazovanie.Free;
end;

procedure TfrmMain.aNKPExecute(Sender: TObject);
begin
     frmNKM:=TfrmNKM.Create(Application);
     frmNKM.ShowModal;
     frmNKM.Free;
end;

procedure TfrmMain.aObrazovanieExecute(Sender: TObject);
begin
     frmObrazovanie:=TfrmObrazovanie.Create(Application);
     frmObrazovanie.ShowModal;
     frmObrazovanie.Free;
end;

procedure TfrmMain.aObrazovanieObukaExecute(Sender: TObject);
begin
     frmObrazovanieObuka:=TfrmObrazovanieObuka.Create(Application);
     frmObrazovanieObuka.Tag:=2;
     frmObrazovanieObuka.ShowModal;
     frmObrazovanieObuka.Free;
end;

procedure TfrmMain.aObrazovanieObukaMExecute(Sender: TObject);
begin
     frmObrazovanieObuka:=TfrmObrazovanieObuka.Create(Application);
     frmObrazovanieObuka.Tag:=4;
     frmObrazovanieObuka.ShowModal;
     frmObrazovanieObuka.Free;
end;

procedure TfrmMain.aObrazovniInstituciiExecute(Sender: TObject);
begin
      frmObrazovniInstitucii:=TfrmObrazovniInstitucii.Create(Application);
      frmObrazovniInstitucii.ShowModal;
      frmObrazovniInstitucii.Free;
end;

procedure TfrmMain.aObukaPregledExecute(Sender: TObject);
begin
     frmObukiPregled:=TfrmObukiPregled.Create(Application);
     frmObukiPregled.ShowModal;
     frmObukiPregled.Free;
end;

procedure TfrmMain.aObukiExecute(Sender: TObject);
begin
     frmObuki:=TfrmObuki.Create(Application);
     frmObuki.ShowModal;
     frmObuki.Free;
end;

procedure TfrmMain.aOglasExecute(Sender: TObject);
begin
     frmOglas:=TfrmOglas.Create(self,false);
     frmOglas.ShowModal;
     frmOglas.Free;
end;

procedure TfrmMain.aOpstinaExecute(Sender: TObject);
begin
     frmOpstina:=TfrmOpstina.Create(Application);
     frmOpstina.ShowModal;
     frmOpstina.Free;
end;

procedure TfrmMain.aOsnovNaOdjavuvanjeExecute(Sender: TObject);
begin
     frmOsnovOdjavuvanje:=TfrmOsnovOdjavuvanje.Create(Application);
     frmOsnovOdjavuvanje.ShowModal;
     frmOsnovOdjavuvanje.Free;
end;

procedure TfrmMain.aOsnovNaOsiguruvanjeExecute(Sender: TObject);
begin
     frmOsnovNaOsiguruvanje:=TfrmOsnovNaOsiguruvanje.Create(Application);
     frmOsnovNaOsiguruvanje.ShowModal;
     frmOsnovNaOsiguruvanje.Free;
end;

procedure TfrmMain.aOsnovPrijavuvanjeExecute(Sender: TObject);
begin
     frmOsnovNaPrijavuvawe:=TfrmOsnovNaPrijavuvawe.Create(Application);
     frmOsnovNaPrijavuvawe.ShowModal;
     frmOsnovNaPrijavuvawe.Free;
end;

procedure TfrmMain.aOsveziPodatociExecute(Sender: TObject);
var status: TStatusWindowHandle;
begin
status := cxCreateStatusWindow();
 try
  dmMat.tblRE.FullRefresh;
  dmMat.tblValuta.FullRefresh;
  dmSis.tblVidVrabotuvanje.FullRefresh;
  dmSis.tblGrupaRM.FullRefresh;
  dmSis.tblGrupaPlata.FullRefresh;
  dmSis.tblRabotnoMesto.ParamByName('firma').AsInteger:=firma;
  dmSis.tblRabotnoMesto.FullRefresh;
  dmSis.tblObrazovanie.FullRefresh;
  dmSis.tblSistematizacija.ParamByName('firma').Value:=firma;
  dmSis.tblSistematizacija.FullRefresh;
  dmSis.tblDrzavjanstvo.FullRefresh;
  dmSis.tblStranskiJazici.FullRefresh;
  dmSis.tblOrgRMRE.FullRefresh;
  //dmSis.tblOrgRMRE.ParamByName('re').Asinteger:=firma;
  dmSis.tblOrgRMRE.FullRefresh;
  dmSis.tblTipKS.FullRefresh;
  dmSis.tblTipRabotnoVreme.FullRefresh;
  dmSis.tblReVoFirma.ParamByName('re').AsInteger:=firma;
  dmsis.tblReVoFirma.FullRefresh;
  dm.tblRe.ParamByName('firma').AsInteger:=firma;
  dm.tblRe.FullRefresh;
 // tblRe.open;
  dmSis.tblRMRE.ParamByName('param').Value:=1;
  dmSis.tblRMRE.ParamByName('firma').AsInteger:=firma;
  dmSis.tblRMRE.FullRefresh;
//  dmsis.tblKategorja.ParamByName('rm').AsString:='%';
//  dmsis.tblKategorja.ParamByName('firma').asinteger:=firma;
//  dmSis.tblKategorja.open;
  dmSis.tblSZObrazovanie.FullRefresh;
  dmMat.tblPartner.FullRefresh;
  dmMat.tblStatus.ParamByName('app').Value:=dmkon.aplikacija;
  dmMat.tblStatus.FullRefresh;
  dmMat.tblStatusGrupa.ParamByName('app').Value:=dmkon.aplikacija;
  dmMat.tblStatusGrupa.FullRefresh;
  dmMat.tblSysApp.FullRefresh;
  dmmat.tblDrzava.FullRefresh;
  dmMat.tblMesto.FullRefresh;
  dmMat.tblOpstina.FullRefresh;
  dmMat.tblRegioni.FullRefresh;
  dmMat.tblFzo.FullRefresh;

  //dm.tblUsloviPoteskiOdNormalni.open;

  tblProveriStatusOts.ParamByName('app').Value:=dmkon.aplikacija;
  tblProveriStatusOts.FullRefresh;

  dm.tblVidDokument.FullRefresh;

  dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
  dm.viewVraboteni.ParamByName('mb').Value:='%';
  dm.viewVraboteni.ParamByName('re').Value:='%';
  dm.viewVraboteni.FullRefresh;
 finally
 	   cxRemoveStatusWindow(status);
 end;
end;

procedure TfrmMain.aParamKolektivenDogExecute(Sender: TObject);
begin
     frmParamKolektivenDog:=TfrmParamKolektivenDog.Create(Application);
     frmParamKolektivenDog.ShowModal;
     frmParamKolektivenDog.Free;
end;

procedure TfrmMain.aPenzioneriExecute(Sender: TObject);
begin
     frmPregledNaPenzioneri:=TfrmPregledNaPenzioneri.Create(Application);
     frmPregledNaPenzioneri.ShowModal;
     frmPregledNaPenzioneri.Free;
end;

procedure TfrmMain.aPlanZaOtsustvaExecute(Sender: TObject);
begin
     frmPlanZaOtsustvo:=TfrmPlanZaOtsustvo.Create(Application);
     frmPlanZaOtsustvo.ShowModal;
     frmPlanZaOtsustvo.Free;
end;

procedure TfrmMain.aPodatociLekarExecute(Sender: TObject);
begin
     frmPodatociLekari:=TfrmPodatociLekari.Create(Application);
     frmPodatociLekari.ShowModal;
     frmPodatociLekari.Free;
end;

procedure TfrmMain.aPotvrdaRabotenOdnosExecute(Sender: TObject);
begin
     frmPotvrdaRabotenOdnos:=TfrmPotvrdaRabotenOdnos.Create(self, false);
     frmPotvrdaRabotenOdnos.ShowModal;
     frmPotvrdaRabotenOdnos.Free;
end;

procedure TfrmMain.aPovredaExecute(Sender: TObject);
begin
      frmPovreda:=TfrmPovreda.Create(Application);
      frmPovreda.ShowModal;
      frmPovreda.Free;
end;

procedure TfrmMain.aPovredaNaRMExecute(Sender: TObject);
begin
     frmPovredaNaRM:=TfrmPovredaNaRM.Create(Application);
     frmPovredaNaRM.ShowModal;
     frmPovredaNaRM.Free;
end;

procedure TfrmMain.aPPRExecute(Sender: TObject);
begin
     frmPPR_Obrazec:=TfrmPPR_Obrazec.Create(Application);
     frmPPR_Obrazec.ShowModal;
     frmPPR_Obrazec.Free;
end;

procedure TfrmMain.aPrazniciExecute(Sender: TObject);
begin
   frmPraznici:=TfrmPraznici.Create(Application);
   frmPraznici.ShowModal;
   frmPraznici.Free;
end;

procedure TfrmMain.aPregledNevraboteniExecute(Sender: TObject);
begin
     frmPregledNevraboteni:=TfrmPregledNevraboteni.Create(Application);
     frmPregledNevraboteni.ShowModal;
     frmPregledNevraboteni.Free;
end;

procedure TfrmMain.aPregledOtsustvaExecute(Sender: TObject);
begin
     frmPregedOtsustva:=TfrmPregedOtsustva.Create(Application);
     frmPregedOtsustva.ShowModal;
     frmPregedOtsustva.Free;
end;

procedure TfrmMain.aPregledPoPolExecute(Sender: TObject);
begin
     frmPregledPoPol:=TfrmPregledPoPol.Create(Application);
     frmPregledPoPol.ShowModal;
     frmPregledPoPol.Free;
end;

procedure TfrmMain.aPregledPoVidNaRabotenOdnosExecute(Sender: TObject);
begin
     frmPregledPoVidRabotenOdnos:=TfrmPregledPoVidRabotenOdnos.Create(Application);
     frmPregledPoVidRabotenOdnos.ShowModal;
     frmPregledPoVidRabotenOdnos.Free;
end;

procedure TfrmMain.aPregledScanDokumentiExecute(Sender: TObject);
begin
     frmPregledScanDokument:=TfrmPregledScanDokument.Create(Application);
     frmPregledScanDokument.ShowModal;
     frmPregledScanDokument.Free;
end;

procedure TfrmMain.aPregledSistematizacijaExecute(Sender: TObject);
begin
     frmPregledSistematizacija:=TfrmPregledSistematizacija.Create(Application);
     frmPregledSistematizacija.ShowModal;
     frmPregledSistematizacija.Free;
end;

procedure TfrmMain.aPregledVraboteniObrazovanieExecute(Sender: TObject);
begin
     frmPregledPoObrazovanie:=TfrmPregledPoObrazovanie.Create(Application);
     frmPregledPoObrazovanie.ShowModal;
     frmPregledPoObrazovanie.Free;
end;

procedure TfrmMain.aPregledVraboteniPoSektoriExecute(Sender: TObject);
begin
     frmPregledVraboteniRabMesta:=TfrmPregledVraboteniRabMesta.Create(Application);
     frmPregledVraboteniRabMesta.ShowModal;
     frmPregledVraboteniRabMesta.Free;
end;

procedure TfrmMain.aPrijavaOdjavaExecute(Sender: TObject);
begin
     frmPrijavaOdjava:=TfrmPrijavaOdjava.Create(Application);
     frmPrijavaOdjava.ShowModal;
     frmPrijavaOdjava.Free;
end;

procedure TfrmMain.aProveriStatusOtsExecute(Sender: TObject);
begin
    aPlanZaOtsustva.Execute;
end;

procedure TfrmMain.aRabotnoMestoUsloviPoteskiExecute(Sender: TObject);
begin
  frmRmReUsloviRabota:=TfrmRmReUsloviRabota.Create(Self, true);
  frmRmReUsloviRabota.Tag:=0;
  frmRmReUsloviRabota.ShowModal;
  frmRmReUsloviRabota.Free;
end;

procedure TfrmMain.aReExecute(Sender: TObject);
begin
//    frmOrganogram:=TfrmOrganogram.Create(Application);
//    frmOrganogram.WindowState:=wsMaximized;
//    frmOrganogram.ShowModal;
//    frmOrganogram.Free;

    frmRE:=TfrmRE.Create(Application);
    frmRE.ShowModal;
    frmRE.free;
end;

procedure TfrmMain.aResenieGOExecute(Sender: TObject);
begin
      frmResenijeZaGodisenOdmor:=TfrmResenijeZaGodisenOdmor.Create(Application);
      frmResenijeZaGodisenOdmor.ShowModal;
      frmResenijeZaGodisenOdmor.Free;
      pom_tab:=0;
end;

procedure TfrmMain.aResenieOtkazExecute(Sender: TObject);
begin
      frmResenieOtkaz:=TfrmResenieOtkaz.Create(Application);
      frmResenieOtkaz.ShowModal;
      frmResenieOtkaz.Free;
end;

procedure TfrmMain.aResenieRaspredelbaExecute(Sender: TObject);
begin
     frmReseniePreraspredelba:=TfrmReseniePreraspredelba.Create(Application);
     frmReseniePreraspredelba.ShowModal;
     frmReseniePreraspredelba.Free;
end;

procedure TfrmMain.aRMExecute(Sender: TObject);
begin
    frmRabotnoMesto:=TfrmRabotnoMesto.Create(Application);
   // frmRabotnoMesto.WindowState:=wsMaximized;
    frmRabotnoMesto.ShowModal;
    frmRabotnoMesto.Free;

end;

procedure TfrmMain.aRMOglasExecute(Sender: TObject);
begin
    frmRMOglas:=TfrmRMOglas.Create(Application);
    frmRMOglas.ShowModal;
    frmRMOglas.Free;
end;

procedure TfrmMain.aRMREExecute(Sender: TObject);
begin
    frmRMRE:=TfrmRMRE.Create(Application);
    frmRMRE.ShowModal;
    frmRMRE.Free;
end;

procedure TfrmMain.aSetupModuleExecute(Sender: TObject);
begin
     frmModule:=TfrmModule.create(Application);
     frmModule.ShowModal;
     frmModule.Free;
end;

procedure TfrmMain.aSezonskoRabotenjeExecute(Sender: TObject);
begin
     frmDogSezonskoRabotenje:=TfrmDogSezonskoRabotenje.Create(Application);
     frmDogSezonskoRabotenje.ShowModal;
     frmDogSezonskoRabotenje.Free;
end;

procedure TfrmMain.aSistematizacijaExecute(Sender: TObject);
begin
     frmSistematizacija:=TfrmSistematizacija.Create(Application,False);
     frmSistematizacija.ShowModal;
     frmSistematizacija.Free;
end;

procedure TfrmMain.aSkenirajExecute(Sender: TObject);
begin
     frmScanDocument:=TfrmScanDocument.Create(Application);
     frmScanDocument.ShowModal;
     frmScanDocument.Free;

end;

procedure TfrmMain.aStarosnaStrukturaExecute(Sender: TObject);
begin
     frmStarosnaStruktura:=TfrmStarosnaStruktura.Create(Application);
     frmStarosnaStruktura.ShowModal;
     frmStarosnaStruktura.Free;
end;

procedure TfrmMain.aStatusExecute(Sender: TObject);
begin
     frmStatus:=TfrmStatus.Create(Application);
     frmStatus.ShowModal;
     frmStatus.Free;
end;

procedure TfrmMain.aStranskiJaziciExecute(Sender: TObject);
begin
     frmStarnskiJazik:=TfrmStarnskiJazik.Create(Application);
     frmStarnskiJazik.ShowModal;
     frmStarnskiJazik.Free;
end;

procedure TfrmMain.aSZObrazovanieExecute(Sender: TObject);
begin
    frmSZObrazovanie:=TfrmSZObrazovanie.Create(Application);
    frmSZObrazovanie.ShowModal;
    frmSZObrazovanie.free;
end;

procedure TfrmMain.aTipKSExecute(Sender: TObject);
begin
   frmTipKS:=TfrmTipKS.Create(Application);
   frmTipKS.ShowModal;
   frmTipKS.Free;
end;

procedure TfrmMain.aTipNaPovredaExecute(Sender: TObject);
begin
     frmTipNaPovreda:=TfrmTipNaPovreda.Create(Application);
     frmTipNaPovreda.ShowModal;
     frmTipNaPovreda.Free;
end;

procedure TfrmMain.aTipNPKExecute(Sender: TObject);
begin
    frmTipNKM:=TfrmTipNKM.Create(Application);
    frmTipNKM.ShowModal;
    frmTipNKM.Free;
end;

procedure TfrmMain.aTipOtsustvoExecute(Sender: TObject);
begin
   frmTipOtsustvo:=TfrmTipOtsustvo.Create(Application);
   frmTipOtsustvo.ShowModal;
   frmTipOtsustvo.Free;
end;

procedure TfrmMain.aTipRVExecute(Sender: TObject);
begin
    frmTipRabotnoVreme:=TfrmTipRabotnoVreme.Create(Application);
    frmTipRabotnoVreme.ShowModal;
    frmTipRabotnoVreme.Free;
end;

procedure TfrmMain.aTreeViewExecute(Sender: TObject);
begin
    dxTree.Visible:=true;
end;

procedure TfrmMain.aUsloviPoteskiOdNormalniExecute(Sender: TObject);
begin
     frmKoeficientUsloviPoteki:=TfrmKoeficientUsloviPoteki.Create(Application);
     frmKoeficientUsloviPoteki.ShowModal;
     frmKoeficientUsloviPoteki.Free;
end;

procedure TfrmMain.aVeroispovedExecute(Sender: TObject);
begin
   frmVeroispoved:=TfrmVeroispoved.Create(Application);
   frmVeroispoved.ShowModal;
   frmVeroispoved.Free;

end;

procedure TfrmMain.aVestiniKvalifikaciiExecute(Sender: TObject);
begin
     frmVestiniKvalifikacii:=TfrmVestiniKvalifikacii.Create(Application);
     frmVestiniKvalifikacii.Tag:=2;
     frmVestiniKvalifikacii.ShowModal;
     frmVestiniKvalifikacii.Free;
end;

procedure TfrmMain.aVestiniKvalifikaciiMExecute(Sender: TObject);
begin
     frmVestiniKvalifikacii:=TfrmVestiniKvalifikacii.Create(Application);
     frmVestiniKvalifikacii.Tag:=4;
     frmVestiniKvalifikacii.ShowModal;
     frmVestiniKvalifikacii.Free;
end;

procedure TfrmMain.aVidDokumentExecute(Sender: TObject);
begin
   frmVidDokument:=TfrmVidDokument.Create(Application);
   frmVidDokument.ShowModal;
   frmVidDokument.Free;
end;

procedure TfrmMain.aVidOtkazExecute(Sender: TObject);
begin
     frmVidOtkaz:=TfrmVidOtkaz.Create(Application);
     frmVidOtkaz.ShowModal;
     frmVidOtkaz.Free;
end;

procedure TfrmMain.aVidVrabotuvanjeExecute(Sender: TObject);
begin
    frmVidVrabotuvanje:=TfrmVidVrabotuvanje.Create(Application);
    frmVidVrabotuvanje.ShowModal;
    frmVidVrabotuvanje.Free;
end;

procedure TfrmMain.aViewVraboteniExecute(Sender: TObject);
begin
     frmHRMVraboteni:=TfrmHRMVraboteni.Create(Self,false);
     frmHRMVraboteni.Tag:=1;
     frmHRMVraboteni.ShowModal;
     frmHRMVraboteni.Free;
end;

procedure TfrmMain.aVolonteriExecute(Sender: TObject);
begin
     frmHRMVraboteni:=TfrmHRMVraboteni.Create(Self,false);
     frmHRMVraboteni.Tag:=2;
     frmHRMVraboteni.ShowModal;
     frmHRMVraboteni.Free;
end;

procedure TfrmMain.aVraboteniDetalenExecute(Sender: TObject);
begin
      frmViewVraboteni:=TfrmViewVraboteni.Create(Application);
      frmViewVraboteni.ShowModal;
      frmViewVraboteni.Free;
end;

procedure TfrmMain.aVraboteniExecute(Sender: TObject);
begin
     frmHRMVraboteni:=TfrmHRMVraboteni.Create(Self,false);
     frmHRMVraboteni.Tag:=0;
     frmHRMVraboteni.ShowModal;
     frmHRMVraboteni.Free;
end;

procedure TfrmMain.aVraboteniStranskiJazikExecute(Sender: TObject);
begin
     frmStranskiJazikPregled:=TfrmStranskiJazikPregled.Create(Application);
     frmStranskiJazikPregled.ShowModal;
     frmStranskiJazikPregled.Free;
end;

procedure TfrmMain.aZacuvajSkin2Execute(Sender: TObject);
begin
     dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.cxBarEditItem1PropertiesChange(Sender: TObject);
begin
     dmRes.SkinPromeni(Sender);
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.dxBarButton29Click(Sender: TObject);
begin
     FormProba:=TFormProba.Create(Application);
     FormProba.ShowModal;
     FormProba.Free;
end;

procedure TfrmMain.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
     if ANewTab=rtAbout then
     begin
       aAbout.Execute;
       Allow:=False;
     end;
     if ANewTab=rtIzlez then
     begin
       aIzlez.Execute;
       Allow:=False;
     end;
end;

procedure TfrmMain.dxTreeClick(Sender: TObject);
begin
   tblOrgRMRE.Close;
   tblOrgRMRE.ParamByName('re').AsString:=dm.tblReID.AsString;
   tblOrgRMRE.Open;
end;

procedure TfrmMain.FormActivate(Sender: TObject);
begin
  //  if GridPanel1.Visible then dxNavBar2.SetFocus;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    dmRes.FreeDLL;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin

    dmKon.fibPateka.Connected := true;
    dmkon.aplikacija:='HRM';

    firma:=dmKon.re;

    if (dmKon.fibBaza.Connected=false) then
    begin
      Application.Terminate;
    end
    else
    begin
         otvoriTabeli;
         Application.HelpFile := ExtractFilePath(Application.ExeName) + 'HRM_Help.chm';  //pateka do help -fajlot
    end;
    dmRes.SkinLista(cxBarEditItem1);
    dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
     SpremiForma(Self);

  //cxSetEureka('biljana.trpkoska@codex.mk; danica.stojkova@codex.mk');
  //���� �����, �� ��� �� ������ ����������� �� ��
  frmMK:=TfrmMK.Create(Application);
  frmMK.ShowModal;
  frmMK.free;

     Pukaj.Enabled:=True;
     tblOrgRMRE.Close;
     tblOrgRMRE.ParamByName('re').Asinteger:=firma; //dmkon.firma_id;
     tblOrgRMRE.Open;
     nbItemOtsustva.Caption:='('+tblProveriStatusOtsKOLKU.AsString+') �������� �� ����������' ;

     dmMat.tblStatusGrupa.Locate('APP; NAZIV', VarArrayOf([dmkon.aplikacija,'������ �� ���� �� ��������']) , []);
     statusGrupaOtsustva:=dmMat.tblStatusGrupaID.Value;

     dmMat.tblStatus.Locate('GRUPA; VREDNOST', VarArrayOf([statusGrupaOtsustva, 11]) , []);
     odobren_plan:= dmMat.tblStatusID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� �����������', []);
     dogovorZaRabota:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� ��������������', []);
     reseniePreraspredelba:=dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������ �� ������� �����', []);
     baranjeGO:=dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� ������� �����', []);
     resenieGO:=dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� ��������� ���������', []);
     dogHonorarci:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� ������ �� ������� �����', []);
     resPrekinRabOdnos:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� �������� �����Ō�', []);
     dogSezonci:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','����� �� ������� �� �����������', []);
     aneksDogovor:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� ���������', []);
     dogVolonter:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� - ��ȣ��� �� ����� ���������', []);
     prijava:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� - �ģ��� �� ����� ���������', []);
     odjava:= dm.tblVidDokumentID.Value;

     dmOtsustvo.tblTipOtsustvo.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblTipOtsustvo.Open;

     dmOtsustvo.tblTipOtsustvo.Locate('NAZIV','������� �����', []);
     godisenOdmor:= dmOtsustvo.tblTipOtsustvoID.Value;

     dm.tblVidDokument.Locate('NAZIV','������� �� ������� ������� �����', []);
     potvrda_raboten_odnos:= dm.tblVidDokumentID.Value;

     dm.tblVidDokument.Locate('NAZIV','�ǣ��� �� ������������ �� ������� �����', []);
     izjava_prodolzen_ro:=dm.tblVidDokumentID.Value;

     dmKon.tblSysSetup.Locate('P1; P2', VarArrayOf([dmkon.aplikacija,'dokumenti']) , []);
     patekaDokScan:=dmKon.tblSysSetupV1.Value;

     if (dmKon.reporter_dll) then
  	   begin
		    dmRes.ReporterInit;
		    dmRes.GenerateReportsRibbon(dxRibbon1,dxRibbon1TabPreglediReporter,dxBarManager1);
       end;
end;

procedure TfrmMain.orgRMCreateNode(Sender: TObject; Node: TdxOcNode);
begin
with Node, dmSis.tblOrgRMRE do
  begin
   // if FindField('id').AsInteger > 50 then
    //  Width := FindField('id').AsInteger;
   // if FindField('height').AsInteger > 50 then
    //  Height := FindField('height').AsInteger;
  //  Color := FindField('color').AsInteger;
  //  Node.Text:=getd
   // Color := FindField('boja').AsInteger;
    Data := New(PNodeData);
  //  PNodeData(Data)^.Color := Color;
    //PNodeData(Data)^.slika := FindField('SLIKA').AsExtended;
    PNodeData(Data)^.Name := FindField('NAZIV_RE').AsString;
  end;
end;

procedure TfrmMain.orgRMDeletion(Sender: TObject; Node: TdxOcNode);
begin
 if Assigned(Node.Data) then
    Dispose(PNodeData(Node.Data));
end;

procedure TfrmMain.OtvoriTabeli;
begin
  dmMat.tblRE.Open;
  dmMat.tblValuta.Open;
  dmSis.tblVidVrabotuvanje.Open;
  dmSis.tblGrupaRM.Open;
  dmSis.tblGrupaPlata.Open;
  dmSis.tblRabotnoMesto.ParamByName('firma').AsInteger:=firma;
  dmSis.tblRabotnoMesto.Open;
  dmSis.tblObrazovanie.Open;
  dmSis.tblSistematizacija.ParamByName('firma').Value:=firma;
  dmSis.tblSistematizacija.Open;
  dmSis.tblDrzavjanstvo.Open;
  dmSis.tblStranskiJazici.Open;
  dmSis.tblOrgRMRE.Close;
  //dmSis.tblOrgRMRE.ParamByName('re').Asinteger:=firma;
  dmSis.tblOrgRMRE.Open;
  dmSis.tblTipKS.Open;
  dmSis.tblTipRabotnoVreme.Open;
  dmSis.tblReVoFirma.ParamByName('re').AsInteger:=firma;
  dmsis.tblReVoFirma.open;
  dm.tblRe.ParamByName('firma').AsInteger:=firma;
  dm.tblRe.Open;
 // tblRe.open;
  dmSis.tblRMRE.ParamByName('param').Value:=1;
  dmSis.tblRMRE.ParamByName('firma').AsInteger:=firma;
  dmSis.tblRMRE.Open;
//  dmsis.tblKategorja.ParamByName('rm').AsString:='%';
//  dmsis.tblKategorja.ParamByName('firma').asinteger:=firma;
//  dmSis.tblKategorja.open;
  dmSis.tblSZObrazovanie.Open;
  dmMat.tblPartner.Open;
  dmMat.tblStatus.ParamByName('app').Value:=dmkon.aplikacija;
  dmMat.tblStatus.Open;
  dmMat.tblStatusGrupa.ParamByName('app').Value:=dmkon.aplikacija;
  dmMat.tblStatusGrupa.Open;
  dmMat.tblSysApp.Open;
  dmmat.tblDrzava.Open;
  dmMat.tblMesto.Open;
  dmMat.tblOpstina.Open;
  dmMat.tblRegioni.Open;
  dmMat.tblFzo.Open;

  //dm.tblUsloviPoteskiOdNormalni.open;

  tblProveriStatusOts.ParamByName('app').Value:=dmkon.aplikacija;
  tblProveriStatusOts.Open;

  dm.tblVidDokument.Close;
  dm.tblVidDokument.open;

  dm.tblKategorijaRM.Open;

  dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
  dm.viewVraboteni.ParamByName('mb').Value:='%';
  dm.viewVraboteni.ParamByName('re').Value:='%';
  dm.viewVraboteni.Open;

  dm.qSetupD.Close;
  dm.qSetupD.ExecQuery;
  if not dm.qSetupD.FldByName['v1'].IsNull then
     pat_dokumenti:=dm.qSetupD.FldByName['v1'].Value;

  dm.qSetupD.Close;
  dm.qSetupD.ExecQuery;
  if not dm.qSetupD.FldByName['v1'].IsNull then
     pat_dokumenti:=dm.qSetupD.FldByName['v1'].Value;

  dm.qSetupMinDenGO.Close;
  dm.qSetupMinDenGO.ExecQuery;
  if ((not dm.qSetupMinDenGO.FldByName['v1'].IsNull) and (not dm.qSetupMinDenGO.FldByName['v2'].IsNull))  then
    begin
     min_den_go_v1:=dm.qSetupMinDenGO.FldByName['v1'].Value; // kolku meseci treba da ima minimum za da ima pravo na GO
     min_den_go_v2:=dm.qSetupMinDenGO.FldByName['v2'].Value; // kolku denovi mu se dava na vraboteniot ako ima pomalku od minimum po mesec
    end
  else
    begin
      min_den_go_v1:='0'; // kolku meseci treba da ima minimum za da ima pravo na GO
      min_den_go_v2:='0'; // kolku denovi mu se dava na vraboteniot ako ima pomalku od minimum po mesec
    end;
end;

procedure TfrmMain.PukajTimer(Sender: TObject);
begin
   tblProveriStatusOts.Close;
   tblProveriStatusOts.ParamByName('app').Value:=dmkon.aplikacija;
   tblProveriStatusOts.Open;
   nbItemOtsustva.Caption:='('+tblProveriStatusOtsKOLKU.AsString+') �������� �� ����������' ;

end;

end.
