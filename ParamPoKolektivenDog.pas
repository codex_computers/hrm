unit ParamPoKolektivenDog;

{*******************************************************}
{                                                       }
{     ��������� :  ������ �������                      }
{                                                       }
{     ����� : 25.08.2011                                }
{                                                       }
{     ������ : 1.1.1.14                                }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon, ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit, cxCalendar,
  cxGroupBox, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxPScxPivotGridLnk, dxPSdxDBOCLnk, cxSplitter,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinOffice2013White, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmParamKolektivenDog = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_ZENI: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_MAZI: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_TEL_OST: TcxGridDBColumn;
    cxGrid1DBTableView1DODATOK: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Period: TcxGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    datumOd: TcxDBDateEdit;
    DatumDo: TcxDBDateEdit;
    cxGrid1DBTableView1GO_STAZ_1: TcxGridDBColumn;
    cxGrid1DBTableView1GO_STAZ_2: TcxGridDBColumn;
    cxGrid1DBTableView1GO_STAZ_3: TcxGridDBColumn;
    cxGrid1DBTableView1INVALID_DENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1CUVANJE_DETE: TcxGridDBColumn;
    Otsustva: TcxGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    maxPlateno: TcxDBTextEdit;
    maxneplateno: TcxDBTextEdit;
    Plata: TcxGroupBox;
    Label8: TLabel;
    Staz: TcxDBTextEdit;
    Label19: TLabel;
    cxGrid1DBTableView1MAX_NEPLATENI_DENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1MAX_TIPOTSUSTVO_DEN: TcxGridDBColumn;
    ParametriZaResenieGO: TcxGroupBox;
    cxGroupBox3: TcxGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    prv: TcxDBTextEdit;
    vtor: TcxDBTextEdit;
    tret: TcxDBTextEdit;
    cxGroupBox2: TcxGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Zeni: TcxDBTextEdit;
    Mazi: TcxDBTextEdit;
    Telesno: TcxDBTextEdit;
    Invalid: TcxDBTextEdit;
    CUVANJE_DETE: TcxDBTextEdit;
    Label21: TLabel;
    cxGroupBox7: TcxGroupBox;
    MinBR: TcxDBTextEdit;
    Label4: TLabel;
    Label20: TLabel;
    maxBr2: TcxDBTextEdit;
    maxBr1: TcxDBTextEdit;
    Label22: TLabel;
    cxGrid1DBTableView1MAX_DENOVI_GO1: TcxGridDBColumn;
    cxGrid1DBTableView1MAX_DENOVI_GO2: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    procedure FormCreate(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParamKolektivenDog: TfrmParamKolektivenDog;

implementation

uses dmUnit, Utils;

{$R *.dfm}

procedure TfrmParamKolektivenDog.aAzurirajExecute(Sender: TObject);
begin
  inherited;
  datumOd.SetFocus;
end;

procedure TfrmParamKolektivenDog.aHelpExecute(Sender: TObject);
begin
   Application.HelpContext(125);
end;

procedure TfrmParamKolektivenDog.aNovExecute(Sender: TObject);
begin
  inherited;
  datumOd.SetFocus;
end;

procedure TfrmParamKolektivenDog.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
    end;
  end;
end;

procedure TfrmParamKolektivenDog.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblParamKolektivenDog.Open;
end;

end.
