inherited frmGrupaPlata: TfrmGrupaPlata
  Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1087#1083#1072#1090#1072
  ClientHeight = 578
  ClientWidth = 679
  ExplicitWidth = 695
  ExplicitHeight = 616
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 679
    Height = 254
    ExplicitWidth = 679
    ExplicitHeight = 254
    inherited cxGrid1: TcxGrid
      Width = 675
      Height = 250
      ExplicitWidth = 675
      ExplicitHeight = 250
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsGrupaPlata
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          SortIndex = 0
          SortOrder = soAscending
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 264
        end
        object cxGrid1DBTableView1VALUTA: TcxGridDBColumn
          DataBinding.FieldName = 'VALUTA'
          Width = 60
        end
        object cxGrid1DBTableView1MINIMALNA: TcxGridDBColumn
          DataBinding.FieldName = 'MINIMALNA'
          Width = 86
        end
        object cxGrid1DBTableView1MAKSIMALNA: TcxGridDBColumn
          DataBinding.FieldName = 'MAKSIMALNA'
          Width = 81
        end
        object cxGrid1DBTableView1STAPKA: TcxGridDBColumn
          DataBinding.FieldName = 'STAPKA'
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 380
    Width = 679
    Height = 175
    ExplicitTop = 380
    ExplicitWidth = 679
    ExplicitHeight = 175
    inherited Label1: TLabel
      Left = 500
      Top = 70
      Visible = False
      ExplicitLeft = 500
      ExplicitTop = 70
    end
    object Label2: TLabel [1]
      Left = 42
      Top = 23
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 42
      Top = 45
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1042#1072#1083#1091#1090#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 556
      Top = 67
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsGrupaPlata
      TabOrder = 5
      Visible = False
      ExplicitLeft = 556
      ExplicitTop = 67
    end
    inherited OtkaziButton: TcxButton
      Left = 567
      Top = 131
      TabOrder = 6
      ExplicitLeft = 567
      ExplicitTop = 131
    end
    inherited ZapisiButton: TcxButton
      Left = 486
      Top = 132
      Height = 24
      TabOrder = 4
      ExplicitLeft = 486
      ExplicitTop = 132
      ExplicitHeight = 24
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 99
      Top = 20
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsGrupaPlata
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 350
    end
    object txtValuta: TcxDBTextEdit
      Tag = 1
      Left = 99
      Top = 42
      BeepOnEnter = False
      DataBinding.DataField = 'VALUTA'
      DataBinding.DataSource = dmSis.dsGrupaPlata
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object cbValuta: TcxDBLookupComboBox
      Left = 143
      Top = 42
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'VALUTA'
      DataBinding.DataSource = dmSis.dsGrupaPlata
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'id'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsValuta
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 306
    end
    object cxGroupBox1: TcxGroupBox
      Left = 34
      Top = 69
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = ' '#1043#1088#1072#1085#1080#1094#1080' ('#1055#1083#1072#1090#1072')'
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 3
      DesignSize = (
        327
        91)
      Height = 91
      Width = 327
      object Label4: TLabel
        Left = 1
        Top = 22
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1080#1085#1080#1084#1072#1083#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 139
        Top = 22
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 148
        Top = 61
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1072#1087#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtMin: TcxDBSpinEdit
        Left = 13
        Top = 36
        DataBinding.DataField = 'MINIMALNA'
        DataBinding.DataSource = dmSis.dsGrupaPlata
        Properties.ImmediatePost = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 123
      end
      object txtMaks: TcxDBSpinEdit
        Left = 161
        Top = 36
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'MAKSIMALNA'
        DataBinding.DataSource = dmSis.dsGrupaPlata
        Properties.ImmediatePost = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = txtMaksExit
        OnKeyDown = txtMaksKeyDown
        Width = 133
      end
      object txtStapka: TcxDBTextEdit
        Tag = 1
        Left = 205
        Top = 58
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'STAPKA'
        DataBinding.DataSource = dmSis.dsGrupaPlata
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 89
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 679
    ExplicitWidth = 679
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 555
    Width = 679
    ExplicitTop = 555
    ExplicitWidth = 679
  end
  inherited PopupMenu1: TPopupMenu
    Left = 368
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 448
    Top = 216
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1087#1083#1072#1090#1080
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40248.562297928240000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
