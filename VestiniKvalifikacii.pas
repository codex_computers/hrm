unit VestiniKvalifikacii;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMemo, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox,
  cxRadioGroup, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxScreenTip,
  dxCustomHint;

type
  TfrmVestiniKvalifikacii = class(TfrmMaster)
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1MAJCIN_JAZIK: TcxGridDBColumn;
    cxGrid1DBTableView1DRUSTVENI_V: TcxGridDBColumn;
    cxGrid1DBTableView1ORGANIZACISKI_V: TcxGridDBColumn;
    cxGrid1DBTableView1TEHNICKI_V: TcxGridDBColumn;
    cxGrid1DBTableView1KOMPJUTERSKI_V: TcxGridDBColumn;
    cxGrid1DBTableView1UMETNICKI_V: TcxGridDBColumn;
    cxGrid1DBTableView1DRUGI_V: TcxGridDBColumn;
    cxGrid1DBTableView1VOZACKA_DOZVOLA: TcxGridDBColumn;
    cxGrid1DBTableView1KATEGORIJA_VD: TcxGridDBColumn;
    cxGrid1DBTableView1DOPOLNITELNI_INFORMACII: TcxGridDBColumn;
    cxGrid1DBTableView1PRILOZI: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn;
    Label10: TLabel;
    ID_MOLBA: TcxDBTextEdit;
    IMEPREZIME: TcxDBLookupComboBox;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    MB_VRABOTEN: TcxDBTextEdit;
    Label11: TLabel;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label16: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    KOMPJUTERSKI_V: TcxDBMemo;
    TEHNICKI_V: TcxDBMemo;
    ORGANIZACISKI_V: TcxDBMemo;
    DRUSTVENI_V: TcxDBMemo;
    Label3: TLabel;
    MajcinJazik: TcxDBTextEdit;
    UMETNICKI_V: TcxDBMemo;
    Label6: TLabel;
    Label7: TLabel;
    DRUGI_V: TcxDBMemo;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Kategorija: TcxDBTextEdit;
    cxDBRadioGroup1: TcxDBRadioGroup;
    PRILOZI: TcxDBMemo;
    Label9: TLabel;
    DOPOLNITELNI_INFORMACII: TcxDBMemo;
    Label12: TLabel;
    Label13: TLabel;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    ActionList2: TActionList;
    aStranskiJazici: TAction;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VOZACKA_DOZVOLA1: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    procedure aStranskiJaziciExecute(Sender: TObject);
    procedure DRUSTVENI_VDblClick(Sender: TObject);
    procedure ORGANIZACISKI_VDblClick(Sender: TObject);
    procedure TEHNICKI_VDblClick(Sender: TObject);
    procedure KOMPJUTERSKI_VDblClick(Sender: TObject);
    procedure UMETNICKI_VDblClick(Sender: TObject);
    procedure DRUGI_VDblClick(Sender: TObject);
    procedure DOPOLNITELNI_INFORMACIIDblClick(Sender: TObject);
    procedure PRILOZIDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVestiniKvalifikacii: TfrmVestiniKvalifikacii;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,
  Utils, LVKStranskiJazici, Notepad, Molbi;

{$R *.dfm}

procedure TfrmVestiniKvalifikacii.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2) then
       MB_VRABOTEN.SetFocus;
    if (tag = 4) then
       ID_MOLBA.SetFocus;
    if tag = 5 then
       begin
         MajcinJazik.SetFocus;
       end;
    if tag = 3 then
       begin
         MajcinJazik.SetFocus;
       end;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmVestiniKvalifikacii.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if (tag = 2) then
       MB_VRABOTEN.SetFocus;
    if (tag = 4) then
       ID_MOLBA.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if tag = 5 then
       begin
         dmSis.tblLVKID_MOLBA.Value:=dmSis.tblMolbaID.Value;
         MajcinJazik.SetFocus;
       end;
    if tag = 3 then
       begin
         dmSis.tblLVKMB_VRABOTEN.Value:=dm.tblLicaVraboteniMB.Value;
         MajcinJazik.SetFocus;
       end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmVestiniKvalifikacii.aStranskiJaziciExecute(Sender: TObject);
begin
  inherited;
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
     begin
      if cxGrid1DBTableView1.Controller.SelectedRecordCount > 0 then
         begin
           frmStranskiJazik:=TfrmStranskiJazik.Create(Self, false);
           frmStranskiJazik.ShowModal;
           frmStranskiJazik.Free;
         end
      else ShowMessage('������ ������������ ����� !!!');
     end
   else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmVestiniKvalifikacii.DOPOLNITELNI_INFORMACIIDblClick(
  Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKDOPOLNITELNI_INFORMACII.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKDOPOLNITELNI_INFORMACII.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.DRUGI_VDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKDRUGI_V.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKDRUGI_V.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.DRUSTVENI_VDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKDRUSTVENI_V.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKDRUSTVENI_V.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if tag = 3 then
     begin
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 0) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.FullRefresh;
     end;
  if (tag = 5) and (frmMolbi.Tag = 1) then
     begin
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:=dmSis.tblOglasID.Value;
       dmSis.tblMolba.FullRefresh;
     end;
end;

procedure TfrmVestiniKvalifikacii.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 2 then
     begin
       dmSis.tblLVK.close;
       dmSis.tblLVK.ParamByName('MB').Value:='%';
       dmSis.tblLVK.ParamByName('MolbaID').Value:='0';
       dmSis.tblLVK.open;

       dm.tblLica.Close;
       dm.tblLica.ParamByName('MB').Value:='%';
       dm.tblLica.Open;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;
  if tag = 3 then
     begin
       dmSis.tblLVK.close;
       dmSis.tblLVK.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dmSis.tblLVK.ParamByName('MolbaID').Value:='0';
       dmSis.tblLVK.open;

       dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
       dm.tblLica.FullRefresh;

       ID_MOLBA.Visible:=False;
       IMEPREZIME.Visible:=False;
       Label10.Visible:=False;

       MB_VRABOTEN.Tag:=1;
       VRABOTENNAZIV.Tag:=1;
     end;

  if tag = 4 then
     begin
       cxGrid1DBTableView1ID_MOLBA.Visible:=true;
       cxGrid1DBTableView1IMEPREZIME.Visible:=True;
       cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
       cxGrid1DBTableView1VRABOTENNAZIV.Visible:=false;
       dmSis.tblLVK.close;
       dmSis.tblLVK.ParamByName('MB').Value:='0';
       dmSis.tblLVK.ParamByName('MolbaID').Value:='%';
       dmSis.tblLVK.open;

       dmSis.tblMolba.Close;
       dmSis.tblMolba.ParamByName('ID').Value:='%';
       dmSis.tblMolba.ParamByName('oglasID').Value:='%';
       dmSis.tblMolba.Open;

       VRABOTENNAZIV.Visible:=False;
       MB_VRABOTEN.Visible:=False;
       Label11.Visible:=False;

       ID_MOLBA.Tag:=1;
       IMEPREZIME.Tag:=1;
     end;
  if tag = 5 then
     begin
        cxGrid1DBTableView1ID_MOLBA.Visible:=true;
        cxGrid1DBTableView1IMEPREZIME.Visible:=True;
        cxGrid1DBTableView1MB_VRABOTEN.Visible:=false;
        cxGrid1DBTableView1VRABOTENNAZIV.Visible:=false;
        dmSis.tblLVK.close;
        dmSis.tblLVK.ParamByName('MB').Value:='0';
        dmSis.tblLVK.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblLVK.open;

        dmSis.tblMolba.ParamByName('ID').Value:=dmSis.tblMolbaID.Value;
        dmSis.tblMolba.ParamByName('oglasID').Value:='%';
        dmSis.tblMolba.FullRefresh;

        VRABOTENNAZIV.Visible:=False;
        MB_VRABOTEN.Visible:=False;
        Label11.Visible:=False;

        ID_MOLBA.Tag:=1;
        IMEPREZIME.Tag:=1;
     end;
end;

procedure TfrmVestiniKvalifikacii.KOMPJUTERSKI_VDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKKOMPJUTERSKI_V.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKKOMPJUTERSKI_V.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.ORGANIZACISKI_VDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKORGANIZACISKI_V.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKORGANIZACISKI_V.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.PRILOZIDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKPRILOZI.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKPRILOZI.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.TEHNICKI_VDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKTEHNICKI_V.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKTEHNICKI_V.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmVestiniKvalifikacii.UMETNICKI_VDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblLVKUMETNICKI_V.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblLVKUMETNICKI_V.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

end.
