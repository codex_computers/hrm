unit Praznici;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.18                  }
{                                       }
{   28.02.2012                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit, cxCalendar,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, dxRibbonCustomizationForm,
  System.Actions, dxPScxSchedulerLnk;

type
  TfrmPraznici = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VEROISPOVEDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NACIONALNOSTNAZIV: TcxGridDBColumn;
    DATUM: TcxDBDateEdit;
    NAZIV: TcxDBTextEdit;
    Label2: TLabel;
    Label3: TLabel;
    VEROISPOVED: TcxDBLookupComboBox;
    NACIONALNOST: TcxDBLookupComboBox;
    Label4: TLabel;
    Label5: TLabel;
    NACIONALNOSTID: TcxDBTextEdit;
    VEROISPOVEDID: TcxDBTextEdit;
    cxGrid1DBTableView1VEROISPOVED: TcxGridDBColumn;
    cxGrid1DBTableView1NACIONALNOST: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    aKopirajPraznici: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    cbGodina: TcxBarEditItem;
    procedure FormCreate(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aKopirajPrazniciExecute(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPraznici: TfrmPraznici;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, dmUnitOtsustvo,
  Utils, Nacionalnost, Veroispoved;

{$R *.dfm}

procedure TfrmPraznici.aKopirajPrazniciExecute(Sender: TObject);
begin
  inherited;
  dmOtsustvo.pKopirajPraznici.ParamByName('GODINA').Value := cbGodina.EditValue;
  dmOtsustvo.pKopirajPraznici.ExecProc;
  dmOtsustvo.tblPraznici.ParamByName('godina').Value:=cbGodina.EditValue;
  dmOtsustvo.tblPraznici.FullRefresh;
end;

procedure TfrmPraznici.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.ReportTitle.Text := Caption + ' - '+ intToStr(cbGodina.EditValue);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPraznici.cbGodinaChange(Sender: TObject);
begin
  inherited;
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
      begin
         dmOtsustvo.tblPraznici.ParamByName('godina').Value:=cbGodina.EditValue;
         dmOtsustvo.tblPraznici.FullRefresh;
      end;
end;

procedure TfrmPraznici.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
        VK_INSERT:
        begin
          if (Sender = NACIONALNOSTID) or (Sender = NACIONALNOST) then
             begin
               frmNacionalnost:=TfrmNacionalnost.Create(Application);
               frmNacionalnost.Tag:=1;
               frmNacionalnost.ShowModal;
               frmNacionalnost.Free;
               dmOtsustvo.tblPrazniciNACIONALNOST.Value:=dmOtsustvo.tblNacionalnostID.Value;
             end;
          if (Sender = VEROISPOVEDID) or (Sender = VEROISPOVED) then
             begin
               frmVeroispoved:=TfrmVeroispoved.Create(Application);
               frmVeroispoved.Tag:=1;
               frmVeroispoved.ShowModal;
               frmVeroispoved.Free;
               dmOtsustvo.tblPrazniciVEROISPOVED.Value:=dmOtsustvo.tblVeroispovedID.Value;
             end;
        end;

    end;
end;

procedure TfrmPraznici.FormCreate(Sender: TObject);
begin
  inherited;
   dmOtsustvo.tblPraznici.close;
   dmOtsustvo.tblPraznici.ParamByName('godina').Value:=cbGodina.EditValue;
   dmOtsustvo.tblPraznici.Open;
   dmOtsustvo.tblNacionalnost.Open;
   dmOtsustvo.tblVeroispoved.Open;
   cbGodina.EditValue:=dmKon.godina;
end;

end.
