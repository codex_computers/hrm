inherited frmMolbi: TfrmMolbi
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1052#1086#1083#1073#1080
  ClientHeight = 720
  ClientWidth = 931
  ExplicitLeft = -49
  ExplicitWidth = 947
  ExplicitHeight = 758
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 931
    Height = 162
    ExplicitWidth = 931
    ExplicitHeight = 162
    inherited cxGrid1: TcxGrid
      Width = 927
      Height = 150
      ExplicitWidth = 927
      ExplicitHeight = 150
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsMolba
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_OGLAS: TcxGridDBColumn
          DataBinding.FieldName = 'ID_OGLAS'
          Visible = False
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'MOLBABROJ'
          Width = 77
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 124
        end
        object cxGrid1DBTableView1BROJNAOGLAS: TcxGridDBColumn
          DataBinding.FieldName = 'BROJGODINA'
          Width = 79
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 100
        end
        object cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'IMEPREZIME'
          Width = 213
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1MOMINSKO_PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'MOMINSKO_PREZIME'
          Width = 124
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 98
        end
        object cxGrid1DBTableView1DATUM_RADJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_RADJANJE'
          Width = 117
        end
        object cxGrid1DBTableView1BRACNA_SOSTOJBA1: TcxGridDBColumn
          DataBinding.FieldName = 'BRACNA_SOSTOJBA'
          Visible = False
        end
        object cxGrid1DBTableView1BRACNA_SOSTOJBA: TcxGridDBColumn
          Caption = #1041#1088#1072#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          DataBinding.FieldName = 'Brak'
          Width = 106
        end
        object cxGrid1DBTableView1POL: TcxGridDBColumn
          DataBinding.FieldName = 'PolNaziv'
          Width = 54
        end
        object cxGrid1DBTableView1ZDR_SOSTOJBA: TcxGridDBColumn
          DataBinding.FieldName = 'ZDR_SOSTOJBA'
          Width = 134
        end
        object cxGrid1DBTableView1ID_NACIONALNOST: TcxGridDBColumn
          DataBinding.FieldName = 'ID_NACIONALNOST'
          Visible = False
        end
        object cxGrid1DBTableView1NACIONALNOSTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NACIONALNOSTNAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1ID_VEROISPOVED: TcxGridDBColumn
          DataBinding.FieldName = 'ID_VEROISPOVED'
          Visible = False
        end
        object cxGrid1DBTableView1VEROISPOVEDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VEROISPOVEDNAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 160
        end
        object cxGrid1DBTableView1SLIKA: TcxGridDBColumn
          DataBinding.FieldName = 'SLIKA'
          PropertiesClassName = 'TcxImageProperties'
          Properties.GraphicClassName = 'TJPEGImage'
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
    object SplitterdPanel: TcxSplitter
      Left = 2
      Top = 152
      Width = 927
      Height = 8
      HotZoneClassName = 'TcxMediaPlayer8Style'
      AlignSplitter = salBottom
      AllowHotZoneDrag = False
      Control = dPanel
    end
  end
  inherited dPanel: TPanel
    Top = 288
    Width = 931
    Height = 409
    ExplicitTop = 288
    ExplicitWidth = 931
    ExplicitHeight = 409
    inherited Label1: TLabel
      Left = 552
      Top = 6
      Visible = False
      ExplicitLeft = 552
      ExplicitTop = 6
    end
    object Label12: TLabel [1]
      Left = 57
      Top = 348
      Width = 95
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [2]
      Left = 57
      Top = 75
      Width = 95
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel [3]
      Left = 57
      Top = 22
      Width = 95
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel [4]
      Left = 32
      Top = 42
      Width = 120
      Height = 32
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 608
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsMolba
      TabOrder = 2
      Visible = False
      ExplicitLeft = 608
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 830
      Top = 369
      TabOrder = 8
      ExplicitLeft = 830
      ExplicitTop = 369
    end
    inherited ZapisiButton: TcxButton
      Left = 749
      Top = 369
      TabOrder = 7
      ExplicitLeft = 749
      ExplicitTop = 369
    end
    object ID_RM_RE: TcxDBTextEdit
      Tag = 1
      Left = 158
      Top = 345
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1079#1072' '#1082#1086#1077' '#1089#1077' '#1086#1076#1085#1077#1089#1091#1074#1072' '#1087#1088#1080#1112#1072#1074#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM_RE'
      DataBinding.DataSource = dmSis.dsMolba
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clBlack
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 55
    end
    object ID_RM_RE_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 214
      Top = 345
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1079#1072' '#1082#1086#1077' '#1089#1077' '#1086#1076#1085#1077#1089#1091#1074#1072' '#1087#1088#1080#1112#1072#1074#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM_RE'
      DataBinding.DataSource = dmSis.dsMolba
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1064#1080#1092#1088#1072
          Width = 200
          FieldName = 'ID'
        end
        item
          Caption = #1056#1072#1073#1086#1090#1085#1072' '#1045#1076#1080#1085#1080#1094#1072
          Width = 600
          FieldName = 'RENAZIV'
        end
        item
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
          Width = 600
          FieldName = 'NAZIV_RM'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dsRMRE
      Style.Color = clWhite
      StyleDisabled.TextColor = clBlack
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 315
    end
    object ID_OGLASNaziv: TcxDBLookupComboBox
      Left = 158
      Top = 73
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' '#1089#1087#1086#1088#1077#1076' '#1082#1086#1112' '#1089#1077' '#1074#1088#1096#1080' '#1087#1088#1080#1112#1072#1074#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_OGLAS'
      DataBinding.DataSource = dmSis.dsMolba
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'statusNaziv'
        end
        item
          Width = 100
          FieldName = 'BROJGODINA'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dsOglas
      Properties.OnEditValueChanged = ID_OGLASNazivPropertiesEditValueChanged
      Style.Color = clWhite
      StyleDisabled.TextColor = clBackground
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = ID_OGLASNazivExit
      OnKeyDown = EnterKakoTab
      Width = 162
    end
    object MOLBABROJ: TcxDBTextEdit
      Tag = 1
      Left = 158
      Top = 19
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1084#1086#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'MOLBABROJ'
      DataBinding.DataSource = dmSis.dsMolba
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = MBExit
      OnKeyDown = EnterKakoTab
      Width = 162
    end
    object cxGroupBox1: TcxGroupBox
      Left = 24
      Top = 113
      Caption = #1051#1080#1095#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1085#1072' '#1083#1080#1094#1077#1090#1086' '#1082#1086#1077' '#1112#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072' '#1084#1086#1083#1073#1072#1090#1072
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 4
      Height = 216
      Width = 881
      object Label4: TLabel
        Left = 34
        Top = 25
        Width = 95
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1041#1088'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 34
        Top = 52
        Width = 95
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 98
        Top = 79
        Width = 31
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 8
        Top = 106
        Width = 121
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1086#1084#1080#1085#1089#1082#1086' '#1087#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 15
        Top = 133
        Width = 114
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 287
        Top = 27
        Width = 140
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1076#1088#1072#1089#1090#1074#1077#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 287
        Top = 41
        Width = 140
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1089#1086#1089#1090#1086#1112#1073#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 329
        Top = 152
        Width = 98
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 340
        Top = 179
        Width = 87
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1077#1088#1086#1080#1089#1087#1086#1074#1077#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MB: TcxDBTextEdit
        Left = 135
        Top = 22
        BeepOnEnter = False
        DataBinding.DataField = 'MB'
        DataBinding.DataSource = dmSis.dsMolba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Properties.OnEditValueChanged = MBPropertiesEditValueChanged
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = MBExit
        OnKeyDown = EnterKakoTab
        Width = 162
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 135
        Top = 49
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dmSis.dsMolba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 162
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 135
        Top = 76
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dmSis.dsMolba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 162
      end
      object MOMINSKO_PREZIME: TcxDBTextEdit
        Left = 135
        Top = 103
        BeepOnEnter = False
        DataBinding.DataField = 'MOMINSKO_PREZIME'
        DataBinding.DataSource = dmSis.dsMolba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 162
      end
      object DATUM_RADJANJE: TcxDBDateEdit
        Left = 135
        Top = 130
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_RADJANJE'
        DataBinding.DataSource = dmSis.dsMolba
        Style.Color = clWhite
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 162
      end
      object BracnaSostojba: TcxDBRadioGroup
        Left = 31
        Top = 157
        TabStop = False
        Caption = #1041#1088#1072#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
        DataBinding.DataField = 'BRACNA_SOSTOJBA'
        DataBinding.DataSource = dmSis.dsMolba
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1053#1077
            Value = 0
          end
          item
            Caption = #1044#1072
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfFlat
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfFlat
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 43
        Width = 98
      end
      object Pol: TcxDBRadioGroup
        Left = 135
        Top = 157
        TabStop = False
        Caption = #1055#1086#1083
        DataBinding.DataField = 'POL'
        DataBinding.DataSource = dmSis.dsMolba
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1046#1077#1085#1089#1082#1080
            Value = 2
          end
          item
            Caption = #1052#1072#1096#1082#1080
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfFlat
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfFlat
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 43
        Width = 162
      end
      object ZDR_SOSTOJBA: TcxDBMemo
        Left = 433
        Top = 24
        DataBinding.DataField = 'ZDR_SOSTOJBA'
        DataBinding.DataSource = dmSis.dsMolba
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 119
        Width = 240
      end
      object NACIONALNOSTID: TcxDBTextEdit
        Left = 433
        Top = 149
        BeepOnEnter = False
        DataBinding.DataField = 'ID_NACIONALNOST'
        DataBinding.DataSource = dmSis.dsMolba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object NACIONALNOST: TcxDBLookupComboBox
        Left = 489
        Top = 149
        BeepOnEnter = False
        DataBinding.DataField = 'ID_NACIONALNOST'
        DataBinding.DataSource = dmSis.dsMolba
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmOtsustvo.dsNacionalnost
        Style.Color = clWhite
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 184
      end
      object VEROISPOVED: TcxDBLookupComboBox
        Left = 489
        Top = 176
        BeepOnEnter = False
        DataBinding.DataField = 'ID_VEROISPOVED'
        DataBinding.DataSource = dmSis.dsMolba
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmOtsustvo.dsVeroispoved
        Style.Color = clWhite
        TabOrder = 11
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 184
      end
      object VEROISPOVEDID: TcxDBTextEdit
        Left = 433
        Top = 176
        BeepOnEnter = False
        DataBinding.DataField = 'ID_VEROISPOVED'
        DataBinding.DataSource = dmSis.dsMolba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 10
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 55
      end
      object Slika: TcxDBImage
        Left = 699
        Top = 22
        Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1083#1080#1094#1077#1090#1086
        TabStop = False
        DataBinding.DataField = 'SLIKA'
        DataBinding.DataSource = dmSis.dsMolba
        Properties.GraphicClassName = 'TJPEGImage'
        Properties.Stretch = True
        TabOrder = 12
        Height = 131
        Width = 158
      end
      object Button1: TButton
        Left = 715
        Top = 159
        Width = 126
        Height = 25
        Hint = 'Bitmap image'
        Caption = '...'
        TabOrder = 13
        TabStop = False
        OnClick = Button1Click
      end
    end
    object DatumPodnesuvanje: TcxDBDateEdit
      Left = 158
      Top = 46
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1084#1086#1083#1073#1072
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dmSis.dsMolba
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 162
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 931
    ExplicitWidth = 931
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 697
    Width = 931
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', F' +
          '10 - '#1044#1086#1089#1080#1077', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 697
    ExplicitWidth = 931
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited PopupMenu1: TPopupMenu
    object N2: TMenuItem [0]
      Action = aObrazovanieObuka
    end
    object N4: TMenuItem [1]
      Action = aRabotnoIskustvo
    end
    object N3: TMenuItem [2]
      Action = aLVK
    end
    object N5: TMenuItem [3]
      Action = aKontakt
    end
    object N6: TMenuItem [4]
      Action = aDosie
    end
  end
  inherited dxBarManager1: TdxBarManager
    Top = 224
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 64
      FloatClientHeight = 156
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 624
      FloatClientWidth = 111
      FloatClientHeight = 126
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 831
      FloatClientHeight = 104
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
    object dxBarManager1Bar5: TdxBar [4]
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 985
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [5]
      Caption = #1055#1088#1077#1075#1083#1077#1076
      CaptionButtons = <>
      DockedLeft = 565
      DockedTop = 0
      FloatLeft = 927
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aObrazovanieObuka
      Category = 0
      LargeImageIndex = 2
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aRabotnoIskustvo
      Category = 0
      Glyph.Data = {
        36240000424D3624000000000000360000002800000030000000300000000100
        2000000000000024000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000060200093815005B6624
        00A5873100D7973800EE983800F0882E00DB672200AD3C1400660A03000F0000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000001000002491C006B993A00E9B24C00FFBA54
        00FFBA5400FFB65000FFB44E00FFB44E00FFB24C00FFA94300FF963500F04F1A
        007E040100070000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000D0400127D3200BBBB5500FFCB6500FFCB6500FFC963
        00FFC76100FFC55F00FFC15B00FFBB5500FFB54F00FFB14B00FFB04A00FFA842
        00FF7C2B00CE1407001F00000000000000000000000000000000000000000000
        00000000000046311C4C9E6730B6B9702AD9A05E1AC376410D934C2805622515
        05310704020A0000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000C0400118C3A00D0C96300FFD06A00FFCF6900FFCD6700FFCD67
        00FFCF6700FFE17700FFE47A00FFD97000FFC35C00FFBC5600FFB24C00FFAF49
        00FFAC4600FF8A3000E11407001E000000000000000000000000000000000000
        00007D5E3D7DE9A660FDE8A664FFE8A564FEE39B55FEDB8C3DFED27A21FECA68
        03FEC26100F5AB5702CF8443049E5A30056D311B053C0C080210000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000001813300B5CC6803FFD36D00FFD26C00FFD26C00FFD16B00FFD56C
        00FFEDA037FFF9E3BEFFFFF3DFFFFFDEA4FFFB9B0CFFCA6200FFC25C00FFB54F
        00FFAF4900FFAE4800FF7C2B00CE040100070000000000000000000000004A3A
        2C4AF3BA7DFDEEAF70FFF0AF6EFFF0AE6BFFEFAC69FFEEAB68FFEEAA66FFECA8
        65FFE59D57FEDD8E41FED6812DFED07519FECB6806FEC86300FAB45B02D88C48
        04A8633405783A1F0547120B0517000000000000000000000000000000000000
        0000481D005EC7640AFFD87302FFD67000FFD67000FFD67000FFD56F00FFD67F
        20FFEBF4FDFFF5F7FAFFFDFEFFFFFFFFFFFFFFF8E6FFEB8100FFC55F00FFC35D
        00FFB54F00FFB14B00FFA94300FF501A007E000000000000000000000000C69F
        79CBF2B980FFF0B070FFEFAF6DFFEFAD6AFFEEAB68FFEDA965FFECA762FFEBA7
        61FFEBA55EFFEAA35CFFE9A25AFFE9A159FFE8A058FFE3994FFFDC8C3CFFD680
        2AFFD1751AFFCB6B0AFECB6300FDC05D00E46F5E4EAE4646477B2B2B2B4B5E5D
        5C68AD520CEAE0841CFFDA7400FFDA7400FFDA7400FFDA7400FFD97100FFCB88
        49FFE8EEF4FFF0F0F0FFF7F7F7FFFCFCFEFFFFFFFFFFF38A00FFC96200FFC862
        00FFC35D00FFB14B00FFB14B00FF963600F1090300100000000000000000FACC
        9BFCF2BA81FFF0B070FFEFAF6DFFEFAD6AFFEEAB68FFEDA965FFECA762FFEBA6
        60FFEAA45DFFE9A25AFFE8A058FFE79F55FFE69D52FFE79C51FFE69A4EFFE59A
        4DFFE4984BFFE49749FFDF9244FFDC8730FFB2A294F6A4A5A6FEA0A0A0FEB395
        81FECC6D13FFE28619FFDE7800FFDE7800FFDE7800FFDE7800FFDF7800FFC15A
        00FFDAC6B8FFEAF1F7FFF0F4FAFFF3FBFFFFF4BE73FFDF7600FFCE6800FFCA64
        00FFC66000FFBC5600FFB24C00FFAA4400FF3E1500660000000000000000FDCD
        9FFEF2BB84FFF1B16FFFF0AF6DFFEFAD6BFFEEAB68FFEDA965FFECA863FFEBA6
        60FFEAA45DFFE9A25BFFE8A158FFE89F55FFE79D53FFE69B50FFE5994DFFE498
        4BFFE39648FFE29446FFE19444FFE3903AFFCCB8A4F1D5D6D8FFDEDEDEFFC78D
        63FFE08B2AFFE48613FFE27C00FFE27C00FFE27C00FFE27C00FFE17B00FFE078
        00FFC45900FFC6742FFFD38D4CFFD97C15FFDB7100FFD26C00FFD06A00FFCD67
        00FFCB6500FFC66000FFB54F00FFB34D00FF6A2400AF0000000000000000FDD0
        A2FEF2BB84FFF1B16FFFF0AF6EFFEFAD6BFFEEAB68FFEDAA66FFECA863FFEBA6
        60FFEAA45EFFE9A35BFFE9A158FFE89F56FFE79D53FFE69C50FFE59A4EFFE498
        4BFFE39749FFE29546FFE29343FFE28F3AFFCEB9A3F4D3D4D6FFDFDFDFFFC274
        39FFED9F3CFFE78915FFE68000FFE68000FFE78100FFE68000FFE57F00FFED86
        00FFF58B00FFF38700FFF08200FFF08300FFF08700FFDD7700FFD36D00FFD06A
        00FFCE6800FFCA6400FFBB5500FFB54F00FF8C3200DD0000000000000000FDD0
        A4FEF2BC85FFF1B171FFF0AF6EFFEFAE6BFFEEAC69FFEDAA66FFECA863FFEBA6
        61FFEAA55EFFEAA35BFFE9A159FFE89F56FFE79E53FFE69C51FFE59A4EFFE499
        4CFFE39749FFE39547FFE29344FFE2903CFFD2B9A3F7D6D7D9FFDFDFDFFFC26E
        26FFF1A646FFEB901BFFEA8400FFEB8500FFEB8500FFEA8400FFEA8400FFF086
        00FFF8EBD6FFFEF7EBFFFFF8EAFFFFF9EFFFFFD694FFE57C00FFD67000FFD26C
        00FFCF6900FFCB6500FFC35D00FFB75100FF9D3B00F40000000000000000FDD1
        A7FEF3BC85FFF1B170FFF0B06FFFEFAE6CFFEEAC69FFEDAA67FFECA864FFECA7
        61FFEBA55FFFEAA35CFFE9A159FFE8A057FFE79E54FFE69C51FFE59B4FFFE599
        4CFFE4974AFFE39647FFE29445FFE1903BFFD3BCA4F7D7D9DBFFCACACAFFC16D
        24FFF3AA49FFEF9B29FFEE8700FFEF8900FFEF8900FFEF8900FFEE8800FFEA7D
        00FFF4F1EBFFFDFEFFFFFFFFFFFFFFFFFFFFFFDCA4FFE67C00FFD87200FFD46E
        00FFD16B00FFCD6700FFC76100FFBA5400FF9E3D00F20000000000000000FDD4
        AAFEF3BD88FFF1B272FFF0B06FFFEFAE6CFFEEAC6AFFEEAB67FFEDA965FFECA7
        62FFEBA55FFFEAA45DFFE9A25AFFE8A057FFE79E55FFE69D52FFE69B50FFE599
        4DFFE4984BFFE39648FFE29446FFE1913CFFD4BEA5F7D9DBDDFFD0D0D0FFC074
        37FFF5AC47FFF5A840FFF28B00FFF38D00FFF38D00FFF38D00FFF38D00FFE478
        00FFF2ECE7FFFAFAFBFFFFFFFFFFFFFFFFFFFFDBA2FFE87E00FFDA7400FFD670
        00FFD36D00FFD06A00FFCA6400FFBC5600FF9D4510EB0000000000000000FDD4
        AEFEF3BF8AFFF1B272FFF0B174FFEFB06FFFEFAD6AFFEEAB68FFEDA965FFECA8
        62FFEBA660FFEAA45DFFE9A25BFFE8A158FFE89F55FFE79D53FFE69C50FFE59A
        4EFFE4984BFFE39749FFE39547FFE3903EFFD1B8A3F1DADCDDFFE3E3E3FFCF98
        6DFFED9E38FFF8B658FFF69305FFF79100FFF79100FFF79100FFF69000FFDE72
        00FFEDE7E3FFF5F5F6FFFAFAFAFFFDFFFFFFFFDAA1FFE97F00FFDC7600FFD872
        00FFD46E00FFD06A00FFCC6600FFBD5700FF9A5730EB0000000000000000FDD5
        AFFEF4BF8BFFF4B169FFF29A2AFFF3A955FFF9BE83FFFBC287FFF8BC7FFFF5B7
        76FFF1B06EFFEEAB65FFEBA65EFFEAA259FFE89F56FFE79E54FFE69C50FFE599
        4EFFE4994BFFE49649FFE39648FFE2913FFFD3BBA2F4DCDEE0FFDFDFDFFFDDC7
        B1FFDA8220FFFCB855FFFCA931FFFB9400FFFD9700FFFC9600FFFB9500FFD96D
        00FFE7E1DEFFEDEEEFFFF3F3F3FFF4F7FBFFF7D29CFFE98000FFDD7700FFD973
        00FFD67000FFD36D00FFCC6600FFB65000FF9E7963F30000000000000000FDD8
        B2FEF4C08EFFF7B363FFBE5400FF973100FF9C3700FFA44107FFB05016FFB960
        25FFC57035FFCF7F44FFD88E53FFE0995EFFE7A267FFECAC70FFF2B577FFF5B7
        78FFF3B575FFF0AF6BFFE89F55FFE2923FFFD6BCA0F6DEE0E2FFCDCDCDFFD6D5
        D5FFC67828FFFDB74CFFFFBE5DFFFE9D09FFFE9800FFFF9900FFFF9900FFD266
        00FFDFDBD7FFE8E8E9FFEAEAEAFFECEFF2FFEEC897FFE67D00FFDE7800FFDA74
        00FFD77100FFD26C00FFCC6600FFB05413FF9D9896F50000000000000000FED9
        B6FFF5C18FFFF7B363FFCA6200FFA14008FFAB4E10FFAF5414FFB35B18FFB75F
        1AFFB9611AFFB96118FFB96015FFB85D11FFB7590CFFB35207FFB35003FFB652
        04FFBD5B0CFFC46617FFF9C28AFFE3913EFFD8BDA3F6E1E3E5FFB3B3B3FFB9B9
        B9FFCEAD90FFDF8522FFFFBF57FFFFBA52FFFF9900FFFF9900FFFF9B00FFCD61
        00FFD9D8D8FFE2E6E9FFE3E6E9FFE5EBF1FFE3C196FFE37A00FFDF7900FFDB75
        00FFD77100FFD56F00FFBF5900FFCB9E80FE818181F20000000000000000FEDA
        BAFFF5C28FFFF7B163FFDC7A07FFA23F08FFAE5213FFB25917FFB7621BFFBD68
        20FFC17025FFC67729FFC26E22FFBA6117FFC26E21FFC7782BFFC37227FFBD6B
        22FFB9631FFFB75300FFFAC48EFFE2913EFFD9BEA3F6E3E4E7FFD2D2D2FFC0C0
        C0FFCACACAFFCB874DFFF0A038FFFFBF57FFFFB950FFFF9B04FFFF9B00FFC259
        00FFB98467FFC4936FFFCB996FFFD2A173FFD69148FFE17900FFE17B00FFDD77
        00FFD87200FFCE6800FFBD723CFFDBDADAFD6D6D6DF00000000000000000FEDD
        BDFFF5C390FFF6B062FFF0AA43FFA03B00FFAF5514FFB45E19FFBA651EFFBF6E
        24FFC77729FFB96016FFB65D1BFFD3934EFFBD661EFFC26C1CFFC7792BFFC16F
        24FFBC671FFFC05F0FFFFFCE9EFFE49140FFDBC2A7F6E2E4E6FFE3E3E3FFE3E3
        E3FFE4E4E4FFEAE7E4FFCC823DFFF0A138FFFFBD55FFFFBD58FFFFA824FFF28C
        00FFE87F00FFE77E00FFE67D00FFE57C00FFE27B00FFE27C00FFE07A00FFDE78
        00FFD26C00FFBA6428FFE4DCD7FFD0D0D0FC6C6C6CF00000000000000000FEDE
        BFFFF6C391FFF5AF62FFF6BA58FFB46C44FFAF510FFFB7601CFFBC6820FFC372
        26FFC37022FFB65C1BFFF1D091FFF0D091FFEBC788FFBA5806FFCA7F2FFFC474
        28FFBD671CFFCD8341FFFFCE9EFFE39240FFDEC2A5F8E4E6E8FFE3E3E3FFE3E3
        E3FFE2E2E2FFE3E3E3FFEAE6E4FFD8945AFFE18824FFFFBA4CFFFFBD52FFFEB8
        4EFFFAA62CFFF69915FFF18E07FFED8700FFE98302FFE68307FFE3820DFFCC66
        03FFBD7139FFDAD2CCFFDEDEDEFFD0D0D0FC6D6D6DF00000000000000000FDE0
        C2FFF6C492FFF5B061FFF4B451FFD7D0CEFFA94100FFB7611BFFBD6B22FFC576
        28FFB04F06FFE2B477FFF2D89FFFF1D9A3FFF3DDA7FFBE5A05FFCD8230FFC577
        29FFBE5E08FFE0BA99FFFCCA98FFE59341FFE0C2A5F9E5E7E9FFE7E7E7FFE4E4
        E4FFE6E6E6FFE8E8E8FFE6E6E6FFEDEDEDFFE7C4A7FFCB7F2EFFDF8823FFF3A4
        3AFFFBB245FFF9AE44FFF6A941FFF2A43AFFE6912BFFD17414FFC46A1EFFDCB1
        92FFE2E2E2FFDFDFDFFFDCDCDCFFD2D2D2FC6E6E6EF00000000000000000FDE1
        C5FFF6C593FFF5B062FFF4B34EFFE0E8F1FFCCA68EFFAE4600FFC0691EFFCB71
        20FFC15C11FFFDE1A5FFFDE4ADFFFEEBB7FFFCEBB9FFC25F05FFCC8131FFC571
        21FFC76A16FFF2F8FEFFFDCB99FFE49443FFE1C4A8F9E9EBEDFFC8C8C8FFAFAF
        AFFFB1B1B1FFB4B4B4FFBABABAFFC1C1C1FFCCCCCCFFDDDCDCFFDDC5B0FFD39C
        70FFCA8140FFCA792FFFC9772DFFC67B3BFFCE9467FFDDC0AAFFE7E6E5FFE3E3
        E3FFDEDEDEFFDEDEDEFFDEDEDEFFD2D2D2FC707070F00000000000000000FDE2
        C8FFF6C696FFF7B263FFF2B14EFFDEE1E8FFE5E8EAFFDDB89FFF85552AFF4C65
        64FF27687FFF7CB1D4FF6AB0ECFF78B8EDFFBACDCCFFCA5E00FFC66D13FFC665
        0DFFEADED5FFEEF3F9FFFCCA98FFE49544FFE2C5A9F9EBEDF0FFC5C5C5FFAEAE
        AEFFAEAEAEFFADADADFFABABABFFAAAAAAFFA9A9A9FFA8A8A8FFADADADFFE7E7
        E7FFECECECFFEBEBEBFFEBEBEBFFEAEAEAFFE8E8E8FFE4E4E4FFE1E1E1FFE0E0
        E0FFE0E0E0FFDFDFDFFFE0E0E0FFD5D5D5FC6F6F6FF00000000000000000FDE4
        CAFFF7C89AFFF6B164FFF2B14CFFDFE1E8FFD2D8DCFF207AA7FF006AA3FF006A
        9FFF1D83CCFF50ACFFFF5AB1FFFF55B0FFFF3DA5FFFF5385BAFFDCA676FFEDEE
        EEFFEDEFF0FFEDEEF2FFFCCA98FFE59646FFE4C7ABF9EAECEEFFEBEBEBFFE1E1
        E1FFD8D8D8FFD1D1D1FFC9C9C9FFC0C0C0FFB7B7B7FFAFAFAFFFB5B5B5FFE7E7
        E7FFE5E5E5FFE5E5E5FFE4E4E4FFE4E4E4FFE3E3E3FFE3E3E3FFE2E2E2FFE2E2
        E2FFE1E1E1FFE1E1E1FFE1E1E1FFD5D5D5FC6F6F6FF00000000000000000FDE5
        CDFFF7C99AFFF6B265FFF4B24DFFDEE1E7FF116F9FFF006EA3FF0076A9FF036A
        9DFF61B6FFFF67B9FFFF6BBCFFFF6DBDFFFF6DBEFFFF4FAEFFFF7ABAFDFFF2EE
        EBFFEAEAEAFFEBEEF1FFFCCC9BFFE79748FFE5C8A7FBECEEF0FFEDEDEDFFEDED
        EDFFEEEEEEFFECECECFFEDEDEDFFECECECFFECECECFFEBEBEBFFEBEBEBFFE8E8
        E8FFE7E7E7FFE6E6E6FFE6E6E6FFE5E5E5FFE5E5E5FFE4E4E4FFE4E4E4FFE3E3
        E3FFE3E3E3FFE3E3E3FFE3E3E3FFD8D8D8FC707070F00000000000000000FDE6
        CFFFF7CA9DFFF8B466FFF7B24DFF7EACC8FF006A9FFF0076A9FF0076A9FF197A
        B4FF6BBDFFFF6FBEFFFF76C4FFFF79C4FFFF79C3FFFF75C2FFFF359CFFFFE2E5
        E9FFEBEAE9FFE9EBEEFFFDCD9EFFE6984BFFE6C9A9FAEFF1F4FFE6E6E6FFD6D6
        D6FFDCDCDCFFE1E1E1FFE7E7E7FFEDEDEDFFEEEEEEFFEDEDEDFFECECECFFECEC
        ECFFEAEAEAFFEAEAEAFFE8E8E8FFE8E8E8FFE6E6E6FFE6E6E6FFE5E5E5FFE5E5
        E5FFE5E5E5FFE4E4E4FFE5E5E5FFD8D8D8FC727272F00000000000000000FDE8
        D2FFF8CB9DFFF8B367FFFBB54EFF3E89B3FF0073A7FF0078ABFF0078ABFF2281
        BBFF73C1FFFF7AC5FFFF83CBFFFF87CCFFFF85CCFFFF7DC8FFFF4FABFFFFACCD
        EFFFEBE9E7FFE6E9ECFFFBCC9DFFE8994CFFE9CAACFAF3F6F8FFB8B8B8FFAFAF
        AFFFAFAFAFFFAFAFAFFFADADADFFADADADFFB1B1B1FFB7B7B7FFBDBDBDFFC4C4
        C4FFC9C9C9FFD0D0D0FFD7D7D7FFDCDCDCFFE3E3E3FFE9E9E9FFE8E8E8FFE7E7
        E7FFE6E6E6FFE6E6E6FFE6E6E6FFDADADAFC707070F10000000000000000FDE9
        D5FFF8CC9FFFF7B469FFFBB34FFF3384B0FF0075A9FF007CAFFF007EB1FF1174
        AAFF79C6FFFF80C9FFFF8BD0FFFF94D5FFFF8FD2FFFF84CBFFFF5FB5FFFF8CBE
        F1FFEBE7E5FFE4E7E9FFFBCD9FFFE79C4EFFEACCACFAF3F5F8FFD2D2D2FFB7B7
        B7FFB1B1B1FFAEAEAEFFADADADFFACACACFFABABABFFACACACFFABABABFFAAAA
        AAFFAAAAAAFFA9A9A9FFA9A9A9FFA7A7A7FFA7A7A7FFB1B1B1FFEBEBEBFFE8E8
        E8FFE8E8E8FFE7E7E7FFE8E8E8FFE1E1E1FD777777F80000000000000000FDEA
        D9FFF8CDA0FFF9B66AFFF9B34CFF5595BAFF0073A8FF007DB0FF0081B4FF006A
        9DFF67B6F3FF83CBFFFF8ED2FFFF99D8FFFF92D4FFFF85CDFFFF62B8FFFF87BC
        F1FFE9E5E3FFE4E6E9FFFCCFA3FFE89D50FFEACDADFBF4F6F8FFF5F5F5FFF5F5
        F5FFF5F5F5FFF0F0F0FFE9E9E9FFE1E1E1FFD9D9D9FFD1D1D1FFCACACAFFC2C2
        C2FFBABABAFFB3B3B3FFABABABFFA6A6A6FFA5A5A5FFABABABFFECECECFFEAEA
        EAFFEAEAEAFFE9E9E9FFEAEAEAFFE3E3E3FD767676F80000000000000000FFEC
        DAFFF9CDA2FFF9B56BFFF3B04CFFA0BDD0FF006AA0FF0080B3FF0084B7FF0082
        B5FF0B6FA2FF60AFE6FF7FC6F8FF7FC5F3FF7DC4F6FF81C8FEFF5AB3FFFF9BC4
        EEFFE7E4E2FFE1E3E6FFFACEA2FFE99E52FFEECCADFDF5F8FAFFF4F4F4FFF4F4
        F4FFF3F3F3FFF4F4F4FFF3F3F3FFF3F3F3FFF2F2F2FFF3F3F3FFF2F2F2FFF3F3
        F3FFF2F2F2FFF3F3F3FFF2F2F2FFEEEEEEFFE5E5E5FFEBEBEBFFEDEDEDFFECEC
        ECFFEBEBEBFFEBEBEBFFEBEBEBFFE4E4E4FD777777F80000000000000000FDED
        DDFFF9CFA4FFFAB66CFFF0AE49FFDEDDE1FF1F77A2FF007DB1FF0085B8FF0089
        BCFF0088BBFF0075A7FF006FA2FF0071A4FF0070A3FF00699DFF0D76BBFFCBD6
        E3FFE3E1E0FFDFE1E4FFFBCEA2FFEBA155FFEFCEAFFDF8FAFDFFF7F7F7FFF7F7
        F7FFF6F6F6FFF6F6F6FFF4F4F4FFF4F4F4FFF3F3F3FFF3F3F3FFF2F2F2FFF2F2
        F2FFF1F1F1FFF1F1F1FFF0F0F0FFF1F1F1FFF0F0F0FFF0F0F0FFEEEEEEFFEEEE
        EEFFEDEDEDFFEDEDEDFFEDEDEDFFE6E6E6FD777777F80000000000000000FDEE
        E1FFFAD0A5FFFAB86DFFEEAE4AFFD6D8DFFFB8C6CCFF00689BFF0084B8FF008B
        BEFF008FC2FF0094C7FF0097CAFF0099CCFF0097CAFF0096C9FF0074A9FF94B6
        C6FFE2E0DFFFDDDFE2FFF9D0A6FFEAA158FFF0D1B1FDF9FBFEFFF8F8F8FFEAEA
        EAFFECECECFFF0F0F0FFF5F5F5FFF7F7F7FFFAFAFAFFF8F8F8FFF8F8F8FFF6F6
        F6FFF6F6F6FFF5F5F5FFF5F5F5FFF3F3F3FFF3F3F3FFF2F2F2FFF2F2F2FFF0F0
        F0FFF0F0F0FFEEEEEEFFEFEFEFFFE7E7E7FD787878F80000000000000000FDF0
        E4FFFAD1AAFFFAB96FFFEEAD48FFD1D4DBFFDBD7D6FFA4BBC6FF006A9CFF0080
        B4FF0090C3FF0094C7FF0098CBFF009BCEFF0099CCFF0088BCFF036B9DFFD5D8
        D9FFDEDDDDFFDBDDE0FFFAD0A6FFEBA45AFFF1D2B2FDFDFFFFFFCCCCCCFFADAD
        ADFFAFAFAFFFAEAEAEFFAEAEAEFFAFAFAFFFB1B1B1FFB5B5B5FFBABABAFFBEBE
        BEFFC3C3C3FFC8C8C8FFCCCCCCFFD0D0D0FFD6D6D6FFDADADAFFDFDFDFFFE4E4
        E4FFE8E8E8FFEDEDEDFFF2F2F2FFEAEAEAFD787878F80000000000000000FDF1
        E5FFFBD3ADFFFBBA70FFEDAC48FFD0D3DAFFD3D2D2FFD9D6D5FFC9CED0FF488A
        ACFF00699EFF0072A8FF0076ACFF0073A9FF006A9EFF4287ABFFCAD2D4FFDEDC
        DBFFDCDCDCFFDADCDFFFF8CFA5FFECA55CFFF3D2B2FEFEFFFFFFDCDCDCFFAFAF
        AFFFAEAEAEFFADADADFFAEAEAEFFADADADFFADADADFFACACACFFADADADFFABAB
        ABFFAAAAAAFFABABABFFA9A9A9FFA9A9A9FFAAAAAAFFA8A8A8FFA9A9A9FFA7A7
        A7FFA7A7A7FFA5A5A5FFB1B1B1FFEBEBEBFD797979F80000000000000000FDF3
        E8FFFBD4AEFFFBBB71FFECAD4BFFCDD4E1FFD0D3DAFFD1D5DBFFD4D6DCFFDCDB
        DFFFD7D9DEFFB6C7D3FFADC1D0FFB8C9D5FFDCDCE0FFE1DFE2FFDADBE0FFD9DB
        E0FFD8DBDFFFD7DBE2FFF7CDA1FFEEA761FFF5D3B1FFFFFFFFFFFFFFFFFFFFFF
        FFFFFAFAFAFFF4F4F4FFEFEFEFFFE9E9E9FFE4E4E4FFDDDDDDFFD8D8D8FFD1D1
        D1FFCCCCCCFFC6C6C6FFBFBFBFFFBABABAFFB4B4B4FFAFAFAFFFAAAAAAFFA6A6
        A6FFA7A7A7FFA5A5A5FFA9A9A9FFECECECFD797979F80000000000000000FDF4
        ECFFFBD5B0FFFABC76FFFA9C0AFFEEAB46FFEDAC45FFEDAD49FFECB051FFEDAF
        50FFEDB35BFFEEB45CFFEDB662FFEDB767FFEBB869FFE9BA70FFEABA71FFE8BC
        78FFE8BD78FFE6C188FFFABC6AFFEDA965FFF5D5B5FEFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFEFFFFFFFFFFFFFFFEFFFFFFFEFFFFFFFDFFFFFFFEFFFFFFFDFE
        FFFFFDFFFFFFFCFEFFFFFDFFFFFFFCFDFEFFFCFEFEFFFCFDFDFFFAFAFBFFF6F7
        F7FFF2F2F2FFEBEBEBFFF2F2F2FFF0F0F0FD787878F80000000000000000FDF5
        EEFFFCD6B0FFFAC38DFFFABE7CFFFBBA73FFFAB96FFFFAB76CFFF9B565FFF9B4
        65FFF9B25CFFF8B15CFFF8AE57FFF8AE52FFF7AB50FFF7AA4BFFF7A949FFF7A7
        42FFF8A741FFF6A53FFFF6AC56FFEFAD6AFFEFB276FEF1BB86FFF2BE8AFFF2C1
        91FFF3C596FFF3C89DFFF4CCA4FFF4D0ABFFF4D3B1FFF6D7BAFFF5DCC0FFF5DF
        C7FFF7E2CEFFF7E6D7FFF7EBDDFFF8EEE6FFF8F3ECFFF8F6F4FFF9FAFAFFFAFC
        FFFFF8FEFFFFF9FCFFFFF8FDFFFFF0F5F9FD777C7FF80000000000000000FDF7
        F1FFFCD8B3FFFBC58CFFFAC38DFFF9C38BFFF9C08AFFF8C088FFF7BF86FFF7BD
        84FFF6BC82FFF5BB80FFF5B87EFFF4B87CFFF3B77AFFF3B67AFFF2B578FFF1B3
        76FFF1B274FFF0B073FFF0AF6FFFEFAE6CFFEFAD6BFFEEAC68FFEEAA66FFEDA9
        63FFEDA862FFECA761FFECA55EFFEBA55DFFEBA45BFFEBA259FFEAA158FFEAA1
        56FFEAA055FFE9A053FFE99E52FFE99E52FFE99C50FFE99C50FFE99C4FFFE99D
        51FFE9A157FFEAA45EFFEAA865FFE6A25FFEAD6B27F80000000000000000E1DD
        D7E4FBE6D2FEFBC48BFFFBC48EFFFAC38CFFF9C28AFFF9C088FFF8BF86FFF7BE
        84FFF7BC82FFF6BB80FFF5BA7EFFF5B97CFFF4B77AFFF3B679FFF3B577FFF2B4
        75FFF2B373FFF1B272FFF0B170FFF0AF6EFFEFAE6DFFEFAD6BFFEEAC6AFFEEAC
        69FFEEAB67FFEDAA66FFEDA965FFECA863FFECA762FFECA761FFEBA660FFEBA5
        5FFFEBA55FFFEAA45EFFEAA45DFFEAA35CFFEAA35CFFE9A35BFFE9A25BFFE9A2
        5AFFE9A25AFFE9A25AFFE9A25AFFE6994BFFC07122F600000000000000006967
        646BFDF8F5FEFAD6B2FEFBC38CFFFAC48EFFFAC38CFFF9C18AFFF8C088FFF8BF
        86FFF7BE84FFF7BC82FFF6BB80FFF5BA7EFFF5B97CFFF4B77AFFF3B679FFF3B5
        77FFF2B475FFF2B374FFF1B272FFF1B171FFF0B06FFFF0AF6EFFEFAE6CFFEFAD
        6BFFEEAC69FFEEAB68FFEDAB67FFEDAA66FFEDA965FFECA864FFECA863FFECA7
        62FFEBA761FFEBA660FFEBA660FFEBA55FFFEBA55EFFEAA55EFFEAA45DFFEAA4
        5DFFEAA45DFFEAA45DFFEBA55EFFE69A4DFFA66B30D300000000000000000101
        01019B97949DFCF7F4FDF4DBC3F8F6C795FBF9C289FEFAC186FFF9BF84FFF8BE
        82FFF8BD82FFF7BD80FFF7BA7EFFF6B97CFFF5B87AFFF5B77AFFF4B678FFF4B6
        76FFF3B375FFF2B373FFF2B271FFF1B171FFF1B06EFFF0AF6DFFF0AF6DFFF0AE
        6BFFEFAD6AFFEFAC69FFEEAB67FFEEAA66FFEEAB65FFEDA964FFEDA863FFEDA9
        62FFECA763FFECA862FFECA761FFECA761FFEBA760FFEBA65FFFEBA65FFFEBA6
        60FFEBA65EFFEBA55FFFEAA65FFFE79B4EFE583F277300000000000000000000
        0000020202026B6B6A6BE7E4DFE7F8F2EEF9EAE3DBEDEEE4D8F1F5E8DBF8F5E6
        D6F7F3E1CFF7F4E3CEF8F1DAC5F4EED8C2F3EFD7BEF4ECD4B9F1F1D4B8F5EED2
        B2F4F3D1B2F8F0CEADF7F0CDAAF8F0CCA8F8ECC8A0F5F1C9A2F8EFC79FF8F1C8
        9DFBF2C89BFCF2C496FCF2C496FDF0C08EFCF0BF8CFCEFBE8AFCEFBD88FCF0BC
        87FDF0BA85FEF0B881FEEFB77FFEEFB57BFEEEB47AFEEDB276FEEDB177FEECB0
        75FEECAF72FEEEAE6FFEF0AD6CFE997145B00303030600000000000000000000
        00000000000000000000060606072F2E2D314947454B514E4C5355514E565753
        50595E5955605E595460655F5A66665F59686A635D6C7068607270675F727A70
        687C7A70677D7F746A8283776C8685776C878C7E708E8C7C6F8E938374959684
        73989986749BA08D79A3A08B77A3A8927DABA9917BACAD937DB0B3987DB4B296
        7DB5BB9E80BEBB9D7EBEC3A282C5C7A483C9C9A681CBD0AA85D3D0AA82D3D6AE
        85D9D5AA81D9A98560B34E3E2B5A020101030000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      LargeImageIndex = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aLVK
      Category = 0
      LargeImageIndex = 3
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aKontakt
      Category = 0
      LargeImageIndex = 32
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aDosie
      Category = 0
      LargeImageIndex = 8
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aObrazovanieObuka: TAction
      Caption = #1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '#1080' '#1086#1073#1091#1082#1072
      OnExecute = aObrazovanieObukaExecute
    end
    object aLVK: TAction
      Caption = #1051#1080#1095#1085#1080' '#1074#1077#1096#1090#1080#1085#1080' '#1080' '#1082#1074#1072#1083#1080#1092#1080#1082#1072#1094#1080#1080
      OnExecute = aLVKExecute
    end
    object aRabotnoIskustvo: TAction
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086
      OnExecute = aRabotnoIskustvoExecute
    end
    object aKontakt: TAction
      Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1080' '#1082#1086#1085#1090#1072#1082#1090
      OnExecute = aKontaktExecute
    end
    object aDosieDizajn: TAction
      Caption = 'aDosieDizajn'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDosieDizajnExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 360
    Top = 344
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40288.570566307870000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 504
    Top = 168
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 440
    Top = 208
  end
  object ActionList2: TActionList
    Images = dm.cxLargeImages
    Left = 456
    Top = 336
    object aDosie: TAction
      Caption = #1044#1086#1089#1080#1077
      ImageIndex = 134
      ShortCut = 121
      OnExecute = aDosieExecute
    end
  end
  object tblRMRE: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    mr.naziv ReNaziv'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'left  join hr_rm_oglas hro on hro.id_rm_re=hrr.id'
      
        'where(  coalesce(hs.do_datum, '#39'11.11.1111'#39') like :datum and hs.i' +
        'd_re_firma like :firma and coalesce(hro.id_oglas, 0) like :oglas' +
        'ID'
      '     ) and (     HRR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select distinct mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    mr.naziv ReNaziv'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'left  join hr_rm_oglas hro on hro.id_rm_re=hrr.id'
      
        'where coalesce(hs.do_datum, '#39'11.11.1111'#39') like :datum and hs.id_' +
        're_firma like :firma and coalesce(hro.id_oglas, 0) like :oglasID'
      'order by mr.naziv,hrm.naziv ')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 187
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRMRERENAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 779
    Top = 179
  end
  object dsOglas: TDataSource
    DataSet = tblOglas
    Left = 299
    Top = 71
  end
  object tblOglas: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    og.id,'
      '    og.broj,'
      '    og.datum_objaven,'
      '    og.datum_istekuva,'
      '    og.objaven,'
      '    og.prijavuvanje_do,'
      '    og.opis,'
      '    og.status,'
      '    case og.status when '#39'1'#39' then '#39#1040#1082#1090#1080#1074#1077#1085#39
      '                   else '#39#1053#1077#1072#1082#1090#1080#1074#1077#1085#39
      '    end "statusNaziv",'
      '    og.promenet_od,'
      '    og.ts_ins,'
      '    og.ts_upd,'
      '    og.usr_ins,'
      '    og.usr_upd,'
      
        '    og.broj || '#39'/'#39' || extractyear(og.datum_objaven) as BrojGodin' +
        'a,'
      '    extractyear(og.datum_objaven) as Godina,'
      '    og.id_re_firma'
      'from hr_evidencija_oglas og'
      'where(  id like '#39'%'#39' and og.status like :statusID'
      'and og.id_re_firma = :firma'
      '     ) and (     OG.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    og.id,'
      '    og.broj,'
      '    og.datum_objaven,'
      '    og.datum_istekuva,'
      '    og.objaven,'
      '    og.prijavuvanje_do,'
      '    og.opis,'
      '    og.status,'
      '    case og.status when '#39'1'#39' then '#39#1040#1082#1090#1080#1074#1077#1085#39
      '                   else '#39#1053#1077#1072#1082#1090#1080#1074#1077#1085#39
      '    end "statusNaziv",'
      '    og.promenet_od,'
      '    og.ts_ins,'
      '    og.ts_upd,'
      '    og.usr_ins,'
      '    og.usr_upd,'
      
        '    og.broj || '#39'/'#39' || extractyear(og.datum_objaven) as BrojGodin' +
        'a,'
      '    extractyear(og.datum_objaven) as Godina,'
      '    og.id_re_firma'
      'from hr_evidencija_oglas og'
      'where id like '#39'%'#39' and og.status like :statusID'
      '-- and og.id_re_firma = :firma'
      'order by og.broj desc')
    AutoUpdateOptions.UpdateTableName = 'HR_EVIDENCIJA_OGLAS'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_HR_EVIDENCIJA_OGLAS_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 232
    Top = 55
    oRefreshDeletedRecord = True
    object tblOglasID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOglasDATUM_OBJAVEN: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1073#1112#1072#1074#1072
      FieldName = 'DATUM_OBJAVEN'
    end
    object tblOglasDATUM_ISTEKUVA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1090#1077#1082#1091#1074#1072#1114#1077
      FieldName = 'DATUM_ISTEKUVA'
    end
    object tblOglasOBJAVEN: TFIBStringField
      DisplayLabel = #1054#1073#1112#1072#1074#1077#1085' '#1074#1086'...'
      FieldName = 'OBJAVEN'
      Size = 100
      EmptyStrToNull = True
    end
    object tblOglasPRIJAVUVANJE_DO: TFIBDateField
      DisplayLabel = #1055#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077' '#1076#1086
      FieldName = 'PRIJAVUVANJE_DO'
    end
    object tblOglasOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      EmptyStrToNull = True
    end
    object tblOglasSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblOglasPROMENET_OD: TFIBStringField
      DisplayLabel = #1055#1088#1086#1084#1077#1085#1077#1090' '#1086#1076
      FieldName = 'PROMENET_OD'
      Size = 200
      EmptyStrToNull = True
    end
    object tblOglasTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblOglasTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblOglasUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblOglasUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblOglasGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblOglasstatusNaziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'statusNaziv'
      Size = 9
      EmptyStrToNull = True
    end
    object tblOglasID_RE_FIRMA: TFIBIntegerField
      FieldName = 'ID_RE_FIRMA'
    end
    object tblOglasBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblOglasBROJGODINA: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112'/'#1043#1086#1076#1080#1085#1072
      FieldName = 'BROJGODINA'
      Size = 23
      EmptyStrToNull = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
