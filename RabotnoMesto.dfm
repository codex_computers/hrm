inherited frmRabotnoMesto: TfrmRabotnoMesto
  Width = 1228
  Height = 784
  ParentCustomHint = False
  AutoScroll = True
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
  WindowState = wsMaximized
  ExplicitWidth = 1228
  ExplicitHeight = 784
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1212
    Height = 82
    ExplicitWidth = 1212
    ExplicitHeight = 82
    inherited cxGrid1: TcxGrid
      Width = 1208
      Height = 78
      PopupMenu = PopupMenu1
      ExplicitWidth = 1208
      ExplicitHeight = 78
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsRabotnoMesto
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Editing = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 43
        end
        object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 209
        end
        object cxGrid1DBTableView1ZVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'ZVANJE'
          Options.Editing = False
          Width = 177
        end
        object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
          Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090'('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'GRUPA'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_GRUPA: TcxGridDBColumn
          Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
          DataBinding.FieldName = 'GRUPAOZNAKA'
          Options.Editing = False
          Width = 110
        end
        object cxGrid1DBTableView1KATEGORIJA: TcxGridDBColumn
          DataBinding.FieldName = 'KATEGORIJA'
          Visible = False
          Width = 113
        end
        object cxGrid1DBTableView1KATEGORIJARM_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'KATEGORIJARM_NAZIV'
          Width = 233
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          Caption = #1056#1072#1073#1086#1090#1085#1080' '#1079#1072#1076#1072#1095#1080
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Width = 170
        end
        object cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn
          Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072'('#1064#1080#1092#1088#1072')'
          DataBinding.FieldName = 'OBRAZOVANIE'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_OBRAZOVANIE: TcxGridDBColumn
          Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
          DataBinding.FieldName = 'NAZIV_OBRAZOVANIE'
          Options.Editing = False
          Width = 171
        end
        object cxGrid1DBTableView1ID_SZ_OBRAZOVANIE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_SZ_OBRAZOVANIE'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1OBRAZOVANIE_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OBRAZOVANIE_OPIS'
          Options.Editing = False
          Width = 186
        end
        object cxGrid1DBTableView1OPIS_SZ_OBRAZOVANIE: TcxGridDBColumn
          Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'OPIS_SZ_OBRAZOVANIE'
          Options.Editing = False
          Width = 145
        end
        object cxGrid1DBTableView1STEPEN_SLOZENOST: TcxGridDBColumn
          Caption = #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
          DataBinding.FieldName = 'STEPEN_SLOZENOST'
          Width = 137
        end
        object cxGrid1DBTableView1GRUPA_PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA_PLATA'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_GRUPA_PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_GRUPA_PLATA'
          Visible = False
          Options.Editing = False
          Width = 64
        end
        object cxGrid1DBTableView1RABOTNO_ISKUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_ISKUSTVO'
          Options.Editing = False
          Width = 154
        end
        object cxGrid1DBTableView1USLOVI: TcxGridDBColumn
          Caption = #1055#1086#1089#1077#1073#1085#1080' '#1091#1089#1083#1086#1074#1080
          DataBinding.FieldName = 'USLOVI'
          Options.Editing = False
          Width = 135
        end
        object cxGrid1DBTableView1LOKACIJA_RM: TcxGridDBColumn
          DataBinding.FieldName = 'LOKACIJA_RM'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1ID_TIP_RABOTNO_VREME: TcxGridDBColumn
          DataBinding.FieldName = 'ID_TIP_RABOTNO_VREME'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1RABOTNO_VREME: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_VREME'
          Options.Editing = False
        end
        object cxGrid1DBTableView1TIP_RABOTNO_VREME: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_RABOTNO_VREME'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1ODGOVARA_PRED: TcxGridDBColumn
          Caption = #1054#1076#1075#1086#1074#1072#1088#1072' '#1087#1088#1077#1076
          DataBinding.FieldName = 'ODGOVARA_PRED'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Width = 116
        end
        object cxGrid1DBTableView1AKTIVNO: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1085#1086' '
          DataBinding.FieldName = 'AKTIVNO'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Options.Editing = False
          Width = 79
        end
        object cxGrid1DBTableView1OPIS_AKTIVNO: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'OPIS_AKTIVNO'
          Visible = False
          Options.Editing = False
          Width = 128
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'VID_VRABOTUVANJE'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_VID_VRABOTUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VID_VRABOTUVANJE'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_GRUPA1: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_GRUPA'
          Visible = False
        end
        object cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'DEN_USLOVI_RABOTA'
          Visible = False
        end
        object cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn
          DataBinding.FieldName = 'DENOVI_ODMOR'
          Visible = False
        end
        object cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn
          DataBinding.FieldName = 'RAKOVODENJE'
          Visible = False
        end
        object cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA'
          Visible = False
        end
        object cxGrid1DBTableView1SZO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'SZO_NAZIV'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 216
    Width = 1212
    Height = 506
    ParentShowHint = False
    ExplicitTop = 216
    ExplicitWidth = 1212
    ExplicitHeight = 506
    inherited Label1: TLabel
      Left = 610
      Top = 338
      Visible = False
      ExplicitLeft = 610
      ExplicitTop = 338
    end
    object Label10: TLabel [1]
      Left = 40
      Top = 368
      Width = 107
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1089#1077#1073#1085#1080' '#1091#1089#1083#1086#1074#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 39
      Top = 467
      Width = 107
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1076#1075#1086#1074#1072#1088#1072' '#1087#1088#1077#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [3]
      Left = 29
      Top = 13
      Width = 104
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label12: TLabel [4]
      Left = 40
      Top = 440
      Width = 107
      Height = 13
      Hint = #1063#1072#1089#1086#1074#1080' '#1085#1077#1076#1077#1083#1085#1086' ('#1084#1072#1082#1089'.40)'
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel [5]
      Left = 40
      Top = 341
      Width = 107
      Height = 13
      Hint = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086' '#1074#1086' '#1089#1090#1088#1091#1082#1072#1090#1072
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073'. '#1080#1089#1082#1091#1089#1090#1074#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel [6]
      Left = 344
      Top = 441
      Width = 140
      Height = 13
      Hint = #1063#1072#1089#1086#1074#1080' '#1085#1077#1076#1077#1083#1085#1086' ('#1084#1072#1082#1089'.40)'
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1058#1080#1087' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel [7]
      Left = 13
      Top = 310
      Width = 133
      Height = 25
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel [8]
      Left = 512
      Top = 315
      Width = 106
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1055#1083#1072#1090#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel [9]
      Left = 40
      Top = 413
      Width = 107
      Height = 13
      Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1088#1072#1073#1086#1090#1072#1090#1072'. '
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1051#1086#1082#1072#1094#1080#1112#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 2
      Top = 38
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      TabOrder = 2
      Visible = False
      ExplicitLeft = 2
      ExplicitTop = 38
      ExplicitWidth = 39
      Width = 39
    end
    inherited OtkaziButton: TcxButton
      Left = 1100
      Top = 451
      TabOrder = 16
      ExplicitLeft = 1100
      ExplicitTop = 451
    end
    inherited ZapisiButton: TcxButton
      Left = 1019
      Top = 452
      TabOrder = 15
      ExplicitLeft = 1019
      ExplicitTop = 452
    end
    object cxGroupBox1: TcxGroupBox
      Left = 26
      Top = 50
      Caption = ' '#1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080'  '
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 1
      DesignSize = (
        985
        143)
      Height = 143
      Width = 985
      object Label3: TLabel
        Left = 55
        Top = 22
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1074#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 530
        Top = 3
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object Label6: TLabel
        Left = 474
        Top = 16
        Width = 105
        Height = 27
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label9: TLabel
        Left = 3
        Top = 81
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1080' '#1079#1072#1076#1072#1095#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 526
        Top = 69
        Width = 296
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1088#1080#1084#1072#1114#1072' '#1079#1072' '#1088#1072#1082#1086#1074#1086#1076#1085#1072' '#1092#1091#1085#1082#1094#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Label18: TLabel
        Left = 716
        Top = 138
        Width = 106
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object Label19: TLabel
        Left = 560
        Top = 115
        Width = 262
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054' '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Label20: TLabel
        Left = 520
        Top = 92
        Width = 302
        Height = 16
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1088#1080#1084#1072#1114#1072' '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Label22: TLabel
        Left = 943
        Top = 92
        Width = 15
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '%'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object Label23: TLabel
        Left = -2
        Top = 48
        Width = 105
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object txtZvanje: TcxDBTextEdit
        Left = 111
        Top = 19
        Hint = #1047#1074#1072#1114#1077' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ZVANJE'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 366
      end
      object txtVidVrabotuvanje: TcxDBTextEdit
        Left = 625
        Top = 19
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1042#1080#1076' '#1085#1072' '#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
        BeepOnEnter = False
        DataBinding.DataField = 'VID_VRABOTUVANJE'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 6
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 60
      end
      object cbVidVrabotuvanje: TcxDBLookupComboBox
        Left = 686
        Top = 19
        Hint = #1054#1087#1080#1089' '#1085#1072' '#1042#1080#1076' '#1085#1072' '#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
        DataBinding.DataField = 'VID_VRABOTUVANJE'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'ID'
          end
          item
            Width = 800
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsVidVrabotuvanje
        TabOrder = 7
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 299
      end
      object txtGrupa: TcxDBTextEdit
        Left = 585
        Top = 19
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
        BeepOnEnter = False
        DataBinding.DataField = 'GRUPA'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 60
      end
      object cbGrupa: TcxDBLookupComboBox
        Left = 647
        Top = 19
        Hint = #1054#1087#1080#1089' '#1085#1072' '#1043#1088#1091#1087#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'GRUPA'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'ID'
          end
          item
            Width = 250
            FieldName = 'OZNAKA'
          end
          item
            Width = 800
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListSource = dmSis.dsGrupaRM
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 306
      end
      object txtOpis: TcxDBMemo
        Left = 111
        Top = 73
        Hint = 
          #1054#1087#1080#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086#1090#1086' '#1052#1077#1089#1090#1086' '#1080' '#1088#1072#1073#1086#1090#1085#1080' '#1079#1072#1076#1072#1095#1080'.  '#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072#1087#1072#1090#1080', '#1076#1072' ' +
          #1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090'!'
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 5
        OnDblClick = aUrediTextOpisExecute
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 58
        Width = 842
      end
      object Rakovodenje: TcxDBTextEdit
        Left = 828
        Top = 66
        Hint = 
          #1055#1088#1086#1094#1077#1085#1090' '#1085#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1080#1086#1090' '#1073#1086#1076' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1088#1072#1082#1086#1074#1086#1076#1085 +
          #1072#1090#1072' '#1092#1091#1085#1082#1094#1080#1112#1072
        BeepOnEnter = False
        DataBinding.DataField = 'RAKOVODENJE'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 8
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 109
      end
      object DenoviGO: TcxDBTextEdit
        Left = 828
        Top = 135
        Hint = 
          #1057#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080#1090#1077' '#1079#1072' '#1088#1072#1073#1086#1090#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086', '#1082#1086#1083#1082#1091' '#1076#1077#1085#1086#1074#1080' '#1089#1083#1077#1076#1091 +
          #1074#1072#1072#1090' '#1086#1076#1084#1086#1088
        BeepOnEnter = False
        DataBinding.DataField = 'DENOVI_ODMOR'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 11
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 157
      end
      object DenUsloviGO: TcxDBTextEdit
        Left = 828
        Top = 112
        Hint = 
          #1050#1086#1083#1082#1091' '#1089#1083#1086#1073#1086#1076#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1076#1086#1076#1072#1074#1072#1072#1090' '#1085#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088', '#1082#1072#1082#1086' '#1088#1077#1079#1091#1083 +
          #1090#1072#1090' '#1085#1072' '#1087#1086#1089#1077#1073#1085#1080#1090#1077' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'DEN_USLOVI_RABOTA'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 10
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 157
      end
      object UsloviRabota: TcxDBTextEdit
        Left = 828
        Top = 89
        Hint = 
          #1055#1088#1086#1094#1077#1085#1090' '#1085#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1080#1086#1090' '#1073#1086#1076' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1091#1089#1083#1086#1074#1080#1090#1077 +
          ' '#1079#1072' '#1088#1072#1073#1086#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'USLOVI_RABOTA'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 9
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 109
      end
      object KATEGORIJA_SIFRA: TcxDBTextEdit
        Left = 109
        Top = 46
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1043#1088#1091#1087#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'KATEGORIJA'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 60
      end
      object KATEGORIJA: TcxDBLookupComboBox
        Left = 171
        Top = 46
        Hint = #1054#1087#1080#1089' '#1085#1072' '#1043#1088#1091#1087#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'KATEGORIJA'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'KATEGORIJA'
          end>
        Properties.ListSource = dm.dsKategorijaRM
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 306
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 26
      Top = 192
      Caption = ' '#1054#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' '
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 3
      DesignSize = (
        985
        112)
      Height = 112
      Width = 985
      object Label7: TLabel
        Left = 3
        Top = 20
        Width = 117
        Height = 34
        Hint = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label8: TLabel
        Left = 13
        Top = 53
        Width = 107
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1087#1080#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 13
        Top = 75
        Width = 107
        Height = 31
        Hint = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object txtObrazovanie: TcxDBTextEdit
        Left = 126
        Top = 24
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
        BeepOnEnter = False
        DataBinding.DataField = 'OBRAZOVANIE'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 60
      end
      object cbObrazovanie: TcxDBLookupComboBox
        Left = 187
        Top = 24
        Hint = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OBRAZOVANIE'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'opis'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsObrazovanie
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 766
      end
      object txtSZObrazovanie: TcxDBTextEdit
        Left = 126
        Top = 79
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        BeepOnEnter = False
        DataBinding.DataField = 'SZO_NAZIV'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 60
      end
      object cbSZObrazovanie: TcxDBLookupComboBox
        Left = 187
        Top = 79
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1057#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'SZO_NAZIV'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'NAZIV'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'opis'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmSis.dsSZObrazovanie
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 766
      end
      object txtObrazovanieOpis: TcxDBMemo
        Left = 126
        Top = 51
        Hint = 
          #1054#1087#1080#1089' '#1085#1072' '#1074#1080#1076' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077', '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072',  '#1092#1072#1082#1091#1083#1090#1077#1090', '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088 +
          #1072#1079#1086#1074#1072#1085#1080#1077' ...'
        DataBinding.DataField = 'OBRAZOVANIE_OPIS'
        DataBinding.DataSource = dmSis.dsRabotnoMesto
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        Style.LookAndFeel.Kind = lfUltraFlat
        Style.LookAndFeel.NativeStyle = True
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfUltraFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfUltraFlat
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfUltraFlat
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 2
        OnDblClick = txtObrazovanieOpisDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 22
        Width = 827
      end
    end
    object txtUslovi: TcxDBMemo
      Left = 153
      Top = 365
      Hint = #1055#1086#1089#1077#1073#1085#1080' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086
      DataBinding.DataField = 'USLOVI'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      Properties.ScrollBars = ssVertical
      Properties.WantReturns = False
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.LookAndFeel.NativeStyle = True
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 8
      OnDblClick = aUrediTextUsloviExecute
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 36
      Width = 826
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 795
      Top = 6
      Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086' ('#1072#1082#1090#1080#1074#1077#1085' '#1080#1083#1080' '#1085#1077#1072#1082#1090#1080#1074#1077#1085')'
      Caption = ' '#1057#1090#1072#1090#1091#1089
      DataBinding.DataField = 'AKTIVNO'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentBackground = False
      ParentColor = False
      Properties.Columns = 2
      Properties.DefaultValue = 1
      Properties.Items = <
        item
          Caption = #1040#1082#1090#1080#1074#1085#1086
          Value = 1
        end
        item
          Caption = #1053#1077' '#1072#1082#1090#1080#1074#1085#1086
          Value = 0
        end>
      Style.Color = clBtnFace
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.TextColor = clRed
      Style.TextStyle = []
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 17
      Height = 38
      Width = 184
    end
    object txtOdgovaraPred: TcxDBTextEdit
      Left = 152
      Top = 464
      Hint = 
        #1055#1088#1077#1076' '#1082#1086#1112' '#1089#1077' '#1086#1076#1075#1086#1074#1072#1088#1072' '#1079#1072' '#1080#1079#1074#1088#1096#1077#1085#1072#1090#1072' '#1088#1072#1073#1086#1090#1072' ('#1076#1080#1088#1077#1082#1090#1086#1088', '#1088#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083 +
        '...)'
      BeepOnEnter = False
      DataBinding.DataField = 'ODGOVARA_PRED'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 14
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 827
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 139
      Top = 18
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1072#1073#1086#1085#1086' '#1052#1077#1089#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 639
    end
    object cxButton2: TcxButton
      Left = 1116
      Top = 414
      Width = 34
      Height = 21
      Action = aUrediTextUslovi
      Anchors = [akRight, akBottom]
      LookAndFeel.NativeStyle = True
      TabOrder = 10
      Visible = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnKeyDown = EnterKakoTab
    end
    object txtRabIsk: TcxDBTextEdit
      Left = 152
      Top = 338
      Hint = 
        #1055#1086#1090#1072#1073#1086#1090#1085#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086' '#1080#1089#1082#1091#1089#1090#1074#1086'  ('#1041#1077#1079' '#1088#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086', 1 '#1075#1086 +
        #1076#1080#1085#1072', '#1053#1072#1112#1084#1072#1083#1082#1091' 3 '#1075#1086#1076#1080#1085#1080'...)'
      BeepOnEnter = False
      DataBinding.DataField = 'RABOTNO_ISKUSTVO'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 361
    end
    object txtRabotnoVreme: TcxDBSpinEdit
      Left = 152
      Top = 437
      Hint = #1056#1072#1073#1086#1090#1085#1080' '#1095#1072#1089#1072' '#1085#1077#1076#1077#1083#1085#1086' '
      DataBinding.DataField = 'RABOTNO_VREME'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      TabOrder = 11
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 124
    end
    object txtTipRV: TcxDBTextEdit
      Left = 490
      Top = 437
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1058#1080#1087' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077
      BeepOnEnter = False
      DataBinding.DataField = 'ID_TIP_RABOTNO_VREME'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 12
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cbTipRV: TcxDBLookupComboBox
      Left = 552
      Top = 437
      Hint = 
        #1058#1080#1087' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077'  ('#1087#1088#1077#1090#1087#1083#1072#1076#1085#1077', '#1087#1086#1087#1083#1072#1076#1085#1077', '#1076#1074#1086#1082#1088#1072#1090#1085#1086', '#1074#1086' '#1089#1084#1077#1085#1080 +
        '...)'
      DataBinding.DataField = 'ID_TIP_RABOTNO_VREME'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmSis.dsTipRabotnoVreme
      TabOrder = 13
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 427
    end
    object txtStSlozenost: TcxDBTextEdit
      Left = 152
      Top = 312
      Hint = 
        #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086' ('#1087#1086' '#1082#1086#1083#1077#1082#1090#1080#1074#1077#1085' '#1076#1086#1075#1086#1074#1086 +
        #1088')'
      BeepOnEnter = False
      DataBinding.DataField = 'STEPEN_SLOZENOST'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 124
    end
    object txtGrupaPlata: TcxDBTextEdit
      Left = 624
      Top = 312
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1043#1088#1091#1087#1072' '#1085#1072' '#1055#1083#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'GRUPA_PLATA'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cbGrupaPlata: TcxDBLookupComboBox
      Left = 685
      Top = 312
      Hint = #1054#1087#1080#1089' '#1085#1072' '#1043#1088#1091#1087#1072' '#1085#1072' '#1055#1083#1072#1090#1072
      DataBinding.DataField = 'GRUPA_PLATA'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 200
          FieldName = 'ID'
        end
        item
          Width = 800
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmSis.dsGrupaPlata
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 294
    end
    object txtLokacija: TcxDBTextEdit
      Left = 153
      Top = 410
      Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1088#1072#1073#1086#1090#1072#1090#1072'. '
      BeepOnEnter = False
      DataBinding.DataField = 'LOKACIJA_RM'
      DataBinding.DataSource = dmSis.dsRabotnoMesto
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 372
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1212
    ExplicitWidth = 1212
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 722
    Width = 1212
    ExplicitTop = 722
    ExplicitWidth = 1212
  end
  object SplitterdPanel: TcxSplitter [4]
    Left = 0
    Top = 208
    Width = 1212
    Height = 8
    HotZoneClassName = 'TcxMediaPlayer8Style'
    HotZone.SizePercent = 33
    AlignSplitter = salBottom
    AllowHotZoneDrag = False
    Control = dPanel
    ExplicitWidth = 8
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 640
    Top = 360
  end
  inherited PopupMenu1: TPopupMenu
    Left = 632
    Top = 32
  end
  inherited dxBarManager1: TdxBarManager
    Left = 704
    Top = 64
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 157
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 348
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aStatusPM
      Category = 0
      LargeImageIndex = 14
    end
  end
  inherited ActionList1: TActionList
    Left = 608
    Top = 360
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aStatusPM: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1056#1052
      OnExecute = aStatusPMExecute
    end
    object aUrediTextOpis: TAction
      Caption = '...'
      OnExecute = aUrediTextOpisExecute
    end
    object aUrediTextUslovi: TAction
      Caption = '...'
      OnExecute = aUrediTextUsloviExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 984
    Top = 40
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40249.507547986110000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 848
    Top = 56
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 768
    Top = 8
  end
end
