{*******************************************************}
{                                                       }
{     ��������� : ������ �������� - ����������         }
{                 ������ ������� - ������� �������                                      }
{     ����� : 05.03.2010                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

unit Organogram;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsdxBarPainter, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsdxRibbonPainter, dxSkinscxPCPainter, dxorgchr,
  dxdborgc, cxPC, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, dxBar,
  ExtCtrls, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxGridCustomView, cxGrid, Menus, StdCtrls, cxButtons, ActnList,
  cxContainer, ComCtrls, dxtree, dxdbtree, cxTextEdit, cxDBEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxPCdxBarPopupMenu, dxRibbonSkins, cxCheckBox;

type
  TfrmOrganogram = class(TForm)
    Panel1: TPanel;
    dxBarManager1: TdxBarManager;
    rtMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    pcRE: TcxPageControl;
    tcRabEd: TcxTabSheet;
    tcOrganogram: TcxTabSheet;
    orgRe: TdxDbOrgChart;
    rtOrganogram: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    ActionList1: TActionList;
    aVeritikalno: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    aHorizontalno: TAction;
    aIzlez: TAction;
    lPanel: TPanel;
    dxTree: TdxDBTreeView;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1TIP_NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    aDodadiRabEdinica: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aBrisiRabEdinica: TAction;
    aAzurirajRabEdinica: TAction;
    dxBarManager1Bar2: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton6: TdxBarLargeButton;
    aPomos: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarLargeButton9: TdxBarLargeButton;
    aOsvezi: TAction;
    dPanel: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    Koren: TcxDBTextEdit;
    Rakovoditel: TcxDBTextEdit;
    Poteklo: TcxDBTextEdit;
    Spisok: TcxDBTextEdit;
    TipPartner: TcxDBTextEdit;
    Partner: TcxDBTextEdit;
    cbRE: TcxDBCheckBox;
    cbMagacin: TcxDBCheckBox;
    cbTroskovnoMesto: TcxDBCheckBox;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cBoxExtPartner: TcxExtLookupComboBox;
    procedure orgRECreateNode(Sender: TObject; Node: TdxOcNode);
    procedure FormCreate(Sender: TObject);
    procedure orgREDeletion(Sender: TObject; Node: TdxOcNode);
    procedure aVeritikalnoExecute(Sender: TObject);
    procedure aHorizontalnoExecute(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure aIzlezExecute(Sender: TObject);
    procedure aDodadiRabEdinicaExecute(Sender: TObject);
    procedure OtkaziButtonClick(Sender: TObject);
    procedure ZapisiButtonClick(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aAzurirajRabEdinicaExecute(Sender: TObject);
    procedure aBrisiRabEdinicaExecute(Sender: TObject);
    procedure SifraExit(Sender: TObject);
    procedure NAZIVExit(Sender: TObject);
    procedure KORENExit(Sender: TObject);
    procedure RAKOVODITELExit(Sender: TObject);
    procedure TIPPARTNERExit(Sender: TObject);
    procedure PARTNERExit(Sender: TObject);
    procedure PARTNERNAZIVExit(Sender: TObject);
    procedure SifraEnter(Sender: TObject);
    procedure NAZIVEnter(Sender: TObject);
    procedure KORENEnter(Sender: TObject);
    procedure RAKOVODITELEnter(Sender: TObject);
    procedure TIPPARTNEREnter(Sender: TObject);
    procedure PARTNEREnter(Sender: TObject);
    procedure PARTNERNAZIVEnter(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
    procedure TIPPARTNERPropertiesEditValueChanged(Sender: TObject);
    procedure PARTNERPropertiesEditValueChanged(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure PARTNERNAZIVPropertiesEditValueChanged(Sender: TObject);
  private
   FChange : Boolean;
    { Private declarations }
  protected
    prva, posledna :TWinControl;
  public
    { Public declarations }
  end;

var
  frmOrganogram: TfrmOrganogram;
  StateActive:TDataSetState;
implementation

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, Utils, DaNe;
type
  TNodeData = record
    Color: TColor;
    Name: string;
  end;
  PNodeData = ^TNodeData;

{$R *.dfm}

procedure TfrmOrganogram.aAzurirajRabEdinicaExecute(Sender: TObject);
begin
     if (StateActive in [dsBrowse]) then
        begin
          if (tcRabEd.Visible = true) then
             begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               Sifra.Enabled:=False;
               dm.tblRe.edit;
               StateActive:=dsEdit;
               NAZIV.SetFocus;
             end;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmOrganogram.aBrisiRabEdinicaExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
       begin
         if (tcRabEd.Visible = true) then
            begin
              frmDaNe:=TfrmDaNe.Create(self,'����Ō�!!!!!','���� ��� ������� ���� ������ �� �� ������� �������?',1);
              if frmDaNe.ShowModal =mrYes then
                 dm.tblRe.Delete;
              dm.tblRe.Refresh;
            end
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmOrganogram.aDodadiRabEdinicaExecute(Sender: TObject);
begin
     if (StateActive in [dsBrowse]) then
       begin
         if (tcRabEd.Visible = true) then
            begin
              dPanel.Enabled:=True;
              lPanel.Enabled:=False;
              dm.tblRe.Insert;
              StateActive:=dsInsert;
              prva.SetFocus;
             end
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmOrganogram.aHorizontalnoExecute(Sender: TObject);
begin
     orgRe.Rotated:=False;
end;

procedure TfrmOrganogram.aIzlezExecute(Sender: TObject);
begin
     if StateActive in [dsInsert, dsEdit] then
         begin
           dm.tblRe.Cancel;
           StateActive:=dsBrowse;
           RestoreControls(dPanel);
           dPanel.Enabled:=False;
           lPanel.Enabled:=True;
           Sifra.Enabled:=True;
         end
      else
         Close;
end;

procedure TfrmOrganogram.aOsveziExecute(Sender: TObject);
begin
     dm.tblRe.Refresh;
end;

procedure TfrmOrganogram.aOtkaziExecute(Sender: TObject);
begin
     StateActive:=dsBrowse;
     dm.tblRe.Cancel;
     RestoreControls(dPanel);
     dPanel.Enabled:=False;
     lPanel.Enabled:=True;
     Sifra.Enabled:=True;
end;

procedure TfrmOrganogram.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(4);
end;

procedure TfrmOrganogram.aVeritikalnoExecute(Sender: TObject);
begin
     orgRe.Rotated:=True;
end;

procedure TfrmOrganogram.aZapisiExecute(Sender: TObject);
begin
     if Validacija(dPanel) = false then
        begin
          dm.tblReR.Value:=1;
          dm.tblRe.Post;
          StateActive:=dsBrowse;
          Sifra.Enabled:=True;
          dPanel.Enabled:=False;
          lPanel.Enabled:=True;
          dm.tblRe.FullRefresh;
        end;
end;

procedure TfrmOrganogram.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
     if ANewTab = rtMeni then
        begin
          tcRabEd.Enabled:=True;
          pcRE.ActivePage:=tcRabEd;
          tcOrganogram.Enabled:=False;
        end
     else if ANewTab = rtOrganogram then
        begin
          tcOrganogram.Enabled:=True;
          pcRE.ActivePage:=tcOrganogram;
          tcRabEd.Enabled:=false;
        end;
end;

procedure TfrmOrganogram.FormCreate(Sender: TObject);
begin
//  orgRE.WidthFieldName  := 'naziv';
//  FChange := True;
//
//  if (PARTNER.Text <> '') and (TIPPARTNER.Text <> '') then
//       begin
//          dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TIPPARTNER.Text), StrToInt(PARTNER.Text)]) , []);
//          PARTNERNAZIV.Text:=dmMat.tblPartnerNAZIV.Value;
//       end
//  else PARTNERNAZIV.Text:='';
//
//  PrvPosledenTab(dPanel, posledna, prva);
//  StateActive:=dsBrowse;
//
//  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmOrganogram.KORENEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.KORENExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.NAZIVEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.NAZIVExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.orgRECreateNode(Sender: TObject;
  Node: TdxOcNode);
begin
  with Node, dmMat.tblRE do
  begin
    if FindField('id').AsInteger > 50 then
      Width := FindField('id').AsInteger;
   // if FindField('height').AsInteger > 50 then
    //  Height := FindField('height').AsInteger;
  //  Color := FindField('color').AsInteger;
    Data := New(PNodeData);
    PNodeData(Data)^.Color := Color;
    PNodeData(Data)^.Name := FindField('PartnerLookUp').AsString;
  end;
end;

procedure TfrmOrganogram.orgREDeletion(Sender: TObject; Node: TdxOcNode);
begin
 if Assigned(Node.Data) then
    Dispose(PNodeData(Node.Data));
end;

procedure TfrmOrganogram.OtkaziButtonClick(Sender: TObject);
begin
     dm.tblRe.Post;
     dPanel.Enabled:=False;
     lPanel.Enabled:=True;
end;

procedure TfrmOrganogram.PARTNEREnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.PARTNERExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.PARTNERNAZIVEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.PARTNERNAZIVExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.PARTNERNAZIVPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (StateActive in [dsInsert,dsEdit]) then
        begin
          dm.tblreTIP_PARTNER.Value:= dmMat.tblPartnerTIP_PARTNER.Value ;
          dm.tblRePARTNER.Value:=dmMat.tblPartnerID.Value;
        end;
end;

procedure TfrmOrganogram.PARTNERPropertiesEditValueChanged(Sender: TObject);
begin
//     if (PARTNER.Text <> '') and (TIPPARTNER.Text <> '') then
//       begin
//          dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TIPPARTNER.Text), StrToInt(PARTNER.Text)]) , []);
//          PARTNERNAZIV.Text:=dmMat.tblPartnerNAZIV.Value;
//       end
//     else PARTNERNAZIV.Text:='';
end;

procedure TfrmOrganogram.RAKOVODITELEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.RAKOVODITELExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.SifraEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.SifraExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.TIPPARTNEREnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmOrganogram.TIPPARTNERExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOrganogram.TIPPARTNERPropertiesEditValueChanged(Sender: TObject);
begin
//      if (PARTNER.Text <> '') and (TIPPARTNER.Text <> '') then
//       begin
//          dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TIPPARTNER.Text), StrToInt(PARTNER.Text)]) , []);
//          PARTNERNAZIV.Text:=dmMat.tblPartnerNAZIV.Value;
//       end
//     else PARTNERNAZIV.Text:='';
end;

procedure TfrmOrganogram.ZapisiButtonClick(Sender: TObject);
begin
     dm.tblRe.Cancel;
     dPanel.Enabled:=False;
     lPanel.Enabled:=True;
end;

procedure TfrmOrganogram.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

end.
