{*******************************************************}
{                                                       }
{     ��������� : ������ ��������                      }
{                                                       }
{     ����� : 17.03.2010                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

unit Sistematizacija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxLabel, cxHint, FIBDataSet, pFIBDataSet,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver, frxDesgn, frxClass, frxDBSet, frxRich,
  frxCross, frxEditDataBand, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinOffice2013White, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions;

type
  TfrmSistematizacija = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1OD_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DO_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    txtOpis: TcxDBTextEdit;
    Label3: TLabel;
    txtDatumOd: TcxDBDateEdit;
    Label4: TLabel;
    txtDatumDo: TcxDBDateEdit;
    Label5: TLabel;
    txtDokument: TcxDBTextEdit;
    cxButton1: TcxButton;
    OpenDialog1: TOpenDialog;
    cxHintStyleController1: TcxHintStyleController;
    tblPosSis: TpFIBDataSet;
    tblPosSisIMA: TFIBIntegerField;
    aZatvoriSistematizacija: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton11: TdxBarLargeButton;
    aPregledajSistematizacija: TAction;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    aSistematizacijaMestaSlobodni: TAction;
    dxBarButton3: TdxBarButton;
    aSablonZaSistematizacija: TAction;
    dxBarButton4: TdxBarButton;
    aSistematizacijaFR: TAction;
    dxBarButton5: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton6: TdxBarButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aZatvoriSistematizacijaExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aPregledajSistematizacijaExecute(Sender: TObject);
    procedure aSistematizacijaMestaSlobodniExecute(Sender: TObject);
    procedure aSablonZaSistematizacijaExecute(Sender: TObject);
    procedure aSistematizacijaFRExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSistematizacija: TfrmSistematizacija;
  StateActive:TDataSetState;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem, dmUnit,utils,
  DaNe, dmReportUnit, dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmSistematizacija.aAzurirajExecute(Sender: TObject);
begin
  //inherited;
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    if cxGrid1DBTableView1DO_DATUM.EditValue=Null then
      begin
         dPanel.Enabled:=True;
         lPanel.Enabled:=False;
         prva.SetFocus;
         cxGrid1DBTableView1.DataController.DataSet.Edit;
      end
      else
      begin
        ShowMessage('���������������� � ���������! �� ���� �� �� ������� �������.');
        Abort;
      end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmSistematizacija.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(6);
end;

procedure TfrmSistematizacija.aNovExecute(Sender: TObject);
begin
//  inherited;
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    tblPosSis.Close;
    tblPosSis.ParamByName('firma').AsInteger:=firma;
    tblPosSis.Open;
    if tblPosSisIMA.Value=0 then
       begin
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          txtDatumDo.Enabled:=False;
          prva.SetFocus;
          cxGrid1DBTableView1.DataController.DataSet.Insert;
          cxGrid1DBTableView1OD_DATUM.EditValue:=Now;
          dmSis.tblSistematizacijaID_RE_FIRMA.Value:=firma;
       end
       else
       begin
         ShowMessage('���������� �������������� � �� ���� �������.'+#10#13+'�� ����, ����� ���� �� �� ������� ���������'+#10#13+' �������������� (�.�. ������� ������ �� �����)!');
         Abort;
       end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmSistematizacija.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  txtDatumDo.Enabled:=False;
end;

procedure TfrmSistematizacija.aPregledajSistematizacijaExecute(Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30090;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblPregledSistematizacija.Close;
     dm.tblPregledSistematizacija.ParamByName('sistematizacija').Value:=dmSis.tblSistematizacijaID.Value;
     dm.tblPregledSistematizacija.ParamByName('firma').AsInteger:=firma;
     dm.tblPregledSistematizacija.open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.Variables.AddVariable('VAR', 'SISTEMATIZACIJA', QuotedStr(dmSis.tblSistematizacijaOPIS.Value));

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmSistematizacija.aSablonZaSistematizacijaExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.Sistematizacija.Close;
  dmReport.Sistematizacija.ParamByName('firma').AsInteger:=firma;
  dmReport.Sistematizacija.open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.SistematizacijaDATA.IsNull then
     begin
       RptStream := dmReport.Sistematizacija.CreateBlobStream(dmReport.SistematizacijaDATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
       begin
       RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
       If RichView <> Nil Then
          begin
           RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
           RichView.DataSet:=dmReport.frxSistematizacija;
         end;
       MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
       if MasterData <> Nil then
          MasterData.DataSet:=dmReport.frxSistematizacija;
       //  dmt.frxReport1.Variables.AddVariable('VAR', 'file',value);

       dmReport.frxReport1.ShowReport;

       frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 6);
       end;
     end;
end;

procedure TfrmSistematizacija.aSistematizacijaFRExecute(Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30092;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dmReport.Sistematizacija.Close;
     dmReport.Sistematizacija.ParamByName('firma').AsInteger:=firma;
     dmReport.Sistematizacija.open;

     dm.tblPregledSistematizacija.Close;
     dm.tblPregledSistematizacija.ParamByName('sistematizacija').Value:=dmSis.tblSistematizacijaID.Value;
     dm.tblPregledSistematizacija.ParamByName('firma').AsInteger:=firma;
     dm.tblPregledSistematizacija.open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmSistematizacija.aSistematizacijaMestaSlobodniExecute(
  Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30091;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblPopolnetiRabMesta.Close;
     dm.tblPopolnetiRabMesta.ParamByName('POTEKLO').Value:=IntToStr(firma)+','+'%';
     dm.tblPopolnetiRabMesta.ParamByName('firma').Value:=firma;
     dm.tblPopolnetiRabMesta.open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.Variables.AddVariable('VAR', 'SISTEMATIZACIJA', QuotedStr(dmSis.tblSistematizacijaOPIS.Value));

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmSistematizacija.aZapisiExecute(Sender: TObject);
var  st: TDataSetState;
     pom, pom1, pom2:integer;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        if  st = dsInsert then
           pom:= 1;
        pom1:=dmMat.zemiMax(dmSis.pSistematizacijaID, 'firma', Null, Null, firma, Null, Null, 'sid_out') - 1;
        cxGrid1DBTableView1.DataController.DataSet.Post;
        pom2:=dmMat.zemiMax(dmSis.pSistematizacijaID, 'firma', Null, Null, firma, Null, Null, 'sid_out') - 1;
        txtDatumDo.Enabled:=false;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
        if (pom = 1) and (pom1 <> 0) then
           begin
             frmDaNe:=TfrmDaNe.Create(self,'��������� �� ���� ��������������!','���� ������ �� �� ���������� ����������� '+#10#13+'�� ��������� ����� �� ����������� ��������������?',1);
             if frmDaNe.ShowModal =mrYes then
               begin
                 dmSis.zemiProcedura(dmsis.pSistematizacija,'firma', 'SID_PO_IN', 'SID_PRED_IN', firma, pom2, pom1);
                 dmSis.tblRMRE.FullRefresh;
               end;
           end;
    end;
  end;
end;

procedure TfrmSistematizacija.aZatvoriSistematizacijaExecute(Sender: TObject);
begin
 // inherited;
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    if cxGrid1DBTableView1DO_DATUM.EditValue=Null then
      begin
       frmDaNe:=TfrmDaNe.Create(self,'���������!','���� ��� ������� ���� ������ �� �� ��������� ����������������?'+#10#13+' ����������� �������  �� �e �����!',1);
        if frmDaNe.ShowModal =mrYes then
        begin
           dPanel.Enabled:=True;
           lPanel.Enabled:=False;
           txtDatumDo.Enabled:=True;
           txtDatumDo.SetFocus;
           cxGrid1DBTableView1.DataController.DataSet.Edit;
           cxGrid1DBTableView1DO_DATUM.EditValue:=Now;
        end;
      end
      else
      begin
        ShowMessage('���������������� � ���������! �� ���� �� �� ������� �������.');
        Abort;
      end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmSistematizacija.cxButton1Click(Sender: TObject);
begin
  inherited;
  // �� ������ Dialog  �� ����� �� ��������, �� �� �� ������� ��������� �� ����������������
   openDialog1.Filter := 'PDF ��� Word ���������|*.txt;*.doc;*.pdf';
   if OpenDialog1.Execute then
   begin
      dmsis.tblSistematizacijaDOKUMENT.Value:=OpenDialog1.FileName;
   end;
   ZapisiButton.SetFocus;
end;

procedure TfrmSistematizacija.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if Sender=TEdit(txtDatumDo) then
   begin
     if (txtDatumDo.EditValue<txtDatumOd.EditValue) and (txtDatumDo.Text<>'') then
        begin
          ShowMessage('��������� ����� � ������� �� ���������. �� ������ ������� ��������!');
          txtDatumDo.SetFocus;
          Abort;
        end;
   end;
end;

end.
