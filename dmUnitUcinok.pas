unit dmUnitUcinok;

interface

uses
  SysUtils, Classes, ImgList, Controls, cxGraphics, DB, FIBDataSet, pFIBDataSet,
  frxDesgn, frxClass, frxDBSet, frxRich, frxCross, FIBQuery, pFIBQuery,
  pFIBStoredProc, FIBDatabase, pFIBDatabase;

type
  TdmUcinok = class(TDataModule)
    tblRatingFrom: TpFIBDataSet;
    dsRatingFrom: TDataSource;
    tblFeedBack: TpFIBDataSet;
    dsFeedBack: TDataSource;
    tblRatingFromID: TFIBIntegerField;
    tblRatingFromNAZIV: TFIBStringField;
    tblFeedBackID: TFIBIntegerField;
    tblFeedBackDATUM: TFIBDateField;
    tblFeedBackDATUM_REVIZIJA: TFIBDateField;
    tblFeedBackOPIS: TFIBStringField;
    tblPrasanja: TpFIBDataSet;
    dsPrasanja: TDataSource;
    tblPrasanjaID: TFIBIntegerField;
    tblPrasanjaNAZIV: TFIBStringField;
    tblPrasanjaTS_INS: TFIBDateTimeField;
    tblPrasanjaTS_UPD: TFIBDateTimeField;
    tblPrasanjaUSR_INS: TFIBStringField;
    tblPrasanjaUSR_UPD: TFIBStringField;
    tblGrupaPrasanja: TpFIBDataSet;
    dsGrupaPrasanja: TDataSource;
    tblGrupaPrasanjaID: TFIBIntegerField;
    tblGrupaPrasanjaNAZIV: TFIBStringField;
    tblGrupaPrasanjaNORMATIV: TFIBFloatField;
    tblGrupaPrasanjaTS_INS: TFIBDateTimeField;
    tblGrupaPrasanjaTS_UPD: TFIBDateTimeField;
    tblGrupaPrasanjaUSR_INS: TFIBStringField;
    tblGrupaPrasanjaUSR_UPD: TFIBStringField;
    tblIspitanici: TpFIBDataSet;
    dsIspitanici: TDataSource;
    tblIspitaniciID: TFIBIntegerField;
    tblIspitaniciFEEDBACK_ID: TFIBIntegerField;
    tblIspitaniciMB: TFIBStringField;
    tblIspitaniciTS_INS: TFIBDateTimeField;
    tblIspitaniciTS_UPD: TFIBDateTimeField;
    tblIspitaniciUSR_INS: TFIBStringField;
    tblIspitaniciUSR_UPD: TFIBStringField;
    tblOcenuvaci: TpFIBDataSet;
    dsOcenuvaci: TDataSource;
    tblOcenuvaciID: TFIBIntegerField;
    tblOcenuvaciFEEDBACK_ID: TFIBIntegerField;
    tblOcenuvaciRATING_FROM_ID: TFIBIntegerField;
    tblOcenuvaciTS_INS: TFIBDateTimeField;
    tblOcenuvaciTS_UPD: TFIBDateTimeField;
    tblOcenuvaciUSR_INS: TFIBStringField;
    tblOcenuvaciUSR_UPD: TFIBStringField;
    tblOcenuvaciOCENUVACNAZIV: TFIBStringField;
    tblPrasalnik: TpFIBDataSet;
    dsPrasalnik: TDataSource;
    tblPrasanjaGRUPA: TFIBIntegerField;
    tblPrasanjaGRUPANAZIV: TFIBStringField;
    tblPrasanjaNORMATIV: TFIBFloatField;
    pMaxBrojFeedback: TpFIBStoredProc;
    tblFeedBackBROJ: TFIBIntegerField;
    tblPrasalnikID: TFIBIntegerField;
    tblPrasalnikFEEDBACK_ID: TFIBIntegerField;
    tblPrasalnikPRASANJE_ID: TFIBIntegerField;
    tblPrasalnikGRUPA: TFIBIntegerField;
    tblPrasalnikPRASANJENAZIV: TFIBStringField;
    tblPrasalnikGRUPANAZIV: TFIBStringField;
    tblPrasalnikTS_INS: TFIBDateTimeField;
    tblPrasalnikTS_UPD: TFIBDateTimeField;
    tblPrasalnikUSR_INS: TFIBStringField;
    tblPrasalnikUSR_UPD: TFIBStringField;
    frxpRASANJA: TfrxDBDataset;
    tblRezultati: TpFIBDataSet;
    dsRezultati: TDataSource;
    tblRezultatiID: TFIBIntegerField;
    tblRezultatiMB: TFIBStringField;
    tblRezultatiFEEDBACK_ID: TFIBIntegerField;
    tblRezultatiPRASANJE_ID: TFIBIntegerField;
    tblRezultatiOCENUVAC_ID: TFIBIntegerField;
    tblRezultatiVRABOTENNAZIV: TFIBStringField;
    tblRezultatiPRASANJENAZIV: TFIBStringField;
    tblRezultatiOCENUVACNAZIV: TFIBStringField;
    pInsertRezultati: TpFIBStoredProc;
    tblRezultatiOCENA: TFIBFloatField;
    pCountMBRezultati: TpFIBStoredProc;
    TransakcijaP: TpFIBTransaction;
    tblRezultatiGRUPAPRASANJENAZIV: TFIBStringField;
    tblRezultatiNORMATIV: TFIBFloatField;
    tblFeedBackID_RE_FIRMA: TFIBIntegerField;
    tblRatingFromTS_INS: TFIBDateTimeField;
    tblRatingFromTS_UPD: TFIBDateTimeField;
    tblRatingFromUSR_INS: TFIBStringField;
    tblRatingFromUSR_UPD: TFIBStringField;
    tblFeedBackTS_INS: TFIBDateTimeField;
    tblFeedBackTS_UPD: TFIBDateTimeField;
    tblFeedBackUSR_INS: TFIBStringField;
    tblFeedBackUSR_UPD: TFIBStringField;
    tblIspitaniciNAZIV_VRABOTEN: TFIBStringField;
    tblIspitaniciNAZIV_VRABOTEN_TI: TFIBStringField;
    pCountFedbackRezultati: TpFIBStoredProc;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmUcinok: TdmUcinok;

implementation

uses dmKonekcija, dmMaticni, dmReportUnit, dmResources, DaNe, dmUnit,
  dmUnitOtsustvo;

{$R *.dfm}

procedure TdmUcinok.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;
end.
