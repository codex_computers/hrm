unit AneksNaDogovor;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 25.04.2012                                }
{                                                       }
{     ������ : 1.1.1.19                                }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon,  ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit, cxCalendar,frxClass, frxDBSet, frxRich, frxCross, FIBQuery, pFIBQuery,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, FIBDataSet,
  pFIBDataSet, cxLabel, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxPScxSchedulerLnk, dxPSdxDBOCLnk, dxCustomHint, cxHint,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxMemo, cxRichEdit,
  cxDBRichEdit, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmAneksNaDogovor = class(TfrmMaster)
    Label2: TLabel;
    Broj: TcxDBTextEdit;
    DATUM: TcxDBDateEdit;
    Label3: TLabel;
    aKreirajDokument: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    PeriodNaVazenje: TcxGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    DO_VREME: TcxDBDateEdit;
    OD_VREME: TcxDBDateEdit;
    Plata: TcxGroupBox;
    txtPlata: TcxDBTextEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_DOGOVOR_VRABOUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1KOEFICIENT: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1PLATA: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1VIDVRABOTUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1BROJDOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    tblRMRERAKOVODENJE: TFIBBCDField;
    tblRMREDENOVI_ODMOR: TFIBIntegerField;
    tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField;
    tblRMRESTEPEN_SLOZENOST: TFIBBCDField;
    dsRMRE: TDataSource;
    PodatociZaVrabotuvanje: TcxGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label15: TLabel;
    txtVidVrabotuvanje: TcxDBTextEdit;
    cbVidVrabotuvanje: TcxDBLookupComboBox;
    ID_RM_RE: TcxDBTextEdit;
    ID_RM_RE_NAZIV: TcxDBLookupComboBox;
    RABOTNI_CASOVI: TcxDBTextEdit;
    MB: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label21: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Rakovodenje: TcxDBTextEdit;
    DenUsloviGO: TcxDBTextEdit;
    koeficient: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ_CASOVI: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarEditItem2: TcxBarEditItem;
    qMaxBroj: TpFIBQuery;
    txtArhivskiBroj: TcxTextEdit;
    cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn;
    dxBarLargeButton11: TdxBarLargeButton;
    aBrisiDokument: TAction;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    tblRMREUSLOVI_RABOTA: TFIBIntegerField;
    tblRMREUSLOVIRABOTAID: TFIBIntegerField;
    tblRMREUSLOVIRABOTAPROCENT: TFIBBCDField;
    cxGrid1DBTableView1USLOVIRABID: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVIRABNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVIRABPROCENT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    tblUsloviPoteskiOdNormalni: TpFIBDataSet;
    tblUsloviPoteskiOdNormalniID: TFIBIntegerField;
    tblUsloviPoteskiOdNormalniNAZIV: TFIBStringField;
    tblUsloviPoteskiOdNormalniPROCENT: TFIBBCDField;
    tblUsloviPoteskiOdNormalniTS_INS: TFIBDateTimeField;
    tblUsloviPoteskiOdNormalniTS_UPD: TFIBDateTimeField;
    tblUsloviPoteskiOdNormalniUSR_INS: TFIBStringField;
    tblUsloviPoteskiOdNormalniUSR_UPD: TFIBStringField;
    dsUsloviPoteskiOdNormalni: TDataSource;
    cxHintStyleController1: TcxHintStyleController;
    qCountAneksZaDogovor: TpFIBQuery;
    f: TcxBarEditItem;
    Label10: TLabel;
    BENEFICIRAN_STAZ: TcxDBTextEdit;
    Label11: TLabel;
    cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1BENEFICIRAN_STAZ: TcxGridDBColumn;
    tblRMREBENEFICIRAN_STAZ: TFIBBCDField;
    Label9: TLabel;
    OPIS: TcxDBRichEdit;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton16: TdxBarLargeButton;
    aUsloviPoteskiOdNormalni: TAction;
    tblRMRESIS_OPIS: TFIBStringField;
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aKreirajDokumentExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure fExit(Sender: TObject);
    procedure OPISDblClick(Sender: TObject);
    procedure aUsloviPoteskiOdNormalniExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAneksNaDogovor: TfrmAneksNaDogovor;
  arhivski_broj:string;
implementation

uses dmUnit, dmReportUnit, DaNe, Utils, dmUnitOtsustvo, dmKonekcija, Notepad,
  RabotnoMestoUsloviRabota;

{$R *.dfm}

procedure TfrmAneksNaDogovor.aAzurirajExecute(Sender: TObject);
begin
// if (((dm.tblDogVrabotuvanjeDATUM_DO.IsNull)and(strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_OD.Value)) <=strtodate(DateToStr(Now)))) or ((strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_DO.Value)) >= strtodate(DateToStr(Now)))and (strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_OD.Value)) <=strtodate(DateToStr(Now))))) then
//     begin
//        if (((dm.tblAneksDATUM_DO.IsNull)and(strtodate(DateToStr(dm.tblAneksDATUM_POCETOK.Value)) <=strtodate(DateToStr(Now)))) or ((strtodate(DateToStr(dm.tblAneksDATUM_DO.Value)) >= strtodate(DateToStr(Now)))and (strtodate(DateToStr(dm.tblAneksDATUM_POCETOK.Value)) <=strtodate(DateToStr(Now))))) then
//         begin
//            inherited;
//          end
//        else ShowMessage('��� ����� �� ������� �� ����������� �� � ������� !!!')
//     end
// else
//     ShowMessage('��� ������� �� ����������� �� � ������� !!!')

 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
       begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               OD_VREME.Enabled:=False;
               DO_VREME.Enabled:=false;
               //ID_RM_RE.Enabled:=False;
               //ID_RM_RE_NAZIV.Enabled:=False;
               Broj.SetFocus;
               cxGrid1DBTableView1.DataController.DataSet.Edit;
       end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmAneksNaDogovor.aBrisiDokumentExecute(Sender: TObject);
begin
  frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteAneks.Close;
            dmOtsustvo.qDeleteAneks.ParamByName('id').Value:=dm.tblAneksID.Value;
            dmOtsustvo.qDeleteAneks.ExecQuery;

           // dm.tblAneks.FullRefresh;
        end;
end;

procedure TfrmAneksNaDogovor.aBrisiExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
   begin
     {if (((dm.tblAneksDATUM_DO.IsNull)and(strtodate(DateToStr(dm.tblAneksDATUM_POCETOK.Value)) <=strtodate(DateToStr(Now)))) or ((strtodate(DateToStr(dm.tblAneksDATUM_DO.Value)) >= strtodate(DateToStr(Now)))and (strtodate(DateToStr(dm.tblAneksDATUM_POCETOK.Value)) <=strtodate(DateToStr(Now))))) then
         ShowMessage('��� ����� �� ������� � ������� !!! �������� �� �� ������� !!!')
     else
        begin  }
          frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
          if (frmDaNe.ShowModal <> mrYes) then
             Abort
          else
          begin
         { qCountAneksZaDogovor.Close;
          qCountAneksZaDogovor.ParamByName('id_dogovor').Value:=dm.tblDogVrabotuvanjeID.Value;
          qCountAneksZaDogovor.ExecQuery;
          if qCountAneksZaDogovor.FldByName['c'].Value = 1 then
             begin
                dmOtsustvo.qUpdateVrabotenRM.Close;
                dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID_RM_RE').Value:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
                dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMOD').Value:=dm.tblDogVrabotuvanjeDATUM_OD.Value;
                if dm.tblDogVrabotuvanjeDATUM_DO.isnull then
                   dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMDO').Value:=null
                else
                   dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMDO').Value:= dm.tblDogVrabotuvanjeDATUM_DO.Value;
                dmOtsustvo.qUpdateVrabotenRM.ParamByName('document_id').Value:=dm.tblDogVrabotuvanjeID.Value;
                dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').Value:=dogovorZaRabota;
               // dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').Value:=dm.tblDogVrabotuvanjeVID_DOKUMENT.Value;
                dmOtsustvo.qUpdateVrabotenRM.ParamByName('id').Value:=dm.tblDogVrabotuvanjeID.Value;
                dmOtsustvo.qUpdateVrabotenRM.ExecQuery;
              end;
          dmOtsustvo.qDeleteVrabotenRM.Close;
          dmOtsustvo.qDeleteVrabotenRM.ParamByName('documentID').Value:=dm.tblAneksID.Value;
          dmOtsustvo.qDeleteVrabotenRM.ParamByName('vid_dokument').Value:=aneksDogovor;
          dmOtsustvo.qDeleteVrabotenRM.ExecQuery;
          cxGrid1DBTableView1.DataController.DataSet.Delete();      }
          dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, Null, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re, 3,dm.tblAneksDATUM.Value,Null);
          dm.tblAneks.FullRefresh;
          end;
   end;
end;

procedure TfrmAneksNaDogovor.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(26);
end;

procedure TfrmAneksNaDogovor.aKreirajDokumentExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  dmReport.tblAneksReport.Close;
  dmReport.tblAneksReport.ParamByName('dogv').Value:=dm.tblDogVrabotuvanjeID.Value;
  dmReport.tblAneksReport.ParamByName('id').Value:= '%';
  dmReport.tblAneksReport.open;

  if not dmReport.tblAneksReportData.IsNull then
     begin
       RptStream := dmReport.tblAneksReport.CreateBlobStream(dmReport.tblAneksReportDATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
        begin
           RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
           dmReport.frxReport1.LoadFromStream(RptStream);

           RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
            If RichView <> Nil Then
              begin
                RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
                RichView.DataSet:=dmReport.frxAneks;
              end;

            MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
            if MasterData <> Nil then
              MasterData.DataSet:=dmReport.frxAneks;

            dmReport.frxReport1.ShowReport;

            frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
            if (frmDaNe.ShowModal = mrYes) then
               dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 8);
         end;
     end;
end;

procedure TfrmAneksNaDogovor.aNovExecute(Sender: TObject);
var pom, pom_aktiven_1,pom_aktiven_2:Integer;
begin

 //if (((dm.tblDogVrabotuvanjeDATUM_DO.IsNull)and(strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_OD.Value)) <=strtodate(DateToStr(Now)))) or ((strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_DO.Value)) >= strtodate(DateToStr(Now)))and (strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_OD.Value)) <=strtodate(DateToStr(Now))))) then
//    begin
//      pom:=dmOtsustvo.zemiBroj(dm.pAktivenAneks, 'DOGOVOR', Null, Null, Null,dm.tblDogVrabotuvanjeID.Value, Null, Null, Null, 'BROJ');
//      if pom <> 0 then
//         begin
//           ShowMessage('����� ������� ����� �� ������� �� ��� ���� !!!');
//         end
//      else
//         begin
            if txtArhivskiBroj.Text='' then
                begin
                  frmDaNe := TfrmDaNe.Create(self, '������ ����', '������ ������� �������� ���. ���� ������ �� ����������?', 1);
                  if (frmDaNe.ShowModal <> mrYes) then
                      begin
                        abort;
                      end
                  else
                      begin
                        arhivski_broj:='';
                      end;
                end
            else arhivski_broj:=txtArhivskiBroj.Text;

            txtArhivskiBroj.Enabled:=False;
            if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
              begin
                dPanel.Enabled:=True;
                lPanel.Enabled:=False;
                cxGrid1DBTableView1.DataController.DataSet.Insert;
                txtVidVrabotuvanje.SetFocus;
                txtArhivskiBroj.text:=arhivski_broj;

                dm.tblAneksDATUM.Value:=Now;
                dm.tblAneksID_DOGOVOR_VRABOUVANJE.Value:=dm.tblDogVrabotuvanjeID.Value;
                dm.tblAneksVID_VRABOTUVANJE.Value:=dm.tblDogVrabotuvanjeVID_VRABOTUVANJE.Value;
                dm.tblAneksID_RM_RE.Value:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
                dm.tblAneksBROJ_CASOVI.Value:=dm.tblDogVrabotuvanjeRABOTNO_VREME.Value;
                dm.tblAneksRAKOVODITEL.Value:=dm.tblDogVrabotuvanjeMB_DIREKTOR.Value;
                dm.tblAneksPLATA.Value:=dm.tblDogVrabotuvanjePLATA.Value;

                dm.tblAneksVID_DOKUMENT.Value:=aneksDogovor;

                if ID_RM_RE_NAZIV.Text <> '' then
                    begin
                      dm.tblAneksRAKOVODENJE.Value:=dm.tblDogVrabotuvanjeRAKOVODENJE.Value;
                      dm.tblAneksDEN_USLOVI_RABOTA.Value:=dm.tblDogVrabotuvanjeDEN_USLOVI_RABOTA.Value;
                      dm.tblAneksKOEFICIENT.Value:=dm.tblDogVrabotuvanjeKOEFICIENT.Value;
                      dm.tblAneksBENEFICIRAN_STAZ.Value:=dm.tblDogVrabotuvanjeBENEFICIRAN_STAZ.Value;
                    end;

                dm.tblAneksARHIVSKI_BROJ.value:=txtArhivskiBroj.text;
                qMaxBroj.Close;
                qMaxBroj.ParamByName('arhivski_broj').AsString:=txtArhivskiBroj.text;
                qMaxBroj.ExecQuery;
                dm.tblAneksBROJ.Value:=txtArhivskiBroj.text+'/'+inttostr(qMaxBroj.FldByName['maks'].AsInteger+1);
              end;
        // end
   // end
 //else  ShowMessage('��� ������� �� ����������� �� � ������� ��� ��� ������� ����� �� ������� !!!')

end;

procedure TfrmAneksNaDogovor.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  txtArhivskiBroj.Enabled:=True;
  OD_VREME.Enabled:=true;
  DO_VREME.Enabled:=true;
  ID_RM_RE.Enabled:=true;
  ID_RM_RE_NAZIV.Enabled:=true;
end;

procedure TfrmAneksNaDogovor.aUsloviPoteskiOdNormalniExecute(Sender: TObject);
begin
  inherited;
  frmRmReUsloviRabota:=TfrmRmReUsloviRabota.Create(Self, true);
  frmRmReUsloviRabota.Tag:=3;
  frmRmReUsloviRabota.ShowModal;
  frmRmReUsloviRabota.Free;
end;

procedure TfrmAneksNaDogovor.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  rmre: Integer;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
         begin
            if (OD_VREME.Text <> '')and (DO_VREME.Text <> '') and (DO_VREME.Date < OD_VREME.Date ) then
                begin
                  ShowMessage('������ � �������� ������ !!!');
                  DO_VREME.SetFocus;
                end
            else
                begin
                   qCountAneksZaDogovor.Close;
                   qCountAneksZaDogovor.ParamByName('id_dogovor').Value:=dm.tblDogVrabotuvanjeID.Value;
                   qCountAneksZaDogovor.ExecQuery;
                   if st in [dsInsert] then
                      begin
                        { if qCountAneksZaDogovor.FldByName['c'].Value = 0 then
                            begin
                               if dm.tblAneksDATUM_DO.isNull then
                                  begin
                                     dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, Null, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re, 1,dm.tblAneksDATUM.Value,Null);
                                  end
                               else
                                  begin
                                     dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, dm.tblAneksDATUM_DO.Value, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re,1,dm.tblAneksDATUM.Value,Null);
                                  end;
                               dmOtsustvo.qUpdateVrabotenRM.Close;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID_RM_RE').Value:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMOD').Value:=dm.tblDogVrabotuvanjeDATUM_OD.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMDO').Value:=dm.tblAneksDATUM_POCETOK.Value - 1;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('document_id').Value:=dm.tblDogVrabotuvanjeID.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').Value:=dogovorZaRabota;
                              // dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_DOKUMENT').Value:=dm.tblDogVrabotuvanjeVID_DOKUMENT.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('id').Value:=dm.tblDogVrabotuvanjeID.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ExecQuery;

                               cxGrid1DBTableView1.DataController.DataSet.Post;
                               dPanel.Enabled:=false;
                               lPanel.Enabled:=true;
                               cxGrid1.SetFocus;
                               txtArhivskiBroj.Enabled:=True;
                            end
                         else if qCountAneksZaDogovor.FldByName['c'].Value > 0 then
                            begin
                               if dm.tblAneksDATUM_DO.isNull then
                                  begin
                                     dmOtsustvo.insert(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA',Null,
                                      dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, Null, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re, Null);
                                  end
                               else
                                  begin
                                     dmOtsustvo.insert(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA',Null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, dm.tblAneksDATUM_DO.Value, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re, Null);
                                  end; }
                               if dm.tblAneksDATUM_DO.isNull then
                                  begin
                                     dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, Null, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re, 1,dm.tblAneksDATUM.Value,Null);
                                  end
                               else
                                  begin
                                     dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, dm.tblAneksDATUM_POCETOK.Value, dm.tblAneksDATUM_DO.Value, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re,1,dm.tblAneksDATUM.Value,Null);
                                  end;
                               cxGrid1DBTableView1.DataController.DataSet.Post;
                               dPanel.Enabled:=false;
                               lPanel.Enabled:=true;
                               OD_VREME.Enabled:=true;
                               DO_VREME.Enabled:=true;
                               //ID_RM_RE.Enabled:=true;
                               //ID_RM_RE_NAZIV.Enabled:=true;
                               cxGrid1.SetFocus;
                               txtArhivskiBroj.Enabled:=True;
                      end
                     // end
                   else if st in [dsEdit]then
                     begin
//                        dmOtsustvo.qUpdateVrabotenRM.Close;
//                         dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID_RM_RE').Value:=dm.tblAneksID_RM_RE.Value;
////                         dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMOD').Value:=dm.tblAneksDATUM_POCETOK.Value;
////                         if dm.tblAneksDATUM_DO.IsNull then
////                            dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMDO').Value:=Null
////                         else
////                            dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMDO').Value:=dm.tblAneksDATUM_DO.Value;
//                         dmOtsustvo.qUpdateVrabotenRM.ParamByName('document_id').Value:=dm.tblAneksID.Value;
//                         dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').Value:=aneksDogovor;
//                        // dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID').Value:=dm.tblAneksID.Value;
//                        // dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').Value:=dm.tblAneksVID_DOKUMENT.Value;
//                         dmOtsustvo.qUpdateVrabotenRM.ExecQuery;

                         cxGrid1DBTableView1.DataController.DataSet.Post;
                         dPanel.Enabled:=false;
                         lPanel.Enabled:=true;
                         OD_VREME.Enabled:=true;
                         DO_VREME.Enabled:=true;
                         cxGrid1.SetFocus;
                         txtArhivskiBroj.Enabled:=True;
                         dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                     dm.tblDogVrabotuvanjeMB.Value, dm.tblAneksID_RM_RE.Value, Null, Null, dm.tblAneksVID_DOKUMENT.Value, dm.tblAneksID.Value, dmKon.re, 5,Null,Null);

                         {if qCountAneksZaDogovor.FldByName['c'].Value = 1 then
                            begin
                               dmOtsustvo.qUpdateVrabotenRM.Close;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID_RM_RE').Value:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMOD').Value:=dm.tblDogVrabotuvanjeDATUM_OD.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('DATUMDO').Value:=dm.tblAneksDATUM_POCETOK.Value - 1;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('document_id').Value:=dm.tblDogVrabotuvanjeID.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').Value:=dogovorZaRabota;
                               dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID').Value:=dm.tblAneksID.Value;
                              // dmOtsustvo.qUpdateVrabotenRM.ParamByName('Z_DOKUMENT').Value:=dm.tblAneksVID_DOKUMENT.Value;
                               dmOtsustvo.qUpdateVrabotenRM.ExecQuery;
                            end }
                     end;

                end;

         end;
    end;

end;

procedure TfrmAneksNaDogovor.fExit(Sender: TObject);
begin
  inherited;
  ShowMessage(f.EditValue);
end;

procedure TfrmAneksNaDogovor.cxDBTextEditAllExit(Sender: TObject);
begin
  if Sender=TcxLookupComboBox(ID_RM_RE_NAZIV) then
    begin
       dm.tblAneksRAKOVODENJE.Value:=tblRMRERAKOVODENJE.Value;
       dm.tblAneksDEN_USLOVI_RABOTA.Value:=tblRMREDEN_USLOVI_RABOTA.Value;
       dm.tblAneksKOEFICIENT.Value:=tblRMRESTEPEN_SLOZENOST.Value;
       dm.tblAneksBENEFICIRAN_STAZ.Value:=tblRMREBENEFICIRAN_STAZ.Value;
    end
    else
    if Sender=TcxLookupComboBox(ID_RM_RE) then
    begin
       ID_RM_RE_NAZIV.Properties.ListOptions.SyncMode:=true;
    end;

    TEdit(Sender).Color:=clWhite;

end;

procedure TfrmAneksNaDogovor.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
  inherited;
   if  dxRibbon1.ActiveTab = dxRibbon1Tab1  then
      txtArhivskiBroj.Visible:=False
   else if  dxRibbon1.ActiveTab = dxRibbon1Tab2 then
      txtArhivskiBroj.Visible:=True;
end;

procedure TfrmAneksNaDogovor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  dm.tblDogVrabotuvanje.FullRefresh;
end;

procedure TfrmAneksNaDogovor.FormCreate(Sender: TObject);
begin
  inherited;
  

  tblRMRE.ParamByName('firma').Value:=dmKon.re;
  tblRMRE.Open;
end;

procedure TfrmAneksNaDogovor.OPISDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblAneks.CreateBlobStream(dm.tblAneksOPIS , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblAneksOPIS as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

end.
