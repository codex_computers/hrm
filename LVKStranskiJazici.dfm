inherited frmStranskiJazik: TfrmStranskiJazik
  Caption = #1057#1090#1088#1072#1085#1089#1082#1080' '#1112#1072#1079#1080#1094#1080
  ClientHeight = 653
  ExplicitWidth = 749
  ExplicitHeight = 691
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 258
    ExplicitHeight = 258
    inherited cxGrid1: TcxGrid
      Height = 254
      ExplicitHeight = 254
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsLVKStranskiJazik
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MOLBA'
          Visible = False
        end
        object cxGrid1DBTableView1ID_LVK: TcxGridDBColumn
          DataBinding.FieldName = 'ID_LVK'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 148
        end
        object cxGrid1DBTableView1RAZBIRANJE_SLUSANJE: TcxGridDBColumn
          DataBinding.FieldName = 'RazbiranjeSlusanje'
          Width = 131
        end
        object cxGrid1DBTableView1RAZBIRANJE_CITANJE: TcxGridDBColumn
          DataBinding.FieldName = 'RazbiranjeCitanje'
          Width = 124
        end
        object cxGrid1DBTableView1GOVOR_INTERAKCIJA: TcxGridDBColumn
          DataBinding.FieldName = 'GovorInterakcija'
          Width = 132
        end
        object cxGrid1DBTableView1GOVOR_PRODUKCIJA: TcxGridDBColumn
          DataBinding.FieldName = 'GovorProdukcija'
          SortIndex = 0
          SortOrder = soAscending
          Width = 139
        end
        object cxGrid1DBTableView1PISUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'PisuvanjeNaziv'
          Width = 98
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 384
    Height = 246
    ExplicitTop = 384
    ExplicitHeight = 246
    inherited Label1: TLabel
      Left = 66
      Top = 4
      Visible = False
      ExplicitLeft = 66
      ExplicitTop = 4
    end
    object Label11: TLabel [1]
      Left = 23
      Top = 34
      Width = 93
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1090#1088#1072#1085#1089#1082#1080' '#1112#1072#1079#1080#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 147
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      TabOrder = 1
      Visible = False
      ExplicitLeft = 147
      ExplicitTop = 4
      ExplicitWidth = 70
      Width = 70
    end
    inherited OtkaziButton: TcxButton
      Left = 598
      Top = 190
      TabOrder = 4
      ExplicitLeft = 598
      ExplicitTop = 190
    end
    inherited ZapisiButton: TcxButton
      Left = 517
      Top = 190
      TabOrder = 3
      ExplicitLeft = 517
      ExplicitTop = 190
    end
    object VID_STRANSKI_JAZIK_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 194
      Top = 31
      BeepOnEnter = False
      DataBinding.DataField = 'VID_STRANSKI_JAZIK'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dmSis.dsStranskiJazici
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 279
    end
    object VID_STRANSKI_JAZIK: TcxDBTextEdit
      Tag = 1
      Left = 122
      Top = 31
      BeepOnEnter = False
      DataBinding.DataField = 'VID_STRANSKI_JAZIK'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 71
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 23
      Top = 73
      Caption = #1057#1083#1091#1096#1072#1114#1077
      DataBinding.DataField = 'RAZBIRANJE_SLUSANJE'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1051#1086#1096#1086
          Value = 1
        end
        item
          Caption = #1044#1086#1073#1088#1086
          Value = 2
        end
        item
          Caption = #1054#1076#1083#1080#1095#1085#1086
          Value = 3
        end>
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 5
      Height = 40
      Width = 210
    end
    object cxDBRadioGroup2: TcxDBRadioGroup
      Left = 247
      Top = 73
      Caption = #1063#1080#1090#1072#1114#1077
      DataBinding.DataField = 'RAZBIRANJE_CITANJE'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1051#1086#1096#1086
          Value = 1
        end
        item
          Caption = #1044#1086#1073#1088#1086
          Value = 2
        end
        item
          Caption = #1054#1076#1083#1080#1095#1085#1086
          Value = 3
        end>
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 6
      Height = 40
      Width = 210
    end
    object cxDBRadioGroup3: TcxDBRadioGroup
      Left = 463
      Top = 73
      Caption = #1055#1080#1096#1091#1074#1072#1114#1077
      DataBinding.DataField = 'PISUVANJE'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1051#1086#1096#1086
          Value = 1
        end
        item
          Caption = #1044#1086#1073#1088#1086
          Value = 2
        end
        item
          Caption = #1054#1076#1083#1080#1095#1085#1086
          Value = 3
        end>
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 7
      Height = 40
      Width = 210
    end
    object cxDBRadioGroup4: TcxDBRadioGroup
      Left = 247
      Top = 129
      Caption = #1043#1086#1074#1086#1088' - '#1080#1085#1090#1077#1088#1072#1082#1094#1080#1112#1072
      DataBinding.DataField = 'GOVOR_INTERAKCIJA'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1051#1086#1096#1086
          Value = 1
        end
        item
          Caption = #1044#1086#1073#1088#1086
          Value = 2
        end
        item
          Caption = #1054#1076#1083#1080#1095#1085#1086
          Value = 3
        end>
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 8
      Height = 40
      Width = 210
    end
    object cxDBRadioGroup5: TcxDBRadioGroup
      Left = 23
      Top = 129
      Caption = #1043#1086#1074#1086#1088' - '#1087#1088#1086#1076#1091#1082#1094#1080#1112#1072
      DataBinding.DataField = 'GOVOR_PRODUKCIJA'
      DataBinding.DataSource = dmSis.dsLVKStranskiJazik
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1051#1086#1096#1086
          Value = 1
        end
        item
          Caption = #1044#1086#1073#1088#1086
          Value = 2
        end
        item
          Caption = #1054#1076#1083#1080#1095#1085#1086
          Value = 3
        end>
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 9
      Height = 40
      Width = 210
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 630
    ExplicitTop = 630
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40280.656857256940000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
