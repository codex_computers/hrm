inherited frmKompetencija: TfrmKompetencija
  Caption = #1055#1088#1072#1096'a'#1083#1085#1080#1082
  ClientWidth = 629
  ExplicitWidth = 645
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 629
    Height = 266
    ExplicitWidth = 629
    ExplicitHeight = 266
    inherited cxGrid1: TcxGrid
      Width = 625
      Height = 262
      ExplicitWidth = 625
      ExplicitHeight = 262
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmUcinok.dsPrasanja
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANAZIV'
          Width = 164
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 395
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Visible = False
        end
        object cxGrid1DBTableView1NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'NORMATIV'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 392
    Width = 629
    Height = 138
    ExplicitTop = 392
    ExplicitWidth = 629
    ExplicitHeight = 138
    inherited Label1: TLabel
      Top = 6
      Visible = False
      ExplicitTop = 6
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 33
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel [2]
      Left = -18
      Top = 60
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1043#1088#1091#1087#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Top = 3
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmUcinok.dsPrasanja
      TabOrder = 1
      Visible = False
      ExplicitTop = 3
    end
    inherited OtkaziButton: TcxButton
      Left = 538
      Top = 98
      TabOrder = 5
      ExplicitLeft = 538
      ExplicitTop = 98
    end
    inherited ZapisiButton: TcxButton
      Left = 457
      Top = 98
      TabOrder = 4
      ExplicitLeft = 457
      ExplicitTop = 98
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 30
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmUcinok.dsPrasanja
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 380
    end
    object GRUPA: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 57
      BeepOnEnter = False
      DataBinding.DataField = 'GRUPA'
      DataBinding.DataSource = dmUcinok.dsPrasanja
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 52
    end
    object GRUPANaziv: TcxDBLookupComboBox
      Tag = 1
      Left = 121
      Top = 57
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'GRUPA'
      DataBinding.DataSource = dmUcinok.dsPrasanja
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dmUcinok.dsGrupaPrasanja
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 328
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 629
    ExplicitWidth = 629
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 629
    ExplicitTop = 32000
    ExplicitWidth = 629
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40375.653567476850000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
