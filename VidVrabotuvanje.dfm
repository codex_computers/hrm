inherited frmVidVrabotuvanje: TfrmVidVrabotuvanje
  Caption = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
  ClientHeight = 483
  ClientWidth = 622
  ExplicitWidth = 630
  ExplicitHeight = 514
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 622
    Height = 228
    ExplicitWidth = 622
    ExplicitHeight = 228
    inherited cxGrid1: TcxGrid
      Width = 618
      Height = 224
      ExplicitWidth = 618
      ExplicitHeight = 224
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsVidVrabotuvanje
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          SortIndex = 0
          SortOrder = soAscending
          Width = 79
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 387
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 354
    Width = 622
    Height = 106
    ExplicitTop = 354
    ExplicitWidth = 622
    ExplicitHeight = 106
    inherited Label1: TLabel
      Visible = False
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 41
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsVidVrabotuvanje
      TabOrder = 1
      Visible = False
      OnKeyDown = nil
    end
    inherited OtkaziButton: TcxButton
      Left = 531
      Top = 66
      TabOrder = 3
      ExplicitLeft = 531
      ExplicitTop = 66
    end
    inherited ZapisiButton: TcxButton
      Left = 450
      Top = 66
      TabOrder = 2
      ExplicitLeft = 450
      ExplicitTop = 66
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 38
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsVidVrabotuvanje
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 372
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 622
    ExplicitWidth = 622
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 460
    Width = 622
    ExplicitTop = 460
    ExplicitWidth = 622
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1088#1072#1114#1077
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 80
    Top = 184
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40248.411718483800000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
