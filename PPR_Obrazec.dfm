object frmPPR_Obrazec: TfrmPPR_Obrazec
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1088#1080#1112#1072#1074#1072' '#1079#1072' '#1087#1086#1090#1088#1077#1073#1072' '#1086#1076' '#1088#1072#1073#1086#1090#1085#1080#1082
  ClientHeight = 692
  ClientWidth = 757
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 757
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 669
    Width = 757
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 757
    Height = 543
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 543
    ClientRectRight = 757
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 0
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 757
        Height = 519
        Align = alClient
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 755
          Height = 517
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            DataController.DataSource = dm.dsPPRObrazec
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 95
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 86
            end
            object cxGrid1DBTableView1UBROJ: TcxGridDBColumn
              DataBinding.FieldName = 'UBROJ'
              Width = 68
            end
            object cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'ARHIVSKI_BROJ'
              Width = 82
            end
            object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
              DataBinding.FieldName = 'ID_RM_RE'
              Visible = False
            end
            object cxGrid1DBTableView1PRIPRAVNIK: TcxGridDBColumn
              DataBinding.FieldName = 'PRIPRAVNIK'
              Visible = False
              Width = 83
            end
            object cxGrid1DBTableView1REGISTARSKI_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'REGISTARSKI_BROJ'
              Width = 96
            end
            object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNOMESTONAZIV'
              Width = 135
            end
            object cxGrid1DBTableView1Column4: TcxGridDBColumn
              DataBinding.FieldName = 'KakvoRMNaziv'
              Width = 103
            end
            object cxGrid1DBTableView1NACINVRABOTUVANJENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'NACINVRABOTUVANJENAZIV'
              Width = 146
            end
            object cxGrid1DBTableView1BROJ_RABOTNICI: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_RABOTNICI'
              Width = 88
            end
            object cxGrid1DBTableView1KAKVO_RM: TcxGridDBColumn
              DataBinding.FieldName = 'KAKVO_RM'
              Visible = False
            end
            object cxGrid1DBTableView1ID_NACIN_VRABOTUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'ID_NACIN_VRABOTUVANJE'
              Visible = False
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 150
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 20
            end
            object cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNAEDINICANAZIV'
              Visible = False
              Width = 200
            end
            object cxGrid1DBTableView1SISTEMATIZACIJANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'SISTEMATIZACIJANAZIV'
              Visible = False
              Width = 300
            end
            object cxGrid1DBTableView1RABOTNO_ISKUSTVO: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNO_ISKUSTVO'
              Visible = False
            end
            object cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn
              DataBinding.FieldName = 'OBRAZOVANIE'
              Visible = False
            end
            object cxGrid1DBTableView1OBRAZOVANIE_OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OBRAZOVANIE_OPIS'
              Visible = False
              Width = 200
            end
            object cxGrid1DBTableView1Column3: TcxGridDBColumn
              DataBinding.FieldName = 'PripravnikNaziv'
              Width = 71
            end
            object cxGrid1DBTableView1ID_SZ_OBRAZOVANIE: TcxGridDBColumn
              DataBinding.FieldName = 'ID_SZ_OBRAZOVANIE'
              Width = 109
            end
            object cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'VID_VRABOTUVANJE'
              Visible = False
            end
            object cxGrid1DBTableView1RABOTNO_VREME: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNO_VREME'
              Visible = False
            end
            object cxGrid1DBTableView1ZVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'ZVANJE'
              Visible = False
              Width = 200
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Visible = False
              Width = 300
            end
            object cxGrid1DBTableView1USLOVI: TcxGridDBColumn
              DataBinding.FieldName = 'USLOVI'
              Visible = False
              Width = 300
            end
            object cxGrid1DBTableView1VIDVRABOTUVANJENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'VIDVRABOTUVANJENAZIV'
              Visible = False
              Width = 300
            end
            object cxGrid1DBTableView1STEPENSTRUCNAPODGOTOVKANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'STEPENSTRUCNAPODGOTOVKANAZIV'
              Visible = False
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              DataBinding.FieldName = 'VZ_OBRAZOVANIE'
              SortIndex = 0
              SortOrder = soAscending
              Width = 162
            end
            object cxGrid1DBTableView1Column2: TcxGridDBColumn
              DataBinding.FieldName = 'PROFIL'
              Width = 137
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 1
      object dPanel: TPanel
        Left = 0
        Top = 0
        Width = 757
        Height = 519
        Align = alClient
        Enabled = False
        TabOrder = 0
        DesignSize = (
          757
          519)
        object cxGroupBox2: TcxGroupBox
          Left = 16
          Top = 135
          Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
          TabOrder = 1
          Height = 90
          Width = 529
          object Label4: TLabel
            Left = 8
            Top = 57
            Width = 166
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1085#1080' '#1088#1072#1073#1086#1090#1085#1080#1094#1080' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 75
            Top = 30
            Width = 99
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1074#1088#1072#1073'. :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BROJ_RABOTNICI: TcxDBTextEdit
            Left = 180
            Top = 54
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1085#1080' '#1088#1072#1073#1086#1090#1085#1080#1094#1080
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ_RABOTNICI'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 108
          end
          object ID_NACIN_VRABOTUVANJE: TcxDBTextEdit
            Left = 180
            Top = 27
            Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
            BeepOnEnter = False
            DataBinding.DataField = 'ID_NACIN_VRABOTUVANJE'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 58
          end
          object NACIN_VRABOTUVANJE: TcxDBLookupComboBox
            Left = 239
            Top = 27
            Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
            DataBinding.DataField = 'ID_NACIN_VRABOTUVANJE'
            DataBinding.DataSource = dm.dsPPRObrazec
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsNacinVrabotuvanje
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 277
          end
        end
        object cxGroupBox1: TcxGroupBox
          Left = 16
          Top = 11
          Caption = #1055#1088#1080#1112#1072#1074#1072' '#1079#1072' '#1087#1086#1090#1088#1077#1073#1072' '#1086#1076' '#1088#1072#1073#1086#1090#1085#1080#1082
          TabOrder = 0
          Height = 116
          Width = 529
          object Label1: TLabel
            Left = 5
            Top = 57
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 5
            Top = 84
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1059#1041#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 262
            Top = 58
            Width = 95
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label6: TLabel
            Left = 249
            Top = 85
            Width = 108
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1056#1077#1075#1080#1089#1090#1077#1088#1089#1082#1080' '#1073#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 5
            Top = 30
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1072#1090#1091#1084' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object BROJ: TcxDBTextEdit
            Left = 61
            Top = 54
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1055#1055#1056
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 153
          end
          object UBROJ: TcxDBTextEdit
            Left = 61
            Top = 81
            Hint = #1059' '#1073#1088#1086#1112' '#1085#1072' '#1055#1055#1056
            BeepOnEnter = False
            DataBinding.DataField = 'UBROJ'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 153
          end
          object ARHIVSKI_BROJ: TcxDBTextEdit
            Left = 363
            Top = 54
            Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112' '#1085#1072' '#1055#1055#1056
            BeepOnEnter = False
            DataBinding.DataField = 'ARHIVSKI_BROJ'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 153
          end
          object REGISTARSKI_BROJ: TcxDBTextEdit
            Left = 363
            Top = 81
            Hint = #1056#1077#1075#1080#1089#1090#1077#1088#1089#1082#1080' '#1073#1088#1086#1112' '#1085#1072' '#1055#1055#1056' '#1086#1076' '#1040#1042#1056#1052
            BeepOnEnter = False
            DataBinding.DataField = 'REGISTARSKI_BROJ'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 153
          end
          object DATUM: TcxDBDateEdit
            Left = 61
            Top = 27
            Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1077#1084' '#1085#1072' '#1087#1088#1080#1112#1072#1074#1072#1090#1072
            DataBinding.DataField = 'DATUM'
            DataBinding.DataSource = dm.dsPPRObrazec
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 153
          end
        end
        object cxGroupBox3: TcxGroupBox
          Left = 16
          Top = 381
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
          TabOrder = 3
          Height = 117
          Width = 529
          object Label10: TLabel
            Left = 24
            Top = 30
            Width = 45
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1053#1072#1079#1080#1074' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object ID_RM_RE: TcxDBTextEdit
            Tag = 1
            Left = 75
            Top = 27
            BeepOnEnter = False
            DataBinding.DataField = 'ID_RM_RE'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 55
          end
          object ID_RM_RE_NAZIV: TcxDBLookupComboBox
            Tag = 1
            Left = 130
            Top = 27
            BeepOnEnter = False
            DataBinding.DataField = 'ID_RM_RE'
            DataBinding.DataSource = dm.dsPPRObrazec
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Width = 200
                FieldName = 'ID'
              end
              item
                Width = 600
                FieldName = 'NAZIV_RE'
              end
              item
                Width = 600
                FieldName = 'NAZIV_RM'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListSource = dsRMRE
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 321
          end
          object cxDBRadioGroup1: TcxDBRadioGroup
            Left = 75
            Top = 54
            Caption = #1056#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086' '#1077' '
            DataBinding.DataField = 'KAKVO_RM'
            DataBinding.DataSource = dm.dsPPRObrazec
            Properties.Columns = 2
            Properties.Items = <
              item
                Caption = #1053#1086#1074#1086#1086#1090#1074#1086#1088#1077#1085#1086
                Value = 1
              end
              item
                Caption = #1059#1087#1088#1072#1079#1085#1077#1090#1086
                Value = 0
              end>
            TabOrder = 2
            Height = 48
            Width = 198
          end
        end
        object ZapisiButton: TcxButton
          Left = 574
          Top = 469
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 4
        end
        object OtkaziButton: TcxButton
          Left = 655
          Top = 469
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 5
        end
        object cxGroupBox4: TcxGroupBox
          Left = 16
          Top = 231
          Caption = #1059#1089#1083#1086#1074#1080' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
          TabOrder = 2
          Height = 144
          Width = 529
          object Label8: TLabel
            Left = 26
            Top = 30
            Width = 190
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1042#1080#1076' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = 2
            Top = 57
            Width = 214
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1088#1086#1092#1080#1083' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object VZ_OBRAZOVANIE: TcxDBTextEdit
            Left = 222
            Top = 27
            Hint = #1042#1080#1076' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
            BeepOnEnter = False
            DataBinding.DataField = 'VZ_OBRAZOVANIE'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 294
          end
          object PROFIL: TcxDBTextEdit
            Left = 222
            Top = 54
            Hint = #1055#1088#1086#1092#1080#1083
            BeepOnEnter = False
            DataBinding.DataField = 'PROFIL'
            DataBinding.DataSource = dm.dsPPRObrazec
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 294
          end
          object cxDBRadioGroup2: TcxDBRadioGroup
            Left = 222
            Top = 81
            Caption = #1055#1088#1080#1087#1088#1072#1074#1085#1080#1082
            DataBinding.DataField = 'PRIPRAVNIK'
            DataBinding.DataSource = dm.dsPPRObrazec
            Properties.Columns = 2
            Properties.Items = <
              item
                Caption = #1044#1072
                Value = 1
              end
              item
                Caption = #1053#1077
                Value = 0
              end>
            TabOrder = 2
            Height = 48
            Width = 122
          end
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      ImageIndex = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 757
        Height = 519
        Align = alClient
        Enabled = False
        TabOrder = 0
        DesignSize = (
          757
          519)
        object Label11: TLabel
          Left = 160
          Top = 62
          Width = 53
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1077#1082#1090#1086#1088' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 104
          Top = 89
          Width = 109
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label13: TLabel
          Left = 104
          Top = 35
          Width = 109
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label14: TLabel
          Left = 96
          Top = 362
          Width = 117
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label15: TLabel
          Left = 72
          Top = 271
          Width = 141
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1087#1080#1089' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label16: TLabel
          Left = 16
          Top = 335
          Width = 197
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label17: TLabel
          Left = 72
          Top = 426
          Width = 141
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label18: TLabel
          Left = 72
          Top = 453
          Width = 141
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label19: TLabel
          Left = 72
          Top = 116
          Width = 141
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1047#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 56
          Top = 143
          Width = 157
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1087#1080#1089' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1079#1072#1076#1072#1095#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label21: TLabel
          Left = 72
          Top = 207
          Width = 141
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1059#1089#1083#1086#1074#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RABOTNAEDINICANAZIV: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 59
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'RABOTNAEDINICANAZIV'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object RABOTNOMESTONAZIV: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 86
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'RABOTNOMESTONAZIV'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object SISTEMATIZACIJANAZIV: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 32
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'SISTEMATIZACIJANAZIV'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object RABOTNO_ISKUSTVO: TcxDBMemo
          Left = 219
          Top = 359
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'RABOTNO_ISKUSTVO'
          DataBinding.DataSource = dm.dsPPRObrazec
          TabOrder = 3
          Height = 58
          Width = 494
        end
        object OBRAZOVANIE_OPIS: TcxDBMemo
          Left = 219
          Top = 268
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'OBRAZOVANIE_OPIS'
          DataBinding.DataSource = dm.dsPPRObrazec
          TabOrder = 4
          Height = 58
          Width = 494
        end
        object STEPENSTRUCNAPODGOTOVKANAZIV: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 332
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'STEPENSTRUCNAPODGOTOVKANAZIV'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object VIDVRABOTUVANJENAZIV: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 423
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'VIDVRABOTUVANJENAZIV'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object RABOTNO_VREME: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 450
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'RABOTNO_VREME'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 7
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object ZVANJE: TcxDBTextEdit
          Tag = 1
          Left = 219
          Top = 113
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'ZVANJE'
          DataBinding.DataSource = dm.dsPPRObrazec
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 8
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 494
        end
        object OPIS: TcxDBMemo
          Left = 219
          Top = 140
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'OPIS'
          DataBinding.DataSource = dm.dsPPRObrazec
          TabOrder = 9
          Height = 58
          Width = 494
        end
        object USLOVI: TcxDBMemo
          Left = 219
          Top = 204
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'USLOVI'
          DataBinding.DataSource = dm.dsPPRObrazec
          TabOrder = 10
          Height = 58
          Width = 494
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 488
    Top = 24
  end
  object PopupMenu1: TPopupMenu
    Left = 520
    Top = 80
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 576
    Top = 80
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1055#1056' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 382
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 432
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 632
    Top = 8
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40906.545473993060000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 648
    Top = 72
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblRMRE: TpFIBDataSet
    RefreshSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where(  hs.do_datum is null'
      '     ) and (     HRR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where hs.do_datum is null and hs.id_re_firma = :firma')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 632
    Top = 219
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 779
    Top = 179
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
