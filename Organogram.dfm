object frmOrganogram: TfrmOrganogram
  Left = 0
  Top = 0
  Caption = #1054#1088#1075#1072#1085#1086#1075#1088#1072#1084
  ClientHeight = 844
  ClientWidth = 1284
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1284
    Height = 695
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 1123
    object pcRE: TcxPageControl
      Left = 0
      Top = 0
      Width = 1284
      Height = 695
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Properties.ActivePage = tcRabEd
      ExplicitWidth = 1123
      ClientRectBottom = 695
      ClientRectRight = 1284
      ClientRectTop = 24
      object tcRabEd: TcxTabSheet
        Caption = #1056#1072#1073#1086#1090#1085#1072' '#1045#1076#1080#1085#1080#1094#1072
        ImageIndex = 0
        ExplicitTop = 0
        ExplicitWidth = 1123
        ExplicitHeight = 0
        object lPanel: TPanel
          Left = 0
          Top = 0
          Width = 736
          Height = 671
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 601
          object dxTree: TdxDBTreeView
            Left = 1
            Top = 1
            Width = 734
            Height = 669
            ShowNodeHint = True
            HotTrack = True
            RowSelect = True
            DataSource = dm.dsRe
            DisplayField = 'ID;NAZIV'
            KeyField = 'ID'
            ListField = 'NAZIV'
            ParentField = 'KOREN'
            RootValue = Null
            SeparatedSt = ' -> '
            RaiseOnError = True
            ReadOnly = True
            DragMode = dmAutomatic
            Indent = 19
            Align = alClient
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentCtl3D = False
            Ctl3D = True
            Options = [trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
            SelectedIndex = -1
            TabOrder = 0
            ParentFont = False
            ExplicitWidth = 560
          end
        end
        object dPanel: TPanel
          Left = 736
          Top = 0
          Width = 548
          Height = 671
          Align = alRight
          Color = 15790320
          Enabled = False
          ParentBackground = False
          TabOrder = 1
          DesignSize = (
            548
            671)
          object Label1: TLabel
            Left = 22
            Top = 15
            Width = 80
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1064#1080#1092#1088#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label15: TLabel
            Left = 22
            Top = 42
            Width = 80
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1053#1072#1079#1080#1074' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 22
            Top = 69
            Width = 80
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1050#1086#1088#1077#1085' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label2: TLabel
            Left = 7
            Top = 96
            Width = 95
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 202
            Top = 15
            Width = 80
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1086#1090#1077#1082#1083#1086' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object Label4: TLabel
            Left = 205
            Top = 69
            Width = 80
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1057#1087#1080#1089#1086#1082' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object Label14: TLabel
            Left = 36
            Top = 154
            Width = 66
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1072#1088#1090#1085#1077#1088' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Sifra: TcxDBTextEdit
            Tag = 1
            Left = 108
            Top = 12
            BeepOnEnter = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            TabOrder = 0
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object Naziv: TcxDBTextEdit
            Tag = 1
            Left = 108
            Top = 39
            BeepOnEnter = False
            DataBinding.DataField = 'NAZIV'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 1
            OnKeyDown = EnterKakoTab
            Width = 397
          end
          object Koren: TcxDBTextEdit
            Left = 108
            Top = 66
            BeepOnEnter = False
            DataBinding.DataField = 'KOREN'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            TabOrder = 2
            OnKeyDown = EnterKakoTab
            Width = 100
          end
          object Rakovoditel: TcxDBTextEdit
            Left = 108
            Top = 93
            BeepOnEnter = False
            DataBinding.DataField = 'RAKOVODITEL'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 3
            OnKeyDown = EnterKakoTab
            Width = 397
          end
          object Poteklo: TcxDBTextEdit
            Left = 288
            Top = 12
            BeepOnEnter = False
            DataBinding.DataField = 'POTEKLO'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 4
            Visible = False
            OnKeyDown = EnterKakoTab
            Width = 217
          end
          object Spisok: TcxDBTextEdit
            Left = 288
            Top = 66
            BeepOnEnter = False
            DataBinding.DataField = 'SPISOK'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 5
            Visible = False
            OnKeyDown = EnterKakoTab
            Width = 217
          end
          object TipPartner: TcxDBTextEdit
            Left = 108
            Top = 151
            BeepOnEnter = False
            DataBinding.DataField = 'TIP_PARTNER'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.CharCase = ecUpperCase
            TabOrder = 11
            OnKeyDown = EnterKakoTab
            Width = 53
          end
          object Partner: TcxDBTextEdit
            Left = 167
            Top = 151
            BeepOnEnter = False
            DataBinding.DataField = 'PARTNER'
            DataBinding.DataSource = dmMat.dsRE
            ParentFont = False
            Properties.CharCase = ecUpperCase
            Properties.OnEditValueChanged = PARTNERPropertiesEditValueChanged
            TabOrder = 12
            OnKeyDown = EnterKakoTab
            Width = 50
          end
          object cbRE: TcxDBCheckBox
            Left = 108
            Top = 120
            Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            DataBinding.DataField = 'R'
            DataBinding.DataSource = dmMat.dsRE
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            Properties.Alignment = taLeftJustify
            Properties.DisplayChecked = 'true'
            Properties.DisplayUnchecked = 'false'
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            TabOrder = 6
            Transparent = True
            OnKeyDown = EnterKakoTab
            Width = 111
          end
          object cbMagacin: TcxDBCheckBox
            Left = 343
            Top = 120
            Caption = #1052#1072#1075#1072#1094#1080#1085
            DataBinding.DataField = 'M'
            DataBinding.DataSource = dmMat.dsRE
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            Properties.Alignment = taLeftJustify
            Properties.DisplayChecked = 'true'
            Properties.DisplayUnchecked = 'false'
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            TabOrder = 8
            Transparent = True
            OnKeyDown = EnterKakoTab
            Width = 73
          end
          object cbTroskovnoMesto: TcxDBCheckBox
            Left = 225
            Top = 120
            Caption = #1058#1088#1086#1096#1082#1086#1074#1085#1086' '#1084#1077#1089#1090#1086
            DataBinding.DataField = 'T'
            DataBinding.DataSource = dmMat.dsRE
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            Properties.Alignment = taLeftJustify
            Properties.DisplayChecked = 'true'
            Properties.DisplayUnchecked = 'false'
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            TabOrder = 7
            Transparent = True
            OnKeyDown = EnterKakoTab
            Width = 112
          end
          object ZapisiButton: TcxButton
            Left = 347
            Top = 191
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 9
          end
          object OtkaziButton: TcxButton
            Left = 428
            Top = 191
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 10
          end
          object cBoxExtPartner: TcxExtLookupComboBox
            Left = 223
            Top = 151
            Properties.DropDownSizeable = True
            Properties.View = cxGridViewRepository1DBTableView1
            Properties.KeyFieldNames = 'TIP_PARTNER;ID'
            Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
            TabOrder = 13
            OnKeyDown = EnterKakoTab
            Width = 282
          end
        end
      end
      object tcOrganogram: TcxTabSheet
        Caption = #1054#1088#1075#1072#1085#1086#1075#1088#1072#1084
        Enabled = False
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 1123
        ExplicitHeight = 0
        object orgRe: TdxDbOrgChart
          Left = 0
          Top = 0
          Width = 1284
          Height = 671
          DataSource = dm.dsRe
          KeyFieldName = 'ID'
          ParentFieldName = 'KOREN'
          TextFieldName = 'NAZIV'
          OrderFieldName = 'ID'
          DefaultNodeWidth = 100
          DefaultNodeHeight = 64
          Options = [ocSelect, ocFocus, ocButtons, ocDblClick, ocEdit, ocCanDrag, ocShowDrag, ocInsDel, ocRect3D, ocAnimate]
          EditMode = [emCenter, emVCenter, emWrap, emGrow]
          Images = dmRes.cxLargeImages
          DefaultImageAlign = iaLT
          BorderStyle = bsNone
          OnDeletion = orgREDeletion
          Align = alClient
          Color = clWhite
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 1123
        end
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1284
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    OnTabChanging = dxRibbon1TabChanging
    object rtMeni: TdxRibbonTab
      Active = True
      Caption = #1052#1045#1053#1048
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object rtOrganogram: TdxRibbonTab
      Caption = #1054#1088#1075#1072#1085#1086#1075#1088#1072#1084
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 821
    Width = 1284
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitWidth = 1123
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = dm.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 608
    Top = 136
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1086#1075#1083#1077#1076
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 848
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1149
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 248
      DockedTop = 0
      FloatLeft = 1149
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aVeritikalno
      Category = 0
      LargeImageIndex = 35
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aHorizontalno
      Category = 0
      LargeImageIndex = 42
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aDodadiRabEdinica
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aAzurirajRabEdinica
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aBrisiRabEdinica
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 536
    Top = 136
    object aVeritikalno: TAction
      Caption = #1042#1077#1088#1080#1090#1080#1082#1072#1083#1077#1085
      OnExecute = aVeritikalnoExecute
    end
    object aHorizontalno: TAction
      Caption = #1061#1086#1088#1080#1079#1086#1085#1090#1072#1083#1077#1085
      OnExecute = aHorizontalnoExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aDodadiRabEdinica: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aDodadiRabEdinicaExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziExecute
    end
    object aBrisiRabEdinica: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiRabEdinicaExecute
    end
    object aAzurirajRabEdinica: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajRabEdinicaExecute
    end
    object aPomos: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aPomosExecute
    end
    object aOsvezi: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aOsveziExecute
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 896
    Top = 120
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dmMat.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088' (ID)'
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 45
      end
      object cxGridViewRepository1DBTableView1TIP_NAZIV: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
        DataBinding.FieldName = 'TIP_NAZIV'
        Width = 123
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 67
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 222
      end
    end
  end
end
