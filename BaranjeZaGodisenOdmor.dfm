object frmBaranjeZaGodisenOdmor: TfrmBaranjeZaGodisenOdmor
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
  ClientHeight = 764
  ClientWidth = 962
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 962
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    OnTabChanged = dxRibbon1TabChanged
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 741
    Width = 962
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 962
    Height = 266
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 958
      Height = 262
      Align = alClient
      PopupMenu = PopupMenu1
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsBaranjeGO
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 83
        end
        object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 153
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 84
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
        end
        object cxGrid1DBTableView1OdobrenieNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'OdobrenieNaziv'
          Width = 60
        end
        object cxGrid1DBTableView1KoristenjeNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'KoristenjeNaziv'
          Width = 111
        end
        object cxGrid1DBTableView1PRV_DEL: TcxGridDBColumn
          DataBinding.FieldName = 'PRV_DEL'
          Width = 139
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM1_OD'
          Width = 109
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM1_DO'
          Width = 112
        end
        object cxGrid1DBTableView1VTOR_DEL: TcxGridDBColumn
          DataBinding.FieldName = 'VTOR_DEL'
          Width = 166
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM2_OD'
          Width = 120
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM2_DO'
          Width = 127
        end
        object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RENAZIV'
          Width = 178
        end
        object cxGrid1DBTableView1RMNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RMNAZIV'
          Width = 85
        end
        object cxGrid1DBTableView1MB_RAKOVODITEL: TcxGridDBColumn
          Caption = #1054#1076#1086#1073#1088#1077#1085#1086' '#1086#1076
          DataBinding.FieldName = 'MB_RAKOVODITEL'
          Width = 127
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1VID_DOCUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOCUMENT'
          Visible = False
        end
        object cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDDOKUMENTNAZIV'
          Visible = False
        end
        object cxGrid1DBTableView1ODOBRENIE: TcxGridDBColumn
          DataBinding.FieldName = 'ODOBRENIE'
          Visible = False
        end
        object cxGrid1DBTableView1KORISTENJE: TcxGridDBColumn
          DataBinding.FieldName = 'KORISTENJE'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1076#1086#1076#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
          Visible = False
          Width = 250
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 392
    Width = 962
    Height = 349
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Enabled = False
    TabOrder = 3
    DesignSize = (
      962
      349)
    object Label8: TLabel
      Left = 16
      Top = 12
      Width = 102
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel
      Left = 52
      Top = 47
      Width = 66
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 29
      Top = 122
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1076#1086#1073#1088#1077#1085#1086' '#1086#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object MB: TcxDBTextEdit
      Tag = 1
      Left = 124
      Top = 17
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsBaranjeGO
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Properties.OnEditValueChanged = MBPropertiesEditValueChanged
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object VRABOTENIME: TcxDBLookupComboBox
      Tag = 1
      Left = 248
      Top = 17
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsBaranjeGO
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = VRABOTENIMEExit
      OnKeyDown = EnterKakoTab
      Width = 273
    end
    object DatumKreiranje: TcxDBDateEdit
      Left = 124
      Top = 44
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1073#1072#1088#1072#1114#1077
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsBaranjeGO
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object cxGroupBox1: TcxGroupBox
      Left = 36
      Top = 158
      Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      TabOrder = 5
      Height = 173
      Width = 744
      object Label1: TLabel
        Left = -7
        Top = 39
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072' '#1075#1086#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object godina: TcxDBTextEdit
        Left = 88
        Top = 36
        Hint = #1043#1086#1076#1080#1085#1072' '#1079#1072' '#1082#1086#1112#1072' '#1074#1072#1078#1080' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088
        BeepOnEnter = False
        DataBinding.DataField = 'GODINA'
        DataBinding.DataSource = dm.dsBaranjeGO
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 89
      end
      object cxGroupBox3: TcxGroupBox
        Left = 375
        Top = 71
        Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1074#1090#1086#1088#1080#1086#1090' '#1076#1077#1083' '#1086#1076' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088
        Caption = #1042#1090#1086#1088' '#1076#1077#1083
        Enabled = False
        TabOrder = 3
        Height = 86
        Width = 354
        object Label5: TLabel
          Left = -9
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 16
          Top = 54
          Width = 96
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 160
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Datum2Od: TcxDBDateEdit
          Left = 47
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM2_OD'
          DataBinding.DataSource = dm.dsBaranjeGO
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = Datum2OdExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object VtorDel: TcxDBTextEdit
          Left = 118
          Top = 51
          BeepOnEnter = False
          DataBinding.DataField = 'VTOR_DEL'
          DataBinding.DataSource = dm.dsBaranjeGO
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = VtorDelExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object Datum2Do: TcxDBDateEdit
          Left = 216
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM2_DO'
          DataBinding.DataSource = dm.dsBaranjeGO
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = Datum2DoExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 15
        Top = 71
        Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1087#1088#1074#1080#1086#1090' '#1076#1077#1083' '#1086#1076' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088
        Caption = #1055#1088#1074' '#1076#1077#1083
        TabOrder = 2
        Height = 86
        Width = 354
        object Label2: TLabel
          Left = -9
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 16
          Top = 54
          Width = 96
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 160
          Top = 27
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DatumOd: TcxDBDateEdit
          Left = 47
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM1_OD'
          DataBinding.DataSource = dm.dsBaranjeGO
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 0
          OnEnter = cxDBTextEditAllExit
          OnExit = DatumOdExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object PrvDel: TcxDBTextEdit
          Left = 118
          Top = 51
          BeepOnEnter = False
          DataBinding.DataField = 'PRV_DEL'
          DataBinding.DataSource = dm.dsBaranjeGO
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = PrvDelExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object DatumDo: TcxDBDateEdit
          Left = 216
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM1_DO'
          DataBinding.DataSource = dm.dsBaranjeGO
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = DatumDoExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
      end
      object cxDBRadioGroup2: TcxDBRadioGroup
        Left = 184
        Top = 25
        Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1080#1114#1077' '#1085#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088'. '#1045#1076#1085#1086#1082#1088#1072#1090#1085#1086'/'#1044#1074#1086#1082#1088#1072#1090#1085#1086
        TabStop = False
        Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
        DataBinding.DataField = 'KORISTENJE'
        DataBinding.DataSource = dm.dsBaranjeGO
        Properties.Columns = 2
        Properties.ImmediatePost = True
        Properties.Items = <
          item
            Caption = #1045#1076#1085#1086#1082#1088#1072#1090#1085#1086
            Value = 1
          end
          item
            Caption = #1044#1074#1086#1082#1088#1072#1090#1085#1086
            Value = 2
          end>
        Properties.OnEditValueChanged = cxDBRadioGroup2PropertiesEditValueChanged
        TabOrder = 1
        Height = 40
        Width = 185
      end
    end
    object OtkaziButton: TcxButton
      Left = 867
      Top = 297
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 7
    end
    object ZapisiButton: TcxButton
      Left = 786
      Top = 297
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 6
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 124
      Top = 71
      Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1073#1072#1088#1072#1114#1077'. '#1054#1076#1086#1073#1088#1077#1085#1086'/'#1053#1077#1086#1076#1086#1073#1088#1077#1085#1086
      TabStop = False
      Caption = #1054#1076#1086#1073#1088#1077#1085#1086' '#1073#1072#1088#1072#1114#1077
      DataBinding.DataField = 'ODOBRENIE'
      DataBinding.DataSource = dm.dsBaranjeGO
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 3
      Height = 42
      Width = 115
    end
    object Odobreno_od: TcxDBTextEdit
      Left = 124
      Top = 119
      Hint = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' '#1085#1072' '#1083#1080#1094#1077#1090#1086' '#1082#1086#1077' '#1075#1086' '#1086#1076#1086#1073#1088#1080#1083#1086' '#1073#1072#1088#1072#1114#1077#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'MB_RAKOVODITEL'
      DataBinding.DataSource = dm.dsBaranjeGO
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 397
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 216
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dm.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 440
    Top = 240
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 113
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 575
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 782
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      CaptionButtons = <>
      DockedLeft = 462
      DockedTop = 0
      FloatLeft = 981
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 288
      DockedTop = 0
      FloatLeft = 981
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 996
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 56
          Visible = True
          ItemName = 'ComboBoxGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Caption = #1055#1086#1084#1086#1096
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 24
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aReseniGO
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPecatiDokument
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object cxBarEditItem4: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
    end
    object ComboBoxGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = ComboBoxGodinaChange
      Items.Strings = (
        #1057#1080#1090#1077
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030')
      ItemIndex = -1
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 184
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aReseniGO: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      ImageIndex = 139
      OnExecute = aReseniGOExecute
    end
    object aPecatiDokument: TAction
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      ImageIndex = 19
      OnExecute = aPecatiDokumentExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080' '
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44462.347376377310000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 696
    Top = 96
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 640
    Top = 208
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 152
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
