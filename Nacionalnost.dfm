inherited frmNacionalnost: TfrmNacionalnost
  Caption = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090
  ClientWidth = 602
  ExplicitWidth = 618
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 298
    ExplicitWidth = 602
    ExplicitHeight = 298
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 294
      ExplicitWidth = 598
      ExplicitHeight = 294
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dmOtsustvo.dsNacionalnost
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 98
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 498
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 424
    Width = 602
    Height = 106
    ExplicitTop = 424
    ExplicitWidth = 602
    ExplicitHeight = 106
    inherited Label1: TLabel
      Top = 7
      Visible = False
      ExplicitTop = 7
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 34
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsNacionalnost
      TabOrder = 1
      Visible = False
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 66
      TabOrder = 3
      ExplicitLeft = 511
      ExplicitTop = 66
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 66
      TabOrder = 2
      ExplicitLeft = 430
      ExplicitTop = 66
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 31
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmOtsustvo.dsNacionalnost
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 348
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 602
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 602
    ExplicitTop = 32000
    ExplicitWidth = 602
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1053#1072#1094#1080#1086#1085#1072#1083#1085#1086#1089#1090
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40248.489355856480000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
