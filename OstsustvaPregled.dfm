object frmPregedOtsustva: TfrmPregedOtsustva
  Left = 0
  Top = 0
  Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1072
  ClientHeight = 700
  ClientWidth = 1204
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1204
    Height = 50
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      1204
      50)
    object Label4: TLabel
      Left = 28
      Top = 20
      Width = 109
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Re: TcxLookupComboBox
      Left = 143
      Top = 17
      Anchors = [akLeft, akTop, akRight, akBottom]
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsPodsektori
      Properties.OnEditValueChanged = RePropertiesEditValueChanged
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = ReExit
      OnKeyDown = EnterKakoTab
      Width = 961
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 176
    Width = 1204
    Height = 501
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 1200
      Height = 497
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dmOtsustvo.dsOtsustvaPregled
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.IncSearch = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        Styles.StyleSheet = dmRes.TableViewDefaultStyle
        object cxGrid1DBTableView1MB_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'MB_OUT'
          Width = 91
        end
        object cxGrid1DBTableView1NAZIVVRABOTEN_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIVVRABOTEN_OUT'
          Width = 122
        end
        object cxGrid1DBTableView1COUNT_0_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'COUNT_0_OUT'
          Width = 132
        end
        object cxGrid1DBTableView1COUNT_1_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'COUNT_1_OUT'
          Width = 72
        end
        object cxGrid1DBTableView1PLATENO_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'PLATENO_OUT'
          Width = 116
        end
        object cxGrid1DBTableView1NEPLATENO_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'NEPLATENO_OUT'
          Width = 125
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 176
    Width = 1204
    Height = 501
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxPageControl1: TcxPageControl
      Left = 2
      Top = 2
      Width = 1200
      Height = 497
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 497
      ClientRectRight = 1200
      ClientRectTop = 24
      object cxTabSheet1: TcxTabSheet
        Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1087#1083#1072#1090#1077#1085#1080'/'#1085#1077#1087#1083#1072#1090#1077#1085#1080', '#1087#1083#1072#1085#1080#1088#1072#1085#1080'/'#1088#1077#1072#1083#1080#1079#1080#1088#1072#1085#1080' '#1086#1090#1089#1091#1089#1090#1074#1072
        ImageIndex = 0
        object Panel3: TPanel
          Left = 0
          Top = 73
          Width = 1200
          Height = 400
          Align = alClient
          TabOrder = 0
          object cxGrid2: TcxGrid
            Left = 1
            Top = 1
            Width = 1198
            Height = 398
            Align = alClient
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object cxGridDBTableView1: TcxGridDBTableView
              OnKeyPress = cxGridDBTableView1KeyPress
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dmOtsustvo.dsOtsustvaPregled
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              Filtering.ColumnFilteredItemsList = True
              FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
              FilterRow.Visible = True
              FilterRow.ApplyChanges = fracImmediately
              OptionsBehavior.IncSearch = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              object cxGridDBTableView1RE_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'RE_OUT'
                Visible = False
                Width = 150
              end
              object cxGridDBTableView1Column1: TcxGridDBColumn
                DataBinding.FieldName = 'RENAZIV_OUT'
                Width = 197
              end
              object cxGridDBTableView1HRMRABID_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'HRMRABID_OUT'
                Visible = False
                Width = 150
              end
              object cxGridDBTableView1RMRE_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'RMRE_OUT'
                Visible = False
                Width = 150
              end
              object cxGridDBTableView1Column2: TcxGridDBColumn
                DataBinding.FieldName = 'RABMESTONAZIV_OUT'
                Width = 193
              end
              object cxGridDBTableView1MB_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'MB_OUT'
                Width = 86
              end
              object cxGridDBTableView1NAZIVVRABOTEN_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'NAZIVVRABOTEN_OUT'
                Visible = False
                Width = 250
              end
              object cxGridDBTableView1NAZIV_VRABOTEN_TI_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'NAZIV_VRABOTEN_TI_OUT'
                Width = 250
              end
              object cxGridDBTableView1COUNT_0_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'COUNT_0_OUT'
                Width = 109
              end
              object cxGridDBTableView1COUNT_1_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'COUNT_1_OUT'
              end
              object cxGridDBTableView1PLATENO_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'PLATENO_OUT'
                Width = 104
              end
              object cxGridDBTableView1NEPLATENO_OUT: TcxGridDBColumn
                DataBinding.FieldName = 'NEPLATENO_OUT'
                Width = 121
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBTableView1
            end
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1200
          Height = 73
          Align = alTop
          BevelInner = bvLowered
          BevelOuter = bvSpace
          TabOrder = 1
          object GroupBox1: TGroupBox
            Left = 62
            Top = 11
            Width = 383
            Height = 56
            Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
            TabOrder = 0
            object Label1: TLabel
              Left = 45
              Top = 27
              Width = 27
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1054#1076' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label3: TLabel
              Left = 205
              Top = 27
              Width = 27
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1086' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object OD_DATA: TcxDateEdit
              Left = 78
              Top = 24
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = OD_DATAExit
              OnKeyDown = EnterKakoTab
              Width = 121
            end
            object DO_DATA: TcxDateEdit
              Left = 238
              Top = 24
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = DO_DATAExit
              OnKeyDown = EnterKakoTab
              Width = 121
            end
          end
          object btnPregledPlateniNeplateni: TcxButton
            Left = 451
            Top = 32
            Width = 94
            Height = 25
            Action = actPregledPlateniNeplateni
            TabOrder = 1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1086#1089#1090#1072#1085#1072#1090#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1087#1086#1088#1077#1076' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 1200
          Height = 73
          Align = alTop
          BevelInner = bvLowered
          TabOrder = 0
          object Label2: TLabel
            Left = 73
            Top = 31
            Width = 62
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1043#1086#1076#1080#1085#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object godina: TcxComboBox
            Left = 141
            Top = 28
            Properties.Items.Strings = (
              '2000'
              '2001'
              '2002'
              '2003'
              '2004'
              '2005'
              '2006'
              '2007'
              '2008'
              '2009'
              '2010'
              '2011'
              '2012'
              '2013'
              '2014'
              '2015'
              '2016'
              '2017'
              '2018'
              '2019'
              '2020'
              '2021'
              '2022'
              '2023'
              '2024'
              '2025'
              '2026'
              '2027'
              '2028'
              '2029'
              '2030')
            Properties.OnChange = godinaPropertiesChange
            TabOrder = 0
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object btnPregledPlateniNeplateni1: TcxButton
            Left = 268
            Top = 26
            Width = 94
            Height = 25
            Action = actPregledajOstanatiDenovi
            TabOrder = 1
          end
        end
        object cxGrid3: TcxGrid
          Left = 0
          Top = 73
          Width = 1200
          Height = 400
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object cxGridDBTableView2: TcxGridDBTableView
            OnKeyPress = cxGridDBTableView2KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dmOtsustvo.dsOstanatiDenoviGO
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGridDBTableView2RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
              Width = 150
            end
            object cxGridDBTableView2RABOTNAEDINICANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNAEDINICANAZIV'
              Width = 197
            end
            object cxGridDBTableView2RM_ID: TcxGridDBColumn
              DataBinding.FieldName = 'RM_ID'
              Visible = False
              Width = 150
            end
            object cxGridDBTableView2RABOTNO_MESTO: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNO_MESTO'
              Visible = False
              Width = 150
            end
            object cxGridDBTableView2RABOTNOMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABOTNOMESTONAZIV'
              Width = 193
            end
            object cxGridDBTableView2MB: TcxGridDBColumn
              DataBinding.FieldName = 'MB'
              Width = 86
            end
            object cxGridDBTableView2NAZIV_VRABOTEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN'
              Visible = False
              Width = 250
            end
            object cxGridDBTableView2NAZIV_VRABOTEN_TI: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
              Width = 250
            end
            object cxGridDBTableView2BRGODINARESENIE: TcxGridDBColumn
              DataBinding.FieldName = 'BRGODINARESENIE'
              Width = 161
            end
            object cxGridDBTableView2RESENIEDENOVI: TcxGridDBColumn
              DataBinding.FieldName = 'RESENIEDENOVI'
              Width = 178
            end
            object cxGridDBTableView2DENOVI: TcxGridDBColumn
              DataBinding.FieldName = 'DENOVI'
              Width = 231
            end
            object cxGridDBTableView2OSTANATIDENOVI: TcxGridDBColumn
              DataBinding.FieldName = 'OSTANATIDENOVI'
              Width = 137
            end
            object cxGridDBTableView2ID_RESENIE_GO: TcxGridDBColumn
              Caption = #1056#1077#1096#1077#1085#1080#1077' ('#1064#1080#1092#1088#1072')'
              DataBinding.FieldName = 'RESENIEID'
              Visible = False
              Width = 100
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBTableView2
          end
        end
      end
      object cxTabSheet3: TcxTabSheet
        Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1087#1086#1090#1088#1086#1096#1077#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1087#1086' '#1090#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
        ImageIndex = 2
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pnl1: TPanel
          Left = 0
          Top = 0
          Width = 1200
          Height = 73
          Align = alTop
          BevelInner = bvLowered
          BevelOuter = bvSpace
          TabOrder = 0
          object grp1: TGroupBox
            Left = 62
            Top = 11
            Width = 383
            Height = 56
            Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
            TabOrder = 0
            object lbl1: TLabel
              Left = 45
              Top = 27
              Width = 27
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1054#1076' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lbl2: TLabel
              Left = 205
              Top = 27
              Width = 27
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1086' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object OD_DATA_TO: TcxDateEdit
              Left = 78
              Top = 24
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = OD_DATA_TOExit
              OnKeyDown = EnterKakoTab
              Width = 121
            end
            object DO_DATA_TO: TcxDateEdit
              Left = 238
              Top = 24
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = DO_DATA_TOExit
              OnKeyDown = EnterKakoTab
              Width = 121
            end
          end
          object btnPregledDenoviTipOtsustvo: TcxButton
            Left = 451
            Top = 32
            Width = 94
            Height = 25
            Action = actPregledDenoviTipOtsustvo
            TabOrder = 1
          end
        end
        object cxGrid4: TcxGrid
          Left = 0
          Top = 73
          Width = 1200
          Height = 400
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object cxGridDBTableView3: TcxGridDBTableView
            OnKeyPress = cxGridDBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dmOtsustvo.dsCountTipOtsustvo
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGridDBTableView3RE_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RE_OUT'
              Visible = False
              Width = 147
            end
            object cxGridDBTableView3RENAZIV_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV_OUT'
              Width = 227
            end
            object cxGridDBTableView3RABMESTONAZIV_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RABMESTONAZIV_OUT'
              Width = 250
            end
            object cxGridDBTableView3MB_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'MB_OUT'
              Width = 112
            end
            object cxGridDBTableView3NAZIVVRABOTEN_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIVVRABOTEN_OUT'
              Width = 210
            end
            object cxGridDBTableView3NAZIV_VRABOTEN_TI_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI_OUT'
              Visible = False
              Width = 250
            end
            object cxGridDBTableView3RMRE_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'RMRE_OUT'
              Visible = False
              Width = 250
            end
            object cxGridDBTableView3HRMRABID_OUT: TcxGridDBColumn
              DataBinding.FieldName = 'HRMRABID_OUT'
              Visible = False
              Width = 250
            end
            object cxGridDBTableView3TIP_OTSUSTVO: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_OTSUSTVO_NAZIV'
              Width = 208
            end
            object cxGridDBTableView3TIP_OTSUSTVO_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_OTSUSTVO_NAZIV'
              Visible = False
              Width = 250
            end
            object cxGridDBTableView3COUNT_TIP_OTSUSTVO: TcxGridDBColumn
              DataBinding.FieldName = 'COUNT_TIP_OTSUSTVO'
              Width = 79
            end
          end
          object cxGridLevel3: TcxGridLevel
            GridView = cxGridDBTableView3
          end
        end
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1204
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 3
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 677
    Width = 1204
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090', Shift+Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Exce' +
          'l'
        Width = 330
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object ActionList2: TActionList
    Images = dmRes.cxSmallImages
    Left = 688
    Top = 80
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aPregledOtsustvaPoGodina: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1086#1090#1089#1091#1089#1090#1074#1072' '#1087#1086' '#1075#1086#1076#1080#1085#1072
      ImageIndex = 19
      OnExecute = aPregledOtsustvaPoGodinaExecute
    end
    object aDPregledOtsustvaPoGodina: TAction
      Caption = 'aDPregledOtsustvaPoGodina'
      ImageIndex = 19
      ShortCut = 24697
      OnExecute = aDPregledOtsustvaPoGodinaExecute
    end
    object actPregledPlateniNeplateni: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 19
      OnExecute = actPregledPlateniNeplateniExecute
    end
    object actPregledDenoviTipOtsustvo: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 19
      OnExecute = actPregledDenoviTipOtsustvoExecute
    end
    object actPregledajOstanatiDenovi: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 19
      OnExecute = actPregledajOstanatiDenoviExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 592
    Top = 65528
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 64
      FloatClientHeight = 156
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 111
      FloatClientHeight = 126
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 250
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 104
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 133
      FloatClientHeight = 156
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 355
      DockedTop = 0
      FloatLeft = 1168
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPregledOtsustvaPoGodina
      Category = 0
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 632
    Top = 88
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid3
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 15000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44431.662327233800000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 15000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44431.662327256940000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository2
      Styles.StyleSheet = dxGridReportLinkStyleSheet2
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link3: TdxGridReportLink
      Component = cxGrid4
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 920
    Top = 72
  end
  object PopupMenu1: TPopupMenu
    Left = 416
    Top = 88
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 728
    Top = 48
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 680
    Top = 8
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 728
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 824
    Top = 80
    PixelsPerInch = 96
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle14
      Styles.Caption = cxStyle15
      Styles.CardCaptionRow = cxStyle16
      Styles.CardRowCaption = cxStyle17
      Styles.Content = cxStyle18
      Styles.ContentEven = cxStyle19
      Styles.ContentOdd = cxStyle20
      Styles.FilterBar = cxStyle21
      Styles.Footer = cxStyle22
      Styles.Group = cxStyle23
      Styles.Header = cxStyle24
      Styles.Preview = cxStyle25
      Styles.Selection = cxStyle26
      BuiltIn = True
    end
  end
end
