(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

unit DogovorVrabotuvanje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxCalendar, FIBDataSet, pFIBDataSet,
  cxGroupBox, FIBQuery, pFIBQuery, cxRadioGroup,  frxDesgn, frxClass, frxDBSet, frxRich,
  frxCross, frxEditDataBand, cxHyperLinkEdit, cxLabel,cxImage, GIFImg,
  cxSplitter, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxPScxSchedulerLnk, dxPSdxDBOCLnk, dxCustomHint, cxHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator, dxCore, cxDateUtils,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxRibbonCustomizationForm,
  cxImageComboBox, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmDogovorVrabotuvanje = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    lPanel: TPanel;
    dPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    dsRMRE: TDataSource;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1STEPEN_STRUCNA_PODGOTOVKA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VID_VRAB: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MB_DIREKTOR: TcxGridDBColumn;
    PodatociZaRabotnik: TcxGroupBox;
    Prezime: TcxDBTextEdit;
    Label6: TLabel;
    Ime: TcxDBTextEdit;
    Label7: TLabel;
    MBroj: TcxDBTextEdit;
    Label8: TLabel;
    Adresa: TcxDBTextEdit;
    Label9: TLabel;
    Label10: TLabel;
    MESTO: TcxDBTextEdit;
    PodatociZaVrabotuvanje: TcxGroupBox;
    Label5: TLabel;
    txtVidVrabotuvanje: TcxDBTextEdit;
    cbVidVrabotuvanje: TcxDBLookupComboBox;
    Label2: TLabel;
    ID_RM_RE: TcxDBTextEdit;
    ID_RM_RE_NAZIV: TcxDBLookupComboBox;
    Label1: TLabel;
    RABOTNI_CASOVI: TcxDBTextEdit;
    PeriodNaVazenje: TcxGroupBox;
    Label4: TLabel;
    Label3: TLabel;
    DO_VREME: TcxDBDateEdit;
    OD_VREME: TcxDBDateEdit;
    Label13: TLabel;
    SSP: TcxDBTextEdit;
    Label14: TLabel;
    OBRAZOVANIE_naziv: TcxDBLookupComboBox;
    aVnesiVoVrabotenRM: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    qRERM: TpFIBQuery;
    Label16: TLabel;
    TATKOVO_IME: TcxDBTextEdit;
    qEditPLTVraboteni: TpFIBQuery;
    tblPLTVraboteni: TpFIBDataSet;
    tblPLTVraboteniMB: TFIBStringField;
    tblPLTVraboteniFIRMA: TFIBIntegerField;
    tblPLTVraboteniPREZIME: TFIBStringField;
    tblPLTVraboteniTATKOVO_IME: TFIBStringField;
    tblPLTVraboteniIME: TFIBStringField;
    tblPLTVraboteniV_STA_GG: TFIBIntegerField;
    tblPLTVraboteniV_STA_MM: TFIBIntegerField;
    tblPLTVraboteniV_STA_DD: TFIBIntegerField;
    tblPLTVraboteniTIP_BOD_SAAT: TFIBSmallIntField;
    tblPLTVraboteniBOD_STAZ: TFIBBCDField;
    tblPLTVraboteniPATARINA: TFIBBCDField;
    tblPLTVraboteniLICOSIG: TFIBBCDField;
    tblPLTVraboteniSSM: TFIBSmallIntField;
    tblPLTVraboteniKOLEKTIVNO: TFIBSmallIntField;
    tblPLTVraboteniLBROJ: TFIBStringField;
    tblPLTVraboteniDOSIE: TFIBStringField;
    tblPLTVraboteniRAB_KNISKA: TFIBStringField;
    tblPLTVraboteniSPREMA: TFIBSmallIntField;
    tblPLTVraboteniMESTO: TFIBIntegerField;
    tblPLTVraboteniADRESA: TFIBStringField;
    tblPLTVraboteniTELEFON: TFIBStringField;
    tblPLTVraboteniPENZ_FOND: TFIBSmallIntField;
    tblPLTVraboteniRE_ID: TFIBIntegerField;
    tblPLTVraboteniOTEZNATI: TFIBBCDField;
    tblPLTVraboteniFUNKCIJA: TFIBBCDField;
    tblPLTVraboteniKORISNIK: TFIBStringField;
    tblPLTVraboteniVREME: TFIBDateTimeField;
    tblPLTVraboteniINVALID: TFIBSmallIntField;
    tblPLTVraboteniNORMATIV: TFIBSmallIntField;
    tblPLTVraboteniBOL_PROSEK: TFIBBCDField;
    tblPLTVraboteniNOVOVRABOTEN: TFIBSmallIntField;
    tblPLTVraboteniFZO: TFIBSmallIntField;
    tblPLTVraboteniBRUTO_DOGOVOR: TFIBBCDField;
    dsPLTVraboteni: TDataSource;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1PLATA: TcxGridDBColumn;
    aPrisilnoBrisenje: TAction;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    cbMesto: TcxDBLookupComboBox;
    dxBarLargeButton18: TdxBarLargeButton;
    aPregledDogovor: TAction;
    dxBarManager1Bar6: TdxBar;
    Label11: TLabel;
    DatumKreiranje: TcxDBDateEdit;
    Label12: TLabel;
    Staz: TcxGroupBox;
    Label17: TLabel;
    StazGodina: TcxDBTextEdit;
    Label18: TLabel;
    StazMesec: TcxDBTextEdit;
    Label19: TLabel;
    StazDen: TcxDBTextEdit;
    Label15: TLabel;
    MB: TcxDBTextEdit;
    Label20: TLabel;
    Broj: TcxDBTextEdit;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_DEN: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_MESEC: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    dxBarManager1Bar7: TdxBar;
    aAneks: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    cxGroupBox1: TcxGroupBox;
    Label21: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    Rakovodenje: TcxDBTextEdit;
    DenUsloviGO: TcxDBTextEdit;
    tblRMRERAKOVODENJE: TFIBBCDField;
    tblRMREDENOVI_ODMOR: TFIBIntegerField;
    tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    aKalkulatorStaz: TAction;
    qPlataDaNe: TpFIBQuery;
    qProveriDatumOd: TpFIBQuery;
    aBrisiDokument: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    cxBarEditItem3: TcxBarEditItem;
    cxBarEditItem4: TcxBarEditItem;
    cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn;
    cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    Label26: TLabel;
    koeficient: TcxDBTextEdit;
    tblRMRESTEPEN_SLOZENOST: TFIBBCDField;
    cxGrid1DBTableView1KOEFICIENT: TcxGridDBColumn;
    dxBarLargeButton21: TdxBarLargeButton;
    cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA_PROCENT: TcxGridDBColumn;
    tblRMREUSLOVI_RABOTA: TFIBIntegerField;
    tblRMREUSLOVIRABOTAID: TFIBIntegerField;
    tblRMREUSLOVIRABOTAPROCENT: TFIBBCDField;
    aKopiraj: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    Panel1: TPanel;
    cxGroupBox2: TcxGroupBox;
    Label27: TLabel;
    Label28: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    aKopirajOdPanel: TAction;
    aOtkaziKopiranje: TAction;
    dateOD: TcxDateEdit;
    dateDO: TcxDateEdit;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarButton3: TdxBarButton;
    dxBarLargeButton23: TdxBarLargeButton;
    cxHintStyleController1: TcxHintStyleController;
    Label29: TLabel;
    BENEFICIRAN_STAZ: TcxDBTextEdit;
    Label30: TLabel;
    Label31: TLabel;
    txtPlata: TcxDBTextEdit;
    cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn;
    cxGrid1DBTableView1BENEFICIRAN_STAZ: TcxGridDBColumn;
    tblRMREBENEFICIRAN_STAZ: TFIBBCDField;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton24: TdxBarLargeButton;
    aUsloviPoteskiOdNormanite: TAction;
    tblPLTVraboteniBR_LICNA_KARTA: TFIBStringField;
    Label22: TLabel;
    DATUM_PRVO_VRAB: TcxDBDateEdit;
    cxGrid1DBTableView1DATUM_PRVO_VRAB: TcxGridDBColumn;
    cxGrid1DBTableView1aktiven: TcxGridDBColumn;
    cxGrid1DBTableView1zatvorenSo: TcxGridDBColumn;
    tblPLTVraboteniBOD: TFIBFloatField;
    lbl1: TLabel;
    Neto: TcxDBTextEdit;
    lbl2: TLabel;
    Bodovi: TcxDBTextEdit;
    cxGrid1DBTableView1NETO: TcxGridDBColumn;
    cxGrid1DBTableView1BODOVI: TcxGridDBColumn;
    cxGrid1DBTableView1SIS_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1SIS_DATUM_DO: TcxGridDBColumn;
    tblRMRESIS_OPIS: TFIBStringField;
    aStazPrethoden: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbVidVrabotuvanjePropertiesChange(Sender: TObject);
    procedure VRABOTENIMEPropertiesChange(Sender: TObject);
    procedure MBrojExit(Sender: TObject);
    function proverkaModul11(broj: Int64): boolean;
    procedure setDatumPol(matBr:String);
    procedure MBrojPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure aVnesiVoVrabotenRMExecute(Sender: TObject);
    procedure aPrisilnoBrisenjeExecute(Sender: TObject);
    procedure cxBarEditItem2PropertiesChange(Sender: TObject);
    procedure dxBarLargeButton18Click(Sender: TObject);
    procedure aPregledDogovorExecute(Sender: TObject);
    procedure aAneksExecute(Sender: TObject);
    procedure cxGroupBox1Enter(Sender: TObject);
    procedure aKalkulatorStazExecute(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure ShellOpen(const Url: string; const Params: string = '');
    procedure PodatociZaVrabotuvanjeClick(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aKopirajExecute(Sender: TObject);
    procedure aKopirajOdPanelExecute(Sender: TObject);
    procedure aOtkaziKopiranjeExecute(Sender: TObject);
    procedure aUsloviPoteskiOdNormaniteExecute(Sender: TObject);
    procedure aStazPrethodenExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmDogovorVrabotuvanje: TfrmDogovorVrabotuvanje;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmUnitOtsustvo,
  dmMaticni,dmReportUnit, AneksNaDogovor, dmSistematizacija, StazKalkulator,
  BaranjeZaGodisenOdmor, ShellAPI, RabotnoMestoUsloviRabota;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmDogovorVrabotuvanje.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmDogovorVrabotuvanje.aNovExecute(Sender: TObject);
var pom:Integer;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    {pom:=dmOtsustvo.zemiBroj(dm.PROC_HR_AKTIVEN_DOG_ANEKS, 'MB', 'ID_RE_FIRMA', 'DATUM', Null,dm.tblLicaVraboteniMB.Value, dmKon.re,Now, Null, 'AKTIVEN_BR');
    if pom > 0 then
       begin
          ShowMessage('����� ������� ������� �� ����������� ��� ������� ����� �� ������� �� ��� ���� !!!');
       end
    else
       begin }
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          DatumKreiranje.SetFocus;
          cxGrid1DBTableView1.DataController.DataSet.Insert;
          dm.tblDogVrabotuvanjeVID_DOKUMENT.Value:= dogovorZaRabota;
          dm.tblDogVrabotuvanjeID_RE_FIRMA.Value:=dmKon.re;
          if tag = 1 then
            begin
              dm.tblDogVrabotuvanjeMB.Value:=dm.tblLicaVraboteniMB.Value;
              dm.tblDogVrabotuvanjePREZIME.Value:=dm.tblLicaVraboteniPREZIME.Value;
              dm.tblDogVrabotuvanjeTATKOVO_IME.Value:=dm.tblLicaVraboteniTATKOVO_IME.Value;
              dm.tblDogVrabotuvanjeIME.Value:=dm.tblLicaVraboteniIME.Value;
              dm.tblDogVrabotuvanjeSTEPEN_STRUCNA_PODGOTOVKA.Value:=dm.tblLicaVraboteniOBRAZOVANIE.Value;
              dm.tblDogVrabotuvanjeMESTO.Value:=dm.tblLicaVraboteniMESTO.Value;
              dm.tblDogVrabotuvanjeADRESA.Value:=dm.tblLicaVraboteniADRESA.Value;
            end;
     // end
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmDogovorVrabotuvanje.aAneksExecute(Sender: TObject);
begin
     dm.tblAneks.Close;
     dm.tblAneks.ParamByName('dogv').Value:=dm.tblDogVrabotuvanjeID.Value;
     dm.tblAneks.ParamByName('id').Value:= '%';
     dm.tblAneks.open;
     frmAneksNaDogovor:=TfrmAneksNaDogovor.Create(Application);
     frmAneksNaDogovor.ShowModal;
     frmAneksNaDogovor.Free;
end;

procedure TfrmDogovorVrabotuvanje.aAzurirajExecute(Sender: TObject);
var pom, pom_aktiven_1,pom_aktiven_2:Integer;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
       begin
         // pom_aktiven_1:=dmOtsustvo.zemiBroj(dm.PROC_HR_AKTIVEN_DOG_ID, 'MB', 'ID_RE_FIRMA', 'DATUM', 'DOGOVOR_ID',dm.tblLicaVraboteniMB.Value, dmKon.re,Now, dm.tblDogVrabotuvanjeID.Value, 'BR1');
         //pom_aktiven_2:=dmOtsustvo.zemiBroj(dm.PROC_HR_AKTIVEN_DOG_ID, 'MB', 'ID_RE_FIRMA', 'DATUM', 'DOGOVOR_ID',dm.tblLicaVraboteniMB.Value, dmKon.re,Now, dm.tblDogVrabotuvanjeID.Value, 'BR2');
         //if (pom_aktiven_1 = 1)or (pom_aktiven_2 = -1) then
         //if (dm.tblDogVrabotuvanjeDATUM_DO.IsNull) or (strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_DO.Value)) >= strtodate(DateToStr(Now))) then
         // begin
          qPlataDaNe.Close;
          qPlataDaNe.ParamByName('app').Value:=dmKon.aplikacija;
          qPlataDaNe.ParamByName('param').Value:='zaedno_so_plata';
          qPlataDaNe.ExecQuery;
          if qPlataDaNe.FldByName['v1'].Value = 'da' then
             begin
                if DO_VREME.Text <> '' then
                   pom:=dmOtsustvo.zemiBroj(dm.pUslovPlata, 'DATUM_OD', 'DATUM_DO', 'MB', 'FIRMA', dm.tblDogVrabotuvanjeDATUM_OD.Value, dm.tblDogVrabotuvanjeDATUM_DO.Value, dm.tblDogVrabotuvanjeMB.Value, dmkon.re, 'BROJ')
                else
                   pom:=dmOtsustvo.zemiBroj(dm.pUslovPlata, 'DATUM_OD', 'DATUM_DO', 'MB', 'FIRMA', dm.tblDogVrabotuvanjeDATUM_OD.Value,Now, dm.tblDogVrabotuvanjeMB.Value, dmKon.re, 'BROJ');
                if pom > 0 then
                   begin
                    ShowMessage('�������� �� �� ��������� ��� �������. ��� � ���������� ����� �� ��� ������ !!!');
                    //frmAneksNaDogovor:=TfrmAneksNaDogovor.Create(Application);
                    //frmAneksNaDogovor.ShowModal;
                    //frmAneksNaDogovor.Free;
                   end
                else
                   begin
                     dPanel.Enabled:=True;
                     lPanel.Enabled:=False;
                     MBroj.Enabled:=False;
                     DatumKreiranje.SetFocus;
                     cxGrid1DBTableView1.DataController.DataSet.Edit;
                   end;
             end
          else
             begin
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               MBroj.Enabled:=False;
               OD_VREME.Enabled:=False;
               DO_VREME.Enabled:=false;
               //ID_RM_RE.Enabled:=false;
               //ID_RM_RE_NAZIV.Enabled:=false;
               DatumKreiranje.SetFocus;
               cxGrid1DBTableView1.DataController.DataSet.Edit;
             end;
        ///  end
       //  else  ShowMessage('��� ������� �� ����������� �� � ������� ��� ��� ������� ����� �� �������!!!')
      end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmDogovorVrabotuvanje.aBrisiDokumentExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteDokument.Close;
            dmOtsustvo.qDeleteDokument.ParamByName('id').Value:=dm.tblDogVrabotuvanjeID.Value;
            dmOtsustvo.qDeleteDokument.ExecQuery;
        end;
end;

procedure TfrmDogovorVrabotuvanje.aBrisiExecute(Sender: TObject);
var pom, pom1, f, d, pom_aktiven_1,pom_aktiven_2:Integer;
    m:string;
begin
      if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
         begin
           frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
           if (frmDaNe.ShowModal <> mrYes) then
              Abort
           else
              begin
               { pom_aktiven_1:=dmOtsustvo.zemiBroj(dm.PROC_HR_AKTIVEN_DOG_ID, 'MB', 'ID_RE_FIRMA', 'DATUM', 'DOGOVOR_ID',dm.tblLicaVraboteniMB.Value, dmKon.re,Now, dm.tblDogVrabotuvanjeID.Value, 'BR1');
                pom_aktiven_2:=dmOtsustvo.zemiBroj(dm.PROC_HR_AKTIVEN_DOG_ID, 'MB', 'ID_RE_FIRMA', 'DATUM', 'DOGOVOR_ID',dm.tblLicaVraboteniMB.Value, dmKon.re,Now, dm.tblDogVrabotuvanjeID.Value, 'BR2');
                if (pom_aktiven_1 = 1)or (pom_aktiven_2 = -1) then
                //if (dm.tblDogVrabotuvanjeDATUM_DO.IsNull) or (strtodate(DateToStr(dm.tblDogVrabotuvanjeDATUM_DO.Value)) >= strtodate(DateToStr(Now))) then
                    ShowMessage('��� ������� �� ����������� �� � ������� ��� ��� ������� ����� �� ������� !!!')
                else
                  begin }
                    qPlataDaNe.Close;
                    qPlataDaNe.ParamByName('app').Value:=dmKon.aplikacija;
                    qPlataDaNe.ParamByName('param').Value:='zaedno_so_plata';
                    qPlataDaNe.ExecQuery;
                    if qPlataDaNe.FldByName['v1'].Value = 'da' then
                       begin
                        if DO_VREME.Text <> '' then
                          pom:=dmOtsustvo.zemiBroj(dm.pUslovPlata, 'DATUM_OD', 'DATUM_DO', 'MB', 'FIRMA', dm.tblDogVrabotuvanjeDATUM_OD.Value, dm.tblDogVrabotuvanjeDATUM_DO.Value, dm.tblDogVrabotuvanjeMB.Value, dmkon.re, 'BROJ')
                        else
                          pom:=dmOtsustvo.zemiBroj(dm.pUslovPlata, 'DATUM_OD', 'DATUM_DO', 'MB', 'FIRMA', dm.tblDogVrabotuvanjeDATUM_OD.Value,Now, dm.tblDogVrabotuvanjeMB.Value, dmKon.re, 'BROJ');
                       if pom > 0 then
                          ShowMessage('�������� �� �� ������� ��� �������. ��� � ���������� ����� �� ��� ������ !!!')
                       else  if pom = 0 then
                        begin
                          m:=dm.tblDogVrabotuvanjeMB.Value;
                          d:=dm.tblDogVrabotuvanjeID.Value;
                          cxGrid1DBTableView1.DataController.DataSet.Delete();

                          dmOtsustvo.qDeleteVrabotenRM.Close;
                          dmOtsustvo.qDeleteVrabotenRM.ParamByName('documentID').AsInteger:=d;
                          dmOtsustvo.qDeleteVrabotenRM.ParamByName('vid_dokument').AsInteger:=dogovorZaRabota;
                          dmOtsustvo.qDeleteVrabotenRM.ExecQuery;
                        end;
                        end
                    else
                    begin
                      ///pom1:=dmOtsustvo.zemiBroj(dm.pCountMB_PLT_VRABOTEN, 'MB', 'FIRMA', Null, Null, dm.tblDogVrabotuvanjeMB.Value, dmKon.re, Null, Null, 'BROJ');
                       {m:=dm.tblDogVrabotuvanjeMB.Value;
                       d:=dm.tblDogVrabotuvanjeID.Value;
                       cxGrid1DBTableView1.DataController.DataSet.Delete();

                       dmOtsustvo.qDeleteVrabotenRM.Close;
                       dmOtsustvo.qDeleteVrabotenRM.ParamByName('documentID').AsInteger:=d;
                       dmOtsustvo.qDeleteVrabotenRM.ParamByName('vid_dokument').AsInteger:=dogovorZaRabota;
                       dmOtsustvo.qDeleteVrabotenRM.ExecQuery;   }

                       dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP',null,null,
                                         dm.tblDogVrabotuvanjeMB.Value, Null, Null, Null, dm.tblDogVrabotuvanjeVID_DOKUMENT.Value, dm.tblDogVrabotuvanjeID.Value, dmKon.re, 2,null,null);
                       dm.tblDogVrabotuvanje.FullRefresh;
                    end;
              end;
         end;

end;

//	����� �� ���������� �� ����������
procedure TfrmDogovorVrabotuvanje.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmDogovorVrabotuvanje.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmDogovorVrabotuvanje.ShellOpen(const Url: string; const Params: string = '');
begin
     ShellExecute(0, 'Open', PChar(Url), PChar(Params), nil, SW_SHOWNORMAL);
end;

procedure TfrmDogovorVrabotuvanje.aKalkulatorStazExecute(Sender: TObject);
begin
       ShellOpen('http://www.timeanddate.com/date/duration.html?y1=2005&m1=3&d1=16&y2=2011&m2=3&d2=16&ti=on');
//     frmStazKalkulator:=TfrmStazKalkulator.Create(Application);
//     frmStazKalkulator.ShowModal;
//     frmStazKalkulator.Free;
end;

procedure TfrmDogovorVrabotuvanje.aKopirajExecute(Sender: TObject);
var pom:Integer;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    pom:=dmOtsustvo.zemiBroj(dm.pActivenDogovor, 'MB', 'FIRMA', Null, Null,dm.tblLicaVraboteniMB.Value, dmKon.re, Null, Null, 'BROJ');
    if pom <> 0 then
       begin
          ShowMessage('����� ������� ������� �� ��� ���� !!!');
       end
    else
       begin
         Panel1.Visible:=true;
       end
    end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;



procedure TfrmDogovorVrabotuvanje.aKopirajOdPanelExecute(Sender: TObject);
var pomID:Integer;
begin
          if (Validacija(Panel1) = false) then
             begin
               pomID:=dmOtsustvo.zemiRezultat(dmOtsustvo.pKopirajDogovor, 'ID_DOGOVOR', 'DATUM_OD_IN', 'DATUM_DO_IN', Null, Null, dm.tblDogVrabotuvanjeID.Value, dateOD.EditValue, dateDO.EditValue ,Null, Null, 'pomID');
               dateOD.Text:='';
               dateDO.Text:='';
               panel1.visible:=False;

               dm.tblDogVrabotuvanje.FullRefresh;
               dm.tblDogVrabotuvanje.Locate('ID', pomID, []);
               dPanel.Enabled:=True;
               lPanel.Enabled:=False;
               MBroj.Enabled:=False;
               DatumKreiranje.SetFocus;
               cxGrid1DBTableView1.DataController.DataSet.Edit;
             end;

end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmDogovorVrabotuvanje.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmDogovorVrabotuvanje.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmDogovorVrabotuvanje.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmDogovorVrabotuvanje.cxBarEditItem2PropertiesChange(
  Sender: TObject);
var  AItemList: TcxFilterCriteriaItemList;
begin
  if cxBarEditItem2.EditValue = 0 then
   begin
      cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1aktiven, foEqual, 1, '1');
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
   end
   else
     cxGrid1DBTableView1.DataController.Filter.Clear;
end;

procedure TfrmDogovorVrabotuvanje.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
    if Sender=TcxLookupComboBox(ID_RM_RE_NAZIV) then
    begin
      ID_RM_RE_NAZIV.Properties.ListOptions.SyncMode:=true;
    end;

end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmDogovorVrabotuvanje.cxDBTextEditAllExit(Sender: TObject);
begin
    if (Sender= ID_RM_RE_NAZIV) or (Sender = ID_RM_RE) then
    begin
       if (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
          begin
            dm.tblDogVrabotuvanjeRAKOVODENJE.Value:=tblRMRERAKOVODENJE.Value;
            dm.tblDogVrabotuvanjeDEN_USLOVI_RABOTA.Value:=tblRMREDEN_USLOVI_RABOTA.Value;
            dm.tblDogVrabotuvanjeKOEFICIENT.Value:=tblRMRESTEPEN_SLOZENOST.Value;
            dm.tblDogVrabotuvanjeBENEFICIRAN_STAZ.Value:=tblRMREBENEFICIRAN_STAZ.Value;
          end;
    end
    else
    if Sender=TcxLookupComboBox(ID_RM_RE) then
    begin
       ID_RM_RE_NAZIV.Properties.ListOptions.SyncMode:=true;
    end
    else if ((Sender = OD_VREME)and (OD_VREME.Text <> '')) then
      begin
         aStazPrethoden.Execute();
      end;
    {else if ((Sender = OD_VREME)and (OD_VREME.Text <> '')) then
       begin
          qProveriDatumOd.Close;
          qProveriDatumOd.ParamByName('datum_od').Value:=OD_VREME.EditValue;
          qProveriDatumOd.ParamByName('mb').Value:=dm.tblLicaVraboteniMB.Value;
          qProveriDatumOd.ParamByName('id_re_firma').Value:=dmKon.re;
          qProveriDatumOd.ExecQuery;
          if ((qProveriDatumOd.FldByName['br'].Value >0) and (cxGrid1DBTableView1.DataController.DataSet.State in [dsInsert])) then
            begin
              OD_VREME.Clear;
              ShowMessage('��������� ����� ��, �� ��������� �� ������ �� ����� �� ���� �������� �� ������� ����� !!!');
            end
          else if ((qProveriDatumOd.FldByName['br'].Value >1) and (cxGrid1DBTableView1.DataController.DataSet.State in [dsEdit])) then
            begin
              OD_VREME.Clear;
              ShowMessage('��������� ����� ��, �� ��������� �� ������ �� ����� �� ���� �������� �� ������� ����� !!!');
            end;
       end; }

    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmDogovorVrabotuvanje.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmDogovorVrabotuvanje.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmDogovorVrabotuvanje.VRABOTENIMEPropertiesChange(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmDogovorVrabotuvanje.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmDogovorVrabotuvanje.MBrojExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
//     if (MBroj.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State in [dsInsert, dsEdit]) then
//
//     if proverkaModul11(StrToInt64(MBroj.Text))then
//       setDatumPol(dm.tblHRVraboteniMB.Value);
end;

procedure TfrmDogovorVrabotuvanje.MBrojPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
     if (MBroj.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State in [dsInsert, dsEdit]) then
     if proverkaModul11(StrToInt64(MBroj.Text)) = false then
        begin
           Error:=true;
           ErrorText:='��������� ������� ��� �� � ������� !!!';
           MBroj.SetFocus;
        end
     else  Error:=false;
end;

procedure TfrmDogovorVrabotuvanje.PodatociZaVrabotuvanjeClick(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmDogovorVrabotuvanje.prefrli;
begin
end;

procedure TfrmDogovorVrabotuvanje.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            aZapisi.Execute;
            Action := caFree;
          end
          else Action := caNone;
    end;
    dm.viewVraboteni.Close;
    dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
    dm.viewVraboteni.ParamByName('mb').Value:='%';
    dm.viewVraboteni.ParamByName('re').Value:='%';
    dm.viewVraboteni.Open;

end;
procedure TfrmDogovorVrabotuvanje.FormCreate(Sender: TObject);
begin
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
//     ProcitajFormaIzgled(self);

     dmMat.tblMesto.Open;
   //  tblPLTVraboteni.Close;
    // tblPLTVraboteni.Open;
     tblRMRE.ParamByName('firma').Value:=dmKon.re;
     tblRMRE.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmDogovorVrabotuvanje.FormShow(Sender: TObject);
var  AItemList: TcxFilterCriteriaItemList;
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    if tag = 1 then
       begin
         dm.tblDogVrabotuvanje.close;
         dm.tblDogVrabotuvanje.ParamByName('mb').Value:=dm.tblLicaVraboteniMB.Value;
         dm.tblDogVrabotuvanje.ParamByName('firma').Value:=dmKon.re;
         dm.tblDogVrabotuvanje.Open;
         dxBarManager1Bar5.Visible:=false;
       end
    else
       begin
         dm.tblDogVrabotuvanje.close;
         dm.tblDogVrabotuvanje.ParamByName('mb').Value:='%';
         dm.tblDogVrabotuvanje.ParamByName('firma').Value:=dmKon.re;
         dm.tblDogVrabotuvanje.Open;
         dxBarManager1Bar1.Visible:=false;

         cxBarEditItem2.EditValue:=0;

         if cxBarEditItem2.EditValue = 0 then
          begin
            cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
            try
              cxGrid1DBTableView1.DataController.Filter.Root.Clear;
              cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1aktiven, foEqual, 1, '1');
              cxGrid1DBTableView1.DataController.Filter.Active:=True;
            finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
          end
         else
            cxGrid1DBTableView1.DataController.Filter.Clear;
       end;
end;
//------------------------------------------------------------------------------

procedure TfrmDogovorVrabotuvanje.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmDogovorVrabotuvanje.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmDogovorVrabotuvanje.cxGroupBox1Enter(Sender: TObject);
begin
//        dm.tblDogVrabotuvanjeRAKOVODENJE.AsString:=Rakovodenje.text;
        //dm.tblDogVrabotuvanjeUSLOVI_RABOTA.Value:=dmsis.tblRMREUSLOVI_RABOTA.Value;
       // dm.tblDogVrabotuvanjeDEN_USLOVI_RABOTA.Value:=dmsis.tblRMREDEN_USLOVI_RABOTA.Value;
        //dm. tblDogVrabotuvanjeDENOVI_ODMOR.Value:=dmsis.tblRMREDENOVI_ODMOR.Value;
end;

procedure TfrmDogovorVrabotuvanje.dxBarLargeButton18Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmDogovorVrabotuvanje.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  pom: integer;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsInsert] then
  begin
   if (Validacija(dPanel) = false) then
    begin
        if (OD_VREME.Text <> '')and (DO_VREME.Text <> '') and (DO_VREME.Date < OD_VREME.Date ) then
          begin
               ShowMessage('������ � �������� ������ !!!');
               DO_VREME.SetFocus;
          end
        else
        begin

           //--------�� �� ����� ����� �� hr_vraboten_rm
          if dm.tblDogVrabotuvanjeDATUM_DO.isNull then
             begin
               dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
               dm.tblDogVrabotuvanjeMB.Value, dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value, dm.tblDogVrabotuvanjeDATUM_OD.Value, Null, dm.tblDogVrabotuvanjeVID_DOKUMENT.Value, dm.tblDogVrabotuvanjeID.Value, dmKon.re, 1, dm.tblDogVrabotuvanjeDATUM.Value,null);
             end
          else
             begin
               dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR', Null,
               dm.tblDogVrabotuvanjeMB.Value, dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value, dm.tblDogVrabotuvanjeDATUM_OD.Value, dm.tblDogVrabotuvanjeDATUM_DO.Value, dm.tblDogVrabotuvanjeVID_DOKUMENT.Value, dm.tblDogVrabotuvanjeID.Value, dmKon.re, 1, dm.tblDogVrabotuvanjeDATUM.Value,Null);
             end;
          cxGrid1DBTableView1.DataController.DataSet.Post;
          //---------�� �� ����� ����� �� PLT_VRABOTENI
          qPlataDaNe.Close;
          qPlataDaNe.ParamByName('app').Value:=dmKon.aplikacija;
          qPlataDaNe.ParamByName('param').Value:='zaedno_so_plata';
          qPlataDaNe.ExecQuery;
          if qPlataDaNe.FldByName['v1'].Value = 'da' then
            begin
              pom:=dmOtsustvo.zemiBroj(dm.pCountMB_PLT_VRABOTEN, 'MB', 'FIRMA', Null,Null,dm.tblDogVrabotuvanjeMB.Value, dmkon.re, Null, Null,'BROJ');
              if pom = 0 then
                begin
                    qRERM.close;
                    qRERM.ParamByName('re_id').AsInteger:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
                    qRERM.ExecQuery;
                    tblPLTVraboteni.insert;
                    tblPLTVrabotenimb.AsString:=dm.tblDogVrabotuvanjeMB.Value;
                    tblPLTVrabotenifirma.AsInteger:=dmKon.re;
                    tblPLTVraboteniprezime.AsString:=dm.tblDogVrabotuvanjePREZIME.Value;
                    tblPLTVrabotenitatkovo_ime.AsString:=dm.tblDogVrabotuvanjeTATKOVO_IME.value;
                    tblPLTVraboteniime.AsString:=dm.tblDogVrabotuvanjeIME.Value;
                    tblPLTVrabotenire_id.AsInteger:=qRERM.FldByName['re_id'].Value;
                    tblPLTVrabotenimesto.AsInteger:=dm.tblDogVrabotuvanjeMESTO.Value;
                    tblPLTVraboteni.Post;
                    tblPLTVraboteni.FullRefresh;
                end;
            end;
            MBroj.Enabled:=true;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;
            cxGrid1.SetFocus;
        end;
      end;
    end;

     if (st = dsEdit) then
      begin
        if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            MBroj.Enabled:=true;
            OD_VREME.Enabled:=True;
            DO_VREME.Enabled:=True;
            //ID_RM_RE.Enabled:=True;
            //ID_RM_RE_NAZIV.Enabled:=True;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;
            cxGrid1.SetFocus;

            dmOtsustvo.insert10(dm.pInsertVrabotenRM, 'MB_IN', 'RM_IN', 'DATUMOD_IN', 'DATUDO_IN', 'DOCUMENT_IN', 'DOCUMENT_ID_IN','FIRMA','TIP', 'DATUM_DOGOVOR',null,
                                dm.tblDogVrabotuvanjeMB.Value, dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value, Null, Null, dm.tblDogVrabotuvanjeVID_DOKUMENT.Value, dm.tblDogVrabotuvanjeID.Value, dmKon.re, 4, Null,null);

//            dmOtsustvo.qUpdateVrabotenRM.Close;
////            dmOtsustvo.qUpdateVrabotenRM.ParamByName('datumOD').AsDate:=dm.tblDogVrabotuvanjeDATUM_OD.Value;
////            if dm.tblDogVrabotuvanjeDATUM_DO.IsNull then
////               dmOtsustvo.qUpdateVrabotenRM.ParamByName('datumDO').Value:=Null
////            else
////               dmOtsustvo.qUpdateVrabotenRM.ParamByName('datumDO').AsDate:=dm.tblDogVrabotuvanjeDATUM_DO.Value;
//            dmOtsustvo.qUpdateVrabotenRM.ParamByName('document_id').AsInteger:=dm.tblDogVrabotuvanjeID.Value;
//            dmOtsustvo.qUpdateVrabotenRM.ParamByName('ID_RM_RE').AsInteger:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
//            dmOtsustvo.qUpdateVrabotenRM.ParamByName('vid_dokument').AsInteger:=dogovorZaRabota;
//            dmOtsustvo.qUpdateVrabotenRM.ExecQuery;
          end;
      end;
end;

procedure TfrmDogovorVrabotuvanje.cbVidVrabotuvanjePropertiesChange(
  Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmDogovorVrabotuvanje.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      MBroj.Enabled:=true;
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      OD_VREME.Enabled:=True;
      DO_VREME.Enabled:=True;
      ID_RM_RE.Enabled:=True;
      ID_RM_RE_NAZIV.Enabled:=True;
      cxGrid1.SetFocus;
  end;
end;

procedure TfrmDogovorVrabotuvanje.aOtkaziKopiranjeExecute(Sender: TObject);
begin
      dateOD.Text:='';
      dateDO.Text:='';
      Panel1.Visible:=False;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmDogovorVrabotuvanje.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmDogovorVrabotuvanje.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmDogovorVrabotuvanje.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmDogovorVrabotuvanje.aPregledDogovorExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.DogovorZaVrabotuvanje.close;
  dmReport.DogovorZaVrabotuvanje.ParamByName('ID').Value:=dm.tblDogVrabotuvanjeID.Value;
  dmReport.DogovorZaVrabotuvanje.ParamByName('param').Value:=dm.tblDogVrabotuvanjeDATUM.Value;
  dmReport.DogovorZaVrabotuvanje.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.DogovorZaVrabotuvanjeDATA.IsNull then
     begin
       RptStream := dmReport.DogovorZaVrabotuvanje.CreateBlobStream(dmReport.DogovorZaVrabotuvanjeDATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
       begin
       RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
       If RichView <> Nil Then
          begin
           RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
           RichView.DataSet:=dmReport.frxDogovorVrabotuvanje;
         end;
       MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
       if MasterData <> Nil then
          MasterData.DataSet:=dmReport.frxDogovorVrabotuvanje;

       dmReport.frxReport1.ShowReport;

       frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 2);
       end;
     end;
end;

procedure TfrmDogovorVrabotuvanje.aPrisilnoBrisenjeExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
       cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmDogovorVrabotuvanje.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmDogovorVrabotuvanje.aStazPrethodenExecute(Sender: TObject);
begin
   if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
       if (MBroj.Text<>'') and (OD_VREME.Text<>'') then
          begin
             dm.qCountDogovoriMB.Close;
             dm.qCountDogovoriMB.ParamByName('mb').Value:=MBroj.Text;
             dm.qCountDogovoriMB.ExecQuery();
             if dm.qCountDogovoriMB.FldByName['br'].Value > 0 then
                begin
                  dm.tblDogVrabotuvanjeSTAZ_GODINA.Value:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPROC_HR_STAZ_DOGOVOR,'MB', 'DATUM_OD_IN',null, Null, Null,dm.tblDogVrabotuvanjeMB.Value, dm.tblDogVrabotuvanjeDATUM_OD.Value,null, null, null, 'GODINA');
                  dm.tblDogVrabotuvanjeSTAZ_MESEC.Value:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPROC_HR_STAZ_DOGOVOR,'MB', 'DATUM_OD_IN',null, Null, Null,dm.tblDogVrabotuvanjeMB.Value, dm.tblDogVrabotuvanjeDATUM_OD.Value,null, null, null, 'MESEC');
                  dm.tblDogVrabotuvanjeSTAZ_DEN.Value:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPROC_HR_STAZ_DOGOVOR,'MB', 'DATUM_OD_IN',null, Null, Null,dm.tblDogVrabotuvanjeMB.Value, dm.tblDogVrabotuvanjeDATUM_OD.Value,null, null, null, 'DEN');
                end;
          end;
    end;
end;

procedure TfrmDogovorVrabotuvanje.aUsloviPoteskiOdNormaniteExecute(
  Sender: TObject);
begin
  frmRmReUsloviRabota:=TfrmRmReUsloviRabota.Create(Self, true);
  frmRmReUsloviRabota.Tag:=2;
  frmRmReUsloviRabota.ShowModal;
  frmRmReUsloviRabota.Free;
end;

procedure TfrmDogovorVrabotuvanje.aVnesiVoVrabotenRMExecute(Sender: TObject);
begin

end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmDogovorVrabotuvanje.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmDogovorVrabotuvanje.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmDogovorVrabotuvanje.aHelpExecute(Sender: TObject);
begin
      Application.HelpContext(15);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmDogovorVrabotuvanje.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmDogovorVrabotuvanje.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


function TfrmDogovorVrabotuvanje.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;

procedure TfrmDogovorVrabotuvanje.setDatumPol(matBr:String);
var  datum:string;
begin
     datum:= matBr[1] + matBr[2] + '.' + matBr[3] + matBr[4] + '.';
     if StrToInt(matBr[5]) = 9 then
        datum:=datum + '1'
     else if StrToInt(matBr[5]) in [0,1]then
        datum:=datum + '2';
     datum:=datum + matBr[5] + matBr[6]+ matBr[7];
     dm.tblHRVraboteniDATUM_RADJANJE.Value:=StrToDate(datum);
     if strtoint(matBr[10]) in [0,1,2,3,4] then
        dm.tblHRVraboteniPOL.Value:=1
     else
     if strtoint(matBr[10]) in [5,6,7,8,9] then
        dm.tblHRVraboteniPOL.Value:=2;
end;

end.
