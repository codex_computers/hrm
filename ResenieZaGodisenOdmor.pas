unit ResenieZaGodisenOdmor;

{*******************************************************}
{                                                       }
{     ��������� :  ������ �������                      }
{                                                       }
{     ����� : 25.08.2011                                }
{                                                       }
{     ������ : 1.1.1.14                                }
{                                                       }
{*******************************************************}


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDBLookupComboBox, cxGroupBox, cxRadioGroup,
  cxDropDownEdit, cxCalendar, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, frxDesgn,
  frxClass, frxDBSet, frxRich, frxCross, FIBQuery, pFIBQuery, cxLabel,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk,
  dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxScreenTip, cxSplitter, dxCustomHint,
  cxHint, dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dxSkinOffice2013White,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmResenijeZaGodisenOdmor = class(TForm)
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dPanel: TPanel;
    Label8: TLabel;
    Label6: TLabel;
    MB: TcxDBTextEdit;
    VRABOTENIME: TcxDBLookupComboBox;
    DatumKreiranje: TcxDBDateEdit;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    godina: TcxDBTextEdit;
    cxGroupBox3: TcxGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Datum2Od: TcxDBDateEdit;
    VtorDel: TcxDBTextEdit;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DatumOd: TcxDBDateEdit;
    PrvDel: TcxDBTextEdit;
    DatumDo: TcxDBDateEdit;
    Label10: TLabel;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    cxDBRadioGroup2: TcxDBRadioGroup;
    Label11: TLabel;
    Odobreno_od: TcxDBTextEdit;
    Datum2Do: TcxDBDateEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1PRV_DEL: TcxGridDBColumn;
    cxGrid1DBTableView1VTOR_DEL: TcxGridDBColumn;
    cxGrid1DBTableView1MB_RAKOVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1RMNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM1_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM1_DO: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM2_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM2_DO: TcxGridDBColumn;
    cxGrid1DBTableView1KoristenjeNaziv: TcxGridDBColumn;
    cxGroupBox4: TcxGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    STAZ: TcxDBTextEdit;
    Label14: TLabel;
    STEPEN_NA_SLOZENOST: TcxDBTextEdit;
    Label15: TLabel;
    ZDRASTVENA_SOSTOJBA: TcxDBTextEdit;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    Label16: TLabel;
    SUMAGO: TcxDBTextEdit;
    Label17: TLabel;
    Label18: TLabel;
    aPecati: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KORISTENJE: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1BARANJE_GO: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    Label21: TLabel;
    Broj: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    ZakonskiMinimum: TcxDBTextEdit;
    DenoviOdStaz: TcxDBTextEdit;
    SlozenostNaRabota: TcxDBTextEdit;
    Label22: TLabel;
    PosebniUsloviZaRabota: TcxDBTextEdit;
    Label23: TLabel;
    PovozrasniVrabotenii: TcxDBTextEdit;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    qZakonskiMinimum: TpFIBQuery;
    Label28: TLabel;
    mbzamena: TcxDBTextEdit;
    vrabotenZamena: TcxDBLookupComboBox;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1MB_ZAMENA: TcxGridDBColumn;
    cxGrid1DBTableView1ZAKONSKI_MINIMUM: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_DENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1STEPEN_NA_SLOZENOST: TcxGridDBColumn;
    cxGrid1DBTableView1POSEBNI_USLOVI: TcxGridDBColumn;
    cxGrid1DBTableView1POL_VOZRAST: TcxGridDBColumn;
    cxGrid1DBTableView1ZDRASTVENA_SOSTOJBA: TcxGridDBColumn;
    qVrabotenParam: TpFIBQuery;
    dxBarManager1Bar6: TdxBar;
    dxBarEdit1: TdxBarEdit;
    txtArhivskiBroj1: TcxBarEditItem;
    qMaxBroj: TpFIBQuery;
    dxBarEdit2: TdxBarEdit;
    cxBarEditItem2: TcxBarEditItem;
    CustomdxBarCombo1: TCustomdxBarCombo;
    cxBarEditItem3: TcxBarEditItem;
    Label29: TLabel;
    cuvanje_dete: TcxDBTextEdit;
    Label30: TLabel;
    cxGrid1DBTableView1SZ_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1CUVANJE_DETE: TcxGridDBColumn;
    Label31: TLabel;
    Invalid: TcxDBTextEdit;
    Label32: TLabel;
    CheckBox1: TCheckBox;
    txtArhivskiBroj: TcxTextEdit;
    cxSplitter1: TcxSplitter;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    dxBarLargeButton18: TdxBarLargeButton;
    aBrisiDokument: TAction;
    cxGrid1DBTableView1INVALID_DENOVI: TcxGridDBColumn;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    dxBarManager1Bar7: TdxBar;
    lcGodina: TcxBarEditItem;
    cxBarEditItem4: TcxBarEditItem;
    cxBarEditItem5: TcxBarEditItem;
    cGodina: TdxBarCombo;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    STAZ_MESECI: TcxDBTextEdit;
    Label33: TLabel;
    cxGrid1DBTableView1STAZ_MESECI: TcxGridDBColumn;
    aPrevzemiVraboten: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure t(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VRABOTENIMEExit(Sender: TObject);
    procedure cxDBRadioGroup2PropertiesEditValueChanged(Sender: TObject);
    procedure DatumDoExit(Sender: TObject);
    procedure DatumOdExit(Sender: TObject);
    procedure Datum2OdExit(Sender: TObject);
    procedure Datum2DoExit(Sender: TObject);
    procedure Odobreno_odPropertiesChange(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure DatumKreiranjeExit(Sender: TObject);
    procedure PrvDelExit(Sender: TObject);
    procedure VtorDelExit(Sender: TObject);
    procedure txtArhivskiBrojChange(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure txtArhivskiBroj1Exit(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure dxRibbon1TabChanged(Sender: TdxCustomRibbon);
    procedure dxBarLargeButton18Click(Sender: TObject);
    procedure dxBarLargeButton19Click(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure cGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure cGodinaChange(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure ZakonskiMinimumPropertiesChange(Sender: TObject);
    procedure aPrevzemiVrabotenExecute(Sender: TObject);
    procedure VRABOTENIMEPropertiesEditValueChanged(Sender: TObject);
    procedure MBPropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmResenijeZaGodisenOdmor: TfrmResenijeZaGodisenOdmor;
  arhivski_broj:string;
  maxbr1, maxbr2, tag_godina:Integer;
implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmUnitOtsustvo,
  dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmResenijeZaGodisenOdmor.CheckBox1Click(Sender: TObject);
begin
      if CheckBox1.Checked then
         dm.tblResenieGOCUVANJE_DETE.Value:= qZakonskiMinimum.FldByName['cuvanje_dete'].Value
      else
         dm.tblResenieGOCUVANJE_DETE.Value:= 0;
end;

constructor TfrmResenijeZaGodisenOdmor.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmResenijeZaGodisenOdmor.aNovExecute(Sender: TObject);
var vre, pom_zakonski_min:integer;
    vnaziv:string;
begin
//  if txtArhivskiBroj.is then cxGrid1.SetFocus;
  if txtArhivskiBroj.Text='' then
  begin
    frmDaNe := TfrmDaNe.Create(self, '������ ����', '������ ������� �������� ���. ���� ������ �� ����������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
           abort;
        end
       else
          begin
            arhivski_broj:='';
          end;
  end
  else
    arhivski_broj:=txtArhivskiBroj.Text;

  txtArhivskiBroj.Enabled:=False;
  CheckBox1.Visible:=True;
  CheckBox1.Checked:=False;
  pom_zakonski_min:=0;
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    txtArhivskiBroj.text:=arhivski_broj;
    if tag = 1 then
       begin
         dm.tblResenieGOMB.Value:=dm.tblBaranjeGOMB.Value;
         dm.tblResenieGODATUM.Value:=dm.tblBaranjeGODATUM.Value;
         dm.tblResenieGOBARANJE_GO.Value:=dm.tblBaranjeGOID.Value;
         dm.tblResenieGODATUM1_OD.AsString:=dm.tblBaranjeGODATUM1_OD.AsString;
         dm.tblResenieGODATUM1_DO.AsString:=dm.tblBaranjeGODATUM1_DO.AsString;
         dm.tblResenieGODATUM2_OD.AsString:=dm.tblBaranjeGODATUM2_OD.AsString;
         dm.tblResenieGODATUM2_DO.AsString:=dm.tblBaranjeGODATUM2_DO.AsString;
         dm.tblResenieGOPRV_DEL.Value:=dm.tblBaranjeGOPRV_DEL.Value;
         dm.tblResenieGOVTOR_DEL.Value:=dm.tblBaranjeGOVTOR_DEL.Value;
         dm.tblResenieGOGODINA.Value:=dm.tblBaranjeGOGODINA.Value;
         dm.tblResenieGOKORISTENJE.Value:=dm.tblBaranjeGOKORISTENJE.Value;
         dm.tblResenieGOMB_RAKOVODITEL.Value:=dm.tblBaranjeGOMB_RAKOVODITEL.Value;
         dm.tblResenieGOVID_DOKUMENT.Value:=resenieGO;
         aPrevzemiVraboten.Execute;
         mb.Enabled:=false;
         VRABOTENIME.Enabled:=false;
         Odobreno_od.SetFocus;
       end
    else
       begin
        MB.SetFocus;
        if cGodina.Text <> '' then
           dm.tblResenieGOGODINA.Value:=StrToInt(cGodina.Text)
        else
           dm.tblResenieGOGODINA.Value:=dmKon.godina;

        dm.tblResenieGOVID_DOKUMENT.Value:=resenieGO;
        dm.tblResenieGODATUM.Value:=Now;

        dm.tblResenieGOARHIVSKI_BROJ.value:=txtArhivskiBroj.text;
        qMaxBroj.Close;
        qMaxBroj.ParamByName('arhivski_broj').AsString:=txtArhivskiBroj.text;
        qMaxBroj.ExecQuery;
        dm.tblResenieGOBROJ.Value:=txtArhivskiBroj.text+'/'+inttostr(qMaxBroj.FldByName['maks'].AsInteger+1);
        pom_zakonski_min:=0;
       end
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmResenijeZaGodisenOdmor.aAzurirajExecute(Sender: TObject);
begin
  txtArhivskiBroj.Enabled:=False;
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    CheckBox1.Visible:=True;

    cxGrid1DBTableView1.DataController.DataSet.Edit;

    if dm.tblResenieGOCUVANJE_DETE.Value <> 0 then
       CheckBox1.Checked:=True
    else
       CheckBox1.Checked:=False;
    if tag = 1  then
       begin
         mb.Enabled:=false;
         VRABOTENIME.Enabled:=false;
         broj.SetFocus;
       end
    else
       prva.SetFocus;
    if dm.tblResenieGOKORISTENJE.Value = 1 then
       cxGroupBox3.Enabled:=false
    else
       cxGroupBox3.Enabled:=true;
    qZakonskiMinimum.Close;
    if DatumKreiranje.Text <> '' then
       qZakonskiMinimum.ParamByName('param').Value:= dm.tblResenieGODATUM.Value
    else
       qZakonskiMinimum.ParamByName('param').Value:= Now;
    qZakonskiMinimum.ExecQuery;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmResenijeZaGodisenOdmor.aBrisiDokumentExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteReseniGO.Close;
            dmOtsustvo.qDeleteReseniGO.ParamByName('id').Value:=dm.tblResenieGOID.Value;
            dmOtsustvo.qDeleteReseniGO.ExecQuery;
        end;
end;

procedure TfrmResenijeZaGodisenOdmor.aBrisiExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmResenijeZaGodisenOdmor.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmResenijeZaGodisenOdmor.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmResenijeZaGodisenOdmor.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmResenijeZaGodisenOdmor.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmResenijeZaGodisenOdmor.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmResenijeZaGodisenOdmor.cxDBRadioGroup2PropertiesEditValueChanged(
  Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) or (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit)then
       begin
         if dm.tblResenieGOKORISTENJE.Value = 2 then
            cxGroupBox3.Enabled:=true
         else
            begin
              Datum2Od.Clear;
              Datum2Do.Clear;
              VtorDel.Clear;
              cxGroupBox3.Enabled:=false;
            end;
       end;
end;

procedure TfrmResenijeZaGodisenOdmor.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmResenijeZaGodisenOdmor.t(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmResenijeZaGodisenOdmor.txtArhivskiBrojChange(Sender: TObject);
begin
//  if dm.tblResenieGO.State=dsInsert then
 // begin
    //povikaj procedura za max, spored arivskiot broj

    //smeni go abrojot

    //smeni go arhivskiot broj
 //   dm.tblResenieGOARHIVSKI_BROJ.Value:=txtArhivskiBroj.EditValue;
 // end;
end;

procedure TfrmResenijeZaGodisenOdmor.txtArhivskiBroj1Exit(Sender: TObject);
begin
 //  arhivski_broj:=txtArhivskiBroj.Caption;
  // PostMessage(Handle,WM_NextDlgCtl,0,0);
end;

procedure TfrmResenijeZaGodisenOdmor.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
   txtArhivskiBroj.text:=dm.tblResenieGOARHIVSKI_BROJ.AsString;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmResenijeZaGodisenOdmor.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmResenijeZaGodisenOdmor.VRABOTENIMEExit(Sender: TObject);
var vre:integer;
    vnaziv:string;
begin
      TEdit(Sender).Color:=clWhite;
end;

procedure TfrmResenijeZaGodisenOdmor.VRABOTENIMEPropertiesEditValueChanged(
  Sender: TObject);
begin
     aPrevzemiVraboten.Execute();
end;

procedure TfrmResenijeZaGodisenOdmor.VtorDelExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (Datum2Od.Text <> '') and (VtorDel.Text <> '') and (MB.Text <> '') then
        begin
          dm.tblResenieGODATUM2_DO.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'OD_DATUM', 'DENOVI', 'MB', Null, dm.tblResenieGODATUM2_OD.Value, dm.tblResenieGOVTOR_DEL.Value, dm.tblResenieGOMB.Value, Null, 'DO_DATUM');
        end;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmResenijeZaGodisenOdmor.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmResenijeZaGodisenOdmor.MBPropertiesEditValueChanged(
  Sender: TObject);
begin
     aPrevzemiVraboten.Execute;
end;

procedure TfrmResenijeZaGodisenOdmor.Odobreno_odPropertiesChange(
  Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmResenijeZaGodisenOdmor.prefrli;
begin
end;

procedure TfrmResenijeZaGodisenOdmor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmResenijeZaGodisenOdmor.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
  pom_tab_resGO:=0;
end;

//------------------------------------------------------------------------------

procedure TfrmResenijeZaGodisenOdmor.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

    tag_godina:= 1;
  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

   // pom_tab_resGO:=1;

    if tag = 1 then
     begin
        dm.tblResenieGO.close;
        dm.tblResenieGO.ParamByName('firma').Value:=dmKon.re;
        dm.tblResenieGO.ParamByName('RE').Value:='%';
        dm.tblResenieGO.ParamByName('MB').Value:='%';
        dm.tblResenieGO.ParamByName('bgo').Value:=dm.tblBaranjeGOID.Value;
        dm.tblResenieGO.open;
     end
  else
     begin
        dm.tblResenieGO.close;
        dm.tblResenieGO.ParamByName('firma').Value:=dmKon.re;
        dm.tblResenieGO.ParamByName('RE').Value:='%';
        dm.tblResenieGO.ParamByName('MB').Value:='%';
        dm.tblResenieGO.ParamByName('bgo').Value:='%';
        dm.tblResenieGO.open;
     end;
  pom_tab:=1;
  cGodina.Text:=IntToStr(dmKon.godina);

  cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(cGodina.Text), cGodina.Text);
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;

   dm.viewVraboteni.Close;
   dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
   dm.viewVraboteni.ParamByName('mb').Value:= '%';
   dm.viewVraboteni.ParamByName('re').Value:='%';
   dm.viewVraboteni.Open;
   // txtArhivskiBroj.EditValue:=dm.tblResenieGOARHIVSKI_BROJ.Value;
end;
//------------------------------------------------------------------------------

procedure TfrmResenijeZaGodisenOdmor.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmResenijeZaGodisenOdmor.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmResenijeZaGodisenOdmor.Datum2DoExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (Datum2Od.Text <> '') and (Datum2Do.Text <> '') then
        begin
          dm.tblResenieGOvtor_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblResenieGODATUM2_OD.Value, dm.tblResenieGODATUM2_DO.Value, dm.tblResenieGOMB.Value,Null, 'DENOVI');
        end;
end;

procedure TfrmResenijeZaGodisenOdmor.Datum2OdExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (Datum2Od.Text <> '') and (Datum2Do.Text <> '') then
        begin
          //dm.tblResenieGOvtor_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblResenieGODATUM2_OD.Value, dm.tblResenieGODATUM2_DO.Value + 1, dm.tblResenieGOMB.Value,Null, 'DENOVI');
          dm.tblResenieGOvtor_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblResenieGODATUM2_OD.Value, dm.tblResenieGODATUM2_DO.Value, dm.tblResenieGOMB.Value,Null, 'DENOVI');
        end
end;

procedure TfrmResenijeZaGodisenOdmor.DatumDoExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (DatumOd.Text <> '') and (DatumDo.Text <> '') then
        begin
          dm.tblResenieGOPRV_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblResenieGODATUM1_OD.Value, dm.tblResenieGODATUM1_DO.Value, dm.tblResenieGOMB.Value,Null, 'DENOVI');
        end;
end;

procedure TfrmResenijeZaGodisenOdmor.DatumKreiranjeExit(Sender: TObject);
begin
   TEdit(Sender).Color:=clWhite;
   aPrevzemiVraboten.Execute();

end;

procedure TfrmResenijeZaGodisenOdmor.DatumOdExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (DatumOd.Text <> '') and (DatumDo.Text <> '') then
        begin
          //dm.tblResenieGOPRV_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblResenieGODATUM1_OD.Value, dm.tblResenieGODATUM1_DO.Value + 1, dm.tblResenieGOMB.Value, Null,'DENOVI');
          dm.tblResenieGOPRV_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblResenieGODATUM1_OD.Value, dm.tblResenieGODATUM1_DO.Value, dm.tblResenieGOMB.Value, Null,'DENOVI');
        end;
end;

procedure TfrmResenijeZaGodisenOdmor.dxBarLargeButton18Click(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteReseniGO.Close;
            dmOtsustvo.qDeleteReseniGO.ParamByName('id').Value:=dm.tblResenieGOID.Value;
            dmOtsustvo.qDeleteReseniGO.ExecQuery;
        end;
end;

procedure TfrmResenijeZaGodisenOdmor.dxBarLargeButton19Click(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteReseniGO.Close;
            dmOtsustvo.qDeleteReseniGO.ParamByName('id').Value:=dm.tblResenieGOID.Value;
            dmOtsustvo.qDeleteReseniGO.ExecQuery;
        end;
end;

procedure TfrmResenijeZaGodisenOdmor.dxRibbon1TabChanged(
  Sender: TdxCustomRibbon);
begin
      if pom_tab_resGO <> 0 then
         begin
          if dxRibbon1.ActiveTab = dxRibbon1Tab2 then
            begin
               txtArhivskiBroj.Visible:=False;
               //cGodina.Visible:=false;
            end
          else
            begin
               txtArhivskiBroj.Visible:=True;
               //cGodina.Visible:=True;
            end;
         end;
end;

procedure TfrmResenijeZaGodisenOdmor.dxRibbon1TabChanging(
  Sender: TdxCustomRibbon; ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
     
end;

//  ����� �� �����
procedure TfrmResenijeZaGodisenOdmor.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  pom: Integer;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
       if (DatumOd.Text <> '') and (DatumDo.Text <> '') and (DatumDo.Date < DatumOd.date) then
         begin
           ShowMessage('������ � �������� ������ !!!');
           DatumDo.SetFocus;
         end
      else if (Datum2Od.Text <> '') and (Datum2Do.Text <> '') and (Datum2Do.Date < Datum2Od.date) then
         begin
           ShowMessage('������ � �������� ������ !!!');
           Datum2Do.SetFocus;
         end
      else
      begin
        pom:= dm.tblResenieGOZAKONSKI_MINIMUM.Value +
              dm.tblResenieGOSTAZ_DENOVI.Value +
              dm.tblResenieGOSTEPEN_NA_SLOZENOST.Value +
              dm.tblResenieGOPOSEBNI_USLOVI.Value +
              dm.tblResenieGOPOL_VOZRAST.Value +
              dm.tblResenieGOZDRASTVENA_SOSTOJBA.Value +
              dm.tblResenieGOCUVANJE_DETE.Value+
              dm.tblResenieGOINVALID_DENOVI.Value;

        if pom <= qZakonskiMinimum.FldByName['max_denovi_go1'].value then
           begin
              CheckBox1.Visible:=False;
              dm.tblResenieGOVKUPNO_DENOVI.Value:=pom;
              cxGrid1DBTableView1.DataController.DataSet.Post;
              cxGroupBox3.Enabled:=false;
              dPanel.Enabled:=false;
              lPanel.Enabled:=true;
              cxGrid1.SetFocus;
              txtArhivskiBroj.Enabled:=True;
           end
        else
           if ((dm.tblResenieGOINVALID_DENOVI.Value <> 0) or (dm.tblResenieGOCUVANJE_DETE.Value <> 0) or (dm.tblResenieGOZDRASTVENA_SOSTOJBA.Value <> 0) or (dm.tblResenieGOPOL_VOZRAST.Value <> 0)) then
             begin
                if pom <= qZakonskiMinimum.FldByName['max_denovi_go2'].value then
                   begin
                      CheckBox1.Visible:=False;
                      dm.tblResenieGOVKUPNO_DENOVI.Value:=pom;
                      cxGrid1DBTableView1.DataController.DataSet.Post;
                      dm.tblResenieGO.FullRefresh;
                      cxGroupBox3.Enabled:=false;

                      dPanel.Enabled:=false;
                      lPanel.Enabled:=true;
                      cxGrid1.SetFocus;
                      txtArhivskiBroj.Enabled:=True;
                   end
                else
                   begin
                      frmDaNe := TfrmDaNe.Create(self, '����� �� ��������', '�������� ��� �� ������ �� ������� ����� � ������� �� '+IntToStr(maxbr2)+' !!! ���� ������ �� ������ ��� �� ������ �� �� ���� '+IntToStr(maxbr2)+' ?', 1);
                      if (frmDaNe.ShowModal <> mrYes) then
                         mb.SetFocus
                      else
                         begin
                           CheckBox1.Visible:=False;
                           dm.tblResenieGOVKUPNO_DENOVI.Value:=qZakonskiMinimum.FldByName['max_denovi_go2'].value;
                           cxGrid1DBTableView1.DataController.DataSet.Post;
                           cxGroupBox3.Enabled:=false;

                           dPanel.Enabled:=false;
                           lPanel.Enabled:=true;
                           cxGrid1.SetFocus;
                           txtArhivskiBroj.Enabled:=True;
                         end;
                   end
             end
           else
             begin
                frmDaNe := TfrmDaNe.Create(self, '����� �� ��������', '�������� ��� �� ������ �� ������� ����� � ������� �� '+IntToStr(maxbr1)+' !!! ���� ������ �� ������ ��� �� ������ �� �� ���� '+IntToStr(maxbr1)+' ?', 1);
                      if (frmDaNe.ShowModal <> mrYes) then
                         mb.SetFocus
                      else
                         begin
                           CheckBox1.Visible:=False;
                           dm.tblResenieGOVKUPNO_DENOVI.Value:=qZakonskiMinimum.FldByName['max_denovi_go1'].value;
                           cxGrid1DBTableView1.DataController.DataSet.Post;
                           cxGroupBox3.Enabled:=false;

                           dPanel.Enabled:=false;
                           lPanel.Enabled:=true;
                           cxGrid1.SetFocus;
                           txtArhivskiBroj.Enabled:=True;
                         end;
             end;
      end;
    end;
  end;
end;

procedure TfrmResenijeZaGodisenOdmor.cGodinaChange(Sender: TObject);
begin
   if ((cGodina.Text = '����') and (tag_godina = 1)) then
         cxGrid1DBTableView1.DataController.Filter.Clear
     else if cGodina.Text <> '' then
      begin
          cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(cGodina.Text), cGodina.Text);
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
     end;
end;

procedure TfrmResenijeZaGodisenOdmor.cGodinaPropertiesEditValueChanged(
  Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmResenijeZaGodisenOdmor.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      CheckBox1.Checked:=false;
      CheckBox1.Visible:=False;
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);

      cxGroupBox3.Enabled:=false;
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
      txtArhivskiBroj.Enabled:=True;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmResenijeZaGodisenOdmor.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmResenijeZaGodisenOdmor.aPecatiExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;
//
  dmReport.ResenieZaGO.close;
  dmReport.ResenieZaGO.ParamByName('id').Value:=dm.tblResenieGOID.Value;
  dmReport.ResenieZaGO.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.ResenieZaGODATA.IsNull then
     begin
       RptStream := dmReport.ResenieZaGO.CreateBlobStream(dmReport.ResenieZaGODATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
        begin
           RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
           dmReport.frxReport1.LoadFromStream(RptStream);

            RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
            If RichView <> Nil Then
              begin
                RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
                RichView.DataSet:=dmReport.frxResenieGO;
              end;
            MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
            if MasterData <> Nil then
              MasterData.DataSet:=dmReport.frxResenieGO;

            dmReport.frxReport1.ShowReport;

            frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
            if (frmDaNe.ShowModal = mrYes) then
              dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 3);
         end;
     end;
end;

procedure TfrmResenijeZaGodisenOdmor.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + cGodina.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmResenijeZaGodisenOdmor.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmResenijeZaGodisenOdmor.aPrevzemiVrabotenExecute(Sender: TObject);
var vre,pom_zakonski_min,iskoristeni_go:Integer;
    vnaziv:string;
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsInsert)or (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit))  then
     begin
       if ((MB.Text<>'')and (VRABOTENIME.Text<>'')) then
           begin
             if dm.tblResenieGODATUM.isNull then
                dm.tblResenieGODATUM.Value:=Now;

             dm.tblVraboteniRGO.Close;
             dm.tblVraboteniRGO.ParamByName('firma').Value:=dmKon.re;
             dm.tblVraboteniRGO.ParamByName('mb').Value:= MB.Text;
             dm.tblVraboteniRGO.ParamByName('re').Value:='%';
             dm.tblVraboteniRGO.ParamByName('param_datum').Value:=dm.tblResenieGODATUM.Value;
             dm.tblVraboteniRGO.Open;

             vre:=dm.tblVraboteniRGORE.Value;



             qZakonskiMinimum.Close;
             qZakonskiMinimum.ParamByName('param').Value:= dm.tblResenieGODATUM.Value;
             qZakonskiMinimum.ExecQuery;

             maxbr1:= qZakonskiMinimum.FldByName['max_denovi_go1'].value;
             maxbr2:= qZakonskiMinimum.FldByName['max_denovi_go2'].value;
             if (not qZakonskiMinimum.FldByName['denovi_odmor'].IsNull)  then
                dm.tblResenieGOZAKONSKI_MINIMUM.Value:=qZakonskiMinimum.FldByName['denovi_odmor'].value;

             vnaziv:=dm.tblVraboteniRGORABOTNAEDINICANAZIV.Value;
             dm.tblRe.Locate('RE; NAZIV', VarArrayOf([vre,vnaziv]) , []);
             dm.tblResenieGOMB_RAKOVODITEL.Value:= dm.tblReRAKOVODITEL.Value;

             dm.tblResenieGOSTAZ.Value:=dm.tblVraboteniRGOSTAZ.Value;
             dm.tblResenieGOSTAZ_MESECI.Value:=dm.tblVraboteniRGOSTAZ_MESECI.Value;

             if (dm.tblResenieGOSTAZ.Value =0)and (dm.tblResenieGOSTAZ_MESECI.Value < StrToInt(min_den_go_v1)) then
                 dm.tblResenieGOZAKONSKI_MINIMUM.Value:=dm.tblResenieGOSTAZ_MESECI.Value*StrToInt(min_den_go_v2);

             //tuka od zakonskiot minimum odzemam iskoristeni denovi preku resenie od tekovna godina
             iskoristeni_go:=dmOtsustvo.zemiBroj(dmOtsustvo.pOstanatiGOGodina, 'GODINA', 'MB', Null,Null, dm.tblResenieGOGODINA.Value, dm.tblResenieGOMB.Value, Null,Null, 'ISKORISTENI_OUT');
             if iskoristeni_go>0 then
                dm.tblResenieGOZAKONSKI_MINIMUM.Value:=dm.tblResenieGOZAKONSKI_MINIMUM.Value - iskoristeni_go;
             if (dm.tblResenieGOSTAZ.Value > 0)and (dm.tblResenieGOSTAZ.Value <= 10) then
                 dm.tblResenieGOSTAZ_DENOVI.Value:= qZakonskiMinimum.FldByName['go_staz_1'].Value
             else if (dm.tblResenieGOSTAZ.Value > 10)and (dm.tblResenieGOSTAZ.Value <= 20) then
                 dm.tblResenieGOSTAZ_DENOVI.Value:= qZakonskiMinimum.FldByName['go_staz_2'].Value
             else if (dm.tblResenieGOSTAZ.Value > 20) then
                 dm.tblResenieGOSTAZ_DENOVI.Value:= qZakonskiMinimum.FldByName['go_staz_3'].Value
             else
                 dm.tblResenieGOSTAZ_DENOVI.Value:=0;

             dm.tblResenieGOSTEPEN_NA_SLOZENOST.Value:= dm.tblVraboteniRGOSZODNENOVIGO.Value;
             dm.tblResenieGOPOSEBNI_USLOVI.Value:= dm.tblVraboteniRGODEN_USLOVI_RABOTA.Value;
             dm.tblResenieGOPOL_VOZRAST.Value:=dm.tblVraboteniRGOpol_vozrast.Value;
             dm.tblResenieGOZDRASTVENA_SOSTOJBA.Value:=dm.tblVraboteniRGOtelesno_ostetuvanje.Value;
             dm.tblResenieGOSZ_NAZIV.Value:=dm.tblVraboteniRGOSZO_NAZIV.Value;
             if (dm.tblVraboteniRGOINVALID.Value = 1) then
               dm.tblResenieGOINVALID_DENOVI.Value:=qZakonskiMinimum.FldByName['INVALID_DENOVI'].Value
             else
               dm.tblResenieGOINVALID_DENOVI.Value:=0;
           end;
     end;
end;

procedure TfrmResenijeZaGodisenOdmor.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmResenijeZaGodisenOdmor.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;


//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmResenijeZaGodisenOdmor.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmResenijeZaGodisenOdmor.aHelpExecute(Sender: TObject);
begin
      Application.HelpContext(122);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmResenijeZaGodisenOdmor.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

procedure TfrmResenijeZaGodisenOdmor.ZakonskiMinimumPropertiesChange(
  Sender: TObject);
begin

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmResenijeZaGodisenOdmor.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


procedure TfrmResenijeZaGodisenOdmor.PrvDelExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (DatumOd.Text <> '') and (PrvDel.Text <> '') and (MB.Text <> '') then
        begin
          dm.tblResenieGODATUM1_DO.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'OD_DATUM', 'DENOVI', 'MB', Null, dm.tblResenieGODATUM1_OD.Value, dm.tblResenieGOPRV_DEL.Value, dm.tblResenieGOMB.Value, Null, 'DO_DATUM');
        end;

end;

end.
