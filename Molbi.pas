unit Molbi;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                   }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxImage, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMemo, cxMaskEdit,
  cxCalendar, cxGroupBox, cxRadioGroup, ExtDlgs, FIBDataSet, pFIBDataSet, cxHint,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxSplitter, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk;

type
  TfrmMolbi = class(TfrmMaster)
    Label12: TLabel;
    ID_RM_RE: TcxDBTextEdit;
    ID_RM_RE_NAZIV: TcxDBLookupComboBox;
    Label2: TLabel;
    ID_OGLASNaziv: TcxDBLookupComboBox;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1MOMINSKO_PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1POL: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_RADJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1BRACNA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1ZDR_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1SLIKA: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1NACIONALNOSTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VEROISPOVEDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJNAOGLAS: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    OpenPictureDialog1: TOpenPictureDialog;
    aObrazovanieObuka: TAction;
    N2: TMenuItem;
    aLVK: TAction;
    N3: TMenuItem;
    aRabotnoIskustvo: TAction;
    N4: TMenuItem;
    aKontakt: TAction;
    N5: TMenuItem;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    ActionList2: TActionList;
    aDosie: TAction;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    dsRMRE: TDataSource;
    N6: TMenuItem;
    dxBarManager1Bar6: TdxBar;
    aDosieDizajn: TAction;
    MOLBABROJ: TcxDBTextEdit;
    Label13: TLabel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    tblRMRERENAZIV: TFIBStringField;
    dsOglas: TDataSource;
    tblOglas: TpFIBDataSet;
    tblOglasID: TFIBIntegerField;
    tblOglasDATUM_OBJAVEN: TFIBDateField;
    tblOglasDATUM_ISTEKUVA: TFIBDateField;
    tblOglasOBJAVEN: TFIBStringField;
    tblOglasPRIJAVUVANJE_DO: TFIBDateField;
    tblOglasOPIS: TFIBStringField;
    tblOglasSTATUS: TFIBIntegerField;
    tblOglasPROMENET_OD: TFIBStringField;
    tblOglasTS_INS: TFIBDateTimeField;
    tblOglasTS_UPD: TFIBDateTimeField;
    tblOglasUSR_INS: TFIBStringField;
    tblOglasUSR_UPD: TFIBStringField;
    tblOglasGODINA: TFIBIntegerField;
    tblOglasstatusNaziv: TFIBStringField;
    cxHintStyleController1: TcxHintStyleController;
    cxGroupBox1: TcxGroupBox;
    Label4: TLabel;
    MB: TcxDBTextEdit;
    Label3: TLabel;
    PREZIME: TcxDBTextEdit;
    Label6: TLabel;
    IME: TcxDBTextEdit;
    Label5: TLabel;
    MOMINSKO_PREZIME: TcxDBTextEdit;
    Label7: TLabel;
    DATUM_RADJANJE: TcxDBDateEdit;
    BracnaSostojba: TcxDBRadioGroup;
    Pol: TcxDBRadioGroup;
    Label9: TLabel;
    Label8: TLabel;
    ZDR_SOSTOJBA: TcxDBMemo;
    Label10: TLabel;
    NACIONALNOSTID: TcxDBTextEdit;
    NACIONALNOST: TcxDBLookupComboBox;
    VEROISPOVED: TcxDBLookupComboBox;
    VEROISPOVEDID: TcxDBTextEdit;
    Label11: TLabel;
    Slika: TcxDBImage;
    Button1: TButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1ID_OGLAS: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ID_NACIONALNOST: TcxGridDBColumn;
    cxGrid1DBTableView1ID_VEROISPOVED: TcxGridDBColumn;
    cxGrid1DBTableView1BRACNA_SOSTOJBA1: TcxGridDBColumn;
    SplitterdPanel: TcxSplitter;
    tblOglasID_RE_FIRMA: TFIBIntegerField;
    tblOglasBROJ: TFIBIntegerField;
    tblOglasBROJGODINA: TFIBStringField;
    DatumPodnesuvanje: TcxDBDateEdit;
    Label14: TLabel;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    procedure Button1Click(Sender: TObject);
    procedure aObrazovanieObukaExecute(Sender: TObject);
    procedure aLVKExecute(Sender: TObject);
    procedure aRabotnoIskustvoExecute(Sender: TObject);
    procedure aKontaktExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aDosieExecute(Sender: TObject);
    procedure MBExit(Sender: TObject);
    procedure aDosieDizajnExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ID_OGLASNazivExit(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure ID_OGLASNazivPropertiesEditValueChanged(Sender: TObject);
    function proverkaModul11(broj: Int64): boolean;
    procedure MBPropertiesEditValueChanged(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMolbi: TfrmMolbi;
  StateActive:TDataSetState;

implementation

uses dmKonekcija, dmMaticni, dmSistematizacija, Utils, dmUnitOtsustvo,
  ObrazovanieObuka, VestiniKvalifikacii, IskustvoRabotno, Kontakt, Nacionalnost,
  Veroispoved, dmUnit, dmResources, DaNe, dmReportUnit;

{$R *.dfm}
procedure Split(const Delimiter: Char; Input: string; const Strings: TStrings) ;
begin
end;
procedure TfrmMolbi.aAzurirajExecute(Sender: TObject);
begin
     tblOglas.Close;
     tblOglas.ParamByName('statusID').Value:='1';
    // tblOglas.ParamByName('firma').AsInteger:=firma;
     tblOglas.Open;
     if (tag = 1) and (dmSis.tblOglasSTATUS.Value=0)  then
         ShowMessage('������� �� � �������!')
     else
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          dPanel.Enabled:=True;
          SplitterdPanel.OpenSplitter;
          lPanel.Enabled:=False;
          tblRMRE.ParamByName('firma').Value:=firma;
          tblRMRE.ParamByName('datum').Value:='11.11.1111';
          tblRMRE.FullRefresh;
          if tag = 1 then
            mb.SetFocus
          else
            ID_OGLASNaziv.SetFocus;
          cxGrid1DBTableView1.DataController.DataSet.Edit;
          if tag =1  then
             begin
               tblRMRE.ParamByName('oglasID').Value:=ID_OGLASNaziv.EditValue;
               tblRMRE.FullRefresh;
             end
          else
          if ID_OGLASNaziv.Text <> '' then
             begin
                tblRMRE.ParamByName('oglasID').Value:=ID_OGLASNaziv.EditValue;
                tblRMRE.FullRefresh;
              end;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmMolbi.aDosieDizajnExecute(Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30073;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblLicniPodatoci.Close;
     dm.tblLicniPodatoci.ParamByName('ID').Value:=dmSis.tblMolbaID.Value;
     dm.tblLicniPodatoci.open;

     dmSis.tblRabotnoIskustvo.Close;
     dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:='0';
     dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
     dmSis.tblRabotnoIskustvo.Open;

     dmSis.tblObrazovanieObuka.Close;
     dmSis.tblObrazovanieObuka.ParamByName('MB').Value:='0';
     dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
     dmSis.tblObrazovanieObuka.Open;

     dmSis.tblLVK.Close;
     dmSis.tblLVK.ParamByName('MB').Value:='0';
     dmSis.tblLVK.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
     dmSis.tblLVK.open;

     dm.tblLVKSJ.Close;
     dm.tblLVKSJ.ParamByName('molbaID').Value:=dmSis.tblMolbaID.Value;
     dm.tblLVKSJ.ParamByName('MB').Value:='0';
     dm.tblLVKSJ.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
//     dmReport.frxReport1.ShowReport;
end;

procedure TfrmMolbi.aDosieExecute(Sender: TObject);
  var RptStream :TStream;
begin

     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30073;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblLicniPodatoci.Close;
     dm.tblLicniPodatoci.ParamByName('ID').Value:=dmSis.tblMolbaID.Value;
     dm.tblLicniPodatoci.open;

     dmSis.tblRabotnoIskustvo.Close;
     dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:='0';
     dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
     dmSis.tblRabotnoIskustvo.Open;

     dmSis.tblObrazovanieObuka.Close;
     dmSis.tblObrazovanieObuka.ParamByName('MB').Value:='0';
     dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
     dmSis.tblObrazovanieObuka.Open;

     dmSis.tblLVK.Close;
     dmSis.tblLVK.ParamByName('MB').Value:='0';
     dmSis.tblLVK.ParamByName('MolbaID').Value:=dmSis.tblMolbaID.Value;
     dmSis.tblLVK.open;

     dm.tblLVKSJ.Close;
     dm.tblLVKSJ.ParamByName('molbaID').Value:=dmSis.tblMolbaID.Value;
     dm.tblLVKSJ.ParamByName('MB').Value:='0';
     dm.tblLVKSJ.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmMolbi.aHelpExecute(Sender: TObject);
begin
  inherited;
   Application.HelpContext(132);
end;

procedure TfrmMolbi.aKontaktExecute(Sender: TObject);
begin
  inherited;
  frmKontakt:=TfrmKontakt.Create(Application);
  frmKontakt.Tag:=5;
  frmKontakt.ShowModal;
  frmKontakt.Free;
end;

procedure TfrmMolbi.aLVKExecute(Sender: TObject);
begin
  inherited;
  frmVestiniKvalifikacii:=TfrmVestiniKvalifikacii.Create(Application);
  frmVestiniKvalifikacii.Tag:=5;
  frmVestiniKvalifikacii.ShowModal;
  frmVestiniKvalifikacii.Free;
end;

procedure TfrmMolbi.aNovExecute(Sender: TObject);
begin
     tblOglas.Close;
     tblOglas.ParamByName('statusID').Value:='1';
     //tblOglas.ParamByName('firma').AsInteger:=firma;
     tblOglas.Open;
     if (tag = 1) and (dmSis.tblOglasSTATUS.Value=0) then
         ShowMessage('������� �� � �������!')
     else
          begin
             if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
                begin
                  dPanel.Enabled:=True;
                  SplitterdPanel.OpenSplitter;
                  lPanel.Enabled:=False;
                  tblRMRE.ParamByName('firma').Value:=firma;
                  tblRMRE.ParamByName('datum').Value:='11.11.1111';
                  tblRMRE.FullRefresh;
                  if tag = 1 then
                     MB.SetFocus
                  else
                     ID_OGLASNaziv.SetFocus;
                  cxGrid1DBTableView1.DataController.DataSet.Insert;
                  dmSis.tblMolbaMOLBABROJ.Value:=dmMat.zemiMax(dmsis.pMaxBrMolba,Null, Null, Null, Null, Null, Null, 'MAXBR');
                  dmSis.tblMolbaDATUM.Value:=Now;
                  if tag =1  then
                     begin
                       dmSis.tblMolbaID_OGLAS.Value:=dmSis.tblOglasID.Value;
                       tblRMRE.ParamByName('oglasID').Value:=ID_OGLASNaziv.EditValue;
                       tblRMRE.FullRefresh;
                     end;
                end
             else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
          end;
end;

procedure TfrmMolbi.aObrazovanieObukaExecute(Sender: TObject);
begin
  inherited;
  frmObrazovanieObuka:=TfrmObrazovanieObuka.Create(Application);
  frmObrazovanieObuka.Tag:=5;
  frmObrazovanieObuka.ShowModal;
  frmObrazovanieObuka.Free;
end;

procedure TfrmMolbi.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  tblOglas.ParamByName('statusID').Value:='%';
  tblOglas.FullRefresh;
  tblrmre.ParamByName('firma').Value:='%';
  tblrmre.ParamByName('datum').Value:='%';
  tblrmre.FullRefresh;
end;

procedure TfrmMolbi.aRabotnoIskustvoExecute(Sender: TObject);
begin
  inherited;
  frmIskustvoRabotno:=TfrmIskustvoRabotno.Create(Application);
  frmIskustvoRabotno.Tag:=5;
  frmIskustvoRabotno.ShowModal;
  frmIskustvoRabotno.Free;
end;

procedure TfrmMolbi.aZacuvajExcelExecute(Sender: TObject);
begin
     if tag = 0 then
        zacuvajVoExcel(cxGrid1, Caption)
     else
        zacuvajVoExcel(cxGrid1, '�����������������������');
end;

procedure TfrmMolbi.aZapisiExecute(Sender: TObject);
var  st: TDataSetState;
begin
     if ID_RM_RE_NAZIV.Text = '' then
        ID_RM_RE.Text:= '';
     st := cxGrid1DBTableView1.DataController.DataSet.State;
     if st in [dsEdit,dsInsert] then
        if (Validacija(dPanel) = false) then
           begin
             cxGrid1DBTableView1.DataController.DataSet.Post;
             tblOglas.Close;
             tblOglas.ParamByName('statusID').Value:='%';
             //tblOglas.ParamByName('firma').AsInteger:=firma;
             tblOglas.Open;
             dPanel.Enabled:=false;
             lPanel.Enabled:=true;
             tblrmre.ParamByName('firma').Value:='%';
             tblrmre.ParamByName('datum').Value:='%';
             tblrmre.FullRefresh;
             cxGrid1.SetFocus;
           end;
end;

procedure TfrmMolbi.Button1Click(Sender: TObject);
begin
  inherited;
  OpenPictureDialog1.Filter :=  'JPEG Image File |*.jpg;*.jpeg';
  OpenPictureDialog1.Execute();
  if OpenPictureDialog1.FileName <> '' then
     dmSis.tblMolbaSLIKA.LoadFromFile(OpenPictureDialog1.FileName);

end;

procedure TfrmMolbi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
       VK_INSERT:begin
         if (Sender = NACIONALNOSTID) or (Sender = NACIONALNOST) then
            begin
              frmNacionalnost:=TfrmNacionalnost.Create(Application);
              frmNacionalnost.Tag:=1;
              frmNacionalnost.ShowModal;
              frmNacionalnost.Free;
              dmsis.tblmolbaID_NACIONALNOST.Value:=dmOtsustvo.tblNacionalnostID.Value;
            end;

         if (Sender = VEROISPOVED) or (Sender = VEROISPOVEDID) then
            begin
              frmVeroispoved:=TfrmVeroispoved.Create(Application);
              frmVeroispoved.Tag:=1;
              frmVeroispoved.ShowModal;
              frmVeroispoved.Free;
              dmsis.tblmolbaID_VEROISPOVED.Value:=dmOtsustvo.tblVeroispovedID.Value;
            end;
       end;
  end;
end;

procedure TfrmMolbi.FormShow(Sender: TObject);
begin
  inherited;
   if tag = 1  then
      begin
        dmSis.tblMolba.Close;
        dmSis.tblMolba.ParamByName('ID').Value:='%';
        dmsis.tblMolba.ParamByName('oglasID').Value:=dmsis.tblOglasID.Value;
//        dmsis.tblMolba.ParamByName('firma').AsInteger:=firma;
        dmSis.tblMolba.Open;

        tblrmre.Close;
        tblrmre.ParamByName('oglasID').Value:=dmsis.tblOglasID.Value;
        tblrmre.ParamByName('firma').Value:='%';
        tblrmre.ParamByName('datum').Value:='%';
        tblRMRE.Open;

        tblOglas.Close;
        tblOglas.ParamByName('statusID').Value:='%';
        //tblOglas.ParamByName('firma').AsInteger:=firma;
        tblOglas.Open;
      end
   else
      begin
        dmSis.tblMolba.Close;
        dmSis.tblMolba.ParamByName('ID').Value:='%';
        dmsis.tblMolba.ParamByName('oglasID').Value:='%';
        //dmsis.tblMolba.ParamByName('firma').AsInteger:=firma;
        dmSis.tblMolba.Open;

        tblrmre.Close;
        tblrmre.ParamByName('oglasID').Value:='%';
        tblrmre.ParamByName('firma').Value:='%';
        tblrmre.ParamByName('datum').Value:='%';
        tblRMRE.Open;

        tblOglas.Close;
        tblOglas.ParamByName('statusID').Value:='%';
       // tblOglas.ParamByName('firma').AsInteger:=firma;
        tblOglas.Open;
      end;
   dmOtsustvo.tblNacionalnost.Open;
   dmOtsustvo.tblVeroispoved.Open;
  if tag = 1 then
     begin
     //  ID_OGLASNaziv.Enabled:=False;
     end;
end;

procedure TfrmMolbi.ID_OGLASNazivExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmMolbi.ID_OGLASNazivPropertiesEditValueChanged(Sender: TObject);
var pom:integer;
begin
   if (ID_OGLASNaziv.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
       begin
         tblRMRE.ParamByName('oglasID').Value:=ID_OGLASNaziv.EditValue;
         tblRMRE.FullRefresh;
         if tblRMRE.IsEmpty then
             begin
               showMessage('�� ��� ����� ���� ������������ ������� ����� !!!');
               ID_OGLASNaziv.SetFocus;
               ID_RM_RE_NAZIV.Text:='';
               ID_RM_RE.Text:='';
             end;
       end
   else
     begin
       tblRMRE.ParamByName('oglasID').Value:='%';
       tblRMRE.FullRefresh;
     end;
end;

function TfrmMolbi.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;

procedure TfrmMolbi.MBExit(Sender: TObject);
begin
   TEdit(Sender).Color:=clWhite;
end;

procedure TfrmMolbi.MBPropertiesEditValueChanged(Sender: TObject);
var pom, date:string;
begin
  if (mb.Text <> '')  and ((cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse)) then
      begin
        if proverkaModul11(StrToInt64(MB.Text))then
          begin
            pom:=dmSis.tblMolbaMB.Value;
            date:= pom[1] + pom[2] + '.' + pom[3] + pom[4] + '.';
            if StrToInt(pom[5]) = 9 then
              date:=date + '1'
            else if StrToInt(pom[5]) in [0,1]then
              date:=date + '2';
            date:=date + pom[5] + pom[6]+ pom[7];
            dmSis.tblMolbaDATUM_RADJANJE.Value:=StrToDate(date);
            if strtoint(pom[10]) in [0,1,2,3,4] then
              dmSis.tblMolbaPOL.Value:=1
            else if strtoint(pom[10]) in [5,6,7,8,9] then
              dmSis.tblMolbaPOL.Value:=2
          end
        else
          begin
            ShowMessage('��������� ������� ��� �� � ������� !!!');
          end;
      end;
end;

end.
