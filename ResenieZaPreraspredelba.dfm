object frmReseniePreraspredelba: TfrmReseniePreraspredelba
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 
    #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1088#1072#1089#1087#1086#1088#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1086#1076' '#1077#1076#1085#1086' '#1085#1072' '#1076#1088#1091#1075#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077 +
    #1089#1090#1086
  ClientHeight = 685
  ClientWidth = 1002
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1002
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 662
    Width = 1002
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048 +
          #1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dPanel: TPanel
    Left = 0
    Top = 360
    Width = 1002
    Height = 302
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Enabled = False
    TabOrder = 2
    DesignSize = (
      1002
      302)
    object Label8: TLabel
      Left = 16
      Top = 52
      Width = 98
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label3: TLabel
      Left = 211
      Top = 17
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 3
      Top = 9
      Width = 111
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object MB: TcxDBTextEdit
      Tag = 1
      Left = 120
      Top = 57
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsReseniePreraspredelba
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clDefault
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object VRABOTENIME: TcxDBLookupComboBox
      Tag = 1
      Left = 244
      Top = 57
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsReseniePreraspredelba
      ParentFont = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      Properties.OnEditValueChanged = VRABOTENIMEPropertiesEditValueChanged
      StyleDisabled.TextColor = clDefault
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = VRABOTENIMEExit
      OnKeyDown = EnterKakoTab
      Width = 273
    end
    object ZapisiButton: TcxButton
      Left = 823
      Top = 253
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 6
    end
    object OtkaziButton: TcxButton
      Left = 904
      Top = 253
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 7
    end
    object cxGroupBox2: TcxGroupBox
      Left = 52
      Top = 184
      Caption = #1053#1086#1074#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      TabOrder = 5
      Height = 90
      Width = 757
      object Label2: TLabel
        Left = -24
        Top = 31
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1072#1079#1080#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 3
        Top = 58
        Width = 62
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object NOVO_RM_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 136
        Top = 28
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'NOVO_RM'
        DataBinding.DataSource = dm.dsReseniePreraspredelba
        ParentFont = False
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            Width = 60
            FieldName = 'ID'
          end
          item
            Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
            Width = 265
            FieldName = 'NAZIV_RM'
          end
          item
            Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            Width = 265
            FieldName = 'NAZIV_RE'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dsRMRE
        StyleDisabled.TextColor = clBackground
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 593
      end
      object NOVO_RM: TcxDBTextEdit
        Tag = 1
        Left = 71
        Top = 28
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'NOVO_RM'
        DataBinding.DataSource = dm.dsReseniePreraspredelba
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 65
      end
      object Novo_od: TcxDBDateEdit
        Tag = 1
        Left = 71
        Top = 55
        Hint = 
          #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1086#1112' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090' '#1087#1086#1095#1085#1091#1074#1072' '#1076#1072' '#1088#1072#1073#1086#1090#1080' '#1085#1072' '#1085#1086#1074#1086#1090#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089 +
          #1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsReseniePreraspredelba
        ParentFont = False
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikMask
        ShowHint = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 123
      end
    end
    object cxGroupBox1: TcxGroupBox
      Left = 52
      Top = 88
      Caption = #1055#1088#1077#1076#1093#1086#1076#1085#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      TabOrder = 4
      Height = 90
      Width = 757
      object Label1: TLabel
        Left = -48
        Top = 31
        Width = 113
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1072#1079#1080#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 12
        Top = 58
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object staro_rm: TcxDBTextEdit
        Tag = 1
        Left = 68
        Top = 28
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'STARO_RM'
        DataBinding.DataSource = dm.dsReseniePreraspredelba
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 68
      end
      object ID_RM_RE_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 136
        Top = 28
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'STARO_RM'
        DataBinding.DataSource = dm.dsReseniePreraspredelba
        Enabled = False
        ParentFont = False
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            Width = 529
            FieldName = 'NAZIV_RM'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dsRMRE
        StyleDisabled.TextColor = clBackground
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 593
      end
      object staro_do: TcxDBDateEdit
        Tag = 1
        Left = 68
        Top = 55
        Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1112' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090' '#1088#1072#1073#1086#1090#1077#1083' '#1085#1072' '#1087#1088#1077#1076#1093#1086#1076#1085#1086#1090#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsReseniePreraspredelba
        ParentFont = False
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikMask
        ShowHint = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 123
      end
    end
    object Broj: TcxDBTextEdit
      Left = 306
      Top = 14
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsReseniePreraspredelba
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clDefault
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object txtDatumKreiranje: TcxDBDateEdit
      Tag = 1
      Left = 120
      Top = 14
      Hint = #1044#1072#1090#1091#1084' '#1082#1086#1075#1072' '#1077' '#1082#1088#1077#1080#1088#1072#1085#1086' '#1088#1077#1096#1077#1085#1080#1077#1090#1086' '#1079#1072' '#1087#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM_KREIRANJE'
      DataBinding.DataSource = dm.dsReseniePreraspredelba
      ParentFont = False
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 1002
    Height = 234
    Align = alClient
    Caption = 'lPanel'
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1000
      Height = 232
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsReseniePreraspredelba
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDDOKUMENTNAZIV'
          Width = 141
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 113
        end
        object cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VRABOTENNAZIV'
          Width = 266
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 263
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 145
        end
        object cxGrid1DBTableView1STARO_RM: TcxGridDBColumn
          DataBinding.FieldName = 'STARO_RM'
        end
        object cxGrid1DBTableView1NOVO_RM: TcxGridDBColumn
          DataBinding.FieldName = 'NOVO_RM'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1002
    Height = 234
    Align = alClient
    TabOrder = 4
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1000
      Height = 232
      Align = alClient
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsReseniePreraspredelba
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGridDBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGridDBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 89
        end
        object cxGridDBTableView1DATUM_KREIRANJE: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_KREIRANJE'
          Width = 107
        end
        object cxGridDBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 89
        end
        object cxGridDBTableView1VRABOTENNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 200
        end
        object cxGridDBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGridDBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGridDBTableView1NOVO_RM: TcxGridDBColumn
          DataBinding.FieldName = 'NOVO_RM'
          Visible = False
        end
        object cxGridDBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          Caption = #1058#1077#1082#1086#1074#1085#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 190
        end
        object cxGridDBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Visible = False
        end
        object cxGridDBTableView1STARO_RM: TcxGridDBColumn
          Caption = #1057#1090#1072#1088#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086'(ID) '
          DataBinding.FieldName = 'STARO_RM'
          Visible = False
        end
        object cxGridDBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'STARONAZIV'
          Width = 200
        end
        object cxGridDBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 103
        end
        object cxGridDBTableView1Column3: TcxGridDBColumn
          DataBinding.FieldName = 'NOVONAZIV'
          Width = 200
        end
        object cxGridDBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 111
        end
        object cxGridDBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
        end
        object cxGridDBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDDOKUMENTNAZIV'
          Visible = False
        end
        object cxGridDBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
        end
        object cxGridDBTableView1TS_INS: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGridDBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGridDBTableView1USR_INS: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1076#1086#1076#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGridDBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGridDBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 232
    Top = 256
  end
  object PopupMenu1: TPopupMenu
    Left = 328
    Top = 272
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 408
    Top = 264
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 341
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 548
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 167
      DockedTop = 0
      FloatLeft = 781
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPreraspredeliRabotnik
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPreraspredeliRabotnik
      Category = 0
      LargeImageIndex = 6
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPecatiPreraspredelba
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 216
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      Visible = False
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aPreraspredeliRabotnik: TAction
      Caption = #1048#1079#1074#1088#1096#1080' '#1087#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072
      Hint = #1057#1077' '#1074#1088#1096#1080' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090
    end
    object aPecatiPreraspredelba: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072' '
      ImageIndex = 19
      OnExecute = aPecatiPreraspredelbaExecute
    end
    object aPrisilnoBrisenje: TAction
      Caption = 'aPrisilnoBrisenje'
      ShortCut = 16503
      OnExecute = aPrisilnoBrisenjeExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 72
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44452.359673981480000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 616
    Top = 168
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 779
    Top = 179
  end
  object tblRMRE: TpFIBDataSet
    RefreshSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where(  hs.do_datum is null'
      '     ) and (     HRR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hs.OPIS as sis_opis'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'where /*$$IBEC$$ hs.do_datum is null and $$IBEC$$*/  hs.id_re_fi' +
        'rma = :firma')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 616
    Top = 235
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRMRESIS_OPIS: TFIBStringField
      DisplayLabel = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'SIS_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 152
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 632
    Top = 432
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object qStaroMesto: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select vrm.ID_RM_RE'
      '      from hr_vraboten_rm vrm'
      
        '      where vrm.mb = :mb and vrm.id_re_firma = :firma and (:datu' +
        'm_kreiranje between vrm.datum_od and coalesce(vrm.datum_do, :dat' +
        'um_kreiranje))')
    Left = 520
    Top = 262
  end
end
