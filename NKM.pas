unit NKM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxGroupBox, cxRadioGroup, cxDropDownEdit, cxCalendar,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, FIBQuery,
  pFIBQuery, FIBDataSet, pFIBDataSet, dxRibbonSkins, cxPCdxBarPopupMenu,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip,
  dxCustomHint, cxHint;

type
//  niza = Array[1..5] of Variant;

  TfrmNKM = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dPanel: TPanel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    lPanel: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxTabSheet3: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxComponentPrinter1Link3: TdxGridReportLink;
    Label3: TLabel;
    MB: TcxDBTextEdit;
    NAZIV_VRABOTEN: TcxDBLookupComboBox;
    Label5: TLabel;
    RabMesto: TcxDBTextEdit;
    RabMestoNaziv: TcxDBLookupComboBox;
    cxGroupBox1: TcxGroupBox;
    Label2: TLabel;
    NAZIV: TcxDBTextEdit;
    TIP_NAZIV: TcxDBLookupComboBox;
    TIP: TcxDBTextEdit;
    Label15: TLabel;
    Label12: TLabel;
    DATUM: TcxDBDateEdit;
    txtOpis: TcxDBMemo;
    Label4: TLabel;
    cxGridDBTableView2ID: TcxGridDBColumn;
    cxGridDBTableView2TIP: TcxGridDBColumn;
    cxGridDBTableView2NAZIV: TcxGridDBColumn;
    cxGridDBTableView2DATUM: TcxGridDBColumn;
    cxGridDBTableView2MB: TcxGridDBColumn;
    cxGridDBTableView2ID_RM_RE: TcxGridDBColumn;
    cxGridDBTableView2ID_RE_FIRMA: TcxGridDBColumn;
    cxGridDBTableView2ZABELESKA: TcxGridDBColumn;
    cxGridDBTableView2NAZIVVRABOTEN: TcxGridDBColumn;
    cxGridDBTableView2RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGridDBTableView2RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGridDBTableView2TIPNAZIV: TcxGridDBColumn;
    cxGridDBTableView2TS_INS: TcxGridDBColumn;
    cxGridDBTableView2TS_UPD: TcxGridDBColumn;
    cxGridDBTableView2USR_INS: TcxGridDBColumn;
    cxGridDBTableView2USR_UPD: TcxGridDBColumn;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1TIP: TcxGridDBColumn;
    cxGridDBTableView1NAZIV: TcxGridDBColumn;
    cxGridDBTableView1DATUM: TcxGridDBColumn;
    cxGridDBTableView1MB: TcxGridDBColumn;
    cxGridDBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGridDBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGridDBTableView1ZABELESKA: TcxGridDBColumn;
    cxGridDBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGridDBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGridDBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGridDBTableView1TIPNAZIV: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIPNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    qStaroMesto: TpFIBQuery;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMRENAZIV_RM: TFIBStringField;
    dsRMRE: TDataSource;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGridDBTableView2NAZIV_VRABOTEN: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyleRepository3: TcxStyleRepository;
    dxGridReportLinkStyleSheet3: TdxGridReportLinkStyleSheet;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;
    cxStyle33: TcxStyle;
    cxStyle34: TcxStyle;
    cxStyle35: TcxStyle;
    cxStyle36: TcxStyle;
    cxStyle37: TcxStyle;
    cxStyle38: TcxStyle;
    cxStyle39: TcxStyle;
    cxHintStyleController1: TcxHintStyleController;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure NAZIV_VRABOTENExit(Sender: TObject);
    procedure txtOpisDblClick(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmNKM: TfrmNKM;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, Notepad;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmNKM.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmNKM.aNovExecute(Sender: TObject);
begin
  if(dm.tblNKM.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if cxPageControl1.ActivePage = cxTabSheet1 then
      begin
       cxGrid1DBTableView1.DataController.DataSet.Insert;
       dm.tblNKMFLAG.Value:=1;
      end;
    if cxPageControl1.ActivePage = cxTabSheet2 then
      begin
       cxGridDBTableView1.DataController.DataSet.Insert;
       dm.tblNKMFLAG.Value:=2;
      end;
    if cxPageControl1.ActivePage = cxTabSheet3 then
      begin
       cxGridDBTableView2.DataController.DataSet.Insert;
       dm.tblNKMFLAG.Value:=3;
      end;
    dm.tblNKMID_RE_FIRMA.Value:=dmKon.re;
    prva.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmNKM.aAzurirajExecute(Sender: TObject);
begin
  if(dm.tblNKM.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    if cxPageControl1.ActivePage = cxTabSheet1 then
      begin
       cxGrid1DBTableView1.DataController.DataSet.Edit;
       dm.tblNKMFLAG.Value:=1;
      end;
    if cxPageControl1.ActivePage = cxTabSheet2 then
      begin
       cxGridDBTableView1.DataController.DataSet.Edit;
       dm.tblNKMFLAG.Value:=2;
      end;
    if cxPageControl1.ActivePage = cxTabSheet3 then
      begin
       cxGridDBTableView2.DataController.DataSet.Edit;
       dm.tblNKMFLAG.Value:=3;
      end;
    NAZIV.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmNKM.aBrisiExecute(Sender: TObject);
begin
  if(dm.tblNKM.State = dsBrowse) then
   begin
    if cxPageControl1.ActivePage = cxTabSheet1 then
       cxGrid1DBTableView1.DataController.DataSet.Delete();
    if cxPageControl1.ActivePage = cxTabSheet2 then
       cxGridDBTableView1.DataController.DataSet.Delete();
    if cxPageControl1.ActivePage = cxTabSheet3 then
       cxGridDBTableView2.DataController.DataSet.Delete();
   end;
end;

//	����� �� ���������� �� ����������
procedure TfrmNKM.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmNKM.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmNKM.aSnimiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheet1 then
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  if cxPageControl1.ActivePage = cxTabSheet2 then
     zacuvajGridVoBaza(Name,cxGridDBTableView1);
  if cxPageControl1.ActivePage = cxTabSheet3 then
     zacuvajGridVoBaza(Name,cxGridDBTableView2);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmNKM.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajVoExcel(cxGrid1, Caption);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajVoExcel(cxGrid2, Caption);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        zacuvajVoExcel(cxGrid3, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmNKM.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmNKM.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmNKM.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmNKM.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmNKM.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmNKM.txtOpisDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblNKMZABELESKA.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
      begin
	      dm.tblNKMZABELESKA.Value := frmNotepad.ReturnText;
      end;
    frmNotepad.Free;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmNKM.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmNKM.NAZIV_VRABOTENExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
      if (dm.tblNKM.State = dsEdit) or (dm.tblNKM.State = dsInsert) then
        begin
          if (NAZIV_VRABOTEN.Text<> '')then
             begin
                qStaroMesto.Close;
                qStaroMesto.ParamByName('mb').Value:=mb.Text;
                qStaroMesto.ParamByName('firma').Value:=dmKon.re;
                qStaroMesto.ExecQuery;
                dm.tblNKMID_RM_RE.Value:= qStaroMesto.FldByName['id_rm_re'].Value;
             end;
        end;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmNKM.prefrli;
begin
end;

procedure TfrmNKM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (dm.tblNKM.State = dsEdit) or (dm.tblNKM.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            dm.tblNKM.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            dm.tblNKM.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmNKM.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmNKM.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
    procitajGridOdBaza(Name,cxGridDBTableView2,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
    procitajPrintOdBaza(Name,cxGridDBTableView2.Name, dxComponentPrinter1Link3);

    dm.tblTipNKM.close;
    dm.tblTipNKM.ParamByName('flag').Value:= '1';
    dm.tblTipNKM.open;

    if tag = 1 then
       begin
          dm.tblNKM.close;
          dm.tblNKM.ParamByName('flag').Value:= '1';
          dm.tblNKM.ParamByName('firma').Value:= dmKon.re;
          dm.tblNKM.ParamByName('mb').Value:= dm.tblLicaVraboteniMB.Value;
          dm.tblNKM.open;
          dxBarManager1Bar1.Visible:=false;
       end
    else
       begin
          dm.tblNKM.close;
          dm.tblNKM.ParamByName('flag').Value:= '1';
          dm.tblNKM.ParamByName('firma').Value:= dmKon.re;
          dm.tblNKM.ParamByName('mb').Value:= '%';
          dm.tblNKM.open;
       end;

     tblRMRE.ParamByName('firma').Value:=dmKon.re;
     tblRMRE.Open;
end;
//------------------------------------------------------------------------------

procedure TfrmNKM.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmNKM.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmNKM.cxPageControl1Change(Sender: TObject);
begin
      if cxPageControl1.ActivePage = cxTabSheet1 then
         begin
           dm.tblTipNKM.ParamByName('flag').Value:= 1;
           dm.tblTipNKM.FullRefresh;
           dm.tblNKM.ParamByName('flag').Value:= 1;
           dm.tblNKM.FullRefresh;
         end;
      if cxPageControl1.ActivePage = cxTabSheet2 then
         begin
           dm.tblTipNKM.ParamByName('flag').Value:= 2;
           dm.tblTipNKM.FullRefresh;
           dm.tblNKM.ParamByName('flag').Value:= 2;
           dm.tblNKM.FullRefresh;
         end;
      if cxPageControl1.ActivePage = cxTabSheet3 then
         begin
           dm.tblTipNKM.ParamByName('flag').Value:= 3;
           dm.tblTipNKM.FullRefresh;
           dm.tblNKM.ParamByName('flag').Value:= 3;
           dm.tblNKM.FullRefresh;
         end;
end;

//  ����� �� �����
procedure TfrmNKM.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := dm.tblNKM.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        dm.tblNKM.Post;
//        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxPageControl1.ActivePage.SetFocus;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmNKM.aOtkaziExecute(Sender: TObject);
begin
  if (dm.tblNKM.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      dm.tblNKM.Cancel;
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxPageControl1.ActivePage.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmNKM.aPageSetupExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1Link1.PageSetup;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        dxComponentPrinter1Link2.PageSetup;
     if cxPageControl1.ActivePage = cxTabSheet3 then
        dxComponentPrinter1Link3.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmNKM.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        begin
          dxComponentPrinter1Link1.ReportTitle.Text := Caption;

          dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
          dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
          dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

          dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
         //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
        end;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        begin
          dxComponentPrinter1Link2.ReportTitle.Text := Caption;

          dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
          dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
          dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

          dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
         //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
        end;
     if cxPageControl1.ActivePage = cxTabSheet3 then
        begin
          dxComponentPrinter1Link3.ReportTitle.Text := Caption;

          dxComponentPrinter1Link3.PrinterPage.PageHeader.RightTitle.Clear;
          dxComponentPrinter1Link3.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
          dxComponentPrinter1Link3.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

          dxComponentPrinter1Link3.PrinterPage.PageHeader.LeftTitle.Clear;
         //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link3);
        end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmNKM.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
      if cxPageControl1.ActivePage = cxTabSheet1 then
          dxComponentPrinter1Link1.DesignReport();
      if cxPageControl1.ActivePage = cxTabSheet2 then
          dxComponentPrinter1Link2.DesignReport();
      if cxPageControl1.ActivePage = cxTabSheet3 then
          dxComponentPrinter1Link3.DesignReport();
end;

procedure TfrmNKM.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link2);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        zacuvajPrintVoBaza(Name,cxGridDBTableView2.Name,dxComponentPrinter1Link3);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmNKM.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
     if cxPageControl1.ActivePage = cxTabSheet3 then
        brisiPrintOdBaza(Name,cxGridDBTableView2.Name, dxComponentPrinter1Link3);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmNKM.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmNKM.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmNKM.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
