unit EvidencijaOtsustvaPrvDel;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.8                    }
{                                       }
{   22.03.2010                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter, cxContainer,
  cxEdit, Menus, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxScheduler, cxSchedulerStorage, cxSchedulerCustomControls,
  cxSchedulerCustomResourceView, cxSchedulerDayView, cxSchedulerDateNavigator,
  cxSchedulerHolidays, cxSchedulerTimeGridView, cxSchedulerUtils,
  cxSchedulerWeekView, cxSchedulerYearView, cxSchedulerGanttView,
   dxSkinsdxBarPainter, cxDBLookupComboBox, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, cxSchedulerDBStorage, dxPSCore, dxPScxCommon,
   cxGridCustomPopupMenu, cxGridPopupMenu, cxGrid, ActnList,
  dxBar, cxBarEditItem, cxGroupBox, cxRadioGroup, StdCtrls, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxMemo, cxDBEdit, cxButtons, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxMaskEdit, cxCalendar, cxTextEdit, cxPC,
  dxStatusBar, dxRibbonStatusBar, dxRibbon, ComCtrls, cxSpinEdit, cxTimeEdit,
  IdBaseComponent,  IdSchedulerOfThread, IdSchedulerOfThreadDefault, Math,
  cxLabel, cxCheckBox, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, FIBQuery, pFIBQuery, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxSkinscxSchedulerPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip, dxCustomHint ;

type
  TfrmEvidencijaOtsustva = class(TForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxPodesuvanje: TdxRibbonTab;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxPageControl1: TcxPageControl;
    tcPlanOtsustvo: TcxTabSheet;
    Panel2: TPanel;
    Label15: TLabel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    MB: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    buttonOtkazi: TcxButton;
    buttonZapisi: TcxButton;
    VRABOTENIME: TcxDBLookupComboBox;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    txtOpis: TcxDBMemo;
    PRICINANAZIV: TcxDBLookupComboBox;
    Panel4: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENIME: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1OD_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1DO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1TIPOTSUSTVONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA_OPIS: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    tcPregled: TcxTabSheet;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton6: TRadioButton;
    Panel3: TPanel;
    cxSchedulerOtsustva: TcxScheduler;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    ActionList1: TActionList;
    aPlanZaOtsustvo: TAction;
    aDodadiPlanZaOtsustvo: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aIzlez: TAction;
    aAzurirajPlan: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    aSnimiGoIzgledot: TAction;
    aZacuvajVoExcel: TAction;
    aPecatenje: TAction;
    aPecatiTabela: TAction;
    aStatusGrupa: TAction;
    aStatus: TAction;
    aPrebaraj: TAction;
    aIscisti: TAction;
    aPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aSnimiPecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxSchedulerDBStorage1: TcxSchedulerDBStorage;
    aPomos: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton6: TdxBarButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton32: TdxBarLargeButton;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarLargeButton34: TdxBarLargeButton;
    cxBarEditRabEdinica: TcxBarEditItem;
    cas: TcxLabel;
    cxLabel1: TcxLabel;
    den: TcxLabel;
    cxLabel3: TcxLabel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    OD_VREME: TcxDBDateEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_ZAPIS: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENTATKOVOIME: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OPTIONS: TcxGridDBColumn;
    cxGrid1DBTableView1EVENT_TYPE: TcxGridDBColumn;
    cxGrid1DBTableView1TASK_LINKS_FIELD: TcxGridDBColumn;
    cxGrid1DBTableView1PLATENO: TcxGridDBColumn;
    cxGrid1DBTableView1FLAG: TcxGridDBColumn;
    Label7: TLabel;
    DO_VREME: TcxDBDateEdit;
    DO_VREME_L: TcxDBDateEdit;
    aFormConfig: TAction;
    Label6: TLabel;
    cbResenieGO: TcxDBLookupComboBox;
    qTipOtsustvo: TpFIBQuery;
    cxGrid1DBTableView1ID_RESENIE_GO: TcxGridDBColumn;
    cxGrid1DBTableView1BRGODINARESENIE: TcxGridDBColumn;
    cxLabel4: TcxLabel;
    OstanatiDenovi: TcxLabel;
    qSumTipOtsustvo: TpFIBQuery;
    qSumNeplateni: TpFIBQuery;
    qKolektivenDog: TpFIBQuery;
    Panel5: TPanel;
    cxGroupBox3: TcxGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    vrabotenprebaraj: TcxLookupComboBox;
    TipOtsustvoPrebaraj: TcxLookupComboBox;
    ButtonIscisti: TcxButton;
    dxComponentPrinter1Link2: TcxSchedulerReportLink;
    procedure FormCreate(Sender: TObject);
    procedure aPlanZaOtsustvoExecute(Sender: TObject);
    procedure aDodadiPlanZaOtsustvoExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aAzurirajPlanExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aSnimiGoIzgledotExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aIscistiExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure vrabotenprebarajPropertiesEditValueChanged(Sender: TObject);
    procedure TipOtsustvoPrebarajPropertiesEditValueChanged(Sender: TObject);
    procedure cxBarEditRabEdinicaChange(Sender: TObject);
    procedure OD_VREMEExit(Sender: TObject);
    procedure DO_VREMEExit(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure MBExit(Sender: TObject);
    procedure VRABOTENIMEExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aFormConfigExecute(Sender: TObject);
    procedure PRICINANAZIVExit(Sender: TObject);
    procedure cbResenieGOExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEvidencijaOtsustva: TfrmEvidencijaOtsustva;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmResources, dmUnit, dmUnitOtsustvo, Utils,
  dmSystem, TipOtsustvo, FormConfig;

{$R *.dfm}

procedure TfrmEvidencijaOtsustva.aAzurirajPlanExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          if dmOtsustvo.tblOtsustvaflag.value = 0 then
             begin
                cxPageControl1.ActivePage:=tcPlanOtsustvo;
                Panel2.Enabled:=True;
                Panel4.Enabled:=False;
                tcPregled.Enabled:=False;
                dmOtsustvo.tblOtsustva.Edit;
                StateActive:=dsEdit;
                MB.SetFocus;
                if (PRICINANAZIV.Text <> '') then
                  begin
                    qTipOtsustvo.Close;
                    qTipOtsustvo.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaPRICINA.Value;
                    qTipOtsustvo.ExecQuery;
                    if qTipOtsustvo.FldByName['naziv'].Value = '������� �����' then
                       begin
                          Label6.Enabled:=true;
                          cbResenieGO.Enabled:=true;
                          cbResenieGO.SetFocus;
                       end
                    else
                       begin
                          Label6.Enabled:=false;
                          cbResenieGO.Enabled:=false;
                       end;
                  end;
             end
          else ShowMessage('�� � ��������� ��������� �� ��� �����. (������ ��� ����� � ����������� ����� �� �����.)');
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmEvidencijaOtsustva.aBrisiExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          if dmOtsustvo.tblOtsustvaflag.value = 0 then
             begin
                cxPageControl1.ActivePage:=tcPlanOtsustvo;
                cxGrid1DBTableView1.DataController.DataSet.Delete();
             end
          else ShowMessage('�� � ��������� ��������� �� ��� �����. (������ ��� ����� � ����������� ����� �� �����.)');
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmEvidencijaOtsustva.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaOtsustva.aDodadiPlanZaOtsustvoExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          Panel2.Enabled:=True;
          Panel4.Enabled:=False;
          tcPregled.Enabled:=False;
          dmOtsustvo.tblOtsustva.Insert;
          StateActive:=dsInsert;
          dmOtsustvo.tblOtsustvaPlateno.Value:=1;
          cas.Caption:=' ';
          den.Caption:=' ';
          MB.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmEvidencijaOtsustva.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmEvidencijaOtsustva.aIscistiExecute(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.aIzlezExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
    pom_od, pom_do, h, m :string;
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
          dmOtsustvo.tblOtsustva.Cancel;
          StateActive:=dsBrowse;
          RestoreControls(Panel2);
          if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
             begin
               if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM','MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
          Label6.Enabled:=false;
          cbResenieGO.Enabled:=false;
          cxLabel4.Visible:=False;
          OstanatiDenovi.Visible:=False;
          Panel4.Enabled:=true;
          Panel2.Enabled:=false;
          tcPregled.Enabled:=True;
          cxGrid1.SetFocus;
        end
     else Close;
end;

procedure TfrmEvidencijaOtsustva.aOsveziExecute(Sender: TObject);
begin
     dmOtsustvo.tblOtsustva.Refresh;
end;

procedure TfrmEvidencijaOtsustva.aOtkaziExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     dmOtsustvo.tblOtsustva.Cancel;
     StateActive:=dsBrowse;
     RestoreControls(Panel2);
     Panel4.Enabled:=true;
     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
             begin
               if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
     Label6.Enabled:=false;
     cbResenieGO.Enabled:=false;
     cxLabel4.Visible:=False;
     OstanatiDenovi.Visible:=False;

     Label6.visible:=true;
     cbResenieGO.visible:=true;
     cxLabel4.Left:=256;
     OstanatiDenovi.Left:=352;

     Panel2.Enabled:=false;
     tcPregled.Enabled:=True;
     cxGrid1.SetFocus;

end;

procedure TfrmEvidencijaOtsustva.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmEvidencijaOtsustva.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = tcPlanOtsustvo then
        begin
          dxComponentPrinter1Link1.ReportTitle.Text := '��������� ������ �� ��������';
          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
        end
     else if cxPageControl1.ActivePage = tcPregled then
        begin
          dxComponentPrinter1Link1.ReportTitle.Text := '����������� ������ �� ��������';
          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
        end
end;

procedure TfrmEvidencijaOtsustva.aPlanZaOtsustvoExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     cxPageControl1.ActivePage:=tcPlanOtsustvo;
     panel2.Enabled:=True;
     Panel4.Enabled:=False;
     tcPregled.Enabled:=False;
     dmOtsustvo.tblOtsustva.Insert;
     StateActive:=dsInsert;
     dmOtsustvo.tblOtsustvaOD_VREME.Value:= cxSchedulerOtsustva.SelStart;
     dmOtsustvo.tblOtsustvaDO_VREME.Value:= cxSchedulerOtsustva.SelFinish;
     dmOtsustvo.tblOtsustvaDO_VREME_L.Value:= cxSchedulerOtsustva.SelFinish - 1;
     dmOtsustvo.tblOtsustvaPlateno.Value:=1;

     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
         begin
           if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
              begin
                pom_od:=OD_VREME.Text;
                pom_do:=DO_VREME.Text;

                cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                if cas_od = cas_do then
                   begin
                     casovi:=0;
                     minuti:= minuta_do - minuta_od;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                     Den.Caption:='0';
                   end
                else
                   begin
                     minuti:= 60 - minuta_od + minuta_do;
                     if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                     Den.Caption:='0';
                   end;
              end
           else
              begin
                denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', Null,dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value, Null, 'DENOVI');
                casovi:= denovi * 8;
                den.Caption:=IntToStr(denovi);
                Cas.Caption:=IntToStr(casovi);
              end;
         end
     else
         begin
           den.Caption:=' ';
           Cas.Caption:=' ';
         end;
     MB.SetFocus;
end;

procedure TfrmEvidencijaOtsustva.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencijaOtsustva.aSnimiGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaOtsustva.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaOtsustva.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmEvidencijaOtsustva.aZapisiExecute(Sender: TObject);
var poraka, ostanatiTip:integer;
    sod,sdo:string;
begin
    if Validacija(Panel2) = false then
        begin
           dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;

           if (OstanatiDenovi.Caption <> '')and (cbResenieGO.Enabled = true) and (den.Caption <> '')and(StrToInt(OstanatiDenovi.Caption)<(StrToInt(den.Caption))) then
                   begin
                      frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '�������� ������ ������ �� �����������, ������ ��������� �� ������� ����� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                      if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                   end
            else
                begin
                  poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.pKontrolaOtsustva, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN','TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'PORAKA_OUT');
                  if poraka = 1 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '1. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������ ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 2 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '2. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 3 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '3. �������� ������ ������ �� ����������� �� ��������� ��������(�� ���� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 4 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '4. �������� ������ ������ �� ����������� �� ��������� �������� (������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 5 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '5. �������� ������ ������ �� ����������� �� ��������� �������� (������� �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 6 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '6. �������� ������ ������ �� ����������� �� ��������� �������� (���� ������ ������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;

                     end
                  else  if poraka = 7 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '7. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ������ ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 8 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '8.�������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ������� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 9 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '9. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ���� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 10 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '10. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
               else  if poraka = 11 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '11. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (������� �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 12 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '12. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ���� ������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                end;
           dmOtsustvo.tblOtsustva.Post;
           StateActive:=dsBrowse;
           RestoreControls(Panel2);
           Label6.Enabled:=false;
           cbResenieGO.Enabled:=False;
           cxLabel4.Visible:=False;
           OstanatiDenovi.Visible:=False;

           Label6.visible:=true;
           cbResenieGO.visible:=true;
           cxLabel4.Left:=256;
           OstanatiDenovi.Left:=352;

           Panel4.Enabled:=true;
           Panel2.Enabled:=false;
           tcPregled.Enabled:=True;
           cxGrid1.SetFocus;
        end;
end;

procedure TfrmEvidencijaOtsustva.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
     dm.viewVraboteni.ParamByName('mb').Value:='%';
     dm.viewVraboteni.ParamByName('re').Value:='%';
     dm.viewVraboteni.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.FormCreate(Sender: TObject);
begin
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
     dmOtsustvo.tblOtsustva.close;
     dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('re').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOtsustva.Open;

     dmOtsustvo.tblTipOtsustvo.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblTipOtsustvo.Open;

     dmOtsustvo.tblResenieOtsustva.ParamByName('MB').Value:= '%';
     dmOtsustvo.tblResenieOtsustva.ParamByName('godina').Value:= dmKon.godina;
     dmOtsustvo.tblResenieOtsustva.Open;

     StateActive:=dsBrowse;
end;

procedure TfrmEvidencijaOtsustva.FormShow(Sender: TObject);
begin
     dxBarManager1Bar1.Caption := Caption;
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

     dmOtsustvo.tblPodsektori.close;
     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.Open;

     cxBarEditRabEdinica.EditValue:=firma;
end;

procedure TfrmEvidencijaOtsustva.MBExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
    if (VRABOTENIME.Text <> '') and (MB.Text <> '')then
         begin
           dmOtsustvo.tblResenieOtsustva.ParamByName('MB').Value:= dmOtsustvo.tblOtsustvaMB.Value;
           dmOtsustvo.tblResenieOtsustva.ParamByName('godina').Value:= dmKon.godina;
           dmOtsustvo.tblResenieOtsustva.FullRefresh;
         end;
     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
         denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
         casovi:= denovi * 8;
         den.Caption:=IntToStr(denovi);
         Cas.Caption:=IntToStr(casovi);
        end;
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencijaOtsustva.OD_VREMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     TEdit(Sender).Color:=clWhite;
     if StateActive in [dsEdit, dsInsert] then
        begin
          if (OD_VREME.Text <> '') and (DO_VREME_L.Text <> '') then
             begin
               if DO_vreme_L.Date < OD_vreme.Date then
                 begin
                   ShowMessage('������ � �������� ������ !!!');
                   OD_vreme.SetFocus;
                 end
               else if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
        end;
end;

procedure TfrmEvidencijaOtsustva.PRICINANAZIVExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (PRICINANAZIV.Text <> '') then
        begin
          qTipOtsustvo.Close;
          qTipOtsustvo.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaPRICINA.Value;
          qTipOtsustvo.ExecQuery;
          if qTipOtsustvo.FldByName['naziv'].Value = '������� �����' then
             begin
               Label6.visible:=true;
               cbResenieGO.visible:=true;
               cxLabel4.Left:=256;
               OstanatiDenovi.Left:=352;

               Label6.Enabled:=true;
               cbResenieGO.Enabled:=true;
               cbResenieGO.SetFocus;
             end
          else
             begin
               cbResenieGO.Clear;
               Label6.Enabled:=false;
               cbResenieGO.Enabled:=false;

               Label6.visible:=false;
               cbResenieGO.visible:=false;
               cxLabel4.Left:=80;
               OstanatiDenovi.Left:=176;

               if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (qTipOtsustvo.FldByName['denovi'].Value <> Null) then
                  begin
                    if mb.Text <> '' then
                      begin
                        cxLabel4.Visible:=true;
                        OstanatiDenovi.Visible:=true;
                        OstanatiDenovi.Caption:=IntToStr(dmOtsustvo.zemiRezultat(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', Null, Null, dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, Null, Null, 'OSTANATI'));
                      end;
                  end
               else if (qTipOtsustvo.FldByName['plateno'].Value = 0) then
                  begin
                    if mb.Text <> '' then
                      begin
                        cxLabel4.Visible:=true;
                        OstanatiDenovi.Visible:=true;
                        OstanatiDenovi.Caption:=IntToStr(dmOtsustvo.zemiRezultat(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', Null, Null, dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, Null, Null, 'OSTANATI'));
                       end;
                  end;
             end;
          dmOtsustvo.tblOtsustvaPLATENO.Value:=qTipOtsustvo.FldByName['plateno'].Value
        end;
end;

procedure TfrmEvidencijaOtsustva.RadioButton1Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewDay.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton2Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewWeek.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton3Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewWeeks.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton4Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewYear.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton5Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewGantt.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton6Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewTimeGrid.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.TipOtsustvoPrebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
        dmOtsustvo.tblOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
     if TipOtsustvoPrebaraj.Text<> '' then
        dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
     if TipOtsustvoPrebaraj.Text= '' then
        dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.VRABOTENIMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     if (VRABOTENIME.Text <> '') and (MB.Text <> '')then
         begin
           dmOtsustvo.tblResenieOtsustva.ParamByName('MB').Value:= dmOtsustvo.tblOtsustvaMB.Value;
           dmOtsustvo.tblResenieOtsustva.ParamByName('godina').Value:= dmKon.godina;
           dmOtsustvo.tblResenieOtsustva.FullRefresh;
         end;

     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
         denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', Null,dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
         casovi:= denovi * 8;
         den.Caption:=IntToStr(denovi);
         Cas.Caption:=IntToStr(casovi);
        end;
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencijaOtsustva.vrabotenprebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
        dmOtsustvo.tblOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
     if TipOtsustvoPrebaraj.Text<> '' then
        dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
     if (vrabotenprebaraj.Text = '') then
        dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.cbResenieGOExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if cbResenieGO.Text <> '' then
        begin
          cxLabel4.Visible:=True;
          OstanatiDenovi.Visible:=True;
          OstanatiDenovi.Caption:= IntToStr(dmOtsustvo.zemiBroj(dmOtsustvo.pOstanatiDenovi, 'IDRESENIEGO', Null, Null, Null, dmOtsustvo.tblOtsustvaID_RESENIE_GO.Value, Null, Null, Null, 'OSTANATI_OUT'));
        end;
end;

procedure TfrmEvidencijaOtsustva.cxBarEditRabEdinicaChange(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     if cxBarEditRabEdinica.EditValue = firma then
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:='%';
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('re').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva.FullRefresh;
        end
     else
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:=cxBarEditRabEdinica.EditValue;
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblOtsustva.FullRefresh;
        end;
end;

procedure TfrmEvidencijaOtsustva.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;
procedure TfrmEvidencijaOtsustva.cxDBTextEditAllExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencijaOtsustva.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
 begin

        if (StateActive in [dsBrowse]) and (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
           if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
              begin
                pom_od:=OD_VREME.Text;
                pom_do:=DO_VREME.Text;

                cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                if cas_od = cas_do then
                   begin
                     casovi:=0;
                     minuti:= minuta_do - minuta_od;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                     Den.Caption:='0';
                   end
                else
                   begin
                     minuti:= 60 - minuta_od + minuta_do;
                     if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                     Den.Caption:='0';
                   end;
              end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value, Null,'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
        end;
end;

procedure TfrmEvidencijaOtsustva.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaOtsustva.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if cxPageControl1.ActivePage = tcPregled then
        cxSchedulerOtsustva.SetFocus;
end;

procedure TfrmEvidencijaOtsustva.DO_VREMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin

     TEdit(Sender).Color:=clWhite;
     if StateActive in [dsEdit, dsInsert] then
        begin
          if (OD_VREME.Text <> '') and (DO_VREME_L.Text <> '') then
             begin
               dmOtsustvo.tblOtsustvaDO_VREME.Value:=dmOtsustvo.tblOtsustvaDO_VREME_L.Value + 1;
               if DO_vreme_L.Date < OD_vreme.Date then
                 begin
                   ShowMessage('������ � �������� ������ !!!');
                   DO_vreme_L.SetFocus;
                 end
               else if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;

                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
        end;
end;

procedure TfrmEvidencijaOtsustva.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
           if (Sender = PRICINANAZIV) then
              begin
                frmTipOtsustvo:=TfrmTipOtsustvo.Create(Application);
                frmTipOtsustvo.Tag:=1;
                frmTipOtsustvo.ShowModal;
                frmTipOtsustvo.Free;
                dmOtsustvo.tblOtsustvaPRICINA.Value:=dmOtsustvo.tblTipOtsustvoID.Value;
              end;
        end;
    end;
end;
end.
