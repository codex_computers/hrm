unit FeedBack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxSkinscxPCPainter, dxBarSkinnedCustForm, cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxDBLookupComboBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxTextEdit, cxDBEdit, StdCtrls, ExtCtrls, dxBar,
  dxPSCore, dxPScxCommon,  ActnList, cxBarEditItem, Menus,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, cxPC, cxButtons, cxMemo, cxCalendar, cxGroupBox, cxBlobEdit,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxScreenTip;

type
  TfrmFeedBack = class(TForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    StatusBar1: TdxRibbonStatusBar;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    Panel1: TPanel;
    Panel2: TPanel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    Panel3: TPanel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxGroupBox1: TcxGroupBox;
    OPIS: TcxDBMemo;
    Label5: TLabel;
    DATUM: TcxDBDateEdit;
    Label4: TLabel;
    BROJ: TcxDBTextEdit;
    Label2: TLabel;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aDodadiStavka: TAction;
    aZapisiStavka: TAction;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    Panel4: TPanel;
    aBrisiStavka: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarLargeButton20: TdxBarLargeButton;
    aAzurirajStavka: TAction;
    aOtkaziStavka: TAction;
    aOtkazi1: TAction;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM_REVIZIJA: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    DATUM_REVIZIJA: TcxDBDateEdit;
    Label3: TLabel;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENNAIV: TcxGridDBColumn;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    aStavki: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGridPopupMenu4: TcxGridPopupMenu;
    cxButton1: TcxButton;
    Panel5: TPanel;
    cxButton2: TcxButton;
    Panel6: TPanel;
    cxButton3: TcxButton;
    aBrisiIspitanici: TAction;
    aBrisiOcenuvaci: TAction;
    aBrisiPrasanja: TAction;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1OCENUVACNAZIV: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGrid4DBTableView1PRASANJENAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1GRUPANAZIV: TcxGridDBColumn;
    aAnketenList: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    cxButton4: TcxButton;
    aVnesNaRezultati: TAction;
    aPregledajRezultati: TAction;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid4DBTableView1ID: TcxGridDBColumn;
    cxGrid4DBTableView1FEEDBACK_ID: TcxGridDBColumn;
    cxGrid4DBTableView1PRASANJE_ID: TcxGridDBColumn;
    cxGrid4DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid4DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid4DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid4DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid4DBTableView1USR_UPD: TcxGridDBColumn;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1FEEDBACK_ID: TcxGridDBColumn;
    cxGridDBTableView1RATING_FROM_ID: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1FEEDBACK_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aOtkazi1Execute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aStavkiExecute(Sender: TObject);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid4DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aBrisiIspitaniciExecute(Sender: TObject);
    procedure aBrisiOcenuvaciExecute(Sender: TObject);
    procedure aBrisiPrasanjaExecute(Sender: TObject);
    procedure aAnketenListExecute(Sender: TObject);
    procedure aVnesNaRezultatiExecute(Sender: TObject);
    procedure aPregledajRezultatiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  frmFeedBack: TfrmFeedBack;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmReportUnit, dmResources, dmUnitUcinok,
  dmUnit, Utils, FeedBackStavki, dmUnitOtsustvo, PopolnuvanjeRezultati,
  dmSistematizacija, FormConfig;

{$R *.dfm}

procedure TfrmFeedBack.aAnketenListExecute(Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30088;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dmUcinok.tblPrasanja.FullRefresh;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmFeedBack.aAzurirajExecute(Sender: TObject);
begin
     if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          cxTabSheet1.Enabled:=false;
          cxPageControl1.ActivePage:=cxTabSheet2;
          Panel3.Enabled:=true;
          BROJ.SetFocus;
          cxGrid2DBTableView1.DataController.DataSet.Edit;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmFeedBack.aBrisiExecute(Sender: TObject);
begin
     if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
        cxGrid2DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmFeedBack.aBrisiIspitaniciExecute(Sender: TObject);
var broj:Integer;
begin
     broj:=dmOtsustvo.zemiBroj(dmUcinok.pCountMBRezultati, 'FID', 'MB', Null,Null, dmUcinok.tblFeedBackID.Value, dmUcinok.tblIspitaniciMB.Value, Null,Null, 'BROJ');
     if broj = 0 then
        begin
          if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
              cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
     else
       ShowMessage('������� �� �� ������� �������. ����� ������� ��������� �� ��� �������� !!!');

     cxGrid1.SetFocus;
end;

procedure TfrmFeedBack.aBrisiOcenuvaciExecute(Sender: TObject);
var broj:Integer;
begin
    broj:=dmOtsustvo.zemiBroj(dmUcinok.pCountFedbackRezultati, 'FID', Null, Null,Null, dmUcinok.tblFeedBackID.Value, Null, Null,Null, 'BR_OUT');
     if broj = 0 then
        begin
          if(cxGridDBTableView1.DataController.DataSource.State = dsBrowse) then
              cxGridDBTableView1.DataController.DataSet.Delete();
        end
     else
       ShowMessage('������� �� �� ������� �������. ����� ������� ��������� �� ��� Feedback !!!');

     cxGrid3.SetFocus;
end;

procedure TfrmFeedBack.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmFeedBack.aBrisiPrasanjaExecute(Sender: TObject);
var broj:Integer;
begin
     broj:=dmOtsustvo.zemiBroj(dmUcinok.pCountFedbackRezultati, 'FID', Null, Null,Null, dmUcinok.tblFeedBackID.Value, Null, Null,Null, 'BR_OUT');
     if broj = 0 then
        begin
          if(cxGrid4DBTableView1.DataController.DataSource.State = dsBrowse) then
              cxGrid4DBTableView1.DataController.DataSet.Delete();
        end
     else
       ShowMessage('������� �� �� ������� �������. ����� ������� ��������� �� ��� feedback !!!');
     cxGrid4.SetFocus;
end;

procedure TfrmFeedBack.aFormConfigExecute(Sender: TObject);
begin
    frmFormConfig := TfrmFormConfig.Create(Application);
    frmFormConfig.formPtr := Addr(Self);
    frmFormConfig.ShowModal;
    frmFormConfig.Free;
end;

procedure TfrmFeedBack.aIzlezExecute(Sender: TObject);
begin
      if cxGrid2DBTableView1.DataController.DataSet.State in [dsInsert, dsEdit] then
        begin
           cxGrid2DBTableView1.DataController.DataSet.Cancel;
           RestoreControls(Panel3);
           Panel3.Enabled:=False;
           cxTabSheet1.Enabled:=true;
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxGrid2.SetFocus;
        end
      else
        close;
end;

procedure TfrmFeedBack.aNovExecute(Sender: TObject);
begin
     if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
          cxTabSheet1.Enabled:=false;
          cxPageControl1.ActivePage:=cxTabSheet2;
          Panel3.Enabled:=true;
          cxGrid2DBTableView1.DataController.DataSet.Insert;
          dmUcinok.tblFeedBackBROJ.Value:=dmOtsustvo.zemiBroj(dmUcinok.pMaxBrojFeedback,NULL, NULL,Null, NULL, NULL, NULL, NULL,Null, 'MAXBR')+1;
          dmUcinok.tblFeedBackID_RE_FIRMA.Value:=firma;
          DATUM.SetFocus;
        end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmFeedBack.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

procedure TfrmFeedBack.aOtkazi1Execute(Sender: TObject);
begin
     cxGrid2DBTableView1.DataController.DataSet.Cancel;
     RestoreControls(Panel3);
     Panel3.Enabled:=False;
     cxTabSheet1.Enabled:=true;
     cxPageControl1.ActivePage:=cxTabSheet1;
     cxGrid2.SetFocus;
end;

procedure TfrmFeedBack.aOtkaziExecute(Sender: TObject);
begin
      if cxGrid2DBTableView1.DataController.DataSet.State in [dsInsert, dsEdit] then
        begin
           cxGrid2DBTableView1.DataController.DataSet.Cancel;
           RestoreControls(Panel3);
           Panel3.Enabled:=False;
           cxTabSheet1.Enabled:=true;
           cxPageControl1.ActivePage:=cxTabSheet1;
           cxGrid2.SetFocus;
        end
      else
        close;
end;

procedure TfrmFeedBack.aPageSetupExecute(Sender: TObject);
begin
      dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmFeedBack.aPecatiTabelaExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmFeedBack.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
      dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmFeedBack.aPregledajRezultatiExecute(Sender: TObject);
begin
     frmVnesNaOcenki:=TfrmVnesNaOcenki.Create(Application);
     frmVnesNaOcenki.ShowModal;
     frmVnesNaOcenki.Free;
end;

procedure TfrmFeedBack.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmFeedBack.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmFeedBack.aStavkiExecute(Sender: TObject);
var broj:Integer;
begin
     broj:=dmOtsustvo.zemiBroj(dmUcinok.pCountFedbackRezultati, 'FID', Null, Null, Null,dmUcinok.tblFeedBackID.Value, Null, Null,Null, 'BR_OUT');
     if broj = 0 then
        begin
          frmFeedBackStavki:=TfrmFeedBackStavki.Create(Application);
          frmFeedBackStavki.ShowModal;
          frmFeedBackStavki.Free;
        end
     else
        ShowMessage('�������� �� ��������. ����� ������� ��������� �� ��� Feedback !!!');
end;

procedure TfrmFeedBack.aVnesNaRezultatiExecute(Sender: TObject);
begin
     dmSis.zemiProcedura(dmUcinok.pInsertRezultati,'MB_IN', 'FEEDBACKID_IN', Null, dmUcinok.tblIspitaniciMB.Value, dmUcinok.tblFeedBackID.Value, Null);
     frmVnesNaOcenki:=TfrmVnesNaOcenki.Create(Application);
     frmVnesNaOcenki.ShowModal;
     frmVnesNaOcenki.Free;
     cxGrid1.SetFocus;
end;

procedure TfrmFeedBack.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, Caption);
end;

procedure TfrmFeedBack.aZapisiExecute(Sender: TObject);
begin
     if Validacija(Panel3)=false then
        begin
          cxGrid2DBTableView1.DataController.DataSet.Post;
          Panel3.Enabled:=False;
          cxTabSheet1.Enabled:=true;
          cxPageControl1.ActivePage:=cxTabSheet1;
          cxGrid2.SetFocus;
        end;
end;

procedure TfrmFeedBack.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmFeedBack.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmFeedBack.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmFeedBack.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmFeedBack.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView1);
end;

procedure TfrmFeedBack.cxGrid4DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid4DBTableView1);
end;

procedure TfrmFeedBack.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (cxGrid2DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid2DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid2DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(Panel3) = false) then
          begin
            cxGrid2DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;

procedure TfrmFeedBack.FormCreate(Sender: TObject);
begin
     dmUcinok.tblFeedBack.ParamByName('firma').Value:=firma;
     dmUcinok.tblFeedBack.Open;
     dmUcinok.tblIspitanici.Open;
     dmUcinok.tblOcenuvaci.Open;
     dmUcinok.tblPrasalnik.Open;
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmFeedBack.FormShow(Sender: TObject);
begin
//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
////	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link1);

     StateActive:=dsBrowse;
     cxGrid2.SetFocus;
end;

end.
