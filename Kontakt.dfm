inherited frmKontakt: TfrmKontakt
  Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1080' '#1082#1086#1085#1090#1072#1082#1090
  ClientHeight = 721
  ClientWidth = 1025
  ExplicitWidth = 1041
  ExplicitHeight = 759
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1025
    Height = 240
    ExplicitWidth = 1025
    ExplicitHeight = 240
    inherited cxGrid1: TcxGrid
      Width = 1021
      Height = 236
      ExplicitWidth = 1021
      ExplicitHeight = 236
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsKontakt
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MOLBA'
          Visible = False
          Width = 106
        end
        object cxGrid1DBTableView1lkNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'lkNaziv'
          Width = 89
        end
        object cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'MB_VRABOTEN'
          Width = 80
        end
        object cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 136
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1IMEPREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'IMEPREZIME'
          Visible = False
          Width = 117
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 69
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 45
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1MESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTONAZIV'
          Width = 139
        end
        object cxGrid1DBTableView1DRZAVA: TcxGridDBColumn
          DataBinding.FieldName = 'DRZAVA'
          Visible = False
        end
        object cxGrid1DBTableView1DRZAVANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'DRZAVANAZIV'
          Width = 76
        end
        object cxGrid1DBTableView1DRZAVJANSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'DRZAVJANSTVO'
          Visible = False
        end
        object cxGrid1DBTableView1DRZAVJANSTVONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'DRZAVJANSTVONAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1TEL: TcxGridDBColumn
          DataBinding.FieldName = 'TEL'
          Width = 72
        end
        object cxGrid1DBTableView1MOBILEN: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN'
          Width = 105
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 107
        end
        object cxGrid1DBTableView1ITNO_SRODSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ITNO_SRODSTVO'
          Width = 91
        end
        object cxGrid1DBTableView1ITNO_IME_PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'ITNO_IME_PREZIME'
          Width = 98
        end
        object cxGrid1DBTableView1ITNO_ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ITNO_ADRESA'
          Width = 241
        end
        object cxGrid1DBTableView1ITNO_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'ITNO_TELEFON'
          Width = 114
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1085#1072#1087#1088#1072#1074#1080#1083' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 366
    Width = 1025
    Height = 332
    ExplicitTop = 366
    ExplicitWidth = 1025
    ExplicitHeight = 332
    inherited Label1: TLabel
      Left = 824
      Top = 151
      Visible = False
      ExplicitLeft = 824
      ExplicitTop = 151
    end
    object Label10: TLabel [1]
      Left = 63
      Top = 51
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1086#1083#1073#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 20
      Top = 44
      Width = 93
      Height = 27
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label8: TLabel [3]
      Left = 20
      Top = 229
      Width = 93
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1088#1078#1072#1074#1112#1072#1085#1089#1090#1074#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 880
      Top = 148
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsKontakt
      TabOrder = 1
      Visible = False
      ExplicitLeft = 880
      ExplicitTop = 148
    end
    inherited OtkaziButton: TcxButton
      Left = 927
      Top = 284
      TabOrder = 11
      ExplicitLeft = 927
      ExplicitTop = 284
    end
    inherited ZapisiButton: TcxButton
      Left = 846
      Top = 284
      TabOrder = 10
      ExplicitLeft = 846
      ExplicitTop = 284
    end
    object ID_MOLBA: TcxDBTextEdit
      Left = 119
      Top = 48
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1086#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsKontakt
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object IMEPREZIME: TcxDBLookupComboBox
      Left = 216
      Top = 48
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' '#1082#1086#1077' '#1112#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072' '#1084#1086#1083#1073#1072#1090#1072
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsKontakt
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'ID'
        end
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Width = 800
          FieldName = 'IMEPREZIME'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dmSis.dsMolba
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 282
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Left = 119
      Top = 48
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsKontakt
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Left = 216
      Top = 48
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsKontakt
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      ShowHint = True
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 282
    end
    object cxGroupBox1: TcxGroupBox
      Left = 39
      Top = 88
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 5
      DesignSize = (
        459
        121)
      Height = 121
      Width = 459
      object Label7: TLabel
        Left = 9
        Top = 84
        Width = 65
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1088#1078#1072#1074#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 23
        Top = 57
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 198
        Top = 30
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = -28
        Top = 30
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DRZAVANAZIV: TcxDBLookupComboBox
        Left = 143
        Top = 81
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'DRZAVA'
        DataBinding.DataSource = dmSis.dsKontakt
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmMat.dsDrzava
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 294
      end
      object ADRESA: TcxDBTextEdit
        Tag = 1
        Left = 80
        Top = 27
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
      object DRZAVA: TcxDBTextEdit
        Left = 80
        Top = 81
        BeepOnEnter = False
        DataBinding.DataField = 'DRZAVA'
        DataBinding.DataSource = dmSis.dsKontakt
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 62
      end
      object MESTO: TcxDBTextEdit
        Tag = 1
        Left = 80
        Top = 54
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dmSis.dsKontakt
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 62
      end
      object MESTONAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 143
        Top = 54
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dmSis.dsKontakt
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 294
      end
      object BROJ: TcxDBTextEdit
        Left = 306
        Top = 27
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 131
      end
    end
    object DRZAVJANSTVO: TcxDBTextEdit
      Left = 119
      Top = 226
      BeepOnEnter = False
      DataBinding.DataField = 'DRZAVJANSTVO'
      DataBinding.DataSource = dmSis.dsKontakt
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object DRZAVJANSTVONAZIV: TcxDBLookupComboBox
      Left = 182
      Top = 226
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'DRZAVJANSTVO'
      DataBinding.DataSource = dmSis.dsKontakt
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dmSis.dsDrzavjanstvo
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 294
    end
    object cxGroupBox2: TcxGroupBox
      Left = 526
      Top = 12
      Anchors = [akRight, akBottom]
      Caption = #1050#1086#1085#1090#1072#1082#1090
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 8
      DesignSize = (
        305
        115)
      Height = 115
      Width = 305
      object Label9: TLabel
        Left = -4
        Top = 32
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 22
      end
      object Label12: TLabel
        Left = -4
        Top = 59
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 49
      end
      object Label13: TLabel
        Left = -4
        Top = 86
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 76
      end
      object TEL: TcxDBTextEdit
        Left = 104
        Top = 29
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'TEL'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
      object MOBILEN: TcxDBTextEdit
        Left = 104
        Top = 56
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
      object EMAIL: TcxDBTextEdit
        Left = 104
        Top = 83
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
    end
    object cxGroupBox3: TcxGroupBox
      Left = 526
      Top = 133
      Anchors = [akRight, akBottom]
      Caption = #1050#1086#1085#1090#1072#1082#1090' '#1079#1072' '#1080#1090#1085#1072' '#1087#1086#1090#1088#1077#1073#1072
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 9
      DesignSize = (
        305
        140)
      Height = 140
      Width = 305
      object Label4: TLabel
        Left = -4
        Top = 30
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1057#1088#1086#1076#1089#1090#1074#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = -4
        Top = 57
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = -4
        Top = 84
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = -4
        Top = 111
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ITNO_SRODSTVO: TcxDBTextEdit
        Left = 104
        Top = 27
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ITNO_SRODSTVO'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
      object ITNO_IME_PREZIME: TcxDBTextEdit
        Left = 104
        Top = 54
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ITNO_IME_PREZIME'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
      object ITNO_TELEFON: TcxDBTextEdit
        Left = 104
        Top = 81
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ITNO_TELEFON'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
      object ITNO_ADRESA: TcxDBTextEdit
        Left = 104
        Top = 108
        Anchors = [akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ITNO_ADRESA'
        DataBinding.DataSource = dmSis.dsKontakt
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 178
      end
    end
    object cxDBCheckBox1: TcxDBCheckBox
      Left = 279
      Top = 21
      Caption = #1040#1076#1088#1077#1089#1072' '#1086#1076' '#1051#1080#1095#1085#1072' '#1082#1072#1088#1090#1072
      DataBinding.DataField = 'LK'
      DataBinding.DataSource = dmSis.dsKontakt
      Properties.Alignment = taLeftJustify
      Properties.NullStyle = nssUnchecked
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      TabOrder = 12
      Width = 146
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1025
    ExplicitWidth = 1025
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 698
    Width = 1025
    ExplicitTop = 698
    ExplicitWidth = 1025
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 200
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40280.557885844910000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 680
    Top = 64
  end
end
