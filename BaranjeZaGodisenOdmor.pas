unit BaranjeZaGodisenOdmor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon, cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDBLookupComboBox, cxGroupBox, cxRadioGroup,
  cxDropDownEdit, cxCalendar, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, frxDesgn,
  frxClass, frxDBSet, frxRich, frxCross, cxLabel, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxPScxSchedulerLnk, dxCustomHint, cxHint,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinOffice2013White, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxPSdxDBOCLnk;

type
//  niza = Array[1..5] of Variant;

  TfrmBaranjeZaGodisenOdmor = class(TForm)
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1PRV_DEL: TcxGridDBColumn;
    cxGrid1DBTableView1VTOR_DEL: TcxGridDBColumn;
    cxGrid1DBTableView1MB_RAKOVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1RMNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OdobrenieNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1KoristenjeNaziv: TcxGridDBColumn;
    dPanel: TPanel;
    Label8: TLabel;
    Label6: TLabel;
    MB: TcxDBTextEdit;
    VRABOTENIME: TcxDBLookupComboBox;
    DatumKreiranje: TcxDBDateEdit;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    godina: TcxDBTextEdit;
    cxGroupBox3: TcxGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Datum2Od: TcxDBDateEdit;
    VtorDel: TcxDBTextEdit;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    DatumOd: TcxDBDateEdit;
    PrvDel: TcxDBTextEdit;
    DatumDo: TcxDBDateEdit;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxDBRadioGroup2: TcxDBRadioGroup;
    Label11: TLabel;
    Odobreno_od: TcxDBTextEdit;
    Datum2Do: TcxDBDateEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxBarManager1Bar5: TdxBar;
    aReseniGO: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aPecatiDokument: TAction;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOCUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ODOBRENIE: TcxGridDBColumn;
    cxGrid1DBTableView1KORISTENJE: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    dxBarLargeButton19: TdxBarLargeButton;
    aBrisiDokument: TAction;
    cxHintStyleController1: TcxHintStyleController;
    dxBarManager1Bar7: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    cxBarEditItem4: TcxBarEditItem;
    ComboBoxGodina: TdxBarCombo;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarLargeButton20: TdxBarLargeButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VRABOTENIMEExit(Sender: TObject);
    procedure cxDBRadioGroup2PropertiesEditValueChanged(Sender: TObject);
    procedure DatumDoExit(Sender: TObject);
    procedure DatumOdExit(Sender: TObject);
    procedure Datum2OdExit(Sender: TObject);
    procedure Datum2DoExit(Sender: TObject);
    procedure aReseniGOExecute(Sender: TObject);
    procedure aPecatiDokumentExecute(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure dxRibbon1TabChanged(Sender: TdxCustomRibbon);
    procedure cbGodinaPropertiesEditValueChanged(Sender: TObject);
    procedure ComboBoxGodinaChange(Sender: TObject);
    procedure PrvDelExit(Sender: TObject);
    procedure VtorDelExit(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure MBPropertiesEditValueChanged(Sender: TObject);
    private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmBaranjeZaGodisenOdmor: TfrmBaranjeZaGodisenOdmor;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmUnitOtsustvo,
  ResenieZaGodisenOdmor, dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmBaranjeZaGodisenOdmor.ComboBoxGodinaChange(Sender: TObject);
begin
     if ComboBoxGodina.Text = '����' then
         cxGrid1DBTableView1.DataController.Filter.Clear
     else if ComboBoxGodina.Text <> '' then
     begin
          cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(ComboBoxGodina.Text), ComboBoxGodina.Text);
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
     end;
end;

constructor TfrmBaranjeZaGodisenOdmor.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmBaranjeZaGodisenOdmor.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblBaranjeGOVID_DOCUMENT.Value:=baranjeGO;
    dm.tblBaranjeGOGODINA.Value:=dmKon.godina;
    dm.tblBaranjeGODATUM.Value:=Now;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmBaranjeZaGodisenOdmor.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
    if dm.tblBaranjeGOKORISTENJE.Value = 1 then
       cxGroupBox3.Enabled:=false
    else
       cxGroupBox3.Enabled:=true;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmBaranjeZaGodisenOdmor.aBrisiDokumentExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteBaranjeGO.Close;
            dmOtsustvo.qDeleteBaranjeGO.ParamByName('id').Value:=dm.tblBaranjeGOID.Value;
            dmOtsustvo.qDeleteBaranjeGO.ExecQuery;
        end;
end;

procedure TfrmBaranjeZaGodisenOdmor.aBrisiExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmBaranjeZaGodisenOdmor.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmBaranjeZaGodisenOdmor.aReseniGOExecute(Sender: TObject);
begin
     pom_tab_resGO:=0;
     frmResenijeZaGodisenOdmor:=TfrmResenijeZaGodisenOdmor.Create(Application);
     frmResenijeZaGodisenOdmor.Tag:=1;
     frmResenijeZaGodisenOdmor.ShowModal;
     frmResenijeZaGodisenOdmor.Free;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmBaranjeZaGodisenOdmor.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmBaranjeZaGodisenOdmor.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmBaranjeZaGodisenOdmor.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmBaranjeZaGodisenOdmor.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmBaranjeZaGodisenOdmor.cxDBRadioGroup2PropertiesEditValueChanged(
  Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) or (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit)then
       begin
         if dm.tblBaranjeGOKORISTENJE.Value = 2 then
            cxGroupBox3.Enabled:=true
         else
            begin
              Datum2Od.Clear;
              Datum2Do.Clear;
              VtorDel.Clear;
              cxGroupBox3.Enabled:=false;
            end;
       end;
end;

procedure TfrmBaranjeZaGodisenOdmor.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmBaranjeZaGodisenOdmor.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmBaranjeZaGodisenOdmor.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmBaranjeZaGodisenOdmor.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmBaranjeZaGodisenOdmor.VRABOTENIMEExit(Sender: TObject);
begin
      TEdit(Sender).Color:=clWhite;
end;

procedure TfrmBaranjeZaGodisenOdmor.VtorDelExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (Datum2Od.Text <> '') and (VtorDel.Text <> '') and (MB.Text <> '') then
        begin
          dm.tblBaranjeGODATUM2_DO.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'OD_DATUM', 'DENOVI', 'MB', Null, dm.tblBaranjeGODATUM2_OD.Value, dm.tblBaranjeGOVTOR_DEL.Value, dm.tblBaranjeGOMB.Value, Null, 'DO_DATUM');
        end;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmBaranjeZaGodisenOdmor.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmBaranjeZaGodisenOdmor.MBPropertiesEditValueChanged(
  Sender: TObject);
var vre:integer;
    vnaziv:string;
begin
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
      begin
      if MB.Text <> '' then
               begin
                  dm.viewVraboteni.Locate('MB', MB.Text, []);
                  vre:=dm.viewVraboteniRE.Value;
                  vnaziv:=dm.viewVraboteniRABOTNAEDINICANAZIV.Value;
                  dm.tblRe.Locate('RE; NAZIV', VarArrayOf([vre,vnaziv]) , []);
                  dm.tblBaranjeGOMB_RAKOVODITEL.Value:= dm.tblReRAKOVODITEL.Value;
               end;
      end;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmBaranjeZaGodisenOdmor.prefrli;
begin
end;

procedure TfrmBaranjeZaGodisenOdmor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmBaranjeZaGodisenOdmor.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dm.tblBaranjeGO.close;
  dm.tblBaranjeGO.ParamByName('firma').Value:=dmKon.re;
  dm.tblBaranjeGO.ParamByName('MB').Value:='%';
  dm.tblBaranjeGO.ParamByName('RE').Value:='%';
  dm.tblBaranjeGO.open;

  ComboBoxGodina.Text:=intToStr(dmKon.godina);

//  cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
//     try
//        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
//        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(ComboBoxGodina.Text), ComboBoxGodina.Text);
//        cxGrid1DBTableView1.DataController.Filter.Active:=True;
//     finally
//        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
//     end;
 pom_tab_baranje_go:=0;
end;

//------------------------------------------------------------------------------

procedure TfrmBaranjeZaGodisenOdmor.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    pom_tab_baranje_go:=1;
end;
//------------------------------------------------------------------------------

procedure TfrmBaranjeZaGodisenOdmor.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmBaranjeZaGodisenOdmor.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmBaranjeZaGodisenOdmor.Datum2DoExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (Datum2Od.Text <> '') and (Datum2Do.Text <> '') then
        begin
          dm.tblBaranjeGOvtor_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblBaranjeGODATUM2_OD.Value, dm.tblBaranjeGODATUM2_DO.Value, dm.tblBaranjeGOMB.Value,Null, 'DENOVI');
        end;
end;

procedure TfrmBaranjeZaGodisenOdmor.Datum2OdExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (Datum2Od.Text <> '') and (Datum2Do.Text <> '') then
        begin
          dm.tblBaranjeGOvtor_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dm.tblBaranjeGODATUM2_OD.Value, dm.tblBaranjeGODATUM2_DO.Value, dm.tblBaranjeGOMB.Value,Null, 'DENOVI');
        end;
end;

procedure TfrmBaranjeZaGodisenOdmor.DatumDoExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (DatumOd.Text <> '') and (DatumDo.Text <> '') then
        begin
          dm.tblBaranjeGOPRV_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', null,dm.tblBaranjeGODATUM1_OD.Value, dm.tblBaranjeGODATUM1_DO.Value, dm.tblBaranjeGOMB.Value,Null, 'DENOVI');
        end;
end;

procedure TfrmBaranjeZaGodisenOdmor.DatumOdExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (DatumOd.Text <> '') and (DatumDo.Text <> '') then
        begin
          dm.tblBaranjeGOPRV_DEL.Value:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', null,dm.tblBaranjeGODATUM1_OD.Value, dm.tblBaranjeGODATUM1_DO.Value, dm.tblBaranjeGOMB.Value,Null, 'DENOVI');
        end;
end;

procedure TfrmBaranjeZaGodisenOdmor.dxRibbon1TabChanged(
  Sender: TdxCustomRibbon);
begin
      if pom_tab_baranje_go <> 0 then
         begin
          if dxRibbon1.ActiveTab = dxRibbon1Tab2 then
            begin
              // ComboBoxGodina.Visible:=false;
            end
          else
            begin
             //  ComboBoxGodina.Visible:=True;
            end;
         end;
end;

//  ����� �� �����
procedure TfrmBaranjeZaGodisenOdmor.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if (DatumOd.Text <> '') and (DatumDo.Text <> '') and (DatumDo.Date < DatumOd.date) then
         begin
           ShowMessage('������ � �������� ������ !!!');
           DatumDo.SetFocus;
         end
      else if (Datum2Od.Text <> '') and (Datum2Do.Text <> '') and (Datum2Do.Date < Datum2Od.date) then
         begin
           ShowMessage('������ � �������� ������ !!!');
           Datum2Do.SetFocus;
         end
      else
      begin
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        cxGroupBox3.Enabled:=false;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        cxGroupBox3.Enabled:=false;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        cxGroupBox3.Enabled:=false;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
    end;
  end;
end;

procedure TfrmBaranjeZaGodisenOdmor.cbGodinaPropertiesEditValueChanged(
  Sender: TObject);
begin
     

end;

//	����� �� ���������� �� �������
procedure TfrmBaranjeZaGodisenOdmor.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      cxGroupBox3.Enabled:=false;
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmBaranjeZaGodisenOdmor.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmBaranjeZaGodisenOdmor.aPecatiDokumentExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.BaranjeZaGO.close;
  dmReport.BaranjeZaGO.ParamByName('id').Value:=dm.tblBaranjeGOID.Value;
  dmReport.BaranjeZaGO.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.BaranjeZaGODATA.IsNull then
     begin
       RptStream := dmReport.BaranjeZaGO.CreateBlobStream(dmReport.BaranjeZaGODATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
        begin
          RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream);

          RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
          If RichView <> Nil Then
            begin
              RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
              RichView.DataSet:=dmReport.frxBaranjeZaGO;
            end;
          MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
          if MasterData <> Nil then
            MasterData.DataSet:=dmReport.frxBaranjeZaGO;

          dmReport.frxReport1.ShowReport;

          frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
          if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 1);
        end;
     end;
end;

procedure TfrmBaranjeZaGodisenOdmor.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + ComboBoxGodina.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmBaranjeZaGodisenOdmor.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmBaranjeZaGodisenOdmor.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmBaranjeZaGodisenOdmor.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmBaranjeZaGodisenOdmor.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmBaranjeZaGodisenOdmor.aHelpExecute(Sender: TObject);
begin
     Application.HelpContext(121);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmBaranjeZaGodisenOdmor.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmBaranjeZaGodisenOdmor.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


procedure TfrmBaranjeZaGodisenOdmor.PrvDelExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (DatumOd.Text <> '') and (PrvDel.Text <> '') and (MB.Text <> '') then
        begin
          //dm.tblBaranjeGODATUM1_DO.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'DATUM_OD', 'BROJ', 'MB', Null, dm.tblBaranjeGODATUM1_OD.Value, dm.tblBaranjeGOPRV_DEL.Value, dm.tblBaranjeGOMB.Value, Null, 'DATUM_DO');
          //EXECUTE PROCEDURE PROC_HR_WORK_DATE (?OD_DATUM, ?DENOVI, ?MB)
          dm.tblBaranjeGODATUM1_DO.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'OD_DATUM', 'DENOVI', 'MB', Null, dm.tblBaranjeGODATUM1_OD.Value, dm.tblBaranjeGOPRV_DEL.Value, dm.tblBaranjeGOMB.Value, Null, 'DO_DATUM');
       end;
end;

end.
