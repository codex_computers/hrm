unit PlanZaOtsustva;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.8                   }
{                                       }
{   17.03.2010                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  cxStyles, cxEdit, cxScheduler, cxSchedulerStorage, cxSchedulerCustomControls,
  cxSchedulerCustomResourceView, cxSchedulerDayView, cxSchedulerDateNavigator,
  cxSchedulerHolidays, cxSchedulerTimeGridView, cxSchedulerUtils,
  cxSchedulerWeekView, cxSchedulerYearView, cxSchedulerGanttView, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
   dxSkinsdxBarPainter, dxSkinsdxRibbonPainter,
  dxStatusBar, dxRibbonStatusBar, ActnList, cxClasses, dxRibbon, dxBar,
  IdBaseComponent,  IdSchedulerOfThread, IdSchedulerOfThreadDefault,
  cxSchedulerDBStorage, cxSchedulerAggregateStorage, cxSchedulercxGridConnection,
  StdCtrls, ExtCtrls, cxContainer, cxTextEdit, cxDBEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxCalendar,
  cxGroupBox, cxMemo, cxGrid, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxGridCustomView, cxPC, cxButtons,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxBarSkinnedCustForm,
  dxPSCore, dxPScxCommon,  cxRadioGroup, cxBarEditItem,
  cxColorComboBox, cxCheckBox, cxDBExtLookupComboBox, cxLabel, cxSpinEdit,
  cxTimeEdit, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, FIBQuery, pFIBQuery, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxSkinscxSchedulerPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip, dxCustomHint,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dxSkinOffice2013White,
  cxNavigator, System.Actions, cxSchedulerTreeListBrowser, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxSchedulerAgendaView,
  cxSchedulerRecurrence, cxSchedulerRibbonStyleEventEditor, dxPSdxDBOCLnk ;

type
  TfrmPlanZaOtsustvo = class(TForm)
    dxBarManager1: TdxBarManager;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    ActionList1: TActionList;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    aPlanZaOtsustvo: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxGridViewRepository1: TcxGridViewRepository;
    cxPageControl1: TcxPageControl;
    tcPlanOtsustvo: TcxTabSheet;
    tcPregled: TcxTabSheet;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton6: TRadioButton;
    Panel3: TPanel;
    Panel2: TPanel;
    Label15: TLabel;
    Label1: TLabel;
    Label61: TLabel;
    Sifra: TcxDBTextEdit;
    MB: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    STATUS: TcxDBLookupComboBox;
    Panel4: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    aDodadiPlanZaOtsustvo: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    buttonOtkazi: TcxButton;
    buttonZapisi: TcxButton;
    aZapisi: TAction;
    aOtkazi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aIzlez: TAction;
    VRABOTENIME: TcxDBLookupComboBox;
    cxGroupBox2: TcxGroupBox;
    txtOpis: TcxDBMemo;
    Label2: TLabel;
    PRICINANAZIV: TcxDBLookupComboBox;
    Label5: TLabel;
    cxGridPopupMenu2: TcxGridPopupMenu;
    aAzurirajPlan: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    aBrisi: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aOsvezi: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    aSnimiGoIzgledot: TAction;
    dxBarLargeButton14: TdxBarLargeButton;
    aZacuvajVoExcel: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    aPecatenje: TAction;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    aPecatiTabela: TAction;
    dxBarButton3: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxSchedulerDBStorage1: TcxSchedulerDBStorage;
    cxSchedulerPlanOtsustva: TcxScheduler;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aStatusGrupa: TAction;
    aStatus: TAction;
    aPrebaraj: TAction;
    aIscisti: TAction;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxPodesuvanje: TdxRibbonTab;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aSnimiPecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    aPomos: TAction;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1BOJA: TcxGridDBColumn;
    dxBarButton6: TdxBarButton;
    dxBarManager1Bar4: TdxBar;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    cxBarEditRabEdinica: TcxBarEditItem;
    dxBarManager1Bar2: TdxBar;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton7: TdxBarButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    cxBarEditVraboten: TcxBarEditItem;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarLargeButton34: TdxBarLargeButton;
    dxBarLargeButton35: TdxBarLargeButton;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    Den: TcxLabel;
    OD_VREME: TcxDBDateEdit;
    cxHintStyleController1: TcxHintStyleController;
    cxHintStyleController2: TcxHintStyleController;
    Cas: TcxLabel;
    DO_VREME_L: TcxDBDateEdit;
    DO_VREME: TcxDBDateEdit;
    aFormConfig: TAction;
    Panel5: TPanel;
    cxGroupBox3: TcxGroupBox;
    Label9: TLabel;
    Label8: TLabel;
    ButtonIscisti: TcxButton;
    TipOtsustvoPrebaraj: TcxLookupComboBox;
    vrabotenprebaraj: TcxLookupComboBox;
    qTipOtsustvo: TpFIBQuery;
    cbGodina: TdxBarCombo;
    statusprebaraj: TcxLookupComboBox;
    Label7: TLabel;
    brDenovi: TcxTextEdit;
    aBrzaEvidencijaOtsustvo: TAction;
    N2: TMenuItem;
    dxBarButton8: TdxBarButton;
    aPecatiKalendar: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxComponentPrinter1Link2: TcxSchedulerReportLink;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton36: TdxBarLargeButton;
    dxBarLargeButton37: TdxBarLargeButton;
    dxBarLargeButton38: TdxBarLargeButton;
    dxBarLargeButton39: TdxBarLargeButton;
    aPodesuvanjePecatenje2: TAction;
    aPageSetup2: TAction;
    aSnimiPecatenje2: TAction;
    aBrisiPodesuvanjePecatenje2: TAction;
    dxBarLargeButton40: TdxBarLargeButton;
    dxBarLargeButton41: TdxBarLargeButton;
    dxBarLargeButton42: TdxBarLargeButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_ZAPIS: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1OD_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1DO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1DO_VREME_L: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1TIPOTSUSTVONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENPREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENTATKOVOIME: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENIME: TcxGridDBColumn;
    cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_BOJA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxGrid1DBTableView1DENOVI: TcxGridDBColumn;
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aDodadiPlanZaOtsustvoExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aAzurirajPlanExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aSnimiGoIzgledotExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure dxBarButton3Click(Sender: TObject);
    procedure aPecatenjeExecute(Sender: TObject);
    procedure aPlanZaOtsustvoExecute(Sender: TObject);
    procedure vrabotenprebarajPropertiesEditValueChanged(Sender: TObject);
    procedure aIscistiExecute(Sender: TObject);
    procedure TipOtsustvoPrebarajPropertiesEditValueChanged(Sender: TObject);
    procedure statusprebarajPropertiesEditValueChanged(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure FormShow(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure cxBarEditRabEdinicaChange(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure OD_VREMEExit(Sender: TObject);
    procedure DO_VREMEExit(Sender: TObject);
    procedure VRABOTENIMEExit(Sender: TObject);
    procedure MBExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aFormConfigExecute(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure brDenoviExit(Sender: TObject);
    procedure aBrzaEvidencijaOtsustvoExecute(Sender: TObject);
    procedure cxSchedulerPlanOtsustvaDblClick(Sender: TObject);
    procedure aPecatiKalendarExecute(Sender: TObject);
    procedure aPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aPageSetup2Execute(Sender: TObject);
    procedure aSnimiPecatenje2Execute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanZaOtsustvo: TfrmPlanZaOtsustvo;
  StateActive:TDataSetState;
implementation

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, dmUnitOtsustvo,
  Utils, DaNe, Status, StatusGrupa, TipOtsustvo, dmSistematizacija, FormConfig;

{$R *.dfm}

procedure TfrmPlanZaOtsustvo.aAzurirajPlanExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
           cxPageControl1.ActivePage:=tcPlanOtsustvo;
           Panel2.Enabled:=True;
           Panel4.Enabled:=False;
           tcPregled.Enabled:=False;
           dmOtsustvo.tblPlanOtsustva.Edit;
           StateActive:=dsEdit;
           brDenovi.Visible:=true;
           brDenovi.Text:='';
           MB.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPlanZaOtsustvo.aBrisiExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          cxGrid1DBTableView1.DataController.DataSet.Delete();
          dmOtsustvo.tblPlanOtsustva2.FullRefresh;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPlanZaOtsustvo.aBrisiPodesuvanjePecatenje2Execute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxSchedulerPlanOtsustva.Name, dxComponentPrinter1Link2);
end;

procedure TfrmPlanZaOtsustvo.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPlanZaOtsustvo.aBrzaEvidencijaOtsustvoExecute(Sender: TObject);
begin
     if vrabotenprebaraj.Text <> '' then
       if statusprebaraj.Text <> '' then
          if TipOtsustvoPrebaraj.Text <> '' then
             begin
                dmOtsustvo.tblPlanOtsustva.Insert;
                dmOtsustvo.tblPlanOtsustvaTIP_ZAPIS.Value:=0;
                dmOtsustvo.tblPlanOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                dmOtsustvo.tblPlanOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                dmOtsustvo.tblPlanOtsustvaSTATUS.Value:=statusprebaraj.EditValue;
                dmOtsustvo.tblPlanOtsustvaOD_VREME.Value:=cxSchedulerPlanOtsustva.SelStart;
                dmOtsustvo.tblPlanOtsustvaDO_VREME.Value:=cxSchedulerPlanOtsustva.SelFinish;
                dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value:=cxSchedulerPlanOtsustva.SelFinish -1;
                dmOtsustvo.tblPlanOtsustva.Post;
                dmOtsustvo.tblPlanOtsustva.FullRefresh;
                dmOtsustvo.tblPlanOtsustva2.FullRefresh;
                StateActive:=dsBrowse;
             end
          else ShowMessage('�������� ��� �� �������� !!!')
       else ShowMessage('�������� ������ !!!')
     else  ShowMessage('�������� ���� !!!')
end;

procedure TfrmPlanZaOtsustvo.aDodadiPlanZaOtsustvoExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin

          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          Panel2.Enabled:=True;
          Panel4.Enabled:=False;
          tcPregled.Enabled:=False;
          dmOtsustvo.tblPlanOtsustva.Insert;
          StateActive:=dsInsert;
          Cas.Caption:=' ';
          Den.Caption:=' ';
          brDenovi.Visible:=true;
          brDenovi.Text:='';

          if vrabotenprebaraj.Text <> '' then
             dmOtsustvo.tblPlanOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
          if statusprebaraj.Text <> ''  then
             dmOtsustvo.tblPlanOtsustvaSTATUS.Value:=statusprebaraj.EditValue;
          if TipOtsustvoPrebaraj.Text <> ''  then
             dmOtsustvo.tblPlanOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;

          MB.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmPlanZaOtsustvo.aFormConfigExecute(Sender: TObject);
begin
    frmFormConfig := TfrmFormConfig.Create(Application);
    frmFormConfig.formPtr := Addr(Self);
    frmFormConfig.ShowModal;
    frmFormConfig.Free;
end;

procedure TfrmPlanZaOtsustvo.aIscistiExecute(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     statusprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblPlanOtsustva.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
     dmOtsustvo.tblPlanOtsustva.FullRefresh;

     dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.FullRefresh;
end;

procedure TfrmPlanZaOtsustvo.aIzlezExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
          dmOtsustvo.tblPlanOtsustva.Cancel;
          StateActive:=dsBrowse;
          RestoreControls(Panel2);
          if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
             begin
               if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value,Null, 'DENOVI');;
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
          brDenovi.Visible:=false;
          Panel4.Enabled:=true;
          Panel2.Enabled:=false;
          tcPregled.Enabled:=True;
          cxGrid1.SetFocus;
        end
     else Close;
end;

procedure TfrmPlanZaOtsustvo.aOsveziExecute(Sender: TObject);
begin
     dmOtsustvo.tblPlanOtsustva.Refresh;
end;

procedure TfrmPlanZaOtsustvo.aOtkaziExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     dmOtsustvo.tblPlanOtsustva.Cancel;
     StateActive:=dsBrowse;
     RestoreControls(Panel2);
     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
             begin
               if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value,Null, 'DENOVI');;
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
     brDenovi.Visible:=false;
     Panel4.Enabled:=true;
     Panel2.Enabled:=false;
     tcPregled.Enabled:=True;
     cxGrid1.SetFocus;
end;

procedure TfrmPlanZaOtsustvo.aPageSetup2Execute(Sender: TObject);
begin
     dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmPlanZaOtsustvo.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmPlanZaOtsustvo.aPecatenjeExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPlanZaOtsustvo.aPecatiKalendarExecute(Sender: TObject);
var rabEdinica:string;
begin
     dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
     dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
     dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

     dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
     dmOtsustvo.tblPodsektori.Locate('ID',cxBarEditRabEdinica.EditValue, []);
     rabEdinica:= dmOtsustvo.tblPodsektoriNAZIV.Value;
     dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� �������: '+rabEdinica);
     dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: '+cbGodina.Text);
     if vrabotenprebaraj.Text <> '' then
        dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('����: '+vrabotenprebaraj.text);
     if statusprebaraj.Text <> '' then
        dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: '+statusprebaraj.text);
     if TipOtsustvoPrebaraj.Text <> '' then
        dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('��� �� ��������: '+TipOtsustvoPrebaraj.text);
     dxComponentPrinter1Link2.ReportTitle.Text := '����������� ������ �� ��������';
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);

end;

procedure TfrmPlanZaOtsustvo.aPecatiTabelaExecute(Sender: TObject);
var rabEdinica:string;
begin
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
     dmOtsustvo.tblPodsektori.Locate('ID',cxBarEditRabEdinica.EditValue, []);
     rabEdinica:= dmOtsustvo.tblPodsektoriNAZIV.Value;
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� �������: '+rabEdinica);
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������: '+cbGodina.Text);
     if vrabotenprebaraj.Text <> '' then
        dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����: '+vrabotenprebaraj.text);
     if statusprebaraj.Text <> '' then
        dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������: '+statusprebaraj.text);
     if TipOtsustvoPrebaraj.Text <> '' then
        dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('��� �� ��������: '+TipOtsustvoPrebaraj.text);
     dxComponentPrinter1Link1.ReportTitle.Text := '��������� ������ �� ��������';
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPlanZaOtsustvo.aPlanZaOtsustvoExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     cxPageControl1.ActivePage:=tcPlanOtsustvo;
     Panel2.Enabled:=True;
     Panel4.Enabled:=False;
     tcPregled.Enabled:=False;
     dmOtsustvo.tblPlanOtsustva.Insert;
     StateActive:=dsInsert;

     dmOtsustvo.tblPlanOtsustvaOD_VREME.Value:= cxSchedulerPlanOtsustva.SelStart;
     dmOtsustvo.tblPlanOtsustvaDO_VREME.Value:= cxSchedulerPlanOtsustva.SelFinish;
     dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value:= cxSchedulerPlanOtsustva.SelFinish - 1;
     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
         begin
           if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
              begin
                pom_od:=OD_VREME.Text;
                pom_do:=DO_VREME.Text;

                cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                if cas_od = cas_do then
                   begin
                     casovi:=0;
                     minuti:= minuta_do - minuta_od;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                     Den.Caption:='0';
                   end
                else
                   begin
                     minuti:= 60 - minuta_od + minuta_do;
                     if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                     Den.Caption:='0';
                   end;
              end
           else
              begin
                denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value, Null, 'DENOVI');
                casovi:= denovi * 8;
                den.Caption:=IntToStr(denovi);
                Cas.Caption:=IntToStr(casovi);
              end;
         end
     else
         begin
           den.Caption:=' ';
           Cas.Caption:=' ';
         end;
     MB.SetFocus;
end;

procedure TfrmPlanZaOtsustvo.aPodesuvanjePecatenje2Execute(Sender: TObject);
begin
     dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmPlanZaOtsustvo.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPlanZaOtsustvo.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(112);
end;

procedure TfrmPlanZaOtsustvo.aSnimiGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmPlanZaOtsustvo.aSnimiPecatenje2Execute(Sender: TObject);
begin
      zacuvajPrintVoBaza(Name,cxSchedulerPlanOtsustva.Name,dxComponentPrinter1Link2);
end;

procedure TfrmPlanZaOtsustvo.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPlanZaOtsustvo.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmPlanZaOtsustvo.aZapisiExecute(Sender: TObject);
var poraka, ostanatiTip:integer;
    sod,sdo:string;
begin
     if Validacija(Panel2) = false then
         begin
           dmOtsustvo.tblPlanOtsustvaTIP_ZAPIS.Value:=0;
           dmOtsustvo.tblPlanOtsustva.Post;
           StateActive:=dsBrowse;
           RestoreControls(Panel2);
           brDenovi.Visible:=false;

           Panel4.Enabled:=true;
           Panel2.Enabled:=false;
           tcPregled.Enabled:=True;
           cxGrid1.SetFocus;
           dmOtsustvo.tblPlanOtsustva2.FullRefresh;
//           if STATUS.Text = '�������' then
//              begin
//                frmDaNe := TfrmDaNe.Create(self, '��������� �� ��������', '���� ������ ��� ���� �� �������o �� �� ���������� �� ��������� �� ��������?', 1);
//                if (frmDaNe.ShowModal = mrYes) then
//                    begin
//                      dmSis.pInsertOtsustvo.ParamByName('PRICINA_OPIS').Value:=dmOtsustvo.tblPlanOtsustvaPRICINA_OPIS.Value;
//                      dmSis.pInsertOtsustvo.ParamByName('PRICINA').Value:=dmOtsustvo.tblPlanOtsustvaPRICINA.Value;
//                      dmSis.pInsertOtsustvo.ParamByName('OD_VREME').Value:=dmOtsustvo.tblPlanOtsustvaOD_VREME.Value;
//                      dmSis.pInsertOtsustvo.ParamByName('DO_VREME').Value:=dmOtsustvo.tblPlanOtsustvaDO_VREME.Value;
//                      dmSis.pInsertOtsustvo.ParamByName('MB').Value:=dmOtsustvo.tblPlanOtsustvaMB.Value;
//                      dmSis.pInsertOtsustvo.ParamByName('DO_VREME_L').Value:=dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value;
//                      dmSis.pInsertOtsustvo.ExecProc;
//                    end;
//              end;
        end;
end;

procedure TfrmPlanZaOtsustvo.brDenoviExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (OD_VREME.Text <> '') and (brDenovi.Text <> '') and (MB.Text <> '') then
        begin
          //dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'DATUM_OD', 'BROJ', 'MB', Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, StrToInt(brDenovi.Text), dmOtsustvo.tblPlanOtsustvaMB.Value, Null, 'DATUM_DO');
          dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'OD_DATUM', 'DENOVI', 'MB', Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, StrToInt(brDenovi.Text), dmOtsustvo.tblPlanOtsustvaMB.Value, Null, 'DO_DATUM');
        end;
end;

procedure TfrmPlanZaOtsustvo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
     dm.viewVraboteni.ParamByName('mb').Value:='%';
     dm.viewVraboteni.ParamByName('re').Value:='%';
     dm.viewVraboteni.FullRefresh;
end;

procedure TfrmPlanZaOtsustvo.FormCreate(Sender: TObject);
begin
     //Position:=poDesktopCenter;
     dmMat.tblStatusGrupa.Open;
     dmMat.tblStatusGrupa.Locate('APP; TABELA', VarArrayOf([dmkon.aplikacija, 'HR_OTSUSTVA']), []);
     StatGrupaPlanOts:= dmMat.tblStatusGrupaID.Value;
     //dmMat.tblStatusGrupa.Close;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;

     cbGodina.Text:=IntToStr(dmKon.godina);

     dmOtsustvo.tblPlanOtsustva.close;
     dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
     dmOtsustvo.tblPlanOtsustva.ParamByName('re').Value:='%';
     dmOtsustvo.tblPlanOtsustva.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblPlanOtsustva.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
     dmOtsustvo.tblPlanOtsustva.Open;

     dmOtsustvo.tblPlanOtsustva2.close;
     dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.ParamByName('re').Value:='%';
     dmOtsustvo.tblPlanOtsustva2.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblPlanOtsustva2.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
     dmOtsustvo.tblPlanOtsustva2.Open;

     dmOtsustvo.tblTipOtsustvo.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblTipOtsustvo.Open;
     dmOtsustvo.tblStatus.close;
     dmOtsustvo.tblStatus.ParamByName('GRUPA').Value:=StatGrupaPlanOts;
     dmOtsustvo.tblStatus.Open;

     StateActive:=dsBrowse;
end;

procedure TfrmPlanZaOtsustvo.FormShow(Sender: TObject);
begin
     dxBarManager1Bar1.Caption := Caption;
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     procitajPrintOdBaza(Name,cxSchedulerPlanOtsustva.Name, dxComponentPrinter1Link2);

     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.Open;

     cxBarEditRabEdinica.EditValue:=firma;
end;

procedure TfrmPlanZaOtsustvo.MBExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
      if (OD_VREME.Text <> '') and (DO_VREME .Text <> '')then
        begin
          denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value, Null,'DENOVI');
          casovi:= denovi * 8;
          den.Caption:=IntToStr(denovi);
          Cas.Caption:=IntToStr(casovi);
        end;
      TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPlanZaOtsustvo.OD_VREMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     TEdit(Sender).Color:=clWhite;
     if StateActive in [dsEdit, dsInsert] then
        begin
          if (OD_VREME.Text <> '') and (DO_VREME_L.Text <> '') then
             begin
               if DO_vreme_L.Date < OD_vreme.Date then
                 begin
                   ShowMessage('������ � �������� ������ !!!');
                   OD_vreme.SetFocus;
                 end
               else if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value, Null,'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
        end;
end;

procedure TfrmPlanZaOtsustvo.RadioButton1Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewDay.Active:=True;
end;

procedure TfrmPlanZaOtsustvo.RadioButton2Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewWeek.Active:=True;
end;

procedure TfrmPlanZaOtsustvo.RadioButton3Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewWeeks.Active:=True;
end;

procedure TfrmPlanZaOtsustvo.RadioButton4Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewYear.Active:=True;
end;

procedure TfrmPlanZaOtsustvo.RadioButton5Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewGantt.Active:=True;
end;

procedure TfrmPlanZaOtsustvo.RadioButton6Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewTimeGrid.Active:=True;
end;

procedure TfrmPlanZaOtsustvo.statusprebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
       end;
     if TipOtsustvoPrebaraj.Text<> '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
       end;
     if statusprebaraj.Text <> '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:=statusprebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:=statusprebaraj.EditValue;
       end;
     if statusprebaraj.Text = '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
        dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
       end;
     dmOtsustvo.tblPlanOtsustva.FullRefresh;
     dmOtsustvo.tblPlanOtsustva2.FullRefresh;
end;

procedure TfrmPlanZaOtsustvo.TipOtsustvoPrebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
       end;
     if TipOtsustvoPrebaraj.Text<> '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
       end;
     if statusprebaraj.Text <> '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:=statusprebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:=statusprebaraj.EditValue;
       end;
     if TipOtsustvoPrebaraj.Text = '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
        dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
       end;
     dmOtsustvo.tblPlanOtsustva.FullRefresh;
     dmOtsustvo.tblPlanOtsustva2.FullRefresh;
end;

procedure TfrmPlanZaOtsustvo.VRABOTENIMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
      if (OD_VREME.Text <> '') and (DO_VREME .Text <> '')then
        begin
          denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', Null,dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value,Null, 'DENOVI');
          casovi:= denovi * 8;
          den.Caption:=IntToStr(denovi);
          Cas.Caption:=IntToStr(casovi);
        end;
      TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPlanZaOtsustvo.vrabotenprebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
       end;
     if TipOtsustvoPrebaraj.Text<> '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
       end;
     if statusprebaraj.Text <> '' then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:=statusprebaraj.EditValue;
        dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:=statusprebaraj.EditValue;
       end;
     if (vrabotenprebaraj.Text = '') then
       begin
        dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
        dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
       end;
     dmOtsustvo.tblPlanOtsustva.FullRefresh;
     dmOtsustvo.tblPlanOtsustva2.FullRefresh;
end;

procedure TfrmPlanZaOtsustvo.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
           if (Sender = PRICINANAZIV) then
              begin
                frmTipOtsustvo:=TfrmTipOtsustvo.Create(Application);
                frmTipOtsustvo.Tag:=1;
                frmTipOtsustvo.ShowModal;
                frmTipOtsustvo.Free;
                dmOtsustvo.tblPlanOtsustvaPRICINA.Value:=dmOtsustvo.tblTipOtsustvoID.Value;
              end;
        end;
    end;
end;

procedure TfrmPlanZaOtsustvo.cxDBTextEditAllExit(Sender: TObject);
begin

     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPlanZaOtsustvo.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
      if (StateActive in [dsBrowse]) and (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
           if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
              begin
                pom_od:=OD_VREME.Text;
                pom_do:=DO_VREME.Text;

                cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                if cas_od = cas_do then
                   begin
                     casovi:=0;
                     minuti:= minuta_do - minuta_od;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                     Den.Caption:='0';
                   end
                else
                   begin
                     minuti:= 60 - minuta_od + minuta_do;
                     if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                     Den.Caption:='0';
                   end;
              end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value, Null,'DENOVI');;
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
        end;
end;

procedure TfrmPlanZaOtsustvo.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPlanZaOtsustvo.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if cxPageControl1.ActivePage = tcPregled then
        cxSchedulerPlanOtsustva.SetFocus;
end;

procedure TfrmPlanZaOtsustvo.cxSchedulerPlanOtsustvaDblClick(Sender: TObject);
begin
     if vrabotenprebaraj.Text <> '' then
       if statusprebaraj.Text <> '' then
          if TipOtsustvoPrebaraj.Text <> '' then
             begin
                dmOtsustvo.tblPlanOtsustva.Insert;
                dmOtsustvo.tblPlanOtsustvaTIP_ZAPIS.Value:=0;
                dmOtsustvo.tblPlanOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                dmOtsustvo.tblPlanOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                dmOtsustvo.tblPlanOtsustvaSTATUS.Value:=statusprebaraj.EditValue;
                dmOtsustvo.tblPlanOtsustvaOD_VREME.Value:=cxSchedulerPlanOtsustva.SelStart;
                dmOtsustvo.tblPlanOtsustvaDO_VREME.Value:=cxSchedulerPlanOtsustva.SelFinish;
                dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value:=cxSchedulerPlanOtsustva.SelFinish -1;
                dmOtsustvo.tblPlanOtsustva.Post;
                dmOtsustvo.tblPlanOtsustva.FullRefresh;
                dmOtsustvo.tblPlanOtsustva2.FullRefresh;
                StateActive:=dsBrowse;
             end
          else ShowMessage('�������� ��� �� �������� !!!')
       else ShowMessage('�������� ������ !!!')
     else  ShowMessage('�������� ���� !!!')
end;

procedure TfrmPlanZaOtsustvo.DO_VREMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     TEdit(Sender).Color:=clWhite;
     if StateActive in [dsEdit, dsInsert] then
        begin
           if (OD_VREME.Text <> '') and (DO_VREME_L.Text <> '') then
             begin

               dmOtsustvo.tblPlanOtsustvaDO_VREME.Value:=dmOtsustvo.tblPlanOtsustvaDO_VREME_L.Value + 1;
               if DO_vreme_L.Date < OD_vreme.Date then
                 begin
                   ShowMessage('������ � �������� ������ !!!');
                   DO_vreme_L.SetFocus;
                 end
               else if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;

                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', Null,dmOtsustvo.tblPlanOtsustvaOD_VREME.Value, dmOtsustvo.tblPlanOtsustvaDO_VREME.Value, dmOtsustvo.tblPlanOtsustvaMB.Value, Null,'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
        end;
end;

procedure TfrmPlanZaOtsustvo.dxBarButton3Click(Sender: TObject);
begin
     dxComponentPrinter1Link1.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPlanZaOtsustvo.cbGodinaChange(Sender: TObject);
begin
   if cbGodina.Text <> '' then
    begin
     vrabotenprebaraj.Text:='';
     statusprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     if cxBarEditRabEdinica.EditValue = firma then
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:='%';
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('re').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva.FullRefresh;

          dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('re').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva2.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva2.FullRefresh;

          cxSchedulerPlanOtsustva.GoToDate(StrToDate('01.01.'+cbGodina.Text));
        end
     else
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:=cxBarEditRabEdinica.EditValue;
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblPlanOtsustva.FullRefresh;

          dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva2.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblPlanOtsustva2.FullRefresh;

          cxSchedulerPlanOtsustva.GoToDate(StrToDate('01.01.'+cbGodina.Text));
        end;
    end;
end;

procedure TfrmPlanZaOtsustvo.cxBarEditRabEdinicaChange(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     statusprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     if cxBarEditRabEdinica.EditValue = firma then
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:='%';
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('re').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva.FullRefresh;

          dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('re').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva2.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva2.FullRefresh;

        end
     else
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:=cxBarEditRabEdinica.EditValue;
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblPlanOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblPlanOtsustva.FullRefresh;

          dmOtsustvo.tblPlanOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblPlanOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('param_godina').Value:=StrToInt(cbGodina.Text);
          dmOtsustvo.tblPlanOtsustva2.ParamByName('status').Value:='%';
          dmOtsustvo.tblPlanOtsustva2.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblPlanOtsustva2.FullRefresh;

        end;
end;

procedure TfrmPlanZaOtsustvo.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

end.
