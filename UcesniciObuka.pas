unit UcesniciObuka;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.0.0.0                   }
{                                       }
{   07.04.2010                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, ExtCtrls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxBlobEdit, cxGridCustomPopupMenu, cxGridPopupMenu, cxColorComboBox,
  cxContainer, Menus, cxScheduler, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerCustomResourceView, cxSchedulerDayView,
  cxSchedulerDateNavigator, cxSchedulerHolidays, cxSchedulerTimeGridView,
  cxSchedulerUtils, cxSchedulerWeekView, cxSchedulerYearView,
  cxSchedulerGanttView,  dxSkinsdxBarPainter,
  cxDBLookupComboBox, dxBar, ActnList, cxBarEditItem, cxGroupBox, cxRadioGroup,
  StdCtrls, cxMemo, cxDBEdit, cxButtons, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxMaskEdit, cxCalendar, cxTextEdit, cxPC, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,cxLabel, cxHint,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk,
  dxPScxPivotGridLnk, dxPSdxDBOCLnk, dxScreenTip, dxCustomHint;

type
  TfrmUcesniciObuka = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridDBTableView1MB: TcxGridDBColumn;
    cxGridDBTableView1OCENKA: TcxGridDBColumn;
    cxGridDBTableView1SERTIFIKATI: TcxGridDBColumn;
    cxGridDBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    dxBarManager1: TdxBarManager;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarButton6: TdxBarButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    cxBarEditRabEdinica: TcxBarEditItem;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton7: TdxBarButton;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    cxBarEditVraboten: TcxBarEditItem;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarLargeButton34: TdxBarLargeButton;
    dxBarLargeButton35: TdxBarLargeButton;
    dxBarLargeButton36: TdxBarLargeButton;
    dxBarLargeButton37: TdxBarLargeButton;
    dxBarLargeButton38: TdxBarLargeButton;
    ActionList1: TActionList;
    aEvidentirajObuka: TAction;
    aDodadiPlanZaOtsustvo: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aIzlez: TAction;
    aAzurirajPlan: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    aSnimiGoIzgledot: TAction;
    aZacuvajVoExcel: TAction;
    aPecatenje: TAction;
    aPecatiTabela: TAction;
    aStatusGrupa: TAction;
    aStatus: TAction;
    aPrebaraj: TAction;
    aIscisti: TAction;
    aPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aSnimiPecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPomos: TAction;
    aUcesnici: TAction;
    dxBarLargeButton39: TdxBarLargeButton;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton40: TdxBarLargeButton;
    dxBarLargeButton41: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton42: TdxBarLargeButton;
    dxBarLargeButton43: TdxBarLargeButton;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton44: TdxBarLargeButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton45: TdxBarLargeButton;
    dxBarLargeButton46: TdxBarLargeButton;
    dxBarSubItem6: TdxBarSubItem;
    dxBarLargeButton47: TdxBarLargeButton;
    dxBarLargeButton48: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton49: TdxBarLargeButton;
    dxBarLargeButton50: TdxBarLargeButton;
    dxBarSubItem7: TdxBarSubItem;
    dxBarButton8: TdxBarButton;
    Panel8: TPanel;
    GroupBox1: TGroupBox;
    Ocenka: TcxDBMemo;
    cxLabel3: TcxLabel;
    cxLabel1: TcxLabel;
    Sertifikati: TcxDBMemo;
    dxBarLargeButton51: TdxBarLargeButton;
    cxHintStyleController1: TcxHintStyleController;
    cxLabel2: TcxLabel;
    cxLabel4: TcxLabel;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1OBUKA_ID: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1RM_RE_ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aIzlezExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aSnimiGoIzgledotExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aAzurirajPlanExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUcesniciObuka: TfrmUcesniciObuka;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnitOtsustvo, Utils,
  dmUnit;

{$R *.dfm}

procedure TfrmUcesniciObuka.aAzurirajPlanExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          if cxGridDBTableView1.Controller.SelectedRecordCount = 1 then
             begin
               Panel8.Visible:=True;
               Panel8.Top:= 261;
               panel8.Left:= 19;
               dmOtsustvo.tblObukaUcesnici.Edit;
               StateActive:=dsEdit;
               Ocenka.SetFocus;
             end
          else ShowMessage('����������� ������� !!!');
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmUcesniciObuka.aBrisiExecute(Sender: TObject);
begin
     if cxGridDBTableView1.Controller.SelectedRecordCount = 1 then
       begin
         if StateActive in [dsBrowse] then
            begin
              cxGridDBTableView1.DataController.DataSet.Delete();
            end
         else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
       end
     else ShowMessage('����������� ������� !!!');
end;

procedure TfrmUcesniciObuka.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmUcesniciObuka.aIzlezExecute(Sender: TObject);
begin
     if (StateActive in [dsInsert, dsEdit])and (Panel8.Visible = true) then
        begin
          dmOtsustvo.tblObukaUcesnici.Cancel;
          StateActive:=dsBrowse;
          Panel8.Visible:=False;
          cxGrid2.SetFocus;
        end
     else Close;
end;

procedure TfrmUcesniciObuka.aOsveziExecute(Sender: TObject);
begin
      dmOtsustvo.tblObukaUcesnici.Refresh;
end;

procedure TfrmUcesniciObuka.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmUcesniciObuka.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmUcesniciObuka.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmUcesniciObuka.aSnimiGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGridDBTableView1);
end;

procedure TfrmUcesniciObuka.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmUcesniciObuka.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, Caption);
end;

procedure TfrmUcesniciObuka.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var br:Integer;
begin
     case Key of
        VK_RETURN:begin
          br:=dmOtsustvo.zemiBroj(dmOtsustvo.CountUcesnikObuka,'OBUKAID_IN', 'MB_IN',NULL,Null, dmOtsustvo.tblObukiID.Value, dm.viewVraboteniMB.Value, Null,NULL, 'COUNT_OUT');
          if br = 0 then
             begin
               Panel8.Visible:=True;
               Panel8.Top:= 247;
               Panel8.Left:= 459;
               Ocenka.SetFocus;
               dmOtsustvo.tblObukaUcesnici.Insert;
               StateActive:=dsInsert;
               dmOtsustvo.tblObukaUcesniciMB.Value:=dm.viewVraboteniMB.Value;
               dmOtsustvo.tblObukaUcesniciOBUKA_ID.Value:=dmOtsustvo.tblObukiID.Value;
             end
          else ShowMessage('��� �������� ��� � ����������� �� ���� �����');
        end;
     end;
end;

procedure TfrmUcesniciObuka.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmOtsustvo.tblObukaUcesnici.Close;

end;

procedure TfrmUcesniciObuka.FormCreate(Sender: TObject);
begin

     dmOtsustvo.tblObukaUcesnici.ParamByName('obukaID').Value:=dmOtsustvo.tblObukiID.Value;
     dmOtsustvo.tblObukaUcesnici.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;

     StateActive:=dsBrowse;
end;

procedure TfrmUcesniciObuka.FormShow(Sender: TObject);
begin
      procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
      procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
      Caption:='�������� �� ����� - '+ dmOtsustvo.tblObukiNAZIV.Value;
end;

procedure TfrmUcesniciObuka.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmUcesniciObuka.cxDBTextEditAllExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmUcesniciObuka.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          if Sender = Sertifikati then
             begin
               dmOtsustvo.tblObukaUcesnici.Post;
               dmOtsustvo.tblObukaUcesnici.FullRefresh;
               StateActive:=dsBrowse;
               Panel8.Visible:=False;
             end;
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin

        end;
    end;
end;

end.
