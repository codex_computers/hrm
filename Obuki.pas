unit Obuki;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.0.0.0                   }
{                                       }
{   01.04.2010                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxContainer, cxEdit, Menus, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxColorComboBox,
  cxScheduler, cxSchedulerStorage, cxSchedulerCustomControls,
  cxSchedulerCustomResourceView, cxSchedulerDayView, cxSchedulerDateNavigator,
  cxSchedulerHolidays, cxSchedulerTimeGridView, cxSchedulerUtils,
  cxSchedulerWeekView, cxSchedulerYearView, cxSchedulerGanttView,
  dxSkinsdxBarPainter, cxDBLookupComboBox, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, dxPSCore, dxPScxCommon,  dxBar, cxBarEditItem,
  cxClasses, cxSchedulerDBStorage, cxGroupBox, cxRadioGroup, StdCtrls, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, cxMemo, cxDBEdit, cxButtons, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxMaskEdit, cxCalendar, cxLabel, cxTextEdit,
  cxPC, cxGridCustomPopupMenu, cxGridPopupMenu, ActnList, cxBlobEdit, cxHint,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxPCdxBarPopupMenu, dxSkinscxSchedulerPainter, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxCustomHint, dxPScxSchedulerLnk,
  dxPScxPivotGridLnk, dxPSdxDBOCLnk;

type
  TfrmObuki = class(TForm)
    ActionList1: TActionList;
    aEvidentirajObuka: TAction;
    aDodadiPlanZaOtsustvo: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aIzlez: TAction;
    aAzurirajPlan: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    aSnimiGoIzgledot: TAction;
    aZacuvajVoExcel: TAction;
    aPecatenje: TAction;
    aPecatiTabela: TAction;
    aStatusGrupa: TAction;
    aStatus: TAction;
    aPrebaraj: TAction;
    aIscisti: TAction;
    aPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aSnimiPecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPomos: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxPageControl1: TcxPageControl;
    tcPlanOtsustvo: TcxTabSheet;
    Panel2: TPanel;
    Label15: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    Sifra: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    DO_VREME: TcxDBDateEdit;
    OD_VREME: TcxDBDateEdit;
    STATUS: TcxDBLookupComboBox;
    buttonOtkazi: TcxButton;
    buttonZapisi: TcxButton;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    OPIS: TcxDBMemo;
    Panel4: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    tcPregled: TcxTabSheet;
    Panel3: TPanel;
    cxSchedulerPlanOtsustva: TcxScheduler;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1BOJA: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton6: TRadioButton;
    cxRadioGroup1: TcxRadioGroup;
    statusprebaraj: TcxLookupComboBox;
    cxSchedulerDBStorage1: TcxSchedulerDBStorage;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar5: TdxBar;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarButton6: TdxBarButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    cxBarEditRabEdinica: TcxBarEditItem;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton7: TdxBarButton;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    cxBarEditVraboten: TcxBarEditItem;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarLargeButton34: TdxBarLargeButton;
    dxBarLargeButton35: TdxBarLargeButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxPodesuvanje: TdxRibbonTab;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1PODGOTVENOST: TcxGridDBColumn;
    cxGrid1DBTableView1CEL: TcxGridDBColumn;
    cxGrid1DBTableView1SREDSTVA: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD_OD: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD_DO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1ODGOVORNO_LICE: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn;
    NAZIV: TcxDBTextEdit;
    CEL: TcxDBMemo;
    Label5: TLabel;
    PODGOTVENOST: TcxDBMemo;
    Label10: TLabel;
    ODGOVORNO_ICE: TcxDBMemo;
    Label11: TLabel;
    Label12: TLabel;
    MESTO: TcxDBMemo;
    Label13: TLabel;
    Label14: TLabel;
    ZABELESKA: TcxDBMemo;
    Label16: TLabel;
    SREDSTVA: TcxDBTextEdit;
    ButtonIscisti: TcxButton;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton36: TdxBarLargeButton;
    aUcesnici: TAction;
    dxBarLargeButton37: TdxBarLargeButton;
    dxBarLargeButton38: TdxBarLargeButton;
    dxBarLargeButton39: TdxBarLargeButton;
    dxBarButton8: TdxBarButton;
    aObukiPregled: TAction;
    cxHintStyleController1: TcxHintStyleController;
    cxLabel1: TcxLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    aFormConfig: TAction;
    Label7: TLabel;
    Dokument: TcxDBTextEdit;
    cxGrid1DBTableView1DOKUMENT: TcxGridDBColumn;
    cxButton1: TcxButton;
    aOtvoridDokument: TAction;
    OpenDialog1: TOpenDialog;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure aDodadiPlanZaOtsustvoExecute(Sender: TObject);
    procedure aAzurirajPlanExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aSnimiGoIzgledotExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure aEvidentirajObukaExecute(Sender: TObject);
    procedure aIscistiExecute(Sender: TObject);
    procedure statusprebarajPropertiesEditValueChanged(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aUcesniciExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OPISDblClick(Sender: TObject);
    procedure CELDblClick(Sender: TObject);
    procedure PODGOTVENOSTDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZABELESKADblClick(Sender: TObject);
    procedure OD_VREMEExit(Sender: TObject);
    procedure DO_VREMEExit(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure aOtvoridDokumentExecute(Sender: TObject);
   // procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmObuki: TfrmObuki;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit,
  dmUnitOtsustvo, Utils, UcesniciObuka, Notepad, ObukiPregled, FormConfig;

{$R *.dfm}

procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
begin
     Assert(Assigned(Strings)) ;
     Strings.Clear;
     Strings.Delimiter := Delimiter;
     Strings.DelimitedText := Input;
end;


procedure TfrmObuki.aAzurirajPlanExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          Panel2.Enabled:=True;
          Panel4.Enabled:=False;
          tcPregled.Enabled:=False;
          dmOtsustvo.tblObuki.Edit;
          StateActive:=dsEdit;
          NAZIV.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmObuki.aBrisiExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmObuki.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmObuki.aDodadiPlanZaOtsustvoExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          Panel2.Enabled:=True;
          Panel4.Enabled:=False;
          tcPregled.Enabled:=False;
          dmOtsustvo.tblObuki.Insert;
          StateActive:=dsInsert;
          NAZIV.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmObuki.aEvidentirajObukaExecute(Sender: TObject);
begin
     cxPageControl1.ActivePage:=tcPlanOtsustvo;
     Panel2.Enabled:=True;
     Panel4.Enabled:=False;
     tcPregled.Enabled:=False;
     dmOtsustvo.tblObuki.Insert;
     StateActive:=dsInsert;

     dmOtsustvo.tblObukiPERIOD_OD.Value:= cxSchedulerPlanOtsustva.SelStart;
     dmOtsustvo.tblObukiPERIOD_DO.Value:= cxSchedulerPlanOtsustva.SelFinish;

     NAZIV.SetFocus;
end;

procedure TfrmObuki.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmObuki.aIscistiExecute(Sender: TObject);
begin
     statusprebaraj.Text:='';
     dmOtsustvo.tblObuki.ParamByName('status').Value:='%';
     dmOtsustvo.tblObuki.FullRefresh;
end;

procedure TfrmObuki.aIzlezExecute(Sender: TObject);
begin
      if StateActive in [dsEdit,dsInsert] then
        begin
          dmOtsustvo.tblObuki.Cancel;
          StateActive:=dsBrowse;
          RestoreControls(Panel2);
          Panel4.Enabled:=true;
          Panel2.Enabled:=false;
          tcPregled.Enabled:=True;
          cxGrid1.SetFocus;
        end
     else Close;
end;

procedure TfrmObuki.aOsveziExecute(Sender: TObject);
begin
     dmOtsustvo.tblObuki.Refresh;
end;

procedure TfrmObuki.aOtkaziExecute(Sender: TObject);
begin
     dmOtsustvo.tblObuki.Cancel;
     StateActive:=dsBrowse;
     RestoreControls(Panel2);
     Panel4.Enabled:=true;
     Panel2.Enabled:=false;
     tcPregled.Enabled:=True;
     cxGrid1.SetFocus;
end;

procedure TfrmObuki.aOtvoridDokumentExecute(Sender: TObject);
var pom:string;
    i, pom1, dolzina:integer;
    A: TStringList;
begin
//      pom:=dm.dmOtsustvo.tblObukiDOKUMENT.Value;
//      A := TStringList.Create;
//      try
//        Split(',', pom, A) ;
//        dolzina:= a.Count;
//        for i := 0 to dolzina - 2 do
//           if IntToStr(dmKon.re) = a[i] then
//              begin
//                     pom1:=1;
//                  end
//         finally
//            A.Free;
//         end;
     openDialog1.Filter := 'PDF ��� Word ���������|*.txt;*.doc;*.pdf';
     if Dokument.Text <> '' then
       begin
        OpenDialog1.InitialDir:=dmOtsustvo.tblObukiDOKUMENT.Value;
        OpenDialog1.Execute();
       end

     else if OpenDialog1.Execute then
        begin
          dmOtsustvo.tblObukiDOKUMENT.Value:=OpenDialog1.FileName;
        end;
end;

procedure TfrmObuki.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmObuki.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmObuki.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmObuki.aSnimiGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmObuki.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmObuki.aUcesniciExecute(Sender: TObject);
begin
     if cxGrid1DBTableView1.Controller.SelectedRecordCount = 1 then
        begin
          frmUcesniciObuka:=TfrmUcesniciObuka.Create(Application);
          frmUcesniciObuka.ShowModal;
          frmUcesniciObuka.Free;
        end
     else ShowMessage('����������� �����');
end;

procedure TfrmObuki.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmObuki.aZapisiExecute(Sender: TObject);
begin
     if Validacija(Panel2) = false then
        begin
          if (OD_VREME.Text <> '')and (DO_VREME.Text <> '') then
             if DO_vreme.Date < OD_vreme.Date then
               begin
                 ShowMessage('������ � �������� ������ !!!');
                 DO_vreme.SetFocus;
               end
          else
             begin
               dmOtsustvo.tblObuki.Post;
               StateActive:=dsBrowse;
               RestoreControls(Panel2);
               Panel4.Enabled:=true;
               Panel2.Enabled:=false;
               tcPregled.Enabled:=True;
               dmOtsustvo.tblObuki.FullRefresh;
               cxGrid1.SetFocus;
             end;
        end;
end;

procedure TfrmObuki.CELDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblObukiCEL.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
       begin
	       dmOtsustvo.tblObukiCEL.Value := frmNotepad.ReturnText;
       end;
  frmNotepad.Free;
end;

procedure TfrmObuki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dmOtsustvo.tblObuki.Close;
end;

procedure TfrmObuki.FormCreate(Sender: TObject);
begin
     dmOtsustvo.tblObuki.ParamByName('status').Value:='%';
     dmOtsustvo.tblObuki.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;

     dmMat.tblStatusGrupa.Open;
     dmMat.tblStatusGrupa.Locate('APP; TABELA', VarArrayOf([dmkon.aplikacija, 'HR_OBUKI']), []);
     StatGrupaObuki:= dmMat.tblStatusGrupaID.Value;
     //dmMat.tblStatusGrupa.Close;

     dmOtsustvo.tblStatus.close;
     dmOtsustvo.tblStatus.ParamByName('GRUPA').Value:=StatGrupaObuki;
     dmOtsustvo.tblStatus.Open;

     StateActive:=dsBrowse;
end;

procedure TfrmObuki.FormShow(Sender: TObject);
begin
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmObuki.OD_VREMEExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmObuki.OPISDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblObukiOPIS.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
       begin
	       dmOtsustvo.tblObukiOPIS.Value := frmNotepad.ReturnText;
       end;
  frmNotepad.Free;
end;

procedure TfrmObuki.PODGOTVENOSTDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblObukiPODGOTVENOST.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
       begin
	       dmOtsustvo.tblObukiPODGOTVENOST.Value := frmNotepad.ReturnText;
       end;
  frmNotepad.Free;
end;

procedure TfrmObuki.RadioButton1Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewDay.Active:=True;
end;

procedure TfrmObuki.RadioButton2Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewWeek.Active:=True;
end;

procedure TfrmObuki.RadioButton3Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewWeeks.Active:=True;
end;

procedure TfrmObuki.RadioButton4Click(Sender: TObject);
begin
      cxSchedulerPlanOtsustva.ViewYear.Active:=True;
end;

procedure TfrmObuki.RadioButton5Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewGantt.Active:=True;
end;

procedure TfrmObuki.RadioButton6Click(Sender: TObject);
begin
     cxSchedulerPlanOtsustva.ViewTimeGrid.Active:=True;
end;

procedure TfrmObuki.statusprebarajPropertiesEditValueChanged(Sender: TObject);
begin
     if statusprebaraj.Text <> '' then
        dmOtsustvo.tblObuki.ParamByName('status').Value:=statusprebaraj.EditValue;
     if statusprebaraj.Text = '' then
        dmOtsustvo.tblObuki.ParamByName('status').Value:='%';
     dmOtsustvo.tblObuki.FullRefresh;
end;

procedure TfrmObuki.ZABELESKADblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblObukiZABELESKA.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
       begin
	       dmOtsustvo.tblObukiZABELESKA.Value := frmNotepad.ReturnText;
       end;
  frmNotepad.Free;
end;

procedure TfrmObuki.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
        end;
    end;
end;

procedure TfrmObuki.cxDBTextEditAllExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmObuki.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     case key of
            VK_INSERT:begin
              frmUcesniciObuka:=TfrmUcesniciObuka.Create(Application);
              frmUcesniciObuka.ShowModal;
              frmUcesniciObuka.Free;
            end;
     end;
end;

procedure TfrmObuki.DO_VREMEExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;

end;

procedure TfrmObuki.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;

end.
