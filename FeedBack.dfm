object frmFeedBack: TfrmFeedBack
  Left = 0
  Top = 0
  Caption = '360 '#1055#1086#1074#1088#1072#1090#1085#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1112#1072
  ClientHeight = 654
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 747
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 631
    Width = 747
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', Insert - '#1044#1086#1076#1072#1076 +
          #1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 747
    Height = 245
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 2
    object cxPageControl1: TcxPageControl
      Left = 2
      Top = 2
      Width = 743
      Height = 241
      Align = alClient
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      Properties.ActivePage = cxTabSheet1
      ClientRectBottom = 241
      ClientRectRight = 743
      ClientRectTop = 24
      object cxTabSheet1: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 0
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 743
          Height = 217
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid2DBTableView1KeyPress
            DataController.DataSource = dmUcinok.dsFeedBack
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            object cxGrid2DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 74
            end
            object cxGrid2DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
            end
            object cxGrid2DBTableView1DATUM_REVIZIJA: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_REVIZIJA'
              Width = 121
            end
            object cxGrid2DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Width = 477
            end
            object cxGrid2DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid2DBTableView1ID_RE_FIRMA: TcxGridDBColumn
              DataBinding.FieldName = 'ID_RE_FIRMA'
              Visible = False
            end
            object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 743
          Height = 217
          Align = alClient
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          DesignSize = (
            743
            217)
          object Label1: TLabel
            Left = 5
            Top = 3
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1064#1080#1092#1088#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object Sifra: TcxDBTextEdit
            Tag = 1
            Left = 61
            Top = 0
            BeepOnEnter = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dmUcinok.dsFeedBack
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            Visible = False
            Width = 80
          end
          object ZapisiButton: TcxButton
            Left = 526
            Top = 173
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 2
            OnKeyDown = EnterKakoTab
          end
          object OtkaziButton: TcxButton
            Left = 607
            Top = 172
            Width = 75
            Height = 25
            Action = aOtkazi1
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 3
            OnKeyDown = EnterKakoTab
          end
          object cxGroupBox1: TcxGroupBox
            Left = 20
            Top = 14
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = '360 '#1055#1086#1074#1088#1072#1090#1085#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1112#1072
            Style.LookAndFeel.Kind = lfOffice11
            Style.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.Kind = lfOffice11
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.Kind = lfOffice11
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.Kind = lfOffice11
            StyleHot.LookAndFeel.NativeStyle = False
            TabOrder = 0
            DesignSize = (
              428
              187)
            Height = 187
            Width = 428
            object Label5: TLabel
              Left = -3
              Top = 73
              Width = 50
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1054#1087#1080#1089' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 217
              Top = 22
              Width = 50
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label2: TLabel
              Left = -3
              Top = 22
              Width = 50
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label3: TLabel
              Left = 145
              Top = 49
              Width = 122
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1074#1080#1079#1080#1112#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object OPIS: TcxDBMemo
              Left = 53
              Top = 73
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataBinding.DataField = 'OPIS'
              DataBinding.DataSource = dmUcinok.dsFeedBack
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Height = 96
              Width = 334
            end
            object DATUM: TcxDBDateEdit
              Tag = 1
              Left = 273
              Top = 19
              DataBinding.DataField = 'DATUM'
              DataBinding.DataSource = dmUcinok.dsFeedBack
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 114
            end
            object BROJ: TcxDBTextEdit
              Tag = 1
              Left = 53
              Top = 19
              BeepOnEnter = False
              DataBinding.DataField = 'BROJ'
              DataBinding.DataSource = dmUcinok.dsFeedBack
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 100
            end
            object DATUM_REVIZIJA: TcxDBDateEdit
              Left = 273
              Top = 46
              DataBinding.DataField = 'DATUM_REVIZIJA'
              DataBinding.DataSource = dmUcinok.dsFeedBack
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 114
            end
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 371
    Width = 747
    Height = 260
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    ParentBackground = False
    TabOrder = 7
    object cxPageControl2: TcxPageControl
      Left = 2
      Top = 2
      Width = 743
      Height = 256
      Align = alClient
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      Properties.ActivePage = cxTabSheet3
      ClientRectBottom = 256
      ClientRectRight = 743
      ClientRectTop = 24
      object cxTabSheet3: TcxTabSheet
        Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080
        ImageIndex = 0
        object Panel4: TPanel
          Left = 0
          Top = 192
          Width = 743
          Height = 40
          Align = alBottom
          BevelInner = bvLowered
          BevelOuter = bvSpace
          TabOrder = 0
          DesignSize = (
            743
            40)
          object cxButton1: TcxButton
            Left = 19
            Top = 6
            Width = 75
            Height = 25
            Action = aBrisiIspitanici
            Anchors = []
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 0
            OnKeyDown = EnterKakoTab
          end
          object cxButton4: TcxButton
            Left = 100
            Top = 6
            Width = 178
            Height = 25
            Action = aVnesNaRezultati
            Anchors = []
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 1
            OnKeyDown = EnterKakoTab
          end
        end
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 743
          Height = 192
          Align = alClient
          TabOrder = 1
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            DataController.DataSource = dmUcinok.dsIspitanici
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            object cxGrid1DBTableView1MB: TcxGridDBColumn
              DataBinding.FieldName = 'MB'
              Width = 100
            end
            object cxGrid1DBTableView1VRABOTENNAIV: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
              Width = 637
            end
            object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN'
              Visible = False
              Width = 250
            end
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1FEEDBACK_ID: TcxGridDBColumn
              DataBinding.FieldName = 'FEEDBACK_ID'
              Visible = False
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxTabSheet4: TcxTabSheet
        Caption = #1054#1094#1077#1085#1091#1074#1072#1095#1080
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel5: TPanel
          Left = 0
          Top = 202
          Width = 743
          Height = 40
          Align = alBottom
          BevelInner = bvLowered
          BevelOuter = bvSpace
          TabOrder = 0
          DesignSize = (
            743
            40)
          object cxButton2: TcxButton
            Left = 19
            Top = 6
            Width = 75
            Height = 25
            Action = aBrisiOcenuvaci
            Anchors = []
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 0
            OnKeyDown = EnterKakoTab
          end
        end
        object cxGrid3: TcxGrid
          Left = 0
          Top = 0
          Width = 743
          Height = 192
          Align = alClient
          TabOrder = 1
          ExplicitHeight = 202
          object cxGridDBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid4DBTableView1KeyPress
            DataController.DataSource = dmUcinok.dsOcenuvaci
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            object cxGridDBTableView1OCENUVACNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'OCENUVACNAZIV'
              Width = 372
            end
            object cxGridDBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGridDBTableView1FEEDBACK_ID: TcxGridDBColumn
              DataBinding.FieldName = 'FEEDBACK_ID'
              Visible = False
            end
            object cxGridDBTableView1RATING_FROM_ID: TcxGridDBColumn
              DataBinding.FieldName = 'RATING_FROM_ID'
              Visible = False
            end
            object cxGridDBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGridDBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGridDBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGridDBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridDBTableView1
          end
        end
      end
      object cxTabSheet5: TcxTabSheet
        Caption = #1055#1088#1072#1096#1072#1114#1072
        ImageIndex = 2
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGrid4: TcxGrid
          Left = 0
          Top = 0
          Width = 743
          Height = 202
          Align = alClient
          TabOrder = 0
          object cxGrid4DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid4DBTableView1KeyPress
            DataController.DataSource = dmUcinok.dsPrasalnik
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            object cxGrid4DBTableView1GRUPANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPANAZIV'
              Width = 323
            end
            object cxGrid4DBTableView1PRASANJENAZIV: TcxGridDBColumn
              AlternateCaption = '300'
              DataBinding.FieldName = 'PRASANJENAZIV'
              Width = 418
            end
            object cxGrid4DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid4DBTableView1FEEDBACK_ID: TcxGridDBColumn
              DataBinding.FieldName = 'FEEDBACK_ID'
              Visible = False
            end
            object cxGrid4DBTableView1PRASANJE_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PRASANJE_ID'
              Visible = False
            end
            object cxGrid4DBTableView1GRUPA: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPA'
              Visible = False
            end
            object cxGrid4DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid4DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid4DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGrid4DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
          end
          object cxGrid4Level1: TcxGridLevel
            GridView = cxGrid4DBTableView1
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 202
          Width = 743
          Height = 40
          Align = alBottom
          BevelInner = bvLowered
          BevelOuter = bvSpace
          TabOrder = 1
          DesignSize = (
            743
            40)
          object cxButton3: TcxButton
            Left = 19
            Top = 6
            Width = 75
            Height = 25
            Action = aBrisiPrasanja
            Anchors = []
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 0
            OnKeyDown = EnterKakoTab
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 192
    Top = 240
  end
  object PopupMenu1: TPopupMenu
    Left = 664
    Top = 152
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 488
    Top = 24
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = '360 '#1055#1086#1074#1088#1072#1090#1085#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1112#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 439
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 646
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085#1080'/'#1054#1094#1077#1085#1091#1074#1072#1095#1080'/'#1055#1088#1072#1096#1072#1114#1072
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 774
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      CaptionButtons = <>
      DockedLeft = 369
      DockedTop = 0
      FloatLeft = 774
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiStavka
      Category = 0
      LargeImageIndex = 10
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiStavka
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aAzurirajStavka
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aStavki
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aAnketenList
      Category = 0
      LargeImageIndex = 8
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 416
    Top = 88
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        '')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aDodadiStavka: TAction
      Caption = #1053#1086#1074
      SecondaryShortCuts.Strings = (
        'Ctrl+F5')
    end
    object aZapisiStavka: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
    end
    object aBrisiStavka: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      SecondaryShortCuts.Strings = (
        'Ctrl+F8')
    end
    object aAzurirajStavka: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      SecondaryShortCuts.Strings = (
        'Ctrl+F6')
    end
    object aOtkaziStavka: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
    end
    object aOtkazi1: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkazi1Execute
    end
    object aStavki: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aStavkiExecute
    end
    object aBrisiIspitanici: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiIspitaniciExecute
    end
    object aBrisiOcenuvaci: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiOcenuvaciExecute
    end
    object aBrisiPrasanja: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiPrasanjaExecute
    end
    object aAnketenList: TAction
      Caption = #1040#1085#1082#1077#1090#1077#1085' '#1083#1080#1089#1090
      OnExecute = aAnketenListExecute
    end
    object aVnesNaRezultati: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1042#1085#1077#1089#1080' '#1088#1077#1079#1091#1083#1090#1072#1090#1080
      ImageIndex = 10
      OnExecute = aVnesNaRezultatiExecute
    end
    object aPregledajRezultati: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112' '#1088#1077#1079#1091#1083#1090#1072#1090#1080
      ImageIndex = 19
      OnExecute = aPregledajRezultatiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 720
    Top = 160
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 608
    Top = 128
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 304
    Top = 240
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 88
    Top = 240
  end
  object cxGridPopupMenu4: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 432
    Top = 240
  end
end
