unit Intervju;

(***************************************)
{   ������ �������                     }
{   ������ ��������                    }
{                                       }
{   Version   1.1.1.17                   }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxCalendar, cxHint, cxGroupBox,
  cxRadioGroup, FIBDataSet, pFIBDataSet, cxSplitter, FIBQuery, pFIBQuery,
  cxBlobEdit, cxLabel, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxScreenTip, dxCustomHint;

type
//  niza = Array[1..5] of Variant;

  TfrmIntervju = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    lPanel: TPanel;
    dPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1MB_INTERVJUIST: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_SREDINA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_KRAJ: TcxGridDBColumn;
    cxGrid1DBTableView1INTERVJUISTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MOLBANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BRUCESNICINAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OBLIKNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1SELEKCIJANAZIV: TcxGridDBColumn;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    ID_MOLBANaziv: TcxDBLookupComboBox;
    ID_MOLBA: TcxDBTextEdit;
    Label4: TLabel;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_OBLIK: TcxGridDBColumn;
    cxGrid1DBTableView1ID_BROJ_UCESNICI: TcxGridDBColumn;
    cxGrid1DBTableView1ID_SELEKCIJA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    dxRibbon1Tab3: TdxRibbonTab;
    rgTip: TcxDBRadioGroup;
    tblKategorja: TpFIBDataSet;
    tblKategorjaID: TFIBIntegerField;
    tblKategorjaNAZIV: TFIBStringField;
    tblKategorjaID_RM_RE: TFIBIntegerField;
    tblKategorjaTS_INS: TFIBDateTimeField;
    tblKategorjaTS_UPD: TFIBDateTimeField;
    tblKategorjaUSR_INS: TFIBStringField;
    tblKategorjaUSR_UPD: TFIBStringField;
    tblKategorjaID_RM: TFIBIntegerField;
    tblKategorjaNAZIV_RM: TFIBStringField;
    dsKategorija: TDataSource;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RM_MOLBA: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label5: TLabel;
    MB_INTERVJUIST: TcxDBTextEdit;
    MB_INTERVJUISTNaziv: TcxDBLookupComboBox;
    Label9: TLabel;
    DATUM: TcxDBDateEdit;
    cxGroupBox2: TcxGroupBox;
    Label10: TLabel;
    ID_BROJ_UCESNICI: TcxDBTextEdit;
    ID_BROJ_UCESNICINaziv: TcxDBLookupComboBox;
    ID_SELEKCIJANaZIV: TcxDBLookupComboBox;
    ID_SELEKCIJA: TcxDBTextEdit;
    Label2: TLabel;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    OPIS_POCETOK: TcxDBMemo;
    OPIS_SREDINA: TcxDBMemo;
    OPIS_KRAJ: TcxDBMemo;
    qInsertKategorija: TpFIBQuery;
    tblIntKategorija: TpFIBDataSet;
    dsIntKategorija: TDataSource;
    tblIntKategorijaBROJ: TFIBIntegerField;
    tblIntKategorijaNAZIV: TFIBStringField;
    tblIntKategorijaZABELESKA: TFIBStringField;
    tblIntKategorijaID: TFIBIntegerField;
    tblIntKategorijaID_INTERVJU: TFIBIntegerField;
    tblIntKategorijaID_KATEGORIJA: TFIBIntegerField;
    dxBarManager1Bar5: TdxBar;
    aBrisiPrasanja: TAction;
    qBrisiKategorija: TpFIBQuery;
    dxBarLargeButton10: TdxBarLargeButton;
    PopupMenu2: TPopupMenu;
    aBrisiEdnoP: TAction;
    aBrisiEdnoP1: TMenuItem;
    aDodadiP: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    dxComponentPrinter1Link2: TdxGridReportLink;
    aPecatiP: TAction;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    aAzurirajP: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    gbGridP: TcxGroupBox;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1ID_INTERVJU: TcxGridDBColumn;
    cxGrid2DBTableView1ID_KATEGORIJA: TcxGridDBColumn;
    cxGrid2DBTableView2: TcxGridDBTableView;
    cxGrid2DBTableView2BROJ: TcxGridDBColumn;
    cxGrid2DBTableView2NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView2ZABELESKA: TcxGridDBColumn;
    cxGrid2DBTableView2ID: TcxGridDBColumn;
    cxGrid2DBTableView2ID_INTERVJU: TcxGridDBColumn;
    cxGrid2DBTableView2ID_KATEGORIJA: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    bsStrukturirano: TdxBarSubItem;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    cxButton1: TcxButton;
    aZavrsi: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    Label3: TLabel;
    txtBroj: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ_MOLBA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);


    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);

    procedure FormCreate(Sender: TObject);
    procedure OPIS_POCETOKDblClick(Sender: TObject);
    procedure OPIS_SREDINADblClick(Sender: TObject);
    procedure OPIS_KRAJDblClick(Sender: TObject);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure ProveriTip;
    procedure aBrisiPrasanjaExecute(Sender: TObject);
    procedure aBrisiEdnoPExecute(Sender: TObject);
    procedure ActionList1Execute(Action: TBasicAction; var Handled: Boolean);
    procedure aPecatiPExecute(Sender: TObject);
    procedure aAzurirajPExecute(Sender: TObject);
    procedure gbGridPEnter(Sender: TObject);
    procedure gbGridPExit(Sender: TObject);
    procedure aDodadiPExecute(Sender: TObject);
    procedure aZavrsiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure dxBarLargeButton7Click(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    //������� �� ���������� ������������ _sifra_kluc
  end;

var
  frmIntervju: TfrmIntervju;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija, dmSystem,
  dmUnitOtsustvo, Utils, DaNe, dmUnit, IntBrojUcesnici, IntOblik, IntSelekcija,
  Notepad, Kategorija, FormConfig;



{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIntervju.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmIntervju.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
//  frmDaNe:=TfrmDaNe.Create(self,'����������','���� ��������� � �� �����?',0);
//   if frmDaNe.ShowModal=mrYes then
//    //if rgIzbor.ItemIndex=1 then
//    begin
//      Label4.Visible:=True;
//      ID_MOLBA.Visible:=True;
//      ID_MOLBANaziv.Visible:=True;
//      Label3.Visible:=False;
//      txtRmRe.Visible:=False;
//      cbRmRe.Visible:=False;
//      dPanel.Enabled:=True;
//      lPanel.Enabled:=False;
//      prva.SetFocus;
//    end
//    else
//    begin
//      Label4.Visible:=False;
//      ID_MOLBA.Visible:=False;
//      ID_MOLBANaziv.Visible:=False;
//      Label3.Visible:=True;
//      txtRmRe.Visible:=True;
//      cbRmRe.Visible:=True;
//      dPanel.Enabled:=True;
//      lPanel.Enabled:=False;
//      txtrmre.SetFocus;
//    end;
      dPanel.Enabled:=True;
      lPanel.Enabled:=False;
      prva.SetFocus;
      cxGrid1DBTableView1.DataController.DataSet.Insert;
      dmOtsustvo.tblIntervjuID_OBLIK.Value:=0;
      dmOtsustvo.tblIntervjuBROJ.Value:=dmMat.zemiMax(dmsis.pMaxBrIntervju,Null, Null, Null, Null, Null, Null, 'MAXBR');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmIntervju.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    //lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmIntervju.aAzurirajPExecute(Sender: TObject);
begin
  if dmOtsustvo.tblIntervjuID_OBLIK.Value=1 then
  begin
    Panel1.Enabled:=True;
    gbGridP.Enabled:=True;
    cxGrid2.SetFocus;
  end;
end;

procedure TfrmIntervju.aBrisiEdnoPExecute(Sender: TObject);
begin
if not tblIntKategorija.IsEmpty then
begin
 frmDaNe:=TfrmDaNe.Create(self,'������','���� ��������� ������ �� ���������'+#10#13+' ������� �� ��������� �� '+dmOtsustvo.tblIntervjuMOLBANAZIV.AsString+'?',0);
   if frmDaNe.ShowModal=mrYes then
   begin
      tblIntKategorija.delete;
     // tblIntKategorija.close;
     // tblIntKategorija.open;
   end;
end;
end;

procedure TfrmIntervju.aBrisiExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmIntervju.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIntervju.aDodadiPExecute(Sender: TObject);
var i:integer;
begin
   frmKategorija:=TfrmKategorija.Create(Application,false);
   frmKategorija.tag:=1;
//   dmSis.tblKategorja.close;
//   dmSis.tblKategorja.ParamByName('rm').Value:=dmOtsustvo.tblIntervjuID_RM_RE_MOLBA.Value;
//   dmsis.tblKategorja.ParamByName('firma').asinteger:=firma;
//   dmsis.tblKategorja.Open;
   frmKategorija.ShowModal;
   for i:=0 to frmKategorija.cxGrid1DBTableView1.Controller.SelectedRecordCount-1 do
           begin
                 qInsertKategorija.Close;
                 qInsertKategorija.ParamByName('id_kategorija').Value:=frmKategorija.cxGrid1DBTableView1.Controller.SelectedRecords[i].Values[0];
                 qInsertKategorija.ParamByName('id_intervju').Value:=dmOtsustvo.tblIntervjuID.Value;
                 qInsertKategorija.ExecQuery;

           end;
              frmKategorija.cxGrid1DBTableView1.Controller.ClearSelection;


   frmKategorija.Free;
   tblIntKategorija.Close;
   tblIntKategorija.Open;
end;

procedure TfrmIntervju.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmIntervju.aHelpExecute(Sender: TObject);
begin
      Application.HelpContext(133);
end;

procedure TfrmIntervju.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIntervju.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIntervju.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIntervju.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;

        VK_INSERT:begin
          if (sender = ID_BROJ_UCESNICINaziv) or (sender = ID_BROJ_UCESNICI) then
             begin
               frmIntBrojUcesnici:=TfrmIntBrojUcesnici.Create(Application);
               frmIntBrojUcesnici.Tag:=1;
               frmIntBrojUcesnici.ShowModal;
               frmIntBrojUcesnici.Free;
               dmOtsustvo.tblIntervjuID_BROJ_UCESNICI.Value:=dmOtsustvo.tblIntBrUcesniciID.Value;
             end;
          if (sender = ID_SELEKCIJANaZIV) or (sender = ID_SELEKCIJA) then
             begin
               frmIntSelekcija:=TfrmIntSelekcija.Create(Application);
               frmIntSelekcija.Tag:=1;
               frmIntSelekcija.ShowModal;
               frmIntSelekcija.Free;
               dmOtsustvo.tblIntervjuID_SELEKCIJA.Value:=dmOtsustvo.tblIntSelekcijaID.Value;
             end;

//          if (sender = ID_OBLIKnAZIV) or (sender = ID_OBLIK) then
//             begin
//               frmIntOblik:=TfrmIntOblik.Create(Application);
//               frmIntOblik.Tag:=1;
//               frmIntOblik.ShowModal;
//               frmIntOblik.Free;
//               dmOtsustvo.tblIntervjuID_OBLIK.Value:=dmOtsustvo.tblIntOblikID.Value;
//             end;

        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIntervju.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
    if (Sender = TEdit(OPIS_POCETOK))  or
        (Sender = TEdit(OPIS_SREDINA)) or
        (Sender = TEdit(OPIS_KRAJ)) then
    begin
       dmOtsustvo.tblIntervju.edit;
    end;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIntervju.cxDBTextEditAllExit(Sender: TObject);
begin
    if ((Sender = TEdit(OPIS_POCETOK))  or
        (Sender = TEdit(OPIS_SREDINA)) or
        (Sender = TEdit(OPIS_KRAJ))) and (dmOtsustvo.tblIntervju.State in [dsEdit,dsInsert]) then
    begin
       dmOtsustvo.tblIntervju.Post;
    end;
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmIntervju.prefrli;
begin
end;

procedure TfrmIntervju.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
            dmSis.tblMolba.close;
            dmOtsustvo.tblIntBrUcesnici.Close;
            dmOtsustvo.tblIntSelekcija.Close;
            dmOtsustvo.tblIntOblik.Close;
            dmOtsustvo.tblIntervju.Close;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmIntervju.FormCreate(Sender: TObject);
begin
     dmSis.tblMolba.Close;
     dmSis.tblMolba.ParamByName('ID').Value:='%';
     dmSis.tblMolba.ParamByName('oglasID').Value:='%';
     dmSis.tblMolba.Open;


     dmOtsustvo.tblIntBrUcesnici.Open;
     dmOtsustvo.tblIntSelekcija.Open;
     dmOtsustvo.tblIntOblik.Open;
     dmOtsustvo.tblIntervju.ParamByName('firma').AsInteger:=firma;
     dmOtsustvo.tblIntervju.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmIntervju.FormShow(Sender: TObject);
begin
//	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    if dmOtsustvo.tblIntervjuID_OBLIK.Value=1 then
     begin
      tblIntKategorija.open;
      gbGridP.Visible:=True;
      bsStrukturirano.Enabled:=True;
    end
    else
    begin
       gbGridP.Visible:=False;
       bsStrukturirano.Enabled:=False;
    end;

end;
procedure TfrmIntervju.gbGridPEnter(Sender: TObject);
begin
     gbGridP.Style.Color:=$00D1D1D1;
end;

procedure TfrmIntervju.gbGridPExit(Sender: TObject);
begin
     gbGridP.Style.Color:=$00F2F8F9;
end;

procedure TfrmIntervju.OPIS_KRAJDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblIntervjuOPIS_KRAJ.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
     begin
	     dmOtsustvo.tblIntervjuOPIS_KRAJ.Value := frmNotepad.ReturnText;
     end;
     frmNotepad.Free;
end;

procedure TfrmIntervju.OPIS_POCETOKDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblIntervjuOPIS_POCETOK.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
     begin
	     dmOtsustvo.tblIntervjuOPIS_POCETOK.Value := frmNotepad.ReturnText;
     end;
     frmNotepad.Free;
end;

procedure TfrmIntervju.OPIS_SREDINADblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dmOtsustvo.tblIntervjuOPIS_SREDINA.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
     begin
	     dmOtsustvo.tblIntervjuOPIS_SREDINA.Value := frmNotepad.ReturnText;
     end;
     frmNotepad.Free;
end;

//------------------------------------------------------------------------------

procedure TfrmIntervju.SaveToIniFileExecute(Sender: TObject);
begin
    //cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmIntervju.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if dmOtsustvo.tblIntervjuID_OBLIK.Value=1 then
  begin
    tblIntKategorija.open;
    gbGridP.Visible:=True;
    bsStrukturirano.Enabled:=True;
  end
  else
  begin
    gbGridP.Visible:=False;
    bsStrukturirano.Enabled:=false;
  end;
end;

procedure TfrmIntervju.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIntervju.cxGrid2DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key=VK_RETURN) then
    begin
      if (cxGrid2DBTableView1.DataController.DataSet.State=dsEdit)
          and (cxGrid2DBTableView1ZABELESKA.Editing)  then
        tblIntKategorija.Post;
    end;
end;

procedure TfrmIntervju.dxBarLargeButton7Click(Sender: TObject);
begin
      Close;
end;

//  ����� �� �����
procedure TfrmIntervju.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        // dmSis.tblOglas.Locate('BROJ', VarArrayOf([Sifra.Text]),[])
      if dmSis.tblMolba.Locate('MOLBABROJ', VarArrayOf([ID_MOLBA.Text]),[]) then
           dmOtsustvo.tblIntervjuID_MOLBA.Value:=dmSis.tblMolbaID.Value;
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        ProveriTip;
      //  lPanel.Enabled:=true;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        ProveriTip;
       // lPanel.Enabled:=true;
       // cxGrid1.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmIntervju.aZavrsiExecute(Sender: TObject);
begin
      aOtkazi.Execute;
end;

//	����� �� ���������� �� �������
procedure TfrmIntervju.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)
     and (not Panel1.Enabled) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  if (cxGrid1DBTableView1.DataController.DataSource.State in [dsEdit,dsInsert]) then
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      RestoreControls(Panel1);
      dPanel.Enabled := false;
      Panel1.Enabled:=False;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end
  else
  if (cxGrid2DBTableView1.DataController.DataSource.State in [dsEdit,dsInsert]) then
  begin
      cxGrid2DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(Panel1);
      dPanel.Enabled := false;
      Panel1.Enabled:=False;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end
  else
  if (tblIntKategorija.State = dsBrowse) then
  begin
      Panel1.Enabled:=False;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIntervju.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIntervju.aPecatiPExecute(Sender: TObject);
begin
    dxComponentPrinter1Link2.ReportTitle.Text := '��������� �� ��������� �� '+dmOtsustvo.tblIntervjuMOLBANAZIV.AsString;
    dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmIntervju.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIntervju.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmIntervju.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIntervju.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmIntervju.aBrisiPrasanjaExecute(Sender: TObject);
begin
if not tblIntKategorija.IsEmpty then
begin
 frmDaNe:=TfrmDaNe.Create(self,'������','���� ��������� ������ �� �� ���������'+#10#13+' ��������� �� ��������� �� '+dmOtsustvo.tblIntervjuMOLBANAZIV.AsString+'?',0);
   if frmDaNe.ShowModal=mrYes then
   begin
      //tblIntKategorija.delete;
      qBrisiKategorija.close;
      qBrisiKategorija.ParamByName('ID').Value:=dmOtsustvo.tblIntervjuID.Value;
      qBrisiKategorija.ExecQuery;
      tblIntKategorija.close;
      tblIntKategorija.open;
   end;
end;
end;

procedure TfrmIntervju.ActionList1Execute(Action: TBasicAction;
  var Handled: Boolean);
begin
   //za dodavawe na prasawe
end;

//��������� �� �������� �� ������� (�����) �� ���������
procedure TfrmIntervju.ProveriTip;
begin
         //�������� ����� � ��������� (������������� ��� ���������������)
       if rgTip.ItemIndex=0 then
       //���������������
        begin
          panel1.Enabled:=True;
        end
       else
       //�������������
       begin
          tblKategorja.close;
          tblKategorja.ParamByName('rm').Value:=dmOtsustvo.tblIntervjuID_RM_RE_MOLBA.Value;
          tblKategorja.ParamByName('firma').asinteger:=firma;
          tblKategorja.open;
          // �� �� ���������� �� �������� HR_INT_KATEGORIJA
          tblIntKategorija.Close;
          tblIntKategorija.Open;
          if tblIntKategorija.IsEmpty then
          begin
            tblKategorja.First;
              while not tblKategorja.Eof do
              begin
                 qInsertKategorija.Close;
                 qInsertKategorija.ParamByName('id_kategorija').Value:=tblKategorjaID.Value;
                 qInsertKategorija.ParamByName('id_intervju').Value:=dmOtsustvo.tblIntervjuID.Value;
                 qInsertKategorija.ExecQuery;
                 tblKategorja.next;
              end;
          end;
          tblIntKategorija.Close;
          tblIntKategorija.Open;
          Panel1.Enabled:=True;
          gbGridP.Visible:=True;
          ActiveControl:=cxGrid2;
       end;
end;

end.
