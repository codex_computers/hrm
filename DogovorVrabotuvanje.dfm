object frmDogovorVrabotuvanje: TfrmDogovorVrabotuvanje
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
  ClientHeight = 741
  ClientWidth = 1229
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1229
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 718
    Width = 1229
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 1229
    Height = 175
    Align = alClient
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1227
      Height = 165
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsDogVrabotuvanje
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1aktiven: TcxGridDBColumn
          DataBinding.FieldName = 'aktiven'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1040#1082#1090#1080#1074#1077#1085
              ImageIndex = 2
              Value = 1
            end
            item
              Description = #1053#1077' '#1077' '#1072#1082#1090#1080#1074#1077#1085
              ImageIndex = 3
              Value = 0
            end>
          Width = 99
        end
        object cxGrid1DBTableView1zatvorenSo: TcxGridDBColumn
          DataBinding.FieldName = 'zatvorenSo'
          Width = 226
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VID_DOKUMENT'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 109
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 97
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 85
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 114
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '
          DataBinding.FieldName = 'TATKOVO_IME'
          Visible = False
          Width = 104
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 137
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 250
        end
        object cxGrid1DBTableView1STEPEN_STRUCNA_PODGOTOVKA: TcxGridDBColumn
          DataBinding.FieldName = 'STEPEN_STRUCNA_PODGOTOVKA'
          Visible = False
          Width = 44
        end
        object cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn
          DataBinding.FieldName = 'OBRAZOVANIE'
          Width = 165
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 107
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_MESTO'
          Width = 102
        end
        object cxGrid1DBTableView1STAZ_DEN: TcxGridDBColumn
          DataBinding.FieldName = 'STAZ_DEN'
          Width = 100
        end
        object cxGrid1DBTableView1STAZ_MESEC: TcxGridDBColumn
          DataBinding.FieldName = 'STAZ_MESEC'
          Width = 100
        end
        object cxGrid1DBTableView1STAZ_GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'STAZ_GODINA'
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 86
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 89
        end
        object cxGrid1DBTableView1VID_VRABOTUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'VID_VRABOTUVANJE'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_VID_VRAB: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VID_VRAB'
          Width = 117
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Visible = False
          Width = 68
        end
        object cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABMESTONAZIV'
          Width = 164
        end
        object cxGrid1DBTableView1RABOTNO_VREME: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_VREME'
          Width = 76
        end
        object cxGrid1DBTableView1PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'PLATA'
          Width = 40
        end
        object cxGrid1DBTableView1MB_DIREKTOR: TcxGridDBColumn
          DataBinding.FieldName = 'MB_DIREKTOR'
          Width = 104
        end
        object cxGrid1DBTableView1KOEFICIENT: TcxGridDBColumn
          DataBinding.FieldName = 'KOEFICIENT'
        end
        object cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn
          Caption = #1056#1072#1082#1086#1074#1086#1076#1077#1114#1077
          DataBinding.FieldName = 'RAKOVODENJE'
        end
        object cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'DEN_USLOVI_RABOTA'
        end
        object cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USLOVI_RABOTA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA_NAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1USLOVI_RABOTA_PROCENT: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA_PROCENT'
          Width = 173
        end
        object cxGrid1DBTableView1DENOVI_ODMOR: TcxGridDBColumn
          DataBinding.FieldName = 'DENOVI_ODMOR'
          Width = 100
        end
        object cxGrid1DBTableView1BENEFICIRAN_STAZ: TcxGridDBColumn
          DataBinding.FieldName = 'BENEFICIRAN_STAZ'
          Width = 107
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM_PRVO_VRAB: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_PRVO_VRAB'
        end
        object cxGrid1DBTableView1NETO: TcxGridDBColumn
          DataBinding.FieldName = 'NETO'
          Width = 70
        end
        object cxGrid1DBTableView1BODOVI: TcxGridDBColumn
          DataBinding.FieldName = 'BODOVI'
          Width = 70
        end
        object cxGrid1DBTableView1SIS_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'SIS_OPIS'
          Width = 98
        end
        object cxGrid1DBTableView1SIS_DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'SIS_DATUM_DO'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 166
      Width = 1227
      Height = 8
      HotZoneClassName = 'TcxMediaPlayer9Style'
      AlignSplitter = salBottom
      AllowHotZoneDrag = False
      InvertDirection = True
      Control = dPanel
    end
    object Panel1: TPanel
      Left = 210
      Top = 90
      Width = 439
      Height = 129
      BevelInner = bvLowered
      BevelOuter = bvSpace
      TabOrder = 2
      Visible = False
      DesignSize = (
        439
        129)
      object cxGroupBox2: TcxGroupBox
        Left = 22
        Top = 21
        Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
        TabOrder = 0
        Height = 60
        Width = 395
        object Label27: TLabel
          Left = 213
          Top = 26
          Width = 29
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label28: TLabel
          Left = 16
          Top = 26
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dateOD: TcxDateEdit
          Tag = 1
          Left = 72
          Top = 22
          TabOrder = 0
          OnEnter = cxGroupBox1Enter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object dateDO: TcxDateEdit
          Left = 248
          Top = 22
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
      end
      object cxButton1: TcxButton
        Left = 259
        Top = 96
        Width = 75
        Height = 25
        Action = aKopirajOdPanel
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 1
      end
      object cxButton2: TcxButton
        Left = 340
        Top = 96
        Width = 75
        Height = 25
        Action = aOtkaziKopiranje
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 2
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 301
    Width = 1229
    Height = 417
    Align = alBottom
    Enabled = False
    TabOrder = 3
    DesignSize = (
      1229
      417)
    object Label11: TLabel
      Left = 17
      Top = 9
      Width = 136
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 19
      Top = 22
      Width = 136
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 306
      Top = 18
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ZapisiButton: TcxButton
      Left = 1033
      Top = 362
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 6
    end
    object OtkaziButton: TcxButton
      Left = 1114
      Top = 362
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 7
    end
    object PodatociZaRabotnik: TcxGroupBox
      Left = 17
      Top = 42
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090
      TabOrder = 2
      DesignSize = (
        521
        211)
      Height = 211
      Width = 521
      object Label6: TLabel
        Left = 33
        Top = 53
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 34
        Top = 104
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 33
        Top = 26
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 33
        Top = 157
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 72
        Top = 184
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 3
        Top = 123
        Width = 119
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 3
        Top = 136
        Width = 119
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 33
        Top = 78
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Prezime: TcxDBTextEdit
        Tag = 1
        Left = 128
        Top = 50
        Hint = #1055#1088#1077#1079#1080#1084#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 378
      end
      object Ime: TcxDBTextEdit
        Tag = 1
        Left = 128
        Top = 101
        Hint = #1048#1084#1077' '
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 378
      end
      object MBroj: TcxDBTextEdit
        Tag = 1
        Left = 128
        Top = 23
        Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MB'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Properties.OnValidate = MBrojPropertiesValidate
        Style.Shadow = False
        StyleDisabled.TextColor = clDefault
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = MBrojExit
        OnKeyDown = EnterKakoTab
        Width = 378
      end
      object Adresa: TcxDBTextEdit
        Left = 128
        Top = 154
        Hint = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1079#1077#1084#1077#1085#1072' '#1086#1076' '#1083#1080#1095#1085#1072' '#1082#1072#1088#1090#1072' '
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 378
      end
      object MESTO: TcxDBTextEdit
        Tag = 1
        Left = 128
        Top = 181
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 62
      end
      object SSP: TcxDBTextEdit
        Tag = 1
        Left = 128
        Top = 127
        Hint = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072' '
        BeepOnEnter = False
        DataBinding.DataField = 'STEPEN_STRUCNA_PODGOTOVKA'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 62
      end
      object OBRAZOVANIE_naziv: TcxDBLookupComboBox
        Tag = 1
        Left = 190
        Top = 127
        Hint = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'STEPEN_STRUCNA_PODGOTOVKA'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'OPIS'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsObrazovanie
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 316
      end
      object TATKOVO_IME: TcxDBTextEdit
        Left = 128
        Top = 75
        Hint = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'TATKOVO_IME'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 378
      end
      object cbMesto: TcxDBLookupComboBox
        Tag = 1
        Left = 190
        Top = 181
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'Naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 316
      end
    end
    object PodatociZaVrabotuvanje: TcxGroupBox
      Left = 544
      Top = 42
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      TabOrder = 5
      OnClick = PodatociZaVrabotuvanjeClick
      DesignSize = (
        661
        285)
      Height = 285
      Width = 661
      object Label5: TLabel
        Left = 20
        Top = 26
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1080#1076' '#1085#1072' '#1074#1088#1072#1073'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 14
        Top = 53
        Width = 95
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 10
        Top = 80
        Width = 99
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1080' '#1095#1072#1089#1086#1074#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 20
        Top = 107
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label31: TLabel
        Left = 239
        Top = 80
        Width = 46
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1083#1072#1090#1072' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl1: TLabel
        Left = 375
        Top = 80
        Width = 46
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1077#1090#1086' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl2: TLabel
        Left = 512
        Top = 80
        Width = 53
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1086#1076#1086#1074#1080' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtVidVrabotuvanje: TcxDBTextEdit
        Tag = 1
        Left = 115
        Top = 23
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1042#1080#1076' '#1085#1072' '#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
        BeepOnEnter = False
        DataBinding.DataField = 'VID_VRABOTUVANJE'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 58
      end
      object cbVidVrabotuvanje: TcxDBLookupComboBox
        Tag = 1
        Left = 174
        Top = 23
        Hint = #1054#1087#1080#1089' '#1085#1072' '#1042#1080#1076' '#1085#1072' '#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'VID_VRABOTUVANJE'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'ID'
          end
          item
            Width = 600
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsVidVrabotuvanje
        Properties.OnChange = cbVidVrabotuvanjePropertiesChange
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 472
      end
      object ID_RM_RE: TcxDBTextEdit
        Tag = 1
        Left = 115
        Top = 50
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'RABOTNO_MESTO'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 58
      end
      object ID_RM_RE_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 174
        Top = 50
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'RABOTNO_MESTO'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV_RE'
          end
          item
            FieldName = 'NAZIV_RM'
          end
          item
            FieldName = 'SIS_OPIS'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsRMRE
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 472
      end
      object RABOTNI_CASOVI: TcxDBTextEdit
        Left = 115
        Top = 77
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1095#1072#1089#1086#1074#1080
        BeepOnEnter = False
        DataBinding.DataField = 'RABOTNO_VREME'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 118
      end
      object MB: TcxDBTextEdit
        Left = 115
        Top = 104
        Hint = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MB_DIREKTOR'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 530
      end
      object cxGroupBox1: TcxGroupBox
        Left = 14
        Top = 131
        Anchors = [akLeft, akTop, akRight]
        Caption = '  '#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1088#1080#1084#1072#1114#1072'  '
        Style.LookAndFeel.Kind = lfOffice11
        Style.LookAndFeel.NativeStyle = False
        Style.TextStyle = [fsBold]
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.NativeStyle = False
        TabOrder = 9
        OnEnter = cxGroupBox1Enter
        Height = 142
        Width = 625
        object Label21: TLabel
          Left = 6
          Top = 53
          Width = 154
          Height = 18
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1082#1086#1074#1086#1076#1077#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label23: TLabel
          Left = 6
          Top = 102
          Width = 154
          Height = 26
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1077#1085'. '#1079#1072' '#1043#1054' '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label25: TLabel
          Left = 260
          Top = 53
          Width = 24
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          Caption = '%'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label26: TLabel
          Left = 6
          Top = 26
          Width = 154
          Height = 18
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label29: TLabel
          Left = 6
          Top = 80
          Width = 154
          Height = 18
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label30: TLabel
          Left = 264
          Top = 80
          Width = 24
          Height = 12
          Alignment = taRightJustify
          AutoSize = False
          Caption = '%'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Rakovodenje: TcxDBTextEdit
          Left = 166
          Top = 50
          Hint = 
            #1047#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1080#1086#1090' '#1073#1086#1076' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1088#1072#1082#1086#1074#1086#1076#1085#1072#1090#1072' '#1092#1091#1085#1082#1094#1080#1112 +
            #1072
          BeepOnEnter = False
          DataBinding.DataField = 'RAKOVODENJE'
          DataBinding.DataSource = dm.dsDogVrabotuvanje
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
        object DenUsloviGO: TcxDBTextEdit
          Left = 166
          Top = 104
          Hint = 
            #1050#1086#1083#1082#1091' '#1089#1083#1086#1073#1086#1076#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1076#1086#1076#1072#1074#1072#1072#1090' '#1085#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088', '#1082#1072#1082#1086' '#1088#1077#1079#1091#1083 +
            #1090#1072#1090' '#1085#1072' '#1087#1086#1089#1077#1073#1085#1080#1090#1077' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1072
          BeepOnEnter = False
          DataBinding.DataField = 'DEN_USLOVI_RABOTA'
          DataBinding.DataSource = dm.dsDogVrabotuvanje
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
        object koeficient: TcxDBTextEdit
          Left = 166
          Top = 23
          Hint = 
            #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090' ('#1086#1074#1086#1112' '#1082#1086#1077#1092#1080#1094#1080#1077#1085#1090' '#1089#1077' '#1087#1088#1077#1074#1079#1077#1084#1072' '#1086#1076' '#1082#1086#1077#1092#1080#1094#1080#1077#1085#1090#1086#1090' '#1085#1072' '#1089#1083#1086#1078#1077 +
            #1085#1086#1089#1090' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1086#1090#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086')'
          BeepOnEnter = False
          DataBinding.DataField = 'KOEFICIENT'
          DataBinding.DataSource = dm.dsDogVrabotuvanje
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
        object BENEFICIRAN_STAZ: TcxDBTextEdit
          Left = 166
          Top = 77
          Hint = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078' %'
          BeepOnEnter = False
          DataBinding.DataField = 'BENEFICIRAN_STAZ'
          DataBinding.DataSource = dm.dsDogVrabotuvanje
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 92
        end
      end
      object txtPlata: TcxDBTextEdit
        Left = 291
        Top = 77
        Hint = 
          #1055#1083#1072#1090#1072' '#1074#1086' '#1076#1077#1085#1072#1088#1080'. '#1057#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1089#1077' '#1087#1088#1072#1074#1080' '#1076#1086#1075#1086#1074#1086#1088' '#1085#1072' '#1092#1080#1082#1089#1085#1072 +
          ' '#1087#1083#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'PLATA'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 70
      end
      object Neto: TcxDBTextEdit
        Left = 427
        Top = 77
        Hint = 
          #1055#1083#1072#1090#1072' '#1074#1086' '#1076#1077#1085#1072#1088#1080'. '#1057#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1089#1077' '#1087#1088#1072#1074#1080' '#1076#1086#1075#1086#1074#1086#1088' '#1085#1072' '#1092#1080#1082#1089#1085#1072 +
          ' '#1087#1083#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'NETO'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 70
      end
      object Bodovi: TcxDBTextEdit
        Left = 571
        Top = 77
        Hint = 
          #1055#1083#1072#1090#1072' '#1074#1086' '#1076#1077#1085#1072#1088#1080'. '#1057#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1089#1077' '#1087#1088#1072#1074#1080' '#1076#1086#1075#1086#1074#1086#1088' '#1085#1072' '#1092#1080#1082#1089#1085#1072 +
          ' '#1087#1083#1072#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'BODOVI'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 74
      end
    end
    object PeriodNaVazenje: TcxGroupBox
      Left = 17
      Top = 259
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
      TabOrder = 3
      Height = 57
      Width = 521
      object Label4: TLabel
        Left = 180
        Top = 26
        Width = 29
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 16
        Top = 26
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 304
        Top = 18
        Width = 99
        Height = 28
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1074#1086' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DO_VREME: TcxDBDateEdit
        Left = 215
        Top = 23
        Hint = 
          #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089' '#1076#1086#1082#1086#1083#1082#1091' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1077' '#1085#1072' '#1086#1087#1088#1077#1076 +
          #1077#1083#1077#1085#1086' '#1074#1088#1077#1084#1077
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikRegExpr
        ShowHint = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 93
      end
      object OD_VREME: TcxDBDateEdit
        Tag = 1
        Left = 72
        Top = 23
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1087#1086#1095#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikRegExpr
        ShowHint = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 89
      end
      object DATUM_PRVO_VRAB: TcxDBDateEdit
        Left = 409
        Top = 23
        Hint = 
          #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089' '#1076#1086#1082#1086#1083#1082#1091' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1077' '#1085#1072' '#1086#1087#1088#1077#1076 +
          #1077#1083#1077#1085#1086' '#1074#1088#1077#1084#1077
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_PRVO_VRAB'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikRegExpr
        ShowHint = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 93
      end
    end
    object DatumKreiranje: TcxDBDateEdit
      Tag = 1
      Left = 161
      Top = 15
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsDogVrabotuvanje
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 146
    end
    object Staz: TcxGroupBox
      Left = 17
      Top = 322
      Caption = #1057#1090#1072#1078' '#1087#1088#1077#1090#1093#1086#1076#1077#1085
      TabOrder = 4
      Height = 57
      Width = 521
      object Label17: TLabel
        Left = 16
        Top = 26
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1043#1086#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 177
        Top = 26
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1077#1094' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label19: TLabel
        Left = 327
        Top = 26
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1077#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object StazGodina: TcxDBTextEdit
        Left = 72
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'STAZ_GODINA'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 75
      end
      object StazMesec: TcxDBTextEdit
        Left = 233
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'STAZ_MESEC'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 75
      end
      object StazDen: TcxDBTextEdit
        Left = 383
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'STAZ_DEN'
        DataBinding.DataSource = dm.dsDogVrabotuvanje
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 75
      end
    end
    object Broj: TcxDBTextEdit
      Left = 362
      Top = 15
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1089#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsDogVrabotuvanje
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 113
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 112
    Top = 280
  end
  object PopupMenu1: TPopupMenu
    Left = 392
    Top = 192
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 584
    Top = 144
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      CaptionButtons = <>
      DockedLeft = 140
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 794
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1127
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 986
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 375
      DockedTop = 0
      FloatLeft = 985
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      CaptionButtons = <>
      DockedLeft = 549
      DockedTop = 0
      FloatLeft = 985
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      CaptionButtons = <>
      DockedLeft = 677
      DockedTop = 0
      FloatLeft = 1329
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aVnesiVoVrabotenRM
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1074#1086' '#1074#1088#1072#1073#1086#1090#1077#1085#1080
      Category = 0
      Hint = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1088#1072#1073#1086#1090#1085#1080#1082' '#1082#1072#1082#1086' '#1074#1088#1072#1073#1086#1090#1077#1085
      LargeImageIndex = 26
    end
    object cxBarEditItem2: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1080
          Value = 1
        end
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 0
        end>
      Properties.OnChange = cxBarEditItem2PropertiesChange
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPregledDogovor
      Caption = #1050#1088#1077#1080#1088#1072'j/'#1055#1088#1077#1075#1083#1077#1076
      Category = 0
      LargeImageIndex = 10
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aAneks
      Category = 0
      LargeImageIndex = 8
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Action = aKalkulatorStaz
      Category = 0
      LargeImageIndex = 4
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = #1055#1088#1077#1089#1084#1077#1090#1082#1072' '#1085#1072' '#1089#1090#1072#1078
      Category = 0
      Hint = #1055#1088#1077#1089#1084#1077#1090#1082#1072' '#1085#1072' '#1089#1090#1072#1078
      Visible = ivAlways
      ShowCaption = True
      PropertiesClassName = 'TcxHyperLinkEditProperties'
      Properties.ReadOnly = True
      Properties.Prefix = 
        'http://www.timeanddate.com/date/duration.html?y1=2005&m1=3&d1=16' +
        '&y2=2011&m2=3&d2=16&ti=on'
    end
    object cxBarEditItem4: TcxBarEditItem
      Caption = #1089
      Category = 0
      Hint = #1089
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aKalkulatorStaz
      Category = 0
      LargeImageIndex = 4
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aKopiraj
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aUsloviPoteskiOdNormanite
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 184
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      OnExecute = aHelpExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aVnesiVoVrabotenRM: TAction
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1074#1086' '#1074#1088#1072#1073#1086#1090#1088#1085#1080
      Hint = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1074#1086' '#1074#1088#1072#1073#1086#1090#1077#1085#1080
      OnExecute = aVnesiVoVrabotenRMExecute
    end
    object aPrisilnoBrisenje: TAction
      Caption = 'aPrisilnoBrisenje'
      ShortCut = 16503
      OnExecute = aPrisilnoBrisenjeExecute
    end
    object aPregledDogovor: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1114#1077'/'#1055#1088#1077#1075#1083#1077#1076
      Hint = 
        #1044#1086#1082#1086#1083#1082#1091' '#1085#1077' '#1077' '#1082#1088#1077#1080#1088#1072#1085' '#1076#1086#1082#1091#1084#1077#1085#1090' '#1080#1084#1072' '#1084#1086#1078#1085#1086#1089#1090' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1090#1077#1088#1082' '#1080' '#1079#1072#1095 +
        #1091#1074#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1082#1091#1084#1077#1085#1090#1086#1090' '#1074#1086' '#1073#1072#1079#1072#1090'. '#1044#1086#1082#1086#1083#1082#1091' '#1074#1077#1116#1077' '#1080#1084#1072' '#1082#1088#1077#1080#1088#1072#1085#1086' '#1076#1086#1082#1091#1084#1077#1085 +
        #1090' '#1089#1077' '#1087#1088#1077#1075#1083#1077#1076#1091#1074#1072' '#1080#1089#1090#1080#1086#1090
      OnExecute = aPregledDogovorExecute
    end
    object aAneks: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1050#1088#1077#1080#1088#1072#1112
      OnExecute = aAneksExecute
    end
    object aKalkulatorStaz: TAction
      Caption = #1055#1088#1077#1089#1084#1077#1090#1082#1072' '#1085#1072' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080', '#1084#1077#1089#1077#1094#1080', '#1075#1086#1076#1080#1085#1080' '#1087#1086#1084#1077#1107#1091' '#1076#1074#1077' '#1076#1072#1090#1080
      OnExecute = aKalkulatorStazExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
    object aKopiraj: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112
      ImageIndex = 8
      OnExecute = aKopirajExecute
    end
    object aKopirajOdPanel: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112
      ImageIndex = 7
      OnExecute = aKopirajOdPanelExecute
    end
    object aOtkaziKopiranje: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziKopiranjeExecute
    end
    object aUsloviPoteskiOdNormanite: TAction
      Caption = #1059#1089#1083#1086#1074#1080' '#1087#1086#1090#1077#1096#1082#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1083#1085#1080#1090#1077
      ImageIndex = 92
      OnExecute = aUsloviPoteskiOdNormaniteExecute
    end
    object aStazPrethoden: TAction
      Caption = 'aStazPrethoden'
      OnExecute = aStazPrethodenExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44512.353728495370000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 656
    Top = 200
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblRMRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hrr.rakovodenje,'
      '    hrr.denovi_odmor,'
      '    hrr.den_uslovi_rabota,'
      '    hrr.uslovi_rabota,'
      '    hrm.stepen_slozenost,'
      
        '    hrk.id as usloviRabotaID,  hrk.procent as usloviRabotaProcen' +
        't,'
      '    hrr.beneficiran_staz,'
      '    hs.opis as sis_opis'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'left outer join hr_koeficient_uslovirab hrk on hrk.id = hrr.uslo' +
        'vi_rabota'
      
        'where /*$$IBEC$$ hs.do_datum is null and $$IBEC$$*/ hs.id_re_fir' +
        'ma = :firma'
      'order by 4, 8')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 307
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRMRERAKOVODENJE: TFIBBCDField
      FieldName = 'RAKOVODENJE'
      Size = 2
    end
    object tblRMREDENOVI_ODMOR: TFIBIntegerField
      FieldName = 'DENOVI_ODMOR'
    end
    object tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblRMRESTEPEN_SLOZENOST: TFIBBCDField
      FieldName = 'STEPEN_SLOZENOST'
      Size = 8
    end
    object tblRMREUSLOVI_RABOTA: TFIBIntegerField
      FieldName = 'USLOVI_RABOTA'
    end
    object tblRMREUSLOVIRABOTAID: TFIBIntegerField
      FieldName = 'USLOVIRABOTAID'
    end
    object tblRMREUSLOVIRABOTAPROCENT: TFIBBCDField
      FieldName = 'USLOVIRABOTAPROCENT'
      Size = 2
    end
    object tblRMREBENEFICIRAN_STAZ: TFIBBCDField
      DisplayLabel = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078
      FieldName = 'BENEFICIRAN_STAZ'
      Size = 2
    end
    object tblRMRESIS_OPIS: TFIBStringField
      DisplayLabel = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'SIS_OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 995
    Top = 307
  end
  object qRERM: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select hrr.id_re re_id'
      'from hr_rm_re hrr'
      'where hrr.id=:re_id')
    Left = 848
    Top = 240
    qoStartTransaction = True
  end
  object qEditPLTVraboteni: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        ' insert into plt_vraboteni (mb,firma,prezime,tatkovo_ime,ime, me' +
        'sto,re_id)'
      ' values (:mb,:firma ,:prezime,:tatkovo_ime,:ime,:mesto,:re_id)')
    Left = 888
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblPLTVraboteni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PLT_VRABOTENI'
      'SET '
      '    PREZIME = :PREZIME,'
      '    TATKOVO_IME = :TATKOVO_IME,'
      '    IME = :IME,'
      '    V_STA_GG = :V_STA_GG,'
      '    V_STA_MM = :V_STA_MM,'
      '    V_STA_DD = :V_STA_DD,'
      '    TIP_BOD_SAAT = :TIP_BOD_SAAT,'
      '    BOD = :BOD,'
      '    BOD_STAZ = :BOD_STAZ,'
      '    PATARINA = :PATARINA,'
      '    LICOSIG = :LICOSIG,'
      '    SSM = :SSM,'
      '    KOLEKTIVNO = :KOLEKTIVNO,'
      '    LBROJ = :LBROJ,'
      '    DOSIE = :DOSIE,'
      '    RAB_KNISKA = :RAB_KNISKA,'
      '    SPREMA = :SPREMA,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    BR_LICNA_KARTA = :BR_LICNA_KARTA,'
      '    PENZ_FOND = :PENZ_FOND,'
      '    RE_ID = :RE_ID,'
      '    OTEZNATI = :OTEZNATI,'
      '    FUNKCIJA = :FUNKCIJA,'
      '    KORISNIK = :KORISNIK,'
      '    VREME = :VREME,'
      '    INVALID = :INVALID,'
      '    NORMATIV = :NORMATIV,'
      '    BOL_PROSEK = :BOL_PROSEK,'
      '    NOVOVRABOTEN = :NOVOVRABOTEN,'
      '    FZO = :FZO,'
      '    BRUTO_DOGOVOR = :BRUTO_DOGOVOR'
      'WHERE'
      '    MB = :OLD_MB'
      '    and FIRMA = :OLD_FIRMA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    PLT_VRABOTENI'
      'WHERE'
      '        MB = :OLD_MB'
      '    and FIRMA = :OLD_FIRMA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO PLT_VRABOTENI('
      '    MB,'
      '    FIRMA,'
      '    PREZIME,'
      '    TATKOVO_IME,'
      '    IME,'
      '    V_STA_GG,'
      '    V_STA_MM,'
      '    V_STA_DD,'
      '    TIP_BOD_SAAT,'
      '    BOD,'
      '    BOD_STAZ,'
      '    PATARINA,'
      '    LICOSIG,'
      '    SSM,'
      '    KOLEKTIVNO,'
      '    LBROJ,'
      '    DOSIE,'
      '    RAB_KNISKA,'
      '    SPREMA,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    BR_LICNA_KARTA,'
      '    PENZ_FOND,'
      '    RE_ID,'
      '    OTEZNATI,'
      '    FUNKCIJA,'
      '    KORISNIK,'
      '    VREME,'
      '    INVALID,'
      '    NORMATIV,'
      '    BOL_PROSEK,'
      '    NOVOVRABOTEN,'
      '    FZO,'
      '    BRUTO_DOGOVOR'
      ')'
      'VALUES('
      '    :MB,'
      '    :FIRMA,'
      '    :PREZIME,'
      '    :TATKOVO_IME,'
      '    :IME,'
      '    :V_STA_GG,'
      '    :V_STA_MM,'
      '    :V_STA_DD,'
      '    :TIP_BOD_SAAT,'
      '    :BOD,'
      '    :BOD_STAZ,'
      '    :PATARINA,'
      '    :LICOSIG,'
      '    :SSM,'
      '    :KOLEKTIVNO,'
      '    :LBROJ,'
      '    :DOSIE,'
      '    :RAB_KNISKA,'
      '    :SPREMA,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :BR_LICNA_KARTA,'
      '    :PENZ_FOND,'
      '    :RE_ID,'
      '    :OTEZNATI,'
      '    :FUNKCIJA,'
      '    :KORISNIK,'
      '    :VREME,'
      '    :INVALID,'
      '    :NORMATIV,'
      '    :BOL_PROSEK,'
      '    :NOVOVRABOTEN,'
      '    :FZO,'
      '    :BRUTO_DOGOVOR'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    MB,'
      '    FIRMA,'
      '    PREZIME,'
      '    TATKOVO_IME,'
      '    IME,'
      '    V_STA_GG,'
      '    V_STA_MM,'
      '    V_STA_DD,'
      '    TIP_BOD_SAAT,'
      '    BOD,'
      '    BOD_STAZ,'
      '    PATARINA,'
      '    LICOSIG,'
      '    SSM,'
      '    KOLEKTIVNO,'
      '    LBROJ,'
      '    DOSIE,'
      '    RAB_KNISKA,'
      '    SPREMA,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    BR_LICNA_KARTA,'
      '    PENZ_FOND,'
      '    RE_ID,'
      '    OTEZNATI,'
      '    FUNKCIJA,'
      '    KORISNIK,'
      '    VREME,'
      '    INVALID,'
      '    NORMATIV,'
      '    BOL_PROSEK,'
      '    NOVOVRABOTEN,'
      '    FZO,'
      '    BRUTO_DOGOVOR'
      'FROM'
      '    PLT_VRABOTENI '
      'WHERE'
      '        MB = :OLD_MB'
      '    and FIRMA = :OLD_FIRMA')
    SelectSQL.Strings = (
      'SELECT'
      '    MB,'
      '    FIRMA,'
      '    PREZIME,'
      '    TATKOVO_IME,'
      '    IME,'
      '    V_STA_GG,'
      '    V_STA_MM,'
      '    V_STA_DD,'
      '    TIP_BOD_SAAT,'
      '    BOD,'
      '    BOD_STAZ,'
      '    PATARINA,'
      '    LICOSIG,'
      '    SSM,'
      '    KOLEKTIVNO,'
      '    LBROJ,'
      '    DOSIE,'
      '    RAB_KNISKA,'
      '    SPREMA,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    BR_LICNA_KARTA,'
      '    PENZ_FOND,'
      '    RE_ID,'
      '    OTEZNATI,'
      '    FUNKCIJA,'
      '    KORISNIK,'
      '    VREME,'
      '    INVALID,'
      '    NORMATIV,'
      '    BOL_PROSEK,'
      '    NOVOVRABOTEN,'
      '    FZO,'
      '    BRUTO_DOGOVOR'
      'FROM'
      '    PLT_VRABOTENI ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 904
    Top = 496
    oRefreshDeletedRecord = True
    object tblPLTVraboteniMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniFIRMA: TFIBIntegerField
      FieldName = 'FIRMA'
    end
    object tblPLTVraboteniPREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniTATKOVO_IME: TFIBStringField
      FieldName = 'TATKOVO_IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniIME: TFIBStringField
      FieldName = 'IME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniV_STA_GG: TFIBIntegerField
      FieldName = 'V_STA_GG'
    end
    object tblPLTVraboteniV_STA_MM: TFIBIntegerField
      FieldName = 'V_STA_MM'
    end
    object tblPLTVraboteniV_STA_DD: TFIBIntegerField
      FieldName = 'V_STA_DD'
    end
    object tblPLTVraboteniTIP_BOD_SAAT: TFIBSmallIntField
      FieldName = 'TIP_BOD_SAAT'
    end
    object tblPLTVraboteniBOD_STAZ: TFIBBCDField
      FieldName = 'BOD_STAZ'
      Size = 8
    end
    object tblPLTVraboteniPATARINA: TFIBBCDField
      FieldName = 'PATARINA'
      Size = 2
    end
    object tblPLTVraboteniLICOSIG: TFIBBCDField
      FieldName = 'LICOSIG'
      Size = 2
    end
    object tblPLTVraboteniSSM: TFIBSmallIntField
      FieldName = 'SSM'
    end
    object tblPLTVraboteniKOLEKTIVNO: TFIBSmallIntField
      FieldName = 'KOLEKTIVNO'
    end
    object tblPLTVraboteniLBROJ: TFIBStringField
      FieldName = 'LBROJ'
      Size = 12
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniDOSIE: TFIBStringField
      FieldName = 'DOSIE'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniRAB_KNISKA: TFIBStringField
      FieldName = 'RAB_KNISKA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniSPREMA: TFIBSmallIntField
      FieldName = 'SPREMA'
    end
    object tblPLTVraboteniMESTO: TFIBIntegerField
      FieldName = 'MESTO'
    end
    object tblPLTVraboteniADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 120
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniTELEFON: TFIBStringField
      FieldName = 'TELEFON'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniPENZ_FOND: TFIBSmallIntField
      FieldName = 'PENZ_FOND'
    end
    object tblPLTVraboteniRE_ID: TFIBIntegerField
      FieldName = 'RE_ID'
    end
    object tblPLTVraboteniOTEZNATI: TFIBBCDField
      FieldName = 'OTEZNATI'
      Size = 8
    end
    object tblPLTVraboteniKORISNIK: TFIBStringField
      FieldName = 'KORISNIK'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniVREME: TFIBDateTimeField
      FieldName = 'VREME'
    end
    object tblPLTVraboteniINVALID: TFIBSmallIntField
      FieldName = 'INVALID'
    end
    object tblPLTVraboteniNORMATIV: TFIBSmallIntField
      FieldName = 'NORMATIV'
    end
    object tblPLTVraboteniBOL_PROSEK: TFIBBCDField
      FieldName = 'BOL_PROSEK'
      Size = 2
    end
    object tblPLTVraboteniNOVOVRABOTEN: TFIBSmallIntField
      FieldName = 'NOVOVRABOTEN'
    end
    object tblPLTVraboteniFZO: TFIBSmallIntField
      FieldName = 'FZO'
    end
    object tblPLTVraboteniFUNKCIJA: TFIBBCDField
      FieldName = 'FUNKCIJA'
      Size = 8
    end
    object tblPLTVraboteniBRUTO_DOGOVOR: TFIBBCDField
      FieldName = 'BRUTO_DOGOVOR'
      Size = 2
    end
    object tblPLTVraboteniBR_LICNA_KARTA: TFIBStringField
      FieldName = 'BR_LICNA_KARTA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPLTVraboteniBOD: TFIBFloatField
      FieldName = 'BOD'
    end
  end
  object dsPLTVraboteni: TDataSource
    DataSet = tblPLTVraboteni
    Left = 704
    Top = 208
  end
  object qPlataDaNe: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1'
      'from sys_setup s'
      'where s.p1 = :app and p2 = :param')
    Left = 144
    Top = 240
  end
  object qProveriDatumOd: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  count(rm.id)br'
      'from hr_vraboten_rm rm'
      
        'where  :datum_od between rm.datum_od and coalesce(rm.datum_do, c' +
        'urrent_date) and rm.id_re_firma = :id_re_firma and rm.mb = :mb')
    Left = 800
    Top = 296
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 104
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 872
    Top = 184
  end
end
