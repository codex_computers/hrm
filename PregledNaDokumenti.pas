unit PregledNaDokumenti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
   cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, FIBDataSet, pFIBDataSet, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxScreenTip, dxPSdxDBOCLnk,
  dxCustomHint, cxHint;

type
//  niza = Array[1..5] of Variant;

  TfrmPregledNaDokumenti = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1VID_DOCUMENT: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM: TcxGridDBColumn;
    dxBarLargeButton10: TdxBarLargeButton;
    aPregled: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxHintStyleController1: TcxHintStyleController;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid2DBTableView1DblClick(Sender: TObject);
    procedure dxBarLargeButton10Click(Sender: TObject);
    procedure aPregledExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmPregledNaDokumenti: TfrmPregledNaDokumenti;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmReportUnit,
  dmUnitOtsustvo;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmPregledNaDokumenti.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmPregledNaDokumenti.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPregledNaDokumenti.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPregledNaDokumenti.aBrisiExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

//	����� �� ���������� �� ����������
procedure TfrmPregledNaDokumenti.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPregledNaDokumenti.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPregledNaDokumenti.aSnimiIzgledExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajGridVoBaza(Name,cxGrid2DBTableView1);

  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPregledNaDokumenti.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajVoExcel(cxGrid1, Caption);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajVoExcel(cxGrid2, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPregledNaDokumenti.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPregledNaDokumenti.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPregledNaDokumenti.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmPregledNaDokumenti.cxGrid1DBTableView1DblClick(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
begin
     if dm.tblDokumentiVID_DOKUMENT.Value = reseniePreraspredelba then
        begin
          dmReport.ResenieZaPreraspredelba.close;
          dmReport.ResenieZaPreraspredelba.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.ResenieZaPreraspredelba.Open;
          if not dmReport.ResenieZaPreraspredelbaDATA.IsNull then
            begin
              RptStream := dmReport.ResenieZaPreraspredelba.CreateBlobStream(dmReport.ResenieZaPreraspredelbaDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = resPrekinRabOdnos then
        begin
          dmReport.PrestanokNaRabotenOdnos.close;
          dmReport.PrestanokNaRabotenOdnos.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.PrestanokNaRabotenOdnos.Open;
          if not dmReport.PrestanokNaRabotenOdnosDATA.IsNull then
            begin
              RptStream := dmReport.PrestanokNaRabotenOdnos.CreateBlobStream(dmReport.PrestanokNaRabotenOdnosDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = dogovorZaRabota then
        begin
          dmReport.DogovorZaVrabotuvanje.close;
          dmReport.DogovorZaVrabotuvanje.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dmReport.DogovorZaVrabotuvanje.Open;

          if not dmReport.DogovorZaVrabotuvanjeDATA.IsNull then
            begin
             RptStream := dmReport.DogovorZaVrabotuvanje.CreateBlobStream(dmReport.DogovorZaVrabotuvanjeDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;

      if dm.tblDokumentiVID_DOKUMENT.Value = aneksDogovor then
        begin
          dm.tblAneks.close;
          dm.tblAneks.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dm.tblAneks.Open;

          if not dm.tblAneksDATA.IsNull then
            begin
             RptStream := dm.tblAneks.CreateBlobStream(dm.tblAneksDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;

      if dm.tblDokumentiVID_DOKUMENT.Value = dogVolonter then
        begin
          dmReport.tblDogVolonteriodberi.close;
          dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dmReport.tblDogVolonteriodberi.Open;

          if not dmReport.tblDogVolonteriodberiDATA.IsNull then
            begin
             RptStream := dmReport.tblDogVolonteriodberi.CreateBlobStream(dmReport.tblDogVolonteriodberiDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;

end;

procedure TfrmPregledNaDokumenti.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPregledNaDokumenti.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmPregledNaDokumenti.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPregledNaDokumenti.prefrli;
begin
end;

procedure TfrmPregledNaDokumenti.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmPregledNaDokumenti.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dm.tblDokumenti.close;
  dm.tblDokumenti.ParamByName('mb').Value:=dm.tblLicaVraboteniMB.Value;
  dm.tblDokumenti.ParamByName('firma').Value:=dmkon.re;
  dm.tblDokumenti.Open;

  dm.tblDrugiDokumenti.Close;
  dm.tblDrugiDokumenti.ParamByName('mb').Value:=dm.tblLicaVraboteniMB.Value;
  dm.tblDrugiDokumenti.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmPregledNaDokumenti.FormShow(Sender: TObject);
begin
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
    dxComponentPrinter1Link2.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;
//------------------------------------------------------------------------------

procedure TfrmPregledNaDokumenti.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPregledNaDokumenti.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPregledNaDokumenti.cxGrid2DBTableView1DblClick(Sender: TObject);
var
     RptStream: TStream;
     SqlStream: TStream;
begin
     if dm.tblDrugiDokumentiVID_DOCUMENT.Value = baranjeGO then
        begin
         dmReport.BaranjeZaGO.close;
         dmReport.BaranjeZaGO.ParamByName('id').Value:=dm.tblDrugiDokumentiID.Value;
         dmReport.BaranjeZaGO.Open;
         if not dmReport.BaranjeZaGODATA.IsNull then
           begin
             RptStream := dmReport.BaranjeZaGO.CreateBlobStream(dmReport.BaranjeZaGODATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport();
           end;
        end;
      if dm.tblDrugiDokumentiVID_DOCUMENT.Value = resenieGO then
        begin
          dmReport.ResenieZaGO.close;
          dmReport.ResenieZaGO.ParamByName('id').Value:=dm.tblDrugiDokumentiID.Value;
          dmReport.ResenieZaGO.Open;
          if not dmReport.ResenieZaGODATA.IsNull then
           begin
             RptStream := dmReport.ResenieZaGO.CreateBlobStream(dmReport.ResenieZaGODATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
end;

procedure TfrmPregledNaDokumenti.dxBarLargeButton10Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmPregledNaDokumenti.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmPregledNaDokumenti.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPregledNaDokumenti.aPageSetupExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        dxComponentPrinter1Link1.PageSetup;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        dxComponentPrinter1Link2.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmPregledNaDokumenti.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        begin
          dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
          dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
          dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

          dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
        end;
     if cxPageControl1.ActivePage = cxTabSheet2 then
        begin
          dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
          dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
          dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

          dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
        end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPregledNaDokumenti.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
      if cxPageControl1.ActivePage = cxTabSheet1 then
         dxComponentPrinter1Link1.DesignReport();
      if cxPageControl1.ActivePage = cxTabSheet2 then
         dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmPregledNaDokumenti.aPregledExecute(Sender: TObject);
var
     RptStream: TStream;
     SqlStream: TStream;
begin
   if cxPageControl1.ActivePage = cxTabSheet2 then
     begin
     if dm.tblDrugiDokumentiVID_DOCUMENT.Value = baranjeGO then
        begin
         dmReport.BaranjeZaGO.close;
         dmReport.BaranjeZaGO.ParamByName('id').Value:=dm.tblDrugiDokumentiID.Value;
         dmReport.BaranjeZaGO.Open;
         if not dmReport.BaranjeZaGODATA.IsNull then
           begin
             RptStream := dmReport.BaranjeZaGO.CreateBlobStream(dmReport.BaranjeZaGODATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport();
           end;
        end;
      if dm.tblDrugiDokumentiVID_DOCUMENT.Value = resenieGO then
        begin
          dmReport.ResenieZaGO.close;
          dmReport.ResenieZaGO.ParamByName('id').Value:=dm.tblDrugiDokumentiID.Value;
          dmReport.ResenieZaGO.Open;
          if not dmReport.ResenieZaGODATA.IsNull then
           begin
             RptStream := dmReport.ResenieZaGO.CreateBlobStream(dmReport.ResenieZaGODATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
     end;

   if cxPageControl1.ActivePage = cxTabSheet1 then
     begin
      if dm.tblDokumentiVID_DOKUMENT.Value = reseniePreraspredelba then
        begin
          dmReport.ResenieZaPreraspredelba.close;
          dmReport.ResenieZaPreraspredelba.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.ResenieZaPreraspredelba.Open;
          if not dmReport.ResenieZaPreraspredelbaDATA.IsNull then
            begin
              RptStream := dmReport.ResenieZaPreraspredelba.CreateBlobStream(dmReport.ResenieZaPreraspredelbaDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = resPrekinRabOdnos then
        begin
          dmReport.PrestanokNaRabotenOdnos.close;
          dmReport.PrestanokNaRabotenOdnos.ParamByName('id').Value:=dm.tblDokumentiID.Value;
          dmReport.PrestanokNaRabotenOdnos.Open;
          if not dmReport.PrestanokNaRabotenOdnosDATA.IsNull then
            begin
              RptStream := dmReport.PrestanokNaRabotenOdnos.CreateBlobStream(dmReport.PrestanokNaRabotenOdnosDATA, bmRead);
              dmReport.frxReport1.LoadFromStream(RptStream);
              dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = dogovorZaRabota then
        begin
          dmReport.DogovorZaVrabotuvanje.close;
          dmReport.DogovorZaVrabotuvanje.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dmReport.DogovorZaVrabotuvanje.Open;

          if not dmReport.DogovorZaVrabotuvanjeDATA.IsNull then
            begin
             RptStream := dmReport.DogovorZaVrabotuvanje.CreateBlobStream(dmReport.DogovorZaVrabotuvanjeDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = aneksDogovor then
        begin
          dm.tblAneks.close;
          dm.tblAneks.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dm.tblAneks.Open;

          if not dm.tblAneksDATA.IsNull then
            begin
             RptStream := dm.tblAneks.CreateBlobStream(dm.tblAneksDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
      if dm.tblDokumentiVID_DOKUMENT.Value = dogVolonter then
        begin
          dmReport.tblDogVolonteriodberi.close;
          dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=dm.tblDokumentiID.Value;
          dmReport.tblDogVolonteriodberi.Open;

          if not dmReport.tblDogVolonteriodberiDATA.IsNull then
            begin
             RptStream := dmReport.tblDogVolonteriodberi.CreateBlobStream(dmReport.tblDogVolonteriodberiDATA, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);
             dmReport.frxReport1.ShowReport;
            end
        end;
     end;
end;

procedure TfrmPregledNaDokumenti.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPregledNaDokumenti.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheet1 then
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     if cxPageControl1.ActivePage = cxTabSheet2 then
        brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPregledNaDokumenti.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmPregledNaDokumenti.aHelpExecute(Sender: TObject);
begin
      Application.HelpContext(19);
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPregledNaDokumenti.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPregledNaDokumenti.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
