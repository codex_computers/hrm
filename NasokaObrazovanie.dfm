inherited frmNasokaObrazovanie: TfrmNasokaObrazovanie
  Caption = ' '#1053#1072#1089#1086#1082#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
  ClientWidth = 739
  ExplicitWidth = 755
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 739
    ExplicitWidth = 739
    inherited cxGrid1: TcxGrid
      Width = 735
      ExplicitWidth = 735
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsNasokaObrazovanie
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1OINAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OINAZIV'
          Width = 254
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 530
        end
        object cxGrid1DBTableView1OBRAZOVNA_INSTITUCIJA: TcxGridDBColumn
          DataBinding.FieldName = 'OBRAZOVNA_INSTITUCIJA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 739
    ExplicitWidth = 739
    inherited Label1: TLabel
      Visible = False
    end
    object Label2: TLabel [1]
      Left = 119
      Top = 32
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 2
      Top = 59
      Width = 167
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsNasokaObrazovanie
      TabOrder = 4
      Visible = False
    end
    inherited OtkaziButton: TcxButton
      Left = 648
      TabOrder = 5
      ExplicitLeft = 648
    end
    inherited ZapisiButton: TcxButton
      Left = 567
      TabOrder = 3
      ExplicitLeft = 567
    end
    object naziv: TcxDBTextEdit
      Tag = 1
      Left = 175
      Top = 29
      Hint = #1053#1072#1089#1086#1082#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsNasokaObrazovanie
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 392
    end
    object Obrazovna: TcxDBTextEdit
      Left = 175
      Top = 56
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072
      DataBinding.DataField = 'OBRAZOVNA_INSTITUCIJA'
      DataBinding.DataSource = dm.dsNasokaObrazovanie
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object Institucija: TcxDBLookupComboBox
      Left = 256
      Top = 56
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1085#1072' '#1080#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'OBRAZOVNA_INSTITUCIJA'
      DataBinding.DataSource = dm.dsNasokaObrazovanie
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dm.dsObrazovnaInstitucija
      StyleDisabled.TextColor = clBtnText
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 311
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 739
    ExplicitWidth = 739
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 739
    ExplicitTop = 32000
    ExplicitWidth = 739
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page # of Pages #]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40700.521870763890000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
