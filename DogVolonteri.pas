unit DogVolonteri;

{*******************************************************}
{                                                       }
{     ��������� : ������ ��������                      }
{                 ������ �������                       }
{                                                       }
{     ����� : 21.12.2011                                }
{                                                       }
{     ������ : 1.1.1.17                                }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon,  ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, cxCalendar, cxVariants,
  cxCheckGroup, cxCheckBox, cxCheckComboBox, cxRadioGroup, FIBDataSet, frxDesgn, frxClass, frxDBSet, frxRich,
  frxCross, frxEditDataBand,
  pFIBDataSet, FIBQuery, pFIBQuery, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxCustomHint, cxHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator, Vcl.ComCtrls, dxCore,
  cxDateUtils, System.Actions;
  type
  TfrmDogVolonteri = class(TfrmMaster)
    Period: TcxGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    DATUM_OD: TcxDBDateEdit;
    DATUM_DO: TcxDBDateEdit;
    dxBarButton1: TdxBarButton;
    aFilterAktivni: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    afilterSite: TAction;
    cxBarEditItem2: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    cxBarEditItem4: TcxBarEditItem;
    cxBarEditItem5: TcxBarEditItem;
    cxBarEditItem6: TcxBarEditItem;
    dxBarButton4: TdxBarButton;
    Label10: TLabel;
    Broj: TcxDBTextEdit;
    PodatociZaRabotnik: TcxGroupBox;
    IME: TcxDBTextEdit;
    Label5: TLabel;
    TATKOVO_IME: TcxDBTextEdit;
    Label4: TLabel;
    PREZIME: TcxDBTextEdit;
    Label3: TLabel;
    MB: TcxDBTextEdit;
    Label2: TLabel;
    PodatociZaVrabotuvanje: TcxGroupBox;
    RABOTNO_MESTO: TcxDBTextEdit;
    Label14: TLabel;
    RABOTNO_MESTO_NAZIV: TcxDBLookupComboBox;
    MB_DIREKTOR: TcxDBTextEdit;
    Label9: TLabel;
    PLATA: TcxDBTextEdit;
    Label8: TLabel;
    dsRMRE: TDataSource;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    tblRMRERAKOVODENJE: TFIBBCDField;
    tblRMREDENOVI_ODMOR: TFIBIntegerField;
    tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField;
    tblRMRESTEPEN_SLOZENOST: TFIBBCDField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    Panel1: TPanel;
    cxButton1: TcxButton;
    aKopirajDogovori: TAction;
    dxBarManager1Bar6: TdxBar;
    aKreirajPregledajDokument: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aBrisiDokument: TAction;
    Label11: TLabel;
    DatumKreiranje: TcxDBDateEdit;
    Label12: TLabel;
    BrLicnaKarta: TcxDBTextEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVRABMESTO: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_CASOVI: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1RAKOVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1LICNA_KARTA: TcxGridDBColumn;
    cxGrid1DBTableView1PLATA: TcxGridDBColumn;
    Panel2: TPanel;
    cxGroupBox1: TcxGroupBox;
    Label13: TLabel;
    Label15: TLabel;
    DO_DATUM: TcxDateEdit;
    OD_DATUM: TcxDateEdit;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    aZapisiDogovori: TAction;
    aOtkaziDogovori: TAction;
    CheckBox1: TCheckBox;
    Label16: TLabel;
    BROJ_CASOVI: TcxDBTextEdit;
    Label17: TLabel;
    qMaxBroj: TpFIBQuery;
    Broj_Del: TcxTextEdit;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxBarEditItem6PropertiesChange(Sender: TObject);
    procedure aKopirajDogovoriExecute(Sender: TObject);
    procedure aKreirajPregledajDokumentExecute(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure DO_DATUMExit(Sender: TObject);
    procedure OD_DATUMExit(Sender: TObject);
    procedure aZapisiDogovoriExecute(Sender: TObject);
    procedure aOtkaziDogovoriExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure siteD(kako:Boolean);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDogVolonteri: TfrmDogVolonteri;

implementation

uses dmKonekcija, dmUnit, dmSistematizacija, dmUnitOtsustvo, Utils,
  dmReportUnit, DaNe;

{$R *.dfm}

procedure TfrmDogVolonteri.aBrisiDokumentExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteDokVolonter.Close;
            if tag = 1 then
             dmOtsustvo.qDeleteDokVolonter.ParamByName('ID').Value:=dm.tblDogVolonteriID.Value
            else
              dmOtsustvo.qDeleteDokVolonter.ParamByName('ID').Value:=dm.tblDogVolonteriodberiID.Value;
           dmOtsustvo.qDeleteDokVolonter.ExecQuery;
        end;
end;

procedure TfrmDogVolonteri.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(18);
end;

procedure TfrmDogVolonteri.aKopirajDogovoriExecute(Sender: TObject);
var  i, idii:Integer;
     RptStream: TStream;
     SqlStream: TStream;
     sl: TStringList;
     value:Variant;
     RichView: TfrxRichView;
     MasterData: TfrxMasterData;
     pom:Boolean;
begin
     pom:=false;
     with cxGrid1DBTableView1.DataController do
     for I := 0 to RecordCount - 1 do   //izvrti gi site stiklirani - ne samo filtered
         begin
           if (DisplayTexts[i,cxGrid1DBTableView1Column1.Index]= '�����') then
              pom:= true;
          end;
     if pom then
        Panel2.Visible:=true
     else
        ShowMessage('����������� ������� !!!');
end;

procedure TfrmDogVolonteri.aKreirajPregledajDokumentExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.tblDogVolonteriodberi.close;
  if tag = 1 then
      dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=dm.tblDogVolonteriID.Value
  else
      dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=dm.tblDogVolonteriodberiID.Value;
  dmReport.tblDogVolonteriodberi.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.tblDogVolonteriodberiDATA.IsNull then
     begin
       RptStream := dmReport.tblDogVolonteriodberi.CreateBlobStream(dmReport.tblDogVolonteriodberiDATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
       begin
       RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
       If RichView <> Nil Then
          begin
           RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
           RichView.DataSet:=dmReport.frxDBDogVolonteri;
         end;
       MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
       if MasterData <> Nil then
          MasterData.DataSet:=dmReport.frxDBDogVolonteri;

       dmReport.frxReport1.ShowReport();

       frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 9);
       end;
     end;
end;

procedure TfrmDogVolonteri.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;

    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblDogVolonteriVID_DOKUMENT.Value:= dogVolonter;
    dm.tblDogVolonteriID_RE_FIRMA.Value:=dmKon.re;
    dm.tblDogVolonteriMB.Value:=dm.tblLicaVraboteniMB.Value;
    dm.tblDogVolonteriPREZIME.Value:=dm.tblLicaVraboteniPREZIME.Value;
    dm.tblDogVolonteriTATKOVO_IME.Value:=dm.tblLicaVraboteniTATKOVO_IME.Value;
    dm.tblDogVolonteriIME.Value:=dm.tblLicaVraboteniIME.Value;
    dm.tblDogVolonteriDATUM.Value:=now;
    dm.tblDogVolonteriLICNA_KARTA.Value:=dm.tblLicaVraboteniLICNA_KARTA.Value;

    Broj.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmDogVolonteri.aOtkaziDogovoriExecute(Sender: TObject);
begin
  inherited;
  Panel2.Visible:=false;
end;

procedure TfrmDogVolonteri.aZapisiDogovoriExecute(Sender: TObject);
var  i, idii:Integer;
     RptStream: TStream;
     SqlStream: TStream;
     sl: TStringList;
     value:Variant;
     RichView: TfrxRichView;
     MasterData: TfrxMasterData;
     ab:string;
begin

  if (OD_DATUM.Date > DO_DATUM.Date ) then
    begin
      ShowMessage('������ � �������� ������ !!!');
      DO_DATUM.SetFocus;
    end
  else
   begin
    if Validacija(panel2) = false then
      begin
       

        dmReport.OpenDialog1.FileName:='';
        dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
        dmReport.OpenDialog1.Execute();

        ab:=Broj_del.Text;
        with cxGrid1DBTableView1.DataController do
        for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
         begin
           if (DisplayTexts[i,cxGrid1DBTableView1Column1.Index]= '�����') then
            begin
               qMaxBroj.Close;
               qMaxBroj.ParamByName('arhivski_broj').AsString:=ab;
               qMaxBroj.ExecQuery;
               idii:=dmOtsustvo.insert12(dmOtsustvo.pInsertVolonteri,'MB', 'ID_RM_RE', 'VID_DOKUMENT', 'ID_RE_FIRMA', 'DATUM_OD', 'DATUM_DO', 'PLATA', 'BROJ_CASOVI', 'RAKOVODITEL', 'BROJ','DATUM','ARHIVSKI_BROJ',
                                                               GetValue(i,cxGrid1DBTableView1MB.Index),
                                                               GetValue(i,cxGrid1DBTableView1ID_RM_RE.Index),
                                                               dogVolonter, firma,
                                                               OD_DATUM.Date, DO_DATUM.Date,
                                                               GetValue(i,cxGrid1DBTableView1PLATA.Index),
                                                               GetValue(i,cxGrid1DBTableView1BROJ_CASOVI.Index),
                                                               GetValue(i,cxGrid1DBTableView1RAKOVODITEL.Index),
                                                               ab+'/'+inttostr(qMaxBroj.FldByName['maks'].AsInteger+1), now,Broj_Del.Text, 'ID');
               Panel2.Visible:=false;

                 dmReport.frxReport1.Clear;
                 dmReport.frxReport1.Script.Clear;

                 dmReport.tblDogVolonteriodberi.close;
                 dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=idii;
                 dmReport.tblDogVolonteriodberi.Open;

                 dmReport.Template.Close;
                 dmReport.Template.ParamByName('broj').Value:=1;
                 dmReport.Template.Open;


               if dmReport.OpenDialog1.FileName <> '' then
                  begin
                    RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
                    dmReport.frxReport1.LoadFromStream(RptStream);

                    RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
                    If RichView <> Nil Then
                      begin
                        RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
                        RichView.DataSet:=dmReport.frxDBDogVolonteri;
                      end;
                    MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
                    if MasterData <> Nil then
                       MasterData.DataSet:=dmReport.frxDBDogVolonteri;

                   dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 9);
                  end;
         end;
      end;
     dm.tblDogVolonteriodberi.FullRefresh;
     cxGrid1.SetFocus;
   end;
   end;
end;

procedure TfrmDogVolonteri.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        if (DATUM_OD.Text <> '')and (DATUM_DO.Text <> '') and (DATUM_DO.Date < DATUM_OD.Date ) then
          begin
               ShowMessage('������ � �������� ������ !!!');
               DATUM_DO.SetFocus;
          end
        else
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;
            cxGrid1.SetFocus;
          end;
    end;
  end;
end;

procedure TfrmDogVolonteri.CheckBox1Click(Sender: TObject);
var i:integer;
begin
  if CheckBox1.Checked then
     begin
        with cxGrid1DBTableView1.DataController do
        for I := 0 to FilteredRecordCount - 1 do
        begin
          Values[FilteredRecordIndex[i] , cxGrid1DBTableView1Column1.Index]:=  true;
        end;
     end
  else
     begin
        with cxGrid1DBTableView1.DataController do
        for I := 0 to FilteredRecordCount - 1 do
        begin
          Values[FilteredRecordIndex[i] , cxGrid1DBTableView1Column1.Index]:= false;
        end;
     end
end;

procedure TfrmDogVolonteri.siteD(kako:Boolean);   //procedura so koa gi stikliram - otstikliram site
var    suma, I:INTEGER;
begin
       
end;

procedure TfrmDogVolonteri.cxBarEditItem6PropertiesChange(
  Sender: TObject);
begin

   if cxBarEditItem6.EditValue = 0 then
   begin
      cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_OD, foLessEqual, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_DO, foGreater, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
   end
   else
     cxGrid1DBTableView1.DataController.Filter.Clear;
end;

procedure TfrmDogVolonteri.DO_DATUMExit(Sender: TObject);
var  i, idii:Integer;
     RptStream: TStream;
     SqlStream: TStream;
     sl: TStringList;
     value:Variant;
     RichView: TfrxRichView;
     MasterData: TfrxMasterData;
begin
  inherited;
   TEdit(Sender).Color:=clWhite;
//   if Validacija(panel2) = false then
//      begin
//        dmReport.Template.Close;
//        dmReport.Template.ParamByName('broj').Value:=1;
//        dmReport.Template.Open;
//
//        dmReport.OpenDialog1.FileName:='';
//        dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
//        dmReport.OpenDialog1.Execute();
//
//        with cxGrid1DBTableView1.DataController do
//        for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
//        begin
//          idii:= 100;
//          if (DisplayTexts[i,cxGrid1DBTableView1Column1.Index]= '�����') then
//          begin
//               idii:=dmOtsustvo.insert11(dmOtsustvo.pInsertVolonteri,'MB', 'ID_RM_RE', 'VID_DOKUMENT', 'ID_RE_FIRMA', 'DATUM_OD', 'DATUM_DO', 'PLATA', 'BROJ_CASOVI', 'RAKOVODITEL', 'BROJ','DATUM',
//                                                               GetValue(i,cxGrid1DBTableView1MB.Index),
//                                                               GetValue(i,cxGrid1DBTableView1ID_RM_RE.Index),
//                                                               dogVolonter, firma,
//                                                               OD_DATUM.Date, DO_DATUM.Date,
//                                                               GetValue(i,cxGrid1DBTableView1PLATA.Index),
//                                                               GetValue(i,cxGrid1DBTableView1BROJ_CASOVI.Index),
//                                                               GetValue(i,cxGrid1DBTableView1RAKOVODITEL.Index),
//                                                               'broj', now, 'ID');
//               Panel2.Visible:=false;
//
//               dmReport.frxReport1.Clear;
//               dmReport.frxReport1.Script.Clear;
//
//               dmReport.tblDogVolonteriodberi.close;
//               dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=idii;
//               dmReport.tblDogVolonteriodberi.Open;
//
//               if dmReport.OpenDialog1.FileName <> '' then
//                  begin
//                    RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
//                    dmReport.frxReport1.LoadFromStream(RptStream);
//
//                    RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
//                    If RichView <> Nil Then
//                      begin
//                        RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
//                        RichView.DataSet:=dmReport.frxDBDogVolonteri;
//                      end;
//                    MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
//                    if MasterData <> Nil then
//                       MasterData.DataSet:=dmReport.frxDBDogVolonteri;
//
//                   dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 9);
//                  end;
//          end;
//      end;
//     dm.tblDogVolonteriodberi.FullRefresh;
//     cxGrid1.SetFocus;
//   end;
end;

procedure TfrmDogVolonteri.FormCreate(Sender: TObject);
begin
  inherited;
   tblRMRE.ParamByName('firma').Value:=dmKon.re;
   tblRMRE.Open;
end;

procedure TfrmDogVolonteri.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 1 then
     begin
       dm.tblDogVolonteri.Close;
       dm.tblDogVolonteri.parambyName('mb').value:=dm.tblLicaVraboteniMB.Value;
       dm.tblDogVolonteri.parambyname('firma').value:=dmKon.re;
       dm.tblDogVolonteri.Open;

       dm.tblDogVolonteri.AutoUpdateOptions.AutoReWriteSqls:= true;
       cxGrid1DBTableView1Column1.Visible:=false;

     end
  else
     begin
       dm.tblDogVolonteriodberi.Close;
       dm.tblDogVolonteriodberi.parambyName('mb').value:='%';
       dm.tblDogVolonteriodberi.parambyname('firma').value:= dmKon.re;
       dm.tblDogVolonteriodberi.Open;

       cxGrid1DBTableView1.DataController.DataSource:=dm.dsDogVolonteriOdberi;
       dPanel.Visible:= false;
       Panel1.Visible:=true;

       dxBarManager1Bar1.Visible:=false;
     end;
  cxBarEditItem6.EditValue:= 0;
  if cxBarEditItem6.EditValue = 0 then
   begin
      cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_OD, foLessEqual, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_DO, foGreater, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
   end
   else
     cxGrid1DBTableView1.DataController.Filter.Clear;
end;

procedure TfrmDogVolonteri.OD_DATUMExit(Sender: TObject);
var  i, idii:Integer;
     RptStream: TStream;
     SqlStream: TStream;
     sl: TStringList;
     value:Variant;
     RichView: TfrxRichView;
     MasterData: TfrxMasterData;
begin
   TEdit(Sender).Color:=clWhite;
//   if Validacija(panel2) = false then
//      begin
//        dmReport.Template.Close;
//        dmReport.Template.ParamByName('broj').Value:=1;
//        dmReport.Template.Open;
//
//        dmReport.OpenDialog1.FileName:='';
//        dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
//        dmReport.OpenDialog1.Execute();
//
//        with cxGrid1DBTableView1.DataController do
//        for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
//         begin
//           if (DisplayTexts[i,cxGrid1DBTableView1Column1.Index]= '�����') then
//            begin
//               idii:=dmOtsustvo.insert11(dmOtsustvo.pInsertVolonteri,'MB', 'ID_RM_RE', 'VID_DOKUMENT', 'ID_RE_FIRMA', 'DATUM_OD', 'DATUM_DO', 'PLATA', 'BROJ_CASOVI', 'RAKOVODITEL', 'BROJ','DATUM',
//                                                               GetValue(i,cxGrid1DBTableView1MB.Index),
//                                                               GetValue(i,cxGrid1DBTableView1ID_RM_RE.Index),
//                                                               dogVolonter, firma,
//                                                               OD_DATUM.Date, DO_DATUM.Date,
//                                                               GetValue(i,cxGrid1DBTableView1PLATA.Index),
//                                                               GetValue(i,cxGrid1DBTableView1BROJ_CASOVI.Index),
//                                                               GetValue(i,cxGrid1DBTableView1RAKOVODITEL.Index),
//                                                              'broj', now, 'ID');
//               Panel2.Visible:=false;
//
//               dmReport.frxReport1.Clear;
//               dmReport.frxReport1.Script.Clear;
//
//               dmReport.tblDogVolonteriodberi.close;
//               dmReport.tblDogVolonteriodberi.ParamByName('ID').Value:=idii;
//               dmReport.tblDogVolonteriodberi.Open;
//
//               if dmReport.OpenDialog1.FileName <> '' then
//                  begin
//                    RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
//                    dmReport.frxReport1.LoadFromStream(RptStream);
//
//                    RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
//                    If RichView <> Nil Then
//                      begin
//                        RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
//                        RichView.DataSet:=dmReport.frxDBDogVolonteri;
//                      end;
//                    MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
//                    if MasterData <> Nil then
//                       MasterData.DataSet:=dmReport.frxDBDogVolonteri;
//
//                   dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 9);
//                  end;
//          end;
//      end;
//     dm.tblDogVolonteriodberi.FullRefresh;
//     cxGrid1.SetFocus;
//   end;
end;

end.
