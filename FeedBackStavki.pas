unit FeedBackStavki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxBlobEdit, cxImage, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxPC, ComCtrls, cxCheckBox, FIBDataSet, pFIBDataSet,
  Menus, StdCtrls, cxButtons, ExtCtrls, ActnList, cxContainer, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver;

type
  TfrmFeedBackStavki = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    dsRatingFrom: TDataSource;
    tblRatingFrom: TpFIBDataSet;
    tblHRVraboteni: TpFIBDataSet;
    dsHRVraboteni: TDataSource;
    dsPrasanja: TDataSource;
    tblPrasanja: TpFIBDataSet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    Panel1: TPanel;
    ZapisiButton: TcxButton;
    Panel2: TPanel;
    cxButton1: TcxButton;
    Panel3: TPanel;
    cxButton2: TcxButton;
    ActionList1: TActionList;
    aOdberiIspitanici: TAction;
    aOdberiOcenuvaci: TAction;
    aOdberiPrasanja: TAction;
    Panel4: TPanel;
    Grupa: TcxLookupComboBox;
    Label3: TLabel;
    aIzlez: TAction;
    tblHRVraboteniMB_OUT: TFIBStringField;
    tblHRVraboteniRABMESTONAZIV_OUT: TFIBStringField;
    tblHRVraboteniVRABOTENNAZIV_OUT: TFIBStringField;
    tblHRVraboteniRENAZIV_OUT: TFIBStringField;
    tblHRVraboteniOBRAZOVANIENAZIV_OUT: TFIBStringField;
    cxGridDBTableView2MB_OUT: TcxGridDBColumn;
    cxGridDBTableView2RABMESTONAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView2VRABOTENNAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView2RENAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView2OBRAZOVANIENAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView2Column1: TcxGridDBColumn;
    tblRatingFromID_OUT: TFIBIntegerField;
    tblRatingFromNAZIV_OUT: TFIBStringField;
    cxGridDBTableView1ID_OUT: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_OUT: TcxGridDBColumn;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    tblPrasanjaID_OUT: TFIBIntegerField;
    tblPrasanjaNAZIV_OUT: TFIBStringField;
    tblPrasanjaGRUPA_OUT: TFIBIntegerField;
    tblPrasanjaGRUPANAZIV_OUT: TFIBStringField;
    tblPrasanjaNORMATIV: TFIBFloatField;
    cxGridDBTableView3Column1: TcxGridDBColumn;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGridDBTableView3Column2: TcxGridDBColumn;
    cxGridDBTableView3Column3: TcxGridDBColumn;
    cxGridDBTableView3ID_OUT: TcxGridDBColumn;
    cxGridDBTableView3Column5: TcxGridDBColumn;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure aOdberiIspitaniciExecute(Sender: TObject);
    procedure aOdberiOcenuvaciExecute(Sender: TObject);
    procedure aOdberiPrasanjaExecute(Sender: TObject);
    procedure GrupaPropertiesEditValueChanged(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure cxGridDBTableView3KeyPress(Sender: TObject; var Key: Char);
    procedure cxGridDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGridDBTableView2KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFeedBackStavki: TfrmFeedBackStavki;

implementation

uses dmUnitUcinok, dmUnit, dmResources, Utils, dmKonekcija, dmMaticni;

{$R *.dfm}

procedure TfrmFeedBackStavki.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmFeedBackStavki.aOdberiIspitaniciExecute(Sender: TObject);
var  i:Integer;
begin
      with cxGridDBTableView2.DataController do
      for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
      begin
          if (DisplayTexts[i,cxGridDBTableView2Column1.Index]= '�����') then
          begin
             dmUcinok.tblIspitanici.Insert;
             dmUcinok.tblIspitaniciFEEDBACK_ID.Value:=dmUcinok.tblFeedBackID.Value;
             dmUcinok.tblIspitaniciMB.Value:= GetValue(i,cxGridDBTableView2MB_OUT.Index);
             dmUcinok.tblIspitanici.Post;
             dmUcinok.tblIspitanici.FullRefresh;
          end;
      end;
      tblHRVraboteni.FullRefresh;
      cxGrid3.SetFocus;
end;

procedure TfrmFeedBackStavki.aOdberiOcenuvaciExecute(Sender: TObject);
var  i:Integer;
begin
      with cxGridDBTableView1.DataController do
      for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
      begin
          if (DisplayTexts[i,cxGridDBTableView1Column1.Index]= '�����') then
          begin
             dmUcinok.tblOcenuvaci.Insert;
             dmUcinok.tblOcenuvaciFEEDBACK_ID.Value:=dmUcinok.tblFeedBackID.Value;
             dmUcinok.tblOcenuvaciRATING_FROM_ID.Value:= GetValue(i,cxGridDBTableView1ID_OUT.Index);
             dmUcinok.tblOcenuvaci.Post;
             dmUcinok.tblOcenuvaci.FullRefresh;
          end;
      end;
      tblRatingFrom.fullRefresh;
      cxGrid2.SetFocus;
end;

procedure TfrmFeedBackStavki.aOdberiPrasanjaExecute(Sender: TObject);
var  i:Integer;
begin
      with cxGridDBTableView3.DataController do
      for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
      begin
          if (DisplayTexts[i,cxGridDBTableView3Column1.Index]= '�����') then
          begin
             dmUcinok.tblPrasalnik.Insert;
             dmUcinok.tblPrasalnikFEEDBACK_ID.Value:=dmUcinok.tblFeedBackID.Value;
             dmUcinok.tblPrasalnikPRASANJE_ID.Value:= GetValue(i,cxGridDBTableView3ID_OUT.Index);
             dmUcinok.tblPrasalnik.Post;
             dmUcinok.tblPrasalnik.FullRefresh;
          end;
      end;
      tblPrasanja.FullRefresh;
      cxGrid1.SetFocus;
end;

procedure TfrmFeedBackStavki.cxGridDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView1);
end;

procedure TfrmFeedBackStavki.cxGridDBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView2);
end;

procedure TfrmFeedBackStavki.cxGridDBTableView3KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGridDBTableView3);
end;

procedure TfrmFeedBackStavki.FormCreate(Sender: TObject);
begin
     tblRatingFrom.Open;
     tblPrasanja.ParamByName('grupa').Value:=Grupa.EditValue;
     tblPrasanja.Open;
     tblHRVraboteni.ParamByName('FIRMA').Value:=dmKon.re;
     tblHRVraboteni.Open;
     dmUcinok.tblGrupaPrasanja.Open;
     Grupa.EditValue:=dmUcinok.tblGrupaPrasanjaID.Value;
    // dxRibbonStatusBar.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmFeedBackStavki.GrupaPropertiesEditValueChanged(Sender: TObject);
var  i:Integer;
begin
      tblPrasanja.ParamByName('grupa').Value:=Grupa.EditValue;
      tblPrasanja.FullRefresh;
//      with cxGridDBTableView3.DataController do
//      for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
//          DisplayTexts[i,cxGridDBTableView3Column1.Index]:='�����'

end;

end.
