unit StazKalkulator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, Menus, StdCtrls, cxButtons, cxGroupBox, cxTextEdit,
  cxLabel, cxMaskEdit, cxDropDownEdit, cxCalendar, ComCtrls, ExtCtrls,
  cxHyperLinkEdit, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxCore,
  cxDateUtils;

type
  TfrmStazKalkulator = class(TForm)
    Panel1: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxLabel6: TcxLabel;
    RGodina: TcxTextEdit;
    cxLabel7: TcxLabel;
    RDen: TcxTextEdit;
    cxLabel8: TcxLabel;
    RMesec: TcxTextEdit;
    cxGroupBox2: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel5: TcxLabel;
    MDatum: TcxDateEdit;
    DatumDogovor: TcxDateEdit;
    cxButton1: TcxButton;
    cxHyperLinkEdit1: TcxHyperLinkEdit;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStazKalkulator: TfrmStazKalkulator;

implementation

uses dmUnit, dmUnitOtsustvo, Utils;

{$R *.dfm}

procedure TfrmStazKalkulator.cxButton1Click(Sender: TObject);
begin
      if Validacija(Panel1) = false then
         begin
           RGodina.Text:=intToStr(dmOtsustvo.zemiRezultat(dm.pStazPresmetka, 'DATUMDOGOVOR', 'DATUMSTAZ', Null, Null, Null, DatumDogovor.EditValue, MDatum.EditValue, Null, Null, Null,'godina_out'));
           RMesec.Text:=intToStr(dmOtsustvo.zemiRezultat(dm.pStazPresmetka, 'DATUMDOGOVOR', 'DATUMSTAZ', Null, Null, Null, DatumDogovor.EditValue, MDatum.EditValue, Null, Null, Null,'mesec_out'));
           RDen.Text:=intToStr(dmOtsustvo.zemiRezultat(dm.pStazPresmetka, 'DATUMDOGOVOR', 'DATUMSTAZ', Null, Null, Null,DatumDogovor.EditValue, MDatum.EditValue, Null, Null, Null,'den_out'));
         end;
end;

procedure TfrmStazKalkulator.FormShow(Sender: TObject);
begin
      DatumDogovor.SetFocus;
end;

procedure TfrmStazKalkulator.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmStazKalkulator.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmStazKalkulator.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

end.
