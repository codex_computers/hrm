inherited frmSZObrazovanie: TfrmSZObrazovanie
  Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
  ClientHeight = 612
  ClientWidth = 738
  ExplicitWidth = 754
  ExplicitHeight = 650
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 738
    Height = 314
    ExplicitWidth = 738
    ExplicitHeight = 314
    inherited cxGrid1: TcxGrid
      Width = 734
      Height = 310
      ExplicitWidth = 734
      ExplicitHeight = 310
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsSZObrazovanie
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'NAZIV'
          Width = 136
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 352
        end
        object cxGrid1DBTableView1KREDITI: TcxGridDBColumn
          DataBinding.FieldName = 'KREDITI'
        end
        object cxGrid1DBTableView1DENOVI_GO: TcxGridDBColumn
          DataBinding.FieldName = 'DENOVI_GO'
          Width = 163
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 440
    Width = 738
    Height = 149
    ExplicitTop = 440
    ExplicitWidth = 738
    ExplicitHeight = 149
    inherited Label1: TLabel
      Left = 525
      Top = 30
      Visible = False
      ExplicitLeft = 525
      ExplicitTop = 30
    end
    object Label2: TLabel [1]
      Left = 24
      Top = 25
      Width = 145
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 119
      Top = 52
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [3]
      Left = 40
      Top = 99
      Width = 129
      Height = 31
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel [4]
      Left = 40
      Top = 79
      Width = 129
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1088#1077#1076#1080#1090#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel [5]
      Left = 261
      Top = 79
      Width = 129
      Height = 18
      AutoSize = False
      Caption = #1089#1087#1086#1088#1077#1076' '#1045#1050#1058#1057
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 581
      Top = 27
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsSZObrazovanie
      TabOrder = 5
      Visible = False
      ExplicitLeft = 581
      ExplicitTop = 27
    end
    inherited OtkaziButton: TcxButton
      Left = 639
      Top = 112
      TabOrder = 6
      ExplicitLeft = 639
      ExplicitTop = 112
    end
    inherited ZapisiButton: TcxButton
      Left = 558
      Top = 112
      TabOrder = 4
      ExplicitLeft = 558
      ExplicitTop = 112
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 175
      Top = 22
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsSZObrazovanie
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object txtOpis: TcxDBTextEdit
      Left = 175
      Top = 49
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmSis.dsSZObrazovanie
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 383
    end
    object DenoviGo: TcxDBTextEdit
      Left = 175
      Top = 103
      Hint = 
        #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088' '#1089#1087#1086#1088#1077#1076' '#1089#1090#1077#1087#1077#1085' '#1085#1072' '#1079#1072#1074#1088#1096#1077#1085#1086' '#1086 +
        #1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'DENOVI_GO'
      DataBinding.DataSource = dmSis.dsSZObrazovanie
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object Krediti: TcxDBTextEdit
      Left = 175
      Top = 76
      BeepOnEnter = False
      DataBinding.DataField = 'KREDITI'
      DataBinding.DataSource = dmSis.dsSZObrazovanie
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 738
    ExplicitWidth = 738
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 589
    Width = 738
    ExplicitTop = 589
    ExplicitWidth = 738
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 224
  end
  inherited PopupMenu1: TPopupMenu
    Left = 352
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    Top = 208
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Top = 240
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageHeader.LeftTitle.Strings = (
        '')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40673.634012951390000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 648
    Top = 240
  end
end
