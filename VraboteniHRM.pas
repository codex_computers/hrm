unit VraboteniHRM;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon, ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxGroupBox, cxRadioGroup, cxImage, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxMemo, ExtDlgs, FIBDataSet, pFIBDataSet,
  cxBlobEdit, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, FIBQuery, pFIBQuery, cxCheckGroup, cxCheckComboBox,
  cxSplitter, cxHyperLinkEdit, cxLabel, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmHRMVraboteni = class(TfrmMaster)
    OpenPictureDialog1: TOpenPictureDialog;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    PREZIME: TcxDBTextEdit;
    TATKOVO_IME: TcxDBTextEdit;
    IME: TcxDBTextEdit;
    MOMINSKO_PREZIME: TcxDBTextEdit;
    Pol: TcxDBRadioGroup;
    BracnaSostojba: TcxDBRadioGroup;
    Label9: TLabel;
    ZDR_SOSTOJBA: TcxDBMemo;
    Label10: TLabel;
    NACIONALNOSTID: TcxDBTextEdit;
    NACIONALNOST: TcxDBLookupComboBox;
    VEROISPOVED: TcxDBLookupComboBox;
    VEROISPOVEDID: TcxDBTextEdit;
    Label11: TLabel;
    Slika: TcxDBImage;
    Button1: TButton;
    aObrazovanieObuka: TAction;
    aObrazovanieObuka1: TMenuItem;
    aRabotnoIskustvo: TAction;
    N2: TMenuItem;
    aLVK: TAction;
    N3: TMenuItem;
    aKontakt: TAction;
    N4: TMenuItem;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    aDosie: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    aVrabRM: TAction;
    N5: TMenuItem;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton23: TdxBarLargeButton;
    aDeca: TAction;
    aDosieDizajn: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    OBRAZOVANIE_naziv: TcxDBLookupComboBox;
    OBRAZOVANIE_ID: TcxDBTextEdit;
    Label12: TLabel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxHintStyleController1: TcxHintStyleController;
    gbRadjanje: TcxGroupBox;
    Label7: TLabel;
    DATUM_RADJANJE: TcxDBDateEdit;
    Label14: TLabel;
    txtMesto: TcxDBTextEdit;
    cbMesto: TcxDBLookupComboBox;
    Label8: TLabel;
    Label13: TLabel;
    txtDrzava: TcxTextEdit;
    txtOpstina: TcxTextEdit;
    qEditPLTVraboteni: TpFIBQuery;
    qRERM: TpFIBQuery;
    dxBarManager1Bar7: TdxBar;
    aDogovorVrabotuvanje: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    aDogovorZaHonorarci: TAction;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1SSTRUCNAPNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_RADJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1POL: TcxGridDBColumn;
    cxGrid1DBTableView1STEPEN_STRUCNA_PODGOTOVKA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_NACIONALNOST: TcxGridDBColumn;
    cxGrid1DBTableView1ID_VEROISPOVED: TcxGridDBColumn;
    cxGrid1DBTableView1MOMINSKO_PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1BRACNA_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1ZDR_SOSTOJBA: TcxGridDBColumn;
    cxGrid1DBTableView1SLIKA: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_RADJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1OPSTINA_RADJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1DRZAVA_RADJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1NACIONALNOSNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VEROISPOVEDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POLNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BRAK: TcxGridDBColumn;
    cxGrid1DBTableView1INVALID: TcxGridDBColumn;
    cxGrid1DBTableView1INVALIDNAZIV: TcxGridDBColumn;
    dxBarLargeButton17: TdxBarLargeButton;
    aDokumenti: TAction;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aSezonskoRabotewe: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    aPovredi: TAction;
    aNKM: TAction;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    Telesno: TcxDBTextEdit;
    Label2: TLabel;
    Label15: TLabel;
    BrLicnaKarta: TcxDBTextEdit;
    Label16: TLabel;
    cxGrid1DBTableView1OBRAZOVANIE: TcxGridDBColumn;
    cxGrid1DBTableView1LICNA_KARTA: TcxGridDBColumn;
    cxGrid1DBTableView1TELESNO_OSTETUVANJE: TcxGridDBColumn;
    SplitterdPanel: TcxSplitter;
    dxBarButton10: TdxBarButton;
    aSpecijalizacija: TAction;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton30: TdxBarLargeButton;
    aPodatociLekari: TAction;
    dxBarButton11: TdxBarButton;
    Label17: TLabel;
    BROJ_PASOS: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ_PASOS: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    aDogVolonteri: TAction;
    dxBarLargeButton31: TdxBarLargeButton;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    rgStatus: TcxDBRadioGroup;
    dxBarManager1Bar9: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    cxGrid1DBTableView1ActivenNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1ACTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1RM_RE_ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1SIS_ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1RM_ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    lblCustom1: TLabel;
    txtCustom1: TcxDBTextEdit;
    lblCustom2: TLabel;
    txtCustom2: TcxDBTextEdit;
    lblCustom3: TLabel;
    txtCustom3: TcxDBTextEdit;
    Label18: TLabel;
    ZVANJE: TcxDBTextEdit;
    cxGrid1DBTableView1ZVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VID_DOKUMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure ZDR_SOSTOJBADblClick(Sender: TObject);
    procedure aObrazovanieObukaExecute(Sender: TObject);
    procedure aRabotnoIskustvoExecute(Sender: TObject);
    procedure aLVKExecute(Sender: TObject);
    procedure aKontaktExecute(Sender: TObject);
    procedure SifraExit(Sender: TObject);
    procedure aDosieExecute(Sender: TObject);
    procedure aDecaExecute(Sender: TObject);
    procedure aDosieDizajnExecute(Sender: TObject);
    function proverkaModul11(broj: Int64): boolean;
    procedure SifraPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure setDatumPol(matBr:String);
    procedure aNovExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aZapisiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aDogovorVrabotuvanjeExecute(Sender: TObject);
    procedure aDogovorZaHonorarciExecute(Sender: TObject);
    procedure aDokumentiExecute(Sender: TObject);
    procedure aVrabRMExecute(Sender: TObject);
    procedure aBaranjeZaGodisenOdmorExecute(Sender: TObject);
    procedure aSezonskoRaboteweExecute(Sender: TObject);
    procedure aNKMExecute(Sender: TObject);
    procedure aPovrediExecute(Sender: TObject);
    procedure aSpecijalizacijaExecute(Sender: TObject);
    procedure aPodatociLekariExecute(Sender: TObject);
    procedure aDogVolonteriExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure cxBarEditItem3PropertiesChange(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure CustomFields();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHRMVraboteni: TfrmHRMVraboteni;
  StateActive:TDataSetState;
  custom1, custom2, custom3:String;

implementation

uses DaNe, dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, Utils,
  dmUnitOtsustvo, dmSistematizacija, Nacionalnost, Veroispoved, Notepad,
  VestiniKvalifikacii, ObrazovanieObuka, IskustvoRabotno, Kontakt, VrabotenRM,
  Deca, dmReportUnit, DogovorVrabotuvanje, DogHonorarnaSorabotka,
  PregledNaDokumenti, BaranjeZaGodisenOdmor, DogSezonskoRabotenje, NKM, Povreda,
  dmUnitUcinok, PovredaNaRM, Specijalizacija, PodatociLekari, DecaSopruznici,
  DogVolonteri;

{$R *.dfm}

procedure TfrmHRMVraboteni.aAzurirajExecute(Sender: TObject);
var pom:integer;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    //pom:=dmOtsustvo.zemiBroj(dm.pCountLiceDogovor,'MB', Null, Null,Null,dm.tblLicaVraboteniMB.Value, Null, Null,Null, 'BROJ');
    SplitterdPanel.OpenSplitter;
    {if pom > 0 then
       begin
         ShowMessage('�������� �� �� ��������� ���� ��������. �� ��� ���� ��� ������������ ������� �� ����������� !!!');
         dPanel.Enabled:=True;
         lPanel.Enabled:=False;
         Sifra.Enabled:=false;
        // PREZIME.Enabled:=false;
        // TATKOVO_IME.Enabled:=false;
        // IME.Enabled:=false;
         DATUM_RADJANJE.Enabled:=false;
         //txtMesto.Enabled:=false;
         //cbMesto.Enabled:=false;
         //OBRAZOVANIE_naziv.Enabled:=false;
         //OBRAZOVANIE_ID.Enabled:=false;
         BrLicnaKarta.SetFocus;
         cxGrid1DBTableView1.DataController.DataSet.Edit;
       end
    else }
      // begin
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          Sifra.Enabled:=false;
          Prezime.SetFocus;
          cxGrid1DBTableView1.DataController.DataSet.Edit;
      // end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmHRMVraboteni.aBaranjeZaGodisenOdmorExecute(Sender: TObject);
begin
  inherited;
  frmBaranjeZaGodisenOdmor:=TfrmBaranjeZaGodisenOdmor.Create(Application);
  frmBaranjeZaGodisenOdmor.ShowModal;
  frmBaranjeZaGodisenOdmor.Free;
end;

procedure TfrmHRMVraboteni.aBrisiExecute(Sender: TObject);
var pom:integer;
begin
    if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
      begin
         pom:=dmOtsustvo.zemiBroj(dm.pCountLiceDogovor,'MB', Null, Null,Null,dm.tblLicaVraboteniMB.Value,Null, Null, Null, 'BROJ');
         if pom > 0 then
           begin
             ShowMessage('�������� �� �� ������� �������. �� ��� ���� ��� ������������ ������� �� ����������� !!!');
           end
         else cxGrid1DBTableView1.DataController.DataSet.Delete();
      end;
end;

procedure TfrmHRMVraboteni.aDecaExecute(Sender: TObject);
begin
      frmDecaSopruznici:=TfrmDecaSopruznici.Create(Application);
      frmDecaSopruznici.Tag:=1;
      frmDecaSopruznici.ShowModal;
      frmDecaSopruznici.Free;
end;

procedure TfrmHRMVraboteni.aDogovorVrabotuvanjeExecute(Sender: TObject);
begin
     frmDogovorVrabotuvanje:=TfrmDogovorVrabotuvanje.Create(Application);
     frmDogovorVrabotuvanje.Tag:=1;
     frmDogovorVrabotuvanje.ShowModal;
     frmDogovorVrabotuvanje.Free;
end;

procedure TfrmHRMVraboteni.aDogovorZaHonorarciExecute(Sender: TObject);
begin
     frmDogHonorarnaSorabotka:=TfrmDogHonorarnaSorabotka.Create(Application);
     frmDogHonorarnaSorabotka.Tag:=1;
     frmDogHonorarnaSorabotka.ShowModal;
     frmDogHonorarnaSorabotka.Free;
end;

procedure TfrmHRMVraboteni.aDogVolonteriExecute(Sender: TObject);
begin
  inherited;
  frmDogVolonteri:= TfrmDogVolonteri.Create(Self, False);
  frmDogVolonteri.Tag:=1;
  frmDogVolonteri.ShowModal;
  frmDogVolonteri.Free;
end;

procedure TfrmHRMVraboteni.aDokumentiExecute(Sender: TObject);
begin
     frmPregledNaDokumenti:=TfrmPregledNaDokumenti.Create(Application);
     frmPregledNaDokumenti.Caption:='������� �� ��������� �� ' + dm.tblLicaVraboteniNAZIV_VRABOTEN_TI.Value;
     frmPregledNaDokumenti.ShowModal;
     frmPregledNaDokumenti.Free;
end;

procedure TfrmHRMVraboteni.aDosieDizajnExecute(Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30074;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblVrabotenLP.Close;
     dm.tblVrabotenLP.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dm.tblVrabotenLP.open;

     dmSis.tblRabotnoIskustvo.Close;
     dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:='0';
     dmSis.tblRabotnoIskustvo.Open;

     dmSis.tblObrazovanieObuka.Close;

     dmSis.tblObrazovanieObuka.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:='0';
     dmSis.tblObrazovanieObuka.Open;

     dmSis.tblLVK.Close;
     dmSis.tblLVK.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dmSis.tblLVK.ParamByName('MolbaID').Value:='0';
     dmSis.tblLVK.open;

     dm.tblLVKSJ.Close;
     dm.tblLVKSJ.ParamByName('molbaID').Value:='0';
     dm.tblLVKSJ.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dm.tblLVKSJ.Open;

     dm.tblDeca.Close;
     dm.tblDeca.ParamByName('roditelMB').Value:=dm.tblLicaVraboteniMB.Value;
     dm.tblDeca.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
//     dmReport.frxReport1.ShowReport;
end;

procedure TfrmHRMVraboteni.aDosieExecute(Sender: TObject);
  var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30074;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblVrabotenLP.Close;
     dm.tblVrabotenLP.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dm.tblVrabotenLP.open;

     dmSis.tblRabotnoIskustvo.Close;
     dmSis.tblRabotnoIskustvo.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dmSis.tblRabotnoIskustvo.ParamByName('MolbaID').Value:='0';
     dmSis.tblRabotnoIskustvo.Open;

     dmSis.tblObrazovanieObuka.Close;
     dmSis.tblObrazovanieObuka.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dmSis.tblObrazovanieObuka.ParamByName('MolbaID').Value:='0';
     dmSis.tblObrazovanieObuka.Open;

     dmSis.tblLVK.Close;
     dmSis.tblLVK.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dmSis.tblLVK.ParamByName('MolbaID').Value:='0';
     dmSis.tblLVK.open;

     dm.tblLVKSJ.Close;
     dm.tblLVKSJ.ParamByName('molbaID').Value:='0';
     dm.tblLVKSJ.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
     dm.tblLVKSJ.Open;

     dm.tblDeca.Close;
     dm.tblDeca.ParamByName('roditelMB').Value:=dm.tblLicaVraboteniMB.Value;
     dm.tblDeca.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;

end;

procedure TfrmHRMVraboteni.aHelpExecute(Sender: TObject);
begin
  inherited;
   if tag = 0  then
      Application.HelpContext(8);
   if tag = 1  then
      Application.HelpContext(23);
end;

procedure TfrmHRMVraboteni.aKontaktExecute(Sender: TObject);
begin
  inherited;
  frmKontakt:=TfrmKontakt.Create(Self,false);
  frmKontakt.Tag:=3;
  frmKontakt.ShowModal;
  frmKontakt.Free;
end;

procedure TfrmHRMVraboteni.aLVKExecute(Sender: TObject);
begin
  inherited;
  frmVestiniKvalifikacii:=TfrmVestiniKvalifikacii.Create(Self, false);
  frmVestiniKvalifikacii.Tag:=3;
  frmVestiniKvalifikacii.ShowModal;
  frmVestiniKvalifikacii.Free;
end;

procedure TfrmHRMVraboteni.aNKMExecute(Sender: TObject);
begin
     frmNKM:=TfrmNKM.Create(Application);
     frmNKM.Tag:=1;
     frmNKM.ShowModal;
     frmnkm.Free;

end;

procedure TfrmHRMVraboteni.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    SplitterdPanel.OpenSplitter;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    txtOpstina.Clear;
    txtDrzava.Clear;
    dm.tblLicaVraboteniBRACNA_SOSTOJBA.Value:=0;
    dm.tblLicaVraboteniINVALID.Value:=0;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmHRMVraboteni.aObrazovanieObukaExecute(Sender: TObject);
begin
  inherited;
  frmObrazovanieObuka:=TfrmObrazovanieObuka.Create(Self,False);
  frmObrazovanieObuka.Tag:=3;
  frmObrazovanieObuka.ShowModal;
  frmObrazovanieObuka.Free;
end;

procedure TfrmHRMVraboteni.aOtkaziExecute(Sender: TObject);
begin
  inherited;
        Sifra.Enabled:=true;
        PREZIME.Enabled:=true;
        TATKOVO_IME.Enabled:=true;
        IME.Enabled:=true;
        DATUM_RADJANJE.Enabled:=true;
        txtMesto.Enabled:=true;
        cbMesto.Enabled:=true;
        OBRAZOVANIE_naziv.Enabled:=true;
        OBRAZOVANIE_ID.Enabled:=true;
end;

procedure TfrmHRMVraboteni.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  if cxBarEditItem3.EditValue = 0 then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('���� ����')
  Else
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� ����');
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmHRMVraboteni.aPodatociLekariExecute(Sender: TObject);
begin
  inherited;
  frmPodatociLekari:=TfrmPodatociLekari.Create(self, false);
  frmPodatociLekari.Tag:=1;
  frmPodatociLekari.ShowModal;
  frmPodatociLekari.Free;
end;

procedure TfrmHRMVraboteni.aPovrediExecute(Sender: TObject);
begin
  inherited;
  frmPovredaNaRM:=TfrmPovredaNaRM.Create(Application);
  frmPovredaNaRM.Tag:=1;
  frmPovredaNaRM.ShowModal;
  frmPovredaNaRM.Free;
end;

procedure TfrmHRMVraboteni.aRabotnoIskustvoExecute(Sender: TObject);
begin
  inherited;
  frmIskustvoRabotno:=TfrmIskustvoRabotno.Create(Self, False);
  frmIskustvoRabotno.Tag:=3;
  frmIskustvoRabotno.ShowModal;
  frmIskustvoRabotno.Free;
end;

procedure TfrmHRMVraboteni.aSezonskoRaboteweExecute(Sender: TObject);
begin
     frmDogSezonskoRabotenje:=TfrmDogSezonskoRabotenje.Create(Application);
     frmDogSezonskoRabotenje.Tag:=1;
     frmDogSezonskoRabotenje.ShowModal;
     frmDogSezonskoRabotenje.Free;
end;

procedure TfrmHRMVraboteni.aSpecijalizacijaExecute(Sender: TObject);
begin
     frmSpecijalizacija:=TfrmSpecijalizacija.Create(Application);
     frmSpecijalizacija.Tag:=1;
     frmSpecijalizacija.ShowModal;
     frmSpecijalizacija.Free;
end;

procedure TfrmHRMVraboteni.aVrabRMExecute(Sender: TObject);
begin
  inherited;
  frmPromenaRM:=TfrmPromenaRM.Create(Application);
  frmPromenaRM.ShowModal;
  frmPromenaRM.Free;
end;

procedure TfrmHRMVraboteni.aZapisiExecute(Sender: TObject);
begin
    inherited;
    Sifra.Enabled:=true;
  //  PREZIME.Enabled:=true;
    //TATKOVO_IME.Enabled:=true;
   // IME.Enabled:=true;
    DATUM_RADJANJE.Enabled:=true;
   // txtMesto.Enabled:=true;
  //  cbMesto.Enabled:=true;
 //   OBRAZOVANIE_naziv.Enabled:=true;
//    OBRAZOVANIE_ID.Enabled:=true;
end;

procedure TfrmHRMVraboteni.Button1Click(Sender: TObject);
begin
  OpenPictureDialog1.Filter := 'JPEG Image File |*.jpg;*.jpeg';
  OpenPictureDialog1.Execute();
  if OpenPictureDialog1.FileName <> '' then
     dm.tblLicaVraboteniSLIKA.LoadFromFile(OpenPictureDialog1.FileName);
end;

procedure TfrmHRMVraboteni.cxBarEditItem3PropertiesChange(Sender: TObject);
begin
  if cxBarEditItem3.EditValue = 0 then
     cxGrid1DBTableView1.DataController.Filter.Clear
   else
    begin
      cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1ACTIVEN, foEqual, 1, '�������');
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
   end
end;

procedure TfrmHRMVraboteni.cxDBTextEditAllExit(Sender: TObject);
begin
  if (Sender=TcxComboBoxLookupData(cbMesto))and (txtMesto.Text<>'') then
  begin
      if dmmat.tblmesto.Locate('ID',VarArrayOf([txtMesto.Text]),[]) then
         begin
           txtOpstina.Text:=dmMat.tblMestoOpstinaNaziv.Value;
           txtDrzava.Text:=dmMat.tblMestoDRZAVA_NAZIV.Value;
         end;
  end
  else if (txtMesto.Text='') then
  begin
     txtOpstina.Clear;
     txtDrzava.Clear;
  end;

  inherited;
end;

procedure TfrmHRMVraboteni.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if dm.tblLicaVraboteni.State = dsBrowse then
  begin
    txtOpstina.Text:=dm.tblLicaVraboteniOPSTINA_RADJANJE.AsString;
    txtDrzava.Text:=dm.tblLicaVraboteniDRZAVA_RADJANJE.AsString;
  end;
  inherited;
end;

procedure TfrmHRMVraboteni.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
       VK_INSERT:begin
         if (Sender = NACIONALNOSTID) or (Sender = NACIONALNOST) then
            begin
              frmNacionalnost:=TfrmNacionalnost.Create(Application);
              frmNacionalnost.Tag:=1;
              frmNacionalnost.ShowModal;
              frmNacionalnost.Free;
              dm.tblLicaVraboteniID_NACIONALNOST.Value:=dmOtsustvo.tblNacionalnostID.Value;
            end;

         if (Sender = VEROISPOVED) or (Sender = VEROISPOVEDID) then
            begin
              frmVeroispoved:=TfrmVeroispoved.Create(Application);
              frmVeroispoved.Tag:=1;
              frmVeroispoved.ShowModal;
              frmVeroispoved.Free;
              dm.tblLicaVraboteniID_VEROISPOVED.Value:=dmOtsustvo.tblVeroispovedID.Value;
            end;
       end;
  end;
end;

procedure TfrmHRMVraboteni.FormCreate(Sender: TObject);
begin
  inherited;
//  dxRibbon1.ColorSchemeName := dmRes.skin_name;
  dmOtsustvo.tblNacionalnost.Open;
  dmOtsustvo.tblVeroispoved.Open;
  dmMat.tblMesto.Open;
  Position:=poDesktopCenter;
  WindowState:=wsMaximized;
end;

procedure TfrmHRMVraboteni.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 0 then
     begin
        dm.tblLicaVraboteni.close;
        dm.tblLicaVraboteni.ParamByName('firma').Value:=0;
        dm.tblLicaVraboteni.ParamByName('param').Value:=0;
        dm.tblLicaVraboteni.Open;

        cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1ACTIVEN, foEqual, 1, '�������');
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
     end
  else if tag = 1 then
     begin
        dm.tblLicaVraboteni.close;
        dm.tblLicaVraboteni.ParamByName('firma').Value:=dmkon.re;
        dm.tblLicaVraboteni.ParamByName('param').Value:=1;
        dm.tblLicaVraboteni.Open;

        dPanel.Visible:=false;
        caption:='������� �� ���������';

        cxGrid1DBTableView1RABOTNOMESTONAZIV.Visible:=true;
        cxGrid1DBTableView1RABOTNAEDINICANAZIV.Visible:=true;

        dxBarManager1Bar1.Visible:=false;
        dxBarManager1Bar7.Visible:=false;
        dxBarManager1Bar8.Visible:=True;
        dxBarManager1Bar9.Visible:=False;

        aNov.Enabled:=false;
        aAzuriraj.Enabled:=false;
        aBrisi.Enabled:=false;
     end
    else if tag = 2 then
     begin
        dm.tblLicaVraboteni.close;
        dm.tblLicaVraboteni.ParamByName('firma').Value:=dmkon.re;
        dm.tblLicaVraboteni.ParamByName('param').Value:=2;
        dm.tblLicaVraboteni.Open;

        dPanel.Visible:=false;
        caption:='������� �� ���������';

        cxGrid1DBTableView1RABOTNOMESTONAZIV.Visible:=true;
        cxGrid1DBTableView1RABOTNAEDINICANAZIV.Visible:=true;

        dxBarManager1Bar1.Visible:=false;
        dxBarManager1Bar7.Visible:=false;
        dxBarManager1Bar8.Visible:=True;

        aNov.Enabled:=false;
        aAzuriraj.Enabled:=false;
        aBrisi.Enabled:=false;
     end
    else if tag = 3 then
     begin
        dm.tblLicaVraboteni.close;
        dm.tblLicaVraboteni.ParamByName('firma').Value:=0;
        dm.tblLicaVraboteni.ParamByName('param').Value:=0;
        dm.tblLicaVraboteni.Open;



        dPanel.Visible:=false;
        caption:='������� �� �����������';

        cxGrid1DBTableView1RABOTNOMESTONAZIV.Visible:=False;
        cxGrid1DBTableView1RABOTNAEDINICANAZIV.Visible:=False;

        dxBarManager1Bar1.Visible:=false;
        dxBarManager1Bar7.Visible:=True;
        dxBarManager1Bar8.Visible:=True;
        dxBarManager1Bar9.Visible:=False;

        aNov.Enabled:=false;
        aAzuriraj.Enabled:=false;
        aBrisi.Enabled:=false;

        cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
           try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1MB, foEqual, dm.tblResenieOtkazMB.Value, '������� ���');
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
           finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
           end
     end;

    CustomFields();
end;

function TfrmHRMVraboteni.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;
procedure TfrmHRMVraboteni.setDatumPol(matBr:String);
var  datum:string;
begin
     datum:= matBr[1] + matBr[2] + '.' + matBr[3] + matBr[4] + '.';
     if StrToInt(matBr[5]) = 9 then
        datum:=datum + '1'
     else if StrToInt(matBr[5]) in [0,1]then
        datum:=datum + '2';
     datum:=datum + matBr[5] + matBr[6]+ matBr[7];
     dm.tblLicaVraboteniDATUM_RADJANJE.Value:=StrToDate(datum);
     if strtoint(matBr[10]) in [0,1,2,3,4] then
        dm.tblLicaVraboteniPOL.Value:=1
     else
     if strtoint(matBr[10]) in [5,6,7,8,9] then
        dm.tblLicaVraboteniPOL.Value:=2;
end;
procedure TfrmHRMVraboteni.SifraExit(Sender: TObject);
var pom, date:string;
begin
   TEdit(Sender).Color:=clWhite;
   if (sifra.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State in [dsInsert, dsEdit]) then

     if proverkaModul11(StrToInt64(sifra.Text))then
       setDatumPol(dm.tblLicaVraboteniMB.Value);
end;

procedure TfrmHRMVraboteni.SifraPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  if (sifra.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State in [dsInsert, dsEdit]) then
     if proverkaModul11(StrToInt64(sifra.Text)) = false then
        begin
           Error:=true;
           ErrorText:='��������� ������� ��� �� � ������� !!!';
           sifra.SetFocus;
        end
     else  Error:=false;
end;

procedure TfrmHRMVraboteni.ZDR_SOSTOJBADblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dm.tblLicaVraboteniZDR_SOSTOJBA.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dm.tblLicaVraboteniZDR_SOSTOJBA.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

procedure TfrmHRMVraboteni.CustomFields();
begin
  custom1 := getCustomField('HR_VRABOTEN','CUSTOM1');
  custom2 := getCustomField('HR_VRABOTEN','CUSTOM2');
  custom3 := getCustomField('HR_VRABOTEN','CUSTOM3');

  if custom1 <> '' then
  begin
    cxGrid1DBTableView1CUSTOM1.Visible := true;
    cxGrid1DBTableView1CUSTOM1.VisibleForCustomization := true;
    cxGrid1DBTableView1CUSTOM1.Caption := custom1;

    lblCustom1.Visible := true;
    lblCustom1.Caption := custom1 + ' :';
    txtCustom1.Visible := true;
  end;
  if custom2 <> '' then
  begin
    cxGrid1DBTableView1CUSTOM2.Visible := true;
    cxGrid1DBTableView1CUSTOM2.VisibleForCustomization := true;
    cxGrid1DBTableView1CUSTOM2.Caption := custom2;

    lblCustom2.Visible := true;
    lblCustom2.Caption := custom2 + ' :';
    txtCustom2.Visible := true;
  end;
  if custom3 <> '' then
  begin
    cxGrid1DBTableView1CUSTOM3.Visible := true;
    cxGrid1DBTableView1CUSTOM3.VisibleForCustomization := true;
    cxGrid1DBTableView1CUSTOM3.Caption := custom3;

    lblCustom3.Visible := true;
    lblCustom3.Caption := custom3 + ' :';
    txtCustom3.Visible := true;
  end;
end;

end.
