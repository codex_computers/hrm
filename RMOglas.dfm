inherited frmRMOglas: TfrmRMOglas
  Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' '#1087#1086' '#1054#1075#1083#1072#1089
  ClientHeight = 752
  ClientWidth = 794
  ExplicitWidth = 810
  ExplicitHeight = 790
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 794
    Height = 202
    ExplicitWidth = 794
    ExplicitHeight = 202
    inherited cxGrid1: TcxGrid
      Width = 790
      Height = 198
      PopupMenu = PopupMenu1
      ExplicitWidth = 790
      ExplicitHeight = 198
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsRMOglas
        object cxGrid1DBTableView1ID_OGLAS: TcxGridDBColumn
          DataBinding.FieldName = 'ID_OGLAS'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ_OGLAS: TcxGridDBColumn
          DataBinding.FieldName = 'BRGODINAOGLAS'
          Width = 85
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
          Width = 83
        end
        object cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RE'
          Width = 161
        end
        object cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RM'
          Width = 188
        end
        object cxGrid1DBTableView1BR_RABOTNI_IMESTA: TcxGridDBColumn
          DataBinding.FieldName = 'BR_RABOTNI_IMESTA'
          Width = 121
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          Caption = #1059#1089#1083#1086#1074#1080
          DataBinding.FieldName = 'OPIS'
          Width = 168
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          Caption = 'E-MAIL '#1072#1076#1088#1077#1089#1072
          DataBinding.FieldName = 'EMAIL'
          Width = 108
        end
        object cxGrid1DBTableView1URL: TcxGridDBColumn
          DataBinding.FieldName = 'URL'
          Width = 136
        end
        object cxGrid1DBTableView1TEL: TcxGridDBColumn
          DataBinding.FieldName = 'TEL'
          Width = 94
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 328
    Width = 794
    Height = 401
    ExplicitTop = 328
    ExplicitWidth = 794
    ExplicitHeight = 401
    inherited Label1: TLabel
      Left = 608
      Top = 81
      Width = 99
      Height = 16
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' :'
      Font.Height = -12
      Visible = False
      ExplicitLeft = 608
      ExplicitTop = 81
      ExplicitWidth = 99
      ExplicitHeight = 16
    end
    inherited Sifra: TcxDBTextEdit
      Left = 665
      Top = 103
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089
      TabStop = False
      DataBinding.DataField = 'ID_OGLAS'
      DataBinding.DataSource = dmSis.dsRMOglas
      Properties.ReadOnly = True
      TabOrder = 1
      Visible = False
      ExplicitLeft = 665
      ExplicitTop = 103
      ExplicitWidth = 87
      Width = 87
    end
    inherited OtkaziButton: TcxButton
      Left = 694
      Top = 338
      TabOrder = 5
      ExplicitLeft = 694
      ExplicitTop = 338
    end
    inherited ZapisiButton: TcxButton
      Left = 613
      Top = 338
      TabOrder = 4
      ExplicitLeft = 613
      ExplicitTop = 338
    end
    object cxGroupBox1: TcxGroupBox
      Left = 19
      Top = 95
      Caption = 
        #1059#1089#1083#1086#1074#1080' '#1082#1086#1080' '#1090#1088#1077#1073#1072' '#1076#1072' '#1075#1080' '#1080#1089#1087#1086#1083#1085#1080' '#1082#1072#1085#1076#1080#1076#1072#1090#1086#1090' '#1079#1072' '#1086#1076#1073#1088#1072#1085#1086#1090#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084 +
        #1077#1089#1090#1086
      PanelStyle.OfficeBackgroundKind = pobkGradient
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.TextStyle = []
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 0
      Height = 169
      Width = 550
      object Label2: TLabel
        Left = 7
        Top = 19
        Width = 99
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 7
        Top = 70
        Width = 99
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1059#1089#1083#1086#1074#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 7
        Top = 40
        Width = 99
        Height = 27
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088'. '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object txtRMRE: TcxDBTextEdit
        Tag = 1
        Left = 112
        Top = 16
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dmSis.dsRMOglas
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 73
      end
      object cbRMRE: TcxDBLookupComboBox
        Tag = 1
        Left = 185
        Top = 16
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dmSis.dsRMOglas
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            Width = 200
            FieldName = 'id'
          end
          item
            Caption = #1056#1072#1073#1086#1090#1085#1072' '#1045#1076#1080#1085#1080#1094#1072
            Width = 600
            FieldName = 'NAZIV_RE'
          end
          item
            Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
            Width = 600
            FieldName = 'NAZIV_RM'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListSource = dsRMRE
        Style.Color = clWhite
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 342
      end
      object txtOpis: TcxDBMemo
        Left = 112
        Top = 70
        Hint = 
          #1059#1089#1083#1086#1074#1080' '#1082#1086#1080' '#1090#1088#1077#1073#1072' '#1076#1072' '#1075#1080' '#1080#1089#1087#1083#1085#1091#1074#1072' '#1088#1072#1073#1086#1090#1085#1080#1082#1086#1090' '#1079#1072' '#1086#1074#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090 +
          #1086' ('#1079#1072#1076#1086#1083#1078#1077#1085#1080#1112#1072'...).  '#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072#1087#1072#1090#1080', '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090'!'
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dmSis.dsRMOglas
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        Style.LookAndFeel.Kind = lfUltraFlat
        Style.LookAndFeel.NativeStyle = True
        Style.Shadow = False
        StyleDisabled.LookAndFeel.Kind = lfUltraFlat
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfUltraFlat
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfUltraFlat
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 3
        OnDblClick = txtOpisDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 86
        Width = 415
      end
      object BrRabotnMesta: TcxDBTextEdit
        Left = 112
        Top = 43
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'BR_RABOTNI_IMESTA'
        DataBinding.DataSource = dmSis.dsRMOglas
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 73
      end
    end
    object gbOlas: TcxGroupBox
      Left = 19
      Top = 22
      Hint = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1079#1072' '#1086#1075#1083#1072#1089#1086#1090
      Caption = #1054#1075#1083#1072#1089' '#1073#1088#1086#1112' '
      Enabled = False
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.TextColor = clNavy
      Style.IsFontAssigned = True
      StyleDisabled.Color = clBtnFace
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.TextColor = clNavy
      StyleDisabled.TextStyle = [fsBold]
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 2
      Height = 67
      Width = 550
      object Label7: TLabel
        Left = 13
        Top = 27
        Width = 95
        Height = 31
        Alignment = taCenter
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1073#1112#1072#1074#1091#1074#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object Label8: TLabel
        Left = 292
        Top = 27
        Width = 87
        Height = 30
        Alignment = taCenter
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1090#1077#1082#1091#1074#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object txtDatumObj: TcxDBDateEdit
        Tag = 1
        Left = 100
        Top = 32
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1073#1112#1072#1074#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1075#1083#1072#1089#1086#1090
        DataBinding.DataField = 'DATUM_OBJAVEN'
        DataBinding.DataSource = dmSis.dsOglas
        Style.LookAndFeel.Kind = lfUltraFlat
        Style.LookAndFeel.NativeStyle = False
        Style.Shadow = False
        Style.ButtonTransparency = ebtHideInactive
        StyleDisabled.LookAndFeel.Kind = lfUltraFlat
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.Kind = lfUltraFlat
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.Kind = lfUltraFlat
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 142
      end
      object txtDatumIstekuva: TcxDBDateEdit
        Left = 383
        Top = 32
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1090#1077#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1075#1083#1072#1089#1086#1090
        DataBinding.DataField = 'DATUM_ISTEKUVA'
        DataBinding.DataSource = dmSis.dsOglas
        Style.ButtonTransparency = ebtHideInactive
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 127
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 19
      Top = 270
      Caption = #1050#1086#1085#1090#1072#1082#1090
      Style.LookAndFeel.Kind = lfFlat
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 3
      Height = 98
      Width = 550
      object Label3: TLabel
        Left = 3
        Top = 21
        Width = 102
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1045'-mail :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 4
        Top = 42
        Width = 102
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'WEB '#1057#1090#1088#1072#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 4
        Top = 63
        Width = 102
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtEmail: TcxDBTextEdit
        Left = 112
        Top = 18
        Hint = 'e-mail '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1089#1086' '#1092#1080#1088#1084#1072#1090#1072' '#1082#1086#1112#1072' '#1075#1086' '#1076#1072#1083#1072' '#1086#1075#1083#1072#1089#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dmSis.dsRMOglas
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 415
      end
      object txtURL: TcxDBTextEdit
        Left = 112
        Top = 39
        Hint = 'Web '#1089#1090#1088#1072#1085#1072
        BeepOnEnter = False
        DataBinding.DataField = 'URL'
        DataBinding.DataSource = dmSis.dsRMOglas
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 415
      end
      object txtTelefon: TcxDBTextEdit
        Left = 112
        Top = 60
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1085#1072' '#1092#1080#1088#1084#1072#1090#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'TEL'
        DataBinding.DataSource = dmSis.dsRMOglas
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 415
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 794
    ExplicitWidth = 794
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 729
    Width = 794
    ExplicitTop = 729
    ExplicitWidth = 794
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072'  '#1087#1086' '#1086#1076#1075#1083#1072#1089
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40288.417347407410000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 584
    Top = 48
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 219
    Top = 275
  end
  object tblRMRE: TpFIBDataSet
    RefreshSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'where(  coalesce(hs.do_datum,'#39'11.11.1111'#39') like :datum and hs.id' +
        '_re_firma like :firma'
      '     ) and (     HRR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv as naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv as naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      
        'where coalesce(hs.do_datum,'#39'11.11.1111'#39') like :datum and hs.id_r' +
        'e_firma like :firma'
      'order by mr.naziv, hrm.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 288
    Top = 275
    oRefreshDeletedRecord = True
    object FIBIntegerField1: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object FIBIntegerField2: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object FIBIntegerField3: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object FIBIntegerField4: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object FIBIntegerField5: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object FIBDateTimeField1: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object FIBDateTimeField2: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object FIBStringField1: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object FIBStringField3: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      EmptyStrToNull = True
    end
    object FIBStringField5: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      EmptyStrToNull = True
    end
    object FIBIntegerField6: TFIBIntegerField
      FieldName = 'KOREN'
    end
  end
end
