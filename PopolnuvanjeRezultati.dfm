object frmVnesNaOcenki: TfrmVnesNaOcenki
  Left = 0
  Top = 0
  Caption = '360 '#1055#1086#1074#1088#1072#1090#1085#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1112#1072' - '#1056#1077#1079#1091#1083#1090#1072#1090#1080
  ClientHeight = 798
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 900
    Height = 649
    Align = alClient
    ParentBackground = False
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 898
      Height = 56
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      ParentBackground = False
      TabOrder = 0
      object Label5: TLabel
        Left = 0
        Top = 14
        Width = 97
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Feedback '#1073#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 0
        Top = 33
        Width = 97
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1088#1072#1073#1086#1090#1077#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxDBLabel1: TcxDBLabel
        Left = 105
        Top = 31
        DataBinding.DataField = 'VRABOTENNAIV'
        DataBinding.DataSource = dmUcinok.dsIspitanici
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Height = 21
        Width = 410
      end
      object cxDBLabel2: TcxDBLabel
        Left = 105
        Top = 12
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dmUcinok.dsFeedBack
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Height = 21
        Width = 410
      end
    end
    object cxPageControl1: TcxPageControl
      Left = 1
      Top = 378
      Width = 898
      Height = 270
      Align = alBottom
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      Properties.ActivePage = cxTabSheet1
      ClientRectBottom = 270
      ClientRectRight = 898
      ClientRectTop = 24
      object cxTabSheet1: TcxTabSheet
        Caption = #1055#1086#1087#1086#1083#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1077#1079#1091#1083#1090#1072#1090#1080
        ImageIndex = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 898
          Height = 246
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            DataController.DataSource = dmUcinok.dsRezultati
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPAPRASANJENAZIV'
              Options.Editing = False
              Width = 220
            end
            object cxGrid1DBTableView1PRASANJENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PRASANJENAZIV'
              Options.Editing = False
              Width = 297
            end
            object cxGrid1DBTableView1OCENUVACNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'OCENUVACNAZIV'
              Options.Editing = False
              Width = 105
            end
            object cxGrid1DBTableView1OCENA: TcxGridDBColumn
              DataBinding.FieldName = 'OCENA'
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object cxPageControl2: TcxPageControl
      Left = 1
      Top = 57
      Width = 898
      Height = 321
      Align = alClient
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      Properties.ActivePage = cxTabSheet3
      ClientRectBottom = 321
      ClientRectRight = 898
      ClientRectTop = 24
      object cxTabSheet3: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object sys: TcxDBPivotGrid
          Left = 0
          Top = 0
          Width = 898
          Height = 297
          Align = alClient
          DataSource = dmUcinok.dsRezultati
          Groups = <>
          OptionsView.ColumnGrandTotalText = #1057#1088#1077#1076#1085#1072' '#1086#1094#1077#1085#1082#1072
          OptionsView.ColumnGrandTotalWidth = 146
          OptionsView.RowGrandTotalText = #1057#1088#1077#1076#1085#1072' '#1086#1094#1077#1085#1082#1072
          OptionsView.RowGrandTotalWidth = 146
          TabOrder = 0
          object cxDBPivotGrid1Prasanje: TcxDBPivotGridField
            Area = faRow
            AreaIndex = 1
            DataBinding.FieldName = 'PRASANJENAZIV'
            Visible = True
            Width = 200
          end
          object cxDBPivotGrid1Ocenuvac: TcxDBPivotGridField
            Area = faColumn
            AreaIndex = 0
            DataBinding.FieldName = 'OCENUVACNAZIV'
            DataVisibility = dvGrandTotalCells
            SummaryType = stAverage
            Visible = True
          end
          object cxDBPivotGrid1Ocena: TcxDBPivotGridField
            Area = faData
            AreaIndex = 0
            DataBinding.FieldName = 'OCENA'
            SummaryType = stAverage
            Visible = True
          end
          object cxDBPivotGrid1Field1: TcxDBPivotGridField
            Area = faRow
            AreaIndex = 0
            DataBinding.FieldName = 'GRUPAPRASANJENAZIV'
            Visible = True
            Width = 200
          end
        end
      end
      object cxTabSheet4: TcxTabSheet
        Caption = #1043#1088#1072#1092#1080#1095#1082#1080' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 898
          Height = 297
          Align = alClient
          TabOrder = 0
          object cxGrid2DBChartView1: TcxGridDBChartView
            DataController.DataSource = dmUcinok.dsRezultati
            DiagramPie.Active = True
            ToolBox.CustomizeButton = True
            ToolBox.DiagramSelector = True
            object cxGrid2DBChartView1DataGroup3: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'GRUPAPRASANJENAZIV'
            end
            object cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'PRASANJENAZIV'
            end
            object cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'OCENUVACNAZIV'
            end
            object cxGrid2DBChartView1Series1: TcxGridDBChartSeries
              DataBinding.FieldName = 'OCENA'
              GroupSummaryKind = skAverage
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBChartView1
          end
        end
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 900
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 775
    Width = 900
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPivotGridSummaryDataSet1: TcxPivotGridSummaryDataSet
    SynchronizeData = True
    Left = 304
    Top = 384
  end
  object dsPivot1: TDataSource
    DataSet = cxPivotGridSummaryDataSet1
    Left = 504
    Top = 184
  end
  object ActionList2: TActionList
    Images = dmRes.cxSmallImages
    Left = 552
    Top = 200
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object Action1: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = Action1Execute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
    end
    object aPecatiTabelaren: TAction
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 30
      OnExecute = aPecatiTabelarenExecute
    end
    object aPecatiGraficki: TAction
      Caption = #1043#1088#1072#1092#1080#1095#1082#1080' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 30
      OnExecute = aPecatiGrafickiExecute
    end
    object aPecatiRezultati: TAction
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1086#1087#1086#1083#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1077#1079#1091#1083#1090#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiRezultatiExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 440
    Top = 168
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1077#1095#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 64
      FloatClientHeight = 156
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 303
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 111
      FloatClientHeight = 126
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 439
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 104
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 133
      FloatClientHeight = 208
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 12
      ShortCut = 117
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Caption = #1041#1088#1080#1096#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 11
      ShortCut = 119
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = Action1
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1053#1086#1074
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 10
      ShortCut = 116
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPecatiTabelaren
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPecatiGraficki
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPecatiRezultati
      Category = 0
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 496
    Top = 280
    object dxComponentPrinter1Link2: TcxPivotGridReportLink
      Component = sys
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link3: TdxGridReportLink
      Component = cxGrid2
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 480
    Top = 400
  end
  object PopupMenu1: TPopupMenu
    Left = 112
    Top = 56
  end
end
