unit DogHonorarnaSorabotka;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon,  ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, cxCalendar,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxCustomHint, cxHint;

type
  TfrmDogHonorarnaSorabotka = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1FIRMANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MB_DIREKTOR: TcxGridDBColumn;
    cxGrid1DBTableView1PLATA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label14: TLabel;
    RABOTNO_MESTO: TcxDBTextEdit;
    RABOTNO_MESTO_NAZIV: TcxDBLookupComboBox;
    Label2: TLabel;
    MB: TcxDBTextEdit;
    PREZIME: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    TATKOVO_IME: TcxDBTextEdit;
    Label5: TLabel;
    IME: TcxDBTextEdit;
    Label6: TLabel;
    Label7: TLabel;
    DATUM_OD: TcxDBDateEdit;
    DATUM_DO: TcxDBDateEdit;
    Label8: TLabel;
    PLATA: TcxDBTextEdit;
    Label9: TLabel;
    MB_DIREKTOR: TcxDBTextEdit;
    dxBarManager1Bar5: TdxBar;
    cxBarEditItem2: TcxBarEditItem;
    Label11: TLabel;
    DatumKreiranje: TcxDBDateEdit;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxBarEditItem2PropertiesChange(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDogHonorarnaSorabotka: TfrmDogHonorarnaSorabotka;

implementation

uses dmKonekcija, dmUnit, dmSistematizacija, dmUnitOtsustvo, Utils;

{$R *.dfm}

procedure TfrmDogHonorarnaSorabotka.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(16);
end;

procedure TfrmDogHonorarnaSorabotka.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblDogHonorarciVID_DOKUMENT.Value:=dogHonorarci;
    dm.tblDogHonorarciID_RE_FIRMA.Value:=dmKon.re;
    dm.tblDogHonorarciMB.Value:=dm.tblLicaVraboteniMB.Value;
    dm.tblDogHonorarciPREZIME.Value:=dm.tblLicaVraboteniPREZIME.Value;
    dm.tblDogHonorarciTATKOVO_IME.Value:=dm.tblLicaVraboteniTATKOVO_IME.Value;
    dm.tblDogHonorarciIME.Value:=dm.tblLicaVraboteniIME.Value;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmDogHonorarnaSorabotka.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        if (DATUM_OD.Text <> '')and (DATUM_DO.Text <> '') and (DATUM_DO.Date < DATUM_OD.Date ) then
          begin
               ShowMessage('������ � �������� ������ !!!');
               DATUM_DO.SetFocus;
          end
        else
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;
            cxGrid1.SetFocus;
          end;
    end;
  end;
end;

procedure TfrmDogHonorarnaSorabotka.cxBarEditItem2PropertiesChange(
  Sender: TObject);
begin
  if cxBarEditItem2.EditValue = 0 then
   begin
      cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
     try
        cxGrid1DBTableView1.DataController.Filter.Root.Clear;
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_OD, foLessEqual, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_DO, foGreater, Now, DateToStr(Now));
        cxGrid1DBTableView1.DataController.Filter.Active:=True;
     finally
        cxGrid1DBTableView1.DataController.Filter.EndUpdate;
     end;
   end
   else
     cxGrid1DBTableView1.DataController.Filter.Clear;

end;

procedure TfrmDogHonorarnaSorabotka.FormCreate(Sender: TObject);
begin
  inherited;
   dmSis.tblRabotnoMesto.ParamByName('firma').Value:=dmkon.re;
   dmsis.tblRabotnoMesto.Open;
end;

procedure TfrmDogHonorarnaSorabotka.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 1 then
     begin
       dm.tbldoghonorarci.Close;
       dm.tblDogHonorarci.parambyName('mb').value:=dm.tblLicaVraboteniMB.Value;
       dm.tbldoghonorarci.parambyname('firma').value:=dmKon.re;
       dm.tbldoghonorarci.Open;
     end
  else
     begin
       dm.tbldoghonorarci.Close;
       dm.tblDogHonorarci.parambyName('mb').value:='%';
       dm.tbldoghonorarci.parambyname('firma').value:= dmKon.re;
       dm.tbldoghonorarci.Open;

       dxBarManager1Bar1.Visible:=false;
       cxBarEditItem2.EditValue:= 0;
       if cxBarEditItem2.EditValue = 0 then
        begin
          cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
           cxGrid1DBTableView1.DataController.Filter.Root.Clear;
           cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_OD, foLessEqual, Now, DateToStr(Now));
           cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1DATUM_DO, foGreater, Now, DateToStr(Now));
           cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
           cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
        end
       else
          cxGrid1DBTableView1.DataController.Filter.Clear;
     end
end;

end.
