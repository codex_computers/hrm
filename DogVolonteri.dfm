inherited frmDogVolonteri: TfrmDogVolonteri
  Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1074#1086#1083#1086#1085#1090#1077#1088#1080
  ClientHeight = 763
  ClientWidth = 1038
  WindowState = wsMaximized
  ExplicitWidth = 1046
  ExplicitHeight = 794
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1038
    Height = 275
    ExplicitWidth = 1038
    ExplicitHeight = 275
    inherited cxGrid1: TcxGrid
      Width = 1034
      Height = 271
      ExplicitWidth = 1034
      ExplicitHeight = 271
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsDogVolonteri
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Editing = True
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1054#1076#1073#1077#1088#1080
          DataBinding.ValueType = 'Boolean'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.MultiLine = True
          Width = 58
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Options.Editing = False
          Width = 62
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1LICNA_KARTA: TcxGridDBColumn
          DataBinding.FieldName = 'LICNA_KARTA'
          Options.Editing = False
          Width = 93
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
          DataBinding.FieldName = 'TATKOVO_IME'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 250
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1NAZIVRABMESTO: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIVRABMESTO'
          Options.Editing = False
          Width = 198
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Options.Editing = False
          Width = 67
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Options.Editing = False
          Width = 68
        end
        object cxGrid1DBTableView1BROJ_CASOVI: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_CASOVI'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'PLATA'
          Options.Editing = False
          Width = 66
        end
        object cxGrid1DBTableView1RAKOVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'RAKOVODITEL'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
          Options.Editing = False
          Width = 63
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
          Options.Editing = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 449
    Width = 1038
    Height = 291
    ExplicitTop = 449
    ExplicitWidth = 1038
    ExplicitHeight = 291
    inherited Label1: TLabel
      Left = 610
      Top = 28
      Visible = False
      ExplicitLeft = 610
      ExplicitTop = 28
    end
    object Label10: TLabel [1]
      Left = 295
      Top = 26
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 8
      Top = 17
      Width = 137
      Height = 36
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 666
      Top = 25
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsDogVolonteri
      TabOrder = 6
      Visible = False
      ExplicitLeft = 666
      ExplicitTop = 25
    end
    inherited OtkaziButton: TcxButton
      Left = 916
      Top = 236
      TabOrder = 7
      OnKeyDown = EnterKakoTab
      ExplicitLeft = 916
      ExplicitTop = 236
    end
    inherited ZapisiButton: TcxButton
      Left = 835
      Top = 236
      TabOrder = 5
      OnKeyDown = EnterKakoTab
      ExplicitLeft = 835
      ExplicitTop = 236
    end
    object Period: TcxGroupBox
      Left = 434
      Top = 173
      Caption = #1055#1077#1088#1080#1086#1076
      TabOrder = 4
      Height = 57
      Width = 380
      object Label6: TLabel
        Left = 56
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 214
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 103
        Top = 22
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsDogVolonteri
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 100
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 261
        Top = 22
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsDogVolonteri
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 100
      end
    end
    object Broj: TcxDBTextEdit
      Left = 351
      Top = 23
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsDogVolonteri
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
    object PodatociZaRabotnik: TcxGroupBox
      Left = 48
      Top = 59
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1073#1086#1090#1085#1080#1082
      TabOrder = 2
      Height = 172
      Width = 380
      object Label5: TLabel
        Left = 13
        Top = 107
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 13
        Top = 80
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 13
        Top = 53
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 13
        Top = 26
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 13
        Top = 127
        Width = 84
        Height = 27
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1095#1085#1072' '#1082#1072#1088#1090#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 104
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsDogVolonteri
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 258
      end
      object TATKOVO_IME: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 77
        BeepOnEnter = False
        DataBinding.DataField = 'TATKOVO_IME'
        DataBinding.DataSource = dm.dsDogVolonteri
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 258
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 50
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsDogVolonteri
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 258
      end
      object MB: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'MB'
        DataBinding.DataSource = dm.dsDogVolonteri
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 258
      end
      object BrLicnaKarta: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 131
        BeepOnEnter = False
        DataBinding.DataField = 'LICNA_KARTA'
        DataBinding.DataSource = dm.dsDogVolonteri
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 258
      end
    end
    object PodatociZaVrabotuvanje: TcxGroupBox
      Left = 434
      Top = 59
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      TabOrder = 3
      DesignSize = (
        557
        110)
      Height = 110
      Width = 557
      object Label14: TLabel
        Left = 22
        Top = 26
        Width = 94
        Height = 17
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label9: TLabel
        Left = 22
        Top = 53
        Width = 94
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label8: TLabel
        Left = 270
        Top = 80
        Width = 46
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1083#1072#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label16: TLabel
        Left = 22
        Top = 80
        Width = 94
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object RABOTNO_MESTO: TcxDBTextEdit
        Tag = 1
        Left = 122
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dm.dsDogVolonteri
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 48
      end
      object RABOTNO_MESTO_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 170
        Top = 23
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dm.dsDogVolonteri
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 50
            FieldName = 'ID'
          end
          item
            Width = 500
            FieldName = 'NAZIV_RM'
          end
          item
            Width = 50
            FieldName = 'ID_RE'
          end
          item
            Width = 500
            FieldName = 'NAZIV_RE'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dsRMRE
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 367
      end
      object MB_DIREKTOR: TcxDBTextEdit
        Left = 122
        Top = 50
        BeepOnEnter = False
        DataBinding.DataField = 'RAKOVODITEL'
        DataBinding.DataSource = dm.dsDogVolonteri
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 407
      end
      object PLATA: TcxDBTextEdit
        Left = 322
        Top = 77
        BeepOnEnter = False
        DataBinding.DataField = 'PLATA'
        DataBinding.DataSource = dm.dsDogVolonteri
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 124
      end
      object BROJ_CASOVI: TcxDBTextEdit
        Left = 122
        Top = 77
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_CASOVI'
        DataBinding.DataSource = dm.dsDogVolonteri
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 124
      end
    end
    object DatumKreiranje: TcxDBDateEdit
      Left = 151
      Top = 23
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsDogVolonteri
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1038
    ExplicitWidth = 1038
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 740
    Width = 1038
    ExplicitTop = 740
    ExplicitWidth = 1038
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 401
    Width = 1038
    Height = 48
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 8
    Visible = False
    object cxButton1: TcxButton
      Left = 151
      Top = 13
      Width = 127
      Height = 25
      Action = aKopirajDogovori
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 0
      OnKeyDown = EnterKakoTab
    end
    object CheckBox1: TCheckBox
      Left = 24
      Top = 16
      Width = 121
      Height = 17
      Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077
      TabOrder = 1
      OnClick = CheckBox1Click
    end
  end
  object Panel2: TPanel [5]
    Left = 351
    Top = 246
    Width = 378
    Height = 149
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 9
    Visible = False
    DesignSize = (
      378
      149)
    object Label17: TLabel
      Left = 8
      Top = 83
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxGroupBox1: TcxGroupBox
      Left = 15
      Top = 10
      Caption = #1055#1077#1088#1080#1086#1076
      TabOrder = 0
      Height = 56
      Width = 348
      object Label13: TLabel
        Left = 0
        Top = 27
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 174
        Top = 27
        Width = 25
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DO_DATUM: TcxDateEdit
        Tag = 1
        Left = 205
        Top = 24
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = DO_DATUMExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object OD_DATUM: TcxDateEdit
        Tag = 1
        Left = 47
        Top = 24
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = OD_DATUMExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object cxButton2: TcxButton
      Left = 271
      Top = 113
      Width = 75
      Height = 25
      Action = aOtkaziDogovori
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 1
      OnKeyDown = EnterKakoTab
    end
    object cxButton3: TcxButton
      Left = 184
      Top = 113
      Width = 81
      Height = 25
      Action = aZapisiDogovori
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      OnKeyDown = EnterKakoTab
    end
    object Broj_Del: TcxTextEdit
      Left = 64
      Top = 80
      TabOrder = 3
      Text = 'Broj_Del'
      Width = 121
    end
  end
  inherited dxBarManager1: TdxBarManager
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 263
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 629
      FloatClientWidth = 120
      FloatClientHeight = 152
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 874
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem6'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 446
      DockedTop = 0
      FloatLeft = 828
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = aFilterAktivni
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem2: TcxBarEditItem
      Action = aFilterAktivni
      Category = 0
      PropertiesClassName = 'TcxCheckGroupProperties'
      Properties.Items = <>
    end
    object cxBarEditItem3: TcxBarEditItem
      Action = afilterSite
      Category = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object cxBarEditItem4: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckComboBoxProperties'
      Properties.Items = <>
    end
    object cxBarEditItem5: TcxBarEditItem
      Action = aFilterAktivni
      Category = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ValueChecked = 'False'
    end
    object cxBarEditItem6: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 1
        end
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 0
        end>
      Properties.OnChange = cxBarEditItem6PropertiesChange
      InternalEditValue = 1
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aKreirajPregledajDokument
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aFilterAktivni: TAction
      Caption = #1040#1082#1090#1080#1074#1085#1080' '#1076#1086#1075#1086#1074#1086#1088#1080
    end
    object afilterSite: TAction
      Caption = #1057#1080#1090#1077' '#1076#1086#1075#1086#1074#1086#1088#1080
    end
    object aKopirajDogovori: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1076#1086#1075#1086#1074#1086#1088#1080
      ImageIndex = 8
      OnExecute = aKopirajDogovoriExecute
    end
    object aKreirajPregledajDokument: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 10
      OnExecute = aKreirajPregledajDokumentExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
    object aZapisiDogovori: TAction
      Caption = #1055#1088#1086#1076#1086#1083#1078#1080
      ImageIndex = 7
      OnExecute = aZapisiDogovoriExecute
    end
    object aOtkaziDogovori: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziDogovoriExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageHeader.LeftTitle.Strings = (
        '')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40668.495844351860000000
      ShrinkToPageWidth = True
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 656
    Top = 240
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 603
    Top = 179
  end
  object tblRMRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hrr.rakovodenje,'
      '    hrr.denovi_odmor,'
      '    hrr.den_uslovi_rabota,'
      '    hrm.stepen_slozenost'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where hs.do_datum is null and hs.id_re_firma = :firma'
      'order by 4, 8')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 560
    Top = 219
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRMRERAKOVODENJE: TFIBBCDField
      FieldName = 'RAKOVODENJE'
      Size = 2
    end
    object tblRMREDENOVI_ODMOR: TFIBIntegerField
      FieldName = 'DENOVI_ODMOR'
    end
    object tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblRMRESTEPEN_SLOZENOST: TFIBBCDField
      FieldName = 'STEPEN_SLOZENOST'
    end
  end
  object qMaxBroj: TpFIBQuery
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select cast((coalesce(max(cast(coalesce(substring(hrg.broj from ' +
        'strlen(hrg.arhivski_broj)+2 for 3),0) as integer)),0)) as intege' +
        'r) maks'
      'from hr_dogovor_volonteri hrg'
      'where cast(hrg.arhivski_broj as varchar(20)) = :arhivski_broj')
    Left = 224
    Top = 264
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 872
    Top = 184
  end
end
