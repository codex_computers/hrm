object FormProba: TFormProba
  Left = 0
  Top = 0
  Caption = 'FormProba'
  ClientHeight = 467
  ClientWidth = 674
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxScheduler1: TcxScheduler
    Left = 0
    Top = 0
    Width = 673
    Height = 266
    ViewYear.Active = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    TabOrder = 0
    Splitters = {
      110200007E000000A0020000830000000C020000010000001102000009010000}
    StoredClientBounds = {0100000001000000A002000009010000}
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 272
    Width = 673
    Height = 200
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dmOtsustvo.dsOtsustva
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
      end
      object cxGrid1DBTableView1VRABOTENIME: TcxGridDBColumn
        DataBinding.FieldName = 'VRABOTENIME'
      end
      object cxGrid1DBTableView1OD_VREME: TcxGridDBColumn
        DataBinding.FieldName = 'OD_VREME'
      end
      object cxGrid1DBTableView1DO_VREME: TcxGridDBColumn
        DataBinding.FieldName = 'DO_VREME'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxSchedulerDBStorage1: TcxSchedulerDBStorage
    Resources.Items = <>
    CustomFields = <>
    DataSource = dmOtsustvo.dsOtsustva
    FieldNames.ActualFinish = 'OD_VREME'
    FieldNames.ActualStart = 'DO_VREME'
    FieldNames.Finish = 'DO_VREME'
    FieldNames.ID = 'ID'
    FieldNames.Start = 'OD_VREME'
    Left = 584
    Top = 184
  end
end
