inherited frmIntOblik: TfrmIntOblik
  Caption = #1054#1073#1083#1080#1082
  ClientHeight = 595
  ClientWidth = 688
  ExplicitWidth = 696
  ExplicitHeight = 629
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 688
    Height = 258
    ExplicitWidth = 688
    ExplicitHeight = 268
    inherited cxGrid1: TcxGrid
      Width = 684
      Height = 264
      ExplicitWidth = 684
      ExplicitHeight = 264
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dmOtsustvo.dsIntOblik
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 300
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 417
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 384
    Width = 688
    Height = 188
    ExplicitTop = 384
    ExplicitWidth = 688
    ExplicitHeight = 188
    inherited Label1: TLabel
      Left = 477
      Top = 9
      Visible = False
      ExplicitLeft = 477
      ExplicitTop = 9
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 25
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = -39
      Top = 52
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 533
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmOtsustvo.dsIntOblik
      TabOrder = 1
      Visible = False
      ExplicitLeft = 533
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 573
      Top = 124
      TabOrder = 4
      ExplicitLeft = 573
      ExplicitTop = 124
    end
    inherited ZapisiButton: TcxButton
      Left = 492
      Top = 124
      TabOrder = 3
      ExplicitLeft = 492
      ExplicitTop = 124
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 22
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmOtsustvo.dsIntOblik
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 380
    end
    object txtOpis: TcxDBMemo
      Left = 69
      Top = 49
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmOtsustvo.dsIntOblik
      Properties.ScrollBars = ssVertical
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 99
      Width = 380
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 688
    ExplicitWidth = 688
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 572
    Width = 688
    ExplicitTop = 572
    ExplicitWidth = 688
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40294.659965219910000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
