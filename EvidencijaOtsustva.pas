unit EvidencijaOtsustva;

(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.8                    }
{                                       }
{   22.03.2010                          }
{                                       }
(***************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter, cxContainer,
  cxEdit, Menus, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxScheduler, cxSchedulerStorage, cxSchedulerCustomControls,
  cxSchedulerCustomResourceView, cxSchedulerDayView, cxSchedulerDateNavigator,
  cxSchedulerHolidays, cxSchedulerTimeGridView, cxSchedulerUtils,
  cxSchedulerWeekView, cxSchedulerYearView, cxSchedulerGanttView,
   dxSkinsdxBarPainter, cxDBLookupComboBox, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, cxSchedulerDBStorage, dxPSCore, dxPScxCommon,
   cxGridCustomPopupMenu, cxGridPopupMenu, cxGrid, ActnList,
  dxBar, cxBarEditItem, cxGroupBox, cxRadioGroup, StdCtrls, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxMemo, cxDBEdit, cxButtons, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxMaskEdit, cxCalendar, cxTextEdit, cxPC,
  dxStatusBar, dxRibbonStatusBar, dxRibbon, ComCtrls, cxSpinEdit, cxTimeEdit,
  IdBaseComponent,  IdSchedulerOfThread, IdSchedulerOfThreadDefault, Math,
  cxLabel, cxCheckBox, cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, FIBQuery, pFIBQuery, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxSkinscxSchedulerPainter, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip, dxCustomHint,
  dxPSdxDBOCLnk, cxColorComboBox, FIBDataSet, pFIBDataSet, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator,
  cxSchedulerTreeListBrowser, System.Actions, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, dxBarBuiltInMenu, cxSchedulerAgendaView,
  cxSchedulerRecurrence, cxSchedulerRibbonStyleEventEditor ;

type
  TfrmEvidencijaOtsustva = class(TForm)
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxPodesuvanje: TdxRibbonTab;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxPageControl1: TcxPageControl;
    tcPlanOtsustvo: TcxTabSheet;
    Panel2: TPanel;
    Label15: TLabel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    MB: TcxDBTextEdit;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    buttonOtkazi: TcxButton;
    buttonZapisi: TcxButton;
    VRABOTENIME: TcxDBLookupComboBox;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    txtOpis: TcxDBMemo;
    PRICINANAZIV: TcxDBLookupComboBox;
    Panel4: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    tcPregled: TcxTabSheet;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton6: TRadioButton;
    Panel3: TPanel;
    cxSchedulerOtsustva: TcxScheduler;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    ActionList1: TActionList;
    aPlanZaOtsustvo: TAction;
    aDodadiPlanZaOtsustvo: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aIzlez: TAction;
    aAzurirajPlan: TAction;
    aBrisi: TAction;
    aOsvezi: TAction;
    aSnimiGoIzgledot: TAction;
    aZacuvajVoExcel: TAction;
    aPecatenje: TAction;
    aPecatiTabela: TAction;
    aStatusGrupa: TAction;
    aStatus: TAction;
    aPrebaraj: TAction;
    aIscisti: TAction;
    aPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aSnimiPecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxSchedulerDBStorage1: TcxSchedulerDBStorage;
    aPomos: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton6: TdxBarButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton32: TdxBarLargeButton;
    dxBarLargeButton33: TdxBarLargeButton;
    cxBarEditRabEdinica: TcxBarEditItem;
    cas: TcxLabel;
    cxLabel1: TcxLabel;
    den: TcxLabel;
    cxLabel3: TcxLabel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxHintStyleController1: TcxHintStyleController;
    OD_VREME: TcxDBDateEdit;
    Label7: TLabel;
    DO_VREME_L: TcxDBDateEdit;
    aFormConfig: TAction;
    qTipOtsustvo: TpFIBQuery;
    OstanatiDenovi: TcxLabel;
    qSumTipOtsustvo: TpFIBQuery;
    qSumNeplateni: TpFIBQuery;
    qKolektivenDog: TpFIBQuery;
    Panel5: TPanel;
    cxGroupBox3: TcxGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    vrabotenprebaraj: TcxLookupComboBox;
    TipOtsustvoPrebaraj: TcxLookupComboBox;
    ButtonIscisti: TcxButton;
    dxComponentPrinter1Link2: TcxSchedulerReportLink;
    aBrzoOtsustvo: TAction;
    N2: TMenuItem;
    cbGodina: TcxBarEditItem;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_ZAPIS: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1OD_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1DO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENPREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENTATKOVOIME: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENIME: TcxGridDBColumn;
    cxGrid1DBTableView1PlatenoDaNe: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1DENOVI: TcxGridDBColumn;
    cxGrid1DBTableView1DO_VREME_L: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxGrid1DBTableView1TIPOTSUSTVONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KolektivenDogDaNe: TcxGridDBColumn;
    aZapisiVerzija2: TAction;
    aZapisiFinal: TAction;
    aZapisiKraj: TAction;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    dsBojaOtsustva: TDataSource;
    tblBojOtsustva: TpFIBDataSet;
    tblBojOtsustvaNAZIV: TFIBStringField;
    tblBojOtsustvaDENOVI: TFIBSmallIntField;
    tblBojOtsustvaPlatenoNaziv: TFIBStringField;
    tblBojOtsustvaDogovorNaziv: TFIBStringField;
    tblBojOtsustvaBOJA: TFIBIntegerField;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1DENOVI: TcxGridDBColumn;
    cxGrid2DBTableView1PlatenoNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1DogovorNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1BOJA: TcxGridDBColumn;
    cxGrid1DBTableView1BOJA: TcxGridDBColumn;
    aPecatiKaldendar: TAction;
    dxBarButton7: TdxBarButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton35: TdxBarLargeButton;
    dxBarLargeButton36: TdxBarLargeButton;
    dxBarLargeButton37: TdxBarLargeButton;
    aPodesuvanjePecatenje2: TAction;
    aPageSetup2: TAction;
    aSnimiPecatenje2: TAction;
    aBrisiPodesuvanjePecatenje2: TAction;
    dxBarLargeButton39: TdxBarLargeButton;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    brDenovi: TcxTextEdit;
    DO_VREME: TcxDBDateEdit;
    procedure FormCreate(Sender: TObject);
    procedure aPlanZaOtsustvoExecute(Sender: TObject);
    procedure aDodadiPlanZaOtsustvoExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aAzurirajPlanExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aOsveziExecute(Sender: TObject);
    procedure aSnimiGoIzgledotExecute(Sender: TObject);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aIscistiExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton4Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure vrabotenprebarajPropertiesEditValueChanged(Sender: TObject);
    procedure TipOtsustvoPrebarajPropertiesEditValueChanged(Sender: TObject);
    procedure cxBarEditRabEdinicaChange(Sender: TObject);
    procedure OD_VREMEExit(Sender: TObject);
    procedure DO_VREMEExit(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure MBExit(Sender: TObject);
    procedure VRABOTENIMEExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aFormConfigExecute(Sender: TObject);
    procedure PRICINANAZIVExit(Sender: TObject);
    procedure cxSchedulerOtsustvaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxSchedulerOtsustvaDblClick(Sender: TObject);
    procedure aBrzoOtsustvoExecute(Sender: TObject);
    procedure aPomosExecute(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure aZapisiVerzija2Execute(Sender: TObject);
    procedure aZapisiFinalExecute(Sender: TObject);
    procedure aZapisiKrajExecute(Sender: TObject);
    procedure aPecatiKaldendarExecute(Sender: TObject);
    procedure aPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure aPageSetup2Execute(Sender: TObject);
    procedure aSnimiPecatenje2Execute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenje2Execute(Sender: TObject);
    procedure brDenoviExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEvidencijaOtsustva: TfrmEvidencijaOtsustva;
  StateActive:TDataSetState;
implementation

uses DaNe, dmKonekcija, dmMaticni, dmResources, dmUnit, dmUnitOtsustvo, Utils,
  dmSystem, TipOtsustvo, FormConfig;

{$R *.dfm}

procedure TfrmEvidencijaOtsustva.aAzurirajPlanExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          if dmOtsustvo.tblOtsustvaflag.value = 0 then
             begin
                cxPageControl1.ActivePage:=tcPlanOtsustvo;
                Panel2.Enabled:=True;
                Panel4.Enabled:=False;
                tcPregled.Enabled:=False;
                dmOtsustvo.tblOtsustva.Edit;
                StateActive:=dsEdit;
                MB.SetFocus;
                brDenovi.Visible:=true;
                brDenovi.Text:='';
                if (PRICINANAZIV.Text <> '') then
                  begin
                    qTipOtsustvo.Close;
                    qTipOtsustvo.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaPRICINA.Value;
                    qTipOtsustvo.ExecQuery;
                  end;
             end
          else ShowMessage('�� � ��������� ��������� �� ��� �����. (������ ��� ����� � ����������� ����� �� �����.)');
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmEvidencijaOtsustva.aBrisiExecute(Sender: TObject);
begin

     if StateActive in [dsBrowse] then
        begin
          if dmOtsustvo.tblOtsustvaflag.value = 0 then
             begin
                frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
                if (frmDaNe.ShowModal <> mrYes) then
                   Abort
                else
                   begin
                      cxPageControl1.ActivePage:=tcPlanOtsustvo;
                      dmOtsustvo.qDeleteResenieOtsustva.close;
                      dmOtsustvo.qDeleteResenieOtsustva.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaID.Value;
                      dmOtsustvo.qDeleteResenieOtsustva.ExecQuery;
                      cxGrid1DBTableView1.DataController.DataSet.Delete();
                      dmOtsustvo.tblOtsustva2.FullRefresh;
                   end;
             end
          else ShowMessage('�� � ��������� ��������� �� ��� �����. (������ ��� ����� � ����������� ����� �� �����.)');
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmEvidencijaOtsustva.aBrisiPodesuvanjePecatenje2Execute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxSchedulerOtsustva.Name, dxComponentPrinter1Link2);
end;

procedure TfrmEvidencijaOtsustva.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaOtsustva.aBrzoOtsustvoExecute(Sender: TObject);
var poraka, flag, ostanati_1, ostanati_2,ostanati_po_tip_1, ostanati_po_tip_2, denovi, denovi_1, denovi_2, maxNeplateni, maxPlateni, maxTipPlateni:integer;
begin
     if vrabotenprebaraj.Text <> '' then
     if TipOtsustvoPrebaraj.Text <> '' then
        begin
           dmOtsustvo.tblOtsustva.Insert;
           qTipOtsustvo.Close;
           qTipOtsustvo.ParamByName('id').Value:=TipOtsustvoPrebaraj.EditValue;
           qTipOtsustvo.ExecQuery;
           qKolektivenDog.close;
           qKolektivenDog.ParamByName('param').Value:=now;
           qKolektivenDog.ExecQuery;
           if (qTipOtsustvo.FldByName['naziv'].Value = '������� �����') then
              begin
                 flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPreraspredelbaOtsustva,'OTSUSTVO_ID_IN', 'MB_IN', 'DATUM_OD', 'DATUM_DO',Null,dmOtsustvo.tblOtsustvaID.Value,vrabotenprebaraj.EditValue, cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish - 1, Null, 'FLAG');
                 if (flag = 0 ) then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else if (flag = 1)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else if (flag= 2)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else
                   begin
                      dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                      dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                      dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                      dmOtsustvo.tblOtsustvaPLATENO.Value:=1;
                      dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                      dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                      dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                      dmOtsustvo.tblOtsustva.Post;
                      dmOtsustvo.tblOtsustva.FullRefresh;
                      dmOtsustvo.tblOtsustva2.FullRefresh;
                      StateActive:=dsBrowse;
                   end;
              end
          else
           begin
             if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1)  then
                begin
                   maxNeplateni:=qKolektivenDog.FldByName['max_neplateni_denovi'].Value;
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'PORAKA_OUT');
                   if poraka = 1 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '1. �������� ������ ������ �� ����������� �� ��������� �������� (�� ���� ������), ������ ���������� �������. ������������ ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                            begin
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else
                             begin
                                dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                dmOtsustvo.tblOtsustva.Post;
                             end;
                      end
                   else if poraka = 2 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '2. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ���� ������), ������ ���������� �������. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             begin
                                dmOtsustvo.tblOtsustva.Cancel;
                                StateActive:=dsBrowse;
                             end
                         else
                             begin
                                dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                dmOtsustvo.tblOtsustva.Post;
                             end;
                      end
                   else if poraka = 3 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '3. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                begin
                                   dmOtsustvo.tblOtsustva.Cancel;
                                   StateActive:=dsBrowse;
                                end
                             else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 4 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '4. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                            begin
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else
                            begin
                               dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                               dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                               dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                               dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                               dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                               dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                               dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                               dmOtsustvo.tblOtsustva.Post;
                            end;
                      end
                   else if poraka = 5 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '5. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                begin
                                   dmOtsustvo.tblOtsustva.Cancel;
                                   StateActive:=dsBrowse;
                                 end
                            else
                                begin
                                   dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                   dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                   dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                   dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                   dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                   dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                   dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                   dmOtsustvo.tblOtsustva.Post;
                                end;
                      end
                   else if poraka = 6 then
                      begin
                         ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '6. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                begin
                                   dmOtsustvo.tblOtsustva.Cancel;
                                   StateActive:=dsBrowse;
                                end
                            else
                                begin
                                  dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                  dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                  dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                  dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                  dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                  dmOtsustvo.tblOtsustva.Post;
                                end;
                      end
                   else
                      begin
                        dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                        dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                        dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                        dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                        dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                        dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                        dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                        dmOtsustvo.tblOtsustva.Post;
                      end;
                end
             else if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1)  then
                begin
                   maxPlateni:=qKolektivenDog.FldByName['max_tipotsustvo_den'].Value;
                   maxTipPlateni:= qTipOtsustvo.FldByName['denovi'].Value;
                   ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_1_OUT');
                   ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_2_OUT');
                   ostanati_po_tip_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue,TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_1');
                   ostanati_po_tip_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_2');
                   denovi:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'DENOVI_OUT');
                   denovi_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'DENOVI1_OUT');
                   denovi_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'DENOVI2_OUT');
                   flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'FLAG_V');
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'PORAKA_OUT');
                   if flag = 1 then
                      begin
                         if ostanati_po_tip_1 >= denovi then
                            if ostanati_1 >= denovi then
                               begin
                                  dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                  dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaPLATENO.Value:=1;
                                  dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                  dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                  dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                  dmOtsustvo.tblOtsustva.Post;
                               end
                            else
                               begin
                                  frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '9. �������� ������ ������ �� ���������� ������ �� ������� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_1)+' ������� � '+intToStr(denovi - ostanati_1)+' ��������� ��������?', 1);
                                  if (frmDaNe.ShowModal <> mrYes) then
                                     begin
                                        dmOtsustvo.tblOtsustva.Cancel;
                                        StateActive:=dsBrowse;
                                     end
                                  else
                                      begin
                                         dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                            cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish,ostanati_1,denovi-ostanati_1,vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, '', Null, Null, Null, Null, Null,'BR');
                                         dmOtsustvo.tblOtsustva.FullRefresh;
                                         dmOtsustvo.tblOtsustva2.FullRefresh;
                                      end;
                               end
                         else
                            begin
                               if ostanati_1 >= ostanati_po_tip_1 then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_po_tip_1)+' ������� � '+intToStr(denovi - ostanati_po_tip_1)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                         begin
                                            dmOtsustvo.tblOtsustva.Cancel;
                                            StateActive:=dsBrowse;
                                         end
                                     else
                                         begin
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                               cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish,ostanati_po_tip_1,denovi-ostanati_po_tip_1,vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, '', Null, Null, Null, Null, Null,'BR');
                                            dmOtsustvo.tblOtsustva.FullRefresh;
                                            dmOtsustvo.tblOtsustva2.FullRefresh;
                                         end;
                                  end
                               else  if (ostanati_1 < ostanati_po_tip_1) then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_1)+' ������� � '+intToStr(denovi - ostanati_1)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                        begin
                                           dmOtsustvo.tblOtsustva.Cancel;
                                           StateActive:=dsBrowse;
                                        end
                                     else
                                         begin
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                               cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish,ostanati_1,denovi-ostanati_1,vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, '', Null, Null, Null, Null, Null,'BR');
                                            dmOtsustvo.tblOtsustva.FullRefresh;
                                            dmOtsustvo.tblOtsustva2.FullRefresh;
                                         end;
                                  end;
                            end;
                      end
                   else if flag = 2 then
                      begin
                         if poraka = 10 then
                            begin
                               ShowMessage('10. �������� ������ ������ �� ����������� �� ������� �������� (�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 11 then
                            begin
                               ShowMessage('11. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 12 then
                            begin
                               ShowMessage('12. �������� ������ ������ �� ����������� �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 13 then
                            begin
                               ShowMessage('13. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������� ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 14 then
                            begin
                               ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_1_OUT');
                               ShowMessage('14. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 15 then
                            begin
                               ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_2_OUT');
                               ShowMessage('15. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 17 then
                            begin
                               ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_1');
                               ShowMessage('17. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 18 then
                            begin
                               ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_2');
                               ShowMessage('18. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                      end;
                end
             else
                 begin
                    dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                    dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                    dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                    dmOtsustvo.tblOtsustvaPLATENO.Value:=qTipOtsustvo.FldByName['plateno'].Value;
                    dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                    dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                    dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                    dmOtsustvo.tblOtsustva.Post;
                 end;
           end;
           dmOtsustvo.tblOtsustva2.FullRefresh;
        end
     else ShowMessage('�������� ��� �� �������� !!!')
    else ShowMessage('�������� ���� !!!');

end;

procedure TfrmEvidencijaOtsustva.aDodadiPlanZaOtsustvoExecute(Sender: TObject);
begin
     if StateActive in [dsBrowse] then
        begin
          cxPageControl1.ActivePage:=tcPlanOtsustvo;
          Panel2.Enabled:=True;
          Panel4.Enabled:=False;
          tcPregled.Enabled:=False;
          dmOtsustvo.tblOtsustva.Insert;
          StateActive:=dsInsert;
          //dmOtsustvo.tblOtsustvaPlateno.Value:=1;
          cas.Caption:=' ';
          den.Caption:=' ';

          if TipOtsustvoPrebaraj.Text <> '' then
             dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
          if vrabotenprebaraj.Text <> '' then
             begin
               dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
               PRICINANAZIV.SetFocus;
             end
          else
               MB.SetFocus;
          if (vrabotenprebaraj.Text <> '') and (TipOtsustvoPrebaraj.Text <> '') then
             begin
               dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
               PRICINANAZIV.EditValue;
             end;
          brDenovi.Visible:=true;
          brDenovi.Text:='';
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmEvidencijaOtsustva.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmEvidencijaOtsustva.aIscistiExecute(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva.FullRefresh;
     dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva2.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva2.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.aIzlezExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
    pom_od, pom_do, h, m :string;
begin
     if StateActive in [dsEdit,dsInsert] then
        begin
          dmOtsustvo.tblOtsustva.Cancel;
          StateActive:=dsBrowse;
          RestoreControls(Panel2);
          if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
             begin
               if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM','MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
          OstanatiDenovi.Visible:=False;
          brDenovi.Visible:=false;
          Panel4.Enabled:=true;
          Panel2.Enabled:=false;
          tcPregled.Enabled:=True;
          cxGrid1.SetFocus;
        end
     else Close;
end;

procedure TfrmEvidencijaOtsustva.aOsveziExecute(Sender: TObject);
begin
     dmOtsustvo.tblOtsustva.Refresh;
end;

procedure TfrmEvidencijaOtsustva.aOtkaziExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     dmOtsustvo.tblOtsustva.Cancel;
     StateActive:=dsBrowse;
     RestoreControls(Panel2);
     Panel4.Enabled:=true;
     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
             begin
               if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;

     OstanatiDenovi.Visible:=False;
     brDenovi.Visible:=false;
     Panel2.Enabled:=false;
     tcPregled.Enabled:=True;
     cxGrid1.SetFocus;

end;

procedure TfrmEvidencijaOtsustva.aPageSetup2Execute(Sender: TObject);
begin
     dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmEvidencijaOtsustva.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmEvidencijaOtsustva.aPecatiKaldendarExecute(Sender: TObject);
var rabEdinica:string;
begin
     dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
     dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
     dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

     dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
     dmOtsustvo.tblPodsektori.Locate('ID',cxBarEditRabEdinica.EditValue, []);
     rabEdinica:= dmOtsustvo.tblPodsektoriNAZIV.Value;
     dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� �������: '+rabEdinica);
     dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: '+IntToStr(cbGodina.EditValue));
     if vrabotenprebaraj.Text <> '' then
        dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('����: '+vrabotenprebaraj.text);
     if TipOtsustvoPrebaraj.Text <> '' then
        dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('��� �� ��������: '+TipOtsustvoPrebaraj.text);
     dxComponentPrinter1Link2.ReportTitle.Text := '����������� ������ �� ��������';
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);

end;

procedure TfrmEvidencijaOtsustva.aPecatiTabelaExecute(Sender: TObject);
var rabEdinica:string;
begin
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
     dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
     dmOtsustvo.tblPodsektori.Locate('ID',cxBarEditRabEdinica.EditValue, []);
     rabEdinica:= dmOtsustvo.tblPodsektoriNAZIV.Value;
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� �������: '+rabEdinica);
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������: '+IntToStr(cbGodina.EditValue));
     if vrabotenprebaraj.Text <> '' then
        dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����: '+vrabotenprebaraj.text);
     if TipOtsustvoPrebaraj.Text <> '' then
        dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('��� �� ��������: '+TipOtsustvoPrebaraj.text);
     dxComponentPrinter1Link1.ReportTitle.Text := '��������� ������ �� ��������';
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaOtsustva.aPlanZaOtsustvoExecute(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     cxPageControl1.ActivePage:=tcPlanOtsustvo;
     panel2.Enabled:=True;
     Panel4.Enabled:=False;
     tcPregled.Enabled:=False;
     dmOtsustvo.tblOtsustva.Insert;
     StateActive:=dsInsert;
     dmOtsustvo.tblOtsustvaOD_VREME.Value:= cxSchedulerOtsustva.SelStart;
     dmOtsustvo.tblOtsustvaDO_VREME.Value:= cxSchedulerOtsustva.SelFinish;
     dmOtsustvo.tblOtsustvaDO_VREME_L.Value:= cxSchedulerOtsustva.SelFinish - 1;
     dmOtsustvo.tblOtsustvaPlateno.Value:=1;

     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
         begin
           if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
              begin
                pom_od:=OD_VREME.Text;
                pom_do:=DO_VREME.Text;

                cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                if cas_od = cas_do then
                   begin
                     casovi:=0;
                     minuti:= minuta_do - minuta_od;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                     Den.Caption:='0';
                   end
                else
                   begin
                     minuti:= 60 - minuta_od + minuta_do;
                     if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                     Den.Caption:='0';
                   end;
              end
           else
              begin
                denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', Null,dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value, Null, 'DENOVI');
                casovi:= denovi * 8;
                den.Caption:=IntToStr(denovi);
                Cas.Caption:=IntToStr(casovi);
              end;
         end
     else
         begin
           den.Caption:=' ';
           Cas.Caption:=' ';
         end;
     MB.SetFocus;
end;

procedure TfrmEvidencijaOtsustva.aPodesuvanjePecatenje2Execute(Sender: TObject);
begin
     dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmEvidencijaOtsustva.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencijaOtsustva.aPomosExecute(Sender: TObject);
begin
     Application.HelpContext(113);
end;

procedure TfrmEvidencijaOtsustva.aSnimiGoIzgledotExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaOtsustva.aSnimiPecatenje2Execute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxSchedulerOtsustva.Name,dxComponentPrinter1Link2);
end;

procedure TfrmEvidencijaOtsustva.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaOtsustva.aZacuvajVoExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmEvidencijaOtsustva.aZapisiFinalExecute(Sender: TObject);
var poraka, ostanati_1, ostanati_2, denovi, denovi_1, denovi_2, maxNeplateni, maxPlateni, maxTipPlateni:integer;
begin
    if Validacija(Panel2) = false then
        begin
             dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
             if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) then
                begin
                   maxNeplateni:=qKolektivenDog.FldByName['max_neplateni_denovi'].Value;
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'PORAKA_OUT');
                   if poraka = 1 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '1. �������� ������ ������ �� ����������� �� ��������� �������� (�� ���� ������), ������ ���������� �������. ������������ ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                             dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 2 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '2. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ���� ������), ������ ���������� �������. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                             dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 3 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '3. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 4 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '4. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                            aOtkazi.Execute()
                         else
                            dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 5 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '5. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 6 then
                      begin
                         ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '6. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else  dmOtsustvo.tblOtsustva.Post;
                end
             else if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) then
                begin
                   maxPlateni:=qKolektivenDog.FldByName['max_tipotsustvo_den'].Value;
                   maxTipPlateni:= qTipOtsustvo.FldByName['denovi'].Value;
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'PORAKA_OUT');
                   if poraka = 7 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '7. �������� ������ ������ �� ����������� �� ������� �������� (�� ���� ������), ������ ���������� �������. ������������ ��� �� ������ � '+intToStr(maxPlateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                            // dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                  //                         dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati,denovi-ostanati,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                      end
                   else if poraka = 8 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '8. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ��� (�� ���� ������), ������ ���������� �������. ������������ ��� �� ������ � '+intToStr(maxTipPlateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                             dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 9 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '9. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ���� ������), ������ ���������� �������. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                             dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 10 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '10. �������� ������ ������ �� ����������� �� ������� �������� (�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxPlateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 11 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '11. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 12 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '12. �������� ������ ������ �� ����������� �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxPlateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 13 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '13. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������� ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 14 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '14. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 15 then
                      begin
                         ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '15. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 16 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_1');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ���� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 17 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_1');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '17. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 18 then
                      begin
                         ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_2');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '18. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else  if poraka = 0 then dmOtsustvo.tblOtsustva.Post;

                end
             else dmOtsustvo.tblOtsustva.Post;

             StateActive:=dsBrowse;
             RestoreControls(Panel2);

             OstanatiDenovi.Visible:=False;

             Panel4.Enabled:=true;
             Panel2.Enabled:=false;
             tcPregled.Enabled:=True;
             cxGrid1.SetFocus;
        end;
end;

procedure TfrmEvidencijaOtsustva.aZapisiKrajExecute(Sender: TObject);
var poraka, flag, ostanati_1, ostanati_2,ostanati_po_tip_1, ostanati_po_tip_2, denovi, denovi_1, denovi_2, maxNeplateni, maxPlateni, maxTipPlateni:integer;
    e_mb, e_pricina_opis, e_odvreme, e_dovreme, e_dovreme_l: string;
    e_pricina, e, e_plateno:Integer;
begin
      if Validacija(Panel2) = false then
        begin
         if StateActive in [dsEdit] then
            begin
               qKolektivenDog.close;
               qKolektivenDog.ParamByName('param').Value:=now;
               qKolektivenDog.ExecQuery;
               e_mb:=dmOtsustvo.tblOtsustvaMB.Value;
               e_pricina:=dmOtsustvo.tblOtsustvaPRICINA.Value;
               e_pricina_opis:=dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value;
               e_odvreme:=DateToStr(dmOtsustvo.tblOtsustvaOD_VREME.value);
               e_dovreme:=DateToStr(dmOtsustvo.tblOtsustvaDO_VREME.value);
               e_dovreme_l:=DateToStr(dmOtsustvo.tblOtsustvaDO_VREME_L.value);
               e_plateno:=dmOtsustvo.tblOtsustvaPLATENO.Value;
               e:=1;
               dmOtsustvo.qDeleteResenieOtsustva.close;
               dmOtsustvo.qDeleteResenieOtsustva.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaID.Value;
               dmOtsustvo.qDeleteResenieOtsustva.ExecQuery;
               dmOtsustvo.qdeleteOtsustvo.close;
               dmOtsustvo.qDeleteotsustvo.ParamByName('old_id').Value:=dmOtsustvo.tblOtsustvaID.Value;
               dmOtsustvo.qDeleteotsustvo.execQuery;

               dmOtsustvo.tblOtsustva.Insert;
               StateActive:=dsInsert;
            end;
          if e = 1 then
             Begin
              dmOtsustvo.tblOtsustvaMB.Value:=e_mb;
              dmOtsustvo.tblOtsustvaPRICINA.Value:=e_pricina;
              dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value:=e_pricina_opis;
              dmOtsustvo.tblOtsustvaOD_VREME.value:=StrToDate(e_odvreme);
              dmOtsustvo.tblOtsustvaDO_VREME.value:=StrToDate(e_dovreme);
              dmOtsustvo.tblOtsustvaDO_VREME_L.value:=StrToDate(e_dovreme_l);
              dmOtsustvo.tblOtsustvaPLATENO.Value:=e_plateno;
             End;

          dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
          if ((qTipOtsustvo.FldByName['naziv'].Value = '������� �����') and (dmOtsustvo.tblOtsustvaPLATENO.Value = 1)) then
              begin
                 flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPreraspredelbaOtsustva,'OTSUSTVO_ID_IN', 'MB_IN', 'DATUM_OD', 'DATUM_DO',Null,dmOtsustvo.tblOtsustvaID.Value,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, Null, 'FLAG');
                 if (flag = 0 ) then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else if (flag = 1)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else if (flag= 2)  then                begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else
                   dmOtsustvo.tblOtsustva.Post;
              end
          else
           begin
             if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1)  then
                begin
                   maxNeplateni:=qKolektivenDog.FldByName['max_neplateni_denovi'].Value;
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'PORAKA_OUT');
                   if poraka = 1 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '1. �������� ������ ������ �� ����������� �� ��������� �������� (�� ���� ������), ������ ���������� �������. ������������ ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                             dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 2 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '2. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ���� ������), ������ ���������� �������. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             aOtkazi.Execute()
                         else
                             dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 3 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '3. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 4 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '4. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                            aOtkazi.Execute()
                         else
                            dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 5 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '5. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 6 then
                      begin
                         ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '6. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                aOtkazi.Execute()
                            else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else  dmOtsustvo.tblOtsustva.Post;
                end
             else if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (dmOtsustvo.tblOtsustvaPLATENO.Value = 1) then
                begin
                   maxPlateni:=qKolektivenDog.FldByName['max_tipotsustvo_den'].Value;
                   maxTipPlateni:= qTipOtsustvo.FldByName['denovi'].Value;
                   ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                   ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                   ostanati_po_tip_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_1');
                   ostanati_po_tip_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_2');
                   denovi:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'DENOVI_OUT');
                   denovi_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'DENOVI1_OUT');
                   denovi_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'DENOVI2_OUT');
                   flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'FLAG_V');
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'PORAKA_OUT');
                   if flag = 1 then
                      begin
                         if ostanati_po_tip_1 >= denovi then
                            if ostanati_1 >= denovi then
                               dmOtsustvo.tblOtsustva.Post
                            else
                               begin
                                  frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '9. �������� ������ ������ �� ���������� ������ �� ������� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_1)+' ������� � '+intToStr(denovi - ostanati_1)+' ��������� ��������?', 1);
                                  if (frmDaNe.ShowModal <> mrYes) then
                                      aOtkazi.Execute()
                                  else
                                      begin
                                         dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                            dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati_1,denovi-ostanati_1,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                                         dmOtsustvo.tblOtsustva.FullRefresh;
                                         dmOtsustvo.tblOtsustva2.FullRefresh;
                                      end;
                               end
                         else
                            begin
                               if ostanati_1 >= ostanati_po_tip_1 then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_po_tip_1)+' ������� � '+intToStr(denovi - ostanati_po_tip_1)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                        aOtkazi.Execute()
                                     else
                                         begin
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                               dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati_po_tip_1,denovi-ostanati_po_tip_1,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                                            dmOtsustvo.tblOtsustva.FullRefresh;
                                            dmOtsustvo.tblOtsustva2.FullRefresh;
                                         end;
                                  end
                               else  if (ostanati_1 < ostanati_po_tip_1) then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_1)+' ������� � '+intToStr(denovi - ostanati_1)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                        aOtkazi.Execute()
                                     else
                                         begin
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                               dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati_1,denovi-ostanati_1,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                                            dmOtsustvo.tblOtsustva.FullRefresh;
                                            dmOtsustvo.tblOtsustva2.FullRefresh;
                                         end;
                                  end;
                            end;
                      end
                   else if flag = 2 then
                      begin
                         if poraka = 10 then
                            begin
                               ShowMessage('10. �������� ������ ������ �� ����������� �� ������� �������� (�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxPlateni));
                               aOtkazi.Execute()
                            end
                         else if poraka = 11 then
                            begin
                               ShowMessage('11. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni));
                               aOtkazi.Execute()
                            end
                         else if poraka = 12 then
                            begin
                               ShowMessage('12. �������� ������ ������ �� ����������� �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxPlateni));
                               aOtkazi.Execute()
                            end
                         else if poraka = 13 then
                            begin
                               ShowMessage('13. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������� ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni));
                               aOtkazi.Execute()
                            end
                         else if poraka = 14 then
                            begin
                               ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                               ShowMessage('14. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1));
                               aOtkazi.Execute()
                            end
                         else if poraka = 15 then
                            begin
                               ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                               ShowMessage('15. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2));
                               aOtkazi.Execute()
                            end
                         else if poraka = 17 then
                            begin
                               ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_1');
                               ShowMessage('17. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1));
                               aOtkazi.Execute()
                            end
                         else if poraka = 18 then
                            begin
                               ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_PO_TIP_2');
                               ShowMessage('18. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2));
                               aOtkazi.Execute()
                            end
                      end;
                end
             else dmOtsustvo.tblOtsustva.Post;
           end;
          StateActive:=dsBrowse;
          RestoreControls(Panel2);

          OstanatiDenovi.Visible:=False;
          brDenovi.Visible:=false;
          Panel4.Enabled:=true;
          Panel2.Enabled:=false;
          tcPregled.Enabled:=True;
          cxGrid1.SetFocus;
        end;
        dmOtsustvo.tblOtsustva.FullRefresh;
        dmOtsustvo.tblOtsustva2.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.aZapisiVerzija2Execute(Sender: TObject);
var poraka, ostanatiTip, flag, ostanati, ostanati_po_tip, denovi:integer;
    sod,sdo:string;
begin
    if Validacija(Panel2) = false then
        begin
           dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
           if qTipOtsustvo.FldByName['naziv'].Value = '������� �����' then
              begin
                   flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPreraspredelbaOtsustva,'OTSUSTVO_ID_IN', 'MB_IN', 'DATUM_OD', 'DATUM_DO',Null,dmOtsustvo.tblOtsustvaID.Value,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, Null, 'FLAG');
                   if (flag =0 ) then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                   else if (flag = 1)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                   else if (flag= 2)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                   else
                      dmOtsustvo.tblOtsustva.Post;
              end
            else
                begin
                  if (qTipOtsustvo.FldByName['dogovor'].Value = 0)then
                     begin
                        dmOtsustvo.tblOtsustva.Post;
                     end
                  else  if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (qTipOtsustvo.FldByName['denovi'].Value <> Null) then
                     begin
                        ostanati:=dmOtsustvo.zemiRezultat(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', Null, Null, dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, Null, Null, 'OSTANATI');
                        ostanati_po_tip:=dmOtsustvo.zemiRezultat(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', Null, Null, dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, Null, Null, 'OSTANATI_PO_TIP');
                        denovi:=StrToInt(den.Caption);
                        if (ostanati - denovi)>=0 then
                           begin
                              if (ostanati_po_tip - denovi)>=0 then
                                 begin
                                    dmOtsustvo.tblOtsustva.Post;
                                 end
                              else
                                 begin
                                   frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '�������� ������ ������ !!! ���� ��������� ������ �� �� �������� ������� so '+intToStr(ostanati_po_tip)+' ������� � '+intToStr(denovi-ostanati_po_tip)+' ��������� ��������?', 1);
                                    if (frmDaNe.ShowModal <> mrYes) then
                                       Abort
                                    else
                                      begin
                                         if (ostanati_po_tip) > 0 then
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                                dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati_po_tip,denovi-ostanati_po_tip,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                                         if (ostanati_po_tip)<= 0 then
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                                dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,0,denovi-ostanati_po_tip,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');

                                         dmOtsustvo.tblOtsustva.close;
                                         dmOtsustvo.tblOtsustva.open;

                                         dmOtsustvo.tblOtsustva2.close;
                                         dmOtsustvo.tblOtsustva2.open;
                                      end;
                                 end;
                           end
                        else
                           begin
                              if (ostanati_po_tip <= ostanati) then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '�������� ������ ������ !!! ���� ��������� ������ �� �� �������� ������� so '+IntToStr(ostanati_po_tip)+' ������� � '+intToStr(denovi-ostanati_po_tip)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                        Abort
                                     else
                                       dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                           dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati_po_tip,denovi-ostanati_po_tip,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                                  end
                              else if (ostanati_po_tip > ostanati) then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '�������� ������ ������ !!! ���� ��������� ������ �� �� �������� ������� so '+IntToStr(ostanati)+' ������� � '+intToStr(denovi-ostanati)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                        Abort
                                     else
                                       dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                           dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,ostanati,denovi-ostanati,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
                                  end  
                           end;
                     end;
                end;
         StateActive:=dsBrowse;
         RestoreControls(Panel2);

         OstanatiDenovi.Visible:=False;

         Panel4.Enabled:=true;
         Panel2.Enabled:=false;
         tcPregled.Enabled:=True;
         cxGrid1.SetFocus;
        end;
end;

procedure TfrmEvidencijaOtsustva.brDenoviExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
     if (OD_VREME.Text <> '') and (brDenovi.Text <> '') and (MB.Text <> '') then
        begin
          dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=dmOtsustvo.zemiBroj(dmOtsustvo.pdatumdo, 'OD_DATUM', 'DENOVI', 'MB', Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, StrToInt(brDenovi.Text), dmOtsustvo.tblOtsustvaMB.Value, Null, 'DO_DATUM');
        end;
end;

procedure TfrmEvidencijaOtsustva.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
     dm.viewVraboteni.ParamByName('mb').Value:='%';
     dm.viewVraboteni.ParamByName('re').Value:='%';
     dm.viewVraboteni.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.FormCreate(Sender: TObject);
begin
     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmEvidencijaOtsustva.FormShow(Sender: TObject);
begin
     dxBarManager1Bar1.Caption := Caption;
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     procitajPrintOdBaza(Name,cxSchedulerOtsustva.Name, dxComponentPrinter1Link2);

     cbGodina.EditValue:=dmKon.godina;

     dmOtsustvo.tblOtsustva.close;
     dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('re').Value:='%';
     dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
     dmOtsustvo.tblOtsustva.Open;

     dmOtsustvo.tblOtsustva2.close;
     dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
     dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
     dmOtsustvo.tblOtsustva2.ParamByName('re').Value:='%';
     dmOtsustvo.tblOtsustva2.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
     dmOtsustvo.tblOtsustva2.Open;

     dmOtsustvo.tblTipOtsustvo.ParamByName('firma').Value:=firma;
     dmOtsustvo.tblTipOtsustvo.Open;

     tblBojOtsustva.Close;
     tblBojOtsustva.ParamByName('firma').Value:= firma;
     tblBojOtsustva.Open;

     dmOtsustvo.tblPodsektori.close;
     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.Open;

     cxBarEditRabEdinica.EditValue:=firma;
     StateActive:=dsBrowse;
end;

procedure TfrmEvidencijaOtsustva.MBExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti, ostanati_go, ostanati_site, ostanati_po_tip:integer;
   pom_od, pom_do, h, m :string;
begin
 if StateActive in [dsBrowse] then
   begin
    if ((PRICINANAZIV.Text <> '') and (mb.Text<> '') and (VRABOTENIME.Text<>'')) then
        begin
          qTipOtsustvo.Close;
          qTipOtsustvo.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaPRICINA.Value;
          qTipOtsustvo.ExecQuery;
          qKolektivenDog.close;
          qKolektivenDog.ParamByName('param').Value:=now;
          qKolektivenDog.ExecQuery;
          if qTipOtsustvo.FldByName['naziv'].Value = '������� �����' then
             begin
                ostanati_go:=dmOtsustvo.zemiBroj(dmOtsustvo.pOstanatiGOGodina, 'GODINA', 'MB', Null,Null, cbGodina.EditValue, dmOtsustvo.tblOtsustvaMB.Value, Null,Null, 'OSTANATI_OUT');
                OstanatiDenovi.Visible:=true;
                OstanatiDenovi.Caption:= '�������� ������ ������� �� �� '+ intToStr(ostanati_go);
             end
          else
             begin
              if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (qTipOtsustvo.FldByName['denovi'].Value <> Null) then
                  begin
                    if mb.Text <> '' then
                      begin
                        OstanatiDenovi.Visible:=true;
                        ostanati_site:= dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_1');
                        ostanati_po_tip:= dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_PO_TIP_1');
                        OstanatiDenovi.Caption:='�������� ������ '+IntToStr(ostanati_site)+', �������� �� ��������� ��� '+IntToStr(ostanati_po_tip);
                      end;
                  end
               else if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (mb.Text <> '')then
                  begin
                     OstanatiDenovi.Visible:=true;
                     OstanatiDenovi.Caption:='�������� ������ '+IntToStr(dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_1'));
                  end;
             end;
          dmOtsustvo.tblOtsustvaPLATENO.Value:=qTipOtsustvo.FldByName['plateno'].Value;
        end;
    if (VRABOTENIME.Text <> '') and (MB.Text <> '')then
         begin
           dmOtsustvo.tblResenieOtsustva.ParamByName('MB').Value:= dmOtsustvo.tblOtsustvaMB.Value;
           dmOtsustvo.tblResenieOtsustva.ParamByName('godina').Value:= dmKon.godina;
           dmOtsustvo.tblResenieOtsustva.FullRefresh;
         end;
     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
         denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
         casovi:= denovi * 8;
         den.Caption:=IntToStr(denovi);
         Cas.Caption:=IntToStr(casovi);
        end;
   end;
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencijaOtsustva.OD_VREMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin
     TEdit(Sender).Color:=clWhite;
     if StateActive in [dsEdit, dsInsert] then
        begin
          if (OD_VREME.Text <> '') and (DO_VREME_L.Text <> '') then
             begin
               if DO_vreme_L.Date < OD_vreme.Date then
                 begin
                   ShowMessage('������ � �������� ������ !!!');
                   OD_vreme.SetFocus;
                 end
               else if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
        end;
end;

procedure TfrmEvidencijaOtsustva.PRICINANAZIVExit(Sender: TObject);
var ostanati_site, ostanati_po_tip, ostanati_go:Integer;
begin
  TEdit(Sender).Color:=clWhite;
  if StateActive in [dsInsert, dsEdit] then
    begin
     if ((PRICINANAZIV.Text <> '') and (mb.Text<> '')) then
        begin
          qTipOtsustvo.Close;
          qTipOtsustvo.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaPRICINA.Value;
          qTipOtsustvo.ExecQuery;
          qKolektivenDog.close;
          qKolektivenDog.ParamByName('param').Value:=now;
          qKolektivenDog.ExecQuery;
          if qTipOtsustvo.FldByName['naziv'].Value = '������� �����' then
             begin
                ostanati_go:=dmOtsustvo.zemiBroj(dmOtsustvo.pOstanatiGOGodina, 'GODINA', 'MB', Null,Null, cbGodina.EditValue, dmOtsustvo.tblOtsustvaMB.Value, Null,Null, 'OSTANATI_OUT');
                OstanatiDenovi.Visible:=true;
                OstanatiDenovi.Caption:= '�������� ������ ������� �� �� '+ intToStr(ostanati_go);
             end
          else
             begin
              if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (qTipOtsustvo.FldByName['denovi'].Value <> Null) then
                  begin
                    if mb.Text <> '' then
                      begin
                        OstanatiDenovi.Visible:=true;
                        ostanati_site:= dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_1');
                        ostanati_po_tip:= dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_PO_TIP_1');
                        OstanatiDenovi.Caption:='�������� ������ '+IntToStr(ostanati_site)+', �������� �� ��������� ��� '+IntToStr(ostanati_po_tip);
                      end;
                  end
               else if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (mb.Text <> '')then
                  begin
                     OstanatiDenovi.Visible:=true;
                     OstanatiDenovi.Caption:='�������� ������ '+IntToStr(dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_1'));
                  end;
             end;
          dmOtsustvo.tblOtsustvaPLATENO.Value:=qTipOtsustvo.FldByName['plateno'].Value;
        end;
    end;
end;

procedure TfrmEvidencijaOtsustva.RadioButton1Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewDay.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton2Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewWeek.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton3Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewWeeks.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton4Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewYear.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton5Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewGantt.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.RadioButton6Click(Sender: TObject);
begin
     cxSchedulerOtsustva.ViewTimeGrid.Active:=True;
end;

procedure TfrmEvidencijaOtsustva.TipOtsustvoPrebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
        begin
           dmOtsustvo.tblOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
           dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
        end;
     if TipOtsustvoPrebaraj.Text<> '' then
        begin
           dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
           dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
        end;
     if TipOtsustvoPrebaraj.Text= '' then
        begin
           dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
           dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
        end;
     dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
     dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
     dmOtsustvo.tblOtsustva.FullRefresh;
     dmOtsustvo.tblOtsustva2.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.VRABOTENIMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti, ostanati_go, ostanati_site, ostanati_po_tip:integer;
   pom_od, pom_do, h, m :string;
begin
if StateActive in [dsEdit, dsInsert] then
   begin
     if ((PRICINANAZIV.Text <> '') and (mb.Text<> '') and (VRABOTENIME.Text<>'')) then
        begin
          qTipOtsustvo.Close;
          qTipOtsustvo.ParamByName('id').Value:=dmOtsustvo.tblOtsustvaPRICINA.Value;
          qTipOtsustvo.ExecQuery;
          qKolektivenDog.close;
          qKolektivenDog.ParamByName('param').Value:=now;
          qKolektivenDog.ExecQuery;
          if qTipOtsustvo.FldByName['naziv'].Value = '������� �����' then
             begin
                ostanati_go:=dmOtsustvo.zemiBroj(dmOtsustvo.pOstanatiGOGodina, 'GODINA', 'MB', Null,Null, cbGodina.EditValue, dmOtsustvo.tblOtsustvaMB.Value, Null,Null, 'OSTANATI_OUT');
                OstanatiDenovi.Visible:=true;
                OstanatiDenovi.Caption:= '�������� ������ ������� �� �� '+ intToStr(ostanati_go);
             end
          else
             begin
              if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (qTipOtsustvo.FldByName['denovi'].Value <> Null) then
                  begin
                    if mb.Text <> '' then
                      begin
                        OstanatiDenovi.Visible:=true;
                        ostanati_site:= dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_1');
                        ostanati_po_tip:= dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_PO_TIP_1');
                        OstanatiDenovi.Caption:='�������� ������ '+IntToStr(ostanati_site)+', �������� �� ��������� ��� '+IntToStr(ostanati_po_tip);
                      end;
                  end
               else if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1) and (mb.Text <> '')then
                  begin
                     OstanatiDenovi.Visible:=true;
                     OstanatiDenovi.Caption:='�������� ������ '+IntToStr(dmOtsustvo.zemiRezultat7(dmOtsustvo.pNeplateniOstanati, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'TIP', 'FLAG_IN', 'GODINA_OD_IN', 'GODINA_DO_IN', Null,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, 1, 1, cbGodina.EditValue, cbGodina.EditValue, Null,  'OSTANATI_1'));
                  end;
             end;
          dmOtsustvo.tblOtsustvaPLATENO.Value:=qTipOtsustvo.FldByName['plateno'].Value;
        end;

     if (VRABOTENIME.Text <> '') and (MB.Text <> '')then
         begin
           dmOtsustvo.tblResenieOtsustva.ParamByName('MB').Value:= dmOtsustvo.tblOtsustvaMB.Value;
           dmOtsustvo.tblResenieOtsustva.ParamByName('godina').Value:= dmKon.godina;
           dmOtsustvo.tblResenieOtsustva.FullRefresh;
         end;

     if (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
         denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB', Null,dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
         casovi:= denovi * 8;
         den.Caption:=IntToStr(denovi);
         Cas.Caption:=IntToStr(casovi);
        end;
     TEdit(Sender).Color:=clWhite;
   end;
end;

procedure TfrmEvidencijaOtsustva.vrabotenprebarajPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (vrabotenprebaraj.Text<> '') then
       begin
          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
          dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:=vrabotenprebaraj.EditValue;
       end;
     if TipOtsustvoPrebaraj.Text<> '' then
       begin
        dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
        dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:=TipOtsustvoPrebaraj.EditValue;
       end;
     if (vrabotenprebaraj.Text = '') then
       begin
        dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
        dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
       end;
     dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
     dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
     dmOtsustvo.tblOtsustva.FullRefresh;
     dmOtsustvo.tblOtsustva2.FullRefresh;
end;

procedure TfrmEvidencijaOtsustva.cbGodinaChange(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     if cxBarEditRabEdinica.EditValue = firma then
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:='%';
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('re').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva.FullRefresh;

          dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('re').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva2.FullRefresh;

          cxSchedulerOtsustva.GoToDate(StrToDate('01.01.'+IntToStr(cbGodina.EditValue)));
        end
     else
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:=cxBarEditRabEdinica.EditValue;
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva.FullRefresh;

          dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva2.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva2.FullRefresh;

          cxSchedulerOtsustva.GoToDate(StrToDate('01.01.'+IntToStr(cbGodina.EditValue)));
        end;
end;

procedure TfrmEvidencijaOtsustva.cxBarEditRabEdinicaChange(Sender: TObject);
begin
     vrabotenprebaraj.Text:='';
     TipOtsustvoPrebaraj.Text:='';
     if cxBarEditRabEdinica.EditValue = firma then
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:='%';
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('re').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva.FullRefresh;

          dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('re').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva2.FullRefresh;
        end
     else
        begin
          dm.viewVraboteni.ParamByName('MB').Value:='%';
          dm.viewVraboteni.ParamByName('Re').Value:=cxBarEditRabEdinica.EditValue;
          dm.viewVraboteni.FullRefresh;

          dmOtsustvo.tblOtsustva.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblOtsustva.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva.FullRefresh;

          dmOtsustvo.tblOtsustva2.ParamByName('MB').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('pricina').Value:='%';
          dmOtsustvo.tblOtsustva2.ParamByName('firma').Value:=firma;
          dmOtsustvo.tblOtsustva2.ParamByName('re').Value:=cxBarEditRabEdinica.EditValue;
          dmOtsustvo.tblOtsustva2.ParamByName('param_godina').Value:=cbGodina.EditValue;
          dmOtsustvo.tblOtsustva2.FullRefresh;
        end;
end;

procedure TfrmEvidencijaOtsustva.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;
procedure TfrmEvidencijaOtsustva.cxDBTextEditAllExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencijaOtsustva.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
 begin

        if (StateActive in [dsBrowse]) and (OD_VREME.Text <> '') and (DO_VREME.Text <> '') then
        begin
           if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
              begin
                pom_od:=OD_VREME.Text;
                pom_do:=DO_VREME.Text;

                cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                if cas_od = cas_do then
                   begin
                     casovi:=0;
                     minuti:= minuta_do - minuta_od;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                     Den.Caption:='0';
                   end
                else
                   begin
                     minuti:= 60 - minuta_od + minuta_do;
                     if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;
                     Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';;
                     Den.Caption:='0';
                   end;
              end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value, Null,'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
        end;
end;

procedure TfrmEvidencijaOtsustva.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaOtsustva.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if cxPageControl1.ActivePage = tcPregled then
        cxSchedulerOtsustva.SetFocus;
end;

procedure TfrmEvidencijaOtsustva.cxSchedulerOtsustvaDblClick(Sender: TObject);
var poraka, flag, ostanati_1, ostanati_2,ostanati_po_tip_1, ostanati_po_tip_2, denovi, denovi_1, denovi_2, maxNeplateni, maxPlateni, maxTipPlateni:integer;
begin
     if vrabotenprebaraj.Text <> '' then
     if TipOtsustvoPrebaraj.Text <> '' then
        begin
           dmOtsustvo.tblOtsustva.Insert;
           qTipOtsustvo.Close;
           qTipOtsustvo.ParamByName('id').Value:=TipOtsustvoPrebaraj.EditValue;
           qTipOtsustvo.ExecQuery;
           qKolektivenDog.close;
           qKolektivenDog.ParamByName('param').Value:=now;
           qKolektivenDog.ExecQuery;
           if (qTipOtsustvo.FldByName['naziv'].Value = '������� �����') then
              begin
                 flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPreraspredelbaOtsustva,'OTSUSTVO_ID_IN', 'MB_IN', 'DATUM_OD', 'DATUM_DO',Null,dmOtsustvo.tblOtsustvaID.Value,vrabotenprebaraj.EditValue, cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish - 1, Null, 'FLAG');
                 if (flag = 0 ) then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else if (flag = 1)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else if (flag= 2)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                 else
                   begin
                      dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                      dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                      dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                      dmOtsustvo.tblOtsustvaPLATENO.Value:=1;
                      dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                      dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                      dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                      dmOtsustvo.tblOtsustva.Post;
                      dmOtsustvo.tblOtsustva.FullRefresh;
                      dmOtsustvo.tblOtsustva2.FullRefresh;
                      StateActive:=dsBrowse;
                   end;
              end
          else
           begin
             if (qTipOtsustvo.FldByName['plateno'].Value = 0) and (qTipOtsustvo.FldByName['dogovor'].Value = 1)  then
                begin
                   maxNeplateni:=qKolektivenDog.FldByName['max_neplateni_denovi'].Value;
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'PORAKA_OUT');
                   if poraka = 1 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '1. �������� ������ ������ �� ����������� �� ��������� �������� (�� ���� ������), ������ ���������� �������. ������������ ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                            begin
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else
                             begin
                                dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                dmOtsustvo.tblOtsustva.Post;
                             end;
                      end
                   else if poraka = 2 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '2. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ���� ������), ������ ���������� �������. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                             begin
                                dmOtsustvo.tblOtsustva.Cancel;
                                StateActive:=dsBrowse;
                             end
                         else
                             begin
                                dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                dmOtsustvo.tblOtsustva.Post;
                             end;
                      end
                   else if poraka = 3 then
                      begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '3. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                begin
                                   dmOtsustvo.tblOtsustva.Cancel;
                                   StateActive:=dsBrowse;
                                end
                             else
                                dmOtsustvo.tblOtsustva.Post;
                      end
                   else if poraka = 4 then
                      begin
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '4. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxNeplateni)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                         if (frmDaNe.ShowModal <> mrYes) then
                            begin
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else
                            begin
                               dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                               dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                               dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                               dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                               dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                               dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                               dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                               dmOtsustvo.tblOtsustva.Post;
                            end;
                      end
                   else if poraka = 5 then
                      begin
                         ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_1_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '5. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                begin
                                   dmOtsustvo.tblOtsustva.Cancel;
                                   StateActive:=dsBrowse;
                                 end
                            else
                                begin
                                   dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                   dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                   dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                   dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                   dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                   dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                   dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                   dmOtsustvo.tblOtsustva.Post;
                                end;
                      end
                   else if poraka = 6 then
                      begin
                         ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value,dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'OSTANATI_2_OUT');
                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '6. �������� ������ ������ �� ���������� ������ �� ��������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2)+'. ���� ��������� ������ �� �� �������� �������?', 1);
                            if (frmDaNe.ShowModal <> mrYes) then
                                begin
                                   dmOtsustvo.tblOtsustva.Cancel;
                                   StateActive:=dsBrowse;
                                end
                            else
                                begin
                                  dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                  dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                                  dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                  dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                  dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                  dmOtsustvo.tblOtsustva.Post;
                                end;
                      end
                   else
                      begin
                        dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                        dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                        dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                        dmOtsustvo.tblOtsustvaPLATENO.Value:=0;
                        dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                        dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                        dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                        dmOtsustvo.tblOtsustva.Post;
                      end;
                end
             else if (qTipOtsustvo.FldByName['plateno'].Value = 1) and (qTipOtsustvo.FldByName['dogovor'].Value = 1)  then
                begin
                   maxPlateni:=qKolektivenDog.FldByName['max_tipotsustvo_den'].Value;
                   maxTipPlateni:= qTipOtsustvo.FldByName['denovi'].Value;
                   ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_1_OUT');
                   ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_2_OUT');
                   ostanati_po_tip_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue,TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_1');
                   ostanati_po_tip_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_2');
                   denovi:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'DENOVI_OUT');
                   denovi_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'DENOVI1_OUT');
                   denovi_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'DENOVI2_OUT');
                   flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'FLAG_V');
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'PORAKA_OUT');
                   if flag = 1 then
                      begin
                         if ostanati_po_tip_1 >= denovi then
                            if ostanati_1 >= denovi then
                               begin
                                  dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                                  dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                                  dmOtsustvo.tblOtsustvaPLATENO.Value:=1;
                                  dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                                  dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                                  dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                                  dmOtsustvo.tblOtsustva.Post;
                               end
                            else
                               begin
                                  frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '9. �������� ������ ������ �� ���������� ������ �� ������� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_1)+' ������� � '+intToStr(denovi - ostanati_1)+' ��������� ��������?', 1);
                                  if (frmDaNe.ShowModal <> mrYes) then
                                     begin
                                        dmOtsustvo.tblOtsustva.Cancel;
                                        StateActive:=dsBrowse;
                                     end
                                  else
                                      begin
                                         dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                            cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish,ostanati_1,denovi-ostanati_1,vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, '', Null, Null, Null, Null, Null,'BR');
                                         dmOtsustvo.tblOtsustva.FullRefresh;
                                         dmOtsustvo.tblOtsustva2.FullRefresh;
                                      end;
                               end
                         else
                            begin
                               if ostanati_1 >= ostanati_po_tip_1 then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_po_tip_1)+' ������� � '+intToStr(denovi - ostanati_po_tip_1)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                         begin
                                            dmOtsustvo.tblOtsustva.Cancel;
                                            StateActive:=dsBrowse;
                                         end
                                     else
                                         begin
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                               cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish,ostanati_po_tip_1,denovi-ostanati_po_tip_1,vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, '', Null, Null, Null, Null, Null,'BR');
                                            dmOtsustvo.tblOtsustva.FullRefresh;
                                            dmOtsustvo.tblOtsustva2.FullRefresh;
                                         end;
                                  end
                               else  if (ostanati_1 < ostanati_po_tip_1) then
                                  begin
                                     frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '16. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_1)+' ������� � '+intToStr(denovi - ostanati_1)+' ��������� ��������?', 1);
                                     if (frmDaNe.ShowModal <> mrYes) then
                                        begin
                                           dmOtsustvo.tblOtsustva.Cancel;
                                           StateActive:=dsBrowse;
                                        end
                                     else
                                         begin
                                            dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
                                                               cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish,ostanati_1,denovi-ostanati_1,vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, '', Null, Null, Null, Null, Null,'BR');
                                            dmOtsustvo.tblOtsustva.FullRefresh;
                                            dmOtsustvo.tblOtsustva2.FullRefresh;
                                         end;
                                  end;
                            end;
                      end
                   else if flag = 2 then
                      begin
                         if poraka = 10 then
                            begin
                               ShowMessage('10. �������� ������ ������ �� ����������� �� ������� �������� (�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 11 then
                            begin
                               ShowMessage('11. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������ ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 12 then
                            begin
                               ShowMessage('12. �������� ������ ������ �� ����������� �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(maxPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 13 then
                            begin
                               ShowMessage('13. �������� ������ ������ �� ����������� �� ������� �������� �� ������ ���(�� ������� ������), ������ ���������� �������. ���������� ��� �� ������ � '+intToStr(maxTipPlateni));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 14 then
                            begin
                               ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_1_OUT');
                               ShowMessage('14. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 15 then
                            begin
                               ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_2_OUT');
                               ShowMessage('15. �������� ������ ������ �� ���������� ������ �� ������� �������� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 17 then
                            begin
                               ostanati_1:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_1');
                               ShowMessage('17. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������ ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_1));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                         else if poraka = 18 then
                            begin
                               ostanati_2:=dmOtsustvo.zemiRezultat(dmOtsustvo.potsustva_realizacija, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN', 'TIP', vrabotenprebaraj.EditValue, TipOtsustvoPrebaraj.EditValue, cxSchedulerOtsustva.SelStart,cxSchedulerOtsustva.SelFinish,1, 'OSTANATI_PO_TIP_2');
                               ShowMessage('18. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� (�� ������� ������), ������ ���������� �������. ������� ��� �� ������ � '+intToStr(ostanati_2));
                               dmOtsustvo.tblOtsustva.Cancel;
                               StateActive:=dsBrowse;
                            end
                      end;
                end
             else
                 begin
                    dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                    dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                    dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                    dmOtsustvo.tblOtsustvaPLATENO.Value:=qTipOtsustvo.FldByName['plateno'].Value;
                    dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                    dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                    dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                    dmOtsustvo.tblOtsustva.Post;
                 end;
           end;
           dmOtsustvo.tblOtsustva2.FullRefresh;
        end
     else ShowMessage('�������� ��� �� �������� !!!')
    else ShowMessage('�������� �������� !!!');

end;

procedure TfrmEvidencijaOtsustva.cxSchedulerOtsustvaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var poraka, ostanatiTip, flag:integer;
    sod,sdo:string;
  begin
      case Key of
        VK_INSERT:
        begin
        if vrabotenprebaraj.Text <> '' then
          if TipOtsustvoPrebaraj.Text <> '' then
             begin
                dmOtsustvo.tblOtsustva.Insert;
                if TipOtsustvoPrebaraj.Text = '������� �����' then
                  begin
                   flag:=dmOtsustvo.zemiRezultat(dmOtsustvo.pPreraspredelbaOtsustva,'OTSUSTVO_ID_IN', 'MB_IN', 'DATUM_OD', 'DATUM_DO',Null,dmOtsustvo.tblOtsustvaID.Value,vrabotenprebaraj.EditValue, cxSchedulerOtsustva.SelStart, cxSchedulerOtsustva.SelFinish - 1, Null, 'FLAG');
                   if (flag =0 ) then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                   else if (flag = 1)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                   else if (flag= 2)  then
                   begin
                      ShowMessage('������ �������� ������ �� ������� ����� ������ ������� !!!');
                      dmOtsustvo.tblOtsustva.Cancel;
                   end
                   else
                     begin
                      dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                      dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                      dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                      dmOtsustvo.tblOtsustvaPLATENO.Value:=1;
                      dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                      dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                      dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                      dmOtsustvo.tblOtsustva.Post;
                     end;
                  end
                else
                  begin
                   poraka:=dmOtsustvo.zemiRezultat(dmOtsustvo.pKontrolaOtsustva, 'MB_IN', 'TIP_OTSUSTVO_ID_IN', 'DATUM_OD_IN', 'DATUM_DO_IN','TIP', dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,1, 'PORAKA_OUT');
                   if poraka = 1 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '1. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������ ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 2 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '2. �������� ������ ������ �� ����������� �� ��������� �������� (�� ������� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 3 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '3. �������� ������ ������ �� ����������� �� ��������� ��������(�� ���� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 4 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '4. �������� ������ ������ �� ����������� �� ��������� �������� (������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 5 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '5. �������� ������ ������ �� ����������� �� ��������� �������� (������� �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 6 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '6. �������� ������ ������ �� ����������� �� ��������� �������� (���� ������ ������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;

                     end
                  else  if poraka = 7 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '7. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ������ ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 8 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '8.�������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ������� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 9 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '9. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ���� ������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 10 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '10. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 11 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '11. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (������� �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end
                  else  if poraka = 12 then
                     begin
                        frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '12. �������� ������ ������ �� ����������� �� ��������� ��� �� �������� (�� ���� ������ �� ���������), ������ ���������� ������� !!! ���� ��������� ������ �� �� �������� �������?', 1);
                        if (frmDaNe.ShowModal <> mrYes) then
                          Abort;
                     end;
                  dmOtsustvo.tblOtsustvaTIP_ZAPIS.Value:=1;
                  dmOtsustvo.tblOtsustvaPRICINA.Value:=TipOtsustvoPrebaraj.EditValue;
                  dmOtsustvo.tblOtsustvaMB.Value:=vrabotenprebaraj.EditValue;
                  dmOtsustvo.tblOtsustvaPLATENO.Value:=1;
                  dmOtsustvo.tblOtsustvaOD_VREME.Value:=cxSchedulerOtsustva.SelStart;
                  dmOtsustvo.tblOtsustvaDO_VREME.Value:=cxSchedulerOtsustva.SelFinish;
                  dmOtsustvo.tblOtsustvaDO_VREME_L.Value:=cxSchedulerOtsustva.SelFinish -1;
                  dmOtsustvo.tblOtsustva.Post;
                   dmOtsustvo.tblOtsustva.Post;
                end
             end
          else ShowMessage('�������� ��� �� ��������')
        else ShowMessage('�������� ��������');

        end;
    end;
end;

procedure TfrmEvidencijaOtsustva.DO_VREMEExit(Sender: TObject);
var casovi, denovi, cas_od, minuta_od, cas_do, minuta_do, minuti:integer;
   pom_od, pom_do, h, m :string;
begin

     TEdit(Sender).Color:=clWhite;
     if StateActive in [dsEdit, dsInsert] then
        begin
          if (OD_VREME.Text <> '') and (DO_VREME_L.Text <> '') then
             begin
               dmOtsustvo.tblOtsustvaDO_VREME.Value:=dmOtsustvo.tblOtsustvaDO_VREME_L.Value + 1;
               if DO_vreme_L.Date < OD_vreme.Date then
                 begin
                   ShowMessage('������ � �������� ������ !!!');
                   DO_vreme_L.SetFocus;
                 end
               else if DateToStr(OD_VREME.date) = DateToStr(DO_VREME.date) then
                  begin
                    pom_od:=OD_VREME.Text;
                    pom_do:=DO_VREME.Text;

                    cas_od:=StrToInt(pom_od[13]+pom_od[14]);
                    cas_do:=StrToInt(pom_do[13]+pom_do[14]);
                    minuta_od:=StrToInt(pom_od[16]+pom_od[17]);
                    minuta_do:=StrToInt(pom_do[16]+pom_do[17]);

                    if cas_od = cas_do then
                      begin
                        casovi:=0;
                        minuti:= minuta_do - minuta_od;
                        Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                        Den.Caption:='0';
                      end
                    else
                       begin
                         minuti:= 60 - minuta_od + minuta_do;
                         if minuti >= 60 then
                            begin
                               minuti:=minuti - 60;
                               casovi:=cas_do - cas_od;
                            end
                         else
                            begin
                               casovi:=cas_do - cas_od-1;
                            end;

                         Cas.Caption:=IntToStr(casovi)+' � '+IntToStr(minuti) + '���';
                         Den.Caption:='0';
                       end;
                  end
               else
                  begin
                    denovi:= dmOtsustvo.zemiBroj(dmOtsustvo.WorkDays,'OD_DATUM', 'DO_DATUM', 'MB',Null, dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME_L.Value, dmOtsustvo.tblOtsustvaMB.Value,Null, 'DENOVI');
                    casovi:= denovi * 8;
                    den.Caption:=IntToStr(denovi);
                    Cas.Caption:=IntToStr(casovi);
                  end;
             end
          else
             begin
               den.Caption:=' ';
               Cas.Caption:=' ';
             end;
        end;
end;

procedure TfrmEvidencijaOtsustva.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
           if (Sender = PRICINANAZIV) then
              begin
                frmTipOtsustvo:=TfrmTipOtsustvo.Create(Application);
                frmTipOtsustvo.Tag:=1;
                frmTipOtsustvo.ShowModal;
                frmTipOtsustvo.Free;
                dmOtsustvo.tblOtsustvaPRICINA.Value:=dmOtsustvo.tblTipOtsustvoID.Value;
              end;
        end;
    end;
end;
end.
//if ostanati_po_tip_1 >= denovi_1 then
//                            if ostanati_1 >= denovi_1 then
//                               if ostanati_po_tip_2 >= denovi_2 then
//                                  if ostanati_2 >= denovi_2 then
//                                     dmOtsustvo.tblOtsustva.Post
//                                  else
//                                      begin
//                                         frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '15. �������� ������ ������ �� ���������� ������ �� ������� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_2)+' ������� � '+intToStr(denovi_2 - ostanati_2)+' ��������� �������� �� ������� ������?', 1);
//                                         if (frmDaNe.ShowModal <> mrYes) then
//                                            aOtkazi.Execute()
//                                         else
//                                             begin
//                                                dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
//                                                                    dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,denovi_1 + ostanati_2,(denovi_2-ostanati_2),dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
//                                                dmOtsustvo.tblOtsustva.FullRefresh;
//                                             end;
//                                      end
//                               else
//                                   begin
//                                      if ostanati_2 >= ostanati_po_tip_2 then
//                                         begin
//                                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '18. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_po_tip_2)+' ������� � '+intToStr(denovi_2 - ostanati_po_tip_2)+' ��������� �������� �� ������� ������?', 1);
//                                            if (frmDaNe.ShowModal <> mrYes) then
//                                                aOtkazi.Execute()
//                                            else
//                                                begin
//                                                   dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
//                                                                      dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,denovi_1 + ostanati_po_tip_2,denovi_2-ostanati_po_tip_2,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
//                                                   dmOtsustvo.tblOtsustva.FullRefresh;
//                                                end;
//                                         end
//                                      else  if (ostanati_2 < ostanati_po_tip_2) then
//                                         begin
//                                            frmDaNe := TfrmDaNe.Create(self, '���������� �� �����', '18. �������� ������ ������ �� ���������� ������ �� ������� �������� �� ������ ��� �� ��������. ���� ��������� ������ �� �� �������� ������� �� '+IntToStr(ostanati_2)+' ������� � '+intToStr(denovi_2 - ostanati_2)+' ��������� �������� �� ������� ������?', 1);
//                                            if (frmDaNe.ShowModal <> mrYes) then
//                                               aOtkazi.Execute()
//                                            else
//                                                begin
//                                                   dmOtsustvo.insert12(dmOtsustvo.pZapisiOtsustvoRaspredeleno,'DATUM_OD', 'DATUM_DO', 'PLATENI', 'NEPLATENI', 'MB', 'PRICINA', 'PRICINA_OPIS', Null, Null, Null, Null, Null,
//                                                                       dmOtsustvo.tblOtsustvaOD_VREME.Value, dmOtsustvo.tblOtsustvaDO_VREME.Value,denovi_1+ostanati_2,denovi_2-ostanati_2,dmOtsustvo.tblOtsustvaMB.Value, dmOtsustvo.tblOtsustvaPRICINA.Value, dmOtsustvo.tblOtsustvaPRICINA_OPIS.Value, Null, Null, Null, Null, Null,'BR');
//                                                   dmOtsustvo.tblOtsustva.FullRefresh;
//                                                end;
//                                         end;
//                                   end
//                            else
//                                begin
//
//                                end;
