unit StarosnaStruktura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxSkinsdxStatusBarPainter,
  cxCustomData, DB, cxDBData, cxGridChartView, cxGridDBChartView,
  cxGridCustomView, cxClasses, cxGridLevel, dxStatusBar, cxGrid, ExtCtrls,
  cxFilter, cxData, cxDataStorage, cxEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxContainer, cxLabel,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxPC, dxSkinsdxBarPainter,
  dxSkinsdxRibbonPainter, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxBarSkinnedCustForm, dxBar, Menus, ActnList,
  dxPSCore, dxPScxCommon,  dxRibbon, dxRibbonStatusBar, cxHint,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxDBLookupComboBox, cxBarEditItem, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator, Vcl.ComCtrls, dxCore,
  cxDateUtils, Vcl.StdCtrls, System.Actions, cxButtons, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, dxBarBuiltInMenu;

type
  TfrmStarosnaStruktura = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    cxGrid2: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBChartView1: TcxGridDBChartView;
    cxGrid1DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series2: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series3: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series4: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series5: TcxGridDBChartSeries;
    cxGrid1DBChartView1Series6: TcxGridDBChartSeries;
    cxGrid1Level1: TcxGridLevel;
    dxBarManager1: TdxBarManager;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxBarManager1Bar1: TdxBar;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarSubItem2: TdxBarSubItem;
    aPecatiPoSektori: TAction;
    aPecatiCelaOrganizacija: TAction;
    dxComponentPrinter2: TdxComponentPrinter;
    dxGridReportLink1: TdxGridReportLink;
    dxComponentPrinter3: TdxComponentPrinter;
    dxGridReportLink2: TdxGridReportLink;
    dxBarButton3: TdxBarButton;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxHintStyleController1: TcxHintStyleController;
    dxBarManager1Bar5: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    Panel1: TPanel;
    Panel4: TPanel;
    Datum: TcxDateEdit;
    cxLabel1: TcxLabel;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1GODINA_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1MESEC_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1DATUMRAGJANJE_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1RE: TcxGridDBColumn;
    cxGrid4DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1RABOTNO_MESTO: TcxGridDBColumn;
    cxGrid4DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    dxComponentPrinter4: TdxComponentPrinter;
    dxGridReportLink3: TdxGridReportLink;
    dxBarButton2: TdxBarButton;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RE_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1RM_RE_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1VOZRAST_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1MB_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_RADJANJE_OUT: TcxGridDBColumn;
    cxGrid1DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGrid1DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    cxTabSheet2: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBChartView1: TcxGridDBChartView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBChartView1Series1: TcxGridDBChartSeries;
    cxGridDBChartView1Series2: TcxGridDBChartSeries;
    cxGridDBChartView1Series3: TcxGridDBChartSeries;
    cxGridDBChartView1Series4: TcxGridDBChartSeries;
    cxGridDBChartView1Series5: TcxGridDBChartSeries;
    cxGridDBChartView1Series6: TcxGridDBChartSeries;
    cxGrid1DBTableView1NAZIVVRABOTEN_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTENTI: TcxGridDBColumn;
    cxGrid4DBTableView1VRABNAZIV_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_GODINI_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_MESECI_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1STAZ_DENOVI_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1STAZ_GODINI_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1STAZ_MESECI_OUT: TcxGridDBColumn;
    cxGrid4DBTableView1STAZ_DENOVI_OUT: TcxGridDBColumn;
    dxBarLargeButton16: TdxBarLargeButton;
    aPregledajPecati: TAction;
    aDizajnPecati: TAction;
    cxGrid4DBTableView1MB_OUT: TcxGridDBColumn;
    cxGrid1DBTableView1BENEFICIRAN_STAZ: TcxGridDBColumn;
    cxGrid1DBTableView1BENEFICIRAN_STAZ_K: TcxGridDBColumn;
    panel: TPanel;
    param1: TcxTextEdit;
    Label1: TLabel;
    param2: TcxTextEdit;
    Label2: TLabel;
    param3: TcxTextEdit;
    Label3: TLabel;
    param4: TcxTextEdit;
    Label4: TLabel;
    param5: TcxTextEdit;
    Label5: TLabel;
    cxButton1: TcxButton;
    aPromeniParametri: TAction;
    procedure FormCreate(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiPoSektoriExecute(Sender: TObject);
    procedure aPecatiCelaOrganizacijaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxBarEditItem1Change(Sender: TObject);
    procedure cxGrid4DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
    procedure DatumExit(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aPregledajPecatiExecute(Sender: TObject);
    procedure aDizajnPecatiExecute(Sender: TObject);
    procedure aPromeniParametriExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;  Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStarosnaStruktura: TfrmStarosnaStruktura;

implementation

uses dmUnit, Utils, dmResources, dmKonekcija, dmUnitOtsustvo, dmReportUnit;

{$R *.dfm}

procedure TfrmStarosnaStruktura.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmStarosnaStruktura.aDizajnPecatiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30303;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.Variables.AddVariable('VAR', 'Datum', QuotedStr(Datum.EditText));

          dmReport.frxReport1.DesignReport();
//     dmReport.frxReport1.ShowReport;
end;

procedure TfrmStarosnaStruktura.aHelpExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmStarosnaStruktura.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmStarosnaStruktura.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmStarosnaStruktura.aPecatiCelaOrganizacijaExecute(Sender: TObject);
begin
     dxGridReportLink2.ReportTitle.Text := Caption;
     dxComponentPrinter3.Preview(true, dxGridReportLink2);
end;

procedure TfrmStarosnaStruktura.aPecatiPoSektoriExecute(Sender: TObject);
begin
     dxGridReportLink1.ReportTitle.Text := Caption;
     dxComponentPrinter2.Preview(true, dxGridReportLink1);
end;

procedure TfrmStarosnaStruktura.aPecatiTabelaExecute(Sender: TObject);
begin
     if cxPageControl2.ActivePage = cxTabSheet3 then
        begin
          dxComponentPrinter1Link1.ReportTitle.Text := Caption;
          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
        end
     else
        begin
          dxComponentPrinter1Link2.ReportTitle.Text := Caption;
          dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
        end;
end;

procedure TfrmStarosnaStruktura.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmStarosnaStruktura.aPregledajPecatiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30303;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.Variables.AddVariable('VAR', 'Datum', QuotedStr(Datum.EditText));

//          dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmStarosnaStruktura.aPromeniParametriExecute(Sender: TObject);
begin
    dm.tblVozrastVraboteniSektor.ParamByName('param1').Value:=StrToInt(param1.Text);
    dm.tblVozrastVraboteniSektor.ParamByName('param2').Value:=StrToInt(param2.Text);
    dm.tblVozrastVraboteniSektor.ParamByName('param3').Value:=StrToInt(param3.Text);
    dm.tblVozrastVraboteniSektor.ParamByName('param4').Value:=StrToInt(param4.Text);
    dm.tblVozrastVraboteniSektor.ParamByName('param5').Value:=StrToInt(param5.Text);
    dm.tblVozrastVraboteniSektor.ParamByName('firma').Value:=dmKon.re;
    dm.tblVozrastVraboteniSektor.ParamByName('re_in').Value:=cxBarEditItem1.EditValue;
    dm.tblVozrastVraboteniSektor.FullRefresh;

    dm.tblStarosnaStruktura.ParamByName('firma').Value:=dmKon.re;
    dm.tblStarosnaStruktura.ParamByName('param1').Value:=StrToInt(param1.Text);
    dm.tblStarosnaStruktura.ParamByName('param2').Value:=StrToInt(param2.Text);
    dm.tblStarosnaStruktura.ParamByName('param3').Value:=StrToInt(param3.Text);
    dm.tblStarosnaStruktura.ParamByName('param4').Value:=StrToInt(param4.Text);
    dm.tblStarosnaStruktura.ParamByName('param5').Value:=StrToInt(param5.Text);
    dm.tblStarosnaStruktura.FullRefresh;

    cxGrid1DBChartView1Series1.DataBinding.Field.DisplayLabel:=param1.Text +' � �������';
    cxGrid1DBChartView1Series2.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param1.Text)+ 1) +'-'+param2.Text;
    cxGrid1DBChartView1Series3.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param2.Text)+ 1) +'-'+param3.Text;
    cxGrid1DBChartView1Series4.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param3.Text)+ 1) +'-'+param4.Text;
    cxGrid1DBChartView1Series5.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param4.Text)+ 1) +'-'+param5.Text;
    cxGrid1DBChartView1Series6.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param5.Text)+ 1) +' � �����';

    cxGridDBChartView1Series1.DataBinding.Field.DisplayLabel:=param1.Text +' � �������';
    cxGridDBChartView1Series2.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param1.Text)+ 1) +'-'+param2.Text;
    cxGridDBChartView1Series3.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param2.Text)+ 1) +'-'+param3.Text;
    cxGridDBChartView1Series4.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param3.Text)+ 1) +'-'+param4.Text;
    cxGridDBChartView1Series5.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param4.Text)+ 1) +'-'+param5.Text;
    cxGridDBChartView1Series6.DataBinding.Field.DisplayLabel:=IntToStr(StrToInt(param5.Text)+ 1) +' � �����';
end;

procedure TfrmStarosnaStruktura.aSnimiIzgledExecute(Sender: TObject);
begin
      zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmStarosnaStruktura.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmStarosnaStruktura.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption)
end;

procedure TfrmStarosnaStruktura.cxBarEditItem1Change(Sender: TObject);
begin

     dm.tblVozrastVraboteniSektor.ParamByName('re_in').Value:=cxBarEditItem1.EditValue;
     dm.tblVozrastVraboteniSektor.FullRefresh;

     dm.tblVozrastVraboteni.Close;
     dm.tblVozrastVraboteni.ParamByName('re_in').Value:=cxBarEditItem1.EditValue;
     dm.tblVozrastVraboteni.Open;

     dm.tblVozrastNaDatum.Close;
     dm.tblVozrastNaDatum.ParamByName('re_in').Value:= cxBarEditItem1.EditValue;
     dm.tblVozrastNaDatum.Open;
end;

procedure TfrmStarosnaStruktura.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmStarosnaStruktura.cxGrid4DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid4DBTableView1);
end;
procedure TfrmStarosnaStruktura.DatumExit(Sender: TObject);
begin
     dm.tblVozrastNaDatum.close;
     dm.tblVozrastNaDatum.ParamByName('datumDO').Value:=Datum.EditValue;
     dm.tblVozrastNaDatum.Open;
end;

procedure TfrmStarosnaStruktura.FormCreate(Sender: TObject);
begin
     dm.tblStarosnaStruktura.Close;
     dm.tblStarosnaStruktura.ParamByName('firma').Value:=dmKon.re;
     dm.tblStarosnaStruktura.ParamByName('param1').Value:=20;
     dm.tblStarosnaStruktura.ParamByName('param2').Value:=30;
     dm.tblStarosnaStruktura.ParamByName('param3').Value:=40;
     dm.tblStarosnaStruktura.ParamByName('param4').Value:=50;
     dm.tblStarosnaStruktura.ParamByName('param5').Value:=60;
     dm.tblStarosnaStruktura.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmStarosnaStruktura.FormShow(Sender: TObject);
begin
    //dxBarManager1Bar1.Caption := Caption;
//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
    dmOtsustvo.tblPodsektori.Open;

    cxBarEditItem1.EditValue:=firma;
    Datum.EditValue:=Now;

    dm.tblVozrastVraboteni.Close;
    dm.tblVozrastVraboteni.ParamByName('firma').Value:=dmKon.re;
    dm.tblVozrastVraboteni.ParamByName('re_in').Value:=cxBarEditItem1.EditValue;
    dm.tblVozrastVraboteni.Open;

    dm.tblVozrastVraboteniSektor.Close;
    dm.tblVozrastVraboteniSektor.ParamByName('param1').Value:=20;
    dm.tblVozrastVraboteniSektor.ParamByName('param2').Value:=30;
    dm.tblVozrastVraboteniSektor.ParamByName('param3').Value:=40;
    dm.tblVozrastVraboteniSektor.ParamByName('param4').Value:=50;
    dm.tblVozrastVraboteniSektor.ParamByName('param5').Value:=60;
    dm.tblVozrastVraboteniSektor.ParamByName('firma').Value:=dmKon.re;
    dm.tblVozrastVraboteniSektor.ParamByName('re_in').Value:=cxBarEditItem1.EditValue;
    dm.tblVozrastVraboteniSektor.Open;

    dm.tblVozrastNaDatum.close;
    dm.tblVozrastNaDatum.ParamByName('re_in').Value:= cxBarEditItem1.EditValue;
    dm.tblVozrastNaDatum.ParamByName('firma').Value:= dmKon.re;
    dm.tblVozrastNaDatum.ParamByName('datumDO').Value:=Datum.EditValue;
    dm.tblVozrastNaDatum.Open;

    end;

procedure TfrmStarosnaStruktura.cxDBTextEditAllEnter(Sender: TObject);
begin
     TEdit(Sender).Color:=clSkyBlue;
end;
procedure TfrmStarosnaStruktura.cxDBTextEditAllExit(Sender: TObject);
begin
     TEdit(Sender).Color:=clWhite;
end;
procedure TfrmStarosnaStruktura.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;
end.
