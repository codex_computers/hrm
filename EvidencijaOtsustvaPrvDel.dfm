object frmEvidencijaOtsustva: TfrmEvidencijaOtsustva
  Left = 0
  Top = 0
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1072
  ClientHeight = 752
  ClientWidth = 1130
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1130
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxPodesuvanje: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 729
    Width = 1130
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 201
    Width = 1130
    Height = 528
    Align = alClient
    TabOrder = 2
    TabStop = False
    Properties.ActivePage = tcPlanOtsustvo
    OnPageChanging = cxPageControl1PageChanging
    ClientRectBottom = 528
    ClientRectRight = 1130
    ClientRectTop = 24
    object tcPlanOtsustvo: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1080' '#1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1072
      ImageIndex = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1130
        Height = 233
        Align = alTop
        Enabled = False
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          1130
          233)
        object Label15: TLabel
          Left = 9
          Top = 20
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 749
          Top = 20
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label7: TLabel
          Left = 550
          Top = 20
          Width = 29
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Sifra: TcxDBTextEdit
          Tag = 1
          Left = 805
          Top = 17
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dmOtsustvo.dsOtsustva
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          Visible = False
          Width = 113
        end
        object MB: TcxDBTextEdit
          Tag = 1
          Left = 95
          Top = 17
          Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dmOtsustvo.dsOtsustva
          ParentFont = False
          ParentShowHint = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          ShowHint = True
          Style.Shadow = False
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = MBExit
          OnKeyDown = EnterKakoTab
          Width = 146
        end
        object cxGroupBox1: TcxGroupBox
          Left = 711
          Top = 44
          Anchors = [akRight, akBottom]
          Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
          ParentShowHint = False
          ShowHint = True
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 4
          Height = 117
          Width = 215
          object Label3: TLabel
            Left = -9
            Top = 22
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1076' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 12
            Top = 49
            Width = 29
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1086' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object cas: TcxLabel
            Left = 145
            Top = 73
            Transparent = True
          end
          object cxLabel1: TcxLabel
            Left = 47
            Top = 73
            Caption = #1056#1072#1073#1086#1090#1085#1080' '#1095#1072#1089#1086#1074#1080': '
            Transparent = True
          end
          object den: TcxLabel
            Left = 146
            Top = 91
            Transparent = True
          end
          object cxLabel3: TcxLabel
            Left = 47
            Top = 91
            Caption = #1056#1072#1073#1086#1090#1085#1080' '#1076#1077#1085#1086#1074#1080': '
            Transparent = True
          end
          object OD_VREME: TcxDBDateEdit
            Tag = 1
            Left = 47
            Top = 19
            BeepOnEnter = False
            DataBinding.DataField = 'OD_VREME'
            DataBinding.DataSource = dmOtsustvo.dsOtsustva
            ParentShowHint = False
            Properties.DateButtons = [btnClear, btnToday]
            Properties.InputKind = ikMask
            ShowHint = True
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = OD_VREMEExit
            OnKeyDown = EnterKakoTab
            Width = 146
          end
          object DO_VREME_L: TcxDBDateEdit
            Tag = 1
            Left = 47
            Top = 46
            BeepOnEnter = False
            DataBinding.DataField = 'DO_VREME_L'
            DataBinding.DataSource = dmOtsustvo.dsOtsustva
            ParentShowHint = False
            Properties.DateButtons = [btnClear, btnToday]
            Properties.InputKind = ikMask
            ShowHint = True
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = DO_VREMEExit
            OnKeyDown = EnterKakoTab
            Width = 146
          end
        end
        object buttonOtkazi: TcxButton
          Left = 1029
          Top = 185
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          TabOrder = 7
          OnKeyDown = EnterKakoTab
        end
        object buttonZapisi: TcxButton
          Left = 948
          Top = 185
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          TabOrder = 6
          OnKeyDown = EnterKakoTab
        end
        object VRABOTENIME: TcxDBLookupComboBox
          Tag = 1
          Left = 241
          Top = 17
          Hint = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dmOtsustvo.dsOtsustva
          ParentShowHint = False
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'MB'
          Properties.ListColumns = <
            item
              Width = 280
              FieldName = 'MB'
            end
            item
              Caption = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077
              Width = 800
              FieldName = 'NAZIVVRABOTEN'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsViewVraboteni
          ShowHint = True
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = VRABOTENIMEExit
          OnKeyDown = EnterKakoTab
          Width = 438
        end
        object cxGroupBox2: TcxGroupBox
          Left = 15
          Top = 44
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1086#1090#1089#1091#1089#1074#1086
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 3
          DesignSize = (
            681
            167)
          Height = 167
          Width = 681
          object Label2: TLabel
            Left = -28
            Top = 76
            Width = 102
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1087#1080#1089' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = -6
            Top = 22
            Width = 80
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1058#1080#1087' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label6: TLabel
            Left = -6
            Top = 42
            Width = 80
            Height = 28
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1043#1054' :'
            Enabled = False
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object txtOpis: TcxDBMemo
            Left = 80
            Top = 73
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'PRICINA_OPIS'
            DataBinding.DataSource = dmOtsustvo.dsOtsustva
            Properties.ScrollBars = ssVertical
            Properties.WantReturns = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 80
            Width = 584
          end
          object PRICINANAZIV: TcxDBLookupComboBox
            Tag = 1
            Left = 80
            Top = 19
            Anchors = [akLeft, akTop, akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'PRICINA'
            DataBinding.DataSource = dmOtsustvo.dsOtsustva
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end
              item
                Width = 120
                FieldName = 'DENOVI'
              end
              item
                Width = 120
                FieldName = 'PlatenoNaziv'
              end>
            Properties.ListSource = dmOtsustvo.dsTipOtsustvo
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = PRICINANAZIVExit
            OnKeyDown = EnterKakoTab
            Width = 584
          end
          object cbResenieGO: TcxDBLookupComboBox
            Left = 80
            Top = 46
            Hint = 
              #1041#1088#1086#1112' '#1080' '#1075#1086#1076#1080#1085#1072' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088' '#1074#1088#1079' '#1082#1086#1077' '#1089#1077' '#1073#1072#1079#1080#1088#1072' '#1086#1090#1089#1091 +
              #1089#1090#1074#1086#1090#1086' '#1086#1076' '#1090#1080#1087' '#1043#1054#1043#1048#1064#1045#1053' '#1054#1044#1052#1054#1056
            DataBinding.DataField = 'ID_RESENIE_GO'
            DataBinding.DataSource = dmOtsustvo.dsOtsustva
            Enabled = False
            ParentShowHint = False
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'BRGODINARESENIE'
              end>
            Properties.ListSource = dmOtsustvo.dsResenieOtsustva
            ShowHint = True
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cbResenieGOExit
            OnKeyDown = EnterKakoTab
            Width = 170
          end
          object cxLabel4: TcxLabel
            Left = 256
            Top = 50
            AutoSize = False
            Caption = #1054#1089#1090#1072#1085#1072#1090#1080' '#1076#1077#1085#1086#1074#1080': '
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 98
          end
          object OstanatiDenovi: TcxLabel
            Left = 352
            Top = 50
            AutoSize = False
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 20
            AnchorX = 372
          end
        end
        object cxDBRadioGroup1: TcxDBRadioGroup
          Left = 711
          Top = 167
          Hint = #1055#1083#1072#1090#1077#1085#1086'/'#1053#1077#1087#1083#1072#1090#1077#1085#1086' '#1086#1090#1089#1091#1089#1090#1074#1086
          TabStop = False
          Anchors = [akRight, akBottom]
          Caption = #1055#1083#1072#1090#1077#1085#1086
          DataBinding.DataField = 'PLATENO'
          DataBinding.DataSource = dmOtsustvo.dsOtsustva
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          ParentShowHint = False
          Properties.Columns = 2
          Properties.DefaultCaption = '1'
          Properties.DefaultValue = 0
          Properties.Items = <
            item
              Caption = #1044#1072
              Value = 1
            end
            item
              Caption = #1053#1077
              Value = 0
            end>
          ShowHint = True
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 5
          Height = 44
          Width = 215
        end
        object DO_VREME: TcxDBDateEdit
          Tag = 1
          Left = 585
          Top = 17
          Hint = 
            #1054#1074#1086#1112' '#1076#1072#1090#1091#1084' '#1085#1077' '#1074#1083#1077#1075#1091#1074#1072' '#1074#1086' '#1087#1077#1088#1080#1086#1076#1086#1090' '#1079#1072' '#1086#1090#1089#1091#1089#1090#1074#1086'. '#1055#1086#1082#1072#1078#1091#1074#1072' '#1044#1054' '#1082#1086#1112' '#1076 +
            #1072#1090#1091#1084' '#1090#1088#1072#1077' '#1086#1090#1089#1091#1089#1090#1074#1086#1090#1086
          BeepOnEnter = False
          DataBinding.DataField = 'DO_VREME'
          DataBinding.DataSource = dmOtsustvo.dsOtsustva
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 8
          Visible = False
          OnEnter = cxDBTextEditAllEnter
          OnExit = DO_VREMEExit
          Width = 146
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 233
        Width = 1130
        Height = 271
        Align = alClient
        Caption = 'Panel4'
        TabOrder = 1
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 1128
          Height = 269
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dmOtsustvo.dsOtsustva
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.CellHints = True
            OptionsBehavior.ImmediateEditor = False
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1MB: TcxGridDBColumn
              DataBinding.FieldName = 'MB'
              Width = 102
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              DataBinding.FieldName = 'VRABOTENPREZIME'
              Width = 163
            end
            object cxGrid1DBTableView1VRABOTENIME: TcxGridDBColumn
              DataBinding.FieldName = 'VRABOTENIME'
              Width = 143
            end
            object cxGrid1DBTableView1OD_VREME: TcxGridDBColumn
              DataBinding.FieldName = 'OD_VREME'
              Width = 100
            end
            object cxGrid1DBTableView1DO_VREME: TcxGridDBColumn
              DataBinding.FieldName = 'DO_VREME_L'
              Width = 100
            end
            object cxGrid1DBTableView1TIPOTSUSTVONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'TIPOTSUSTVONAZIV'
              Width = 164
            end
            object cxGrid1DBTableView1BRGODINARESENIE: TcxGridDBColumn
              DataBinding.FieldName = 'BRGODINARESENIE'
              Width = 100
            end
            object cxGrid1DBTableView1PRICINA_OPIS: TcxGridDBColumn
              Caption = #1054#1087#1080#1089
              DataBinding.FieldName = 'PRICINA_OPIS'
              PropertiesClassName = 'TcxMemoProperties'
              Width = 163
            end
            object cxGrid1DBTableView1Column2: TcxGridDBColumn
              DataBinding.FieldName = 'PlatenoDaNe'
              HeaderAlignmentHorz = taCenter
              Width = 72
            end
            object cxGrid1DBTableView1Column3: TcxGridDBColumn
              DataBinding.FieldName = 'DENOVI'
              HeaderAlignmentHorz = taCenter
              Width = 105
            end
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1TIP_ZAPIS: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_ZAPIS'
              Visible = False
            end
            object cxGrid1DBTableView1PRICINA: TcxGridDBColumn
              DataBinding.FieldName = 'PRICINA'
              Visible = False
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
            object cxGrid1DBTableView1VRABOTENTATKOVOIME: TcxGridDBColumn
              DataBinding.FieldName = 'VRABOTENTATKOVOIME'
              Visible = False
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Visible = False
            end
            object cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'VRABOTENNAZIV'
              Visible = False
            end
            object cxGrid1DBTableView1OPTIONS: TcxGridDBColumn
              DataBinding.FieldName = 'OPTIONS'
              Visible = False
            end
            object cxGrid1DBTableView1EVENT_TYPE: TcxGridDBColumn
              DataBinding.FieldName = 'EVENT_TYPE'
              Visible = False
            end
            object cxGrid1DBTableView1TASK_LINKS_FIELD: TcxGridDBColumn
              DataBinding.FieldName = 'TASK_LINKS_FIELD'
              Visible = False
            end
            object cxGrid1DBTableView1PLATENO: TcxGridDBColumn
              DataBinding.FieldName = 'PLATENO'
              Visible = False
            end
            object cxGrid1DBTableView1FLAG: TcxGridDBColumn
              DataBinding.FieldName = 'FLAG'
              Visible = False
            end
            object cxGrid1DBTableView1ID_RESENIE_GO: TcxGridDBColumn
              DataBinding.FieldName = 'ID_RESENIE_GO'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object tcPregled: TcxTabSheet
      Caption = #1050#1072#1083#1077#1085#1076#1072#1088#1089#1082#1080' '#1087#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1072
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1130
        Height = 73
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object RadioGroup1: TRadioGroup
          Left = 19
          Top = 17
          Width = 375
          Height = 43
          Caption = #1055#1088#1077#1075#1083#1077#1076' '#1087#1086' :'
          TabOrder = 0
        end
        object RadioButton1: TRadioButton
          Left = 42
          Top = 32
          Width = 56
          Height = 17
          Caption = #1044#1077#1085#1086#1074#1080
          TabOrder = 1
          OnClick = RadioButton1Click
        end
        object RadioButton2: TRadioButton
          Left = 144
          Top = 32
          Width = 56
          Height = 17
          Caption = #1053#1077#1076#1077#1083#1072
          TabOrder = 2
          OnClick = RadioButton2Click
        end
        object RadioButton4: TRadioButton
          Left = 238
          Top = 32
          Width = 57
          Height = 17
          Caption = #1043#1086#1076#1080#1085#1072
          Checked = True
          TabOrder = 3
          TabStop = True
          OnClick = RadioButton4Click
        end
        object RadioButton6: TRadioButton
          Left = 341
          Top = 32
          Width = 41
          Height = 17
          Caption = #1043#1088#1080#1076
          TabOrder = 4
          OnClick = RadioButton6Click
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 73
        Width = 1130
        Height = 431
        Align = alClient
        TabOrder = 1
        object cxSchedulerOtsustva: TcxScheduler
          Left = 1
          Top = 1
          Width = 1128
          Height = 429
          DateNavigator.RowCount = 2
          DateNavigator.ShowDatesContainingHolidaysInColor = True
          ViewDay.WorkTimeOnly = True
          ViewGantt.WorkDaysOnly = True
          ViewGantt.WorkTimeOnly = True
          ViewTimeGrid.WorkDaysOnly = True
          ViewTimeGrid.WorkTimeOnly = True
          ViewYear.Active = True
          ViewYear.MonthHeaderPopupMenu.PopupMenu = PopupMenu1
          ViewYear.MonthHeaderPopupMenu.Items = [mhpmiFullYear, mhpmiHalfYear, mhpmiQuarter]
          Align = alClient
          ContentPopupMenu.PopupMenu = PopupMenu1
          ContentPopupMenu.UseBuiltInPopupMenu = False
          EventOperations.DialogShowing = False
          EventOperations.ReadOnly = True
          EventPopupMenu.PopupMenu = PopupMenu1
          EventPopupMenu.UseBuiltInPopupMenu = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          OptionsCustomize.ControlsSizing = False
          OptionsCustomize.IntegralSizing = False
          OptionsView.ShowHints = False
          OptionsView.WorkFinish = 0.666666666666666600
          PopupMenu = PopupMenu1
          Storage = cxSchedulerDBStorage1
          TabOrder = 0
          Splitters = {
            D8030000FB0000006704000000010000D303000001000000D8030000AC010000}
          StoredClientBounds = {010000000100000067040000AC010000}
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 126
    Width = 1130
    Height = 75
    Align = alTop
    TabOrder = 7
    DesignSize = (
      1130
      75)
    object cxGroupBox3: TcxGroupBox
      Left = 19
      Top = 6
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086' :'
      TabOrder = 0
      DesignSize = (
        1094
        60)
      Height = 60
      Width = 1094
      object Label8: TLabel
        Left = -3
        Top = 27
        Width = 80
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1088#1072#1073#1086#1090#1077#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 454
        Top = 27
        Width = 104
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1080#1087' '#1085#1072' '#1086#1090#1089#1091#1089#1090#1074#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object vrabotenprebaraj: TcxLookupComboBox
        Left = 83
        Top = 24
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'MB'
        Properties.ListColumns = <
          item
            Width = 280
            FieldName = 'MB'
          end
          item
            Caption = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077
            Width = 700
            FieldName = 'NAZIVVRABOTEN'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsViewVraboteni
        Properties.OnEditValueChanged = vrabotenprebarajPropertiesEditValueChanged
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 342
      end
      object TipOtsustvoPrebaraj: TcxLookupComboBox
        Left = 564
        Top = 24
        Anchors = [akLeft, akTop, akRight, akBottom]
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmOtsustvo.dsTipOtsustvo
        Properties.OnEditValueChanged = TipOtsustvoPrebarajPropertiesEditValueChanged
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 429
      end
      object ButtonIscisti: TcxButton
        Left = 1010
        Top = 22
        Width = 75
        Height = 25
        Action = aIscisti
        Anchors = []
        TabOrder = 2
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = dm.cxLargeImages
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 832
    Top = 128
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1139
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1141
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 678
          Visible = True
          ItemName = 'cxBarEditRabEdinica'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1054#1090#1089#1091#1089#1090#1074#1086
      CaptionButtons = <>
      DockedLeft = 765
      DockedTop = 0
      FloatLeft = 1141
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 940
      DockedTop = 0
      FloatLeft = 1141
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1061
      DockedTop = 0
      FloatLeft = 1141
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton32'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton33'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 35
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 42
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aDodadiPlanZaOtsustvo
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPecatenje
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarButton3: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton4: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aStatusGrupa
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aStatus
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.ListColumns = <>
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton5: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aDodadiPlanZaOtsustvo
      Caption = #1053#1086#1074
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end>
    end
    object dxBarButton6: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton32: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton33: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton34: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditRabEdinica: TcxBarEditItem
      Caption = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      Category = 0
      Hint = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      Visible = ivAlways
      OnChange = cxBarEditRabEdinicaChange
      Width = 200
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 65
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsPodsektori
      Properties.MaxLength = 100
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 712
    Top = 24
    object aPlanZaOtsustvo: TAction
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1087#1083#1072#1085' '#1079#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
      OnExecute = aPlanZaOtsustvoExecute
    end
    object aDodadiPlanZaOtsustvo: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aDodadiPlanZaOtsustvoExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aAzurirajPlan: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajPlanExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aOsvezi: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      OnExecute = aOsveziExecute
    end
    object aSnimiGoIzgledot: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiGoIzgledotExecute
    end
    object aZacuvajVoExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajVoExcelExecute
    end
    object aPecatenje: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      ImageIndex = 30
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aStatusGrupa: TAction
      Caption = 'aStatusGrupa'
    end
    object aStatus: TAction
      Caption = 'aStatus'
    end
    object aPrebaraj: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
    end
    object aIscisti: TAction
      Caption = #1048#1089#1087#1088#1072#1079#1085#1080
      ImageIndex = 23
      OnExecute = aIscistiExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPomos: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      ShortCut = 112
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 632
    Top = 40
    object N1: TMenuItem
      Action = aPlanZaOtsustvo
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112'  '#1086#1090#1089#1091#1089#1090#1074#1086
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 704
    Top = 112
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 648
    Top = 80
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 976
    Top = 184
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page # of Pages #]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40793.510017604170000000
      ShrinkToPageWidth = True
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TcxSchedulerReportLink
      Active = True
      Component = cxSchedulerOtsustva
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        '[Page # of Pages #]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40793.510018182870000000
      PrintRange.TimePrintFrom = 0.333333333333333300
      PrintRange.TimePrintTo = 0.666666666666666600
      BuiltInReportLink = True
    end
  end
  object cxSchedulerDBStorage1: TcxSchedulerDBStorage
    UseActualTimeRange = True
    Resources.Items = <>
    CustomFields = <>
    DataSource = dmOtsustvo.dsOtsustva
    FieldNames.ActualFinish = 'OD_VREME'
    FieldNames.ActualStart = 'DO_VREME'
    FieldNames.Caption = 'OPIS'
    FieldNames.EventType = 'EVENT_TYPE'
    FieldNames.Finish = 'DO_VREME'
    FieldNames.ID = 'ID'
    FieldNames.Message = 'PRICINA_OPIS'
    FieldNames.Options = 'OPTIONS'
    FieldNames.ParentID = 'ID'
    FieldNames.Start = 'OD_VREME'
    FieldNames.TaskLinksField = 'TASK_LINKS_FIELD'
    Left = 440
    Top = 80
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 152
  end
  object qTipOtsustvo: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select hto.naziv, hto.plateno, hto.denovi, hto.dogovor'
      'from hr_tip_otsustvo hto'
      'where hto.id = :id')
    Left = 360
    Top = 120
  end
  object qSumTipOtsustvo: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select coalesce((sum((select proc_hr_work_days.denovi from proc_' +
        'hr_work_days(hr.od_vreme, hr.do_vreme, hr.mb)))), 0)as denovi'
      'from hr_otsustva hr'
      'inner join hr_tip_otsustvo hto on hto.id = hr.pricina'
      'inner join view_hr_vraboteni v on v.mb=hr.mb'
      'where hr.tip_zapis = 1 and hr.mb = :mb'
      '      and hto.plateno = 1  and  hto.dogovor = 1 and'
      
        '      extractyear(hr.od_vreme) =  extractyear(:param) and  extra' +
        'ctyear(hr.do_vreme) = extractyear(:param)'
      
        '      and (current_date between v.datum_od and coalesce(v.datum_' +
        'do, current_date)) and v.id_re_firma = :firma')
    Left = 496
    Top = 144
  end
  object qSumNeplateni: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select coalesce( (sum((select proc_hr_work_days.denovi from proc' +
        '_hr_work_days(hr.od_vreme, hr.do_vreme, hr.mb)))), 0)as denovi'
      'from hr_otsustva hr'
      'inner join hr_tip_otsustvo hto on hto.id = hr.pricina'
      'inner join view_hr_vraboteni v on v.mb=hr.mb'
      
        'where hr.tip_zapis = 1 and hr.mb = :mb  and v.id_re_firma = :fir' +
        'ma'
      '      and hto.plateno = 0  and'
      
        '      extractyear(hr.od_vreme) =  extractyear(:param) and  extra' +
        'ctyear(hr.do_vreme) = extractyear(:param1)'
      
        '      and (current_date between v.datum_od and coalesce(v.datum_' +
        'do, current_date))')
    Left = 456
    Top = 280
  end
  object qKolektivenDog: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select p.max_tipotsustvo_den, p.max_neplateni_denovi'
      'from hr_parametri_kd p'
      
        'where :param between p.datum_od and coalesce(p.datum_do, current' +
        '_date)')
    Left = 560
    Top = 272
  end
end
