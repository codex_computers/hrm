inherited frmRMRE: TfrmRMRE
  Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' '#1074#1086' '#1056#1072#1073#1086#1090#1085#1080' '#1045#1076#1080#1085#1080#1094#1080
  ClientHeight = 748
  ClientWidth = 978
  ExplicitWidth = 994
  ExplicitHeight = 787
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 328
    Height = 272
    Align = alNone
    ExplicitWidth = 328
    ExplicitHeight = 272
    inherited cxGrid1: TcxGrid
      Width = 324
      Height = 268
      PopupMenu = PopupMenu1
      ExplicitWidth = 324
      ExplicitHeight = 268
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsRMRE
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 75
        end
        object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE'
          Visible = False
          Width = 92
        end
        object cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RE'
          Width = 101
        end
        object cxGrid1DBTableView1ID_SISTEMATIZACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_SISTEMATIZACIJA'
          Visible = False
          Width = 53
        end
        object cxGrid1DBTableView1OPIS_SISTEMATIZACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS_SISTEMATIZACIJA'
          Width = 161
        end
        object cxGrid1DBTableView1ID_RM: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RM'
          Width = 116
        end
        object cxGrid1DBTableView1BROJ_IZVRSITELI: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_IZVRSITELI'
          Width = 189
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1KOREN: TcxGridDBColumn
          DataBinding.FieldName = 'KOREN'
          Visible = False
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RAKOVODENJE: TcxGridDBColumn
          Caption = #1056#1072#1082#1086#1074#1086#1076#1077#1114#1077
          DataBinding.FieldName = 'RAKOVODENJE'
        end
        object cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA'
          Visible = False
        end
        object cxGrid1DBTableView1USLOVI_RABOTA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA_NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1USLOVI_RABOTA_PROCENT: TcxGridDBColumn
          DataBinding.FieldName = 'USLOVI_RABOTA_PROCENT'
          Width = 100
        end
        object cxGrid1DBTableView1DEN_USLOVI_RABOTA: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088', '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080#1090#1077' '#1079#1072' '#1088#1072#1073#1086#1090#1072
          DataBinding.FieldName = 'DEN_USLOVI_RABOTA'
          SortIndex = 0
          SortOrder = soAscending
          Width = 130
        end
        object cxGrid1DBTableView1BENIFICIRAN_STAZ: TcxGridDBColumn
          DataBinding.FieldName = 'BENEFICIRAN_STAZ'
        end
        object cxGrid1DBTableView1ODGOVARA_PRED: TcxGridDBColumn
          Caption = #1054#1076#1075#1086#1074#1072#1088#1072' '#1087#1088#1077#1076
          DataBinding.FieldName = 'ODGOVARA_PRED'
          Width = 410
        end
      end
    end
  end
  inherited dPanel: TPanel
    Left = 37
    Top = 260
    Width = 819
    Height = 341
    Align = alNone
    Anchors = [akLeft, akTop, akRight, akBottom]
    ExplicitLeft = 37
    ExplicitTop = 260
    ExplicitWidth = 819
    ExplicitHeight = 341
    inherited Label1: TLabel
      Left = 38
      Width = 107
      Caption = #1056#1072#1073#1086#1090#1085#1072' '#1045#1076#1080#1085#1080#1094#1072' :'
      ExplicitLeft = 38
      ExplicitWidth = 107
    end
    object Label2: TLabel [1]
      Left = 2
      Top = 46
      Width = 144
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 2
      Top = 69
      Width = 144
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 2
      Top = 115
      Width = 144
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088'. '#1048#1079#1074#1088#1096#1080#1090#1077#1083#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 2
      Top = 92
      Width = 144
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel [5]
      Left = 120
      Top = 214
      Width = 192
      Height = 2
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1043#1054' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [6]
      Left = 2
      Top = 138
      Width = 144
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1076#1075#1086#1074#1072#1088#1072' '#1087#1088#1077#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 151
      DataBinding.DataField = 'ID_RE'
      DataBinding.DataSource = dmSis.dsRMRE
      ExplicitLeft = 151
      ExplicitWidth = 60
      Width = 60
    end
    inherited OtkaziButton: TcxButton
      Left = 727
      Top = 297
      TabOrder = 11
      ExplicitLeft = 727
      ExplicitTop = 297
    end
    inherited ZapisiButton: TcxButton
      Left = 646
      Top = 297
      TabOrder = 10
      ExplicitLeft = 646
      ExplicitTop = 297
    end
    object cbRE: TcxDBLookupComboBox
      Tag = 1
      Left = 210
      Top = 19
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'ID_RE'
      DataBinding.DataSource = dmSis.dsRMRE
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 200
          FieldName = 'ID'
        end
        item
          Width = 800
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmSis.dsReVoFirma
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 590
    end
    object txtSis: TcxDBTextEdit
      Tag = 1
      Left = 151
      Top = 42
      BeepOnEnter = False
      DataBinding.DataField = 'ID_SISTEMATIZACIJA'
      DataBinding.DataSource = dmSis.dsRMRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cbSis: TcxDBLookupComboBox
      Tag = 1
      Left = 210
      Top = 42
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'ID_SISTEMATIZACIJA'
      DataBinding.DataSource = dmSis.dsRMRE
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 200
          FieldName = 'ID'
        end
        item
          Width = 800
          FieldName = 'opis'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dsSitematizacija
      TabOrder = 3
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 590
    end
    object txtRM: TcxDBTextEdit
      Tag = 1
      Left = 151
      Top = 65
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM'
      DataBinding.DataSource = dmSis.dsRMRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cbRM: TcxDBLookupComboBox
      Tag = 1
      Left = 210
      Top = 65
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'ID_RM'
      DataBinding.DataSource = dmSis.dsRMRE
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 200
          FieldName = 'ID'
        end
        item
          Width = 800
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmSis.dsRabotnoMesto
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 590
    end
    object txtBrIzvrsiteli: TcxDBTextEdit
      Tag = 1
      Left = 151
      Top = 111
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ_IZVRSITELI'
      DataBinding.DataSource = dmSis.dsRMRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object txtBroj: TcxDBTextEdit
      Tag = 1
      Left = 151
      Top = 88
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dmSis.dsRMRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cxGroupBox1: TcxGroupBox
      Left = 29
      Top = 180
      Caption = '  '#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1088#1080#1084#1072#1114#1072'  '
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = False
      Style.TextStyle = [fsBold]
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = False
      TabOrder = 8
      Height = 117
      Width = 755
      object Label17: TLabel
        Left = -2
        Top = 26
        Width = 154
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1082#1086#1074#1086#1076#1077#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object Label19: TLabel
        Left = -2
        Top = 44
        Width = 154
        Height = 26
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1077#1085'. '#1079#1072' '#1043#1054' '#1089#1087#1086#1088#1077#1076' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1088#1072#1073#1086#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object Label7: TLabel
        Left = 44
        Top = 80
        Width = 107
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1077#1085#1080#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl1: TLabel
        Left = 243
        Top = 26
        Width = 24
        Height = 12
        Alignment = taRightJustify
        AutoSize = False
        Caption = '%'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl2: TLabel
        Left = 243
        Top = 79
        Width = 24
        Height = 12
        Alignment = taRightJustify
        AutoSize = False
        Caption = '%'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Rakovodenje: TcxDBTextEdit
        Left = 157
        Top = 23
        Hint = 
          #1055#1088#1086#1094#1077#1085#1090' '#1085#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1080#1086#1090' '#1073#1086#1076' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1088#1072#1082#1086#1074#1086#1076#1085 +
          #1072#1090#1072' '#1092#1091#1085#1082#1094#1080#1112#1072
        BeepOnEnter = False
        DataBinding.DataField = 'RAKOVODENJE'
        DataBinding.DataSource = dmSis.dsRMRE
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 92
      end
      object DenUsloviGO: TcxDBTextEdit
        Left = 158
        Top = 50
        Hint = 
          #1050#1086#1083#1082#1091' '#1089#1083#1086#1073#1086#1076#1085#1080' '#1076#1077#1085#1086#1074#1080' '#1089#1077' '#1076#1086#1076#1072#1074#1072#1072#1090' '#1085#1072' '#1075#1086#1076#1080#1096#1085#1080#1086#1090' '#1086#1076#1084#1086#1088', '#1082#1072#1082#1086' '#1088#1077#1079#1091#1083 +
          #1090#1072#1090' '#1085#1072' '#1087#1086#1089#1077#1073#1085#1080#1090#1077' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1072
        BeepOnEnter = False
        DataBinding.DataField = 'DEN_USLOVI_RABOTA'
        DataBinding.DataSource = dmSis.dsRMRE
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 92
      end
      object BENIFICIRAN_STAZ: TcxDBTextEdit
        Left = 157
        Top = 77
        Hint = #1041#1077#1085#1077#1092#1080#1094#1080#1088#1072#1085' '#1089#1090#1072#1078' %'
        BeepOnEnter = False
        DataBinding.DataField = 'BENEFICIRAN_STAZ'
        DataBinding.DataSource = dmSis.dsRMRE
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 91
      end
    end
    object txtOdgovaraPred: TcxDBTextEdit
      Left = 152
      Top = 138
      Hint = 
        #1055#1088#1077#1076' '#1082#1086#1112' '#1086#1076#1075#1086#1074#1072#1088#1072' '#1079#1072' '#1080#1079#1074#1088#1096#1077#1085#1072#1090#1072' '#1088#1072#1073#1086#1090#1072' ('#1076#1080#1088#1077#1082#1090#1086#1088', '#1088#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083'...' +
        ')'
      Anchors = [akLeft, akTop, akRight]
      BeepOnEnter = False
      DataBinding.DataField = 'ODGOVARA_PRED'
      DataBinding.DataSource = dmSis.dsRMRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 643
    end
    object cbSisSite: TcxDBLookupComboBox
      Tag = 1
      Left = 210
      Top = 42
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'ID_SISTEMATIZACIJA'
      DataBinding.DataSource = dmSis.dsRMRE
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 200
          FieldName = 'ID'
        end
        item
          Width = 800
          FieldName = 'opis'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dsSisSite
      TabOrder = 12
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 590
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 978
    OnTabChanging = dxRibbon1TabChanging
    ExplicitWidth = 978
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
    object rtOrganogram: TdxRibbonTab
      Caption = #1054#1088#1075#1072#1085#1086#1075#1088#1072#1084
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end>
      Index = 2
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 725
    Width = 978
    ExplicitTop = 725
    ExplicitWidth = 978
  end
  object Panel1: TPanel [4]
    Left = 432
    Top = 132
    Width = 376
    Height = 395
    TabOrder = 4
    object orgRM: TdxDbOrgChart
      Left = 1
      Top = 1
      Width = 374
      Height = 352
      ParentCustomHint = False
      DataSource = dmSis.dsOrgRMRE
      KeyFieldName = 'id_rmre'
      ParentFieldName = 'id_re'
      TextFieldName = 'naziv_re'
      OrderFieldName = 'id_rmre'
      ImageFieldName = 'slika'
      KeyOrder = True
      SelectedNodeTextColor = clInactiveCaptionText
      DefaultNodeWidth = 100
      DefaultNodeHeight = 70
      Options = [ocSelect, ocFocus, ocButtons, ocDblClick, ocEdit, ocCanDrag, ocShowDrag, ocInsDel, ocRect3D, ocAnimate]
      EditMode = [emLeft, emCenter, emVCenter, emWrap, emUpper, emGrow]
      Images = dmRes.cxSmallImages
      DefaultImageAlign = iaLT
      BorderStyle = bsNone
      OnCreateNode = orgRMCreateNode
      OnDeletion = orgRMDeletion
      OnSetText = orgRMSetText
      Align = alClient
      Color = clDefault
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object Panel2: TPanel
      Left = 1
      Top = 353
      Width = 374
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      Color = clInactiveCaptionText
      ParentBackground = False
      TabOrder = 1
      object cxVerticalGrid1: TcxVerticalGrid
        Left = 229
        Top = 0
        Width = 145
        Height = 41
        Align = alRight
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        OptionsView.RowHeaderWidth = 91
        OptionsData.Editing = False
        ParentFont = False
        TabOrder = 0
        Version = 1
        object cxVerticalGrid1EditorRow1: TcxEditorRow
          Properties.Caption = #1042#1088#1072#1073#1086#1090#1077#1085
          Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
          Properties.EditProperties.Alignment.Horz = taLeftJustify
          Properties.EditProperties.ColorBoxWidth = 50
          Properties.EditProperties.ColorComboStyle = cxccsComboList
          Properties.EditProperties.CustomColors = <>
          Properties.EditProperties.DefaultColor = clAqua
          Properties.EditProperties.PopupAlignment = taCenter
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxVerticalGrid1EditorRow2: TcxEditorRow
          Properties.Caption = #1057#1083#1086#1073#1086#1076#1085#1086' '#1056#1052
          Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
          Properties.EditProperties.Alignment.Horz = taCenter
          Properties.EditProperties.ColorBoxWidth = 50
          Properties.EditProperties.CustomColors = <>
          Properties.EditProperties.PopupAlignment = taCenter
          Properties.DataBinding.ValueType = 'String'
          Properties.Value = Null
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
      end
      object cxCheckBox1: TcxCheckBox
        Left = 0
        Top = 0
        Align = alLeft
        AutoSize = False
        Caption = ' '#1055#1086#1089#1090#1072#1074#1080' '#1089#1087#1086#1088#1077#1076' '#1089#1090#1088#1072#1085#1072#1090#1072
        Style.LookAndFeel.NativeStyle = True
        Style.Shadow = False
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 1
        OnClick = cxCheckBox1Click
        Height = 41
        Width = 248
      end
    end
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1056#1052' '#1074#1086' '#1056#1072#1073#1086#1090#1085#1080' '#1045#1076#1080#1085#1080#1094#1080
      DockedLeft = 113
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 413
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 733
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1055#1086#1089#1090#1072#1074#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 754
      FloatTop = 8
      FloatClientWidth = 95
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1055#1077#1095#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 192
      DockedTop = 0
      FloatLeft = 754
      FloatTop = 8
      FloatClientWidth = 122
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 305
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 754
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar [8]
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      CaptionButtons = <>
      DockedLeft = 296
      DockedTop = 0
      FloatLeft = 1012
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar [9]
      Caption = #1057#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1004
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'StatusSistematizacija'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aHorizontalno
      Category = 0
      LargeImageIndex = 40
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aVeritikalno
      Category = 0
      LargeImageIndex = 41
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPecatiOrg
      Category = 0
      LargeImageIndex = 39
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1053#1072' '#1077#1076#1085#1072' '#1089#1090#1088#1072#1085#1072
      Category = 0
      Hint = #1053#1072' '#1077#1076#1085#1072' '#1089#1090#1088#1072#1085#1072
      Visible = ivAlways
      OnCurChange = cxBarEditItem2CurChange
      ShowCaption = True
      Width = 50
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aUsloviPoteskiOdNormalnite
      Category = 0
    end
    object StatusSistematizacija: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1040#1082#1090#1080#1074#1085#1072
          Value = 1
        end
        item
          Caption = #1057#1080#1090#1077
          Value = 0
        end>
      Properties.OnChange = StatusSistematizacijaPropertiesChange
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aHorizontalno: TAction
      Caption = #1061#1086#1088#1080#1079#1086#1085#1090#1072#1083#1085#1086
      OnExecute = aHorizontalnoExecute
    end
    object aVeritikalno: TAction
      Caption = #1042#1077#1088#1080#1090#1080#1082#1072#1083#1085#1086
      OnExecute = aVeritikalnoExecute
    end
    object aPecatiOrg: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      OnExecute = aPecatiOrgExecute
    end
    object aSnimiPecatenjeOrg: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
    end
    object aBrisiPodPecOrg: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
    end
    object aPodesuvanjePecatenjeOrg: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      OnExecute = aPodesuvanjePecatenjeOrgExecute
    end
    object aPageSetupOrg: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
    end
    object aVraboten: TAction
      Caption = 'aVraboten'
    end
    object aUsloviPoteskiOdNormalnite: TAction
      Caption = #1059#1089#1083#1086#1074#1080' '#1087#1086#1090#1077#1096#1082#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1083#1085#1080#1090#1077
      ImageIndex = 90
      OnExecute = aUsloviPoteskiOdNormalniteExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 512
    Top = 176
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40254.574801423610000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxDBOrgChartReportLink
      Active = True
      Component = orgRM
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44454.614153402780000000
      BorderColor = clWhite
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 712
    Top = 56
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 648
    Top = 240
  end
  object tblSistematizacija: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd'
      'from hr_sistematizacija hs'
      'where(  hs.do_datum is null and hs.id_re_firma=:firma'
      '     ) and (     HS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd'
      'from hr_sistematizacija hs'
      'where hs.do_datum is null and hs.id_re_firma=:firma')
    AutoUpdateOptions.UpdateTableName = 'HR_SISTEMATIZACIJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_SISTEMATIZACIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 59
    oRefreshDeletedRecord = True
    object tblSistematizacijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSistematizacijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaOD_DATUM: TFIBDateField
      DisplayLabel = #1054#1076' '#1076#1072#1090#1091#1084
      FieldName = 'OD_DATUM'
    end
    object tblSistematizacijaDO_DATUM: TFIBDateField
      DisplayLabel = #1044#1086' '#1076#1072#1090#1091#1084
      FieldName = 'DO_DATUM'
    end
    object tblSistematizacijaDOKUMENT: TFIBStringField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'DOKUMENT'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblSistematizacijaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblSistematizacijaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSistematizacijaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSitematizacija: TDataSource
    DataSet = tblSistematizacija
    Left = 739
    Top = 51
  end
  object tblSisSite: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd'
      'from hr_sistematizacija hs'
      'where(  hs.do_datum is null and hs.id_re_firma=:firma'
      '     ) and (     HS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hs.id,'
      '    hs.opis,'
      '    hs.od_datum,'
      '    hs.do_datum,'
      '    hs.dokument,'
      '    hs.ts_ins,'
      '    hs.ts_upd,'
      '    hs.usr_ins,'
      '    hs.usr_upd'
      'from hr_sistematizacija hs'
      'where hs.id_re_firma=:firma')
    AutoUpdateOptions.UpdateTableName = 'HR_SISTEMATIZACIJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_SISTEMATIZACIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 576
    Top = 147
    oRefreshDeletedRecord = True
    object FIBIntegerField1: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object FIBStringField1: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBDateField1: TFIBDateField
      DisplayLabel = #1054#1076' '#1076#1072#1090#1091#1084
      FieldName = 'OD_DATUM'
    end
    object FIBDateField2: TFIBDateField
      DisplayLabel = #1044#1086' '#1076#1072#1090#1091#1084
      FieldName = 'DO_DATUM'
    end
    object FIBStringField2: TFIBStringField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'DOKUMENT'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBDateTimeField1: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object FIBDateTimeField2: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object FIBStringField3: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSisSite: TDataSource
    DataSet = tblSisSite
    Left = 635
    Top = 147
  end
  object tblUsloviPoteskiOdNormalni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_KOEFICIENT_USLOVIRAB'
      'SET '
      '    NAZIV = :NAZIV,'
      '    PROCENT = :PROCENT,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_KOEFICIENT_USLOVIRAB'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_KOEFICIENT_USLOVIRAB('
      '    ID,'
      '    NAZIV,'
      '    PROCENT,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :PROCENT,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select k.id,'
      '       k.naziv,'
      '       k.procent,'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from hr_koeficient_uslovirab k'
      ''
      ' WHERE '
      '        K.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select k.id,'
      '       k.naziv,'
      '       k.procent,'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from hr_koeficient_uslovirab k'
      'order by k.naziv')
    AutoUpdateOptions.UpdateTableName = 'HR_KOEFICIENT_USLOVIRAB'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_KOEFICIENT_USLOVIRAB_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 496
    Top = 1816
    object tblUsloviPoteskiOdNormalniID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblUsloviPoteskiOdNormalniNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUsloviPoteskiOdNormalniPROCENT: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090
      FieldName = 'PROCENT'
      Size = 2
    end
    object tblUsloviPoteskiOdNormalniTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblUsloviPoteskiOdNormalniTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblUsloviPoteskiOdNormalniUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUsloviPoteskiOdNormalniUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
end
