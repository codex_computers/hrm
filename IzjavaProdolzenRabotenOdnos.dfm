inherited frmIzjavaProdolzenRabotenOdnos: TfrmIzjavaProdolzenRabotenOdnos
  Caption = #1048#1079#1112#1072#1074#1072' '#1079#1072' '#1087#1088#1086#1076#1086#1083#1078#1077#1085' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
  ClientHeight = 692
  ClientWidth = 925
  ExplicitWidth = 933
  ExplicitHeight = 723
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 925
    Height = 248
    ExplicitWidth = 925
    ExplicitHeight = 248
    inherited cxGrid1: TcxGrid
      Width = 921
      Height = 244
      ExplicitWidth = 921
      ExplicitHeight = 244
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsIzjavaProdolzenRO
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
        end
        object cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDDOKUMENTNAZIV'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Width = 60
        end
        object cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'ARHIVSKI_BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 108
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 91
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1RABOTNAEDINICANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNAEDINICANAZIV'
          Width = 206
        end
        object cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESTONAZIV'
          Width = 207
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 83
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 222
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          DataBinding.FieldName = 'ZABELESKA'
          Width = 193
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 374
    Width = 925
    Height = 295
    ExplicitTop = 374
    ExplicitWidth = 925
    ExplicitHeight = 295
    inherited Label1: TLabel
      Left = 573
      Top = 7
      Visible = False
      ExplicitLeft = 573
      ExplicitTop = 7
    end
    object Label2: TLabel [1]
      Left = 62
      Top = 27
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1043#1086#1076#1080#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 21
      Top = 54
      Width = 91
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel [3]
      Tag = 15
      Left = 17
      Top = 77
      Width = 95
      Height = 26
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label7: TLabel [4]
      Left = 247
      Top = 27
      Width = 140
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel [5]
      Left = 23
      Top = 184
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' : '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 629
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      TabOrder = 1
      Visible = False
      ExplicitLeft = 629
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 810
      Top = 215
      TabOrder = 9
      ExplicitLeft = 810
      ExplicitTop = 215
    end
    inherited ZapisiButton: TcxButton
      Left = 729
      Top = 215
      TabOrder = 8
      ExplicitLeft = 729
      ExplicitTop = 215
    end
    object BROJ: TcxDBTextEdit
      Tag = 1
      Left = 118
      Top = 51
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1090#1074#1088#1076#1072
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object DatumKreiranje: TcxDBDateEdit
      Tag = 1
      Left = 393
      Top = 24
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      ParentFont = False
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object VRABOTENIME: TcxDBLookupComboBox
      Tag = 1
      Left = 243
      Top = 81
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      ShowHint = True
      StyleDisabled.TextColor = clBtnText
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 390
    end
    object MB: TcxDBTextEdit
      Tag = 1
      Left = 118
      Top = 81
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      StyleDisabled.TextColor = clBtnText
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object OBRAZLOZENIE: TcxDBMemo
      Left = 118
      Top = 181
      Hint = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'ZABELESKA'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      Properties.WantReturns = False
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 62
      Width = 515
    end
    object GODINA: TcxDBComboBox
      Tag = 1
      Left = 118
      Top = 24
      DataBinding.DataField = 'GODINA'
      DataBinding.DataSource = dm.dsIzjavaProdolzenRO
      Properties.DropDownListStyle = lsFixedList
      Properties.Items.Strings = (
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016')
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 123
    end
    object cxGroupBox1: TcxGroupBox
      Left = 118
      Top = 108
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090
      TabOrder = 6
      Height = 61
      Width = 515
      object Label5: TLabel
        Left = 34
        Top = 29
        Width = 67
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label6: TLabel
        Left = 274
        Top = 29
        Width = 67
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 107
        Top = 26
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsIzjavaProdolzenRO
        ParentFont = False
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikMask
        ShowHint = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 118
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 347
        Top = 27
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsIzjavaProdolzenRO
        ParentFont = False
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikMask
        ShowHint = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 118
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 925
    ExplicitWidth = 925
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 669
    Width = 925
    ExplicitTop = 669
    ExplicitWidth = 925
  end
  object txtArhivskiBroj: TcxTextEdit [4]
    Left = 128
    Top = 75
    AutoSize = False
    StyleDisabled.Color = clWindow
    StyleDisabled.TextColor = clWindowText
    TabOrder = 8
    OnExit = t
    OnKeyDown = EnterKakoTab
    Height = 21
    Width = 97
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 296
    Top = 200
  end
  inherited PopupMenu1: TPopupMenu
    Left = 400
    Top = 208
  end
  inherited dxBarManager1: TdxBarManager
    Top = 272
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 232
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 598
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 767
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 741
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 57
          Visible = True
          ItemName = 'cGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      CaptionButtons = <>
      DockedLeft = 114
      DockedTop = 0
      FloatLeft = 741
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 415
      DockedTop = 0
      FloatLeft = 858
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object cGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cGodinaChange
      Items.Strings = (
        #1057#1080#1090#1077
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016')
      ItemIndex = -1
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = '    '#1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112'    '
      Category = 0
      Hint = '    '#1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112'    '
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aKreirajPregledaj
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 176
    Top = 216
    object aKreirajPregledaj: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 19
      OnExecute = aKreirajPregledajExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 88
    Top = 224
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40889.520380949080000000
      ShrinkToPageWidth = True
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 392
    Top = 136
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 240
  end
  object qMaxBroj: TpFIBQuery
    Transaction = dm.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select cast((coalesce(max(cast(coalesce(substring(hrg.broj from ' +
        'strlen(hrg.arhivski_broj)+2 for 3),0) as integer)),0)) as intege' +
        'r) maks'
      'from hr_izjava_prodolzen_ro hrg'
      'where cast(hrg.arhivski_broj as varchar(20)) = :arhivski_broj')
    Left = 536
    Top = 264
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
