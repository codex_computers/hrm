inherited frmKategorija: TfrmKategorija
  Caption = #1055#1088#1072#1096#1072#1114#1072' '#1079#1072' '#1089#1090#1088#1091#1082#1090#1091#1088#1080#1088#1072#1085#1086' '#1080#1085#1090#1077#1088#1074#1112#1091
  ClientHeight = 565
  ClientWidth = 614
  OnKeyDown = FormKeyDown
  ExplicitWidth = 622
  ExplicitHeight = 599
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 614
    Height = 242
    ExplicitTop = 116
    ExplicitWidth = 614
    ExplicitHeight = 252
    inherited cxGrid1: TcxGrid
      Width = 610
      Height = 248
      PopupMenu = PopupMenu1
      ExplicitWidth = 610
      ExplicitHeight = 248
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        PopupMenu = PopupMenu1
        DataController.DataSource = dmSis.dsKategorija
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded, dcoImmediatePost]
        OptionsBehavior.PullFocusing = True
        OptionsData.Appending = True
        OptionsData.CancelOnExit = False
        OptionsData.Editing = True
        OptionsSelection.MultiSelect = True
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_RM: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_RM'
          Options.Editing = False
          Width = 155
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Options.Editing = False
          Width = 110
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 274
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
          Options.Editing = False
          Width = 99
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1ID_RM: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.ValueType = 'Boolean'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssUnchecked
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 368
    Width = 614
    Height = 174
    ExplicitTop = 368
    ExplicitWidth = 614
    ExplicitHeight = 174
    inherited Label1: TLabel
      Width = 108
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
      Font.Color = clNavy
      ExplicitWidth = 108
    end
    object Label2: TLabel [1]
      Left = 12
      Top = 69
      Width = 109
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1088#1072#1096#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 13
      Top = 46
      Width = 108
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 519
      Top = 27
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsKategorija
      TabOrder = 5
      Visible = False
      ExplicitLeft = 519
      ExplicitTop = 27
      ExplicitWidth = 42
      Width = 42
    end
    inherited OtkaziButton: TcxButton
      Left = 523
      Top = 134
      TabOrder = 6
      ExplicitLeft = 523
      ExplicitTop = 134
    end
    inherited ZapisiButton: TcxButton
      Left = 442
      Top = 134
      TabOrder = 4
      ExplicitLeft = 442
      ExplicitTop = 134
    end
    object cbRmRe: TcxDBLookupComboBox
      Left = 169
      Top = 19
      Hint = #1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM_RE'
      DataBinding.DataSource = dmSis.dsKategorija
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV_RM'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmSis.dsRMRE
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 275
    end
    object txtMemo: TcxDBMemo
      Tag = 1
      Left = 127
      Top = 70
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsKategorija
      Properties.WantReturns = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 51
      Width = 317
    end
    object txtRmRe: TcxDBTextEdit
      Left = 127
      Top = 19
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM_RE'
      DataBinding.DataSource = dmSis.dsKategorija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 42
    end
    object txtBroj: TcxDBTextEdit
      Tag = 1
      Left = 127
      Top = 44
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dmSis.dsKategorija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 66
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 614
    ExplicitWidth = 614
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 542
    Width = 614
    ExplicitTop = 542
    ExplicitWidth = 614
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40301.481749027780000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 520
    Top = 40
  end
  object qMaxBezRM: TpFIBQuery
    Transaction = dmOtsustvo.TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select max(coalesce(hk.broj,1))+1 maks'
      '      from hr_kategorija hk'
      '      where hk.id_rm_re is null')
    Left = 448
    Top = 65440
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
