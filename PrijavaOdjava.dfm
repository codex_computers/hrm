object frmPrijavaOdjava: TfrmPrijavaOdjava
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1088#1080#1112#1072#1074#1072' '#1080' '#1054#1076#1112#1072#1074#1072' '#1085#1072' '#1112#1072#1074#1077#1085' '#1089#1083#1091#1078#1073#1077#1085#1080#1082
  ClientHeight = 664
  ClientWidth = 978
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 978
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 641
    Width = 978
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 978
    Height = 515
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 515
    ClientRectRight = 978
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1055#1088#1080#1112#1072#1074#1072
      ImageIndex = 0
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 978
        Height = 217
        Align = alClient
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 976
          Height = 215
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsPrijava
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1TIP: TcxGridDBColumn
              DataBinding.FieldName = 'TIP'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
              DataBinding.FieldName = 'VID_DOKUMENT'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1MB: TcxGridDBColumn
              DataBinding.FieldName = 'MB'
              Width = 82
            end
            object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
              DataBinding.FieldName = 'PREZIME'
              Width = 124
            end
            object cxGrid1DBTableView1IME: TcxGridDBColumn
              DataBinding.FieldName = 'IME'
              Width = 115
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 100
            end
            object cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RABMESTONAZIV'
              Width = 120
            end
            object cxGrid1DBTableView1RAKOVODNO: TcxGridDBColumn
              DataBinding.FieldName = 'RAKOVODNO'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1RakovodnoNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'RakovodnoNaziv'
              Width = 100
            end
            object cxGrid1DBTableView1PROBNA: TcxGridDBColumn
              DataBinding.FieldName = 'PROBNA'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1ProbnoNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'ProbnoNaziv'
              Width = 100
            end
            object cxGrid1DBTableView1OSNOV: TcxGridDBColumn
              DataBinding.FieldName = 'OSNOV'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1OSNOVNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'OSNOVNAZIV'
              Width = 100
            end
            object cxGrid1DBTableView1INSTITUCIJA: TcxGridDBColumn
              DataBinding.FieldName = 'INSTITUCIJA'
              Width = 100
            end
            object cxGrid1DBTableView1STAZ: TcxGridDBColumn
              DataBinding.FieldName = 'STAZ'
              Width = 100
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 100
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object dPanel: TPanel
        Left = 0
        Top = 217
        Width = 978
        Height = 233
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvSpace
        Enabled = False
        TabOrder = 1
        ExplicitLeft = 3
        ExplicitTop = 193
        DesignSize = (
          978
          233)
        object Label1: TLabel
          Left = 644
          Top = -2
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label8: TLabel
          Left = 88
          Top = 36
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1051#1080#1094#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 32
          Top = 63
          Width = 145
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 80
          Top = 90
          Width = 97
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 27
          Top = 142
          Width = 150
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1089#1085#1086#1074' '#1085#1072' '#1087#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 58
          Top = 162
          Width = 119
          Height = 34
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086' '#1074#1086' '#1089#1090#1088#1091#1082#1072#1090#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label3: TLabel
          Left = 254
          Top = 169
          Width = 119
          Height = 13
          AutoSize = False
          Caption = #1075#1086#1076#1080#1085#1080
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Sifra: TcxDBTextEdit
          Tag = 1
          Left = 700
          Top = -5
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsPrijava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 3
          Visible = False
          Width = 80
        end
        object MB: TcxDBTextEdit
          Tag = 1
          Left = 183
          Top = 33
          Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dm.dsPrijava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object Lica: TcxDBLookupComboBox
          Tag = 1
          Left = 306
          Top = 33
          Hint = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077' '
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dm.dsPrijava
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'MB'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV_VRABOTEN'
            end>
          Properties.ListSource = dm.dsViewVraboteni
          StyleDisabled.TextColor = clDefault
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 474
        end
        object Datum: TcxDBDateEdit
          Tag = 1
          Left = 183
          Top = 60
          Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM'
          DataBinding.DataSource = dm.dsPrijava
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object RM: TcxDBTextEdit
          Tag = 1
          Left = 183
          Top = 87
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1089#1087#1086#1088#1077#1076' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
          BeepOnEnter = False
          DataBinding.DataField = 'ID_RM_RE'
          DataBinding.DataSource = dm.dsPrijava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clBackground
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 67
        end
        object RM_NAZIV: TcxDBLookupComboBox
          Tag = 1
          Left = 250
          Top = 87
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' '#1089#1087#1086#1088#1077#1076' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'ID_RM_RE'
          DataBinding.DataSource = dm.dsPrijava
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Width = 200
              FieldName = 'ID'
            end
            item
              FieldName = 'NAZIV_RE'
            end
            item
              Width = 800
              FieldName = 'NAZIV_RM'
            end>
          Properties.ListFieldIndex = 2
          Properties.ListSource = dsRMRE
          StyleDisabled.TextColor = clBackground
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 531
        end
        object OsnovNaziv: TcxDBLookupComboBox
          Tag = 1
          Left = 183
          Top = 139
          Hint = #1054#1089#1085#1086#1074' '#1085#1072' '#1087#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'OSNOV'
          DataBinding.DataSource = dm.dsPrijava
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'BROJ'
            end
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsOsnovNaOsiguruvanje
          Properties.OnChange = OsnovNazivPropertiesChange
          StyleDisabled.TextColor = clBackground
          TabOrder = 8
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 531
        end
        object staz: TcxDBTextEdit
          Tag = 1
          Left = 183
          Top = 166
          Hint = 
            #1042#1082#1091#1087#1085#1080#1086#1090' '#1088#1072#1073#1086#1090#1077#1085' '#1089#1090#1072#1078' '#1080#1083#1080' '#1080#1089#1082#1091#1089#1090#1074#1086' '#1074#1086' '#1089#1090#1088#1091#1082#1072#1090#1072' '#1076#1086' '#1076#1077#1085#1086#1090' '#1085#1072' '#1087#1088#1080#1112#1072 +
            #1074#1091#1074#1072#1114#1077#1090#1086', '#1074#1086' '#1075#1086#1076#1080#1085#1080
          BeepOnEnter = False
          DataBinding.DataField = 'STAZ'
          DataBinding.DataSource = dm.dsPrijava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 9
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 66
        end
        object cxDBRadioGroup1: TcxDBRadioGroup
          Left = 310
          Top = 101
          Hint = '1 - '#1056#1072#1082#1086#1074#1086#1076#1085#1086', 2 - '#1053#1077#1088#1072#1082#1086#1074#1086#1076#1085#1086
          Caption = #1042#1080#1076' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
          DataBinding.DataField = 'RAKOVODNO'
          DataBinding.DataSource = dm.dsPrijava
          Properties.Items = <
            item
              Caption = #1056#1072#1082#1086#1074#1086#1076#1085#1086
              Value = 1
            end
            item
              Caption = #1053#1077#1088#1072#1082#1086#1074#1086#1076#1085#1086
              Value = 2
            end>
          TabOrder = 6
          Visible = False
          Height = 32
          Width = 185
        end
        object cxDBCheckBox1: TcxDBCheckBox
          Left = 183
          Top = 114
          Hint = 
            #1057#1077' '#1096#1090#1080#1082#1083#1080#1088#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1087#1088#1080#1112#1072#1074#1072#1090#1072' '#1089#1077' '#1086#1076#1085#1077#1089#1091#1074#1072' '#1079#1072' '#1087#1088#1086#1073#1085#1072' '#1088#1072#1073#1086#1090#1072', '#1074#1086' '#1089 +
            #1087#1088#1086#1090#1080#1074#1085#1086' '#1085#1077#1084#1072' '#1072#1082#1094#1080#1112#1072
          Caption = #1055#1088#1086#1073#1085#1072' '#1088#1072#1073#1086#1090#1072
          DataBinding.DataField = 'PROBNA'
          DataBinding.DataSource = dm.dsPrijava
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          TabOrder = 7
          Width = 121
        end
        object ZapisiButton: TcxButton
          Left = 808
          Top = 190
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 10
        end
        object OtkaziButton: TcxButton
          Left = 889
          Top = 190
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 11
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 450
        Width = 978
        Height = 41
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 2
        DesignSize = (
          978
          41)
        object cxButton3: TcxButton
          Left = 21
          Top = 10
          Width = 252
          Height = 25
          Action = aKreirajOdjava
          Anchors = [akLeft]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 0
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1054#1076#1112#1072#1074#1072
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 978
        Height = 258
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object cxGrid2: TcxGrid
          Left = 1
          Top = 1
          Width = 976
          Height = 256
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid2DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsOdjava
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid2DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid2DBTableView1TIP: TcxGridDBColumn
              DataBinding.FieldName = 'TIP'
              Visible = False
            end
            object cxGrid2DBTableView1VID_DOKUMENT: TcxGridDBColumn
              DataBinding.FieldName = 'VID_DOKUMENT'
              Visible = False
            end
            object cxGrid2DBTableView1INSTITUCIJA: TcxGridDBColumn
              DataBinding.FieldName = 'INSTITUCIJA'
              Width = 159
            end
            object cxGrid2DBTableView1MB: TcxGridDBColumn
              DataBinding.FieldName = 'MB'
            end
            object cxGrid2DBTableView1PREZIME: TcxGridDBColumn
              DataBinding.FieldName = 'PREZIME'
              Width = 114
            end
            object cxGrid2DBTableView1IME: TcxGridDBColumn
              DataBinding.FieldName = 'IME'
              Width = 152
            end
            object cxGrid2DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
            end
            object cxGrid2DBTableView1PROBNA: TcxGridDBColumn
              DataBinding.FieldName = 'PROBNA'
              Visible = False
            end
            object cxGrid2DBTableView1ProbnoNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'ProbnoNaziv'
              Width = 91
            end
            object cxGrid2DBTableView1OSNOV: TcxGridDBColumn
              DataBinding.FieldName = 'OSNOV'
              Visible = False
            end
            object cxGrid2DBTableView1OSNOVNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'OSNOVNAZIV'
              Width = 216
            end
            object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
            end
            object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
            end
            object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
            end
            object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 258
        Width = 978
        Height = 233
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvSpace
        Enabled = False
        TabOrder = 1
        DesignSize = (
          978
          233)
        object Label7: TLabel
          Left = 89
          Top = 44
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1051#1080#1094#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 628
          Top = 8
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label10: TLabel
          Left = 33
          Top = 71
          Width = 145
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1112#1072#1074#1091#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 29
          Top = 125
          Width = 150
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1089#1085#1086#1074' '#1085#1072' '#1086#1076#1112#1072#1074#1091#1074#1072#1114#1077' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object MB_lice: TcxDBTextEdit
          Tag = 1
          Left = 184
          Top = 41
          Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dm.dsOdjava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object cxDBLookupComboBox1: TcxDBLookupComboBox
          Tag = 1
          Left = 307
          Top = 41
          Hint = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077' '
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dm.dsOdjava
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'MB'
          Properties.ListColumns = <
            item
              Width = 500
              FieldName = 'MB'
            end
            item
              Caption = #1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1048#1084#1077
              Width = 800
              FieldName = 'VRABOTENNAZIV'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsLica
          StyleDisabled.TextColor = clDefault
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 474
        end
        object cxDBTextEdit2: TcxDBTextEdit
          Tag = 1
          Left = 684
          Top = 5
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsOdjava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 2
          Visible = False
          Width = 80
        end
        object datum_odjava: TcxDBDateEdit
          Tag = 1
          Left = 184
          Top = 68
          Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1080#1112#1072#1074#1091#1074#1072#1114#1077
          BeepOnEnter = False
          DataBinding.DataField = 'DATUM'
          DataBinding.DataSource = dm.dsOdjava
          ParentShowHint = False
          Properties.DateButtons = [btnClear, btnToday]
          Properties.InputKind = ikMask
          ShowHint = True
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object cxDBCheckBox2: TcxDBCheckBox
          Left = 183
          Top = 95
          Hint = 
            #1057#1077' '#1096#1090#1080#1082#1083#1080#1088#1072' '#1076#1086#1082#1086#1083#1082#1091' '#1087#1088#1080#1112#1072#1074#1072#1090#1072' '#1089#1077' '#1086#1076#1085#1077#1089#1091#1074#1072' '#1079#1072' '#1087#1088#1086#1073#1085#1072' '#1088#1072#1073#1086#1090#1072', '#1074#1086' '#1089 +
            #1087#1088#1086#1090#1080#1074#1085#1086' '#1085#1077#1084#1072' '#1072#1082#1094#1080#1112#1072
          Caption = #1047#1072#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1088#1086#1073#1085#1072' '#1088#1072#1073#1086#1090#1072
          DataBinding.DataField = 'PROBNA'
          DataBinding.DataSource = dm.dsOdjava
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          TabOrder = 4
          Width = 186
        end
        object OsnovOdjava: TcxDBTextEdit
          Tag = 1
          Left = 183
          Top = 122
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1089#1085#1086#1074' '#1085#1072' '#1086#1076#1112#1072#1074#1091#1074#1072#1114#1077
          BeepOnEnter = False
          DataBinding.DataField = 'OSNOV'
          DataBinding.DataSource = dm.dsOdjava
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clBackground
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 67
        end
        object cxDBLookupComboBox3: TcxDBLookupComboBox
          Tag = 1
          Left = 250
          Top = 122
          Hint = #1054#1089#1085#1086#1074' '#1085#1072' '#1086#1076#1112#1072#1074#1091#1074#1072#1114#1077
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'OSNOV'
          DataBinding.DataSource = dm.dsOdjava
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end
            item
              FieldName = 'BROJ'
            end>
          Properties.ListSource = dsOsnovNaOsiguruvanje
          StyleDisabled.TextColor = clBackground
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 531
        end
        object cxButton1: TcxButton
          Left = 808
          Top = 190
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 7
        end
        object cxButton2: TcxButton
          Left = 889
          Top = 190
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 8
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 232
    Top = 256
  end
  object PopupMenu1: TPopupMenu
    Left = 344
    Top = 240
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 624
    Top = 280
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 247
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 454
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 888
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aJS_2
      Category = 0
      LargeImageIndex = 19
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 240
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aKreirajOdjava: TAction
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1086#1076#1112#1072#1074#1072' '#1079#1072' '#1086#1076#1073#1088#1072#1085#1072#1090#1072' '#1087#1088#1080#1112#1072#1074#1072
      ImageIndex = 10
      OnExecute = aKreirajOdjavaExecute
    end
    object aJS_2: TAction
      Caption = #1054#1073#1088#1072#1079#1077#1094' '#1032#1057'-2'
      OnExecute = aJS_2Execute
    end
    object aDizajnReport: TAction
      Caption = 'aDizajnReport'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDizajnReportExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42039.417803275460000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42039.417803287040000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository2
      Styles.StyleSheet = dxGridReportLinkStyleSheet2
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 656
    Top = 104
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblRMRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select mr.koren,'
      '    hrr.id,'
      '    hrr.id_re,'
      '    mr.naziv naziv_re,'
      '    hrr.id_sistematizacija,'
      '    hs.opis opis_sistematizacija,'
      '    hrr.id_rm,'
      '    hrm.naziv naziv_rm,'
      '    hrr.broj_izvrsiteli,'
      '    hrr.ts_ins,'
      '    hrr.ts_upd,'
      '    hrr.usr_ins,'
      '    hrr.usr_upd,'
      '    hrr.rakovodenje,'
      '    hrr.denovi_odmor,'
      '    hrr.den_uslovi_rabota,'
      '    hrm.stepen_slozenost'
      'from hr_rm_re hrr'
      'inner join mat_re mr on mr.id=hrr.id_re'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where hs.do_datum is null and hs.id_re_firma = :firma'
      'order by 4, 8')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 416
    Top = 235
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMREID_RE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1045
      FieldName = 'ID_RE'
    end
    object tblRMREID_SISTEMATIZACIJA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'ID_SISTEMATIZACIJA'
    end
    object tblRMREID_RM: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1056#1052
      FieldName = 'ID_RM'
    end
    object tblRMREBROJ_IZVRSITELI: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1088#1096#1080#1090#1077#1083#1080' '#1085#1072' '#1056#1052
      FieldName = 'BROJ_IZVRSITELI'
    end
    object tblRMRETS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblRMRETS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblRMREUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RE: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1045
      FieldName = 'NAZIV_RE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1089#1080#1089#1090#1077#1084#1072#1090#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'OPIS_SISTEMATIZACIJA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRMREKOREN: TFIBIntegerField
      FieldName = 'KOREN'
    end
    object tblRMRERAKOVODENJE: TFIBBCDField
      FieldName = 'RAKOVODENJE'
      Size = 2
    end
    object tblRMREDENOVI_ODMOR: TFIBIntegerField
      FieldName = 'DENOVI_ODMOR'
    end
    object tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField
      FieldName = 'DEN_USLOVI_RABOTA'
    end
    object tblRMRESTEPEN_SLOZENOST: TFIBBCDField
      FieldName = 'STEPEN_SLOZENOST'
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 483
    Top = 243
  end
  object tblOsnovNaOsiguruvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_OSNOV_OSIGURUVANJE'
      'SET '
      '    BROJ = :BROJ,'
      '    NAZIV = :NAZIV,'
      '    TIP_OSNOV = :TIP_OSNOV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    HR_OSNOV_OSIGURUVANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO HR_OSNOV_OSIGURUVANJE('
      '    ID,'
      '    BROJ,'
      '    NAZIV,'
      '    TIP_OSNOV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :NAZIV,'
      '    :TIP_OSNOV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select hro.id,'
      '       hro.broj,'
      '       hro.naziv,'
      '       hro.tip_osnov,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd'
      'from hr_osnov_osiguruvanje hro'
      'where(  hro.tip_osnov = :tip'
      '     ) and (     HRO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.broj,'
      '       hro.naziv,'
      '       hro.tip_osnov,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd'
      'from hr_osnov_osiguruvanje hro'
      'where hro.tip_osnov = :tip'
      'order by hro.broj')
    AutoUpdateOptions.UpdateTableName = 'HR_OSNOV_OSIGURUVANJE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_OSNOV_OSIGURUVANJE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 24
    Top = 1272
    object tblOsnovNaOsiguruvanjeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOsnovNaOsiguruvanjeBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblOsnovNaOsiguruvanjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsnovNaOsiguruvanjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1076#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblOsnovNaOsiguruvanjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblOsnovNaOsiguruvanjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1076#1086#1076#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsnovNaOsiguruvanjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsnovNaOsiguruvanjeTIP_OSNOV: TFIBSmallIntField
      FieldName = 'TIP_OSNOV'
    end
  end
  object dsOsnovNaOsiguruvanje: TDataSource
    DataSet = tblOsnovNaOsiguruvanje
    Left = 104
    Top = 1272
  end
  object qBrOdjavi: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(count(h.id),0) as broj'
      'from hr_prijava_odjava h'
      'where h.mb = :mb and h.tip = 1 and h.po_prijava_id = :prijava')
    Left = 576
    Top = 216
  end
  object qStaroMesto: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select hv.id, hv.mb, hv.id_rm_re, hv.id_re_firma, hv.datum_od, h' +
        'v.datum_do, hv.document_id, hv.dokument'
      'from hr_vraboten_rm hv'
      
        'where hv.id_re_firma = :firma and hv.mb = :mb and (current_date ' +
        'between hv.datum_od and coalesce(hv.datum_do, current_date))')
    Left = 680
    Top = 432
  end
  object qStazPrijava: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select d.datum_od,'
      '       d.staz_godina,'
      
        '       div(datediff(month,d.datum_od,:datumprijava),12) + d.staz' +
        '_godina  stazPrijava'
      'from hr_dogovor_vrabotuvanje d'
      'where d.mb = :mb and d.id_re_firma = :firma'
      
        '      and (current_date between d.datum_od and coalesce(d.datum_' +
        'do, current_date))')
    Left = 576
    Top = 392
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 312
    Top = 280
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle14
      Styles.Caption = cxStyle15
      Styles.CardCaptionRow = cxStyle16
      Styles.CardRowCaption = cxStyle17
      Styles.Content = cxStyle18
      Styles.ContentEven = cxStyle19
      Styles.ContentOdd = cxStyle20
      Styles.FilterBar = cxStyle21
      Styles.Footer = cxStyle22
      Styles.Group = cxStyle23
      Styles.Header = cxStyle24
      Styles.Preview = cxStyle25
      Styles.Selection = cxStyle26
      BuiltIn = True
    end
  end
end
