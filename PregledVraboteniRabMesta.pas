unit PregledVraboteniRabMesta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  cxCheckBox, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxStatusBar, dxRibbonStatusBar,
  dxRibbon, cxGridCustomPopupMenu, cxGridPopupMenu, dxBar, cxBarEditItem, Menus,
  ActnList, dxPSCore, dxPScxCommon, FIBDataSet, pFIBDataSet,
  cxHint, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxBarSkinnedCustForm, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxCustomHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxRibbonCustomizationForm, cxNavigator, dxPScxSchedulerLnk, System.Actions,
  dxPSdxDBOCLnk;

type
  TfrmPregledVraboteniRabMesta = class(TForm)
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ActionList2: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton10: TdxBarLargeButton;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aPregledPoSektori: TAction;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    tblRabMesta: TpFIBDataSet;
    tblRabMestaID_RE: TFIBIntegerField;
    tblRabMestaID: TFIBIntegerField;
    tblRabMestaID_RM: TFIBIntegerField;
    tblRabMestaNAZIV: TFIBStringField;
    dsRabMesta: TDataSource;
    cxGrid1DBTableView2ID: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV: TcxGridDBColumn;
    tblVraboteniVoRM: TpFIBDataSet;
    tblVraboteniVoRMMB: TFIBStringField;
    tblVraboteniVoRMPREZIME: TFIBStringField;
    tblVraboteniVoRMTATKOVO_IME: TFIBStringField;
    tblVraboteniVoRMMOMINSKO_PREZIME: TFIBStringField;
    tblVraboteniVoRMIME: TFIBStringField;
    tblVraboteniVoRMPOL: TFIBIntegerField;
    tblVraboteniVoRMDATUM_RADJANJE: TFIBDateField;
    tblVraboteniVoRMBRACNA_SOSTOJBA: TFIBIntegerField;
    tblVraboteniVoRMSLIKA: TFIBBlobField;
    tblVraboteniVoRMZDR_SOSTOJBA: TFIBStringField;
    tblVraboteniVoRMID_NACIONALNOST: TFIBIntegerField;
    tblVraboteniVoRMID_VEROISPOVED: TFIBIntegerField;
    tblVraboteniVoRMNACIONALNOSNAZIV: TFIBStringField;
    tblVraboteniVoRMVEROISPOVEDNAZIV: TFIBStringField;
    tblVraboteniVoRMRABMESTONAZIV: TFIBStringField;
    tblVraboteniVoRMPolNaziv: TFIBStringField;
    tblVraboteniVoRMBrak: TFIBStringField;
    dsVraboteniVoRM: TDataSource;
    cxGrid1Level3: TcxGridLevel;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView3MB: TcxGridDBColumn;
    aDizajnReport: TAction;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxHintStyleController1: TcxHintStyleController;
    tblVraboteniVoRMRABOTNO_MESTO: TFIBIntegerField;
    tblVraboteniVoRMNAZIV_VRABOTEN: TFIBStringField;
    tblVraboteniVoRMNAZIV_VRABOTEN_TI: TFIBStringField;
    cxGrid1DBTableView3NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView3NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPregledPoSektoriExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView3KeyPress(Sender: TObject; var Key: Char);
    procedure aDizajnReportExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPregledVraboteniRabMesta: TfrmPregledVraboteniRabMesta;

implementation

uses Utils, dmResources, dmUnitOtsustvo, dmReportUnit, dmUnit, dmKonekcija,
  dmMaticni, dmSystem;

{$R *.dfm}

procedure TfrmPregledVraboteniRabMesta.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPregledVraboteniRabMesta.aDizajnReportExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30075;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblRabMesta.Close;
     dm.tblRabMesta.Open;

     dm.tblVraboteniVoRM.Close;
     dm.tblVraboteniVoRM.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
//     dmReport.frxReport1.ShowReport;
end;

procedure TfrmPregledVraboteniRabMesta.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPregledVraboteniRabMesta.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmPregledVraboteniRabMesta.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPregledVraboteniRabMesta.aPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPregledVraboteniRabMesta.aPregledPoSektoriExecute(
  Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30075;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.tblRabMesta.Close;
     dm.tblRabMesta.ParamByName('firma').Value:=dmkon.firma_id;
     dm.tblRabMesta.Open;

     dm.tblVraboteniVoRM.Close;
     dm.tblVraboteniVoRM.ParamByName('firma').Value:=dmKon.firma_id;
     dm.tblVraboteniVoRM.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmPregledVraboteniRabMesta.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmPregledVraboteniRabMesta.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPregledVraboteniRabMesta.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmPregledVraboteniRabMesta.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPregledVraboteniRabMesta.cxGrid1DBTableView2KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPregledVraboteniRabMesta.cxGrid1DBTableView3KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPregledVraboteniRabMesta.FormCreate(Sender: TObject);
begin
     dmOtsustvo.tblPodsektori.Close;
     //dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=IntToStr(firma)+','+'%';
     dmOtsustvo.tblPodsektori.ParamByName('poteklo').Value:=dmKon.re;
     dmOtsustvo.tblPodsektori.Open;

     tblRabMesta.ParamByName('firma').Value:=dmKon.re;
     tblRabMesta.Open;

     tblVraboteniVoRM.ParamByName('firma').Value:=dmKon.re;
     tblVraboteniVoRM.Open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmPregledVraboteniRabMesta.FormShow(Sender: TObject);
begin
     dxBarManager1Bar1.Caption := Caption;
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
     procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

end.
