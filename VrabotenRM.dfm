inherited frmPromenaRM: TfrmPromenaRM
  Caption = #1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
  ClientHeight = 595
  ClientWidth = 1093
  ExplicitWidth = 1109
  ExplicitHeight = 634
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1093
    Height = 234
    ExplicitWidth = 1093
    ExplicitHeight = 234
    inherited cxGrid1: TcxGrid
      Width = 1089
      Height = 230
      ExplicitWidth = 1089
      ExplicitHeight = 230
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsVrabRM
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 78
        end
        object cxGrid1DBTableView1VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 169
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
          Width = 106
        end
        object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Width = 195
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 72
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 71
        end
        object cxGrid1DBTableView1DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'DOKUMENT'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1DOCUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'DOCUMENTNAZIV'
          Width = 99
        end
        object cxGrid1DBTableView1DOCUMENT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'DOCUMENT_ID'
          Width = 103
        end
        object cxGrid1DBTableView1Z_DOKUMENT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'Z_DOKUMENT_ID'
          Width = 128
        end
        object cxGrid1DBTableView1Z_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'Z_DOKUMENT'
          Width = 115
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1Z_DOKUMENT_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'Z_DOKUMENT_DATUM'
          Width = 100
        end
        object cxGrid1DBTableView1PRO_DOKUMENT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'PRO_DOKUMENT_ID'
          Width = 100
        end
        object cxGrid1DBTableView1PRO_DOKUMENT_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'PRO_DOKUMENT_DATUM'
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 360
    Width = 1093
    Height = 212
    ExplicitTop = 360
    ExplicitWidth = 1093
    ExplicitHeight = 212
    inherited Label1: TLabel
      Left = 437
      Top = 4
      Visible = False
      ExplicitLeft = 437
      ExplicitTop = 4
    end
    object Label2: TLabel [1]
      Left = 17
      Top = 17
      Width = 95
      Height = 25
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label3: TLabel [2]
      Left = 17
      Top = 48
      Width = 95
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [3]
      Left = 16
      Top = 142
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 493
      Top = 4
      TabOrder = 4
      Visible = False
      ExplicitLeft = 493
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 1002
      Top = 172
      TabOrder = 12
      ExplicitLeft = 1002
      ExplicitTop = 172
    end
    inherited ZapisiButton: TcxButton
      Left = 921
      Top = 172
      TabOrder = 8
      ExplicitLeft = 921
      ExplicitTop = 172
    end
    object cbRmRe: TcxDBLookupComboBox
      Tag = 1
      Left = 174
      Top = 45
      Hint = #1056#1072#1073#1086#1090#1085#1086' '#1052#1077#1089#1090#1086
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM_RE'
      DataBinding.DataSource = dmSis.dsVrabRM
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV_RM'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dsRMRE
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 275
    end
    object txtRmRE: TcxDBTextEdit
      Tag = 1
      Left = 118
      Top = 45
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1052
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RM_RE'
      DataBinding.DataSource = dmSis.dsVrabRM
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 55
    end
    object cxRadioGroup1: TcxRadioGroup
      Left = 118
      Top = 72
      Caption = '  '#1055#1077#1088#1080#1086#1076' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086'  '
      ParentFont = False
      Properties.Items = <>
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.LookAndFeel.NativeStyle = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      TabOrder = 2
      Height = 54
      Width = 331
    end
    object txtDatumDo: TcxDBDateEdit
      Left = 310
      Top = 92
      Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1075#1072' '#1073#1080#1083' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086
      DataBinding.DataField = 'DATUM_DO'
      DataBinding.DataSource = dmSis.dsVrabRM
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 108
    end
    object txtDatumOd: TcxDBDateEdit
      Tag = 1
      Left = 159
      Top = 92
      Hint = #1044#1072#1090#1091#1084' '#1086#1076' '#1082#1086#1075#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090' '#1077' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086
      DataBinding.DataField = 'DATUM_OD'
      DataBinding.DataSource = dmSis.dsVrabRM
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 112
    end
    object cxLabel1: TcxLabel
      Left = 132
      Top = 93
      Caption = #1054#1076
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TextColor = clRed
      Style.IsFontAssigned = True
    end
    object cxLabel2: TcxLabel
      Left = 286
      Top = 93
      Caption = #1044#1086
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TextColor = clActiveCaption
      Style.IsFontAssigned = True
    end
    object txtMB: TcxTextEdit
      Left = 118
      Top = 21
      Enabled = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.BorderColor = clWindowText
      StyleDisabled.Color = clWindow
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.TextColor = clWindowText
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 7
      Text = 'txtMB'
      Width = 115
    end
    object txtVraboten: TcxTextEdit
      Left = 234
      Top = 21
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      Enabled = False
      StyleDisabled.BorderColor = clWindowText
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 9
      Text = 'txtVraboten'
      Width = 215
    end
    object txtDokument: TcxDBTextEdit
      Left = 118
      Top = 139
      Hint = 
        #1051#1086#1082#1072#1094#1080#1112#1072#1090#1072' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1090' '#1087#1086' '#1082#1086#1112' '#1077' '#1087#1088#1080#1084#1077#1085' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1086#1090' '#1080#1083#1080' '#1084#1091' '#1077' '#1089#1084 +
        #1077#1085#1077#1090#1086' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086' '
      BeepOnEnter = False
      DataBinding.DataField = 'DOCUMENTNAZIV'
      DataBinding.DataSource = dmSis.dsVrabRM
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 331
    end
    object cxButton1: TcxButton
      Left = 455
      Top = 138
      Width = 129
      Height = 21
      Caption = #1053#1072#1112#1076#1080' '#1083#1086#1082#1072#1094#1080#1112#1072
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.NativeStyle = False
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.Data = {
        424D361000000000000036000000280000002000000020000000010020000000
        000000000000C40E0000C40E00000000000000000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DB7300FFDB73
        00FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FBBA62FFFAA639FFE77F
        02FFD36B01FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCA21FFFFCC66FFFFFB2
        4BFFE77F02FFD36B01FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FA9B11FFFBBA
        62FFFAA639FFE77F02FFCB6501FFFFFFFF00D36B01FFD36B01FFD36B01FFFD75
        00FFE77F02FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FA9B
        11FFFBBA62FFE58712FFBE5C00FFBE5C00FFCC7439FFC58776FFDD898AFFDD89
        8AFFDB8A4FFFFD7500FFF58900FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E58712FFBE5C00FFCB6501FFC36E66FFC36E66FFC36E66FFD28A83FFDD89
        8AFFDD898AFFDD898AFFDB8A4FFFF58900FFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00BE5C00FF9C6665FFA77472FFC36E66FF9C6665FFC36E66FFC36E
        66FFC36E66FFDD898AFFDD898AFFF58900FFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00D36B01FF8F5252FF9C6665FFB3836BFF9C6665FF8F5252FFB95447FF9C66
        65FFC36E66FFDD898AFFDD898AFFDB8A4FFFF58900FFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00D36B01FF8F5252FFA77472FFC19A83FF8F5252FF8F5252FF8F5252FF8F52
        52FF9C6665FFC36E66FFDD898AFFDD898AFFE77F02FFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E77F02FF8F5252FFC58776FFD7B5A0FFA77472FF8F5252FF8F5252FFA548
        3DFF8F5252FFC36E66FFC36E66FFDD898AFFFD7500FFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E77F02FF8F5252FFC19A83FFEBD4C1FFDAB9ADFF9C6665FF8F5252FF8F52
        52FF8F5252FFA77472FFC36E66FFD28A83FFDB7300FFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00F58900FF8F5252FF9C6665FFEFDDCAFFFFF6E0FFCCA895FF9C6665FF9C66
        65FF8F5252FFB95447FFC36E66FFCC7439FFD36B01FFFFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00F58900FF8F5252FFB3836BFFEFDDCAFFEFDDCAFFCCA895FFA774
        72FF8F5252FF9C6665FFC36E66FFCB6501FFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00F58900FF9C6665FF8F5252FF9C6665FFC58776FFB3836BFF9C66
        65FF9C6665FF9C6665FFCC7439FFBE5C00FFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00F58900FFFD7500FF9C6665FF9C6665FF8F5252FF8F52
        52FF9C6665FFBE5C00FFBE5C00FFFFF9F1FFFFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E77F02FFDB7300FFD36B01FFCB65
        01FFCB6501FFFFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      TabOrder = 13
      Visible = False
      OnClick = cxButton1Click
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1093
    ExplicitWidth = 1093
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 572
    Width = 1093
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 572
    ExplicitWidth = 1093
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 208
  end
  inherited dxBarManager1: TdxBarManager
    Top = 200
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1056#1052' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    inherited aNov: TAction
      Enabled = False
      Visible = False
    end
    inherited aAzuriraj: TAction
      Enabled = False
    end
    inherited aBrisi: TAction
      Enabled = False
    end
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]'
        '')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      ReportDocument.CreationDate = 40688.577876805560000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 616
    Top = 48
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 712
    Top = 88
  end
  object OpenDialog1: TOpenDialog
    InitialDir = 'C:\'
    Left = 512
    Top = 233
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 715
    Top = 203
  end
  object tblRMRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select hrr.id,'
      '       hrm.naziv naziv_rm'
      'from hr_rm_re hrr'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where  hs.id_re_firma = :firma and hrr.id_re_firma = :firma '
      '-- coalesce(hs.do_datum,'#39'11.11.1111'#39') like :datum and')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 616
    Top = 235
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
end
