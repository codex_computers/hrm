
(***************************************)
{   ������ �������                     }
{                                       }
{   Version   1.1.1.17                  }
{                                       }
{   16.12.2011                          }
{                                       }
(***************************************)

unit PodatociLekari;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit, cxCalendar,
  cxDBEdit, cxGroupBox, dxBar, dxPSCore, dxPScxCommon,  ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ExtCtrls, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxScreenTip, dxCustomHint, cxHint;

type
  TfrmPodatociLekari = class(TfrmMaster)
    cxGroupBox1: TcxGroupBox;
    ���: TcxDBTextEdit;
    Label2: TLabel;
    DatumDo: TcxDBDateEdit;
    Label3: TLabel;
    Label4: TLabel;
    DatumOd: TcxDBDateEdit;
    Faksimil: TcxDBTextEdit;
    Label5: TLabel;
    Label11: TLabel;
    MB_VRABOTEN: TcxDBTextEdit;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1FAKSIMIL: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    procedure FormShow(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPodatociLekari: TfrmPodatociLekari;

implementation

uses dmKonekcija, dmResources, dmUnit;

{$R *.dfm}

procedure TfrmPodatociLekari.aNovExecute(Sender: TObject);
begin
  inherited;
   if tag = 1 then
      begin
       dm.tblPodatociLekariMB.Value:= dm.tblLicaVraboteniMB.Value;
       Faksimil.SetFocus;
      end;
end;

procedure TfrmPodatociLekari.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 1 then
     begin
        dm.tblLica.close;
        dm.tblLica.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;
        dm.tblLica.Open;

        dm.tblPodatociLekari.close;
        dm.tblPodatociLekari.ParamByName('mb').Value:=dm.tblLicaVraboteniMB.Value;
        dm.tblPodatociLekari.Open;
     end
  else
     begin
        dm.tblLica.close;
        dm.tblLica.ParamByName('MB').Value:='%';
        dm.tblLica.Open;

        dm.tblPodatociLekari.close;
        dm.tblPodatociLekari.ParamByName('mb').Value:='%';
        dm.tblPodatociLekari.Open;
     end;
end;

end.
