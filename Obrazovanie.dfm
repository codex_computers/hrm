inherited frmObrazovanie: TfrmObrazovanie
  Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
  ClientHeight = 527
  ClientWidth = 746
  ExplicitWidth = 762
  ExplicitHeight = 565
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 746
    Height = 238
    ExplicitWidth = 746
    ExplicitHeight = 238
    inherited cxGrid1: TcxGrid
      Width = 742
      Height = 234
      ExplicitWidth = 742
      ExplicitHeight = 234
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsObrazovanie
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 54
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 230
        end
        object cxGrid1DBTableView1ID_GRUPA_RM: TcxGridDBColumn
          DataBinding.FieldName = 'ID_GRUPA_RM'
          Visible = False
          Width = 104
        end
        object cxGrid1DBTableView1OZNAKA_GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'OZNAKA_GRUPA'
          Width = 132
        end
        object cxGrid1DBTableView1NAZIV_GRUPA: TcxGridDBColumn
          Caption = #1054#1087#1080#1089'  '#1085#1072' '#1075#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
          DataBinding.FieldName = 'NAZIV_GRUPA'
          Width = 364
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 364
    Width = 746
    Height = 140
    ExplicitTop = 364
    ExplicitWidth = 746
    ExplicitHeight = 140
    inherited Label1: TLabel
      Left = 111
      Top = 32
      Width = 40
      Caption = #1053#1072#1079#1080#1074' :'
      ExplicitLeft = 111
      ExplicitTop = 32
      ExplicitWidth = 40
    end
    object Label2: TLabel [1]
      Left = -6
      Top = 59
      Width = 167
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 167
      Top = 29
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dmSis.dsObrazovanie
      ExplicitLeft = 167
      ExplicitTop = 29
      ExplicitWidth = 402
      Width = 402
    end
    inherited OtkaziButton: TcxButton
      Left = 655
      Top = 100
      TabOrder = 4
      ExplicitLeft = 655
      ExplicitTop = 100
    end
    inherited ZapisiButton: TcxButton
      Left = 574
      Top = 100
      TabOrder = 3
      ExplicitLeft = 574
      ExplicitTop = 100
    end
    object txtStepenSlozenost: TcxDBTextEdit
      Left = 167
      Top = 56
      Hint = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
      DataBinding.DataField = 'ID_GRUPA_RM'
      DataBinding.DataSource = dmSis.dsObrazovanie
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cbStepenSlozenost: TcxDBLookupComboBox
      Left = 248
      Top = 56
      Hint = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'ID_GRUPA_RM'
      DataBinding.DataSource = dmSis.dsObrazovanie
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'OZNAKA'
        end
        item
          Caption = #1054#1087#1080#1089
          Width = 800
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dmSis.dsGrupaRM
      StyleDisabled.TextColor = clBtnText
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 321
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 746
    ExplicitWidth = 746
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 504
    Width = 746
    ExplicitTop = 504
    ExplicitWidth = 746
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    Top = 336
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 224
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40673.634166469910000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 648
    Top = 240
  end
end
