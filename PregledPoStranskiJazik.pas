unit PregledPoStranskiJazik;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxGroupBox, cxRadioGroup, cxImage, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxMemo, ExtDlgs, FIBDataSet, pFIBDataSet,
  cxHint, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip,
  dxCustomHint, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxRibbonCustomizationForm, cxNavigator, dxPScxSchedulerLnk, System.Actions;

type
  TfrmStranskiJazikPregled = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    jazik: TcxLookupComboBox;
    CheckBox1: TCheckBox;
    buttonZapisi: TcxButton;
    ActionList1: TActionList;
    aPregled: TAction;
    aIscistiDatum: TAction;
    aIscistiSttus: TAction;
    ActionList2: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    Action1: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    aPregledPoSektori: TAction;
    aDizajnReport: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dsVraboteniPoJazici: TDataSource;
    tblVraboteniPoJazici: TpFIBDataSet;
    tblVraboteniPoJaziciMB: TFIBStringField;
    tblVraboteniPoJaziciRAZBIRANJE_SLUSANJE: TFIBIntegerField;
    tblVraboteniPoJaziciRAZBIRANJE_CITANJE: TFIBIntegerField;
    tblVraboteniPoJaziciGOVOR_INTERAKCIJA: TFIBIntegerField;
    tblVraboteniPoJaziciGOVOR_PRODUKCIJA: TFIBIntegerField;
    tblVraboteniPoJaziciPISUVANJE: TFIBIntegerField;
    tblVraboteniPoJaziciRABEDINICANAZIV: TFIBStringField;
    tblVraboteniPoJaziciRABMESTONAZIV: TFIBStringField;
    tblVraboteniPoJaziciREID: TFIBIntegerField;
    tblVraboteniPoJaziciRABMESTOID: TFIBIntegerField;
    tblVraboteniPoJaziciRazbiranjeSlusanje: TFIBStringField;
    tblVraboteniPoJaziciRazbiranjeCitanje: TFIBStringField;
    tblVraboteniPoJaziciPisuvanjeNaziv: TFIBStringField;
    tblVraboteniPoJaziciGovorInterakcija: TFIBStringField;
    tblVraboteniPoJaziciGovorProdukcija: TFIBStringField;
    dsJazici: TDataSource;
    Jazici: TpFIBDataSet;
    JaziciID: TFIBIntegerField;
    JaziciNAZIV: TFIBStringField;
    JaziciTS_INS: TFIBDateTimeField;
    JaziciTS_UPD: TFIBDateTimeField;
    JaziciUSR_INS: TFIBStringField;
    JaziciUSR_UPD: TFIBStringField;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2MB: TcxGridDBColumn;
    cxGrid1DBTableView2RABEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2RazbiranjeSlusanje: TcxGridDBColumn;
    cxGrid1DBTableView2RazbiranjeCitanje: TcxGridDBColumn;
    cxGrid1DBTableView2PisuvanjeNaziv: TcxGridDBColumn;
    cxGrid1DBTableView2GovorInterakcija: TcxGridDBColumn;
    cxGrid1DBTableView2GovorProdukcija: TcxGridDBColumn;
    tblVraboteniPoJaziciVID_STRANSKI_JAZIK: TFIBIntegerField;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxHintStyleController1: TcxHintStyleController;
    tblVraboteniPoJaziciNAZIV_VRABOTEN: TFIBStringField;
    tblVraboteniPoJaziciNAZIV_VRABOTEN_TI: TFIBStringField;
    cxGrid1DBTableView2NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    procedure CheckBox1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aPregledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aDizajnReportExecute(Sender: TObject);
    procedure cxGrid1DBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStranskiJazikPregled: TfrmStranskiJazikPregled;

implementation

uses dmSistematizacija, dmUnit, dmKonekcija, dmReportUnit, Utils, dmResources,
  dmSystem;

{$R *.dfm}

procedure TfrmStranskiJazikPregled.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmStranskiJazikPregled.aDizajnReportExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30080;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.Jazici.Close;
     dm.Jazici.ParamByName('ID').Value:='%';
     dm.Jazici.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

     dmReport.frxReport1.DesignReport();
//        dmReport.frxReport1.ShowReport;
end;

procedure TfrmStranskiJazikPregled.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmStranskiJazikPregled.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmStranskiJazikPregled.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmStranskiJazikPregled.aPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmStranskiJazikPregled.aPregledExecute(Sender: TObject);
var RptStream :TStream;
begin
     if (CheckBox1.Checked = false) and (jazik.Text = '')  then
        begin
          ShowMessage('�������� �������� ����� !!!');
          jazik.SetFocus;
        end
     else
        begin
          dmReport.tblReportDizajn2.Close;
          dmReport.tblReportDizajn2.ParamByName('br').Value:=30080;
          dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
          dmReport.tblReportDizajn2.Open;

          if CheckBox1.Checked = true then
            begin
              dm.Jazici.Close;
              dm.Jazici.ParamByName('ID').Value:='%';
              dm.Jazici.Open;
            end
          else
            begin
              dm.Jazici.Close;
              dm.Jazici.ParamByName('ID').Value:=jazik.EditValue;
              dm.Jazici.Open;
            end;

          dm.tblVraboteniPoJazici.ParamByName('firma').Value:=dmkon.re;
          dm.tblVraboteniPoJazici.open;

          RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream) ;

//          dmReport.frxReport1.DesignReport();
        dmReport.frxReport1.ShowReport;
        end;
end;

procedure TfrmStranskiJazikPregled.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView2);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmStranskiJazikPregled.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmStranskiJazikPregled.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmStranskiJazikPregled.CheckBox1Click(Sender: TObject);
begin
     if CheckBox1.Checked = true then
        begin
          GroupBox1.Enabled:=false;
          jazik.Text:= '';
        end
     else
        GroupBox1.Enabled:=true;
end;

procedure TfrmStranskiJazikPregled.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmStranskiJazikPregled.cxGrid1DBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmStranskiJazikPregled.FormCreate(Sender: TObject);
begin
     dm.Jazici.ParamByName('ID').Value:='%';
     dm.Jazici.Open;

     Jazici.Close;
     Jazici.ParamByName('ID').Value:='%';
     Jazici.Open;

     tblVraboteniPoJazici.ParamByName('firma').Value:=dmKon.re;
     tblVraboteniPoJazici.open;

     dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmStranskiJazikPregled.FormShow(Sender: TObject);
begin
 //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView2,false,false);
 //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

end;

end.
