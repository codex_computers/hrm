inherited frmIskustvoRabotno: TfrmIskustvoRabotno
  Caption = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086
  ClientHeight = 720
  ClientWidth = 861
  ExplicitWidth = 877
  ExplicitHeight = 758
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 861
    Height = 234
    ExplicitWidth = 861
    ExplicitHeight = 234
    inherited cxGrid1: TcxGrid
      Width = 857
      Height = 230
      ExplicitWidth = 857
      ExplicitHeight = 230
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsRabotnoIskustvo
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_MOLBA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MOLBA'
          Visible = False
          Width = 109
        end
        object cxGrid1DBTableView1MB_VRABOTEN: TcxGridDBColumn
          Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'.'
          DataBinding.FieldName = 'MB_VRABOTEN'
          Width = 84
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 145
        end
        object cxGrid1DBTableView1Column2: TcxGridDBColumn
          DataBinding.FieldName = 'IMEPREZIME'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
        end
        object cxGrid1DBTableView1PERIOD: TcxGridDBColumn
          DataBinding.FieldName = 'PERIOD'
          Visible = False
        end
        object cxGrid1DBTableView1RABOTODAVAC: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTODAVAC'
          Width = 155
        end
        object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Width = 158
        end
        object cxGrid1DBTableView1VID_DEJNOST: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DEJNOST'
          Width = 167
        end
        object cxGrid1DBTableView1ZADACI: TcxGridDBColumn
          DataBinding.FieldName = 'ZADACI'
          Width = 137
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 360
    Width = 861
    Height = 337
    ExplicitTop = 360
    ExplicitWidth = 861
    ExplicitHeight = 337
    inherited Label1: TLabel
      Left = 474
      Top = 30
      Visible = False
      ExplicitLeft = 474
      ExplicitTop = 30
    end
    object Label10: TLabel [1]
      Left = 78
      Top = 17
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1086#1083#1073#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 35
      Top = 27
      Width = 93
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 530
      Top = 27
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsRabotnoIskustvo
      TabOrder = 1
      Visible = False
      ExplicitLeft = 530
      ExplicitTop = 27
      ExplicitWidth = 96
      Width = 96
    end
    inherited OtkaziButton: TcxButton
      Left = 760
      Top = 281
      TabOrder = 8
      ExplicitLeft = 760
      ExplicitTop = 281
    end
    inherited ZapisiButton: TcxButton
      Left = 679
      Top = 281
      TabOrder = 7
      ExplicitLeft = 679
      ExplicitTop = 281
    end
    object GroupBox1: TGroupBox
      Left = 13
      Top = 116
      Width = 644
      Height = 195
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1054#1087#1080#1089' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086
      TabOrder = 6
      DesignSize = (
        644
        195)
      object Label18: TLabel
        Left = 13
        Top = 148
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072#1076#1072#1095#1080' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 13
        Top = 38
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1088#1072#1073#1086#1090#1086#1076#1072#1074#1072#1095#1086#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 13
        Top = 107
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1080#1076' '#1085#1072' '#1076#1077#1112#1085#1086#1089#1090':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 13
        Top = 66
        Width = 102
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 19
        Top = 25
        Width = 96
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' '#1080' '#1072#1076#1088#1077#1089#1072' '#1085#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ZADACI: TcxDBMemo
        Left = 121
        Top = 145
        Hint = 
          #1043#1083#1072#1074#1085#1080' '#1079#1072#1076#1072#1095#1080' '#1080' '#1086#1076#1075#1086#1074#1086#1088#1085#1086#1089#1090#1080' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072 +
          #1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ZADACI'
        DataBinding.DataSource = dmSis.dsRabotnoIskustvo
        ParentFont = False
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        TabOrder = 3
        OnDblClick = ZADACIDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 504
      end
      object RABOTODAVAC: TcxDBMemo
        Tag = 1
        Left = 121
        Top = 22
        Hint = 
          #1048#1084#1077', '#1072#1076#1088#1077#1089#1072' '#1080' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1088#1072#1073#1086#1090#1086#1076#1072#1074#1072#1095#1086#1090' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' ' +
          #1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'RABOTODAVAC'
        DataBinding.DataSource = dmSis.dsRabotnoIskustvo
        ParentFont = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        TabOrder = 0
        OnDblClick = RABOTODAVACDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 504
      end
      object VID_DEJNOST: TcxDBMemo
        Left = 121
        Top = 104
        Hint = 
          #1042#1080#1076' '#1085#1072' '#1076#1077#1112#1085#1086#1089#1090' '#1080#1083#1080' '#1089#1077#1082#1090#1086#1088' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077 +
          #1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'VID_DEJNOST'
        DataBinding.DataSource = dmSis.dsRabotnoIskustvo
        ParentFont = False
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        TabOrder = 2
        OnDblClick = VID_DEJNOSTDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 504
      end
      object RABOTNO_MESTO: TcxDBMemo
        Left = 121
        Top = 63
        Hint = 
          #1047#1072#1085#1080#1084#1072#1114#1077' '#1080#1083#1080' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090 +
          #1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'RABOTNO_MESTO'
        DataBinding.DataSource = dmSis.dsRabotnoIskustvo
        ParentFont = False
        ParentShowHint = False
        Properties.ScrollBars = ssVertical
        Properties.WantReturns = False
        ShowHint = True
        TabOrder = 1
        OnDblClick = RABOTNO_MESTODblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 35
        Width = 504
      end
    end
    object GroupBox2: TGroupBox
      Left = 85
      Top = 60
      Width = 363
      Height = 50
      Caption = #1055#1077#1088#1080#1086#1076
      TabOrder = 5
      object Label13: TLabel
        Left = 126
        Top = 23
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = -50
        Top = 23
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DATUM_DO: TcxDBDateEdit
        Left = 225
        Top = 20
        Hint = #1044#1086' '#1082#1086#1112' '#1076#1072#1090#1091#1084' '#1073#1080#1083' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dmSis.dsRabotnoIskustvo
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = DATUM_DOExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 49
        Top = 20
        Hint = #1054#1076' '#1082#1086#1112' '#1076#1072#1090#1091#1084' '#1073#1080#1083' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086#1090#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dmSis.dsRabotnoIskustvo
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = DATUM_ODExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object ID_MOLBA: TcxDBTextEdit
      Left = 134
      Top = 14
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1086#1083#1073#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsRabotnoIskustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object IMEPREZIME: TcxDBLookupComboBox
      Left = 231
      Top = 14
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' '#1082#1086#1077' '#1112#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072' '#1084#1086#1083#1073#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MOLBA'
      DataBinding.DataSource = dmSis.dsRabotnoIskustvo
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'ID'
        end
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Width = 800
          FieldName = 'IMEPREZIME'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dmSis.dsMolba
      ShowHint = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 407
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Left = 134
      Top = 33
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsRabotnoIskustvo
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Left = 231
      Top = 33
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB_VRABOTEN'
      DataBinding.DataSource = dmSis.dsRabotnoIskustvo
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      ShowHint = True
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 407
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 861
    ExplicitWidth = 861
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 697
    Width = 861
    ExplicitTop = 697
    ExplicitWidth = 861
  end
  inherited dxBarManager1: TdxBarManager
    Left = 656
    Top = 40
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1080#1089#1082#1091#1089#1090#1074#1086
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40277.658098854170000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
