inherited frmGrupaRM: TfrmGrupaRM
  Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080' '#1084#1077#1089#1090#1072
  ClientHeight = 586
  ClientWidth = 737
  ExplicitWidth = 753
  ExplicitHeight = 624
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 737
    Height = 274
    ExplicitWidth = 737
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Width = 733
      Height = 270
      ExplicitWidth = 733
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsGrupaRM
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 75
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 237
        end
        object cxGrid1DBTableView1OZNAKA: TcxGridDBColumn
          DataBinding.FieldName = 'OZNAKA'
          Width = 147
        end
        object cxGrid1DBTableView1STEPEN_SLOZENOST: TcxGridDBColumn
          DataBinding.FieldName = 'STEPEN_SLOZENOST'
          Width = 148
        end
        object cxGrid1DBTableView1STEPEN_NA_SLOZENOST_2: TcxGridDBColumn
          DataBinding.FieldName = 'STEPEN_NA_SLOZENOST_2'
          Width = 124
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Width = 737
    Height = 163
    ExplicitTop = 400
    ExplicitWidth = 737
    ExplicitHeight = 163
    inherited Label1: TLabel
      Top = 9
      Visible = False
      ExplicitTop = 9
    end
    object Label2: TLabel [1]
      Left = 66
      Top = 28
      Width = 83
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [2]
      Left = 66
      Top = 55
      Width = 83
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1079#1085#1072#1082#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsGrupaRM
      TabOrder = 4
      Visible = False
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 646
      Top = 123
      TabOrder = 5
      ExplicitLeft = 646
      ExplicitTop = 123
    end
    inherited ZapisiButton: TcxButton
      Left = 565
      Top = 123
      TabOrder = 3
      ExplicitLeft = 565
      ExplicitTop = 123
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 155
      Top = 25
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsGrupaRM
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 400
    end
    object Oznaka: TcxDBTextEdit
      Left = 155
      Top = 52
      Hint = #1054#1079#1085#1072#1082#1072'/'#1075#1088#1091#1087#1072' '#1079#1072' '#1089#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1072' '#1075#1088#1091#1087#1072#1090#1072' '#1056#1052
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'OZNAKA'
      DataBinding.DataSource = dmSis.dsGrupaRM
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 400
    end
    object cxGroupBox1: TcxGroupBox
      Left = 155
      Top = 79
      Hint = 
        #1057#1090#1077#1087#1077#1085#1086#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1084#1086#1078#1077' '#1076#1072' '#1089#1077' '#1080#1079#1088#1072#1079#1080' '#1074#1086' '#1080#1085#1090#1077#1088#1074#1072#1083' '#1080#1083#1080' '#1089#1072#1084#1086' '#1076#1072' ' +
        #1089#1077' '#1087#1086#1087#1086#1083#1085#1080' '#1089#1072#1084#1086' '#1087#1088#1074#1086#1090#1086' '#1087#1086#1083#1077
      Caption = #1057#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090'/'#1082#1086#1077#1092#1080#1094#1080#1077#1085#1090
      TabOrder = 2
      Height = 66
      Width = 362
      object Label13: TLabel
        Left = 175
        Top = 31
        Width = 35
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 16
        Top = 31
        Width = 35
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtStSlozenost: TcxDBTextEdit
        Left = 57
        Top = 28
        Hint = 
          #1055#1088#1074' '#1076#1077#1083' '#1086#1076' '#1080#1085#1090#1077#1088#1074#1072#1083#1086#1090'. '#1044#1086#1082#1086#1083#1082#1091' '#1089#1090#1077#1087#1077#1085#1086#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1077' '#1089#1077' '#1080#1079#1088#1072#1079 +
          #1091#1074#1072' '#1074#1086' '#1080#1085#1090#1077#1088#1074#1072#1083#1080' '#1089#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1089#1072#1084#1086' '#1086#1074#1072' '#1087#1086#1083#1077
        BeepOnEnter = False
        DataBinding.DataField = 'STEPEN_SLOZENOST'
        DataBinding.DataSource = dmSis.dsGrupaRM
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 112
      end
      object txtstslozenost2: TcxDBTextEdit
        Left = 216
        Top = 28
        Hint = 
          #1042#1090#1086#1088' '#1076#1077#1083' '#1086#1076' '#1080#1085#1090#1077#1088#1074#1072#1083#1086#1090'. '#1044#1086#1082#1086#1083#1082#1091' '#1089#1090#1077#1087#1077#1085#1086#1090' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090' '#1085#1077' '#1089#1077' '#1080#1079#1088#1072 +
          #1079#1091#1074#1072' '#1074#1086' '#1080#1085#1090#1077#1088#1074#1072#1083#1080' '#1089#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1089#1072#1084#1086' '#1087#1088#1074#1080#1086#1090' '#1076#1077#1083
        BeepOnEnter = False
        DataBinding.DataField = 'STEPEN_NA_SLOZENOST_2'
        DataBinding.DataSource = dmSis.dsGrupaRM
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 112
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 737
    ExplicitWidth = 737
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 563
    Width = 737
    ExplicitTop = 563
    ExplicitWidth = 737
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1043#1088#1091#1087#1072' '#1085#1072' '#1056#1052
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.PageHeader.RightTitle.Strings = (
        '[Date Printed]')
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40905.422138715280000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    Left = 648
    Top = 240
  end
end
