inherited frmDogSezonskoRabotenje: TfrmDogSezonskoRabotenje
  Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1089#1077#1079#1086#1085#1089#1082#1086' '#1088#1072#1073#1086#1090#1077#1114#1077
  ClientHeight = 652
  ClientWidth = 794
  ExplicitWidth = 810
  ExplicitHeight = 690
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 794
    ExplicitWidth = 794
    inherited cxGrid1: TcxGrid
      Width = 790
      ExplicitWidth = 790
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsDogSezonci
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 87
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 80
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Width = 100
        end
        object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
          DataBinding.FieldName = 'TATKOVO_IME'
          Width = 100
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Width = 100
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 250
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 72
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 74
        end
        object cxGrid1DBTableView1RABOTNO_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNO_MESTO'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABMESTONAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1FIRMANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'FIRMANAZIV'
          Width = 100
        end
        object cxGrid1DBTableView1RAKOVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'RAKOVODITEL'
          Width = 100
        end
        object cxGrid1DBTableView1PLATA: TcxGridDBColumn
          DataBinding.FieldName = 'PLATA'
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 794
    Height = 253
    ExplicitWidth = 794
    ExplicitHeight = 253
    inherited Label1: TLabel
      Left = 93
      Top = 203
      Visible = False
      ExplicitLeft = 93
      ExplicitTop = 203
    end
    object Label10: TLabel [1]
      Left = 63
      Top = 29
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 149
      Top = 200
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsDogSezonci
      TabOrder = 5
      Visible = False
      ExplicitLeft = 149
      ExplicitTop = 200
    end
    inherited OtkaziButton: TcxButton
      Left = 694
      Top = 206
      TabOrder = 6
      OnKeyDown = EnterKakoTab
      ExplicitLeft = 694
      ExplicitTop = 206
    end
    inherited ZapisiButton: TcxButton
      Left = 613
      Top = 206
      TabOrder = 4
      OnKeyDown = EnterKakoTab
      ExplicitLeft = 613
      ExplicitTop = 206
    end
    object Period: TcxGroupBox
      Left = 287
      Top = 133
      Caption = #1055#1077#1088#1080#1086#1076
      TabOrder = 3
      Height = 57
      Width = 482
      object Label6: TLabel
        Left = 40
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 232
        Top = 25
        Width = 41
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 87
        Top = 22
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsDogSezonci
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 279
        Top = 22
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsDogSezonci
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object Broj: TcxDBTextEdit
      Left = 119
      Top = 26
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsDogSezonci
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 138
    end
    object PodatociZaRabotnik: TcxGroupBox
      Left = 16
      Top = 53
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1073#1086#1090#1085#1080#1082
      TabOrder = 1
      Height = 138
      Width = 257
      object Label5: TLabel
        Left = 13
        Top = 107
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 13
        Top = 80
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 13
        Top = 53
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 13
        Top = 26
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 104
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsDogSezonci
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 138
      end
      object TATKOVO_IME: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 77
        BeepOnEnter = False
        DataBinding.DataField = 'TATKOVO_IME'
        DataBinding.DataSource = dm.dsDogSezonci
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 138
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 50
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsDogSezonci
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 138
      end
      object MB: TcxDBTextEdit
        Tag = 1
        Left = 103
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'MB'
        DataBinding.DataSource = dm.dsDogSezonci
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 138
      end
    end
    object PodatociZaVrabotuvanje: TcxGroupBox
      Left = 287
      Top = 21
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      TabOrder = 2
      Height = 110
      Width = 482
      object Label14: TLabel
        Left = 22
        Top = 26
        Width = 94
        Height = 17
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label9: TLabel
        Left = 22
        Top = 53
        Width = 94
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label8: TLabel
        Left = 70
        Top = 80
        Width = 46
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1083#1072#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object RABOTNO_MESTO: TcxDBTextEdit
        Tag = 1
        Left = 122
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'RABOTNO_MESTO'
        DataBinding.DataSource = dm.dsDogSezonci
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 48
      end
      object RABOTNO_MESTO_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 170
        Top = 23
        BeepOnEnter = False
        DataBinding.DataField = 'RABOTNO_MESTO'
        DataBinding.DataSource = dm.dsDogSezonci
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'ID'
          end
          item
            Width = 800
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dmSis.dsRabotnoMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 271
      end
      object MB_DIREKTOR: TcxDBTextEdit
        Left = 122
        Top = 50
        BeepOnEnter = False
        DataBinding.DataField = 'RAKOVODITEL'
        DataBinding.DataSource = dm.dsDogSezonci
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 319
      end
      object PLATA: TcxDBTextEdit
        Left = 122
        Top = 77
        BeepOnEnter = False
        DataBinding.DataField = 'PLATA'
        DataBinding.DataSource = dm.dsDogSezonci
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 124
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 794
    ExplicitWidth = 794
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 629
    Width = 794
    ExplicitTop = 629
    ExplicitWidth = 794
  end
  inherited dxBarManager1: TdxBarManager
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 263
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 438
      FloatClientWidth = 120
      FloatClientHeight = 152
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 645
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
    object dxBarManager1Bar5: TdxBar [4]
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem6'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = aFilterAktivni
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem2: TcxBarEditItem
      Action = aFilterAktivni
      Category = 0
      Width = 100
      PropertiesClassName = 'TcxCheckGroupProperties'
      Properties.Items = <>
    end
    object cxBarEditItem3: TcxBarEditItem
      Action = afilterSite
      Category = 0
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object cxBarEditItem4: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckComboBoxProperties'
      Properties.Items = <>
    end
    object cxBarEditItem5: TcxBarEditItem
      Action = aFilterAktivni
      Category = 0
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ValueChecked = 'False'
    end
    object cxBarEditItem6: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1080#1090#1077' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 1
        end
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080' '#1076#1086#1075#1086#1074#1086#1088#1080
          Value = 0
        end>
      Properties.OnChange = cxBarEditItem6PropertiesChange
      InternalEditValue = 1
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  inherited ActionList1: TActionList
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    object aFilterAktivni: TAction
      Caption = #1040#1082#1090#1080#1074#1085#1080' '#1076#1086#1075#1086#1074#1086#1088#1080
    end
    object afilterSite: TAction
      Caption = #1057#1080#1090#1077' '#1076#1086#1075#1086#1074#1086#1088#1080
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40490.468232731480000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 640
    Top = 152
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 872
    Top = 184
  end
end
