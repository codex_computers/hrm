inherited frmTipKS: TfrmTipKS
  Caption = #1058#1080#1087' '#1085#1072' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1089#1077#1083#1077#1082#1094#1080#1112#1072
  ClientHeight = 494
  ClientWidth = 602
  ExplicitWidth = 610
  ExplicitHeight = 528
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 231
    ExplicitWidth = 602
    ExplicitHeight = 241
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 237
      ExplicitWidth = 598
      ExplicitHeight = 237
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dmSis.dsTipKS
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 109
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 298
        end
        object cxGrid1DBTableView1MAX_POENI: TcxGridDBColumn
          DataBinding.FieldName = 'MAX_POENI'
          Width = 188
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 357
    Width = 602
    Height = 114
    ExplicitTop = 357
    ExplicitWidth = 602
    ExplicitHeight = 114
    inherited Label1: TLabel
      Left = 397
      Top = 30
      Visible = False
      ExplicitLeft = 397
      ExplicitTop = 30
    end
    object Label2: TLabel [1]
      Left = 77
      Top = 32
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 2
      Top = 60
      Width = 125
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1085#1080' '#1087#1086#1077#1085#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 445
      Top = 27
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsTipKS
      TabOrder = 3
      Visible = False
      ExplicitLeft = 445
      ExplicitTop = 27
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 74
      TabOrder = 4
      ExplicitLeft = 511
      ExplicitTop = 74
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 74
      TabOrder = 2
      ExplicitLeft = 430
      ExplicitTop = 74
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 133
      Top = 29
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsTipKS
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 276
    end
    object txtMaxPoeni: TcxDBSpinEdit
      Tag = 1
      Left = 133
      Top = 56
      DataBinding.DataField = 'MAX_POENI'
      DataBinding.DataSource = dmSis.dsTipKS
      TabOrder = 1
      Width = 100
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 602
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 471
    Width = 602
    ExplicitTop = 471
    ExplicitWidth = 602
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40303.596335289350000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
