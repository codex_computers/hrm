object frmPregledVraboteniRabMesta: TfrmPregledVraboteniRabMesta
  Left = 0
  Top = 0
  Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
  ClientHeight = 657
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 688
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 126
    Width = 688
    Height = 508
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmOtsustvo.dsPodsektori
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      FilterRow.ApplyChanges = fracImmediately
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 82
      end
      object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
        Caption = #1057#1077#1082#1090#1086#1088
        DataBinding.FieldName = 'NAZIV'
        Width = 566
      end
    end
    object cxGrid1DBTableView2: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView2KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsRabMesta
      DataController.DetailKeyFieldNames = 'ID_RE'
      DataController.KeyFieldNames = 'ID'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView2ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 79
      end
      object cxGrid1DBTableView2NAZIV: TcxGridDBColumn
        Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        DataBinding.FieldName = 'NAZIV'
        Width = 352
      end
    end
    object cxGrid1DBTableView3: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView3KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsVraboteniVoRM
      DataController.DetailKeyFieldNames = 'RABOTNO_MESTO'
      DataController.KeyFieldNames = 'MB'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView3MB: TcxGridDBColumn
        DataBinding.FieldName = 'MB'
        Width = 99
      end
      object cxGrid1DBTableView3NAZIV_VRABOTEN: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV_VRABOTEN'
        Visible = False
        Width = 250
      end
      object cxGrid1DBTableView3NAZIV_VRABOTEN_TI: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
        Width = 250
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
      object cxGrid1Level2: TcxGridLevel
        GridView = cxGrid1DBTableView2
        object cxGrid1Level3: TcxGridLevel
          GridView = cxGrid1DBTableView3
        end
      end
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 634
    Width = 688
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F10 - '#1055#1088#1077#1075#1083#1077#1076
        Width = 80
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090', Shift+Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Exce' +
          'l'
        Width = 330
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 544
    Top = 72
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44454.614129108800000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object ActionList2: TActionList
    Images = dmRes.cxSmallImages
    Left = 600
    Top = 272
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
    end
    object aPregledPoSektori: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
      ShortCut = 121
      OnExecute = aPregledPoSektoriExecute
    end
    object aDizajnReport: TAction
      Caption = 'aDizajnReport'
      SecondaryShortCuts.Strings = (
        'Shift+Ctrl+F10')
      OnExecute = aDizajnReportExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 472
    Top = 56
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 472
    Top = 136
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 64
      FloatClientHeight = 156
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 757
      FloatTop = 276
      FloatClientWidth = 111
      FloatClientHeight = 126
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 354
      DockedTop = 0
      FloatLeft = 656
      FloatTop = 374
      FloatClientWidth = 51
      FloatClientHeight = 104
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 477
      FloatTop = 184
      FloatClientWidth = 133
      FloatClientHeight = 208
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1088#1077#1075#1083#1077#1076
      CaptionButtons = <>
      DockedLeft = 207
      DockedTop = 0
      FloatLeft = 657
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPregledPoSektori
      Category = 0
      LargeImageIndex = 19
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 488
    Top = 16
  end
  object tblRabMesta: TpFIBDataSet
    SelectSQL.Strings = (
      'select hrm.id_re,'
      '       hrm.id,'
      '       hrm.id_rm,'
      '       hrMesto.naziv'
      'from hr_rm_re hrm'
      
        'inner join hr_rabotno_mesto hrMesto on hrMesto.id = hrm.id_rm an' +
        'd hrMesto.id_re_firma = hrm.id_re_firma'
      
        'inner join hr_sistematizacija hs on hs.id = hrm.id_sistematizaci' +
        'ja and hs.id_re_firma = hrm.id_re_firma'
      'where  hrm.id_re_firma = :firma'
      'order by hrm.id_re, hrm.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 104
    Top = 328
    object tblRabMestaID_RE: TFIBIntegerField
      FieldName = 'ID_RE'
    end
    object tblRabMestaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRabMestaID_RM: TFIBIntegerField
      FieldName = 'ID_RM'
    end
    object tblRabMestaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRabMesta: TDataSource
    DataSet = tblRabMesta
    Left = 176
    Top = 328
  end
  object tblVraboteniVoRM: TpFIBDataSet
    SelectSQL.Strings = (
      'select hrv.mb,'
      '       hrv.rabotno_mesto,'
      '       hrv.prezime,'
      '       hrv.tatkovo_ime,'
      '       hrv.mominsko_prezime,'
      '       hrv.ime,'
      '       hrv.pol,'
      '       hrv.datum_radjanje,'
      '       hrv.bracna_sostojba,'
      '       hrv.slika,'
      '       hrv.zdr_sostojba,'
      '       hrv.id_nacionalnost,'
      '       hrv.id_veroispoved,'
      '       hrnn.naziv as nacionalnosNaziv,'
      '       hrve.naziv as veroispovedNaziv,'
      '       hrv.rabotnomestonaziv as rabMestoNaziv,'
      '       hrv.naziv_vraboten, hrv.naziv_vraboten_ti,'
      '       case hrv.pol when '#39'1'#39' then '#39#1052#1072#1096#1082#1080#39
      '                        when '#39'2'#39' then '#39#1046#1077#1085#1089#1082#1080#39
      '       end "PolNaziv",'
      '       case hrv.bracna_sostojba when '#39'1'#39' then '#39#1044#1072#39
      '                        when '#39'0'#39' then '#39#1053#1077#39
      '       end "Brak"'
      'from view_hr_vraboteni hrv'
      
        'left outer join hr_nacionalnost hrnn on hrnn.id = hrv.id_naciona' +
        'lnost'
      
        'left outer join hr_veroispoved hrve on hrve.id = hrv.id_veroispo' +
        'ved'
      'WHERE hrv.ID_RE_FIRMA = :FIRMA    '
      ''
      'order by hrv.rabotno_mesto')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsRabMesta
    Left = 352
    Top = 328
    object tblVraboteniVoRMMB: TFIBStringField
      DisplayLabel = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      FieldName = 'MB'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMPREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMTATKOVO_IME: TFIBStringField
      FieldName = 'TATKOVO_IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMMOMINSKO_PREZIME: TFIBStringField
      FieldName = 'MOMINSKO_PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMIME: TFIBStringField
      FieldName = 'IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMPOL: TFIBIntegerField
      FieldName = 'POL'
    end
    object tblVraboteniVoRMDATUM_RADJANJE: TFIBDateField
      FieldName = 'DATUM_RADJANJE'
    end
    object tblVraboteniVoRMBRACNA_SOSTOJBA: TFIBIntegerField
      FieldName = 'BRACNA_SOSTOJBA'
    end
    object tblVraboteniVoRMSLIKA: TFIBBlobField
      FieldName = 'SLIKA'
      Size = 8
    end
    object tblVraboteniVoRMZDR_SOSTOJBA: TFIBStringField
      FieldName = 'ZDR_SOSTOJBA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMID_NACIONALNOST: TFIBIntegerField
      FieldName = 'ID_NACIONALNOST'
    end
    object tblVraboteniVoRMID_VEROISPOVED: TFIBIntegerField
      FieldName = 'ID_VEROISPOVED'
    end
    object tblVraboteniVoRMNACIONALNOSNAZIV: TFIBStringField
      FieldName = 'NACIONALNOSNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMVEROISPOVEDNAZIV: TFIBStringField
      FieldName = 'VEROISPOVEDNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMRABMESTONAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      FieldName = 'RABMESTONAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMPolNaziv: TFIBStringField
      FieldName = 'PolNaziv'
      Size = 6
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMBrak: TFIBStringField
      FieldName = 'Brak'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMRABOTNO_MESTO: TFIBIntegerField
      FieldName = 'RABOTNO_MESTO'
    end
    object tblVraboteniVoRMNAZIV_VRABOTEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVraboteniVoRMNAZIV_VRABOTEN_TI: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      FieldName = 'NAZIV_VRABOTEN_TI'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsVraboteniVoRM: TDataSource
    DataSet = tblVraboteniVoRM
    Left = 440
    Top = 328
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
