unit PovredaNaRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon,  ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxCalendar, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, cxRadioGroup,
  cxMemo, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxSchedulerLnk, dxPScxPivotGridLnk, dxScreenTip, dxCustomHint, cxHint;

type
  TfrmPovredaNaRM = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_POVREDA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_POVREDA: TcxGridDBColumn;
    cxGrid1DBTableView1SMRTONOSNA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_OSNOV_OSIGURUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_POVREDENI: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_ZAGINATI: TcxGridDBColumn;
    cxGrid1DBTableView1KOLKU_PATI: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1ZAGROZENO_RM: TcxGridDBColumn;
    cxGrid1DBTableView1NAREDBA: TcxGridDBColumn;
    cxGrid1DBTableView1KORISTENO: TcxGridDBColumn;
    cxGrid1DBTableView1MERKI: TcxGridDBColumn;
    cxGrid1DBTableView1BEZBEDNOST: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1TIPPOVREDANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POVREDAOPIS: TcxGridDBColumn;
    cxGrid1DBTableView1OSNOVNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VRABOTENNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1SmrtoNosnaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1NaredbaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1KoristenoNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1bezbednostNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1merkiNaziv: TcxGridDBColumn;
    Label11: TLabel;
    MB_VRABOTEN: TcxDBTextEdit;
    VRABOTENNAZIV: TcxDBLookupComboBox;
    DATUM: TcxDBDateEdit;
    Label12: TLabel;
    Label2: TLabel;
    TIP_POVREDA: TcxDBTextEdit;
    TIP_POVREDA_NAziv: TcxDBLookupComboBox;
    Label3: TLabel;
    ID_POVREDA: TcxDBTextEdit;
    ID_POVREDA_naziv: TcxDBLookupComboBox;
    cxDBRadioGroup1: TcxDBRadioGroup;
    Label4: TLabel;
    ID_OSNOV_OSIGURUVANJE: TcxDBTextEdit;
    ID_OSNOV_OSIGURUVANJE_naziv: TcxDBLookupComboBox;
    Label5: TLabel;
    BROJ_POVREDENI: TcxDBTextEdit;
    Label6: TLabel;
    BROJ_ZAGINATI: TcxDBTextEdit;
    Label7: TLabel;
    KOLKU_PATI: TcxDBTextEdit;
    OPIS: TcxDBMemo;
    Label8: TLabel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxDBRadioGroup2: TcxDBRadioGroup;
    cxDBRadioGroup3: TcxDBRadioGroup;
    cxDBRadioGroup4: TcxDBRadioGroup;
    cxDBRadioGroup5: TcxDBRadioGroup;
    cxDBRadioGroup6: TcxDBRadioGroup;
    Label9: TLabel;
    Label10: TLabel;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TIP_POVREDA_NAzivExit(Sender: TObject);
    procedure TIP_POVREDAExit(Sender: TObject);
    procedure OPISDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPovredaNaRM: TfrmPovredaNaRM;

implementation

uses dmUnit, Utils, Notepad, dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmPovredaNaRM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.tblPovreda.close;
end;

procedure TfrmPovredaNaRM.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblPovredaRM.close;
  dm.tblPovredaRM.ParamByName('Re').Value:='%';
  if tag = 1 then
     begin
       dxBarManager1Bar1.Visible:=false;
       dm.tblPovredaRM.ParamByName('MB').Value:=dm.tblLicaVraboteniMB.Value;;
     end
  else
      dm.tblPovredaRM.ParamByName('MB').Value:='%';
  dm.tblPovredaRM.ParamByName('firma').Value:=firma;
  dm.tblPovredaRM.open;
  dm.tblTipPovreda.Open;
  dm.tblPovreda.ParamByName('tip').Value:='%';
  dm.tblPovreda.Open;
  dm.tblOsnovNaOsiguruvanje.open;
end;

procedure TfrmPovredaNaRM.OPISDblClick(Sender: TObject);
begin
   frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblPovredaRMOPIS.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
       begin
	       dm.tblPovredaRMOPIS.Value := frmNotepad.ReturnText;
       end;
  frmNotepad.Free;

end;

procedure TfrmPovredaNaRM.TIP_POVREDAExit(Sender: TObject);
begin
  TEdit(Sender).Color:=clWhite;
  dm.tblPovreda.ParamByName('tip').Value:=TIP_POVREDA_NAziv.EditValue;
  dm.tblPovreda.FullRefresh;
end;

procedure TfrmPovredaNaRM.TIP_POVREDA_NAzivExit(Sender: TObject);
begin
  TEdit(Sender).Color:=clWhite;
  dm.tblPovreda.ParamByName('tip').Value:=TIP_POVREDA_NAziv.EditValue;
  dm.tblPovreda.FullRefresh;
end;

end.
