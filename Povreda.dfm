inherited frmPovreda: TfrmPovreda
  Caption = #1055#1086#1074#1088#1077#1076#1072
  ExplicitWidth = 749
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    inherited cxGrid1: TcxGrid
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPovreda
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1TIP_POVREDA: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_POVREDA'
          Width = 59
        end
        object cxGrid1DBTableView1POVREDANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'POVREDANAZIV'
          SortIndex = 0
          SortOrder = soAscending
          Width = 213
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 362
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    inherited Label1: TLabel
      Left = 21
      Top = 7
      Visible = False
      ExplicitLeft = 21
      ExplicitTop = 7
    end
    object Label5: TLabel [1]
      Left = 11
      Top = 30
      Width = 99
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1058#1080#1087' '#1085#1072' '#1087#1086#1074#1088#1077#1076#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [2]
      Left = 11
      Top = 54
      Width = 99
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 77
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPovreda
      TabOrder = 2
      Visible = False
      ExplicitLeft = 77
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      TabOrder = 5
    end
    inherited ZapisiButton: TcxButton
      TabOrder = 4
    end
    object TIP_POVREDA: TcxDBTextEdit
      Tag = 1
      Left = 116
      Top = 27
      Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      BeepOnEnter = False
      DataBinding.DataField = 'TIP_POVREDA'
      DataBinding.DataSource = dm.dsPovreda
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 58
    end
    object TIP_POVREDAnaziv: TcxDBLookupComboBox
      Tag = 1
      Left = 175
      Top = 27
      Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'TIP_POVREDA'
      DataBinding.DataSource = dm.dsPovreda
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsTipPovreda
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 346
    end
    object OPIS: TcxDBMemo
      Tag = 1
      Left = 116
      Top = 54
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsPovreda
      Properties.WantReturns = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 59
      Width = 405
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40456.566280428240000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
