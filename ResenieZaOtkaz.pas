unit ResenieZaOtkaz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxBar, dxPSCore, dxPScxCommon,  ActnList,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMemo, FIBQuery,
  pFIBQuery, cxCalendar, cxGroupBox, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, FIBDataSet, pFIBDataSet, cxBlobEdit, frxDesgn,
  frxClass, frxDBSet, frxRich, frxCross, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk,
  dxPSdxDBOCLnk, dxScreenTip, dxCustomHint, cxHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinOffice2013White, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, cxRadioGroup;

type
  TfrmResenieOtkaz = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1OBRAZLOZENIE: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINA: TcxGridDBColumn;
    cxGrid1DBTableView1DIREKTOR: TcxGridDBColumn;
    cxGrid1DBTableView1LICENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label8: TLabel;
    MB: TcxDBTextEdit;
    VRABOTENIME: TcxDBLookupComboBox;
    cxGroupBox1: TcxGroupBox;
    Label2: TLabel;
    Label6: TLabel;
    staro_rm: TcxDBTextEdit;
    ID_RM_RE_NAZIV: TcxDBLookupComboBox;
    staro_do: TcxDBDateEdit;
    qStaroMesto: TpFIBQuery;
    cxGroupBox2: TcxGroupBox;
    Label11: TLabel;
    Odobreno_od: TcxDBTextEdit;
    Label3: TLabel;
    PRICINA: TcxDBTextEdit;
    PRICINA_opis: TcxDBLookupComboBox;
    Label4: TLabel;
    OBRAZLOZENIE: TcxDBMemo;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMRENAZIV_RM: TFIBStringField;
    dsRMRE: TDataSource;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    Lica: TcxDBLookupComboBox;
    dxBarLargeButton10: TdxBarLargeButton;
    aPregled: TAction;
    aDesignReport: TAction;
    dxBarManager1Bar5: TdxBar;
    Label5: TLabel;
    Broj: TcxDBTextEdit;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1DATA: TcxGridDBColumn;
    dxBarLargeButton11: TdxBarLargeButton;
    aBrisiDokument: TAction;
    dxBarManager1Bar6: TdxBar;
    ComboBoxGodina: TdxBarCombo;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    qID_VRABOTEN_RM: TpFIBQuery;
    pFIBQuery1: TpFIBQuery;
    qPROMENA_ID: TpFIBQuery;
    DatumKreiranje: TcxDBDateEdit;
    Label7: TLabel;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    dxbrlrgbtn1: TdxBarLargeButton;
    dxbrlrgbtn2: TdxBarLargeButton;
    aIstorijaVraboten: TAction;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure OBRAZLOZENIEDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aPregledExecute(Sender: TObject);
    procedure aDesignReportExecute(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure ComboBoxGodinaChange(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aIstorijaVrabotenExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmResenieOtkaz: TfrmResenieOtkaz;

implementation

uses dmKonekcija, dmUnit, Notepad, dmUnitOtsustvo, Utils, dmReportUnit, DaNe, VraboteniHRM;

{$R *.dfm}

procedure TfrmResenieOtkaz.aAzurirajExecute(Sender: TObject);
begin
  inherited;
  tblRMRE.ParamByName('firma').Value:=firma;
  tblRMRE.FullRefresh;
  VRABOTENIME.Visible:=true;
  lica.Visible:=false;
end;

procedure TfrmResenieOtkaz.aBrisiDokumentExecute(Sender: TObject);
begin
     frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmOtsustvo.qDeleteResenieOtkaz.Close;
            dmOtsustvo.qDeleteResenieOtkaz.ParamByName('id').Value:=dm.tblResenieOtkazID.Value;
            dmOtsustvo.qDeleteResenieOtkaz.ExecQuery;
        end;
end;

procedure TfrmResenieOtkaz.aBrisiExecute(Sender: TObject);
begin
   if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
      begin
         {qPROMENA_ID.close;
         qPROMENA_ID.ParamByName('id').Value:=dm.tblResenieOtkazID.Value;
         qPROMENA_ID.ExecQuery;

         dm.qUpdateVrabotenRM_ID.Close;
         dm.qUpdateVrabotenRM_ID.paramByName('id').Value:=qPROMENA_ID.FldByName['ID_PROMENA'].Value;
         dm.qUpdateVrabotenRM_ID.ExecQuery;

         cxGrid1DBTableView1.DataController.DataSet.Delete(); }
         frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
            Abort
         else
           begin
              dmOtsustvo.insert(dm.pZatvoriRM, 'MB', 'FIRMA', 'DATUMDO', 'DOKUMENT_ID', 'TIP', Null, Null, Null,
                                           mb.Text,dmKon.re, staro_do.Date,dm.tblResenieOtkazID.Value, 2, Null, Null, Null);
              dm.tblResenieOtkaz.FullRefresh;
              dm.viewVraboteni.close;
              dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
              dm.viewVraboteni.ParamByName('mb').Value:='%';
              dm.viewVraboteni.ParamByName('re').Value:='%';
              dm.viewVraboteni.Open;
           end;

      end;
end;

procedure TfrmResenieOtkaz.aDesignReportExecute(Sender: TObject);
begin
    dmReport.Spremi(30102,dmKon.aplikacija);
    dmReport.tblSqlReport.ParamByName('id').Asinteger:=dm.tblResenieOtkazID.Value;
    dmReport.tblSqlReport.Open;

//    dmReport.frxReport1.ShowReport();
dmReport.frxReport1.DesignReport();

end;

procedure TfrmResenieOtkaz.aHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpContext(124);
end;

procedure TfrmResenieOtkaz.aIstorijaVrabotenExecute(Sender: TObject);
begin
  inherited;
     frmHRMVraboteni:=TfrmHRMVraboteni.Create(Self,false);
     frmHRMVraboteni.Tag:=3;
     frmHRMVraboteni.ShowModal;
     frmHRMVraboteni.Free;
end;

procedure TfrmResenieOtkaz.aNovExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    VRABOTENIME.Visible:=True;
    lica.Visible:=false;
    tblRMRE.ParamByName('firma').Value:=dmKon.re;
    tblRMRE.FullRefresh;
    Broj.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblResenieOtkazID_RE_FIRMA.Value:=dmKon.re;
    dm.tblResenieOtkazVID_DOKUMENT.Value:=resPrekinRabOdnos;
    dm.tblResenieOtkazDATUM.Value:=Now;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmResenieOtkaz.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  tblRMRE.ParamByName('firma').Value:=firma;
  tblRMRE.FullRefresh;
  VRABOTENIME.Visible:=False;
  lica.Visible:=True;
end;

procedure TfrmResenieOtkaz.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + ComboBoxGodina.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmResenieOtkaz.aPregledExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.PrestanokNaRabotenOdnos.close;
  dmReport.PrestanokNaRabotenOdnos.ParamByName('id').Value:=dm.tblResenieOtkazID.Value;
  dmReport.PrestanokNaRabotenOdnos.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dmReport.PrestanokNaRabotenOdnosDATA.IsNull then
     begin
       RptStream := dmReport.PrestanokNaRabotenOdnos.CreateBlobStream(dmReport.PrestanokNaRabotenOdnosDATA, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);
       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       dmReport.OpenDialog1.FileName:='';
       dmReport.OpenDialog1.Execute();
       if dmReport.OpenDialog1.FileName <> '' then
        begin
       RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
       If RichView <> Nil Then
         begin
           RichView.RichEdit.Lines.LoadFromFile( dmReport.OpenDialog1.FileName );
           RichView.DataSet:=dmReport.frxPrekinRabotenOdnos;
         end;
       MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
       if MasterData <> Nil then
          MasterData.DataSet:=dmReport.frxPrekinRabotenOdnos;

       dmReport.frxReport1.ShowReport;

       frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 5);
        end;
     end;
end;

procedure TfrmResenieOtkaz.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  id_promena:Integer;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if (Validacija(dPanel) = false) then
    begin
      if st in [dsInsert] then
        begin
          {qID_VRABOTEN_RM.Close;
          qID_VRABOTEN_RM.ParamByName('mb').Value:=mb.Text;
          qID_VRABOTEN_RM.ParamByName('firma').Value:=dmKon.re;
          qID_VRABOTEN_RM.ExecQuery;
          id_promena:=qID_VRABOTEN_RM.FldByName['id'].Value;  }

          dmOtsustvo.insert(dm.pZatvoriRM, 'MB', 'FIRMA', 'DATUMDO', 'DOKUMENT_ID', 'TIP', Null, Null, Null,
                                           mb.Text,dmKon.re, staro_do.Date,dm.tblResenieOtkazID.Value, 1, Null, Null, Null);
        end;
        {dm.tblResenieOtkazID_PROMENA.Value:=id_promena;}
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dm.viewVraboteni.close;
        dm.viewVraboteni.ParamByName('firma').Value:=dmKon.re;
        dm.viewVraboteni.ParamByName('mb').Value:='%';
        dm.viewVraboteni.ParamByName('re').Value:='%';
        dm.viewVraboteni.Open;
        tblRMRE.ParamByName('firma').Value:=firma;
        tblRMRE.FullRefresh;
        VRABOTENIME.Visible:=False;
        lica.Visible:=True;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
  end;
end;

procedure TfrmResenieOtkaz.ComboBoxGodinaChange(Sender: TObject);
begin
  if ComboBoxGodina.Text = '����' then
         cxGrid1DBTableView1.DataController.Filter.Clear
     else  if ComboBoxGodina.Text <> '' then
           
       begin
          cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(ComboBoxGodina.Text), ComboBoxGodina.Text);
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
     end;
end;

procedure TfrmResenieOtkaz.cxDBTextEditAllExit(Sender: TObject);
var pom:String;
begin
     TEdit(Sender).Color:=clWhite;
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
        begin
          if (VRABOTENIME.Text<> '')then
             begin
                qStaroMesto.Close;
                qStaroMesto.ParamByName('mb').Value:=mb.Text;
                qStaroMesto.ParamByName('firma').Value:=dmKon.re;
                qStaroMesto.ExecQuery;
                dm.tblResenieOtkazID_RM_RE.Value:= qStaroMesto.FldByName['id_rm_re'].Value;
             end;
        end;
end;

procedure TfrmResenieOtkaz.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblResenieOtkaz.Open;
  dm.tblVidOtkaz.Close;
  dm.tblVidOtkaz.open;
  tblRMRE.ParamByName('firma').Value:=dmKon.re;
  tblRMRE.Open;
  dm.tblLica.Close;
  dm.tblLica.ParamByName('mb').Value:='%';
  dm.tblLica.Open;
  ComboBoxGodina.Text:=IntToStr(dmkon.godina);
end;

procedure TfrmResenieOtkaz.OBRAZLOZENIEDblClick(Sender: TObject);
begin
  inherited;
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblResenieOtkazOBRAZLOZENIE.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
      begin
	      dm.tblResenieOtkazOBRAZLOZENIE.Value := frmNotepad.ReturnText;
      end;
    frmNotepad.Free;
end;

end.
