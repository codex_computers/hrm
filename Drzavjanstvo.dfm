inherited frmDrzavjanstvo: TfrmDrzavjanstvo
  Caption = #1044#1088#1078#1072#1074#1112#1072#1085#1089#1090#1074#1086
  ClientHeight = 494
  ClientWidth = 602
  ExplicitWidth = 618
  ExplicitHeight = 532
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 249
    ExplicitWidth = 602
    ExplicitHeight = 249
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 245
      ExplicitWidth = 598
      ExplicitHeight = 245
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        DataController.DataSource = dmSis.dsDrzavjanstvo
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 112
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 344
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 375
    Width = 602
    Height = 96
    ExplicitTop = 375
    ExplicitWidth = 602
    ExplicitHeight = 96
    inherited Label1: TLabel
      Top = 28
      Caption = #1053#1072#1079#1080#1074' :'
      ExplicitTop = 28
    end
    inherited Sifra: TcxDBTextEdit
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dmSis.dsDrzavjanstvo
      TabOrder = 3
      Visible = False
      ExplicitTop = 6
      ExplicitWidth = 316
      Width = 316
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 56
      ExplicitLeft = 511
      ExplicitTop = 56
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 56
      ExplicitLeft = 430
      ExplicitTop = 56
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 25
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dmSis.dsDrzavjanstvo
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 356
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 602
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 471
    Width = 602
    ExplicitTop = 471
    ExplicitWidth = 602
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40273.793335497680000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
