inherited frmVidDokument: TfrmVidDokument
  Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
  ClientHeight = 511
  ClientWidth = 615
  ExplicitWidth = 631
  ExplicitHeight = 549
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 615
    Height = 244
    ExplicitWidth = 615
    ExplicitHeight = 244
    inherited cxGrid1: TcxGrid
      Width = 611
      Height = 240
      PopupMenu = PopupMenu1
      ExplicitWidth = 611
      ExplicitHeight = 240
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsVidDokument
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          SortIndex = 0
          SortOrder = soAscending
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 564
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 370
    Width = 615
    Height = 118
    ExplicitTop = 370
    ExplicitWidth = 615
    ExplicitHeight = 118
    inherited Label1: TLabel
      Top = 7
      Visible = False
      ExplicitTop = 7
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 41
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Top = 4
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsVidDokument
      TabOrder = 3
      Visible = False
      ExplicitTop = 4
    end
    inherited OtkaziButton: TcxButton
      Left = 524
      Top = 81
      ExplicitLeft = 524
      ExplicitTop = 81
    end
    inherited ZapisiButton: TcxButton
      Left = 443
      Top = 81
      ExplicitLeft = 443
      ExplicitTop = 81
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 38
      Anchors = [akLeft, akTop, akRight]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsVidDokument
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 376
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 615
    ExplicitWidth = 615
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 488
    Width = 615
    ExplicitTop = 488
    ExplicitWidth = 615
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 216
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40424.435935405090000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
