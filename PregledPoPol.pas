unit PregledPoPol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, dxStatusBar, dxRibbonStatusBar, Menus, ActnList, StdCtrls,
  cxButtons, dxSkinsdxBarPainter, cxDropDownEdit, dxSkinsdxRibbonPainter,
  dxRibbon, dxBar, cxBarEditItem, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,
   cxGridCustomPopupMenu, cxGridPopupMenu, cxGridChartView,
  cxGridDBChartView, cxPC, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk, dxPScxPivotGridLnk;

type
  TfrmPregledPoPol = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1MB: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn;
    cxGrid1DBTableView1RABEDINICANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VIDVRABOTUVAWE: TcxGridDBColumn;
    ActionList1: TActionList;
    aPecati: TAction;
    ActionList2: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarLargeButton10: TdxBarLargeButton;
    lPanel: TPanel;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1MB: TcxGridDBColumn;
    cxGridDBTableView1RENAZIV: TcxGridDBColumn;
    cxGridDBTableView1RABMESTONAZIV: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridLevel2: TcxGridLevel;
    cxGrid3DBChartView1: TcxGridDBChartView;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGrid3DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid3DBChartView1Series2: TcxGridDBChartSeries;
    cxGrid3DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGrid3DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    cxTabSheet3: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBChartView1: TcxGridDBChartView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBChartView1Series1: TcxGridDBChartSeries;
    cxGridDBChartView1Series2: TcxGridDBChartSeries;
    aPecatiGrafik: TAction;
    aPecatiGrafikCelo: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxComponentPrinter1Link3: TdxGridReportLink;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    cxGridDBTableView1NAZIV_VRABOTEN: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_VRABOTEN_TI: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyleRepository3: TcxStyleRepository;
    dxGridReportLinkStyleSheet3: TdxGridReportLinkStyleSheet;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;
    cxStyle33: TcxStyle;
    cxStyle34: TcxStyle;
    cxStyle35: TcxStyle;
    cxStyle36: TcxStyle;
    cxStyle37: TcxStyle;
    cxStyle38: TcxStyle;
    cxStyle39: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPecatiGrafikExecute(Sender: TObject);
    procedure aPecatiGrafikCeloExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPregledPoPol: TfrmPregledPoPol;

implementation

uses Utils, dmUnit, dmReportUnit, dmKonekcija, dmResources, dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmPregledPoPol.aBrisiPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPregledPoPol.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPregledPoPol.aPageSetupExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmPregledPoPol.aPecatiGrafikCeloExecute(Sender: TObject);
begin
     dxComponentPrinter1Link3.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link3);
end;

procedure TfrmPregledPoPol.aPecatiGrafikExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmPregledPoPol.aPecatiTabelaExecute(Sender: TObject);
begin
     dxComponentPrinter1Link1.ReportTitle.Text := Caption;
     dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPregledPoPol.aPodesuvanjePecatenjeExecute(
  Sender: TObject);
begin
     dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmPregledPoPol.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGridDBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmPregledPoPol.aSnimiPecatenjeExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPregledPoPol.aZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, Caption);
end;

procedure TfrmPregledPoPol.FormCreate(Sender: TObject);
begin
     dxRibbon1.ColorSchemeName := dmRes.skin_name;

     dm.tblPolPregled.ParamByName('firma').Value:=dmKon.re;
     dm.tblPolPregled.open;

     dm.tblPolStruktura.ParamByName('POTEKLO').Value:=IntToStr(firma)+','+'%';
     dm.tblPolStruktura.ParamByName('Firma').Value:=dmKon.re;
     dm.tblPolStruktura.Open;

     dm.tblPolStrukturaCelo.ParamByName('firma').Value:=dmKon.re;
     dm.tblPolStrukturaCelo.Open;
end;

procedure TfrmPregledPoPol.FormShow(Sender: TObject);
begin
 //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGridDBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link1);
end;

end.
