unit RabotnoMestoUsloviRabota;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, dxPSdxDBOCLnk, cxDBLookupComboBox, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, FIBDataSet, pFIBDataSet;

type
  TfrmRmReUsloviRabota = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn;
    cxGrid1DBTableView1RABOTNOMESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVI_RABOTA: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVINAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1USLOVIPROCENT: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label20: TLabel;
    UsloviRabota: TcxDBTextEdit;
    cbUsloviRabota: TcxLookupComboBox;
    ID_RM_RE: TcxDBTextEdit;
    ID_RM_RE_NAZIV: TcxDBLookupComboBox;
    Label2: TLabel;
    tblRMRE: TpFIBDataSet;
    tblRMREID: TFIBIntegerField;
    tblRMREID_RE: TFIBIntegerField;
    tblRMREID_SISTEMATIZACIJA: TFIBIntegerField;
    tblRMREID_RM: TFIBIntegerField;
    tblRMREBROJ_IZVRSITELI: TFIBIntegerField;
    tblRMRETS_INS: TFIBDateTimeField;
    tblRMRETS_UPD: TFIBDateTimeField;
    tblRMREUSR_INS: TFIBStringField;
    tblRMREUSR_UPD: TFIBStringField;
    tblRMRENAZIV_RE: TFIBStringField;
    tblRMREOPIS_SISTEMATIZACIJA: TFIBStringField;
    tblRMRENAZIV_RM: TFIBStringField;
    tblRMREKOREN: TFIBIntegerField;
    tblRMRERAKOVODENJE: TFIBBCDField;
    tblRMREDENOVI_ODMOR: TFIBIntegerField;
    tblRMREDEN_USLOVI_RABOTA: TFIBIntegerField;
    tblRMRESTEPEN_SLOZENOST: TFIBBCDField;
    tblRMREUSLOVI_RABOTA: TFIBIntegerField;
    tblRMREUSLOVIRABOTAID: TFIBIntegerField;
    tblRMREUSLOVIRABOTAPROCENT: TFIBBCDField;
    tblRMREBENEFICIRAN_STAZ: TFIBBCDField;
    dsRMRE: TDataSource;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRmReUsloviRabota: TfrmRmReUsloviRabota;

implementation

{$R *.dfm}

uses dmUnit, dmSistematizacija, dmKonekcija, dmMaticni;

procedure TfrmRmReUsloviRabota.aAzurirajExecute(Sender: TObject);
begin
  ID_RM_RE.Enabled:=True;
  ID_RM_RE_NAZIV.Enabled:=True;
  inherited;
  if tag = 1 then
     begin
       UsloviRabota.SetFocus;
       ID_RM_RE.Enabled:=False;
       ID_RM_RE_NAZIV.Enabled:=false;
     end;
end;

procedure TfrmRmReUsloviRabota.aNovExecute(Sender: TObject);
begin
  ID_RM_RE.Enabled:=True;
  ID_RM_RE_NAZIV.Enabled:=True;
  inherited;
  if tag = 1 then
     begin
       dm.tblRMRE_UsloviRabotaID_RM_RE.Value:=dmSis.tblRMREID.Value;
       UsloviRabota.SetFocus;
       ID_RM_RE.Enabled:=False;
       ID_RM_RE_NAZIV.Enabled:=false;
     end;
end;

procedure TfrmRmReUsloviRabota.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if (((Sender as TWinControl)= cbUsloviRabota) or  ((Sender as TWinControl)= UsloviRabota)) and (dm.tblRMRE_UsloviRabota.State in [dsEdit,dsInsert]) then
    begin
      if cbUsloviRabota.Text <> '' then
         begin
           dm.tblUsloviPoteskiOdNormalni.Locate('ID',dm.tblUsloviPoteskiOdNormalniID.Value, []);
           dm.tblRMRE_UsloviRabotaUSLOVIPROCENT.Value:=dm.tblUsloviPoteskiOdNormalniPROCENT.Value;
           dm.tblRMRE_UsloviRabotaUSLOVI_RABOTA.Value:=dm.tblUsloviPoteskiOdNormalniID.Value
         end
      else
         begin
           dm.tblRMRE_UsloviRabotaUSLOVI_RABOTA.Clear;
           dm.tblRMRE_UsloviRabotaUSLOVIPROCENT.Clear;
         end;
    end;
end;

procedure TfrmRmReUsloviRabota.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  if (not dm.tblRMRE_UsloviRabotaUSLOVI_RABOTA.IsNull)  then
         begin
           dm.tblUsloviPoteskiOdNormalni.Locate('ID',dm.tblRMRE_UsloviRabotaUSLOVI_RABOTA.Value, []);
           cbUsloviRabota.Text:= dm.tblUsloviPoteskiOdNormalniNAZIV.Value;
         end
      else
         begin
           cbUsloviRabota.Text:='';
         end;
end;

procedure TfrmRmReUsloviRabota.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblUsloviPoteskiOdNormalni.open;
end;

procedure TfrmRmReUsloviRabota.FormShow(Sender: TObject);
begin
  inherited;
  if tag = 1 then
     begin
      dm.tblRMRE_UsloviRabota.Close;
      dm.tblRMRE_UsloviRabota.ParamByName('id_rm_re').Value:=dmSis.tblRMREID.Value;
      dm.tblRMRE_UsloviRabota.Open;
     end
  else if tag = 2 then
     begin
      dm.tblRMRE_UsloviRabota.Close;
      dm.tblRMRE_UsloviRabota.ParamByName('id_rm_re').Value:=dm.tblDogVrabotuvanjeRABOTNO_MESTO.Value;
      dm.tblRMRE_UsloviRabota.Open;
     end
  else if tag = 3 then
     begin
      dm.tblRMRE_UsloviRabota.Close;
      dm.tblRMRE_UsloviRabota.ParamByName('id_rm_re').Value:=dm.tblAneksID_RM_RE.Value;
      dm.tblRMRE_UsloviRabota.Open;
     end
  else if tag = 0 then
     begin
      dm.tblRMRE_UsloviRabota.Close;
      dm.tblRMRE_UsloviRabota.ParamByName('id_rm_re').Value:='%';
      dm.tblRMRE_UsloviRabota.Open;
     end;

   if (tag = 0) or (Tag = 1)  then
     begin
      dxBarManager1Bar1.Visible:=true;
      aNov.Enabled:=true;
      aAzuriraj.Enabled:=true;
      aBrisi.Enabled:=true;
     end
   else
     begin
      dxBarManager1Bar1.Visible:=false;
      aNov.Enabled:=false;
      aAzuriraj.Enabled:=false;
      aBrisi.Enabled:=false;
     end;

   tblRMRE.ParamByName('firma').Value:=dmKon.re;
   tblRMRE.Open;

   if dm.tblRMRE_UsloviRabotaUSLOVI_RABOTA.IsNull then
     cbUsloviRabota.clear
   else
     begin
       dm.tblUsloviPoteskiOdNormalni.Locate('ID',dm.tblRMRE_UsloviRabotaUSLOVI_RABOTA.Value, []);
       cbUsloviRabota.EditValue:=dm.tblUsloviPoteskiOdNormalniID.Value;
     end;
end;

end.
