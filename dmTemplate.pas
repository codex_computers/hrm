unit dmTemplate;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, frxClass, frxDesgn, frxDBSet,
  frxRich, frxExportRTF, Dialogs, frxDCtrl;

type
  TdmT = class(TDataModule)
    frxDesigner1: TfrxDesigner;
    frxDogovorVrabotuvanje: TfrxDBDataset;
    DogovorZaVrabotuvanje: TpFIBDataSet;
    DogovorZaVrabotuvanjeID: TFIBIntegerField;
    DogovorZaVrabotuvanjeVID_DOKUMENT: TFIBIntegerField;
    DogovorZaVrabotuvanjeNAZIV_VID_DOKUMENT: TFIBStringField;
    DogovorZaVrabotuvanjeMB: TFIBStringField;
    DogovorZaVrabotuvanjeID_RE_FIRMA: TFIBIntegerField;
    DogovorZaVrabotuvanjePREZIME: TFIBStringField;
    DogovorZaVrabotuvanjeTATKOVO_IME: TFIBStringField;
    DogovorZaVrabotuvanjeIME: TFIBStringField;
    DogovorZaVrabotuvanjeADRESA: TFIBStringField;
    DogovorZaVrabotuvanjeMESTO: TFIBIntegerField;
    DogovorZaVrabotuvanjeSTEPEN_STRUCNA_PODGOTOVKA: TFIBIntegerField;
    DogovorZaVrabotuvanjeVID_VRABOTUVANJE: TFIBIntegerField;
    DogovorZaVrabotuvanjeNAZIV_VID_VRAB: TFIBStringField;
    DogovorZaVrabotuvanjeDATUM_OD: TFIBDateField;
    DogovorZaVrabotuvanjeDATUM_DO: TFIBDateField;
    DogovorZaVrabotuvanjeRABOTNO_VREME: TFIBBCDField;
    DogovorZaVrabotuvanjeRABOTNO_MESTO: TFIBIntegerField;
    DogovorZaVrabotuvanjeNAZIV_MESTO: TFIBStringField;
    DogovorZaVrabotuvanjeOBRAZOVANIE: TFIBStringField;
    DogovorZaVrabotuvanjeRABMESTONAZIV: TFIBStringField;
    DogovorZaVrabotuvanjeMB_DIREKTOR: TFIBStringField;
    DogovorZaVrabotuvanjePLATA: TFIBBCDField;
    DogovorZaVrabotuvanjeSTATUS: TFIBSmallIntField;
    DogovorZaVrabotuvanjeDATA: TFIBBlobField;
    frxRTFExport1: TfrxRTFExport;
    frxReport1: TfrxReport;
    Template: TpFIBDataSet;
    dsTemplate: TDataSource;
    TemplateID: TFIBIntegerField;
    TemplateVID: TFIBIntegerField;
    TemplateNAZIV: TFIBStringField;
    TemplateREPORT: TFIBBlobField;
    TemplateTS_INS: TFIBDateTimeField;
    TemplateTS_UPD: TFIBDateTimeField;
    TemplateUSR_UPD: TFIBStringField;
    TemplateUSR_INS: TFIBStringField;
    OpenDialog1: TOpenDialog;
    TemplateBROJ: TFIBIntegerField;
    BaranjeZaGO: TpFIBDataSet;
    BaranjeZaGOID: TFIBIntegerField;
    BaranjeZaGOMB: TFIBStringField;
    BaranjeZaGOGODINA: TFIBIntegerField;
    BaranjeZaGODATUM: TFIBDateField;
    BaranjeZaGOVID_DOCUMENT: TFIBIntegerField;
    BaranjeZaGOVIDDOKUMENTNAZIV: TFIBStringField;
    BaranjeZaGOODOBRENIE: TFIBSmallIntField;
    BaranjeZaGOKORISTENJE: TFIBIntegerField;
    BaranjeZaGOPRV_DEL: TFIBIntegerField;
    BaranjeZaGOVTOR_DEL: TFIBIntegerField;
    BaranjeZaGOMB_RAKOVODITEL: TFIBStringField;
    BaranjeZaGONAZIVVRABOTEN: TFIBStringField;
    BaranjeZaGORMNAZIV: TFIBStringField;
    BaranjeZaGODATUM1_OD: TFIBDateField;
    BaranjeZaGODATUM1_DO: TFIBDateField;
    BaranjeZaGODATUM2_OD: TFIBDateField;
    BaranjeZaGODATUM2_DO: TFIBDateField;
    BaranjeZaGOOdobrenieNaziv: TFIBStringField;
    BaranjeZaGOKoristenjeNaziv: TFIBStringField;
    frxBaranjeZaGO: TfrxDBDataset;
    BaranjeZaGODATA: TFIBBlobField;
    frxResenieGO: TfrxDBDataset;
    ResenieZaGO: TpFIBDataSet;
    ResenieZaGOID: TFIBIntegerField;
    ResenieZaGOMB: TFIBStringField;
    ResenieZaGOGODINA: TFIBIntegerField;
    ResenieZaGODATUM: TFIBDateField;
    ResenieZaGOVID_DOKUMENT: TFIBIntegerField;
    ResenieZaGOVIDDOKUMENTNAZIV: TFIBStringField;
    ResenieZaGOKORISTENJE: TFIBSmallIntField;
    ResenieZaGOPRV_DEL: TFIBIntegerField;
    ResenieZaGOVTOR_DEL: TFIBIntegerField;
    ResenieZaGOMB_RAKOVODITEL: TFIBStringField;
    ResenieZaGONAZIVVRABOTEN: TFIBStringField;
    ResenieZaGORMNAZIV: TFIBStringField;
    ResenieZaGODATUM1_OD: TFIBDateField;
    ResenieZaGODATUM1_DO: TFIBDateField;
    ResenieZaGODATUM2_OD: TFIBDateField;
    ResenieZaGODATUM2_DO: TFIBDateField;
    ResenieZaGOKoristenjeNaziv: TFIBStringField;
    ResenieZaGOSTAZ: TFIBSmallIntField;
    ResenieZaGOSTEPEN_NA_SLOZENOST: TFIBSmallIntField;
    ResenieZaGOZDRASTVENA_SOSTOJBA: TFIBSmallIntField;
    ResenieZaGOSUMAGO: TFIBBCDField;
    ResenieZaGOBARANJE_GO: TFIBIntegerField;
    ResenieZaGOTS_INS: TFIBDateTimeField;
    ResenieZaGOTS_UPD: TFIBDateTimeField;
    ResenieZaGOUSR_INS: TFIBStringField;
    ResenieZaGOUSR_UPD: TFIBStringField;
    ResenieZaGODATA: TFIBBlobField;
    ResenieZaPreraspredelba: TpFIBDataSet;
    frxResenieZaPreraspredelba: TfrxDBDataset;
    ResenieZaPreraspredelbaID: TFIBIntegerField;
    ResenieZaPreraspredelbaVID_DOKUMENT: TFIBIntegerField;
    ResenieZaPreraspredelbaVIDDOKUMENTNAZIV: TFIBStringField;
    ResenieZaPreraspredelbaMB: TFIBStringField;
    ResenieZaPreraspredelbaVRABOTENNAZIV: TFIBStringField;
    ResenieZaPreraspredelbaDATUM_OD: TFIBDateField;
    ResenieZaPreraspredelbaDATUM_DO: TFIBDateField;
    ResenieZaPreraspredelbaNOVO_RM: TFIBIntegerField;
    ResenieZaPreraspredelbaTS_INS: TFIBDateTimeField;
    ResenieZaPreraspredelbaTS_UPD: TFIBDateTimeField;
    ResenieZaPreraspredelbaUSR_INS: TFIBStringField;
    ResenieZaPreraspredelbaUSR_UPD: TFIBStringField;
    ResenieZaPreraspredelbaSTARO_RM: TFIBIntegerField;
    ResenieZaPreraspredelbaSTATUS: TFIBSmallIntField;
    ResenieZaPreraspredelbaSTARONAZIV: TFIBStringField;
    ResenieZaPreraspredelbaNOVONAZIV: TFIBStringField;
    ResenieZaPreraspredelbaDATA: TFIBBlobField;
    frxPrekinRabotenOdnos: TfrxDBDataset;
    PrestanokNaRabotenOdnos: TpFIBDataSet;
    PrestanokNaRabotenOdnosID: TFIBIntegerField;
    PrestanokNaRabotenOdnosMB: TFIBStringField;
    PrestanokNaRabotenOdnosID_RE_FIRMA: TFIBIntegerField;
    PrestanokNaRabotenOdnosID_RM_RE: TFIBIntegerField;
    PrestanokNaRabotenOdnosDATUM_DO: TFIBDateField;
    PrestanokNaRabotenOdnosOBRAZLOZENIE: TFIBStringField;
    PrestanokNaRabotenOdnosPRICINA: TFIBIntegerField;
    PrestanokNaRabotenOdnosDIREKTOR: TFIBStringField;
    PrestanokNaRabotenOdnosLICENAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosRABOTNOMESONAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosRENAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosPRICINANAZIV: TFIBStringField;
    PrestanokNaRabotenOdnosTS_INS: TFIBDateTimeField;
    PrestanokNaRabotenOdnosTS_UPD: TFIBDateTimeField;
    PrestanokNaRabotenOdnosUSR_INS: TFIBStringField;
    PrestanokNaRabotenOdnosUSR_UPD: TFIBStringField;
    PrestanokNaRabotenOdnosDATA: TFIBBlobField;
    function frxDesigner1SaveReport(Report: TfrxReport;
      SaveAs: Boolean; tag: integer): Boolean;
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Spremi(id :Integer ; app:string);
  end;

var
  dmT: TdmT;
   templatename:string;
implementation

uses dmUnit, dmKonekcija;

{$R *.dfm}

function TdmT.frxDesigner1SaveReport(Report: TfrxReport;
  SaveAs: Boolean; tag: integer): Boolean;
var template : TStream;
begin
  if tag = 1 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        dmt.frxReport1.SaveToStream(template);
        dmt.BaranjeZaGO.Open;
        dmt.BaranjeZaGO.edit;
        (dmt.BaranjeZaGODATA as TBlobField).LoadFromStream(template);
        dmt.BaranjeZaGO.Post;
     end;

  if tag = 2 then
     begin
        template := TMemoryStream.Create;
        template.Position := 0;
        dmt.frxReport1.SaveToStream(template);
        dmt.DogovorZaVrabotuvanje.Open;
        dmt.DogovorZaVrabotuvanje.edit;
        (dmt.DogovorZaVrabotuvanjeDATA as TBlobField).LoadFromStream(template);
        dmt.DogovorZaVrabotuvanje.Post;
     end;

  if tag = 3 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        dmt.frxReport1.SaveToStream(template);
        dmt.ResenieZaGO.Open;
        dmt.ResenieZaGO.edit;
        (dmt.ResenieZaGODATA as TBlobField).LoadFromStream(template);
        dmt.ResenieZaGO.Post;
     end;

    if tag = 4 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        dmt.frxReport1.SaveToStream(template);
        dmt.ResenieZaPreraspredelba.Open;
        dmt.ResenieZaPreraspredelba.edit;
        (dmt.ResenieZaPreraspredelbaDATA as TBlobField).LoadFromStream(template);
        dmt.ResenieZaPreraspredelba.Post;
     end;

    if tag = 5 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        dmt.frxReport1.SaveToStream(template);
        dmt.PrestanokNaRabotenOdnos.Open;
        dmt.PrestanokNaRabotenOdnos.edit;
        (dmt.PrestanokNaRabotenOdnosDATA as TBlobField).LoadFromStream(template);
        dmt.PrestanokNaRabotenOdnos.Post;
     end;
end;

procedure TdmT.frxReport1GetValue(const VarName: string; var Value: Variant);
var
  sl: TStringList;
begin
  if CompareText(VarName, 'file') = 0 then
  begin
    sl := TStringList.Create;
    sl.LoadFromFile(OpenDialog1.FileName);
    Value := sl.Text;
    sl.Free;
  end;
end;

procedure TdmT.Spremi(id :Integer ; app:string);
var
  RptStream: TStream;
  SqlStream: TStream;
begin
  frxReport1.Clear;
  frxReport1.Script.Clear;

//  tblReportDizajn2.Close;
//  tblReportDizajn2.Params.ParamByName('br').Value := id;
//  tblReportDizajn2.Params.ParamByName('app').Value:= app;
//  tblReportDizajn2.Open;

//  tblSqlReport.Close;
//  SqlStream := tblReportDizajn2.CreateBlobStream(tblReportDizajn2SQL , bmRead);
//  tblSqlReport.SQLs.SelectSQL.LoadFromStream(SqlStream);

  RptStream := dm.tblDogVrabotuvanje.CreateBlobStream(dm.tblDogVrabotuvanjeDATA, bmRead);
  frxReport1.LoadFromStream(RptStream);
end;
end.
