inherited frmDeca: TfrmDeca
  Caption = #1044#1077#1094#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1080#1090#1077
  ClientHeight = 717
  ClientWidth = 763
  ExplicitWidth = 779
  ExplicitHeight = 756
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 763
    ExplicitWidth = 763
    inherited cxGrid1: TcxGrid
      Width = 759
      ExplicitWidth = 759
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsDeca
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1RODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'RODITEL'
          Visible = False
        end
        object cxGrid1DBTableView1RODITELNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 133
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1MAT_BR: TcxGridDBColumn
          DataBinding.FieldName = 'MAT_BR'
          Width = 100
        end
        object cxGrid1DBTableView1PRZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PRZIME'
          Width = 100
        end
        object cxGrid1DBTableView1TATKOVO_IME: TcxGridDBColumn
          DataBinding.FieldName = 'TATKOVO_IME'
          Width = 100
        end
        object cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn
          Caption = #1048#1084#1077' '#1085#1072' '#1076#1077#1090#1077
          DataBinding.FieldName = 'IME'
          Width = 139
        end
        object cxGrid1DBTableView1DATA_RAGJANJE: TcxGridDBColumn
          DataBinding.FieldName = 'DATA_RAGJANJE'
          Width = 117
        end
        object cxGrid1DBTableView1POL: TcxGridDBColumn
          DataBinding.FieldName = 'PolNaziv'
          Width = 67
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          DataBinding.FieldName = 'vozrast'
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1POL1: TcxGridDBColumn
          DataBinding.FieldName = 'POL'
          Visible = False
        end
        object cxGrid1DBTableView1OSIGURENIK: TcxGridDBColumn
          DataBinding.FieldName = 'OSIGURENIK'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1OsigurenikDaNe: TcxGridDBColumn
          DataBinding.FieldName = 'OsigurenikDaNe'
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 763
    Height = 318
    ExplicitWidth = 763
    ExplicitHeight = 318
    DesignSize = (
      763
      318)
    inherited Label1: TLabel
      Left = 571
      Top = 30
      Visible = False
      ExplicitLeft = 571
      ExplicitTop = 30
    end
    object Label11: TLabel [1]
      Left = 70
      Top = 25
      Width = 93
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1051#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 627
      Top = 27
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsDeca
      TabOrder = 4
      Visible = False
      ExplicitLeft = 627
      ExplicitTop = 27
    end
    inherited OtkaziButton: TcxButton
      Left = 672
      Top = 278
      TabOrder = 5
      ExplicitLeft = 672
      ExplicitTop = 278
    end
    inherited ZapisiButton: TcxButton
      Left = 591
      Top = 278
      TabOrder = 3
      ExplicitLeft = 591
      ExplicitTop = 278
    end
    object VRABOTENNAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 266
      Top = 22
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1086#1076#1080#1090#1077#1083
      BeepOnEnter = False
      DataBinding.DataField = 'RODITEL'
      DataBinding.DataSource = dm.dsDeca
      ParentFont = False
      ParentShowHint = False
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1086#1076#1080#1090#1077#1083
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      ShowHint = True
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 278
    end
    object MB_VRABOTEN: TcxDBTextEdit
      Tag = 1
      Left = 169
      Top = 22
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1088#1086#1076#1080#1090#1077#1083
      BeepOnEnter = False
      DataBinding.DataField = 'RODITEL'
      DataBinding.DataSource = dm.dsDeca
      ParentFont = False
      ParentShowHint = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      ShowHint = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 96
    end
    object cxGroupBox1: TcxGroupBox
      Left = 31
      Top = 49
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1076#1077#1090#1077
      TabOrder = 2
      Height = 217
      Width = 538
      object Label4: TLabel
        Left = 39
        Top = 29
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 39
        Top = 58
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 39
        Top = 80
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 39
        Top = 107
        Width = 93
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 271
        Top = 137
        Width = 53
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1086#1079#1088#1072#1089#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 18
        Top = 137
        Width = 114
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MB: TcxDBTextEdit
        Left = 138
        Top = 26
        Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1090#1077
        BeepOnEnter = False
        DataBinding.DataField = 'MAT_BR'
        DataBinding.DataSource = dm.dsDeca
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Properties.OnEditValueChanged = MBPropertiesEditValueChanged
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 96
      end
      object Prezime: TcxDBTextEdit
        Left = 138
        Top = 53
        Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1076#1077#1090#1077
        BeepOnEnter = False
        DataBinding.DataField = 'PRZIME'
        DataBinding.DataSource = dm.dsDeca
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 375
      end
      object tatkovo_ime: TcxDBTextEdit
        Left = 138
        Top = 80
        Hint = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' '#1085#1072' '#1076#1077#1090#1077
        BeepOnEnter = False
        DataBinding.DataField = 'TATKOVO_IME'
        DataBinding.DataSource = dm.dsDeca
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 375
      end
      object ime: TcxDBTextEdit
        Left = 138
        Top = 107
        Hint = #1048#1084#1077' '#1085#1072' '#1076#1077#1090#1077
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsDeca
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 375
      end
      object vozrast: TcxDBTextEdit
        Left = 330
        Top = 134
        Hint = #1042#1086#1079#1088#1072#1089#1090' '#1085#1072' '#1076#1077#1090#1077
        BeepOnEnter = False
        DataBinding.DataField = 'vozrast'
        DataBinding.DataSource = dm.dsDeca
        Enabled = False
        ParentFont = False
        ParentShowHint = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        ShowHint = True
        Style.Shadow = False
        StyleDisabled.TextColor = clDefault
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 47
      end
      object DATUM_RADJANJE: TcxDBDateEdit
        Left = 138
        Top = 134
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1072#1107#1072#1114#1077' '#1085#1072' '#1076#1077#1090#1077
        BeepOnEnter = False
        DataBinding.DataField = 'DATA_RAGJANJE'
        DataBinding.DataSource = dm.dsDeca
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 127
      end
      object Pol: TcxDBRadioGroup
        Left = 138
        Top = 161
        Hint = #1055#1086#1083' '#1085#1072' '#1076#1077#1090#1077
        TabStop = False
        Caption = #1055#1086#1083' '#1085#1072' '#1076#1077#1090#1077
        DataBinding.DataField = 'POL'
        DataBinding.DataSource = dm.dsDeca
        Properties.Columns = 2
        Properties.Items = <
          item
            Caption = #1046#1077#1085#1089#1082#1080
            Value = 2
          end
          item
            Caption = #1052#1072#1096#1082#1080
            Value = 1
          end>
        Style.LookAndFeel.Kind = lfUltraFlat
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.Kind = lfUltraFlat
        StyleDisabled.LookAndFeel.NativeStyle = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        Height = 43
        Width = 143
      end
      object cxDBRadioGroup1: TcxDBRadioGroup
        Left = 306
        Top = 161
        TabStop = False
        Caption = #1054#1089#1080#1075#1091#1088#1077#1085#1080#1082
        DataBinding.DataField = 'OSIGURENIK'
        DataBinding.DataSource = dm.dsDeca
        Properties.Columns = 2
        Properties.ImmediatePost = True
        Properties.Items = <
          item
            Caption = #1044#1072
            Value = 1
          end
          item
            Caption = #1053#1077
            Value = 0
          end>
        TabOrder = 7
        Height = 43
        Width = 136
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 763
    ExplicitWidth = 763
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 694
    Width = 763
    ExplicitTop = 694
    ExplicitWidth = 763
  end
  inherited dxBarManager1: TdxBarManager
    Top = 200
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40298.488628287040000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 568
    Top = 40
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
