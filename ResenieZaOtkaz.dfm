inherited frmResenieOtkaz: TfrmResenieOtkaz
  Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1077#1082#1080#1085' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
  ClientHeight = 672
  ClientWidth = 1014
  ExplicitWidth = 1030
  ExplicitHeight = 711
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1014
    Height = 266
    ExplicitWidth = 1014
    ExplicitHeight = 266
    inherited cxGrid1: TcxGrid
      Width = 1010
      Height = 262
      ExplicitWidth = 1010
      ExplicitHeight = 262
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsResenieOtkaz
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 200
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 91
        end
        object cxGrid1DBTableView1MB: TcxGridDBColumn
          DataBinding.FieldName = 'MB'
          Width = 81
        end
        object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1LICENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
          Width = 107
        end
        object cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE_FIRMA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1ID_RM_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RM_RE'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RENAZIV'
          Width = 174
        end
        object cxGrid1DBTableView1RABOTNOMESONAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RABOTNOMESONAZIV'
          Width = 155
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 91
        end
        object cxGrid1DBTableView1DIREKTOR: TcxGridDBColumn
          DataBinding.FieldName = 'DIREKTOR'
          Width = 76
        end
        object cxGrid1DBTableView1PRICINA: TcxGridDBColumn
          DataBinding.FieldName = 'PRICINA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1PRICINANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PRICINANAZIV'
          Width = 120
        end
        object cxGrid1DBTableView1OBRAZLOZENIE: TcxGridDBColumn
          DataBinding.FieldName = 'OBRAZLOZENIE'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = False
          Width = 106
        end
        object cxGrid1DBTableView1DATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATA'
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 392
    Width = 1014
    Height = 257
    ExplicitTop = 392
    ExplicitWidth = 1014
    ExplicitHeight = 257
    inherited Label1: TLabel
      Top = 9
      Visible = False
      ExplicitTop = 9
    end
    object Label8: TLabel [1]
      Left = 13
      Top = 60
      Width = 94
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label11: TLabel [2]
      Left = 18
      Top = 202
      Width = 89
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1080#1088#1077#1082#1090#1086#1088' : '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [3]
      Left = 216
      Top = 33
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [4]
      Left = 16
      Top = 26
      Width = 91
      Height = 26
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsResenieOtkaz
      TabOrder = 7
      Visible = False
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 914
      Top = 217
      TabOrder = 9
      ExplicitLeft = 914
      ExplicitTop = 217
    end
    inherited ZapisiButton: TcxButton
      Left = 833
      Top = 217
      TabOrder = 8
      ExplicitLeft = 833
      ExplicitTop = 217
    end
    object MB: TcxDBTextEdit
      Tag = 1
      Left = 113
      Top = 65
      Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsResenieOtkaz
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      StyleDisabled.TextColor = clDefault
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 105
    end
    object VRABOTENIME: TcxDBLookupComboBox
      Tag = 1
      Left = 218
      Top = 65
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsResenieOtkaz
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'NAZIV_VRABOTEN_TI'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsViewVraboteni
      StyleDisabled.TextColor = clDefault
      TabOrder = 3
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 256
    end
    object cxGroupBox1: TcxGroupBox
      Left = 46
      Top = 92
      Caption = #1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      TabOrder = 4
      Height = 90
      Width = 444
      object Label2: TLabel
        Left = -48
        Top = 31
        Width = 113
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1072#1079#1080#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 12
        Top = 58
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object staro_rm: TcxDBTextEdit
        Tag = 1
        Left = 68
        Top = 28
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dm.dsResenieOtkaz
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 68
      end
      object ID_RM_RE_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 136
        Top = 28
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
        BeepOnEnter = False
        DataBinding.DataField = 'ID_RM_RE'
        DataBinding.DataSource = dm.dsResenieOtkaz
        Enabled = False
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV_RM'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dsRMRE
        StyleDisabled.TextColor = clBackground
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 292
      end
      object staro_do: TcxDBDateEdit
        Tag = 1
        Left = 68
        Top = 55
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1082#1080#1085' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsResenieOtkaz
        ParentShowHint = False
        Properties.DateButtons = [btnClear, btnToday]
        Properties.InputKind = ikMask
        ShowHint = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 123
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 505
      Top = 30
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077
      TabOrder = 6
      DesignSize = (
        484
        181)
      Height = 181
      Width = 484
      object Label3: TLabel
        Left = -10
        Top = 37
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1080#1095#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = -10
        Top = 60
        Width = 89
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1087#1080#1089' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PRICINA: TcxDBTextEdit
        Tag = 1
        Left = 85
        Top = 30
        Hint = #1055#1088#1080#1095#1080#1085#1072' ('#1096#1080#1092#1088#1072')'
        BeepOnEnter = False
        DataBinding.DataField = 'PRICINA'
        DataBinding.DataSource = dm.dsResenieOtkaz
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clDefault
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 52
      end
      object PRICINA_opis: TcxDBLookupComboBox
        Tag = 1
        Left = 137
        Top = 30
        Hint = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1088#1077#1082#1080#1085' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'PRICINA'
        DataBinding.DataSource = dm.dsResenieOtkaz
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'ID'
          end
          item
            Caption = #1054#1087#1080#1089
            Width = 800
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsVidOtkaz
        StyleDisabled.TextColor = clDefault
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 332
      end
      object OBRAZLOZENIE: TcxDBMemo
        Left = 85
        Top = 57
        Hint = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077' '#1079#1072' '#1076#1086#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1077#1082#1080#1085' '#1085#1072' '#1088#1072#1073#1086#1090#1077#1085' '#1086#1076#1085#1086#1089
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OBRAZLOZENIE'
        DataBinding.DataSource = dm.dsResenieOtkaz
        Properties.WantReturns = False
        TabOrder = 2
        OnDblClick = OBRAZLOZENIEDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 112
        Width = 384
      end
    end
    object Odobreno_od: TcxDBTextEdit
      Left = 113
      Top = 199
      BeepOnEnter = False
      DataBinding.DataField = 'DIREKTOR'
      DataBinding.DataSource = dm.dsResenieOtkaz
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 361
    end
    object Lica: TcxDBLookupComboBox
      Tag = 1
      Left = 218
      Top = 65
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
      BeepOnEnter = False
      DataBinding.DataField = 'MB'
      DataBinding.DataSource = dm.dsResenieOtkaz
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 500
          FieldName = 'MB'
        end
        item
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          Width = 800
          FieldName = 'VRABOTENNAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsLica
      StyleDisabled.TextColor = clDefault
      TabOrder = 10
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 256
    end
    object Broj: TcxDBTextEdit
      Left = 272
      Top = 30
      Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsResenieOtkaz
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 104
    end
    object DatumKreiranje: TcxDBDateEdit
      Tag = 1
      Left = 113
      Top = 30
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsResenieOtkaz
      ParentShowHint = False
      Properties.DateButtons = [btnClear, btnToday]
      Properties.InputKind = ikMask
      ShowHint = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 105
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1014
    ExplicitWidth = 1014
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 649
    Width = 1014
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', F10 - '#1055#1088#1077#1075#1083#1077#1076', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 649
    ExplicitWidth = 1014
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited dxBarManager1: TdxBarManager
    Left = 472
    Top = 256
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedLeft = 107
      FloatClientWidth = 69
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButtonNov'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButtonBrisi'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn2'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 485
      FloatClientWidth = 120
      FloatClientHeight = 184
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 805
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 311
      DockedTop = 0
      FloatLeft = 964
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1035
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 50
          Visible = True
          ItemName = 'ComboBoxGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPregled
      Caption = #1050#1088#1077#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object ComboBoxGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = ComboBoxGodinaChange
      Items.Strings = (
        #1057#1080#1090#1077
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030')
      ItemIndex = -1
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxbrlrgbtn2: TdxBarLargeButton
      Action = aIstorijaVraboten
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Top = 216
    inherited aAzuriraj: TAction
      ShortCut = 0
    end
    inherited aHelp: TAction
      OnExecute = aHelpExecute
    end
    inherited a1: TAction
      Caption = #1048#1089#1090#1086#1088#1080#1112#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
    end
    object aPregled: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aPregledExecute
    end
    object aDesignReport: TAction
      Caption = 'aDesignReport'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDesignReportExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
    object aIstorijaVraboten: TAction
      Caption = #1048#1089#1090#1086#1088#1080#1112#1072' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      ImageIndex = 58
      OnExecute = aIstorijaVrabotenExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40498.484047418980000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 376
    Top = 144
    PixelsPerInch = 96
  end
  object qStaroMesto: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select hv.id_rm_re'
      'from hr_vraboten_rm hv'
      
        'where hv.id_re_firma = :firma and hv.mb = :mb and (current_date ' +
        'between hv.datum_od and coalesce(hv.datum_do, current_date))')
    Left = 464
    Top = 232
  end
  object tblRMRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select hrr.id,'
      '       hrm.naziv naziv_rm'
      'from hr_rm_re hrr'
      'inner join hr_sistematizacija hs on hs.id=hrr.id_sistematizacija'
      'inner join hr_rabotno_mesto hrm on hrm.id=hrr.id_rm'
      'where  hs.id_re_firma = :firma and hrr.id_re_firma = :firma '
      '-- coalesce(hs.do_datum,'#39'11.11.1111'#39') like :datum and')
    AutoUpdateOptions.UpdateTableName = 'HR_RM_RE'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_HR_RM_RE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 616
    Top = 235
    oRefreshDeletedRecord = True
    object tblRMREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRMRENAZIV_RM: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1056#1052
      FieldName = 'NAZIV_RM'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRMRE: TDataSource
    DataSet = tblRMRE
    Left = 715
    Top = 203
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 912
    Top = 240
  end
  object qID_VRABOTEN_RM: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select r.id'
      '     from hr_vraboten_rm r'
      
        '     where r.mb = :mb and r.id_re_firma = :firma and ((r.datum_d' +
        'o is null)or(r.datum_do >:datum_do))')
    Left = 576
    Top = 152
  end
  object pFIBQuery1: TpFIBQuery
    Left = 832
    Top = 112
  end
  object qPROMENA_ID: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select ID_PROMENA '
      'from hr_resenie_otkaz'
      'where id = :id')
    Left = 560
    Top = 88
  end
end
