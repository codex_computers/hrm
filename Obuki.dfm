object frmObuki: TfrmObuki
  Left = 0
  Top = 0
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1086#1073#1091#1082#1080
  ClientHeight = 786
  ClientWidth = 1276
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1276
    Height = 637
    Align = alClient
    TabOrder = 0
    TabStop = False
    Properties.ActivePage = tcPlanOtsustvo
    ClientRectBottom = 637
    ClientRectRight = 1276
    ClientRectTop = 24
    object tcPlanOtsustvo: TcxTabSheet
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1086#1073#1091#1082#1080
      ImageIndex = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1276
        Height = 308
        Align = alTop
        Enabled = False
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          1276
          308)
        object Label15: TLabel
          Left = 14
          Top = 20
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1053#1072#1079#1080#1074' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 718
          Top = 66
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label6: TLabel
          Left = 16
          Top = 139
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1090#1072#1090#1091#1089' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label16: TLabel
          Left = 250
          Top = 257
          Width = 72
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072':'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label7: TLabel
          Left = 688
          Top = 268
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1086#1082#1091#1084#1077#1085#1090' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Sifra: TcxDBTextEdit
          Tag = 1
          Left = 774
          Top = 63
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dmOtsustvo.dsObuki
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 1
          Visible = False
          Width = 113
        end
        object cxGroupBox1: TcxGroupBox
          Left = 55
          Top = 44
          Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1086#1073#1091#1082#1072
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 2
          Height = 82
          Width = 204
          object Label3: TLabel
            Left = -9
            Top = 22
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1076' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 12
            Top = 49
            Width = 29
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1086' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DO_VREME: TcxDBDateEdit
            Tag = 1
            Left = 47
            Top = 46
            BeepOnEnter = False
            DataBinding.DataField = 'PERIOD_DO'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = DO_VREMEExit
            OnKeyDown = EnterKakoTab
            Width = 146
          end
          object OD_VREME: TcxDBDateEdit
            Tag = 1
            Left = 47
            Top = 19
            BeepOnEnter = False
            DataBinding.DataField = 'PERIOD_OD'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            Properties.DateButtons = [btnClear, btnToday]
            Properties.InputKind = ikRegExpr
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = OD_VREMEExit
            OnKeyDown = EnterKakoTab
            Width = 146
          end
        end
        object STATUS: TcxDBLookupComboBox
          Tag = 1
          Left = 102
          Top = 136
          Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1086#1073#1091#1082#1072
          BeepOnEnter = False
          DataBinding.DataField = 'STATUS'
          DataBinding.DataSource = dmOtsustvo.dsObuki
          ParentShowHint = False
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmOtsustvo.dsStatus
          ShowHint = True
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 146
        end
        object buttonOtkazi: TcxButton
          Left = 1168
          Top = 260
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          TabOrder = 9
          OnKeyDown = EnterKakoTab
        end
        object buttonZapisi: TcxButton
          Left = 1087
          Top = 260
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          TabOrder = 8
          OnKeyDown = EnterKakoTab
        end
        object cxGroupBox2: TcxGroupBox
          Left = 274
          Top = 44
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = #1050#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1085#1072' '#1086#1073#1091#1082#1072
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 4
          DesignSize = (
            775
            195)
          Height = 195
          Width = 775
          object Label2: TLabel
            Left = -54
            Top = 31
            Width = 102
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1087#1080#1089' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TLabel
            Left = -54
            Top = 106
            Width = 102
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1062#1077#1083' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label10: TLabel
            Left = 384
            Top = 25
            Width = 102
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1055#1086#1076#1075#1086#1090#1074#1077#1085#1086#1089#1090':'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 392
            ExplicitTop = 30
          end
          object Label11: TLabel
            Left = 384
            Top = 71
            Width = 102
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1054#1076#1075#1086#1074#1086#1088#1085#1086
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 392
            ExplicitTop = 76
          end
          object Label12: TLabel
            Left = 384
            Top = 83
            Width = 102
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1083#1080#1094#1077':'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 392
            ExplicitTop = 88
          end
          object Label13: TLabel
            Left = 384
            Top = 116
            Width = 102
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1052#1077#1089#1090#1086'/'#1040#1076#1088#1077#1089#1072':'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 392
            ExplicitTop = 121
          end
          object Label14: TLabel
            Left = 414
            Top = 163
            Width = 72
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1057#1088#1077#1076#1089#1090#1074#1072': '
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitLeft = 422
            ExplicitTop = 168
          end
          object OPIS: TcxDBMemo
            Left = 54
            Top = 28
            Hint = 
              #1054#1087#1080#1089'/'#1055#1088#1086#1075#1088#1072#1084#1072' '#1085#1072' '#1086#1073#1091#1082#1072#1090#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082 +
              #1089#1090#1086#1090
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            ParentFont = False
            ParentShowHint = False
            Properties.ScrollBars = ssVertical
            Properties.WantReturns = False
            ShowHint = True
            TabOrder = 0
            OnDblClick = OPISDblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 64
            Width = 320
          end
          object CEL: TcxDBMemo
            Left = 54
            Top = 103
            Hint = 
              #1062#1077#1083' '#1082#1086#1112#1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1087#1086#1089#1090#1080#1075#1085#1077' '#1089#1086' '#1086#1073#1091#1082#1072#1090#1072',  '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1080'  '#1082#1086#1080' '#1089#1077' '#1076 +
              #1086#1073#1080#1074#1072#1072#1090' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'CEL'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            ParentFont = False
            ParentShowHint = False
            Properties.ScrollBars = ssVertical
            Properties.WantReturns = False
            ShowHint = True
            TabOrder = 1
            OnDblClick = CELDblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 64
            Width = 320
          end
          object PODGOTVENOST: TcxDBMemo
            Left = 492
            Top = 23
            Hint = 
              #1055#1086#1090#1088#1077#1073#1085#1072', '#1087#1088#1077#1076#1093#1086#1076#1085#1072' '#1087#1086#1076#1075#1086#1090#1074#1077#1085#1086#1089#1090' '#1085#1072' '#1091#1095#1077#1089#1085#1080#1094#1080#1090#1077' '#1074#1086' '#1086#1073#1091#1082#1072#1090#1072' *'#1050#1083#1080#1082#1085 +
              #1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
            Anchors = [akRight, akBottom]
            DataBinding.DataField = 'PODGOTVENOST'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            ParentFont = False
            ParentShowHint = False
            Properties.ScrollBars = ssVertical
            Properties.WantReturns = False
            ShowHint = True
            TabOrder = 2
            OnDblClick = PODGOTVENOSTDblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 40
            Width = 267
          end
          object ODGOVORNO_ICE: TcxDBMemo
            Left = 492
            Top = 68
            Hint = #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1086#1073#1091#1082#1072#1090#1072
            Anchors = [akRight, akBottom]
            DataBinding.DataField = 'ODGOVORNO_LICE'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            ParentFont = False
            ParentShowHint = False
            Properties.ScrollBars = ssVertical
            Properties.WantReturns = False
            ShowHint = True
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 40
            Width = 267
          end
          object MESTO: TcxDBMemo
            Left = 492
            Top = 114
            Hint = #1052#1077#1089#1090#1086'/'#1040#1076#1088#1077#1089#1072' '#1082#1072#1076#1077' '#1096#1090#1086' '#1089#1077' '#1086#1076#1088#1078#1091#1074#1072' '#1086#1073#1091#1082#1072#1090#1072
            Anchors = [akRight, akBottom]
            DataBinding.DataField = 'MESTO'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            ParentFont = False
            ParentShowHint = False
            Properties.ScrollBars = ssVertical
            Properties.WantReturns = False
            ShowHint = True
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 40
            Width = 267
          end
          object SREDSTVA: TcxDBTextEdit
            Left = 492
            Top = 160
            Hint = #1057#1088#1077#1076#1089#1090#1074#1072' '#1087#1086#1090#1088#1077#1073#1085#1080' '#1079#1072' '#1086#1073#1091#1082#1072#1090#1072', '#1074#1086' '#1076#1077#1085#1072#1088#1089#1082#1072' '#1074#1088#1077#1076#1085#1086#1089#1090
            Anchors = [akRight, akBottom]
            DataBinding.DataField = 'SREDSTVA'
            DataBinding.DataSource = dmOtsustvo.dsObuki
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
        end
        object NAZIV: TcxDBTextEdit
          Tag = 1
          Left = 100
          Top = 17
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1086#1073#1091#1082#1072
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataBinding.DataField = 'NAZIV'
          DataBinding.DataSource = dmOtsustvo.dsObuki
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 487
        end
        object ZABELESKA: TcxDBMemo
          Left = 328
          Top = 256
          Hint = 
            #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1086#1073#1091#1082#1072#1090#1072' ('#1089#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072' '#1087#1086' '#1079#1072#1074#1088#1096#1091#1074#1072#1114#1077#1090#1086' '#1085#1072' '#1086#1073#1091#1082#1072#1090#1072') *' +
            #1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataBinding.DataField = 'ZABELESKA'
          DataBinding.DataSource = dmOtsustvo.dsObuki
          ParentFont = False
          ParentShowHint = False
          Properties.ScrollBars = ssVertical
          Properties.WantReturns = False
          ShowHint = True
          TabOrder = 5
          OnDblClick = ZABELESKADblClick
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 34
          Width = 320
        end
        object Dokument: TcxDBTextEdit
          Left = 774
          Top = 265
          Hint = #1055#1088#1077#1074#1079#1077#1084#1077#1090#1077' '#1112#1072' '#1083#1086#1082#1072#1094#1080#1112#1072#1090#1072' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1090
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataBinding.DataField = 'DOKUMENT'
          DataBinding.DataSource = dmOtsustvo.dsObuki
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 187
        end
        object cxButton1: TcxButton
          Left = 967
          Top = 260
          Width = 66
          Height = 25
          Action = aOtvoridDokument
          Anchors = [akRight, akBottom]
          TabOrder = 7
          OnKeyDown = EnterKakoTab
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 308
        Width = 1276
        Height = 305
        Align = alClient
        Caption = 'Panel4'
        TabOrder = 1
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 1274
          Height = 303
          Align = alClient
          TabOrder = 0
          ExplicitLeft = 250
          ExplicitTop = -32
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyDown = cxGrid1DBTableView1KeyDown
            DataController.DataSource = dmOtsustvo.dsObuki
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.CellHints = True
            OptionsBehavior.ImmediateEditor = False
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV'
              Width = 229
            end
            object cxGrid1DBTableView1PERIOD_OD: TcxGridDBColumn
              DataBinding.FieldName = 'PERIOD_OD'
              Width = 104
            end
            object cxGrid1DBTableView1PERIOD_DO: TcxGridDBColumn
              DataBinding.FieldName = 'PERIOD_DO'
              Width = 93
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              Visible = False
            end
            object cxGrid1DBTableView1STATUSNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'STATUSNAZIV'
              Width = 99
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Width = 170
            end
            object cxGrid1DBTableView1CEL: TcxGridDBColumn
              DataBinding.FieldName = 'CEL'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Width = 104
            end
            object cxGrid1DBTableView1ODGOVORNO_LICE: TcxGridDBColumn
              DataBinding.FieldName = 'ODGOVORNO_LICE'
              Width = 134
            end
            object cxGrid1DBTableView1MESTO: TcxGridDBColumn
              DataBinding.FieldName = 'MESTO'
              Width = 130
            end
            object cxGrid1DBTableView1PODGOTVENOST: TcxGridDBColumn
              DataBinding.FieldName = 'PODGOTVENOST'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Width = 137
            end
            object cxGrid1DBTableView1SREDSTVA: TcxGridDBColumn
              DataBinding.FieldName = 'SREDSTVA'
            end
            object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
              DataBinding.FieldName = 'ZABELESKA'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Width = 58
            end
            object cxGrid1DBTableView1DOKUMENT: TcxGridDBColumn
              DataBinding.FieldName = 'DOKUMENT'
              Width = 200
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object tcPregled: TcxTabSheet
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1086#1073#1091#1082#1080
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 81
        Width = 1276
        Height = 532
        Align = alClient
        TabOrder = 0
        object cxSchedulerPlanOtsustva: TcxScheduler
          Left = 1
          Top = 1
          Width = 1274
          Height = 530
          DateNavigator.RowCount = 2
          ViewDay.AlwaysShowEventTime = True
          ViewDay.TimeRulerPopupMenu.Items = [rpmi60min, rpmi30min, rpmi15min, rpmi10min, rpmi6min, rpmi5min]
          ViewDay.WorkTimeOnly = True
          ViewGantt.EventDetailInfo = True
          ViewGantt.WorkDaysOnly = True
          ViewGantt.WorkTimeOnly = True
          ViewGantt.EventsStyle = esProgress
          ViewTimeGrid.WorkDaysOnly = True
          ViewTimeGrid.WorkTimeOnly = True
          ViewYear.Active = True
          ViewYear.MonthHeaderPopupMenu.PopupMenu = PopupMenu1
          ViewYear.MonthHeaderPopupMenu.Items = [mhpmiFullYear, mhpmiHalfYear, mhpmiQuarter]
          Align = alClient
          BevelInner = bvLowered
          BevelOuter = bvSpace
          ContentPopupMenu.PopupMenu = PopupMenu1
          ContentPopupMenu.UseBuiltInPopupMenu = False
          ContentPopupMenu.Items = [cpmiToday, cpmiGoToDate, cpmiGoToThisDay]
          ControlBox.Control = cxGrid2
          DialogsLookAndFeel.Kind = lfUltraFlat
          DialogsLookAndFeel.NativeStyle = False
          EventOperations.DialogShowing = False
          EventOperations.ReadOnly = True
          EventPopupMenu.PopupMenu = PopupMenu1
          EventPopupMenu.UseBuiltInPopupMenu = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          OptionsView.ShowHints = False
          OptionsView.WorkFinish = 0.666666666666666600
          Storage = cxSchedulerDBStorage1
          TabOrder = 0
          Splitters = {
            6A040000FB000000F90400000001000065040000010000006A04000011020000}
          StoredClientBounds = {0100000001000000F904000011020000}
          object cxGrid2: TcxGrid
            Left = 0
            Top = 0
            Width = 143
            Height = 273
            Align = alClient
            TabOrder = 0
            object cxGrid2DBTableView1: TcxGridDBTableView
              DataController.DataSource = dmOtsustvo.dsStatus
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.GroupByBox = False
              object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'NAZIV'
                Width = 107
              end
              object cxGrid2DBTableView1BOJA: TcxGridDBColumn
                DataBinding.FieldName = 'BOJA'
                PropertiesClassName = 'TcxColorComboBoxProperties'
                Properties.CustomColors = <>
                Width = 32
              end
            end
            object cxGrid2Level1: TcxGridLevel
              GridView = cxGrid2DBTableView1
            end
          end
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1276
        Height = 81
        ParentCustomHint = False
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object RadioGroup1: TRadioGroup
          Left = 19
          Top = 10
          Width = 375
          Height = 63
          Caption = #1055#1088#1077#1075#1083#1077#1076' '#1087#1086' :'
          TabOrder = 0
        end
        object RadioButton1: TRadioButton
          Left = 42
          Top = 44
          Width = 56
          Height = 17
          Caption = #1044#1077#1085#1086#1074#1080
          TabOrder = 1
          OnClick = RadioButton1Click
        end
        object RadioButton2: TRadioButton
          Left = 136
          Top = 44
          Width = 56
          Height = 17
          Caption = #1053#1077#1076#1077#1083#1072
          TabOrder = 2
          OnClick = RadioButton2Click
        end
        object RadioButton4: TRadioButton
          Left = 238
          Top = 44
          Width = 57
          Height = 17
          Caption = #1043#1086#1076#1080#1085#1072
          Checked = True
          TabOrder = 3
          TabStop = True
          OnClick = RadioButton4Click
        end
        object RadioButton6: TRadioButton
          Left = 341
          Top = 44
          Width = 41
          Height = 17
          Caption = #1043#1088#1080#1076
          TabOrder = 4
          OnClick = RadioButton6Click
        end
        object cxRadioGroup1: TcxRadioGroup
          Left = 400
          Top = 10
          Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086' :'
          ParentColor = False
          ParentFont = False
          Properties.Items = <>
          Style.LookAndFeel.Kind = lfOffice11
          Style.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.Kind = lfOffice11
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfOffice11
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfOffice11
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 5
          Height = 63
          Width = 375
        end
        object statusprebaraj: TcxLookupComboBox
          Left = 475
          Top = 41
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmOtsustvo.dsStatus
          Properties.OnEditValueChanged = statusprebarajPropertiesEditValueChanged
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          Width = 194
        end
        object ButtonIscisti: TcxButton
          Left = 675
          Top = 39
          Width = 75
          Height = 25
          Action = aIscisti
          TabOrder = 7
        end
        object cxLabel1: TcxLabel
          Left = 423
          Top = 44
          Caption = #1057#1090#1072#1090#1091#1089' :'
        end
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1276
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 5
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxPodesuvanje: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object dxRibbonStatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 763
    Width = 1276
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', I' +
          'nsert -  '#1059#1095#1077#1089#1085#1080#1094#1080' '#1074#1086' '#1086#1073#1091#1082#1072', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 944
    Top = 144
    object aEvidentirajObuka: TAction
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1087#1083#1072#1085' '#1079#1072' '#1086#1090#1089#1091#1089#1090#1074#1086
      OnExecute = aEvidentirajObukaExecute
    end
    object aDodadiPlanZaOtsustvo: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aDodadiPlanZaOtsustvoExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziExecute
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aIzlezExecute
    end
    object aAzurirajPlan: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajPlanExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aOsvezi: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      OnExecute = aOsveziExecute
    end
    object aSnimiGoIzgledot: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiGoIzgledotExecute
    end
    object aZacuvajVoExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajVoExcelExecute
    end
    object aPecatenje: TAction
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      ImageIndex = 30
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aStatusGrupa: TAction
      Caption = 'aStatusGrupa'
    end
    object aStatus: TAction
      Caption = 'aStatus'
    end
    object aPrebaraj: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
    end
    object aIscisti: TAction
      Caption = #1048#1089#1095#1080#1089#1090#1080
      ImageIndex = 22
      OnExecute = aIscistiExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPomos: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
      ShortCut = 112
    end
    object aUcesnici: TAction
      Caption = #1044#1086#1076#1072#1076#1080'/'#1040#1078#1091#1088#1080#1088#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 27
      OnExecute = aUcesniciExecute
    end
    object aObukiPregled: TAction
      Caption = 'aObukiPregled'
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aOtvoridDokument: TAction
      Caption = '...'
      OnExecute = aOtvoridDokumentExecute
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 808
    Top = 80
  end
  object cxSchedulerDBStorage1: TcxSchedulerDBStorage
    Resources.Items = <>
    CustomFields = <>
    DataSource = dmOtsustvo.dsObuki
    FieldNames.ActualFinish = 'PERIOD_DO'
    FieldNames.ActualStart = 'PERIOD_OD'
    FieldNames.Caption = 'NAZIV'
    FieldNames.EventType = 'EVENT_TYPE'
    FieldNames.Finish = 'PERIOD_DO'
    FieldNames.ID = 'ID'
    FieldNames.LabelColor = 'STATUS_BOJA'
    FieldNames.Message = 'OPIS'
    FieldNames.Options = 'OPTIONS'
    FieldNames.ParentID = 'ID'
    FieldNames.Start = 'PERIOD_OD'
    FieldNames.TaskLinksField = 'TASK_LINKS_FIELD'
    Left = 840
    Top = 24
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = dm.cxLargeImages
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 736
    Top = 112
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1139
      FloatTop = 8
      FloatClientWidth = 133
      FloatClientHeight = 208
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1054#1073#1091#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1176
      FloatTop = 8
      FloatClientWidth = 64
      FloatClientHeight = 208
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 372
      DockedTop = 0
      FloatLeft = 1176
      FloatTop = 8
      FloatClientWidth = 111
      FloatClientHeight = 123
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton34'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton35'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem5'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 579
      DockedTop = 0
      FloatLeft = 1176
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 52
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1059#1095#1077#1089#1085#1080#1094#1080' '#1074#1086' '#1086#1073#1091#1082#1072
      CaptionButtons = <>
      DockedLeft = 189
      DockedTop = 0
      FloatLeft = 1310
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton36'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 35
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 42
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aDodadiPlanZaOtsustvo
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aPecatenje
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarButton3: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end>
    end
    object dxBarButton4: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aStatusGrupa
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aStatus
      Category = 0
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.ListColumns = <>
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end>
    end
    object dxBarButton5: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aDodadiPlanZaOtsustvo
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aAzurirajPlan
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aOsvezi
      Category = 0
    end
    object cxBarEditRabEdinica: TcxBarEditItem
      Caption = #1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072
      Category = 0
      Hint = #1056#1072#1073'. '#1077#1076#1080#1085#1080#1094#1072
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'ID'
        end
        item
          Width = 650
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmOtsustvo.dsPodsektori
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end>
    end
    object dxBarButton7: TdxBarButton
      Action = aPecatiTabela
      Category = 0
      LargeImageIndex = 30
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aPomos
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton32: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditVraboten: TcxBarEditItem
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085
      Category = 0
      Hint = #1042#1088#1072#1073#1086#1090#1077#1085
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'MB'
      Properties.ListColumns = <
        item
          Width = 350
          FieldName = 'MB'
        end
        item
          FieldName = 'VRABOTENNAZIV'
        end>
    end
    object dxBarLargeButton33: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton34: TdxBarLargeButton
      Action = aSnimiGoIzgledot
      Category = 0
    end
    object dxBarLargeButton35: TdxBarLargeButton
      Action = aZacuvajVoExcel
      Category = 0
    end
    object dxBarLargeButton36: TdxBarLargeButton
      Action = aUcesnici
      Category = 0
    end
    object dxBarLargeButton37: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton38: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton39: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton8: TdxBarButton
      Action = aObukiPregled
      Category = 0
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 904
    Top = 32
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40906.495068217590000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 656
    Top = 48
    object N1: TMenuItem
      Action = aEvidentirajObuka
      Caption = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1054#1073#1091#1082#1072
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 872
    Top = 112
  end
  object OpenDialog1: TOpenDialog
    InitialDir = 'C:\'
    Left = 512
    Top = 233
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
