object frmSpecijalizacija: TfrmSpecijalizacija
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1089#1087#1077#1094#1080#1112#1072#1083#1080#1089#1090#1080
  ClientHeight = 656
  ClientWidth = 1010
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1010
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 633
    Width = 1010
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1010
    Height = 507
    Align = alClient
    TabOrder = 6
    Properties.ActivePage = cxTabSheet1
    ClientRectBottom = 507
    ClientRectRight = 1010
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 0
      object lPanel: TPanel
        Left = 0
        Top = 249
        Width = 1010
        Height = 234
        Align = alClient
        BevelInner = bvLowered
        BevelOuter = bvSpace
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 2
          Top = 2
          Width = 1006
          Height = 230
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            DataController.DataSource = dm.dsDogSpecijalisti
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 56
            end
            object cxGrid1DBTableView1MB: TcxGridDBColumn
              DataBinding.FieldName = 'MB'
              Width = 80
            end
            object cxGrid1DBTableView1NAZIVVRABOTEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN_TI'
              Width = 100
            end
            object cxGrid1DBTableView1NAZIV_VRABOTEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_VRABOTEN'
              Visible = False
              Width = 250
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 106
            end
            object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_OD'
              Width = 85
            end
            object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_DO'
              Width = 84
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              Visible = False
            end
            object cxGrid1DBTableView1NAZIVSTATUS: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIVSTATUS'
              Width = 136
            end
            object cxGrid1DBTableView1DIREKTOR: TcxGridDBColumn
              DataBinding.FieldName = 'DIREKTOR'
              Width = 100
            end
            object cxGrid1DBTableView1MESTO: TcxGridDBColumn
              DataBinding.FieldName = 'MESTO'
              Width = 85
            end
            object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV'
              Width = 67
            end
            object cxGrid1DBTableView1HIPOTEKA: TcxGridDBColumn
              DataBinding.FieldName = 'HIPOTEKA'
              Width = 68
            end
            object cxGrid1DBTableView1USLOVI: TcxGridDBColumn
              DataBinding.FieldName = 'USLOVI'
              Width = 37
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 100
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 100
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object dPanel: TPanel
        Left = 0
        Top = 0
        Width = 1010
        Height = 249
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        Enabled = False
        TabOrder = 1
        DesignSize = (
          1010
          249)
        object Label8: TLabel
          Left = 247
          Top = 14
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label3: TLabel
          Left = 17
          Top = 49
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 17
          Top = 76
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 48
          Top = 20
          Width = 58
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object labelaa: TLabel
          Left = 537
          Top = 44
          Width = 89
          Height = 29
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1048#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072', '#1084#1077#1089#1090#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label9: TLabel
          Left = 537
          Top = 22
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1053#1072#1079#1080#1074' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 537
          Top = 95
          Width = 89
          Height = 29
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1086#1087#1089#1090#1074#1077#1085#1080#1082' '#1085#1072' '#1093#1080#1087#1086#1090#1077#1082#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label7: TLabel
          Left = 537
          Top = 76
          Width = 89
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1080#1088#1077#1082#1090#1086#1088' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 537
          Top = 127
          Width = 89
          Height = 29
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1059#1089#1083#1086#1074#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label10: TLabel
          Left = 26
          Top = 169
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1090#1072#1090#1091#1089' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object SIFRA: TcxDBTextEdit
          Tag = 1
          Left = 342
          Top = 14
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clBtnText
          TabOrder = 2
          Visible = False
          Width = 123
        end
        object Broj: TcxDBTextEdit
          Left = 112
          Top = 46
          Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          BeepOnEnter = False
          DataBinding.DataField = 'BROJ'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object MB: TcxDBTextEdit
          Tag = 1
          Left = 112
          Top = 73
          Hint = #1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 123
        end
        object VRABOTENIME: TcxDBLookupComboBox
          Tag = 1
          Left = 236
          Top = 73
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
          BeepOnEnter = False
          DataBinding.DataField = 'MB'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          Properties.DropDownListStyle = lsFixedList
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'MB'
          Properties.ListColumns = <
            item
              Width = 500
              FieldName = 'MB'
            end
            item
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1083#1080#1094#1077
              Width = 800
              FieldName = 'NAZIV_VRABOTEN_TI'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsViewVraboteni
          StyleDisabled.TextColor = clDefault
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 273
        end
        object cxGroupBox2: TcxGroupBox
          Left = 112
          Top = 100
          Hint = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1090#1088#1072#1077#1114#1077' '#1085#1072' '#1089#1087#1077#1094#1080#1112#1072#1083#1080#1079#1072#1094#1080#1112#1072#1090#1072
          Caption = #1055#1077#1088#1080#1086#1076
          TabOrder = 5
          Height = 60
          Width = 397
          object Label4: TLabel
            Left = -9
            Top = 28
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1076' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label6: TLabel
            Left = 192
            Top = 28
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1086' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DatumOd: TcxDBDateEdit
            Tag = 1
            Left = 47
            Top = 25
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_OD'
            DataBinding.DataSource = dm.dsDogSpecijalisti
            ParentShowHint = False
            Properties.DateButtons = [btnClear, btnToday]
            Properties.InputKind = ikMask
            ShowHint = True
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = DatumOdExit
            OnKeyDown = EnterKakoTab
            Width = 123
          end
          object DatumDo: TcxDBDateEdit
            Tag = 1
            Left = 248
            Top = 25
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_DO'
            DataBinding.DataSource = dm.dsDogSpecijalisti
            ParentShowHint = False
            Properties.DateButtons = [btnClear, btnToday]
            Properties.InputKind = ikMask
            ShowHint = True
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = DatumDoExit
            OnKeyDown = EnterKakoTab
            Width = 123
          end
        end
        object mesto: TcxDBTextEdit
          Tag = 1
          Left = 632
          Top = 46
          Hint = #1048#1085#1089#1090#1080#1090#1091#1094#1080#1112#1072' '#1080' '#1084#1077#1089#1090#1086' '#1082#1072#1076#1077' '#1096#1090#1086' '#1089#1077' '#1086#1076#1074#1080#1074#1072' '#1089#1087#1077#1094#1080#1112#1072#1083#1080#1079#1072#1094#1080#1112#1072#1090#1072
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'MESTO'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 8
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 322
        end
        object naziv: TcxDBTextEdit
          Tag = 1
          Left = 632
          Top = 19
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1087#1077#1094#1080#1112#1072#1083#1080#1079#1072#1094#1080#1112#1072
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'NAZIV'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 7
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 322
        end
        object hipoteka: TcxDBTextEdit
          Left = 632
          Top = 100
          Hint = #1057#1086#1087#1089#1090#1074#1077#1085#1080#1082' '#1085#1072' '#1093#1080#1087#1086#1090#1077#1082#1072#1090#1072
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'HIPOTEKA'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 10
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 322
        end
        object DATUM: TcxDBDateEdit
          Tag = 1
          Left = 112
          Top = 19
          Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          DataBinding.DataField = 'DATUM'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object direktor: TcxDBTextEdit
          Tag = 1
          Left = 632
          Top = 73
          Hint = #1044#1080#1088#1077#1082#1090#1086#1088' '#1082#1086#1112' '#1112#1072' '#1086#1076#1086#1073#1088#1091#1074#1072' '#1089#1087#1077#1094#1080#1112#1072#1083#1080#1079#1072#1094#1080#1112#1072#1090#1072
          Anchors = [akLeft, akTop, akRight, akBottom]
          BeepOnEnter = False
          DataBinding.DataField = 'DIREKTOR'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          StyleDisabled.TextColor = clDefault
          TabOrder = 9
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 322
        end
        object ZapisiButton: TcxButton
          Left = 798
          Top = 210
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 12
        end
        object OtkaziButton: TcxButton
          Left = 879
          Top = 210
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 13
        end
        object Uslovi: TcxDBMemo
          Left = 632
          Top = 127
          Hint = 
            #1054#1087#1080#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1086#1090#1086#1054#1089#1090#1072#1085#1072#1090#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1080#1083#1080' '#1091#1089#1083#1086#1074#1080' '#1079#1072' '#1079#1072' '#1089#1087#1077#1094#1080#1112#1072#1083#1080#1079 +
            #1072#1094#1080#1112#1072#1090#1072'.  '#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072#1087#1072#1090#1080', '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090'!'
          Anchors = [akLeft, akTop, akRight, akBottom]
          DataBinding.DataField = 'USLOVI'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          TabOrder = 11
          OnDblClick = aUrediTekstUsloviExecute
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 77
          Width = 322
        end
        object STATUS: TcxDBLookupComboBox
          Left = 112
          Top = 166
          Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1057#1087#1077#1094#1080#1112#1072#1083#1080#1079#1072#1094#1080#1112#1072
          BeepOnEnter = False
          DataBinding.DataField = 'STATUS'
          DataBinding.DataSource = dm.dsDogSpecijalisti
          ParentShowHint = False
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmOtsustvo.dsStatus
          ShowHint = True
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 146
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 248
    Top = 160
  end
  object PopupMenu1: TPopupMenu
    Left = 344
    Top = 152
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 424
    Top = 192
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 382
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 632
    Top = 152
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aUrediTekstUslovi: TAction
      Caption = 'aUrediTekstUslovi'
      OnExecute = aUrediTekstUsloviExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 552
    Top = 88
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40906.503652673610000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 544
    Top = 48
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxSchedulerDBStorage1: TcxSchedulerDBStorage
    Resources.Items = <>
    CustomFields = <>
    DataSource = dm.dsDogSpecijalisti
    FieldNames.ActualFinish = 'DATUM_DO'
    FieldNames.ActualStart = 'DATUM_OD'
    FieldNames.Caption = 'OPIS'
    FieldNames.EventType = 'EVENT_TYPE'
    FieldNames.Finish = 'DO_VREME'
    FieldNames.ID = 'ID'
    FieldNames.Message = 'OPIS'
    FieldNames.Options = 'OPTIONS'
    FieldNames.ParentID = 'ID'
    FieldNames.Start = 'DATUM_OD'
    FieldNames.TaskLinksField = 'TASK_LINKS_FIELD'
    Left = 512
    Top = 184
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
end
