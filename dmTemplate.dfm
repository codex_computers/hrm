object dmT: TdmT
  OldCreateOrder = False
  Height = 514
  Width = 681
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Tahoma'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    TemplateDir = 'Template'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnSaveReport = frxDesigner1SaveReport
    Left = 136
    Top = 40
  end
  object frxDogovorVrabotuvanje: TfrxDBDataset
    UserName = #1044#1086#1075#1086#1074#1086#1088#1047#1072#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'VID_DOKUMENT='#1042#1080#1076#1044#1086#1082#1091#1084#1077#1085#1090'('#1064#1080#1092#1088#1072')'
      'NAZIV_VID_DOKUMENT='#1042#1080#1076#1053#1072#1044#1086#1082#1091#1084#1077#1085#1090
      'MB='#1052#1072#1090#1080#1095#1077#1085#1041#1088#1086#1112
      'ID_RE_FIRMA='#1064#1080#1092#1088#1072#1053#1072#1060#1080#1088#1084#1072
      'PREZIME='#1055#1088#1077#1079#1080#1084#1077
      'TATKOVO_IME='#1058#1072#1090#1082#1086#1074#1086#1048#1084#1077
      'IME='#1048#1084#1077
      'ADRESA='#1040#1076#1088#1077#1089#1072
      'MESTO='#1052#1077#1089#1090#1086'('#1064#1080#1092#1088#1072')'
      'STEPEN_STRUCNA_PODGOTOVKA='#1057#1090#1077#1087#1077#1085#1053#1072#1057#1090#1088#1091#1095#1085#1072#1055#1086#1076#1075#1086#1090#1086#1074#1082#1072
      'VID_VRABOTUVANJE='#1042#1080#1076#1053#1072#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077'('#1064#1080#1092#1088#1072')'
      'NAZIV_VID_VRAB='#1042#1080#1076#1053#1072#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      'DATUM_OD='#1044#1072#1090#1091#1084#1054#1076
      'DATUM_DO='#1044#1072#1090#1091#1084#1044#1086
      'RABOTNO_VREME='#1041#1088#1086#1112#1053#1072#1056#1072#1073#1086#1090#1085#1080#1063#1072#1089#1086#1074#1080
      'RABOTNO_MESTO='#1064#1080#1092#1088#1072#1053#1072#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'NAZIV_MESTO='#1052#1077#1089#1090#1086
      'OBRAZOVANIE='#1057#1057#1055'('#1064#1080#1092#1088#1072')'
      'RABMESTONAZIV='#1056#1072#1073#1086#1090#1085#1086#1052#1077#1089#1090#1086
      'MB_DIREKTOR='#1044#1080#1088#1077#1082#1090#1086#1088
      'PLATA='#1055#1083#1072#1090#1072
      'STATUS=STATUS'
      'DATA=DATA')
    DataSet = DogovorZaVrabotuvanje
    BCDToCurrency = False
    Left = 232
    Top = 40
  end
  object DogovorZaVrabotuvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_DOGOVOR_VRABOTUVANJE'
      'SET '
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    MB = :MB,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    PREZIME = :PREZIME,'
      '    TATKOVO_IME = :TATKOVO_IME,'
      '    IME = :IME,'
      '    ADRESA = :ADRESA,'
      '    MESTO = :MESTO,'
      '    STEPEN_STRUCNA_PODGOTOVKA = :STEPEN_STRUCNA_PODGOTOVKA,'
      '    VID_VRABOTUVANJE = :VID_VRABOTUVANJE,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    RABOTNO_VREME = :RABOTNO_VREME,'
      '    RABOTNO_MESTO = :RABOTNO_MESTO,'
      '    MB_DIREKTOR = :MB_DIREKTOR,'
      '    PLATA = :PLATA,'
      '    STATUS = :STATUS,'
      '    DATA = :DATA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_UPD = :USR_UPD,'
      '    USR_INS = :USR_INS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    hdv.id,'
      '    hdv.vid_dokument,'
      '    hvd.naziv naziv_vid_dokument,'
      '    hdv.mb,'
      '    hdv.id_re_firma,'
      '    hdv.prezime,'
      '    hdv.tatkovo_ime,'
      '    hdv.ime,'
      '    hdv.adresa,'
      '    hdv.mesto,'
      '    hdv.stepen_strucna_podgotovka,'
      '    hdv.vid_vrabotuvanje,'
      '    hvv.naziv naziv_vid_vrab,'
      '    hdv.datum_od,'
      '    hdv.datum_do,'
      '    hdv.rabotno_vreme,'
      '    hdv.rabotno_mesto,'
      '    mm.naziv naziv_mesto,'
      '    po.opis obrazovanie,'
      '    hrmrab.naziv as rabMestoNaziv,'
      '    hdv.mb_direktor,'
      '    hdv.plata,'
      '    hdv.status,'
      '    hdv.data,'
      '    hdv.ts_ins,'
      '    hdv.ts_upd,'
      '    hdv.usr_upd,'
      '    hdv.usr_ins'
      'from hr_dogovor_vrabotuvanje hdv'
      'inner join hr_vid_dokument hvd on hvd.id=hdv.vid_dokument'
      'left outer join mat_mesto mm on mm.id=hdv.mesto'
      
        'inner join plt_obrazovanie po on po.id=hdv.stepen_strucna_podgot' +
        'ovka'
      'inner join hr_rm_re hrm on hrm.id = hdv.rabotno_mesto'
      'inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id_rm'
      
        'inner join hr_sistematizacija hs on hs.id = hrm.id_sistematizaci' +
        'ja'
      
        'inner join hr_vid_vrabotuvanje hvv on hvv.id=hdv.vid_vrabotuvanj' +
        'e'
      
        'where (hs.do_datum is null and hs.id_re_firma = :firma) and (hdv' +
        '.mb like :mb and hdv.id_re_firma =:firma  and hdv.datum_od = (se' +
        'lect max(hdv1.datum_od) from hr_dogovor_vrabotuvanje hdv1'
      '                       where (hdv1.id=hdv.id)))'
      ''
      'order by hdv.datum_od')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 360
    Top = 40
    object DogovorZaVrabotuvanjeID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DogovorZaVrabotuvanjeVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object DogovorZaVrabotuvanjeNAZIV_VID_DOKUMENT: TFIBStringField
      FieldName = 'NAZIV_VID_DOKUMENT'
      Size = 200
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeID_RE_FIRMA: TFIBIntegerField
      FieldName = 'ID_RE_FIRMA'
    end
    object DogovorZaVrabotuvanjePREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 50
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeTATKOVO_IME: TFIBStringField
      FieldName = 'TATKOVO_IME'
      Size = 50
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeIME: TFIBStringField
      FieldName = 'IME'
      Size = 50
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeMESTO: TFIBIntegerField
      FieldName = 'MESTO'
    end
    object DogovorZaVrabotuvanjeSTEPEN_STRUCNA_PODGOTOVKA: TFIBIntegerField
      FieldName = 'STEPEN_STRUCNA_PODGOTOVKA'
    end
    object DogovorZaVrabotuvanjeVID_VRABOTUVANJE: TFIBIntegerField
      FieldName = 'VID_VRABOTUVANJE'
    end
    object DogovorZaVrabotuvanjeNAZIV_VID_VRAB: TFIBStringField
      FieldName = 'NAZIV_VID_VRAB'
      Size = 200
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DogovorZaVrabotuvanjeDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DogovorZaVrabotuvanjeRABOTNO_VREME: TFIBBCDField
      FieldName = 'RABOTNO_VREME'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
      RoundByScale = True
    end
    object DogovorZaVrabotuvanjeRABOTNO_MESTO: TFIBIntegerField
      FieldName = 'RABOTNO_MESTO'
    end
    object DogovorZaVrabotuvanjeNAZIV_MESTO: TFIBStringField
      FieldName = 'NAZIV_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeOBRAZOVANIE: TFIBStringField
      FieldName = 'OBRAZOVANIE'
      Size = 30
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeRABMESTONAZIV: TFIBStringField
      FieldName = 'RABMESTONAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjeMB_DIREKTOR: TFIBStringField
      FieldName = 'MB_DIREKTOR'
      Size = 100
      EmptyStrToNull = True
    end
    object DogovorZaVrabotuvanjePLATA: TFIBBCDField
      FieldName = 'PLATA'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
      RoundByScale = True
    end
    object DogovorZaVrabotuvanjeSTATUS: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'STATUS'
    end
    object DogovorZaVrabotuvanjeDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    DefaultPath = 'template'
    ShowProgress = True
    OverwritePrompt = False
    ExportEMF = True
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 64
    Top = 160
  end
  object frxReport1: TfrxReport
    Version = '4.9.100'
    DotMatrixReport = False
    EngineOptions.TempDir = 'Template'
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40519.596180625000000000
    ReportOptions.LastChange = 40525.547517037030000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '                                                                ' +
        '                   '
      'end;'
      ''
      'procedure richFileOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '               '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 64
    Top = 40
    Datasets = <
      item
        DataSet = dm.frxFirma
        DataSetName = 'frxFirma'
      end
      item
        DataSet = frxBaranjeZaGO
        DataSetName = #1041#1072#1088#1072#1114#1077#1047#1072#1043#1054
      end
      item
        DataSet = frxDogovorVrabotuvanje
        DataSetName = #1044#1086#1075#1086#1074#1086#1088#1047#1072#1042#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      end
      item
        DataSet = frxPrekinRabotenOdnos
        DataSetName = #1055#1088#1077#1082#1080#1085#1053#1072#1056#1072#1073#1086#1090#1077#1085#1054#1076#1085#1086#1089
      end
      item
        DataSet = frxResenieGO
        DataSetName = #1056#1077#1096#1077#1085#1080#1077#1047#1072#1043#1054
      end
      item
        DataSet = frxResenieZaPreraspredelba
        DataSetName = #1056#1077#1096#1077#1085#1080#1077#1047#1072#1055#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 733.228820000000000000
        Top = 18.897650000000000000
        Width = 642.520100000000000000
        RowCount = 0
        Stretched = True
        object richFile: TfrxRichView
          Width = 642.520100000000000000
          Height = 37.795300000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235315C64656666305C6465
            666C616E67313037317B5C666F6E7474626C7B5C66305C666E696C5C66636861
            72736574323034205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F72
            204D7366746564697420352E34312E32312E323530393B7D5C766965776B696E
            64345C7563315C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
      end
    end
  end
  object Template: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select hrt.id, hrt.vid, hrt.broj, hrt.naziv, hrt.report, hrt.ts_' +
        'ins, hrt.ts_upd, hrt.usr_upd, hrt.usr_ins'
      'from hr_report_tamplate hrt'
      'where hrt.broj = :broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 64
    Top = 104
    object TemplateID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TemplateVID: TFIBIntegerField
      FieldName = 'VID'
    end
    object TemplateNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 1024
      EmptyStrToNull = True
    end
    object TemplateREPORT: TFIBBlobField
      FieldName = 'REPORT'
      Size = 8
    end
    object TemplateTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object TemplateTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object TemplateUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 500
      EmptyStrToNull = True
    end
    object TemplateUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 500
      EmptyStrToNull = True
    end
    object TemplateBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
  end
  object dsTemplate: TDataSource
    DataSet = Template
    Left = 136
    Top = 104
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Rich Text  Format|*.rtf'
    InitialDir = '...\HRM\Template'
    Title = #1048#1079#1073#1077#1088#1077#1090#1077' '#1090#1077#1088#1082' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
    Left = 136
    Top = 160
  end
  object BaranjeZaGO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_BARANJE_GO'
      'SET '
      '    MB = :MB,'
      '    GODINA = :GODINA,'
      '    DATUM = :DATUM,'
      '    VID_DOCUMENT = :VID_DOCUMENT,'
      '    ODOBRENIE = :ODOBRENIE,'
      '    KORISTENJE = :KORISTENJE,'
      '    PRV_DEL = :PRV_DEL,'
      '    VTOR_DEL = :VTOR_DEL,'
      '    MB_RAKOVODITEL = :MB_RAKOVODITEL,'
      '    DATUM1_OD = :DATUM1_OD,'
      '    DATUM1_DO = :DATUM1_DO,'
      '    DATUM2_OD = :DATUM2_OD,'
      '    DATUM2_DO = :DATUM2_DO,'
      '    DATA = :DATA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_UPD = :USR_UPD,'
      '    USR_INS = :USR_INS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      '       select go.id,'
      '       go.mb,'
      '       go.godina,'
      '       go.datum,'
      '       go.vid_document,'
      '       hvd.naziv as vidDokumentNaziv,'
      '       go.odobrenie,'
      '       go.koristenje,'
      '       go.prv_del,'
      '       go.vtor_del,'
      '       go.mb_rakovoditel,'
      '       hrv.prezime || '#39' '#39'|| hrv.ime as NazivVraboten,'
      '       (select rabm.naziv'
      '       from hr_vraboten_rm hvr'
      '       inner join hr_rm_re hrm on hrm.id = hvr.id_rm_re'
      '       inner join hr_rabotno_mesto rabm on rabm.id = hrm.id_rm'
      
        '       where hvr.mb = go.mb and (go.datum between hvr.datum_od a' +
        'nd coalesce(hvr.datum_do, current_date)))  as RMNaziv,'
      '       go.datum1_od,'
      '       go.datum1_do,'
      '       go.datum2_od,'
      '       go.datum2_do,'
      '       case go.odobrenie when '#39'0'#39' then '#39#1053#1077#39
      '                         when '#39'1'#39' then '#39#1044#1072#39
      '       end "OdobrenieNaziv",'
      '       case go.koristenje when '#39'1'#39' then '#39#1045#1076#1085#1086#1082#1088#1072#1090#1085#1086#39
      '                          when '#39'2'#39' then '#39#1044#1074#1086#1082#1088#1072#1090#1085#1086#39
      '       end "KoristenjeNaziv",'
      '       go.data,'
      '       go.ts_ins,'
      '       go.ts_upd,'
      '       go.usr_upd,'
      '       go.usr_ins'
      'from hr_baranje_go go'
      'inner join hr_vid_dokument hvd on hvd.id = go.vid_document'
      'inner join hr_vraboten hrv on hrv.mb = go.mb'
      'where go.id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 360
    Top = 104
    object BaranjeZaGOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object BaranjeZaGOMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object BaranjeZaGOGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object BaranjeZaGODATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object BaranjeZaGOVID_DOCUMENT: TFIBIntegerField
      FieldName = 'VID_DOCUMENT'
    end
    object BaranjeZaGOVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object BaranjeZaGOODOBRENIE: TFIBSmallIntField
      FieldName = 'ODOBRENIE'
    end
    object BaranjeZaGOKORISTENJE: TFIBIntegerField
      FieldName = 'KORISTENJE'
    end
    object BaranjeZaGOPRV_DEL: TFIBIntegerField
      FieldName = 'PRV_DEL'
    end
    object BaranjeZaGOVTOR_DEL: TFIBIntegerField
      FieldName = 'VTOR_DEL'
    end
    object BaranjeZaGOMB_RAKOVODITEL: TFIBStringField
      FieldName = 'MB_RAKOVODITEL'
      Size = 100
      EmptyStrToNull = True
    end
    object BaranjeZaGONAZIVVRABOTEN: TFIBStringField
      FieldName = 'NAZIVVRABOTEN'
      Size = 151
      EmptyStrToNull = True
    end
    object BaranjeZaGORMNAZIV: TFIBStringField
      FieldName = 'RMNAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object BaranjeZaGODATUM1_OD: TFIBDateField
      FieldName = 'DATUM1_OD'
    end
    object BaranjeZaGODATUM1_DO: TFIBDateField
      FieldName = 'DATUM1_DO'
    end
    object BaranjeZaGODATUM2_OD: TFIBDateField
      FieldName = 'DATUM2_OD'
    end
    object BaranjeZaGODATUM2_DO: TFIBDateField
      FieldName = 'DATUM2_DO'
    end
    object BaranjeZaGOOdobrenieNaziv: TFIBStringField
      FieldName = 'OdobrenieNaziv'
      Size = 2
      EmptyStrToNull = True
    end
    object BaranjeZaGOKoristenjeNaziv: TFIBStringField
      FieldName = 'KoristenjeNaziv'
      Size = 10
      EmptyStrToNull = True
    end
    object BaranjeZaGODATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxBaranjeZaGO: TfrxDBDataset
    UserName = #1041#1072#1088#1072#1114#1077#1047#1072#1043#1054
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'GODINA='#1043#1086#1076#1080#1085#1072
      'DATUM='#1044#1072#1090#1091#1084
      'VID_DOCUMENT='#1064#1080#1092#1088#1072' '#1085#1072' '#1074#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      'VIDDOKUMENTNAZIV='#1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      'ODOBRENIE='#1054#1076#1086#1073#1088#1077#1085#1086' '#1086#1076
      'KORISTENJE='#1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' ('#1064#1080#1092#1088#1072')'
      'PRV_DEL='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1087#1088#1074' '#1076#1077#1083
      'VTOR_DEL='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1074#1090#1086#1088' '#1076#1077#1083
      'MB_RAKOVODITEL='#1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      'NAZIVVRABOTEN='#1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086#1048#1084#1077' '#1048#1084#1077
      'RMNAZIV='#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'DATUM1_OD='#1055#1088#1074' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1086#1076')'
      'DATUM1_DO='#1087#1088#1074' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1076#1086')'
      'DATUM2_OD='#1074#1090#1086#1088' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1086#1076')'
      'DATUM2_DO='#1074#1090#1086#1088' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1076#1086')'
      'OdobrenieNaziv='#1054#1076#1086#1073#1088#1077#1085#1086' ('#1076#1072'/'#1085#1077')'
      'KoristenjeNaziv='#1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077)
    DataSet = BaranjeZaGO
    BCDToCurrency = False
    Left = 224
    Top = 104
  end
  object frxResenieGO: TfrxDBDataset
    UserName = #1056#1077#1096#1077#1085#1080#1077#1047#1072#1043#1054
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'GODINA='#1043#1086#1076#1080#1085#1072
      'DATUM='#1044#1072#1090#1091#1084' '#1085#1072' '#1076#1086#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      'VID_DOKUMENT=VID_DOKUMENT'
      'VIDDOKUMENTNAZIV=VIDDOKUMENTNAZIV'
      'KORISTENJE=KORISTENJE'
      'PRV_DEL='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1087#1088#1074#1080#1086#1090' '#1076#1077#1083
      'VTOR_DEL='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1074#1090#1086#1088#1080#1086#1090' '#1076#1077#1083
      'MB_RAKOVODITEL='#1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      'NAZIVVRABOTEN='#1055#1088#1077#1079#1080#1084#1077' '#1080' '#1048#1084#1077' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085
      'RMNAZIV='#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'DATUM1_OD='#1055#1088#1074' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1086#1076')'
      'DATUM1_DO='#1055#1088#1074' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1076#1086')'
      'DATUM2_OD='#1042#1090#1086#1088' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1086#1076')'
      'DATUM2_DO='#1042#1090#1086#1088' '#1076#1077#1083' ('#1076#1072#1090#1091#1084' '#1076#1086')'
      'KoristenjeNaziv='#1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
      'STAZ='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1089#1090#1072#1078
      'STEPEN_NA_SLOZENOST='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1089#1090#1077#1087#1077#1085' '#1085#1072' '#1089#1083#1086#1078#1077#1085#1086#1089#1090
      'ZDRASTVENA_SOSTOJBA='#1041#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1079#1076#1088#1072#1089#1090#1077#1074#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
      'SUMAGO='#1042#1082#1091#1087#1085#1086' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      'BARANJE_GO='#1064#1080#1092#1088#1072' '#1085#1072' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1086#1076#1084#1086#1088
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'DATA=DATA')
    DataSet = ResenieZaGO
    BCDToCurrency = False
    Left = 224
    Top = 160
  end
  object ResenieZaGO: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RESENIE_GO'
      'SET '
      '    MB = :MB,'
      '    GODINA = :GODINA,'
      '    DATUM = :DATUM,'
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    KORISTENJE = :KORISTENJE,'
      '    PRV_DEL = :PRV_DEL,'
      '    VTOR_DEL = :VTOR_DEL,'
      '    MB_RAKOVODITEL = :MB_RAKOVODITEL,'
      '    DATUM1_OD = :DATUM1_OD,'
      '    DATUM1_DO = :DATUM1_DO,'
      '    DATUM2_OD = :DATUM2_OD,'
      '    DATUM2_DO = :DATUM2_DO,'
      '    STAZ = :STAZ,'
      '    STEPEN_NA_SLOZENOST = :STEPEN_NA_SLOZENOST,'
      '    ZDRASTVENA_SOSTOJBA = :ZDRASTVENA_SOSTOJBA,'
      '    BARANJE_GO = :BARANJE_GO,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select go.id,'
      '       go.mb,'
      '       go.godina,'
      '       go.datum,'
      '       go.vid_dokument,'
      '       hvd.naziv as vidDokumentNaziv,'
      '       go.koristenje,'
      '       go.prv_del,'
      '       go.vtor_del,'
      '       go.mb_rakovoditel,'
      '       hrv.prezime || '#39' '#39'|| hrv.ime as NazivVraboten,'
      '       (select rabm.naziv'
      '       from hr_vraboten_rm hvr'
      '       inner join hr_rm_re hrm on hrm.id = hvr.id_rm_re'
      '       inner join hr_rabotno_mesto rabm on rabm.id = hrm.id_rm'
      
        '       where hvr.mb = go.mb and (go.datum between hvr.datum_od a' +
        'nd coalesce(hvr.datum_do, current_date))) as RMNaziv,'
      '       go.datum1_od,'
      '       go.datum1_do,'
      '       go.datum2_od,'
      '       go.datum2_do,'
      '       case go.koristenje when '#39'1'#39' then '#39#1045#1076#1085#1086#1082#1088#1072#1090#1085#1086#39
      '                          when '#39'2'#39' then '#39#1044#1074#1086#1082#1088#1072#1090#1085#1086#39
      '       end "KoristenjeNaziv",'
      '       go.staz,'
      '       go.stepen_na_slozenost,'
      '       go.zdrastvena_sostojba,'
      
        '       go.staz + go.stepen_na_slozenost + go.zdrastvena_sostojba' +
        ' + 20 as sumaGO,'
      '       go.baranje_go,   '
      '       go.ts_ins,'
      '       go.ts_upd,'
      '       go.usr_ins,'
      '       go.usr_upd,'
      '       go.data'
      'from hr_resenie_go go'
      'inner join hr_vid_dokument hvd on hvd.id = go.vid_dokument'
      'inner join hr_vraboten hrv on hrv.mb = go.mb'
      'where go.id=:id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 360
    Top = 160
    object ResenieZaGOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object ResenieZaGOMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object ResenieZaGOGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object ResenieZaGODATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object ResenieZaGOVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object ResenieZaGOVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object ResenieZaGOKORISTENJE: TFIBSmallIntField
      FieldName = 'KORISTENJE'
    end
    object ResenieZaGOPRV_DEL: TFIBIntegerField
      FieldName = 'PRV_DEL'
    end
    object ResenieZaGOVTOR_DEL: TFIBIntegerField
      FieldName = 'VTOR_DEL'
    end
    object ResenieZaGOMB_RAKOVODITEL: TFIBStringField
      FieldName = 'MB_RAKOVODITEL'
      Size = 100
      EmptyStrToNull = True
    end
    object ResenieZaGONAZIVVRABOTEN: TFIBStringField
      FieldName = 'NAZIVVRABOTEN'
      Size = 151
      EmptyStrToNull = True
    end
    object ResenieZaGORMNAZIV: TFIBStringField
      FieldName = 'RMNAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object ResenieZaGODATUM1_OD: TFIBDateField
      FieldName = 'DATUM1_OD'
    end
    object ResenieZaGODATUM1_DO: TFIBDateField
      FieldName = 'DATUM1_DO'
    end
    object ResenieZaGODATUM2_OD: TFIBDateField
      FieldName = 'DATUM2_OD'
    end
    object ResenieZaGODATUM2_DO: TFIBDateField
      FieldName = 'DATUM2_DO'
    end
    object ResenieZaGOKoristenjeNaziv: TFIBStringField
      FieldName = 'KoristenjeNaziv'
      Size = 10
      EmptyStrToNull = True
    end
    object ResenieZaGOSTAZ: TFIBSmallIntField
      FieldName = 'STAZ'
    end
    object ResenieZaGOSTEPEN_NA_SLOZENOST: TFIBSmallIntField
      FieldName = 'STEPEN_NA_SLOZENOST'
    end
    object ResenieZaGOZDRASTVENA_SOSTOJBA: TFIBSmallIntField
      FieldName = 'ZDRASTVENA_SOSTOJBA'
    end
    object ResenieZaGOSUMAGO: TFIBBCDField
      FieldName = 'SUMAGO'
      Size = 0
      RoundByScale = True
    end
    object ResenieZaGOBARANJE_GO: TFIBIntegerField
      FieldName = 'BARANJE_GO'
    end
    object ResenieZaGOTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object ResenieZaGOTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object ResenieZaGOUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object ResenieZaGOUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object ResenieZaGODATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object ResenieZaPreraspredelba: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RESENIE_PRERASPREDELBA'
      'SET '
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    MB = :MB,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    NOVO_RM = :NOVO_RM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    STARO_RM = :STARO_RM,'
      '    STATUS = :STATUS,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      ' select hrp.id,'
      '       hrp.vid_dokument,'
      '       hvd.naziv as vidDokumentNaziv,'
      '       hrp.mb,'
      '       hrv.prezime || '#39' '#39' || hrv.ime as VrabotenNaziv,'
      '       hrp.datum_od,'
      '       hrp.datum_do,'
      '       hrp.novo_rm,'
      '       hrp.ts_ins,'
      '       hrp.ts_upd,'
      '       hrp.usr_ins,'
      '       hrp.usr_upd,'
      '       hrp.staro_rm,'
      '       hrp.status, '
      '       (select hrmrab.naziv as staroNaziv'
      '        from hr_rm_re hrm'
      
        '        inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id' +
        '_rm'
      
        '        inner join hr_sistematizacija hs on hs.id = hrm.id_siste' +
        'matizacija'
      '        where hs.do_datum is null and hrm.id = staro_rm),'
      '       (select hrmrab.naziv as novoNaziv'
      '        from hr_rm_re hrm'
      
        '        inner join hr_rabotno_mesto hrmrab on hrmrab.id = hrm.id' +
        '_rm'
      
        '        inner join hr_sistematizacija hs on hs.id = hrm.id_siste' +
        'matizacija'
      '        where hs.do_datum is null and hrm.id = novo_rm),'
      '      hrp.data '
      'from hr_resenie_preraspredelba hrp'
      'inner join hr_vid_dokument hvd on hvd.id = hrp.vid_dokument'
      'inner join hr_vraboten hrv on hrv.mb = hrp.mb'
      'where  hrp.id = :id'
      'order by hrp.datum_od ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 360
    Top = 216
    object ResenieZaPreraspredelbaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object ResenieZaPreraspredelbaVID_DOKUMENT: TFIBIntegerField
      FieldName = 'VID_DOKUMENT'
    end
    object ResenieZaPreraspredelbaVIDDOKUMENTNAZIV: TFIBStringField
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaVRABOTENNAZIV: TFIBStringField
      FieldName = 'VRABOTENNAZIV'
      Size = 151
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object ResenieZaPreraspredelbaDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object ResenieZaPreraspredelbaNOVO_RM: TFIBIntegerField
      FieldName = 'NOVO_RM'
    end
    object ResenieZaPreraspredelbaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object ResenieZaPreraspredelbaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object ResenieZaPreraspredelbaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaSTARO_RM: TFIBIntegerField
      FieldName = 'STARO_RM'
    end
    object ResenieZaPreraspredelbaSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
    object ResenieZaPreraspredelbaSTARONAZIV: TFIBStringField
      FieldName = 'STARONAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaNOVONAZIV: TFIBStringField
      FieldName = 'NOVONAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object ResenieZaPreraspredelbaDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object frxResenieZaPreraspredelba: TfrxDBDataset
    UserName = #1056#1077#1096#1077#1085#1080#1077#1047#1072#1055#1088#1077#1088#1072#1089#1087#1088#1077#1076#1077#1083#1073#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'VID_DOKUMENT=VID_DOKUMENT'
      'VIDDOKUMENTNAZIV=VIDDOKUMENTNAZIV'
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'VRABOTENNAZIV='#1055#1088#1077#1079#1080#1084#1077' '#1080' '#1048#1084#1077
      'DATUM_OD='#1044#1072#1090#1091#1084' '#1086#1076
      'DATUM_DO='#1044#1072#1090#1091#1084' '#1076#1086
      'NOVO_RM=NOVO_RM'
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD'
      'STARO_RM=STARO_RM'
      'STATUS=STATUS'
      'STARONAZIV='#1057#1090#1072#1088#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'NOVONAZIV='#1053#1086#1074#1086' '#1088#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086)
    DataSet = ResenieZaPreraspredelba
    BCDToCurrency = False
    Left = 224
    Top = 216
  end
  object frxPrekinRabotenOdnos: TfrxDBDataset
    UserName = #1055#1088#1077#1082#1080#1085#1053#1072#1056#1072#1073#1086#1090#1077#1085#1054#1076#1085#1086#1089
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID='#1064#1080#1092#1088#1072
      'MB='#1052#1072#1090#1080#1095#1077#1085' '#1073#1088#1086#1112
      'ID_RE_FIRMA=ID_RE_FIRMA'
      'ID_RM_RE=ID_RM_RE'
      'DATUM_DO='#1044#1072#1090#1091#1084' '#1076#1086
      'OBRAZLOZENIE='#1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077
      'PRICINA='#1055#1088#1080#1095#1080#1085#1072' ('#1064#1080#1092#1088#1072')'
      'DIREKTOR='#1044#1080#1088#1077#1082#1090#1086#1088
      'LICENAZIV='#1055#1088#1077#1079#1080#1084#1077' '#1058#1072#1090#1082#1086#1074#1086' '#1048#1084#1077' '#1048#1084#1077
      'RABOTNOMESONAZIV='#1056#1072#1073#1086#1090#1085#1086' '#1084#1077#1089#1090#1086
      'RENAZIV='#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      'PRICINANAZIV='#1055#1088#1080#1095#1080#1085#1072
      'TS_INS=TS_INS'
      'TS_UPD=TS_UPD'
      'USR_INS=USR_INS'
      'USR_UPD=USR_UPD')
    DataSet = PrestanokNaRabotenOdnos
    BCDToCurrency = False
    Left = 224
    Top = 280
  end
  object PrestanokNaRabotenOdnos: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE HR_RESENIE_OTKAZ'
      'SET '
      '    MB = :MB,'
      '    ID_RE_FIRMA = :ID_RE_FIRMA,'
      '    ID_RM_RE = :ID_RM_RE,'
      '    DATUM_DO = :DATUM_DO,'
      '    OBRAZLOZENIE = :OBRAZLOZENIE,'
      '    PRICINA = :PRICINA,'
      '    DIREKTOR = :DIREKTOR,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DATA = :DATA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select hro.id,'
      '       hro.mb,'
      '       hro.id_re_firma,'
      '       hro.id_rm_re,'
      '       hro.datum_do,'
      '       hro.obrazlozenie,'
      '       hro.pricina,'
      '       hro.direktor,'
      
        '       hrv.prezime || '#39' '#39' || coalesce(hrv.tatkovo_ime, '#39#39') ||'#39' '#39 +
        '|| hrv.ime as LiceNaziv,'
      '       hrrabm.naziv as RabotnoMesoNaziv,'
      '       mr.naziv as ReNaziv,'
      '       hrvo.naziv as pricinaNaziv,'
      '       hro.ts_ins,'
      '       hro.ts_upd,'
      '       hro.usr_ins,'
      '       hro.usr_upd,'
      '       hro.data'
      'from hr_resenie_otkaz hro'
      'inner join hr_vraboten hrv on hrv.mb = hro.mb'
      'inner join hr_rm_re hrm on hrm.id = hro.id_rm_re'
      'inner join mat_re mr on mr.id = hrm.id_re'
      'inner join hr_rabotno_mesto hrrabm on hrrabm.id = hrm.id_rm'
      
        'left outer join hr_vid_prekin_rab_odnos hrvo on hrvo.id = hro.pr' +
        'icina'
      'where hro.id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 360
    Top = 280
    object PrestanokNaRabotenOdnosID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PrestanokNaRabotenOdnosMB: TFIBStringField
      FieldName = 'MB'
      Size = 13
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosID_RE_FIRMA: TFIBIntegerField
      FieldName = 'ID_RE_FIRMA'
    end
    object PrestanokNaRabotenOdnosID_RM_RE: TFIBIntegerField
      FieldName = 'ID_RM_RE'
    end
    object PrestanokNaRabotenOdnosDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object PrestanokNaRabotenOdnosOBRAZLOZENIE: TFIBStringField
      FieldName = 'OBRAZLOZENIE'
      Size = 1024
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosPRICINA: TFIBIntegerField
      FieldName = 'PRICINA'
    end
    object PrestanokNaRabotenOdnosDIREKTOR: TFIBStringField
      FieldName = 'DIREKTOR'
      Size = 50
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosLICENAZIV: TFIBStringField
      FieldName = 'LICENAZIV'
      Size = 202
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosRABOTNOMESONAZIV: TFIBStringField
      FieldName = 'RABOTNOMESONAZIV'
      Size = 200
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosRENAZIV: TFIBStringField
      FieldName = 'RENAZIV'
      Size = 100
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosPRICINANAZIV: TFIBStringField
      FieldName = 'PRICINANAZIV'
      Size = 500
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object PrestanokNaRabotenOdnosTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object PrestanokNaRabotenOdnosUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object PrestanokNaRabotenOdnosDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
end
