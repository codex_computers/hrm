{*******************************************************}
{                                                       }
{     ��������� :  ������ ��������                     }
{                  ������ �������                      }
{     ����� :  15.12.2011                               }
{                                                       }
{     ������ : 1.1.1.17                                }
{                                                       }
{*******************************************************}

unit Oglas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxCheckBox, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBarSkinnedCustForm, dxPSCore, dxPScxCommon,  ActnList, dxBar,
  cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit,
  cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxMemo, cxHint, cxGroupBox, cxRadioGroup, cxLabel,
  cxDBLabel, dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxSchedulerLnk,
  dxScreenTip, dxCustomHint, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TfrmOglas = class(TfrmMaster)
    txtBroj: TcxDBTextEdit;
    Label2: TLabel;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OBJAVEN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ISTEKUVA: TcxGridDBColumn;
    cxGrid1DBTableView1OBJAVEN: TcxGridDBColumn;
    cxGrid1DBTableView1PRIJAVUVANJE_DO: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1PROMENET_OD: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label3: TLabel;
    txtDatumObj: TcxDBDateEdit;
    Label4: TLabel;
    txtPrijavuvanjeDo: TcxDBDateEdit;
    Label5: TLabel;
    txtDatumIstekuva: TcxDBDateEdit;
    Label6: TLabel;
    txtOpis: TcxDBMemo;
    Label7: TLabel;
    txtObjaven: TcxDBTextEdit;
    cxHintStyleController1: TcxHintStyleController;
    cxDBRadioGroup1: TcxDBRadioGroup;
    Label8: TLabel;
    txtPromenetOd: TcxDBTextEdit;
    aPromeniStatus: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aRMOglas: TAction;
    N2: TMenuItem;
    dxBarLargeButton19: TdxBarLargeButton;
    aPregledNaMolbi: TAction;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    PregledNaMolbiZaOdbranOglas: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton20: TdxBarLargeButton;
    aMolbi: TAction;
    aPregledMolbiStatus: TAction;
    dxBarButton3: TdxBarButton;
    Label9: TLabel;
    cxDBLabel1: TcxDBLabel;
    dxBarButton4: TdxBarButton;
    aOglas: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS1: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_FIRMA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ1: TcxGridDBColumn;
    dxBarManager1Bar7: TdxBar;
    cGodina: TdxBarCombo;
    procedure aPromeniStatusExecute(Sender: TObject);
    procedure txtOpisDblClick(Sender: TObject);
    procedure aRMOglasExecute(Sender: TObject);
    procedure cxLabel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aPregledNaMolbiExecute(Sender: TObject);
    procedure PregledNaMolbiZaOdbranOglasExecute(Sender: TObject);
    procedure aMolbiExecute(Sender: TObject);
    procedure aPregledMolbiStatusExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOglasExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure cGodinaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOglas: TfrmOglas;

implementation

uses dmKonekcija, dmMaticni, dmResources, dmSistematizacija,
dmSystem, dmUnit, DaNe,Notepad, RMOglas, Molbi, Utils, dmReportUnit,
  dmUnitOtsustvo;

{$R *.dfm}

procedure TfrmOglas.aHelpExecute(Sender: TObject);
begin
  inherited;
       Application.HelpContext(130);
end;

procedure TfrmOglas.aMolbiExecute(Sender: TObject);
begin
   dmSis.tblRMOglas.Close;
   dmSis.tblRMOglas.ParamByName('id').AsInteger:=dmSis.tblOglasID.Value;
   dmSis.tblRMOglas.Open;
   if dmSis.tblRMOglas.IsEmpty then
      ShowMessage('�������� �� ����������/������������ ����� �� ����� �� �� ���� ������������ ������� ����� !!!')
   else
      begin
        frmMolbi:=TfrmMolbi.Create(Application);
        frmMolbi.Tag:=1;
        frmMolbi.ShowModal;
        frmMolbi.Free;
      end;
end;

procedure TfrmOglas.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    txtDatumObj.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dmSis.tblOglasBROJ.Value:=dmMat.zemiMax(dmsis.pMaxBrOglas,Null, Null, Null, Null, Null, Null, 'MAXBR');
    dmSis.tblOglasID_RE_FIRMA.Value:=firma;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� ������� /r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmOglas.aOglasExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30087;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.PregledOglas.Close;
     dm.PregledOglas.ParamByName('ID').Value:=dmSis.tblOglasID.Value;
     dm.PregledOglas.Open;

     dmSis.tblRMOglas.Close;
     dmsis.tblRMOglas.ParamByName('id').Value:=dmSis.tblOglasID.Value;
     dmSis.tblRMOglas.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;
//
//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmOglas.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + cGodina.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);end;

procedure TfrmOglas.aPregledMolbiStatusExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30085;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.OglasiPoRabotniMesta.Close;
     dm.OglasiPoRabotniMesta.ParamByName('status').Value:='1';
     dm.OglasiPoRabotniMesta.ParamByName('ID').Value:='%';
     dm.OglasiPoRabotniMesta.Open;

     dm.MolbiPoOglasi.Close;
     dm.MolbiPoOglasi.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;

end;

procedure TfrmOglas.aPregledNaMolbiExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30082;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.OglasiPoRabotniMesta.Close;
     dm.OglasiPoRabotniMesta.ParamByName('status').Value:='%';
     dm.OglasiPoRabotniMesta.ParamByName('ID').Value:='%';
     dm.OglasiPoRabotniMesta.Open;

     dm.MolbiPoOglasi.Close;
     dm.MolbiPoOglasi.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;
end;

procedure TfrmOglas.aPromeniStatusExecute(Sender: TObject);
begin
  inherited;
  if not dmSis.tblOglas.IsEmpty then
  begin
    if dmSis.tblOglas.State in [dsEdit,dsInsert] then dmSis.tblOglas.Cancel;
    if dmSis.tblOglasSTATUS.Value=1 then
    begin
         frmDaNe := TfrmDaNe.Create(self, '������� �� ������', '������� � �������.'+#10#13+'���� ��������� ������ �� �� ��������� �������� ���������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
           Abort
       else
       begin
          //�� �� update-��� ��������
           dmSis.tblOglas.Edit;
           dmsis.tblOglasSTATUS.Value:=0;
           dmSis.tblOglas.Post;
       end;
    end
    else
     if dmSis.tblOglasSTATUS.Value=0 then
     begin
         frmDaNe := TfrmDaNe.Create(self, '������� �� ������', '������� � ���������.'+#10#13+'���� ��������� ������ �� �� ��������� �������� �������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
            Abort
       else
       begin
          //�� �� update-��� ��������
           dmSis.tblOglas.Edit;
           dmsis.tblOglasSTATUS.Value:=1;
           dmSis.tblOglas.Post;
        end;
     end;
  end;
end;

procedure TfrmOglas.aRMOglasExecute(Sender: TObject);
begin
  inherited;
    frmRMOglas:=TfrmRMOglas.Create(Application);
    dmSis.tblRMOglas.Close;
    dmSis.tblRMOglas.ParamByName('id').AsInteger:=dmSis.tblOglasID.Value;
    dmSis.tblRMOglas.Open;
    frmRMOglas.ShowModal;
    frmRMOglas.Free;
end;

procedure TfrmOglas.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
        begin
          if (Validacija(dPanel) = false) then
            begin
              if ((st = dsInsert) and (not inserting)) then
                begin
                  if (txtPrijavuvanjeDo.Date<txtDatumObj.Date) and (txtPrijavuvanjeDo.Text<>'') then
                    begin
                       ShowMessage('��������� ����� �� ���������� � ����� �� ������� �� ��������� �� �������. �� ������ ������� ��������!');
                       txtPrijavuvanjeDo.SetFocus;
                    end
                  else  if (txtDatumIstekuva.Date<txtDatumObj.EditValue) and (txtDatumIstekuva.Text<>'') then
                    begin
                       ShowMessage('��������� ����� �� ���������� � ����� �� ������� �� ��������� �� �������. �� ������ ������� ��������!');
                       txtDatumIstekuva.SetFocus;
                    end
                  else
                    begin
                      cxGrid1DBTableView1.DataController.DataSet.Post;
                      dmSis.tblOglas.FullRefresh;
                      dPanel.Enabled:=false;
                      lPanel.Enabled:=true;
                      if (dmSis.tblOglasSTATUS.Value=1) and (dmSis.tblOglasDATUM_OBJAVEN.AsDateTime>=Now) then
                        begin
                          frmRMOglas:=TfrmRMOglas.Create(Application);
                          dmSis.tblRMOglas.Close;
                          dmSis.tblRMOglas.ParamByName('id').AsInteger:=dmSis.tblOglasID.Value;
                          dmSis.tblRMOglas.Open;
                          frmRMOglas.ShowModal;
                          frmRMOglas.Free;
                        end
                    end;
                end;

              if (st = dsEdit) then
                begin
                  if (txtPrijavuvanjeDo.Date<txtDatumObj.Date) and (txtPrijavuvanjeDo.Text<>'') then
                    begin
                       ShowMessage('��������� ����� �� ���������� � ����� �� ������� �� ��������� �� �������. �� ������ ������� ��������!');
                       txtPrijavuvanjeDo.SetFocus;
                    end
                  else  if (txtDatumIstekuva.Date<txtDatumObj.EditValue) and (txtDatumIstekuva.Text<>'') then
                    begin
                       ShowMessage('��������� ����� �� ���������� � ����� �� ������� �� ��������� �� �������. �� ������ ������� ��������!');
                       txtDatumIstekuva.SetFocus;
                    end
                  else
                    begin
                      cxGrid1DBTableView1.DataController.DataSet.Post;
                      dmSis.tblOglas.FullRefresh;
                      dPanel.Enabled:=false;
                      lPanel.Enabled:=true;
                      cxGrid1.SetFocus;
                    end;
                end;
            end;
        end;
end;

procedure TfrmOglas.cGodinaChange(Sender: TObject);
begin
  if (cGodina.Text = '����') then
         cxGrid1DBTableView1.DataController.Filter.Clear
     else if cGodina.Text <> '' then
      begin
          cxGrid1DBTableView1.DataController.Filter.BeginUpdate;
          try
            cxGrid1DBTableView1.DataController.Filter.Root.Clear;
            cxGrid1DBTableView1.DataController.Filter.Root.AddItem(cxGrid1DBTableView1GODINA, foEqual, StrToInt(cGodina.Text), cGodina.Text);
            cxGrid1DBTableView1.DataController.Filter.Active:=True;
          finally
            cxGrid1DBTableView1.DataController.Filter.EndUpdate;
          end;
     end;
end;

procedure TfrmOglas.cxLabel1Click(Sender: TObject);
begin
  inherited;
  aRMOglas.Execute;
end;

procedure TfrmOglas.FormCreate(Sender: TObject);
begin
  inherited;
  dmSis.tblOglas.Close;
//  dmsis.tblOglas.ParamByName('firma').AsInteger:=firma;
  dmSis.tblOglas.Open;
  dmSis.tblRMOglas.ParamByName('id').AsString:='%';
  dmSis.tblRMOglas.Open;
end;

procedure TfrmOglas.FormShow(Sender: TObject);
begin
  inherited;
  cGodina.Text:=IntToStr(dmKon.godina);
end;

procedure TfrmOglas.PregledNaMolbiZaOdbranOglasExecute(Sender: TObject);
var RptStream :TStream;
begin
     dmReport.tblReportDizajn2.Close;
     dmReport.tblReportDizajn2.ParamByName('br').Value:=30084;
     dmReport.tblReportDizajn2.ParamByName('app').Value:=dmkon.aplikacija;
     dmReport.tblReportDizajn2.Open;

     dm.OglasiPoRabotniMesta.Close;
     dm.OglasiPoRabotniMesta.ParamByName('status').Value:='%';
     dm.OglasiPoRabotniMesta.ParamByName('ID').Value:=dmSis.tblOglasID.Value;
     dm.OglasiPoRabotniMesta.Open;

     dm.MolbiPoOglasi.Close;
     dm.MolbiPoOglasi.Open;

     RptStream := dmReport.tblReportDizajn2.CreateBlobStream(dmReport.tblReportDizajn2DATA , bmRead);
     dmReport.frxReport1.LoadFromStream(RptStream) ;

//     dmReport.frxReport1.DesignReport();
     dmReport.frxReport1.ShowReport;

end;

procedure TfrmOglas.txtOpisDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dmSis.tblOglasOPIS.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dmSis.tblOglasOPIS.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

end.
